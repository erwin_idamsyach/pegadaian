<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Start extends CI_Controller{
    function Start(){
        parent::__construct();
    }

    function index(){
		if(!isLogin()){
			goLogin();
		}else{
			//header('Location: '.site_url('home'));	
			$data["filelist"] = "home/home";

			$get_access = $this->db->query("SELECT * FROM param_access");
			foreach ($get_access->result() as $acc) {
				# code...
				if (sessionValue('access_level') == $acc->access_level) {
					$data['title'] = $acc->access_call;
					$data["title_menu"] = $acc->access_call;
				}
			}
			$data["menu"] = "home";
			getHTMLWeb($data);
		}
    }	
}
