<!-- front.php, Create By : Erwin Idamsyach Putra -->
<!-- Hadi -->
<script type="text/javascript">
    jQuery(function($){
        $('#example1').DataTable();
    });
</script>
	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>&nbsp;</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
            <li class="active">Data Store</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
        	<?php
        	if(!isset($id_store)){
        		?>
			<div class="box box-default">
				<div class="box-body">
					<b>ADD STORE</b>
					<div style="border: 1px solid black; margin-bottom: 10px"></div>
					<form action="<?php echo site_url('data_store/add_store') ?>" method="post">
					<div class="row">
						<div class="col-md-3 col-sm-6">
							<div class="form-group">
								<label>Store Code <font color="red">*</font></label>
								<input type="text" name="kode" class="form-control" placeholder="Store Code" required>
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="form-group">
								<label>Store Name <font color="red">*</font></label>
								<input type="text" name="name" class="form-control" placeholder="Store Name" required>
							</div>
						</div>
						<div class="col-md-5 col-sm-6">
							<div class="form-group">
								<label>Address <font color="red">*</font></label>
								<input type="text" name="address" class="form-control" placeholder="Address">
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="form-group">
								<label>Phone Number <font color="red">*</font></label>
								<div class="input-group"><span class="input-group-addon">+62</span><input class="form-control" id="phone" name="phone" type="text" required /></div>
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="form-group">
								<label>Fax.</label>
								<div class="input-group"><span class="input-group-addon">+62</span><input class="form-control" id="fax" name="fax" type="text" /></div>
							</div>
						</div>
						
						<div class="col-md-9">
							<div class="form-group">
								<label>Available Services</label><br/>
								<?php
								$get_service = $this->db->query("SELECT * FROM tb_service WHERE ISNULL(delete_by) AND status='Y' ");
								foreach ($get_service->result() as $serv) {
									?>
									<label class="checbox">
										<input type="checkbox" name="service[]" value="<?php echo $serv->kode ?>"> <?php echo $serv->nama ?>
									</label>&nbsp;&nbsp;&nbsp;
									<?php								
								}
								?>
							</div>
						</div>
						<div class="col-md-12">
							<div class="pull-left">
								<button type="submit" class="btn btn-primary">Submit</button>
							</div>
						</div>
					</div>
					</form>
				</div>
			</div>
			<?php
        	}else{
        		$get_store_edit = $this->db->query("SELECT * FROM tb_store WHERE id='$id_store'");
        		foreach ($get_store_edit->result() as $ed) {
        			?>
        	<div class="box box-default">
				<div class="box-body">
					<b>EDIT STORE</b>
					<div style="border: 1px solid black; margin-bottom: 10px"></div>
					<form action="<?php echo site_url('data_store/update_store') ?>" method="post">
					<div class="row">
						<div class="col-md-3 col-sm-6">
							<div class="form-group">
								<label>Store Code <font color="red">*</font></label>
								<input type="text" name="kode" class="form-control" placeholder="Store Code" value="<?php echo $ed->kode_unit ?>" required>
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="form-group">
								<label>Store Name <font color="red">*</font></label>
								<input type="hidden" name="id" value="<?php echo $ed->id ?>">
								<input type="text" name="name" class="form-control" placeholder="Store Name" value="<?php echo $ed->store ?>" required >
							</div>
						</div>
						<div class="col-md-5 col-sm-6">
							<div class="form-group">
								<label>Address <font color="red">*</font></label>
								<input type="text" name="address" class="form-control" placeholder="Address" value="<?php echo $ed->alamat ?>" required>
							</div>
						</div>
						<div class="col-md-4 col-sm-6">
							<div class="form-group">
								<label>Phone Number <font color="red">*</font></label>
								<div class="input-group"><span class="input-group-addon">+62</span><input class="form-control" id="phone" name="phone" type="text" value="<?php echo $ed->telepon ?>"   /></div>
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="form-group">
								<label>Fax.</label>
								<div class="input-group"><span class="input-group-addon">+62</span><input class="form-control" id="fax" name="fax" type="text" value="<?php echo $ed->fax ?>" /></div>
							</div>
						</div>
						
						<div class="col-md-9">
							<div class="form-group">
								<label>Available Services</label><br/>
								<?php
								$arr = explode(',', $ed->service);
								$get_service = $this->db->query("SELECT * FROM tb_service WHERE ISNULL(delete_by) AND status='Y' ");
								foreach ($get_service->result() as $serv) {
									if(in_array($serv->kode, $arr)){
										?>
									<label class="checbox">
										<input type="checkbox" name="service[]" value="<?php echo $serv->kode ?>" checked="true"> <?php echo $serv->nama ?>
									</label>&nbsp;&nbsp;&nbsp;
										<?php
									}else{
										?>
									<label class="checbox">
										<input type="checkbox" name="service[]" value="<?php echo $serv->kode ?>"> <?php echo $serv->nama ?>
									</label>&nbsp;&nbsp;&nbsp;
										<?php
									}
									?>
									<?php								
								}
								?>
							</div>
						</div>
						<div class="col-md-12">
							<div class="pull-left">
								<button type="submit" class="btn btn-primary">Submit</button>
							</div>
						</div>
					</div>
					</form>
				</div>
			</div>
        		<?php
        		}
        	}
        	?>
			<div class="box box-default">
				<div class="box-body">
					<b>DATA STORE</b>
					<div style="border: 1px solid black; margin-bottom: 10px"></div>
					<table class="table table-bordered table-striped table-hover" id="example1">
						<thead>
							<tr>
								<th class="text-center">No</th>
								<th class="text-center">Code</th>
								<th class="text-center">Store</th>
								<th class="text-center">Address</th>
								<th class="text-center">Phone</th>
								<th class="text-center">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$no = 1;
							$get_store = $this->db->query("SELECT * FROM tb_store WHERE delete_by ='' AND kode!='004'");
							foreach ($get_store->result() as $store) {
								?>
							<tr>
								<td class="text-center"><?php echo $no++; ?></td>
								<td><?php echo $store->kode_unit ?></td>
								<td><?php echo $store->store ?></td>
								<td><?php echo $store->alamat ?></td>
								<td>+62 <?php echo $store->telepon ?></td>
								<td>
									<div class="pull-right">
										<a href="<?php echo site_url('data_store/edit_store').'/'.$store->id ?>" class="btn btn-warning" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;&nbsp;

										<a href="<?php echo site_url('data_store/delete_store').'/'.$store->id ?>" class="btn btn-danger" data-toggle="tooltip" data-placement="bottom" onclick="return confirm('Are you sure to delete this data?');" title="Delete"><i class="fa fa-trash"></i></a>
									</div>
								</td>
							</tr>
								<?php
							}
							?>
						</tbody>
					</table>
				</div>
			</div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->