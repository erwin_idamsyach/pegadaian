<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
* Controller Data Store 
* Create By : Erwin Idamsyach Putra
* 23 April 2016
*/

class data_store extends CI_Controller{
	function __construct(){
		parent::__construct();

		if(!isLogin()){
			goLogin();
		}
	}
	
	public function index(){
		$data['filelist'] = "data_store/front";
		$data['title'] = "Data Store";
		$data['title_menu'] = "Admin";
		$data['menu'] = "data_store";

		getHTMLWeb($data);
	}

	public function add_store(){
		$name    = $this->input->post('name');
		$address = $this->input->post('address');
		$phone   = $this->input->post('phone');
		$fax   = $this->input->post('fax');
		$kode   = $this->input->post('kode');
		$service = $this->input->post('service');

		$kode    = $this->db->query("SELECT kode FROM tb_store ORDER BY kode DESC LIMIT 0, 1");
		foreach ($kode->result() as $cod) {
			$kod = $cod->kode;
			$kod = $kod+1;
			if($kod < 10){
				$kod = "00".$kod;
			}else if($kod < 100){
				$kod = "0".$kod;
			}else{
				$kod;
			}
		}

		$arr     = array();
		foreach ($service as $do) {
			array_push($arr, $do);
		}
		/*$serv = array($dos);*/
		$new = implode(',', $arr);

		$data = array(
			'kode' => $kod,
			'store' => $name,
			'alamat' => $address,
			'telepon' => $phone,
			'fax' => $fax,
			'kode_unit' => $kode,
			'service'  => $new,
			'create_by' => sessionValue('nama'),
			'create_date'=> date('Y-m-d')
			);
		$this->db->insert('tb_store', $data);
		redirect(site_url('data_store'));
	}

	public function edit_store(){
		$id = $this->uri->segment(3);

		$data['filelist'] = "data_store/front";
		$data['title'] = "Data Store";
		$data['title_menu'] = "Admin";
		$data['menu'] = "data_store";
		$data['id_store'] = $id;

		getHTMLWeb($data);	
	}

	public function update_store(){
		$id 	 = $this->input->post('id');
		$name    = $this->input->post('name');
		$address = $this->input->post('address');
		$phone   = $this->input->post('phone');
		$fax   = $this->input->post('fax');
		$kode   = $this->input->post('kode');
		$service = $this->input->post('service');

		$arr     = array();
		foreach ($service as $do) {
			array_push($arr, $do);
		}
		/*$serv = array($dos);*/
		$new = implode(',', $arr);

		$data = array(
			'store' => $name,
			'alamat' => $address,
			'telepon' => $phone,
			'fax' => $fax,
			'kode_unit' => $kode,
			'service'  => $new,
			'update_by' => sessionValue('nama'),
			'update_date'=> date('Y-m-d')
			);
		$this->db->where('id', $id);
		$this->db->update('tb_store', $data);
		redirect(site_url('data_store'));
	}
	public function delete_store(){
		$id = $this->uri->segment(3);

		$data = array(
			'delete_by' => sessionValue('nama'),
			'delete_date' => date('Y-m-d')
			);

		$this->db->where('id', $id);
		$this->db->update('tb_store', $data);
		redirect(site_url('data_store'));
	}
}
?>