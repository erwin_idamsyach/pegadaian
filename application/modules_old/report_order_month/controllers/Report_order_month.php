<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
* Controller Report Order Day
* Create By : Erwin Idamsyach Putra
* 23 April 2016
*/
class Report_order_month extends CI_Controller{
	public function index(){
		$data['filelist'] = 'report_order_month/front';
		$data['title'] = 'Report Order';
		$data['title_menu'] = 'Admin';
		$data['menu'] = 'report_order_month';

		getHTMLWeb($data);
	}
	public function print_order_month(){
		$this->load->library('fpdf');
		$sub  = 0;
		$date = $this->uri->segment(3);
		$date = date_create($date);
		$para = date_format($date, 'Y-m-d');
		$num  = date_format($date, 'ym');
		$date = date_format($date, 'D, d-m-Y');

		$pdf = new FPDF('P','mm','A4');
		$pdf->SetFont('helvetica','b', 10);
		$pdf->AddPage();
		$pdf->Image('asset/logo-pegadaian/Logo.jpg', 138, 5, 60, 25);
		$pdf->SetXY(138, 32);
		$pdf->Cell(0, 0, 'Pegadaian Gemological Laboratory');
		$pdf->SetFont('helvetica','', 10);
		$pdf->SetXY(138, 36);
		$pdf->Cell(0, 0, 'Jalan Kramat Raya No. 162');
		$pdf->SetXY(138, 40);
		$pdf->Cell(0, 0, 'Jakarta Pusat, 10430');
		$pdf->Ln();

		$pdf->SetFont('helvetica','b', 14);
		$pdf->SetXY(0, 25);
		$pdf->Cell(0, 0, 'Laporan Order', 0, 0, 'C');
		$pdf->Ln();

		$pdf->SetFont('helvetica','', 12);
		$pdf->SetXY(5, 60);
		$pdf->Cell(0, 0, 'No. Laporan');
		$pdf->SetXY(35, 60);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(40, 60);
		$pdf->Cell(0, 0, 'LB-'.$num.'-01');

		$pdf->SetXY(5, 65);
		$pdf->Cell(0, 0, 'Tanggal');
		$pdf->SetXY(35, 65);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(40, 65);
		$pdf->Cell(0, 0, $date);
		$pdf->Ln();

		$pdf->Rect(10, 80, 15, 120);
		$pdf->Rect(25, 80, 35, 120);
		$pdf->Rect(60, 80, 50, 120);
		$pdf->Rect(110, 80, 30, 120);
		$pdf->Rect(140, 80, 60, 120);

		$pdf->SetFont('helvetica','', 10);
		$pdf->SetXY(10, 80);
		$pdf->Cell(15,8,'No',1,0,'C');
		$pdf->SetXY(25, 80);
		$pdf->Cell(35,8,'ID Order',1,0,'C');
		$pdf->SetXY(60, 80);
		$pdf->Cell(50,8,'Store',1,0,'C');
		$pdf->SetXY(110, 80);
		$pdf->Cell(30,8,'Total Speciment',1,0,'C');
		$pdf->SetXY(140, 80);
		$pdf->Cell(60,8,'Price',1,0,'C');
		$pdf->Ln();
		$no = 1;

		$get_today = $this->db->query("SELECT * FROM tb_front_desk, tb_store WHERE tb_front_desk.store = tb_store.kode AND MONTH(tb_front_desk.create_date) = MONTH(now()) AND tb_front_desk.delete_by='' GROUP BY tb_front_desk.id_order");

        foreach($get_today->result() as $get) {
	        $get_sum = $this->db->query("SELECT COUNT(*) as total_speciment FROM tb_front_desk WHERE id_order='".$get->id_order."' AND delete_by=''");
	        foreach ($get_sum->result() as $sum) {
	          $spec = $sum->total_speciment;
	        }
	        $sub = $sub+$get->price;
        	$pdf->SetX(10);
			$pdf->Cell(15,8,$no++,1,0,'C');
			$pdf->SetX(25);
			$pdf->Cell(35,8,$get->id_order,1,0,'C');
			$pdf->SetX(60);
			$pdf->Cell(50,8,$get->store,1,0,'C');
			$pdf->SetX(110);
			$pdf->Cell(30,8,$spec,1,0,'C');
			$pdf->SetX(140);
			$pdf->Cell(60,8,number_format($get->price, 0,',','.'),1,0,'C');
			$pdf->Ln();
        }
        $pdf->SetXY(140, 200);
        $pdf->Cell(60,8,number_format($sub, 0,',','.'),1,0,'C');
		$pdf->Output();
	}
}
?>