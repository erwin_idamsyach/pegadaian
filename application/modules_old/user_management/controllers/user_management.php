<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class user_management extends CI_Controller{
	function __construct(){
		parent::__construct();

		if(!isLogin()){
			goLogin();
		}
	}
	
	public function index(){
		$data["filelist"] = "user_management/form";
        $data["title"] = "User Management";
        $data["title_menu"] = "Admin";
        $data["menu"] = "user_manage";
		
        getHTMLWeb($data);
	}
	public function add_user(){
		$nama    = $this->input->post('name');
		$gender  = $this->input->post('gender');
		$email   = $this->input->post('email');
		$phone   = $this->input->post('phone');
		$user    = $this->input->post('user');
		$pass    = md5($this->input->post('pass'));
		$access	 = $this->input->post('access');
		$area    = $this->input->post('area');
		$avaliable_user    = $this->input->post('avaliable_user');

		if($this->input->post('area_two')){
			$artw    = $this->input->post('area_two');
		}else{
			$artw    = '';
		}

		if($this->input->post('area_three')){
			$arth    = $this->input->post('area_three');
		}else{
			$arth    = '';
		}

		if($this->input->post('area_four')){
			$arfo    = $this->input->post('area_four');
		}else{
			$arfo    = '';
		}
		
		$success = false;
		$msg = "";
		if($avaliable_user=="true"){		
			$tmp_loc = $_FILES['image']['tmp_name'];
			$name    = basename($_FILES['image']['name']);

			$folder  = "asset/foto-petugas/$name";

			$move    = move_uploaded_file($tmp_loc, $folder);
			if($move){
				$name = $name;
				$success = true;
				//redirect(site_url('user_management'));
			}else{
				$name = "";
				$success = true;
				//redirect(site_url('user_management'));
			}
			
			// Tanda Tangan
			$valid_file = true;
			if($_FILES['image_signature']['size'] > (102400)) //can't be larger than 1 MB
            {
                $valid_file = false;
                //$message = 'Oops!  Your file\'s size is to large.';
				$msg = "Ukuran tanda tangan terlalu besar.";
            }
			if($valid_file){
				$tmp_loc = $_FILES['image_signature']['tmp_name'];
				$tanda_tangan    = basename($_FILES['image_signature']['name']);

				$folder  = "asset/signatures/$tanda_tangan";

				$move    = move_uploaded_file($tmp_loc, $folder);
				if($move){
					$tanda_tangan = $tanda_tangan;
					$success = true;
					//redirect(site_url('user_management'));
				}else{
					$tanda_tangan = "";
					$success = true;
					//redirect(site_url('user_management'));
				}
			}
		}else{
			$success = false;
			$msg = "Username sudah digunakan";
		}
		
		if($success){
			if($name!=""){
				$data = array(
				'username' => $user,
				'password' => $pass,
				'nama' 	   => $nama,
				'email'    => $email,
				'gender'   => $gender,
				'phone'	   => $phone,
				'foto'	   => $name,
				'tanda_tangan' => $tanda_tangan,
				'access_level'  => $access,
				'store'    => $area,
				'store_two'    => $artw,
				'store_three'    => $arth,
				'store_four'    => $arfo,
				'create_by' => sessionValue('id'),
				'create_date'   => date('Y-m-d')
				);
				if($this->db->insert('tb_login', $data)){
					$success = true;
				}else{
					$success = false;
					$msg = "Gagal Disimpan";
				}
			}else{
				$data = array(
				'username' => $user,
				'password' => $pass,
				'nama' 	   => $nama,
				'email'    => $email,
				'gender'   => $gender,
				'phone'	   => $phone,
				'access_level'  => $access,
				'store'    => $area,
				'store_two'    => $artw,
				'store_three'    => $arth,
				'store_four'    => $arfo,
				'create_by' => sessionValue('id'),
				'create_date'   => date('Y-m-d')
				);
				if($this->db->insert('tb_login', $data)){
					$success = true;
				}else{
					$success = false;
					$msg = "Gagal Disimpan";
				}
			}
		}
		
		echo json_encode(array("success"=>$success, "msg"=>$msg));
	}

	public function cek_user(){
		$user = $this->uri->segment(3);
		$cek_ava = $this->db->query("SELECT username FROM tb_login WHERE username='$user' AND delete_by=''");
		if($cek_ava->num_rows()>0){
			echo json_encode(array("status" => "exist"));
		}else{
			echo json_encode(array("status" => "available"));
		}
	}
	public function ajax_data(){
		$id = $this->uri->segment(3);
		$data['id'] = $id;

		$this->load->view('modo', $data);
	}

	public function delete_user(){
		$id = $this->uri->segment(3);

		$data = array(
			'delete_by' => sessionValue('id'),
			'delete_date' => date('Y-m-d')
			);

		$this->db->where('id', $id);
		$this->db->update('tb_login', $data);
		redirect(site_url('user_management'));
	}

	public function edit_user(){
		$id = $this->uri->segment(3);
		$data["filelist"] = "user_management/form";
        $data["title"] = "User Management";
        $data["title_menu"] = "Admin";
        $data["menu"] = "user_manage";
		$data["idd"] = $id;
        getHTMLWeb($data);
	}

	public function update_user(){
		$id      = $this->input->post('idd');
		$nama    = $this->input->post('name');
		$gender  = $this->input->post('gender');
		$email   = $this->input->post('email');
		$phone   = $this->input->post('phone');
		$user    = $this->input->post('user');
		$pass    = md5($this->input->post('pass'));
		$access	 = $this->input->post('access');
		$area    = $this->input->post('area');
		
		
		$pass    = md5($this->input->post('pass'));
		
		$success = false;
		$msg = "";
		
		if($this->input->post('pass')==$this->input->post('repass')){
			$success = true;
		}else{
			$msg = "Password not valid";
		}
		
		if($success){
			if($_FILES['image']['size'] != 0){
				$tmp_loc = $_FILES['image']['tmp_name'];
				$name    = basename($_FILES['image']['name']);

				$folder  = "asset/foto-petugas/$name";

				$move    = move_uploaded_file($tmp_loc, $folder);
				if($move){
					if($this->input->post('pass')==""){
						$data = array(
							'nama' 	   => $nama,
							'email'    => $email,
							'gender'   => $gender,
							'phone'	   => $phone,
							'foto'	   => $name,
							'access_level'  => $access,
							'store'    => $area,
							'update_by' => sessionValue('id'),
							'update_date'   => date('Y-m-d')
						);
					}else{
						$data = array(
							'nama' 	   => $nama,
							'password' => $pass,
							'email'    => $email,
							'gender'   => $gender,
							'phone'	   => $phone,
							'foto'	   => $name,
							'access_level'  => $access,
							'store'    => $area,
							'update_by' => sessionValue('id'),
							'update_date'   => date('Y-m-d')
						);
					}
					$this->db->where('id', $id);
					$this->db->update('tb_login', $data);
					
					$success = true;
					//redirect(site_url('user_management'));
				}else{
				}
			}else{
				if($_FILES['image_signature']['size'] != 0){
					$valid_file = true;
					if($_FILES['image_signature']['size'] > (102400)) //can't be larger than 1 MB
					{
						$valid_file = false;
						//$message = 'Oops!  Your file\'s size is to large.';
						$msg = "Ukuran tanda tangan terlalu besar.";
					}
					if($valid_file){
						$tmp_loc = $_FILES['image_signature']['tmp_name'];
						$tanda_tangan    = basename($_FILES['image_signature']['name']);

						$folder  = "asset/signatures/$tanda_tangan";

						$move    = move_uploaded_file($tmp_loc, $folder);
						if($move){
							$dataSign = array(
								'tanda_tangan'	   => $tanda_tangan,
								'update_by' => sessionValue('id'),
								'update_date'   => date('Y-m-d')
							);
							$this->db->where('id', $id);
							$this->db->update('tb_login', $dataSign);
						}
					}
				}
				if($this->input->post('pass')==""){
					$data = array(
						'nama' 	   => $nama,
						'email'    => $email,
						'gender'   => $gender,
						'phone'	   => $phone,
						'access_level'  => $access,
						'store'    => $area,
						'update_by' => sessionValue('id'),
						'update_date'   => date('Y-m-d')
						);
				}else{
					$data = array(
						'nama' 	   => $nama,
						'password' => $pass,
						'email'    => $email,
						'gender'   => $gender,
						'phone'	   => $phone,
						'access_level'  => $access,
						'store'    => $area,
						'update_by' => sessionValue('id'),
						'update_date'   => date('Y-m-d')
						);
				}
					$this->db->where('id', $id);
					$this->db->update('tb_login', $data);
					
					$success = true;
					//redirect(site_url('user_management'));
			}
		}
		echo json_encode(array("success"=>$success, "msg"=>$msg));
		
	}
}
?>