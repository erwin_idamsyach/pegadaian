<!-- CODE CREATE BY ERWIN, 21 April 2016 -->
<!-- Hadi -->
<script type="text/javascript">
    jQuery(function($){
        $('#example1').DataTable();
		
			$('#form-usermanagement').trigger("reset");
			$("#form-usermanagement").on('submit',function(e){
			e.preventDefault();
			var formatData = new FormData($(this)[0]);
				swal({
				 title: "",
				 text: "Apakah Anda akan menyimpan data?",
				 type: "warning",
				 showCancelButton: true,
				 confirmButtonColor: "#DD6B55",
				 confirmButtonText: "Ya",
				 cancelButtonText: "Tidak",
				 closeOnConfirm: false }, function(){
					$.ajax({
						type : 'POST',
						url : '<?php echo base_url()?>user_management/add_user',
						data : formatData,
						async: false,
						cache: false,
						contentType: false,
						processData: false,
						success:function(html){
							//window.location.reload();
							var data = eval ("(" + html + ")");
							if(data.success){
								swal({
								 title: "Berhasil Menyimpan data!",
								 text: "Klik tombol di bawah.",
								 type: "success",
								 showCancelButton: false,
								 confirmButtonColor: "#257DB6",
								 confirmButtonText: "Ok!",
								 closeOnConfirm: false
								},
								function(){
									location.href = base_url+"user_management";
								});
							}else{
								swal({
								 title: data.msg,
								 text: "Klik tombol di bawah.",
								 type: "warning",
								 showCancelButton: false,
								 confirmButtonColor: "#257DB6",
								 confirmButtonText: "Ok!",
								 closeOnConfirm: false
								});
							}
						}
					});
				});
			});
			
			$('#form-usermanagement-edit').trigger("reset");
			$("#form-usermanagement-edit").on('submit',function(e){
			e.preventDefault();
			var formatData = new FormData($(this)[0]);
				swal({
				 title: "",
				 text: "Apakah Anda akan menyimpan data?",
				 type: "warning",
				 showCancelButton: true,
				 confirmButtonColor: "#DD6B55",
				 confirmButtonText: "Ya",
				 cancelButtonText: "Tidak",
				 closeOnConfirm: false }, function(){
					$.ajax({
						type : 'POST',
						url : '<?php echo base_url()?>user_management/update_user',
						data : formatData,
						async: false,
						cache: false,
						contentType: false,
						processData: false,
						success:function(html){
							//window.location.reload();
							var data = eval ("(" + html + ")");
							if(data.success){
								swal({
								 title: "Berhasil Menyimpan data!",
								 text: "Klik tombol di bawah.",
								 type: "success",
								 showCancelButton: false,
								 confirmButtonColor: "#257DB6",
								 confirmButtonText: "Ok!",
								 closeOnConfirm: false
								},
								function(){
									location.href = base_url+"user_management";
								});
							}else{
								swal({
								 title: data.msg,
								 text: "Klik tombol di bawah.",
								 type: "warning",
								 showCancelButton: false,
								 confirmButtonColor: "#257DB6",
								 confirmButtonText: "Ok!",
								 closeOnConfirm: false
								});
							}
						}
					});
				});
			});
    });
	
	function deleteData(id){
		swal({
            title: "",
            text: "Apakah Anda akan menghapus data?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: false },
			function(isConfirm) {
				if (isConfirm) {
					location.href = base_url+"user_management/delete_user/"+id;
				} else {
					
				}
			}
        );		
		return false;
	}
</script>
    <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>&nbsp;</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
            <li class="active">User Management</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <?php
          if(!isset($idd)){
            ?>
          <div class="box box-default">
            <div class="box-body">
              <b>ADD USER</b>
              <div style="border: 1px solid black; margin-bottom: 10px;"></div>
              <form id="form-usermanagement" method="post" enctype="multipart/form-data">
			  <input type="hidden" name="avaliable_user" id="avaliable_user" value="false">
              <div class="row">
                <div class="col-md-3 col-sm-3">
				  <div class="row">
					<div class="col-md-12 col-sm-12">
					  <div class="form-group">
						<label>Photo</label>
						<input type="file" name="image" class="form-control drop" data-height="118px">
					  </div>
					  
					  <div class="form-group">
						<label>Tanda Tangan (Maximal size 100 Kb)</label>
						<input type="file" name="image_signature" class="form-control drop" data-height="118px">
					  </div>
					</div>
				  </div>                  
                </div>
                <div class="col-md-9 col-sm-9">
                  <div class="row">
                    <div class="col-md-4 col-sm-6">
                      <div class="form-group">
                        <label>Name <font color="red">*</font></label>
                        <input type="text" name="name" class="form-control" placeholder="officer name.." required>
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                      <div class="form-group">
                        <label>Gender</label>
                        <select name="gender" class="form-control">
                          <option value="" disabled selected>--Select gender--</option>
                          <option value="Laki-Laki">Male</option>
                          <option value="Perempuan">Female</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                      <div class="form-group">
                        <label>Email</label>
                        <input type="email" name="email" class="form-control" placeholder="email">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                      <div class="form-group">
                        <label>Phone Number</label>
                        <input type="text" name="phone" class="form-control" placeholder="phone number..">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                      <div class="form-group form-user">
                        <label>Username <font color="red">*</font></label>
                        <input type="text" name="user" id="user" class="form-control" placeholder="username.." onkeyup="cek_user()" required>
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                      <div class="form-group" id="password-1">
                        <label>Password <font color="red">*</font></label>
                        <input type="password" id="pass-1" name="pass" class="form-control" placeholder="password.." style="height: 34px;" required="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                      <div class="form-group" id="password-2">
                        <label>Retype Password</label>
                        <input type="password" id="pass-2" name="repass" class="form-control" placeholder="retype password.." style="height: 34px;" onkeyup="cek_same()" required="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                      <div class="form-group">
                        <label>Access Level <font color="red">*</font></label>
                        <select name="access" class="form-control access" required="" onchange="cek_lab()">
                          <option value="" disabled selected>--Select access--</option>
                          <?php
                          $get_access = $this->db->query("SELECT * FROM param_access");
                          foreach ($get_access->result() as $get) {
                            ?>
                            <option value="<?php echo $get->access_level ?>"><?php echo $get->access_call ?></option>
                            <?php
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                      <div class="form-group">
                        <label>Work Area I <font color="red">*</font></label>
                        <select name="area" class="form-control" required="">
                          <option value="" selected disabled>--Select area--</option>
                          <?php
                          $get_area = $this->db->query("SELECT * FROM tb_store");
                          foreach ($get_area->result() as $area) {
                            ?>
                          <option value="<?php echo $area->kode ?>"><?php echo $area->store ?></option>
                            <?php
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                      <div class="form-group">
                        <label>Work Area II</label>
                        <select name="area_two" class="form-control disb">
                          <option value="" selected disabled>--Select area--</option>
                          <?php
                          $get_area = $this->db->query("SELECT * FROM tb_store");
                          foreach ($get_area->result() as $area) {
                            ?>
                          <option value="<?php echo $area->kode ?>"><?php echo $area->store ?></option>
                            <?php
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                      <div class="form-group">
                        <label>Work Area III</label>
                        <select name="area_three" class="form-control disb">
                          <option value="" selected disabled>--Select area--</option>
                          <?php
                          $get_area = $this->db->query("SELECT * FROM tb_store");
                          foreach ($get_area->result() as $area) {
                            ?>
                          <option value="<?php echo $area->kode ?>"><?php echo $area->store ?></option>
                            <?php
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                      <div class="form-group">
                        <label>Work Area IV</label>
                        <select name="area_four" class="form-control disb">
                          <option value="" selected disabled>--Select area--</option>
                          <?php
                          $get_area = $this->db->query("SELECT * FROM tb_store");
                          foreach ($get_area->result() as $area) {
                            ?>
                          <option value="<?php echo $area->kode ?>"><?php echo $area->store ?></option>
                            <?php
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="pull-right">
                        <button type="submit" class="btn btn-primary">Submit</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              </form>
            </div>
          </div>
          <?php
          }else{
            $get_data_log = $this->db->query("SELECT * FROM tb_login WHERE id='$idd'");
            foreach ($get_data_log->result() as $log) {
            ?>
          <div class="box box-default">
            <div class="box-body">
              <b>EDIT USER</b>
              <div style="border: 1px solid black; margin-bottom: 10px;"></div>
              <form id="form-usermanagement-edit" method="post" enctype="multipart/form-data">
              <div class="row">
                <div class="col-md-3 col-sm-3">
				  <div class="row">
					<div class="col-md-12 col-sm-12">
					  <div class="form-group">
						<label>Photo</label>
						<input type="hidden" name="idd" value="<?php echo $log->id ?>">
						<input type="file" name="image" class="form-control drop" data-height="118px" data-default-file="<?php echo base_url() ?>asset/foto-petugas/<?php echo $log->foto ?>">
					  </div>
					  <div class="form-group">
						<label>Tanda Tangan (Maximal size 100 Kb)</label>
						<input type="file" name="image_signature" class="form-control drop" data-height="118px" data-default-file="<?php echo base_url() ?>asset/signatures/<?php echo $log->tanda_tangan ?>">
					  </div>
					</div>
				  </div>
                </div>
                <div class="col-md-9 col-sm-9">
                  <div class="row">
                    <div class="col-md-4 col-sm-6">
                      <div class="form-group">
                        <label>Name</label>
                        <input type="text" name="name" class="form-control" placeholder="officer name.." value="<?php echo $log->nama ?>">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                      <div class="form-group">
                        <label>Gender</label>
                        <select name="gender" class="form-control">
                        <?php
                        if($log->gender == "Laki-Laki"){
                          ?>
                          <option value="" disabled selected>--Select gender--</option>
                          <option value="Laki-Laki" selected="true">Male</option>
                          <option value="Perempuan">Female</option>
                          <?php
                        }else if($log->gender == "Perempuan"){
                          ?>
                          <option value="" disabled selected>--Select gender--</option>
                          <option value="Laki-Laki">Male</option>
                          <option value="Perempuan" selected="true">Female</option>
                          <?php
                        }else{
                          ?>
                          <option value="" disabled selected>--Select gender--</option>
                          <option value="Laki-Laki">Male</option>
                          <option value="Perempuan" selected="true">Female</option>
                          <?php
                        }
                        ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                      <div class="form-group">
                        <label>Email</label>
                        <input type="email" name="email" class="form-control" placeholder="email" value="<?php echo $log->email ?>">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                      <div class="form-group" id="password-1">
                        <label>Password</label>
                        <input type="password" id="pass-1" name="pass" class="form-control" placeholder="password.." style="height: 34px;">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                      <div class="form-group" id="password-2">
                        <label>Retype Password</label>
                        <input type="password" id="pass-2" name="repass" class="form-control" placeholder="retype password.." style="height: 34px;" onkeyup="cek_same()">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                      <div class="form-group">
                        <label>Phone Number</label>
                        <input type="text" name="phone" class="form-control" placeholder="phone number.." value="<?php echo $log->phone ?>">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                      <div class="form-group">
                        <label>Access Level</label>
                        <select name="access" class="form-control">
                          <option value="" disabled selected>--Select access--</option>
                          <?php
                          $get_access = $this->db->query("SELECT * FROM param_access");
                          foreach ($get_access->result() as $get) {
                            if($log->access_level == $get->access_level){
                              ?>
                            <option value="<?php echo $get->access_level ?>" selected="true"><?php echo $get->access_call ?></option>
                              <?php
                            }else{
                              ?>
                            <option value="<?php echo $get->access_level ?>"><?php echo $get->access_call ?></option>
                              <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                      <div class="form-group">
                        <label>Work Area</label>
                        <select name="area" class="form-control">
                          <option value="" selected disabled>--Select area--</option>
                          <?php
                          $get_area = $this->db->query("SELECT * FROM tb_store");
                          foreach ($get_area->result() as $area) {
                            if($log->store == $area->kode){
                              ?>
                          <option value="<?php echo $area->kode ?>" selected="true"><?php echo $area->store ?></option>
                              <?php
                            }else{
                              ?>
                          <option value="<?php echo $area->kode ?>"><?php echo $area->store ?></option>
                              <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="pull-right">
                        <button type="submit" class="btn btn-primary">Submit</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              </form>
            </div>
          </div>  
            <?php
            }
          }
          ?>

          <div class="box box-default">
            <div class="box-body">
            <b>DATA USER</b>
              <div style="border: 1px solid black; margin-bottom: 10px;"></div>
            <div class="container-fluid">
              <table class="table table-bordered table-striped table-hover table-paging" id="example1">
                <thead>
                  <tr>
                    <th class="text-center">No</th>
                    <th class="text-center">Name</th>
                    <th class="text-center">Phone</th>
                    <th class="text-center">Access Level</th>
                    <th class="text-center">Store</th>
                    <th class="text-center">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $no = 1;
                  $get_user = $this->db->query("SELECT
                                  tb_login.id,
                                  tb_login.nama, tb_login.phone,
                                  tb_login.access_level, tb_store.store
                                  FROM 
                                  tb_login, tb_store
                                  WHERE
                                  tb_login.store = tb_store.kode AND tb_login.delete_by= ''");
                  foreach ($get_user->result() as $get) {
                    ?>
                <tr>
                  <td class="text-center"><?php echo $no++; ?></td>
                  <td><?php echo $get->nama; ?></td>
                  <td><?php echo $get->phone; ?></td>
                  <td><?php echo $get->access_level; ?></td>
                  <td><?php echo $get->store; ?></td>
                  <td>
                  <div class="pull-right">
                    <a href="<?php echo site_url('user_management/edit_user').'/'.$get->id ?>" class="btn btn-warning" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;
					<a href="#" class="btn btn-danger" data-toggle="tooltip" data-placement="bottom" onclick="deleteData('<?php echo $get->id;?>');" title="Delete"><i class="fa fa-trash"></i></a>
                  </div>
                  </td>
                </tr>
                    <?php
                  }
                  ?>
                </tbody>
              </table>
            </div>
            </div>
          </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <div class="modal fade" id="modal-user">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Data User</h4>
              <button class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
            </div>
            <div class="modal-body area-data-jos"></div>
          </div>
        </div>
      </div>