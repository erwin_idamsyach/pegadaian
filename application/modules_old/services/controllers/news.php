<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class News extends CI_Controller{
    function News(){
        parent::__construct();
    }

    function save_comment(){
        $id = $_POST['id'];
        $comment = $_POST['comment'];
        $run = $this->db->query("SELECT * FROM count_comment WHERE news_id=".$id);
        if($run->num_rows==0){
            $data = array(
                "news_id" => $id,
                "comment" => $comment
            );
            $this->db->insert("count_comment",$data);
        }else{
            foreach($run->result() as $viewerdata){
                $data = array(
                    "comment" => $comment
                );
                $this->db->where('news_id',$id);
                $this->db->update("count_comment",$data);
            }
        }
    }
}
