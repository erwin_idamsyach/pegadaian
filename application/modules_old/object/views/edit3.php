<?php
$ox = mysql_query("SELECT * FROM tb_taksiran where id='$id'");
$get = mysql_fetch_array($ox);
?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Main content -->
  <section class="content">

    <div class="box box-warning">
      <div class="box-body">
        <div class="col-md-12">
          <div class="row">
            <div class="form-group">
              <div class="col-md-12 text-right">            
                <h4><b>F-03 - Taksiran</b></h4>
              </div>
            </div>
          </div>
        </div>
      <form id="form-examination-taksiran" action="" method="post" id="form-taksiran">
        <div class="col-md-12">
          <div class="row">
            <div class="form-group">
              <div class="col-md-12">
                  <h5><b>DATA GEMOLOG</b></h5>
                  <hr style="border:1px solid black;margin-top:-5px;">
              </div>
            </div>
          </div>

            <div class="row">
                  <div class="col-md-3">
					<div class="form-group has-success">
                      <label>Gemologist Name <font color="red">*</font></label>
						<select name="gemolog" class="form-control" required>
						  <?php
						  $getGemolog = $this->db->query("SELECT nama nama_gemolog FROM tb_login WHERE store='".sessionValue('kode_store')."' AND access_level='Gemologist' AND delete_by='' ");
						  foreach ($getGemolog->result() as $gemo) {
							if($gemo->nama_gemolog == $yi['gemolog']){
							  ?>
							  <option value="<?php echo $gemo->nama_gemolog; ?>" selected><?php echo $gemo->nama_gemolog ?></option>
							  <?php
							}else{
							  ?>
							  <option value="<?php echo $gemo->nama_gemolog; ?>"><?php echo $gemo->nama_gemolog ?></option>
							  <?php
							}
						  }
						  ?>
						</select>
                    </div>
                  </div>
            </div>
          </div>
          <div class="col-md-12">
          <div class="row">
            <div class="form-group">
              <div class="col-md-12">
                  <h5><b>PENAKSIRAN - PERHIASAN</b></h5>
                  <hr style="border:1px solid black;margin-top:-5px;">
              </div>
			  <div class="col-md-12">
				<div class="row">
				  <div class="col-md-2 col-sm-6">
                    <div class="form-group has-success">
                      <label>Jenis Perhiasan <font color="red">*</font></label>
                      <select name="jenis_perhiasan" class="form-control" required>
                        <option value="" selected disabled>--select--</option>
                        <?php
                        $getPerhiasan = $this->db->query("SELECT * FROM master_jenis_permata");
                        foreach ($getPerhiasan->result() as $key) {
                          if($key->id == $get['jenis_perhiasan']){
                            ?>
                            <option value="<?php echo $key->id ?>" selected><?php echo $key->jenis ?></option>
                            <?php
                          }else{
                            ?>
                            <option value="<?php echo $key->id ?>"><?php echo $key->jenis ?></option>
                            <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-4">
                    <div class="form-group has-success">
                      <label>Jumlah Perhiasan <font color="red">*</font></label>
                      <input type="text" name="jumlah_perhiasan" placeholder="Jumlah Perhiasan" class="form-control" onKeyUp="this.value=ThausandSeperator(this.value,2);" value="<?php echo $get['jumlah_perhiasan'] ?>" required>
                    </div>
                  </div>
				  <div class="col-md-6">
					<div class="form-group has-success">
					  <label>Keterangan Tambahan</label>
					  <textarea name="keterangan_tambahan" id="keterangan_tambahan" class="form-control"><?php echo $get['keterangan'] ?></textarea>
					</div>
				  </div>
				</div>
			  </div>
            </div>
          </div>
            <div class="row">
			  <div class="col-md-12">
                  <h5><b>PENAKSIRAN - PERMATA</b></h5>
                  <hr style="border:1px solid black;margin-top:-5px;">
              </div>
              <div class="col-md-12">
                <div class="row">
                  <div class="col-md-2 col-sm-6">
                    <div class="form-group has-success">
                      <label>Jenis Logam</label>
                      <select name="jenis_logam" id="jenis_logam" class="form-control" onchange="setDisable()">
                        <option value="" selected disabled>--select--</option>
                        <?php
                        $getLogam = $this->db->query("SELECT * FROM master_logam");
                        foreach ($getLogam->result() as $key) {
                          if($key->id == $get['jenis_logam']){
                            ?>
                            <option value="<?php echo $key->id ?>" selected><?php echo $key->jenis_logam ?></option>
                            <?php
                          }else{
                            ?>
                            <option value="<?php echo $key->id ?>"><?php echo $key->jenis_logam ?></option>
                            <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-4">
                    <div class="form-group has-success">
                      <label>Berat Kotor</label>
                      <div class="input-group"><input type="text" name="berat_kotor" id="berat_kotor" placeholder="Berat Kotor" class="form-control" value="<?php echo $get['berat_kotor'] ?>"><span class="input-group-addon">gram</span></div>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-4">
                    <div class="form-group has-success">
                      <label>Berat Bersih</label>
                      <div class="input-group"><input type="text" name="berat_bersih" id="berat_bersih" placeholder="Berat Bersih" class="form-control" value="<?php echo $get['berat_bersih'] ?>"><span class="input-group-addon">gram</span></div>
                    </div>
                  </div>
				  <div class="col-md-2 col-sm-4">
                    <div class="form-group has-success">
                      <label>Karatase Emas</label>
                      <select name="karatase" id="karatase" class="form-control">
                        <option value="" selected disabled="">--select--</option>
                        <?php
                        $getKarat = $this->db->query("SELECT * FROM master_karatase");
                        foreach ($getKarat->result() as $key) {
                          if($key->karatase == $get['karatase_emas']){
                            ?>
                            <option value="<?php echo $key->karatase ?>" selected><?php echo $key->karatase ?></option>
                            <?php
                          }else{
                            ?>
                            <option value="<?php echo $key->karatase ?>"><?php echo $key->karatase ?></option>
                            <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
				</div>
				<div class="row"> 
                  <div class="col-md-2 col-sm-4">
                    <div class="form-group has-success">
                      <label>Jenis Permata</label>
                      <select name="jenis_permata" class="form-control">
                        <option value="" selected disabled>--select--</option>
                        <?php
                        $getPermata = $this->db->query("SELECT * FROM master_permata");
                        foreach ($getPermata->result() as $key) {
                          if($key->id == $get['jenis_permata']){
                            ?>
                            <option value="<?php echo $key->id ?>" selected><?php echo $key->permata ?></option>
                            <?php
                          }else{
                            ?>
                            <option value="<?php echo $key->id ?>"><?php echo $key->permata ?></option>
                            <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-4">
                    <div class="form-group has-success">
                      <label>Jumlah Permata</label>
                      <input type="text" name="jumlah_permata" class="form-control" placeholder="Jumlah Permata" onKeyUp="this.value=ThausandSeperator(this.value,2);" value="<?php echo $get['jumlah_permata'] ?>">
                    </div>
                  </div> 
                  <div class="col-md-2 col-sm-4">
                    <div class="form-group has-success">
                      <label>Berat Total Berlian</label>
                      <div class="input-group"><input type="text" class="form-control" name="berat_total" id="berat_total" placeholder="Berat Total Berlian" value="<?php echo $get['berat_total_permata'] ?>"><span class="input-group-addon">carat</span></div>
                    </div>
                  </div>
				  <div class="col-md-6 col-sm-4">
					<div class="form-group has-success">
					  <label>4C Berlian</label>
					  <textarea name="keterangan_berlian" id="keterangan_berlian" class="form-control"><?php echo $get['keterangan_berlian'] ?></textarea>
					</div>
				  </div>
				  <div class="col-md-12">
					<div class="form-group">
						<div class="col-md-12">
							<button class="btn btn-primary pull-right" style="margin-bottom:20px;margin-right:10px;"><i class="fa fa-save"></i> Save </button>
							<button type="reset" class="btn btn-default pull-right" style="margin-bottom:20px;margin-right:10px;"> <i class="fa fa-refresh"></i> Clear</button>
							<a href="" class="btn btn-danger pull-right" style="margin-bottom:20px;margin-right:10px;"><i class="fa fa-chevron-left"></i> Back</a>
						</div>
					</div>
				  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
              <!-- END -->
              </form>
          <?php
          $xf = $id;
          ?>
<script>
	$(document).ready(function(){
		$.mask.definitions['d'] = '[0-9.]';
		$("#berat_kotor").mask("9?ddd.dd",{placeholder:"____.00"});
		$("#berat_bersih").mask("9?ddd.dd",{placeholder:"____.00"});
		$("#berat_total").mask("9?dd.dd",{placeholder:"___.00"});
		
		setDisable();
		
			$('#form-examination-taksiran').trigger("reset");
			$("#form-examination-taksiran").on('submit',function(e){
			e.preventDefault();
			var formatData = new FormData($(this)[0]);
				swal({
				 title: "",
				 text: "Apakah Anda akan menyimpan data?",
				 type: "warning",
				 showCancelButton: true,
				 confirmButtonColor: "#DD6B55",
				 confirmButtonText: "Ya",
				 cancelButtonText: "Tidak",
				 closeOnConfirm: false }, function(){
					$.ajax({
						type : 'POST',
						url : '<?php echo base_url() ?>object/update_taksiran/<?php echo $id; ?>/<?php echo $id_order; ?>',
						data : formatData,
						async: false,
						cache: false,
						contentType: false,
						processData: false,
						success:function(html){
							var data = eval ("(" + html + ")");
							if(data.success){
								swal({
								 title: "Berhasil Menyimpan data!",
								 text: "Klik tombol di bawah.",
								 type: "success",
								 showCancelButton: false,
								 confirmButtonColor: "#257DB6",
								 confirmButtonText: "Ok!",
								 closeOnConfirm: false
								},
								function(){
									window.location.reload();
								});
							}else{
								swal({
								 title: data.msg,
								 text: "Klik tombol di bawah.",
								 type: "warning",
								 showCancelButton: false,
								 confirmButtonColor: "#257DB6",
								 confirmButtonText: "Ok!",
								 closeOnConfirm: false
								});
							}
						}
					});
				});
			});
	});
	
	function setDisable(){
        if($("#jenis_logam").val()=="1") {
            $("#karatase").removeAttr("disabled");
        } else {
            $("#karatase").attr("disabled","disabled");
        }
    }

  function showSweets(){
	var harga_taksiran = $("#harga_taksiran").val();
	if(harga_taksiran==""){
		return false;
	}else if(harga_taksiran=="0"){
		return false;
	}else{
    swal({
     title: "",
     text: "Apakah Anda akan menyimpan data?",
     type: "warning",
     showCancelButton: true,
     confirmButtonColor: "#DD6B55",
     confirmButtonText: "Ya",
     cancelButtonText: "Tidak",
     closeOnConfirm: false }, function(){
      document.getElementById("form-taksiran").submit();
    });
	}
  }
</script>