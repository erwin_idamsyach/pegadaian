      <script type="text/javascript">
      var id = "<?php echo $id; ?>";
        jQuery(function($){
		   $.mask.definitions['d'] = '[0-9.]';
		   $("#obj_weight, #obj_length, #obj_width, #obj_height").mask("9?ddd.dd",{placeholder:"____.00"});
           $("#hardness").mask("9.9",{placeholder:"_._"});
           $("#sg_end").mask("9.99",{placeholder:"_.__"});
           $("#ri_start").mask("9.999",{placeholder:"_.___"});
		   
		    $('#form-examination-sertifikasi').trigger("reset");
			$("#form-examination-sertifikasi").on('submit',function(e){
			e.preventDefault();
			var formatData = new FormData($(this)[0]);
				swal({
				 title: "",
				 text: "Apakah Anda akan menyimpan data?",
				 type: "warning",
				 showCancelButton: true,
				 confirmButtonColor: "#DD6B55",
				 confirmButtonText: "Ya",
				 cancelButtonText: "Tidak",
				 closeOnConfirm: false }, function(){
					$.ajax({
						type : 'POST',
						url : '<?php echo base_url() ?>object/update_obj/<?php echo $id?>',
						data : formatData,
						async: false,
						cache: false,
						contentType: false,
						processData: false,
						success:function(html){
							var data = eval ("(" + html + ")");
							if(data.success){
								swal({
								 title: "Berhasil Menyimpan data!",
								 text: "Klik tombol di bawah.",
								 type: "success",
								 showCancelButton: false,
								 confirmButtonColor: "#257DB6",
								 confirmButtonText: "Ok!",
								 closeOnConfirm: false
								},
								function(){
									window.location.reload();
								});
							}else{
								swal({
								 title: data.msg,
								 text: "Klik tombol di bawah.",
								 type: "warning",
								 showCancelButton: false,
								 confirmButtonColor: "#257DB6",
								 confirmButtonText: "Ok!",
								 closeOnConfirm: false
								});
							}
						}
					});
				});
			});
        });
        $(".drop").dropify();
      </script>
<?php
$ox = mysql_query("SELECT * FROM tb_lab_desk where id_object='$id'");
$yi = mysql_fetch_array($ox);
$o = mysql_query("SELECT * FROM tb_front_desk where id_object='$id'");
$m = mysql_fetch_array($o);
?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <div class="modal fade" id="myModalxv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
          <div class="modal-dialog">
            <div class="loginmodal-container">
              <center><label style="font-size:20px;">Specific Gravity</label></center><br>
                <div class="col-md-12">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Berat Kering</label>
                        <input type="text" class="form-control" id="bk" name="bk" value="<?php echo $m['obj_weight']; ?>" placeholder="Berat Kering ...">
                      </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Berat Basah</label>
                          <input type="text" class="form-control" id="bb" name="bb" value="" placeholder="Berat Basah ...">
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <input type="submit" name="login" class="login loginmodal-submit" value="Hitung" onclick="hitung()">
                        </div>
                      </div>
                      <div id="hasil_hitung">
                        
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        <!-- Main content -->
  <section class="content">

    <div class="box box-warning">
      <div class="box-body">
        <div class="col-md-12">
          <div class="row">
            <div class="form-group">
              <div class="col-md-12 text-right">            
                <h4><b>F01</b></h4>
              </div>
            </div>
          </div>
        </div>
      <form id="form-examination-sertifikasi" method="post" enctype="multipart/form-data">
        <div class="col-md-12">
          <h5><b>DATA GEMOLOG</b></h5>
          <hr style="border:1px solid black;margin-top:-5px;">
          <div class="row">
            <div class="col-md-4 col-sm-6">
              <div class="form-group has-success">
                <label>Gemologist Name <font color="red">*</font></label>
                <select name="gemolog" class="form-control" required>
                  <?php
                  $getGemolog = $this->db->query("SELECT nama nama_gemolog FROM tb_login WHERE store='".sessionValue('kode_store')."' AND access_level='Gemologist' AND delete_by='' ");
                  foreach ($getGemolog->result() as $gemo) {
                    if($gemo->nama_gemolog == $yi['gemolog']){
                      ?>
                      <option value="<?php echo $gemo->nama_gemolog; ?>" selected><?php echo $gemo->nama_gemolog ?></option>
                      <?php
                    }else{
                      ?>
                      <option value="<?php echo $gemo->nama_gemolog; ?>"><?php echo $gemo->nama_gemolog ?></option>
                      <?php
                    }
                  }
                  ?>
                </select>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="row">
            <div class="form-group">
              <div class="col-md-12">
                <div class="pull-right">
                  <a onclick="front()" id="minfro"><span class="fa fa-minus fa-2x"></span></a> &nbsp;
                  <a onclick="frontx()" id="plufro"><span class="fa fa-plus fa-2x"></span></a>
                </div>
                  <h5><b>MEASUREMENTS</b></h5>
                  <hr style="border:1px solid black;margin-top:-5px;">
              </div>
            </div>
          </div>

          <div id="frontc">
            <div class="col-md-12">
              <div class="row">
                <div class="col-md-6" style="border:2px solid #f39c12;padding:10px;border-radius:10px;">
                  <div class="row">

                    <div class="col-lg-5 col-md-5 col-xs-12">
                      <div class="form-group has-success">
                      <?php
                      if($yi['obj_weight'] < 10){
                        $wei = "000".$yi['obj_weight'];
                      }else if($yi['obj_weight'] < 100){
                        $wei = "00".$yi['obj_weight'];
                      }else if($yi['obj_weight'] < 1000){
                        $wei = "0".$yi['obj_weight'];
                      }else{
                        $wei = $yi['obj_weight'];
                      }
                      ?>
                        <label class="control-label" for="inputSuccess">Weight <font color="red">*</font></label>
                        <input type="text" name="weight" id="obj_weight" class="form-control" placeholder="Weight" value="<?php echo $wei ?>" required>
                      </div>
                    </div>

                    <div class="col-lg-5 col-md-5 col-xs-12">
                      <div class="form-group has-success">
                      <?php
                      if($yi['obj_length'] < 10){
                        $len = "000".$yi['obj_length'];
                      }else if($yi['obj_length'] < 100){
                        $len = "00".$yi['obj_length'];
                      }else if($yi['obj_length'] < 1000){
                        $len = "0".$yi['obj_length'];
                      }else{
                        $len = $yi['obj_length'];
                      }
                      ?>
                        <label class="control-label" for="inputSuccess">Length <font color="red">*</font></label>
                        <input type="text" class="form-control" name="length" id="obj_length" placeholder="Length" value="<?php echo $len ?>" required>
                      </div>
                    </div>

                    <div class="col-lg-5 col-md-5 col-xs-12">
                      <div class="form-group has-success">
                      <?php
                      if($yi['obj_width'] < 10){
                        $wid = "000".$yi['obj_width'];
                      }else if($yi['obj_width'] < 100){
                        $wid = "00".$yi['obj_width'];
                      }else if($yi['obj_width'] < 1000){
                        $wid = "0".$yi['obj_width'];
                      }else{
                        $wid = $yi['obj_width'];
                      }
                      ?>
                        <label class="control-label" for="inputSuccess"> Width <font color="red">*</font></label>
                        <input type="text" class="form-control" name="width" id="obj_width" placeholder="Width" value="<?php echo $wid ?>" required>
                      </div>
                    </div>

                    <div class="col-lg-5 col-md-5 col-xs-12">
                      <div class="form-group has-success">
                      <?php
                      if($yi['obj_height'] < 10){
                        $hei = "000".$yi['obj_height'];
                      }else if($yi['obj_height'] < 100){
                        $hei = "00".$yi['obj_height'];
                      }else if($yi['obj_height'] < 1000){
                        $hei = "0".$yi['obj_height'];
                      }else{
                        $hei = $yi['obj_height'];
                      }
                      ?>
                        <label class="control-label" for="inputSuccess"> Height <font color="red">*</font></label>
                        <input type="text" class="form-control" name="height" id="obj_height" placeholder="Height" value="<?php echo $hei ?>" required>
                      </div>
                    </div>
                  </div>
                </div>
                  <div class="col-md-3">
                    <div class="form-group" style="border: 1px solid black;border-radius:10px;">
                        <center>
                          <input type="file" class="form-control drop" name="image" data-default-file="<?php echo base_url()."asset/images/".$yi['obj_image'] ?>" data-height="153px">
                        </center>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <?php
                      $q = mysql_query("SELECT * FROM tb_lab_desk a
										LEFT JOIN color_stone b ON b.code=a.obj_color 
										WHERE a.id_object='$id'");
                      $xf = mysql_fetch_array($q);
                    ?>
                    <div class="form-group" style="border:2px solid black;padding:10px;border-radius:10px;">
                      <div id="div_color" style="background-color:#<?php echo $xf['rgb_code']?>;height:148px;padding-top:50px;border-radius:10px;">
                      <center>
                        <div style="mix-blend-mode: difference;"  id="div_color_text">
                          <?php $color = explode(";", $xf['color']); echo $color[0] ?>
                        </div>
                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modalCol">EDIT</button>
                      </center>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
                    <!-- input states -->

      <div class="col-md-12">
        <div class="row">
          <div class="form-group">
            <div class="col-md-12">
              <div class="pull-right">
                <a onclick="phisi()" id="minphy"><span class="fa fa-minus fa-2x"></span></a> &nbsp;
                <a onclick="phisix()" id="pluphy"><span class="fa fa-plus fa-2x"></span></a>
              </div>
                <h5><b>PHYSICAL EXAMINATION</b></h5>
                <hr style="border:1px solid black;margin-top:-5px;">
            </div>
          </div>
        </div>

          <div id="physy">
            <div class="col-md-12" style="width:100%;">
              <div class="row">
                <div class="col-md-12">
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group has-success">
                        <label>Shape <font color="red">*</font></label>
                        <select class="form-control select2" style="width:100%;" id="transparent" name="shape" required>
                          <?php
                          $shape = $yi['obj_shape'];
                          if ($shape == "") {
                            ?>
                              <option value="">---select---</option>
                            <?php
                          }else{
                          $zx = mysql_query("SELECT obj_shape FROM tb_lab_desk where obj_shape='$shape'");
                          $yx = mysql_fetch_array($zx);
                          ?>
                          <option value="<?php echo $yx['obj_shape']?>"><?php echo $yx['obj_shape']?></option>
                          <?php
                          }
                          $xz = mysql_query("SELECT * FROM shape where shape!='$shape' order by shape");
                          while ($xy = mysql_fetch_array($xz)){
                            ?>
                            <option value="<?php echo $xy['shape']?>"><?php echo $xy['shape']?></option>
                            <?php
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group has-success">
                        <label>Cutting Style <font color="red">*</font></label>
                        <select class="form-control select2" style="width:100%;" id="transparent" name="cut" required>
                          <?php
                          $cut = $yi['obj_cut'];
                          if ($cut == "") {
                            ?>
                              <option value="">---select---</option>
                            <?php
                          }else{
                          $zx = mysql_query("SELECT obj_cut FROM tb_lab_desk where obj_cut='$cut'");
                          $yx = mysql_fetch_array($zx);
                          ?>
                          <option value="<?php echo $yx['obj_cut']?>"><?php echo $yx['obj_cut']?></option>
                          <?php
                          }
                          $xz = mysql_query("SELECT * FROM cut where cut!='$cut' order by cut");
                          while ($xy = mysql_fetch_array($xz)){
                            ?>
                            <option value="<?php echo $xy['cut']?>"><?php echo $xy['cut']?></option>
                            <?php
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                <div class="col-md-4">
                  <div class="row">
                    <div class="col-md-6 col-xs-12 col-sm-12">
                      <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess"> Hardness </label>
                        <input type="text" id="hardness" class="form-control" name="hardness" onchange="final_advice()" placeholder="Enter..." value="<?php echo $yi['hardness']?>">
                      </div>
                    </div>
                    <div class="col-md-6 col-xs-12">
                      <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess"> SG <font color="red">*</font></label>
                        <input type="text" step="0.01" id="sg_end" class="sg_end form-control" name="gravity" maxlength="4" onchange="final_advice()" placeholder="Enter..." value="<?php echo $yi['sg']?>" required>
                      </div>
                    </div>
                  </div>
                </div>
                    <div class="col-md-4">
                      <div class="form-group has-success has-feedback">
                      <label class="control-label" for="inputSuccess"> Cleavage</label>
                      <input type="text" class="form-control" id="id" name="cleavage" value="<?php echo $yi['cleavage']?>" placeholder="Enter ...">
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group has-success">
                        <label>Fracture</label>
                        <select class="form-control select2" style="width:100%;" id="transparent" name="fracture">
							<option value="">---select---</option>
                          <?php
                          $fracture = $yi['fracture'];
                          $xz = mysql_query("SELECT * FROM fracture");
                          while ($xy = mysql_fetch_array($xz)) {
							  if($fracture == $xy['fracture']){
								  $selected_facture = "selected";
							  }else{
								  $selected_facture = "";
							  }
                            ?>
                          <option value="<?php echo $xy['fracture']?>" <?php echo $selected_facture; ?> ><?php echo $xy['fracture']?></option>
                            <?php
                          }
                          ?>
                        </select>
                      </div>
                    </div>

                    <div class="col-md-4">
                      <div class="form-group has-success has-feedback">
                        <label class="control-label" for="inputSuccess"> Luminescence</label>
                        <select class="form-control select2" style="width:100%;" id="transparent" name="luminescence">
                          <?php
                            $luminescence = $yi['luminescence'];
                            if ($luminescence == "") {
                          ?>
                            <option value="">---select---</option>
                          <?php
                          }else{
                            $zx = mysql_query("SELECT luminescence FROM luminescence where luminescence='$luminescence'");
                            $yx = mysql_fetch_array($zx);
                          ?>
                            <option value="<?php echo $yx['luminescence']?>"><?php echo $yx['luminescence']?></option>
                          <?php
                          }
                            $xz = mysql_query("SELECT * FROM luminescence where luminescence!='$luminescence' order by luminescence");
                            while ($xy = mysql_fetch_array($xz)){
                          ?>
                            <option value="<?php echo $xy['luminescence']?>"><?php echo $xy['luminescence']?></option>
                          <?php
                            }
                          ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group has-success has-feedback">
                        <label class="control-label" for="inputSuccess"> Tenacity</label>
                        <select class="form-control select2" style="width:100%;" id="transparent" name="tenacity">
                          <?php
                            $tenacity = $yi['tenacity'];
                            if ($tenacity == "") {
                          ?>
                            <option value="">---select---</option>
                          <?php
                            }else{
                            $zx = mysql_query("SELECT tenacity FROM tb_lab_desk where tenacity='$tenacity'");
                            $yx = mysql_fetch_array($zx);
                          ?>
                            <option value="<?php echo $yx['tenacity']?>"><?php echo $yx['tenacity']?></option>
                          <?php
                            }
                            $xz = mysql_query("SELECT * FROM tenacity where tenacity!='$tenacity' order by tenacity");
                            while ($xy = mysql_fetch_array($xz)){
                          ?>
                            <option value="<?php echo $xy['tenacity']?>"><?php echo $xy['tenacity']?></option>
                          <?php
                            }
                          ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group has-success">
                        <label>Radioactivity</label>
                        <select class="form-control select2" style="width:100%;" id="radio" name="radio">
                          <?php
                          $radioactivity = $yi['radioactivity'];
                          if ($radioactivity == "") {
                            ?>
                              <option value="">---select---</option>
                            <?php
                          }else{
                          $zx = mysql_query("SELECT radioactivity FROM tb_lab_desk where radioactivity='$radioactivity'");
                          $yx = mysql_fetch_array($zx);
                          ?>
                          <option value="<?php echo $yx['radioactivity']?>"><?php echo $yx['radioactivity']?></option>
                          <?php
                          }
                          $xz = mysql_query("SELECT * FROM radioactivity where radioactive!='$radioactivity' order by radioactive");
                          while ($xy = mysql_fetch_array($xz)){
                            ?>
                            <option value="<?php echo $xy['radioactive']?>"><?php echo $xy['radioactive']?></option>
                            <?php
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                      

                      <div class="col-md-4">
                        <div class="form-group has-success has-feedback">
                          <label>Crystal Gems</label>
                          <select class="form-control select2" style="width:100%;" id="crystal_gems" name="crystal_gems">
                            <?php
                            $crystal_gems = $yi['crystal_gems'];
                            if ($member == "") {
                              ?>
                                <option value="">---select---</option>
                              <?php
                            }else{
                            $zx = mysql_query("SELECT crystal_gems FROM tb_lab_desk where crystal_gems='$crystal_gems'");
                            $yx = mysql_fetch_array($zx);
                            ?>
                            <option value="<?php echo $yx['crystal_gems']?>"><?php echo $yx['crystal_gems']?></option>
                            <?php
                            }
                            $zx = mysql_query("SELECT * FROM crystal where crystal_gems!='$crystal_gems' order by crystal_gems");
                            while($yx = mysql_fetch_array($zx)){
                            ?>
                            <option value="<?php echo $yx['crystal_gems']?>"><?php echo $yx['crystal_gems']?></option>
                            <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

      <div class="col-md-12">
        <div class="row">
          <div class="form-group">
            <div class="col-md-12">
              <div class="pull-right">
                <a onclick="optic()" id="minopt"><span class="fa fa-minus fa-2x"></span></a> &nbsp;
                <a onclick="opticx()" id="pluopt"><span class="fa fa-plus fa-2x"></span></a>
              </div>
                <h5><b>OPTICAL EXAMINATION</b></h5>
                <hr style="border:1px solid black;margin-top:-5px;">
            </div>
          </div>
        </div>

        <div id="optical">
          <div class="row">
            <div class="col-md-12">

              <div class="col-md-3">
                <div class="form-group has-success">
                  <label>Transparency <font color="red">*</font></label>
                  <select class="form-control select2" style="width:100%;" id="transparent" name="transparent" required>
                    <?php
                    $transparancy = $yi['transparancy'];
                    if ($transparancy == "") {
                      ?>
                        <option value="">---select---</option>
                      <?php
                    }else{
                    $zx = mysql_query("SELECT transparancy FROM tb_lab_desk where transparancy='$transparancy'");
                    $yx = mysql_fetch_array($zx);
                    ?>
                    <option value="<?php echo $yx['transparancy']?>"><?php echo $yx['transparancy']?></option>
                    <?php
                    }
                    $xz = mysql_query("SELECT * FROM transparent where transparent!='$transparancy' order by transparent");
                    while ($xy = mysql_fetch_array($xz)){
                      ?>
                      <option value="<?php echo $xy['transparent']?>"><?php echo $xy['transparent']?></option>
                      <?php
                    }
                    ?>
                  </select>
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group has-success">
                  <label class="control-label" for="inputSuccess"> RI <font color="red">*</font></label>
                  <input type="text" step="0.001" max="9.999" id="ri_start" class="form-control" name="ri_start" onchange="final_advice()" placeholder="Enter..." value="<?php echo $yi['ri_start']?>" required>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group has-success">
                  <label>UV LW/SW</label>
                  <input type="text" class="form-control" name="uv" placeholder="UV LW/SW">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group has-success">
                  <label>Chelsea Filter</label>
                  <input type="text" class="form-control" name="chelsea_filter" placeholder="Chelsea Filter">
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group has-success">
                  <label>Spectroscope</label>
                  <input type="text" class="form-control" name="spectroscope" placeholder="Spectroscope">
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group has-success">
                  <label>Luster</label>
                  <select class="form-control select2" style="width:100%;" id="transparent" name="luster">
                    <?php
                    $luster = $yi['luster'];
                    if ($luster == "") {
                      ?>
                        <option value="">---select---</option>
                      <?php
                    }else{
                    $zx = mysql_query("SELECT luster FROM tb_lab_desk where luster='$luster'");
                    $yx = mysql_fetch_array($zx);
                    ?>
                    <option value="<?php echo $yx['luster']?>"><?php echo $yx['luster']?></option>
                    <?php
                    }
                    $xz = mysql_query("SELECT * FROM Luster where luster!='$luster'");
                    while ($xy = mysql_fetch_array($xz)){
                      ?>
                      <option value="<?php echo $xy['luster']?>"><?php echo $xy['luster']?></option>
                      <?php
                    }
                    ?>
                  </select>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group has-success has-feedback">
                <label class="control-label" for="inputSuccess"> Pleochroism</label>
                <input type="text" class="form-control" id="id" name="pleochroism" value="<?php echo $yi['pleochroism']?>" placeholder="Enter ...">
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group has-success">
                  <label class="control-label" for="inputSuccess"> Birefringence</label>
                  <input type="number" step="0.0001" class="form-control" name="birefringence" placeholder="Enter..." value="<?php echo $yi['birefringence']?>">
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group has-success">
                  <label class="control-label" for="inputSuccess"> Dispersion</label>
                  <input type="number" step="0.001" class="form-control" name="dispersion" placeholder="Enter..." value="<?php echo $yi['dispersion']?>">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>


      <div class="col-md-12">
        <div class="row">
          <div class="form-group">
            <div class="col-md-12">
                <div class="pull-right">
                  <a onclick="classc()" id="mincla"><span class="fa fa-minus fa-2x"></span></a> &nbsp;
                  <a onclick="classx()" id="plucla"><span class="fa fa-plus fa-2x"></span></a>
                </div>
                <h5><b>CLASSIFICATION EXAMINATION</b></h5>
                <hr style="border:1px solid black;margin-top:-5px;">
            </div>
          </div>
        </div>

        <div id="classification">
          <div class="row">
            <div class="col-md-12">
              <div class="col-md-3">
                <div class="form-group has-success">
                  <label>Mineral Classification</label>
                  <select class="form-control select2" style="width:100%;" id="transparent" name="mineral">
                    <?php
                    $mineral = $yi['mineral'];
                    if ($mineral == "") {
                      ?>
                        <option value="">---select---</option>
                      <?php
                    }else{
                    $zx = mysql_query("SELECT mineral FROM tb_lab_desk where mineral='$mineral'");
                    $yx = mysql_fetch_array($zx);
                    ?>
                    <option value="<?php echo $yx['mineral']?>"><?php echo $yx['mineral']?></option>
                    <?php
                    }
                    $xz = mysql_query("SELECT * FROM mineral_class where mineral!='$mineral' order by mineral");
                    while ($xy = mysql_fetch_array($xz)){
                      ?>
                      <option value="<?php echo $xy['mineral']?>"><?php echo $xy['mineral']?></option>
                      <?php
                    }
                    ?>
                </select>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group has-success has-feedback">
                <label class="control-label" for="inputSuccess"> Strunz Mineral Classification</label>
                <select class="form-control select2" style="width:100%;" id="transparent" name="strunz">
                    <?php
                    $strunz = $yi['strunz'];
                    if ($strunz == "") {
                      ?>
                        <option value="">---select---</option>
                      <?php
                    }else{
                    $zx = mysql_query("SELECT strunz FROM tb_lab_desk where strunz='$strunz'");
                    $yx = mysql_fetch_array($zx);
                    ?>
                    <option value="<?php echo $yx['strunz']?>"><?php echo $yx['strunz']?></option>
                    <?php
                    }
                    $xz = mysql_query("SELECT * FROM strunz where strunz!='$strunz' order by strunz");
                    while ($xy = mysql_fetch_array($xz)){
                      ?>
                      <option value="<?php echo $xy['strunz']?>"><?php echo $xy['strunz']?></option>
                      <?php
                    }
                    ?>
                </select>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group has-success has-feedback">
              <label class="control-label" for="inputSuccess"> Related To</label>
              <input type="text" class="form-control" id="id" name="related" value="<?php echo $yi['related']?>" placeholder="Enter ...">
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group has-success has-feedback">
              <label class="control-label" for="inputSuccess"> Member Of Group</label>
              <input type="text" class="form-control" id="id" name="member" value="<?php echo $yi['member']?>" placeholder="Enter ...">
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group has-success has-feedback">
                <label class="control-label" for="inputSuccess"> Synonyms</label>
                <input type="text" class="form-control" id="id" name="synonyms" value="<?php echo $yi['synonyms']?>" placeholder="Enter ...">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
      <div class="col-md-12">
        <div class="row">
          <div class="form-group">
            <div class="col-md-12">
                <div class="pull-right">
                  <a onclick="cryst()" id="mincry"><span class="fa fa-minus fa-2x"></span></a> &nbsp;
                  <a onclick="crystx()" id="plucry"><span class="fa fa-plus fa-2x"></span></a>
                </div>
                  <h5><b>CRYSTAL DATA EXAMINATION</b></h5>
                  <hr style="border:1px solid black;margin-top:-5px;">
              </div>
            </div>
          </div>
          <div id="crystal">
            <div class="row">
              <div class="col-md-12">
                <div class="col-md-3">
                  <div class="form-group has-success">
                    <label>Crystallography</label>
                    <select class="form-control select2" style="width:100%;" id="transparent" name="crystallography">
                      <?php
                      $crystallography = $yi['crystallography'];
                      if ($crystallography == "") {
                        ?>
                          <option value="">---select---</option>
                        <?php
                      }else{
                      $zx = mysql_query("SELECT crystallography FROM tb_lab_desk where crystallography='$crystallography'");
                      $yx = mysql_fetch_array($zx);
                      ?>
                      <option value="<?php echo $yx['crystallography']?>"><?php echo $yx['crystallography']?></option>
                      <?php
                      }
                      $xz = mysql_query("SELECT * FROM crystallography where crystal!='$crystallography' order by crystal");
                      while ($xy = mysql_fetch_array($xz)){
                        ?>
                        <option value="<?php echo $xy['crystal']?>"><?php echo $xy['crystal']?></option>
                        <?php
                      }
                      ?>
                    </select>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group has-success has-feedback">
                    <label class="control-label" for="inputSuccess"> Crystal Habit</label>
                    <input type="text" class="form-control" id="id" name="crystal" value="<?php echo $yi['crystal_habit']?>" placeholder="Enter ...">
                    </div>
                  </div>
                  <div class="col-md-3">
                      <div class="form-group has-success has-feedback">
                        <label class="control-label" for="inputSuccess"> Twinning</label>
                        <select class="form-control select2" style="width:100%;" id="transparent" name="twinning">
                          <?php
                            $twinning = $yi['twinning'];
                            if ($twinning == "") {
                              ?>
                                <option value="">---select---</option>
                              <?php
                            }else{
                            $zx = mysql_query("SELECT twinning FROM tb_lab_desk where twinning='$twinning'");
                            $yx = mysql_fetch_array($zx);
                            ?>
                            <option value="<?php echo $yx['twinning']?>"><?php echo $yx['twinning']?></option>
                            <?php
                            }
                            $xz = mysql_query("SELECT * FROM twinning where twinning!='$twinning' order by twinning");
                            while ($xy = mysql_fetch_array($xz)){
                              ?>
                              <option value="<?php echo $xy['twinning']?>"><?php echo $xy['twinning']?></option>
                              <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>


               <div class="col-md-12">
                  <div class="row">
                    <div class="form-group">
                      <div class="col-md-12">
                        <div class="pull-right">
                          <a onclick="occur()" id="minocc"><span class="fa fa-minus fa-2x"></span></a> &nbsp;
                          <a onclick="occurx()" id="pluocc"><span class="fa fa-plus fa-2x"></span></a>
                        </div>
                          <h5><b>OCCURANCES EXAMINATION</b></h5>
                          <hr style="border:1px solid black;margin-top:-5px;">
                      </div>
                    </div>
                  </div>
                    <div id="occuran">
                     <div class="row">
                      <div class="col-md-12">
                        <div class="col-md-3">
                          <div class="form-group has-success has-feedback">
                            <label class="control-label" for="inputSuccess"> Type Locality</label>
                            <select class="form-control select2" style="width:100%;" id="transparent" name="type">
                              <?php
                                $type_local = $yi['type_local'];
                                if ($type_local == "") {
                              ?>
                                <option value="">---select---</option>
                            <?php
                                }else{
                                $zx = mysql_query("SELECT type_local FROM tb_lab_desk where type_local='$type_local'");
                                $yx = mysql_fetch_array($zx);
                                ?>
                                <option value="<?php echo $yx['type_local']?>"><?php echo $yx['type_local']?></option>
                            <?php
                              }
                                $xz = mysql_query("SELECT * FROM origin where origin!='$type_local' order by origin");
                                while ($xy = mysql_fetch_array($xz)){
                            ?>
                                <option value="<?php echo $xy['origin']?>"><?php echo $xy['origin']?></option>
                            <?php
                              }
                            ?>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group has-success has-feedback">
                        <label class="control-label" for="inputSuccess"> Geological Setting</label>
                        <input type="text" class="form-control" id="id" name="geological" value="<?php echo $yi['geological_setting']?>" placeholder="Enter ...">
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group has-success has-feedback">
                        <label class="control-label" for="inputSuccess"> Common Associations</label>
                        <input type="text" class="form-control" id="id" name="common_asso" value="<?php echo $yi['common_asso']?>" placeholder="Enter ...">
                        </div>
                      </div>
                        <div class="col-md-3">
                          <div class="form-group has-success has-feedback">
                          <label class="control-label" for="inputSuccess"> Common Impurities</label>
                          <input type="text" class="form-control" id="id" name="common_impu" value="<?php echo $yi['common_impu']?>" placeholder="Enter ...">
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group has-success has-feedback">
                            <label class="control-label" for="inputSuccess"> Year Discovered</label>
                            <select class="form-control select2" style="width:100%;" id="transparent" name="year">
                              <?php
                                $year = $yi['year_discovered'];
                                if ($year == "") {
                              ?>
                                <option value="">---select---</option>
                              <?php
                                }else{
                                  $zx = mysql_query("SELECT year_discovered FROM tb_lab_desk where year_discovered='$year'");
                                  $yx = mysql_fetch_array($zx);
                              ?>
                                <option value="<?php echo $yx['year_discovered']?>"><?php echo $yx['year_discovered']?></option>
                              <?php
                                }
                                  $xz = mysql_query("SELECT * FROM year_discovered where year!='$year' order by id desc");
                                  while ($xy = mysql_fetch_array($xz)){
                              ?>
                                <option value="<?php echo $xy['year']?>"><?php echo $xy['year']?></option>
                              <?php
                                }
                              ?>
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <!-- FINAL DECISION -->
                <div class="form-group">
                  <div class="col-md-12" style="margin-bottom:0px;">
                    <h5><b>FINAL DECISION</b></h5>
                    <hr style="border:1px solid black;margin-top:-5px;">
                  </div>
                </div>
                <div class="btn-group" data-toggle="buttons" id="batu" style="width:100%;">
                  <div class="col-md-12" style="padding-left:30px;">
                    <div class="row">
                      <div class="col-md-3">
                        <div class="form-group has-success has-feedback">
                          <label>Natural <font color="red">*</font></label>
                          <select class="form-control select2 natural" name="natural" required>
                          <?php
                          $natural = $yi['obj_natural'];
                          if ($natural == "") {
                            ?>
                              <option value="">---select---</option>
                            <?php
                          }else{
                            $zx = mysql_query("SELECT obj_natural FROM tb_lab_desk where obj_natural='$natural'");
                            $yx = mysql_fetch_array($zx);
                            $xx = $yx['obj_natural'];
                            $hasi = strtoupper($xx); 
                            ?>
                            <option value="<?php echo $hasi?>"><?php echo $hasi?></option>
                            <?php
                          }
                            $zx = mysql_query("SELECT * FROM obj_natural where obj_natural!='$natural' order by obj_natural");
                            while($yx = mysql_fetch_array($zx)){
                            $xxc = $yx['obj_natural'];
                            $hasic = strtoupper($xxc);
                            ?>
                            <option value="<?php echo $hasic?>"><?php echo $hasic?></option>
                            <?php
                            }
                          ?>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group has-success has-feedback">
                          <label>Phenomenal <font color="red">*</font></label>
                          <select class="form-control select2 phenom" style="width:100%;" id="radio" name="phenomenal" required>
                          <?php
                          $phenomenal = $yi['phenomenal'];
                          if ($phenomenal == "") {
                            ?>
                              <option value="">---select---</option>
                            <?php
                          }else{
                            $zx = mysql_query("SELECT phenomenal FROM tb_lab_desk where phenomenal='$phenomenal'");
                            $yx = mysql_fetch_array($zx);
                            $xx = $yx['phenomenal'];
                            $hasi = strtoupper($xx); 
                            ?>
                            <option value="<?php echo $hasi?>"><?php echo $hasi?></option>
                            <?php
                          }
                            $zx = mysql_query("SELECT * FROM phenomenal where phenomenal_gems!='$phenomenal' order by phenomenal_gems");
                            while($yx = mysql_fetch_array($zx)){
                              $te = $yx['phenomenal_gems'];
                              $ha = strtoupper($te); 
                            ?>
                            <option value="<?php echo $ha?>"><?php echo $ha?></option>
                            <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div>

                      <div class="col-md-3">
                        <div class="form-group has-success">
                          <label>Varieties <font color="red">*</font></label>
                          <select class="form-control select2 variety" style="width:100%;" id="transparent" name="varieties" required>
                          <?php
                            $variety = $yi['variety'];
                            if ($variety == "") {
                          ?>
                            <option value="">---select---</option>
                          <?php
                            }else{
                            $zx = mysql_query("SELECT variety FROM tb_lab_desk where variety='$variety'");
                            $yx = mysql_fetch_array($zx);
                          ?>
                            <option value="<?php echo $yx['variety']?>"><?php echo $yx['variety']?></option>
                          <?php
                            }
                            $xz = mysql_query("SELECT * FROM mstr_varieties where varieties!='$variety' order by varieties");
                            while ($xy = mysql_fetch_array($xz)){
                          ?>
                            <option value="<?php echo $xy['varieties']?>"><?php echo $xy['varieties']?></option>
                          <?php
                            }
                          ?>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group has-success has-feedback">
                          <label class="control-label" for="inputSuccess">Species <font color="red">*</font></label>
                          <select class="form-control spec select2" style="width:100%;" name="batu" required>
                            <?php
                            $member = $yi['nama_batu'];
                            if ($member == "") {
                              ?>
                                <option value="">---select---</option>
                              <?php
                            }else{
                              $zx = mysql_query("SELECT nama_batu FROM tb_lab_desk where nama_batu='$member'");
                              $yx = mysql_fetch_array($zx);
                                $xx = $yx['nama_batu'];
                                $hasi = strtoupper($xx); 
                              ?>
                                <option value="<?php echo $hasi?>"><?php echo $hasi?></option>
                              <?php
                            }
                              $xz = mysql_query("SELECT * FROM mstr_species where species!='$member' order by species");
                              while ($xy = mysql_fetch_array($xz)){
                                $xx = $xy['species'];
                                $hasiv = strtoupper($xx);
                              ?>
                                <option value="<?php echo $hasiv?>"><?php echo $hasiv?></option>
                              <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                          <div class="form-group has-success has-feedback">
                            <label class="control-label" for="inputSuccess"> Treatment <font color="red">*</font></label>
                              <select class="form-control select2 treat" id="transparent" name="note" required>
                                <?php
                                  $note = $yi['note'];
                                  if ($note == "") {
                                    ?>
                                      <option value="">---select---</option>
                                    <?php
                                  }else{
                                    $zx = mysql_query("SELECT note FROM tb_lab_desk where note='$note'");
                                    $yx = mysql_fetch_array($zx);
                                      $xx = $yx['note'];
                                      $hasi = ucfirst($xx); 
                                    ?>
                                      <option value="<?php echo $hasi?>"><?php echo $hasi?></option>
                                    <?php
                                    }
                                    $zx = mysql_query("SELECT id, comment note FROM master_comments where comment!='$note'");
                                    while($yx = mysql_fetch_array($zx)){
                                      $xxc = $yx['note'];
                                      $hasic = ucfirst($xxc); 
                                    ?>
                                      <option value="<?php echo $hasic?>"><?php echo $hasic?></option>
                                    <?php
                                    }
                                ?>
                              </select>
                          </div>
                        </div>
                        <div class="col-md-3 col-sm-12">
                          <div class="form-group has-success">
                            <label>Microscopic 1 <font color="red">*</font></label>
                            <select name="microscopic" class="form-control" required>
                              <option value="" selected="" disabled="">--select--</option>
                              <?php
                              $getMisc = $this->db->query("SELECT * FROM master_microscopic ORDER BY microscopic");
                              foreach ($getMisc->result() as $misc) {
                                if($yi['microscopic'] == $misc->id){
                                ?>
                                  <option value="<?php echo $misc->id ?>" selected><?php echo $misc->microscopic ?></option>
                                <?php  
                                }else{
                                ?>
                                  <option value="<?php echo $misc->id ?>"><?php echo $misc->microscopic ?></option>
                                <?php
                                }
                              }
                              ?>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-3 col-sm-12">
                          <div class="form-group has-success">
                            <label>Microscopic 2</label>
                            <select name="microscopic_2" class="form-control">
                              <option value="" selected="" disabled="">--select--</option>
                              <?php
                              $getMisc = $this->db->query("SELECT * FROM master_microscopic ORDER BY microscopic");
                              foreach ($getMisc->result() as $misc) {
                                if($yi['microscopic_2'] == $misc->id){
                                ?>
                                  <option value="<?php echo $misc->id ?>" selected><?php echo $misc->microscopic ?></option>
                                <?php  
                                }else{
                                ?>
                                  <option value="<?php echo $misc->id ?>"><?php echo $misc->microscopic ?></option>
                                <?php
                                }
                              }
                              ?>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-3 col-sm-12">
                          <div class="form-group has-success">
                            <label>Microscopic_3</label>
                            <select name="microscopic_3" class="form-control">
                              <option value="" selected="" disabled="">--select--</option>
                              <?php
                              $getMisc = $this->db->query("SELECT * FROM master_microscopic ORDER BY microscopic");
                              foreach ($getMisc->result() as $misc) {
                                if($yi['microscopic_3'] == $misc->id){
                                ?>
                                  <option value="<?php echo $misc->id ?>" selected><?php echo $misc->microscopic ?></option>
                                <?php  
                                }else{
                                ?>
                                  <option value="<?php echo $misc->id ?>"><?php echo $misc->microscopic ?></option>
                                <?php
                                }
                              }
                              ?>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-12">
                         <div class="form-group has-success has-feedback">
                          <label class="control-label" for="inputSuccess"> Comment(s)</label>
                          <textarea class="form-control" name="comment"><?php echo $yi['comment'] ?></textarea>
                         </div>
                        </div>
                      </div>
                    </div>
                    <div class="cobafinal">
                      <div class="col-md-12">
                        <div class="form-group">
                          <div class="row">
                            <div class="col-md-12" style="margin-bottom:0px;">
                              <h5><b>FINAL ADVICE</b></h5>
                              <hr style="border:1px solid black;margin-top:-5px;">
                            </div>
                          </div>
                        </div>
                        <?php
                          $ri_start = $yi['ri_start'];
                          $sg_end = $yi['sg'];
                          $hardness = $yi['hardness'];
                          $query = $this->db->query("SELECT * FROM master_all where ri_start <= '$ri_start' and ri_end >= '$ri_start' and sg_start <= '$sg_end' and sg_end >= '$sg_end' and hard_start <= '$hardness' and hard_end >= '$hardness'");
                          foreach ($query->result_array() as $row) {
                        ?>
                        <div class="col-md-2">
                          <div class="box box-default">
                            <div class="box-header">
                              <h6 class="box-title">
                                <a href="#" onclick="dewa('<?php echo $row['gemstone'] ?>')"><?php echo "#".$row['gemstone']?></a>
                              </h6>
                            </div>
                          </div><!-- /.box -->
                        </div><!-- /.col -->
                        <?php
                        }
                        ?>
                      </div>
                    </div>
                  </div>  
                <div class="form-group">
                  <div class="col-md-12">
                      <button type="submit" name="submit" class="btn btn-primary pull-right" style="margin-bottom:20px;margin-right:10px;"><i class="fa fa-save"></i> Save </button>
                      <button type="reset" class="btn btn-default pull-right" style="margin-bottom:20px;margin-right:10px;"> <i class="fa fa-refresh"></i> Clear</button>
                      <a href="" class="btn btn-danger pull-right" style="margin-bottom:20px;margin-right:10px;"><i class="fa fa-chevron-left"></i> Back</a>
                  </div>
                </div>
              <!-- END -->
              </form>
          <?php
          $xf = $id;
          ?>
          <div class="modal fade" id="modalEdit">
            <div class="modal-dialog modal-sm">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <div class="modal-title" id="myModalLabel">Edit Image</div>
                </div>
                <div class="modal-body">
                  <div class="row">
                    <div class="col-md-12 col-sm-12">
                      <div class="form-group">
                        <form action="" id="frmuploadImgx" method="post" target="iframeUploadImgx" enctype="multipart/form-data">
                          <input type="file" class="drop form-control" name="gambar" id="drop" onchange="submitImagex()" data-default-file="./asset/images/<?php echo $yi['obj_image'] ?>">
                        </form>
                      </div>
                    </div>
                      <input class="hidden" type="text" name="gambaroutx" id="gambaroutx" value="<?php echo $yi['obj_image'] ?>">
                        <iframe class="hidden" name="iframeUploadImgx" id="iframeUploadImgx"></iframe> 
                      <div class="col-md-12 text-right">
                        <button class="btn btn-primary" onclick="edit_gambar('<?php echo $xf?>')">SAVE</button>
                      </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="modal fade" id="modalFro">
            <div class="modal-dialog modal-sm">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <div class="modal-title" id="myModalLabel">Edit Data Front</div>
                </div>
                <div class="modal-body">
                <div class="row">
                <?php
                $k = mysql_query("SELECT * FROM tb_front_desk where id_object='$id'");
                $x = mysql_fetch_array($k);
                ?>
                  <div class="col-lg-6 col-md-6 col-xs-12">
                      <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess">Weight</label>
                        <input type="number" step="0.01" name="weight" id="cts" class="form-control" value="<?php echo $x['obj_weight']?>" placeholder="Enter..." required>
                      </div>
                    </div>

                    <div class="col-lg-6 col-md-6 col-xs-12">
                      <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess">Length</label>
                        <input type="number" step="0.01" class="form-control" id="lmm" name="length" value="<?php echo $x['obj_length']?>" placeholder="Enter..." required>
                      </div>
                    </div>

                    <div class="col-lg-6 col-md-6 col-xs-12">
                      <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess"> Width</label>
                        <input type="number" step="0.01" class="form-control" id="wmm" name="width" value="<?php echo $x['obj_width']?>" placeholder="Enter..." required>
                      </div>
                    </div>

                    <div class="col-lg-6 col-md-6 col-xs-12">
                      <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess"> Height</label>
                        <input type="number" step="0.01" class="form-control" id="hmm" name="height" value="<?php echo $x['obj_height']?>" placeholder="Enter..." required>
                      </div>
                    </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <button class="btn btn-primary" type="submit" onclick="save_dimen('<?php echo $id?>')">SAVE</button>
                    </div>
                  </div>
                </div>
                </div>
              </div>
            </div>
          </div>
          <div class="modal fade" id="modalCol">
            <div class="modal-dialog modal-md">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <div class="modal-title" id="myModalLabel">Edit Color</div>
                </div>
                <div class="modal-body">
                  <div class="row">
                      <div class="col-md-6">
                        <div class="form-group has-success">
                          <label>Color</label>
                          <select name="colr" id="obj" class="form-control" onchange="chos()" required>
                            <option>Select color..</option>
                              <?php
                                $col = mysql_query("SELECT * FROM color_stone GROUP BY jenis_warna");
                                while ($ge = mysql_fetch_array($col)) {
                              ?>
                              <?php
                                if($ge['jenis_warna'] == "Select color.."){
                              ?>
                            <option value="<?php echo $ge['code'] ?>" selected style="text-transform: capitalize;"><?php echo $ge['jenis_warna'] ?></option>  
                              <?php
                                }else{
                              ?>
                            <option value="<?php echo $ge['code'] ?>" style="text-transform: capitalize;"><?php echo $ge['jenis_warna'] ?></option>
                              <?php
                                  }
                              }
                              ?>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group has-success">
                          <label>Specific Color</label>
                          <div style="min-height: 120px;">
                            <div class="btn-group" id="col-area" data-toggle="buttons">
                              -
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <button class="btn btn-primary" onclick="setColor()">SAVE</button>
                        </div>
                      </div>
                   </div>
                  </div>
                </div>
              </div>
            </div>
    
    <script>
      $(function(){
    $('#pluphy').hide();
    $('#minopt').hide();
    $('#minfro').hide();
    $('#frontc').hide();
    $('#optical').hide();
    $('#mincla').hide();
    $('#classification').hide();
    $('#mincry').hide();
    $('#crystal').hide();
    $('#minocc').hide();
    $('#occuran').hide();
  });
  function phisix() {
    $('#minphy').show();
    $('#pluphy').hide();
    $('#physy').show(700);
  }
  function phisi() {
    $('#minphy').hide();
    $('#pluphy').show();
    $('#physy').hide(700);
  }
  function frontx() {
    $('#minfro').show();
    $('#plufro').hide();
    $('#frontc').show(700);
  }
  function front() {
    $('#minfro').hide();
    $('#plufro').show();
    $('#frontc').hide(700);
  }
  function opticx() {
    $('#minopt').show();
    $('#pluopt').hide();
    $('#optical').show(700);
  }
  function optic() {
    $('#minopt').hide();
    $('#pluopt').show();
    $('#optical').hide(700);
  }
  function classx() {
    $('#mincla').show();
    $('#plucla').hide();
    $('#classification').show(700);
  }
  function classc() {
    $('#mincla').hide();
    $('#plucla').show();
    $('#classification').hide(700);
  }
  function crystx() {
    $('#mincry').show();
    $('#plucry').hide();
    $('#crystal').show(700);
  }
  function cryst() {
    $('#mincry').hide();
    $('#plucry').show();
    $('#crystal').hide(700);
  }
  function occurx() {
    $('#minocc').show();
    $('#pluocc').hide();
    $('#occuran').show(700);
  }
  function occur() {
    $('#minocc').hide();
    $('#pluocc').show();
    $('#occuran').hide(700);
  }
    function submit() {
      $('#coba').html('Gambar Sudah Ditetapkan');
    }
    $("#drop").dropify();

    $(document).on('keypress',function(e){
      if(e.keyCode==13){
          e.preventDefault();
      }
    });
    function submitImagex(){
      $( "#frmuploadImgx" ).submit();
    }
    function setColor(){
      var color = $(".coll:checked").val();
      $.ajax({
        type : "POST",
        url : base_url+"object/color_sertifikasi",
        data : {
          "warna" : color, "id_obj" : id
        },
        success:function(response){
			var data = eval ("(" + response + ")");
			if(data.success){
				$("#modalCol").modal('hide');
				$("#div_color").css("background-color","#"+data.rgb_color);
				$("#div_color_text").html(data.color_spek);
			}
			
        }
      });
    }
    </script>