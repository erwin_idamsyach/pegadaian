      <!-- Modal -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" style="width:400px;">
          <div class="modal-content">
            <div class="modal-header" style="background-color:#f8f8f8">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h6 class="modal-title" id="myModalLabel"><b>VIEW OBJECT</b></h6>
            </div>
            <div class="modal-body">
              <div class="lol">
                
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="index">
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>&nbsp;</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-user"></i> Home</a></li>
            <li>Daftar Permintaan</li>
            <li class="active"><?php echo ucfirst($menu) ?></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          
            <div class="box">
            <div class="box-header">
              <h5><b>DAFTAR PERMINTAAN TAKSIRAN</b></h5>
              <hr style="border:1px solid black;margin-bottom:-10px;margin-top:-5px;">
              <br>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><span class="fa fa-minus"></span></button>
                <button class="btn btn-box-tool" data-widget="remove"><span class="fa fa-times"></span></button>
              </div>
            </div><!-- /.box-header -->
            <div class="box-body" style="margin-top:-15px;">
              <?php
                date_default_timezone_get('Asia/Jakarta');
              ?>
                <table id="example2" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th class="text-center" width="10%">NO</th>
                      <th class="text-center" width="17%">DATE</th>
                      <th class="text-center" width="20%">NO ORDER</th>
                      <th class="text-center">NASABAH</th>
                      <th class="text-center" width="10%">ACTION</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php
					$no = 1;
                    $query = $this->db->query($sql);
                    foreach ($query->result_array() as $i) {
                    $tglx = date('Y-m-d H:i:s', strtotime($i['input_date']));
                    $id_order = str_replace('/','-',$i['id_order']);
                  ?>
                    <tr>
                      <td style="text-align:center"><?php echo $no++?></td>
                      <td class="text-center"><?php echo $tglx ?></td>
                      <td><?php echo $i['id_order'] ?></td>
                      <td><?php echo $i['nama'] ?></td>
                      <td style="text-align:right;">
                        <a href="<?php echo site_url('object/taksiran_detail/'.$id_order); ?>" class="btn btn-info" data-toggle="tooltip" data-placement="bottom" title="View Object"><i class="fa fa-eye"></i></a>
                      </td>
                    </tr>
                    <?php
                      }
                    ?>
                  </tbody>
                </table>
              </div>
            </div><!-- /.box-body -->
			
          </section><!-- /.content -->
        </div><!-- /.content-wrapper -->
      </div>
      <script>
        $("#example2").DataTable();
      </script>