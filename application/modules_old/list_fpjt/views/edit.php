	<script type="text/javascript">
		$(document).ready(function(){
			$("#jumlah_permata").attr("disabled","disabled");
			$("#jenis_permata").attr("disabled","disabled");
			$("#obj").attr("disabled","disabled");
			
			setDisable();
			chos();
			
			$('#form-edit-fpjt').trigger("reset");
			$("#form-edit-fpjt").on('submit',function(e){
			e.preventDefault();
			var formatData = new FormData($(this)[0]);
				swal({
				 title: "",
				 text: "Apakah Anda akan menyimpan data?",
				 type: "warning",
				 showCancelButton: true,
				 confirmButtonColor: "#DD6B55",
				 confirmButtonText: "Ya",
				 cancelButtonText: "Tidak",
				 closeOnConfirm: false }, function(){
					$.ajax({
						type : 'POST',
						url : '<?php echo base_url()?>list_fpjt/submitEdit',
						data : formatData,
						async: false,
						cache: false,
						contentType: false,
						processData: false,
						success:function(html){
							var data = eval ("(" + html + ")");
							if(data.success){
								swal({
								 title: "Berhasil Menyimpan data!",
								 text: "Klik tombol di bawah.",
								 type: "success",
								 showCancelButton: false,
								 confirmButtonColor: "#257DB6",
								 confirmButtonText: "Ok!",
								 closeOnConfirm: false
								},
								function(){
									location.href = base_url+"list_fpjt";
								});
							}else{
								swal({
								 title: data.msg,
								 text: "Klik tombol di bawah.",
								 type: "warning",
								 showCancelButton: false,
								 confirmButtonColor: "#257DB6",
								 confirmButtonText: "Ok!",
								 closeOnConfirm: false
								});
							}
						}
					});
				});
			});
			
		});
		jQuery(function($){
			$.mask.definitions['d'] = '[0-9.]';
			
			$("#weight").mask("9?ddd.dd",{placeholder:"____.00"});
        });
		
		function backToList(){
			location.href = base_url+"list_fpjt";
		}
		
        function setDisable(){
          if($("#dengan_permata").val()=="Y") {
             $("#jumlah_permata").removeAttr("disabled");
             $("#jenis_permata").removeAttr("disabled");
             $("#obj").removeAttr("disabled");
             $(".coll").removeAttr("disabled");
          } else {
             $("#jumlah_permata").attr("disabled","disabled");
             $("#jenis_permata").attr("disabled","disabled");
             $("#obj").attr("disabled","disabled");
             $(".coll").attr("disabled","true");
          }
        }
    </script>
      <?php
      $ix = $this->session->userdata('order');
      $get_order  = $this->db->query("SELECT * FROM step WHERE id_order='$ix'");
      $numing = $get_order->num_rows();
      ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>&nbsp;</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-user"></i> Home</a></li>
            <li>Reg. BPT</li>
            <li class="active">Add BPT</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
        <div class="index">
          <div class="box">
            <div class="box-header">
              <b>EDIT : <?php echo $tmp['id_order']; ?></b>
			  
              <div style="border:1px solid black;margin-bottom:-10px;"></div>
                <div class="box-tools pull-right">
                  <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
            <i>DATA NASABAH</i>
              <div style="border:1px solid black;margin-bottom:10px;"></div>
          <form method="post" name="form-edit-fpjt" id="form-edit-fpjt" enctype="multipart/form-data">
			<input type="hidden" name="id_fpjt" id="id_fpjt" value="<?php echo $tmp['id']; ?>">
			<input type="hidden" name="no_fpjt" id="no_fpjt" value="<?php echo $tmp['id_order']; ?>">
            <div class="row">
              <div class="col-md-3 col-sm-6">
                <div class="form-group has-feedback form-cif">
                  <label>No. CIF</label>
                  <div class="input-group">
				  <?php
				  if($tmp['kode']=="A"){
					  $nama = $tmp['first_name'].' '.$tmp['middle_name'].' '.$tmp['last_name'];
				  }else{
					  $nama = $tmp['corp_name'];
				  }
				  ?>
                    <span class="input-group-addon">CIF</span>
                    <input type="text" name="cif" class="form-control cif" id="id_mem" placeholder="No. CIF" value="<?php echo $tmp['id_member']; ?>" readonly required>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6">
                <div class="form-group">
                  <label>Nama</label>
                  <input type="text" name="nama" class="form-control nama" placeholder="Nama" value="<?php echo $nama; ?>" readonly>
                </div>
              </div>
              <div class="col-md-3 col-sm-6">
                <div class="form-group">
                  <label>Alamat</label>
                  <input type="text" name="alamat" class="form-control alamat" placeholder="Alamat" value="<?php echo $tmp['address']; ?>" readonly>
                </div>
              </div>
              <div class="col-md-3 col-sm-6">
                <div class="form-group">
                  <label>No Telp/HP</label>
                  <input type="text" name="telp" class="form-control telp" placeholder="No. Telp/HP" value="<?php echo $tmp['phone']; ?>" readonly>
                </div>
              </div>
            </div>
            <i>DATA OBYEK</i>
              <div style="border:1px solid black;margin-bottom:10px;"></div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Jumlah Perhiasan <font color="red">*</font></label>
                          <input class="form-control" type="text" id="jumlah" name="jumlah" onKeyUp="this.value=ThausandSeperator(this.value,2);" value="<?php echo $tmp['jumlah']; ?>" required />
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Weight <font color="red">*</font></label>
                          <div class="input-group"><input class="form-control" id="weight" name="weight" type="text" value="<?php echo $tmp['berat_perhiasan']; ?>" required /><span class="input-group-addon">gram</span></div>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Jenis Perhiasan </label>
                          <select name="jenis_perhiasan" class="form-control" id="perhiasan">
                            <option value="">--Jenis Perhiasan--</option>
                            <?php
                            $get_perhiasan = $this->db->query("SELECT * FROM master_jenis_permata");
                            foreach ($get_perhiasan->result() as $per) {
								if($per->id==$tmp['jenis_perhiasan']){
									$selected_jenis_perhiasan = "selected";
								}else{
									$selected_jenis_perhiasan = "";
								}
                              ?>
                            <option value="<?php echo $per->id;?>" <?php echo $selected_jenis_perhiasan;?>><?php echo $per->jenis ?></option>
                              <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                            <label>
                              Dengan Permata
                            </label>
							  <?php
							  if($tmp['jenis_permata']!=""){
								  $selected_dengan_permata_y = "selected";
								  $selected_dengan_permata_n = "";
							  }else{
								  $selected_dengan_permata_y = "";
								  $selected_dengan_permata_n = "selected";
							  }
							  ?>
							<select name="dengan_permata" class="form-control" id="dengan_permata" onchange="setDisable()">
							  <option value="">--Dengan Permata--</option>
							  <option value="Y" <?php echo $selected_dengan_permata_y;?>>Ya</option>
							  <option value="N" <?php echo $selected_dengan_permata_n;?>>Tidak</option>
							</select>
                        </div>
                      </div>
					</div>
                  </div>				  
                </div>
            <i>DATA PERMATA</i>
              <div style="border:1px solid black;margin-bottom:10px;"></div>
				<div class="row">
				  <div class="col-md-12">
					<div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Jenis Permata</label>
                          <select name="jenis_permata" class="form-control" id="jenis_permata" required>
                            <option value="">--Jenis Permata--</option>
                            <?php
                            $get_perhiasan = $this->db->query("SELECT * FROM master_permata");
                            foreach ($get_perhiasan->result() as $per) {
								if($per->id==$tmp['jenis_permata']){
									$selected_jenis_permata = "selected";
								}else{
									$selected_jenis_permata = "";
								}
                              ?>
                            <option value="<?php echo $per->id; ?>" <?php echo $selected_jenis_permata; ?>><?php echo $per->permata;?></option>
                              <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Jumlah Permata</label>
                          <input type="text" name="jumlah_permata" class="form-control" id="jumlah_permata" value="<?php echo $tmp['jumlah_permata']; ?>">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Warna</label>
                          <select name="colr" id="obj" class="form-control" onchange="chos()" required>
                            <?php
                            $col = mysql_query("SELECT * FROM color_stone GROUP BY jenis_warna");
                            while ($ge = mysql_fetch_array($col)) {
								if($ge['jenis_warna']==$tmp['jenis_warna']){
									$selected_warna_permata  = "selected";
								}else{
									$selected_warna_permata = "";
								}
                            ?>
                            <option value="<?php echo $ge['code'] ?>" <?php echo $selected_warna_permata;?> style="text-transform: capitalize;"><?php echo $ge['jenis_warna'] ?></option>  
							<?php
                            }
                            ?>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-7">
                        <div class="form-group">
                          <label>Warna Spesifik</label>
                          <div style="min-height: 100px;">
                            <div class="btn-group" id="col-area" data-toggle="buttons">
                              <?php
                              $sql = mysql_query("SELECT * FROM color_stone WHERE jenis_warna='Grey'");
                              while ($do =mysql_fetch_array($sql)) {
                              ?>
                                <label class="btn btn-default" style="white-space:normal; background-color: <?php echo $do['rgb_code'] ?>; width: 110px; height: 110px;">
                                <input type="radio" class="coll" name="spe_col" id="specol" autocomplete="off" value="<?php echo $do['id'] ?>">
                                <span class="fa fa-check fa-1x"></span><br/>
                                <center>
                                  <font style="mix-blend-mode: difference"><?php echo $do['code'].'<br>'.$do['color'] ?></font>
                                </center>
                              </label>
                              <?php
                              }
                              ?>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-5">
                        <div class="form-group">
                          <label>Image (Maximal size 300 Kb)</label>
                          <input type="file" name="image" class="form-control drop" data-default-file="<?php echo base_url() ?>asset/image-fpjt/<?php echo $tmp['foto']; ?>">
                        </div>
                      </div>
					  <div class="col-md-6">
						<div class="form-group">
						  <label>Keterangan Tambahan</label>
						  <textarea name="comment" class="form-control"><?php echo $tmp['keterangan']; ?></textarea>
						</div>
					  </div>
                    </div>
				  </div>
                  <div class="col-md-12" style="margin-top:20px;">
                    <button type="submit" class="btn btn-default"><i class="fa fa-save"></i> SIMPAN</button>
                    <button type="button" class="btn btn-default" onclick="backToList()"><i class="fa fa-refresh"></i> BACK</button>
                  </div>
				</div>
              </form>
                    </div>
                </div>
            </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->