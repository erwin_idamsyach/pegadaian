<script>
		function deleteObjectFPJT(id){
			swal({
             title: "",
             text: "Apakah Anda akan menghapus data?",
             type: "warning",
             showCancelButton: true,
             confirmButtonColor: "#DD6B55",
             confirmButtonText: "Ya",
             cancelButtonText: "Tidak",
             closeOnConfirm: false }, function(){
                $.ajax({
				  type : "POST",
				  url  : base_url+"list_fpjt/delete_object/"+id,
				  data : {
				  },
				  success:function(html){ 
					swal({
					  title: "Berhasil Menghapus data!",
					  text: "Klik tombol di bawah.",
					  type: "success",
					  showCancelButton: false,
					  confirmButtonColor: "#257DB6",
					  confirmButtonText: "Ok!",
					  closeOnConfirm: false
					},
					function(){
					  location.href = base_url+"list_fpjt";
					});
					
				  }
				});
            });
			
		}
</script>

<?php
$do = str_replace('-', '/', $key);
$get_user = $this->db->query("SELECT * FROM tb_fpjt, tb_member WHERE tb_fpjt.id_member = tb_member.id_member AND tb_fpjt.id_order='$do' GROUP BY tb_fpjt.id_order");
?>
<table width="100%">
	<?php
	foreach ($get_user->result() as $userData) {
		$new_ord = substr($userData->id_order, -20, 9);
		$new_ord = str_replace("/", "-", $new_ord);
		$barcode = $userData->id_member.$new_ord; 
	?>
	<tr>
		<td width="13%" style="padding-right: 15px;">No. FPJT</td>
		<td width="5%" style="padding-right: 15px;padding-left: 15px;">:</td>
		<td style="padding-left: 15px;"><?php echo $do?></td>
		<td style="padding-left: 15px;" width="50%" rowspan="3" align="right"><img id="img_fpjt_barcode" height="50" src="<?php echo base_url()?>asset/barcode-fpjt/<?php echo $barcode;?>.gif"></td>
	</tr>
	
	<tr>
		<td style="padding-right: 15px;">Nama Pemohon</td>
		<td style="padding-right: 15px;padding-left: 15px;">:</td>
		<td style="padding-left: 15px;"><?php echo $userData->first_name." ".$userData->middle_name." ".$userData->last_name?></td>
	</tr>	
	<tr>
		<td style="padding-right: 15px;">Alamat</td>
		<td style="padding-right: 15px;padding-left: 15px;">:</td>
		<td style="padding-left: 15px;"><?php echo $userData->address?></td>
	</tr>
		<?php
	}
	?>
</table>
<br>
<table class="table table-bordered table-striped table-hover">
	<tr>
		<th class="text-center" width="7%">No</th>
		<th>Jenis Perhiasan</th>
		<th class="text-center" width="18%">Berat (gram)</th>
		<th class="text-center" width="15%">Jenis Permata</th>
		<th class="text-center" width="15%">Jumlah Permata</th>
		<th class="text-center" width="25%">Action</th>
	</tr>
	<?php
	$no = 1;
	$get_data = $this->db->query("SELECT tb_fpjt.*, master_jenis_permata.jenis, master_permata.permata FROM tb_fpjt
								LEFT JOIN master_jenis_permata ON master_jenis_permata.id = tb_fpjt.jenis_perhiasan
								LEFT JOIN master_permata ON master_permata.id = tb_fpjt.jenis_permata
								WHERE ISNULL(tb_fpjt.delete_by) AND tb_fpjt.id_order='$do'");
	foreach ($get_data->result() as $get) {
		?>
	<tr>
		<td style="vertical-align: middle;" class="text-center"><?php echo $no++; ?></td>
		<td style="vertical-align: middle;"><?php echo $get->jenis ?></td>
		<td style="vertical-align: middle;"><?php echo $get->berat_perhiasan ?></td>
		<td style="vertical-align: middle;"><?php echo $get->permata ?></td>
		<td style="vertical-align: middle;" class="text-right"><?php echo $get->jumlah_permata ?></td>
		<td style="vertical-align: middle;" class="text-center">
            <button class="btn btn-info disabled" data-toggle="tooltip" data-placement="bottom" title="View"><i class="fa fa-eye"></i></button>
            <a href="<?php echo site_url('list_fpjt/edit_object').'/'.$get->id ?>" class="btn btn-warning" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="fa fa-edit"></i></a>
            <a href="#" onclick="deleteObjectFPJT(<?php echo $get->id;?>)" class="btn btn-danger" data-toggle="tooltip" data-placement="bottom" title="Delete"><i class="fa fa-trash"></i></a>
            <a href="#" class="btn btn-primary disabled" data-toggle="tooltip" data-placement="bottom" title="Print"><i class="fa fa-print"></i></a>
		</td>
	</tr>
		<?php
	}
	?>
	<tr>
		<td colspan="5">
			<div style="margin-top: 10px;">
				<b>&nbsp;</b>
			</div>
		</td>
		<td class="pull-right">
			<a href="<?php $ord= str_replace('/', '-', $get->id_order);echo site_url('list_fpjt/print_bpt').'/'.$ord ?>" class="btn btn-primary" target="_blank" data-toggle="tooltip" data-placement="bottom" title="Print BPT">Print BPT</a>
		</td>
	</tr>
</table>