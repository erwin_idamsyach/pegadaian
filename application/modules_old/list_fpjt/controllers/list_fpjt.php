<?php if (! defined('BASEPATH')){ exit('No direct script allowed'); }
/*
* Controller List FPJS
* Create by Erwin Idamsyach @ 2 May 2016
*/
class list_fpjt extends CI_Controller{	
	function list_fpjt(){
        parent::__construct();
		
		if(!isLogin()){
			goLogin();
		}
        $this->load->library('fpdf');
        $this->load->library('Barcode39');
    }
	public function index(){
		$data['filelist'] = 'list_fpjt/front';
		$data['title'] = 'List Form Permintaan Jasa Taksiran (FPJT)';
		$data['title_menu'] = 'Front';
		$data['menu'] = 'list_fpjt';

		getHTMLWeb($data);
	}

	public function view_data(){
		$key = $this->input->post('key');
		$data['key'] = $key;
		$this->load->view('view', $data);
	}
	public function delete_object(){
        $id = $this->uri->segment(3);

        $this->db->where('id', $id);
        $this->db->update('tb_fpjt', array('delete_by'=> sessionValue('id'), "delete_date" => date('Y-m-d H:i:s')));
        redirect(site_url('list_fpjt'));
    }
	
	// Hadi
	function edit_object($id){ 
		$get_transaksi = $this->db->query("
				SELECT tb_fpjt.*, 
				tb_member.first_name, tb_member.middle_name, tb_member.last_name, tb_member.corp_name, tb_member.kode, tb_member.address, tb_member.postal_code, tb_member.phone, tb_member.no_identitas,
				color_stone.jenis_warna
					FROM tb_fpjt 
				LEFT JOIN tb_member ON tb_member.id_member = tb_fpjt.id_member
				LEFT JOIN color_stone ON color_stone.code = tb_fpjt.warna_permata
				WHERE tb_fpjt.id='".$id."'");
		$tmp = $get_transaksi->result_array();
    
		$data['filelist'] = 'list_fpjt/edit';
		$data['title'] = 'Edit Form Permintaan Jasa Taksiran (FPJT)';
		$data['menu'] = 'list_fpjt';
		$data['title_menu'] = 'Front';

		$data['tmp'] = $tmp[0];
		
		getHTMLWeb($data);
	}
	
	function submitEdit(){
		$id = $this->input->post('id_fpjt');
		
		$data = array(
			'jenis_perhiasan' => $this->input->post('jenis_perhiasan'),
			'jenis_permata' => $this->input->post('jenis_permata'),
			'jumlah_permata' => $this->input->post('jumlah_permata'),
			'warna_permata' => $this->input->post('spe_col'),
			'keterangan' => $this->input->post('comment'),
            'update_date' => date("Y-m-d H:i:s"),
            'update_by' => sessionValue('id')
		);
		
		$success = false;
		$msg = "";
		if($_FILES['image']['size'] != 0){
			
			if($_FILES['image']['name'])
			{
				//if no errors...
				if(!$_FILES['image']['error'])
				{
					$valid_file = true;
					//now is the time to modify the future file name and validate the file
					//$new_file_name = $_FILES['image']['name']; //rename file
					$new_file_name = $id.".jpg";

					if($_FILES['image']['size'] > (307200)) //can't be larger than 300 KB
					{
						$valid_file = false;
						$msg = "Ukuran gambar terlalu besar.";
					}
						
					//if the file has passed the test
					if($valid_file){
						//move it to where we want it to be
						$moved = move_uploaded_file($_FILES['image']['tmp_name'], './asset/image-fpjt/'.$new_file_name);
						//$message = 'Congratulations!  Your file was accepted.';
						if($moved){
							$success = true;
							$dataUpdate = array(
								'foto' => $new_file_name
							);
							$this->db->where('id', $id);
							if($this->db->update('tb_fpjt', $dataUpdate)){
								$success = true;
							}else{
								$msg = "Image not be inserted.";
							}
						}else{
							$msg = "Image can't upload";
						}
					}
				}else{
					//set that to be the returned message
					$msg = 'Error: Ooops!  Your upload triggered the following error:  '.$_FILES['image']['error'];
					$success = false;
				}
			}else{
				$success = true;
			}
		}else{
			$success = true;
		}
		
		if($success){
			$this->db->where('id', $id);
			$this->db->update('tb_fpjt', $data);
		}
        
		echo json_encode(array("success"=>$success, "msg"=>$msg));
	}
	
	function delete($id){
		$id = str_replace('-', '/', $id); 
		$data = array(
            'delete_date' => date('Y-m-d H:i:s'),
            'delete_by' => sessionValue('id')
        );
        $this->db->where('id_order', $id);
        $this->db->update('tb_fpjt', $data);
    }
	
	public function print_bpt(){
		$ord = $this->uri->segment(3);
		$ord = str_replace('-', '/', $ord);
		$data['ord'] = $ord;
		$this->load->view('print-order', $data);
	}
	
	function printFPJT($id=NULL){
		
        $pdf = new FPDF('P','cm','A4');
		$pdf->SetMargins(2,1,2);
		$pdf->SetAutoPageBreak(true, 0);
		
		$pdf->AddPage();
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(19, 1, 'FORM PERMINTAAN JASA TAKSIRAN','',0,'C');
			
		$id = str_replace('-', '/', $id);
			
        $que = $this->db->query("SELECT tb_fpjt.id_order, tb_fpjt.create_date, tb_member.kode,
								tb_member.first_name, tb_member.middle_name, tb_member.last_name, tb_member.corp_name, tb_member.address, tb_member.postal_code, tb_member.phone, tb_member.no_identitas,
								kecamatan.kecamatanNama kec1, kabupaten.kabupatenNama kota1, provinsi.provinsiNama prov1,
								master_pekerjaan.pekerjaan, master_identitas.identitas
									FROM 
								tb_fpjt
								LEFT JOIN tb_member ON tb_member.id_member = tb_fpjt.id_member
								LEFT JOIN kecamatan ON kecamatan.kecamatanId = tb_member.district
								LEFT JOIN kabupaten ON kabupaten.kabupatenId = tb_member.city
								LEFT JOIN provinsi ON provinsi.provinsiId = tb_member.province
								LEFT JOIN master_pekerjaan ON master_pekerjaan.id = tb_member.pekerjaan
								LEFT JOIN master_identitas ON master_identitas.id = tb_member.identitas
								WHERE id_order='".$id."' GROUP BY id_order");
        
		foreach ($que->result() as $get) {
			$pdf->Ln();
			$pdf->SetFont('Arial','BI',9);
			$pdf->Cell(19, 0.5, 'IDENTITAS NASABAH','B',0,'L');
			
			$pdf->SetFont('Arial','',9);
			
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'No. Order','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, $get->id_order,'',0,'L');
			
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'No. Identitas','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, '('.$get->identitas.') '.$get->no_identitas,'',0,'L');
			
			if($get->kode=="A"){
				$nama = $get->first_name." ".$get->middle_name." ".$get->last_name;
			}else{
				$nama = $get->corp_name;
			}
			
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'Nama','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, $nama,'',0,'L');
			
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'Alamat','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, $get->address,'',0,'L');
			$pdf->Ln();
			$pdf->Cell(4, 0.5, '','',0,'L');
			$pdf->Cell(0.5, 0.5, '','',0,'L');
			$pdf->Cell(14, 0.5, 'Kode Pos : '.$get->postal_code.', '.$get->kec1.', '.$get->kota1.', '.$get->prov1,'',0,'L');
			
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'Pekerjaan','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, (($get->pekerjaan!="")?$get->pekerjaan:'-'),'',0,'L');
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'No. Telp / HP','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, (($get->phone!="")?$get->phone:'-'),'',0,'L');	
        }
			
        $que = $this->db->query("SELECT tb_fpjt.*, master_jenis_permata.jenis, color_stone.color
									FROM 
								tb_fpjt
								LEFT JOIN tb_member ON tb_member.id_member = tb_fpjt.id_member
								LEFT JOIN master_jenis_permata ON master_jenis_permata.id = tb_fpjt.jenis_perhiasan
								LEFT JOIN color_stone ON color_stone.code = tb_fpjt.warna_permata
								WHERE id_order='".$id."' GROUP BY id_order");
        
		foreach ($que->result() as $get) {
			$pdf->Ln(0.8);
			$pdf->SetFont('Arial','BI',9);
			$pdf->Cell(19, 0.5, 'KETERANGAN BARANG YANG AKAN DIPERIKSA','B',0,'L');
			
			$pdf->SetFont('Arial','',9);
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'Jenis Perhiasan','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, (($get->jenis!="")?$get->jenis:'-'),'',0,'L');
			
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'Jenis Permata','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, (($get->jenis_permata!="")?$get->jenis_permata:'-'),'',0,'L');
			
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'Warna','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, (($get->color!="")?$get->color:'-'),'',0,'L');
			
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'Jumlah','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, (($get->jumlah_permata!="")?$get->jumlah_permata:'-'),'',0,'L');
			
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'Keterangan Lainnya','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, (($get->keterangan!="")?$get->keterangan:'-'),'',0,'L');
			
        }
		
		$pdf->Output();
    }
}
?>