<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class service extends CI_Controller{
    function service(){
        parent::__construct();
    
		if(!isLogin()){
		  goLogin();
		}
    }
    
    function index($text=NULL){
        $data["filelist"] = "service/service";
        $data["title"] = "Admin";
        $data["title_menu"] = "Admin";
        $data["menu"] = "service";
    
        getHTMLWeb($data);
    }

    function delete($id)
    {
        $data = array(
            'delete_by' => sessionValue('id'),
			'delete_date' => date('Y-m-d H:i:s')
        );

        $this->db->where('id', $id);
        $this->db->update('tb_service', $data);
		//$this->db->delete('tb_service', array('id'=>$id));
        redirect('service');
    }

    function edi_servi($id)
    {
        $this->load->view('edit', array('id'=>$id));
    }

    function edit_service($id)
    {
        $code  = $this->input->post('code');
        $name  = $this->input->post('name');
        $price = $this->input->post('price');

        $data = array(
            'kode'  => $code,
            'nama'  => $name,
            'harga' => $price,
            'update_by' => sessionValue('id'),
			'update_date' => date('Y-m-d H:i:s')
        );

        $this->db->where('id', $id);
        $this->db->update('tb_service', $data);
    }
}
