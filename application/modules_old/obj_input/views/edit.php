          <div class="box">
            <div class="box-header">
              <b>EDIT OBJECT</b>
              <div style="border:1px solid black;margin-bottom:0px;"></div>
                <div class="box-tools pull-right">
                  <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
              <div class="col-md-12">
                <div class="row">
                  <div class="col-md-8">
                    <div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>ID Member</label>
                          <?php
                            date_default_timezone_set('Asia/Jakarta');
                            $tgl = date('Y-m-d');
                            $ix = $this->session->userdata('order');
                            $q = $this->db->query("SELECT * FROM tb_front_desk a, color_stone b where a.id_object='$main' and a.obj_color=b.id");
                            foreach ($q->result_array() as $k) {
                            }
                          ?>
                            <input type="text" name="id_mem" id="id_mem" class="form-control id_mem" value="<?php echo $k['id_member'] ?>" placeholder="" autofocus required>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Weight</label>
                          <div class="input-group"><input class="form-control weight" id="weight" name="weight" value="<?php echo $k['obj_weight'] ?>" type="number"/><span class="input-group-addon">CT</span></div>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Length</label>
                          <div class="input-group"><input class="form-control length" id="length" name="length" value="<?php echo $k['obj_length'] ?>" type="number"/><span class="input-group-addon">mm</span></div>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Width</label>
                          <div class="input-group"><input class="form-control width" id="width" name="width" value="<?php echo $k['obj_width'] ?>" type="number"/><span class="input-group-addon">mm</span></div>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Height</label>
                          <div class="input-group"><input class="form-control height" id="height" name="height" value="<?php echo $k['obj_height'] ?>" type="number"/><span class="input-group-addon">mm</span></div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Color</label>
                          <select name="colr" id="obj" class="form-control" onchange="chos()" required>
                            <option value="<?php echo $k['code'] ?>" selected style="text-transform: capitalize;"><?php echo $k['jenis_warna'] ?></option>  
                            <?php
                            $col = mysql_query("SELECT * FROM color_stone where jenis_warna!='".$k['jenis_warna']."' GROUP BY jenis_warna");
                            while ($ge = mysql_fetch_array($col)) {
                            ?>
                            <option value="<?php echo $ge['code'] ?>" style="text-transform: capitalize;"><?php echo $ge['jenis_warna'] ?></option>
                            <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>Specific Color</label>
                          <div style="min-height: 100px;">
                            <div class="btn-group" id="col-area" data-toggle="buttons">
                              <?php
                              $sql = mysql_query("SELECT * FROM color_stone WHERE jenis_warna='".$k['jenis_warna']."'");
                              while ($do = mysql_fetch_array($sql)) {
                                if($do['id'] == $k['obj_color']){
                              ?>
                                <label class="btn btn-default active" style="background-color: <?php echo $do['rgb_code'] ?>; width: 90px; height: 90px;">
                                  <input type="radio" class="coll" name="spe_col" id="specol" autocomplete="off" value="<?php echo $do['id'] ?>" required>
                                  <span class="fa fa-check fa-3x"></span><br/>
                                  <center>
                                    <font style="mix-blend-mode: difference"><?php echo $do['code'] ?></font>
                                  </center>
                                </label>
                              <?php
                              }else{
                              ?>
                                <label class="btn btn-default" style="white-space:normal; background-color: <?php echo $do['rgb_code'] ?>; width: 110px; height: 110px;">
                                  <input type="radio" class="coll" name="spe_col" id="specol" autocomplete="off" value="<?php echo $do['code'] ?>" required>
                                  <span class="fa fa-check fa-1x"></span><br/>
                                  <center>
                                    <font style="mix-blend-mode: difference"><?php echo $do['code'] ?></font>
                                  </center>
                                </label>
                              <?php
                                }
                              }
                              ?>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-12">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <form action="<?php echo site_url('obj_input/save_img')?>" id="frmuploadImg" method="post" target="iframeUploadImg" enctype="multipart/form-data">
                            <label>Image</label>
                            <input type="file" class="drop form-control" name="gambar" id="drop" onchange="submitImage()" data-default-file="<?php echo base_url() ?>asset/images/<?php echo $k['obj_image'] ?>">
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                  <input class="gambarout hidden" type="text" name="gambarout" id="gambarout">
                  <iframe class="hidden" name="iframeUploadImg" id="iframeUploadImg"></iframe>
                  <div class="col-md-12" style="margin-top:20px;">
                    <button type="submit" class="btn btn-default" onclick="edit_obj('<?php echo $main ?>')"><i class="fa fa-save"></i> SAVE</button>
                    <a href="" type="submit" class="btn btn-danger" ><i class="fa fa-chevron-left"></i> BACK</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <script type="text/javascript">
            $(document).ready(function(){
              $(".drop").dropify();
            });
          </script>