<?php
$get_data = mysql_query("SELECT * FROM tb_front_desk, tb_member_individu, master_provinsi, master_kokab, master_kecam
	WHERE
	tb_member_individu.id_member = tb_front_desk.id_member AND
	master_provinsi.provinsi_id = tb_member_individu.province AND
	master_kecam.kecam_id = tb_member_individu.district AND
	master_kokab.kota_id = tb_member_individu.city AND
	tb_front_desk.id_order  = '$inv'");
$data = mysql_query("SELECT * FROM tb_front_desk, tb_member_individu, master_kokab, master_kecam
	WHERE
	tb_member_individu.id_member = tb_front_desk.id_member AND
	master_kecam.kecam_id = tb_member_individu.district AND
	master_kokab.kota_id = tb_member_individu.city AND
	tb_front_desk.id_order  = '$inv'");

	$ce = mysql_fetch_array($get_data);

$pdf = new FPDF();
$pdf->AddPage("P","A4");
/*JUDUL*/
$pdf->SetFont('Arial','B',30);
$pdf->Cell(180,10,'INVOICE',0,0,'C');
/*END*/

/*ALAMAT*/
$pdf->Image('./asset/images/Logo.jpg',142,10,50,20);
$pdf->SetFont('Arial','B',10);
$pdf->SetXY(145, 35);
$pdf->Cell(0,0,'Pegadaian Gemological Laboratory',0,0,'L');
$pdf->SetFont('Arial','',10);
$pdf->SetXY(145, 39);
$pdf->Cell(0,0,'Jl.Kramat Raya 162',0,0,'L');
$pdf->SetXY(145, 43);
$pdf->Cell(0,0,'Jakarta Pusat 10430',0,0,'L');
$pdf->SetXY(145, 47);
$pdf->Cell(0,0,'Indonesia',0,0,'L');
$pdf->SetXY(145, 51);
$pdf->Cell(0,0,'Phone 		 : 	021 315 5550',0,0,'L');
$pdf->SetXY(145, 55);
$pdf->Cell(0,0,'Fax  	 				: 	021 391 4221',0,0,'L');
/*END*/

/*DATA INVOICE*/
$pdf->SetFont('Arial','B',10);

$pdf->SetXY(9, 70);
$pdf->Cell(20,0,'Invoice No.',0,0,'L');

$pdf->SetXY(38, 70);
$pdf->Cell(70,0,': 	211/MIS-INV/V/2016',0,0,'L');

$pdf->SetXY(9, 74);
date_default_timezone_set('Asia/Jakarta');
$tgl = date('d-F-Y');
$pdf->Cell(20,0,'Date',0,0,'L');
$pdf->SetXY(38, 74);
$pdf->Cell(60,0,': 	'.$tgl,0,0,'L');

$pdf->SetXY(9, 78);
$pdf->Cell(20,0,'Kontrak No.',0,0,'L');
$pdf->SetXY(38, 78);
$pdf->Cell(18,0,': 	',0,0,'L');

$pdf->SetXY(9, 82);
$pdf->Cell(20,0,'PO No.',0,0,'L');
$pdf->SetXY(38, 82);
$pdf->Cell(18,0,': 	DO8297971',0,0,'L');

$pdf->SetXY(9, 86);
date_default_timezone_set('Asia/Jakarta');
$tgl = date('d-F-Y');
$pdf->Cell(20,0,'Date',0,0,'L');
$pdf->SetXY(38, 86);
$pdf->Cell(60,0,': 	18-Feb-2016',0,0,'L');

$pdf->SetXY(9, 90);
$pdf->Cell(20,0,'Faktur Pajak.',0,0,'L');
$pdf->SetXY(38, 90);
$pdf->Cell(18,0,': 	010.001-15.28056119',0,0,'L');
/*END*/

/*DATA CUSTOMER*/
if($ce['kode'] == "A"){
$pdf->Rect(100, 68, 100, 25, "D");
$pdf->SetFont('Arial','B',10);

$pdf->SetXY(105, 72);
$pdf->Cell(100,0,'Customer No. : ',0,0,'L');

$pdf->SetXY(180, 72);
$pdf->Cell(20,0,$ce['id_member'],0,0,'L');

$pdf->SetXY(105, 76);
$pdf->Cell(100,0,$ce['first_name']." ".$ce['middle_name']." ".$ce['last_name'],0,0,'L');

$pdf->SetXY(105, 80);
$pdf->Cell(100,0,$ce['address']." ".$ce['nama_kecam'],0,0,'L');

$pdf->SetXY(105, 84);
$pdf->Cell(100,0,$ce['kokab_nama']." ".$ce['provinsi_nama'],0,0,'L');
}else{
$pdf->Rect(100, 68, 100, 25, "D");
$pdf->SetFont('Arial','B',10);

$pdf->SetXY(105, 72);
$pdf->Cell(100,0,'Customer No. : ',0,0,'L');

$pdf->SetXY(180, 72);
$pdf->Cell(20,0,$ce['id_member'],0,0,'L');

$pdf->SetXY(105, 76);
$pdf->Cell(100,0,$ce['corp_name'],0,0,'L');

$pdf->SetXY(105, 80);
$pdf->Cell(100,0,$ce['address']." ".$ce['nama_kecam'],0,0,'L');

$pdf->SetXY(105, 84);
$pdf->Cell(100,0,$ce['kokab_nama']." ".$ce['provinsi_nama'],0,0,'L');	
}
/*END*/

$pdf->Ln(5);
$pdf->Ln(10);$pdf->Ln();$pdf->SetX(10);
$pdf->Cell(20,10,'No',1,0,'C');
$pdf->Cell(80,10,'Description',1,0,'C');
$pdf->Cell(20,10,'QTY',1,0,'C');
$pdf->Cell(30,10,'Unit Price',1,0,'C');
$pdf->Cell(40,10,'Ammount',1,0,'C');
$pdf->Ln();

$no = 1;

$lll = mysql_query("SELECT * FROM tb_service where kode='CE'");
$kx = mysql_fetch_array($lll);

$ll = mysql_query("SELECT * FROM tb_service where kode='GC'");
$ko = mysql_fetch_array($ll);

$l = mysql_query("SELECT * FROM tb_service where kode='DG'");
$k = mysql_fetch_array($l);

$cer = 0;

$mem = 0;

$dim = 0;

$no = 1;

$sub = 0;

$ong = 1;

	while($mo = mysql_fetch_array($data)){
		$pdf->SetX(10);

		$pdf->Cell(20, 7, $no++, 0, 0, 'C');

		$pdf->SetX(35);

		$pdf->Cell(70, 7, 'Speciment '.$ong++, 0, 0, 'L');

		$pdf->Ln();

		if($mo['certificate'] != ''){

		$pdf->SetX(40);

		$pdf->Cell(40, 7, 'Certificate');

		$pdf->SetX(110);

		$pdf->Cell(20, 8, '1', 0, 0, 'C');

		$pdf->SetX(130);

		$pdf->Cell(30, 8, number_format($kx['harga'],'0',',','.'), 0, 0, 'R');

		$pdf->SetX(150);

		$pdf->Cell(45, 8, number_format($kx['harga'],'0',',','.'), 0, 0, 'R');

		$pdf->Ln();

		$sub = $sub+$kx['harga'];

	}else{

		

	}
	
	if($mo['gem_card'] != ''){

		$pdf->SetX(40);

		$pdf->Cell(40, 7, 'Gem Card');

		$pdf->SetX(110);

		$pdf->Cell(20, 8, '1', 0, 0, 'C');

		$pdf->SetX(130);

		$pdf->Cell(30, 8, number_format($ko['harga'],0,',','.'), 0, 0, 'R');

		$pdf->SetX(150);

		$pdf->Cell(45, 8, number_format($ko['harga'],0,',','.'), 0, 0, 'R');

		$pdf->Ln();

		$sub = $sub+$ko['harga'];

	}else{

		

	}

	if($mo['dia_grading'] != ''){

		$pdf->SetX(40);

		$pdf->Cell(40, 7, 'Diamond Grading');

		$pdf->SetX(110);

		$pdf->Cell(20, 8, '1', 0, 0, 'C');

		$pdf->SetX(130);

		$pdf->Cell(30, 8, number_format($k['harga'],'0',',','.'), 0, 0, 'R');

		$pdf->SetX(150);

		$pdf->Cell(45, 8, number_format($k['harga'],'0',',','.'), 0, 0, 'R');

		$pdf->Ln();

		$sub = $sub+$k['harga'];

	}else{

		

	}


	}
$pdf->Ln();
$pdf->Ln();
$pdf->Rect(10,98.6, 190, 90, "D");
$pdf->Ln();
$pdf->Ln();

$pdf->SetXY(15, 200);
		$ppn = $sub*10/100;
		$total = $sub+$ppn;
		$pdf->SetX(130);
		$pdf->Cell(30,0,'Sub Total',0,0,'L');
		$pdf->Cell(40,0,number_format($sub,'0',',','.'),0,0,'R');
		$pdf->Ln(5);
		$pdf->SetX(130);
		$pdf->Cell(30,0,'VAT 10%',0,0,'L');
		$pdf->Cell(40,0,number_format($ppn,'0',',','.'),0,0,'R');
		$pdf->Ln(5);
		$pdf->SetX(130);
		$pdf->Cell(30,0,'TOTAL',0,0,'L');
		$pdf->Cell(40,0,number_format($total,'0',',','.'),0,0,'R');

/*include "terbilang.php";
$terbilang = new Terbilang($total); 

$pdf->Ln(5);
$pdf->SetX(10);
$pdf->Cell(10,10,'Said : ',0,0,'C');
$pdf->SetX(30);
$pdf->Cell(170,10,$terbilang,1,0,'C');*/

$pdf->Ln(13);
$pdf->SetX(10);
$pdf->Cell(10,10,'Note : ',0,0,'C');
$pdf->SetX(30);
$pdf->Cell(90,10,'Please send payment by T/T to our account',0,0,'L');
$pdf->Ln(5);
$pdf->SetX(30);
$pdf->Cell(90,10,'Pegadaian Gemological Laboratory',0,0,'L');
$pdf->Ln(5);
$pdf->SetX(30);
$pdf->Cell(90,10,'Jl.Kramat Raya 162',0,0,'L');
$pdf->Ln(5);
$pdf->SetX(30);
$pdf->Cell(90,10,'Jakarta Pusat 10430',0,0,'L');

$pdf->Output();
?>
?>