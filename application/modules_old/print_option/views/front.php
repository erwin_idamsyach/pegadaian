      <?php
      $get_option = $this->db->query("SELECT * FROM param_opsi_print");
      foreach ($get_option->result() as $get) {
        $ge = $get->opsi_print;
      }
      ?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>&nbsp;</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-user"></i> Parameter</a></li>
            <li class="active">Print Option</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="box">
            <div class="box-header">
              <b>SET PRINT OPTION</b>
              <div style="border:1px solid black;margin-bottom:0px;"></div>
            </div>
            <div class="box-body">
            <form action="<?php echo site_url('print_option/change_option') ?>" method="post">
            <div class="row">
              <div class="col-md-4 col-sm-6">
                <div class="form-group">
                    <label>Print Address Store :</label>
                    <div class="radio-inline" style="padding-top: 2px">
                      <label><input type="radio" name="option" <?php echo ($ge=="PUSAT")?"checked='true'":"" ?> value="PUSAT"> CENTERIZE</label>
                    </div>
                    <div class="radio-inline" style="padding-top: 2px">
                      <label><input type="radio" name="option" <?php echo ($ge=="CABANG")?"checked='true'":"" ?> value="CABANG"> BRANCH</label>
                    </div>
                  </div>
                </div>
                <div class="col-md-8 col-sm-6">
                  <div class="pull-left">
                    <button type="submit" class="btn btn-primary">CHANGE</button>&nbsp;&nbsp;
                    CURRENTLY ACTIVE : <b><?php echo $ge ?></b>
                  </div>
                </div>
              </div>
            </form>
            </div>
          </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->