<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
* Controller Print Diamond Grading
* Create By : Erwin Idamsyach Putra
* 23 April 2016
*/
class Print_grading_done extends CI_Controller{
	public function index(){
		$data['filelist'] = 'print_grading_done/front';
		$data['title'] = 'Finished Print Diamond Grading';
		$data['title_menu'] = 'Print';
		$data['menu'] = 'print-grading-done';

		getHTMLWeb($data);
	}

	public function print_grading(){
		$this->load->library('fpdf');
		$id_obj = $this->uri->segment(3);

		$get_data = $this->db->query("SELECT * FROM step, tb_lab_grading, diamond_color, diamond_clarity 
			WHERE 
			step.id_object = tb_lab_grading.id_object AND
			tb_lab_grading.color_grading = diamond_color.color_code AND
			tb_lab_grading.clarity = diamond_clarity.clarity_grade AND
			step.dia_grading != '' AND
			step.print_grading = 'Sudah' AND
			tb_lab_grading.id_object = '$id_obj'");
		$this->db->where('id_object', $id_obj);
		$this->db->update('step', array("print_grading"=>'Sudah'));
		$pdf = new FPDF('L','mm','A4');
		foreach ($get_data->result() as $get) {

			$date = date_create($get->create_date);
			$date = date_format($date, 'D, d-m-Y');

		$pdf->AddPage();
		$pdf->Image('asset/logo-pegadaian/Logo.png', 10, 5, 60, 20);
		$pdf->Image('asset/logo-pegadaian/logo-png-1.png', 225, 13, 50, 10);

		$pdf->SetDrawColor(130, 186, 83);
		$pdf->SetLineWidth(1);
		$pdf->Line(10, 33, 37, 33);

		$pdf->SetFont('Helvetica','B',16);
		$pdf->SetTextColor(43, 75, 14);

		$pdf->SetXY(37, 32);
		$pdf->Cell(0, 0, 'Diamond Grading Report');

		$pdf->Line(106, 33, 150, 33);
		$pdf->SetDrawColor(0, 0, 0);
		$pdf->Line(150, 33, 285, 33);

		$pdf->SetFont('Helvetica','',13);
		$pdf->SetTextColor(0, 0, 0);

		$pdf->SetXY(10, 50);
		$pdf->Cell(0, 0, 'Report No');
		$pdf->SetXY(50, 50);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(55, 50);
		$pdf->Cell(0, 0, $get->id_object);

		$pdf->SetXY(10, 62);
		$pdf->Cell(0, 0, 'Date');
		$pdf->SetXY(50, 62);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(55, 62);
		$pdf->Cell(0, 0, $date);

		$pdf->SetXY(10, 74);
		$pdf->Cell(0, 0, 'Shape & Cutting');
		$pdf->SetXY(50, 74);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(55, 74);
		$pdf->Cell(0, 0, $get->shape." ".$get->cut);

		$pdf->SetXY(10, 86);
		$pdf->Cell(0, 0, 'Measurements');
		$pdf->SetXY(50, 86);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(55, 86);
		$pdf->Cell(0, 0, $get->obj_length." x ".$get->obj_width." x ".$get->obj_height." mm");

		$pdf->SetFont('Helvetica','B',13);
		$pdf->SetXY(10, 98);
		$pdf->Cell(0, 0, 'Carat Weight');
		$pdf->SetXY(50, 98);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(55, 98);
		$pdf->Cell(0, 0, $get->obj_weight." cts");

		$pdf->SetXY(10, 110);
		$pdf->Cell(0, 0, 'Color');
		$pdf->SetXY(50, 110);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(55, 110);
		$pdf->Cell(0, 0, $get->color_grading." (".$get->color_name.")");

		$pdf->SetXY(10, 122);
		$pdf->Cell(0, 0, 'Clarity');
		$pdf->SetXY(50, 122);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(55, 122);
		$pdf->Cell(0, 0, $get->clarity." (".$get->inform.")");

		$pdf->SetXY(10, 134);
		$pdf->Cell(0, 0, 'Cut Grade');
		$pdf->SetXY(50, 134);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(55, 134);
		$pdf->Cell(0, 0, $get->cut_grade);

		$pdf->SetFont('Helvetica','',13);
		$pdf->SetXY(10, 146);
		$pdf->Cell(0, 0, 'Girdle');
		$pdf->SetXY(50, 146);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(55, 146);
		$pdf->Cell(0, 0, $get->girdle);

		$pdf->SetXY(10, 158);
		$pdf->Cell(0, 0, 'Culet');
		$pdf->SetXY(50, 158);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(55, 158);
		$pdf->Cell(0, 0, $get->culet);

		$pdf->SetXY(10, 170);
		$pdf->Cell(0, 0, 'Fluorescence');
		$pdf->SetXY(50, 170);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(55, 170);
		$pdf->Cell(0, 0, $get->fluorescence);

		$pdf->SetXY(10, 182);
		$pdf->Cell(0, 0, 'Key to Symbol(s)');
		$pdf->SetXY(50, 182);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(55, 182);
		$pdf->Cell(0, 0, $get->key);

		$pdf->Image('asset/images/'.$get->obj_image, 200, 40, 50, 55);
		$pdf->SetFont('Helvetica','',7);
		$pdf->SetXY(210, 90);
		$pdf->Cell(0, 0, 'Photo not the actual size');

		$pdf->Image('asset/diamond-cut-sketch/'.$get->shape.'.jpg', 180, 95, 90, 60);
		}
		$pdf->Output();
	}

	public function print_multiple_grading(){
		$this->load->library('fpdf');
		$id_order = $this->uri->segment(3);

		$get_data = $this->db->query("SELECT * FROM step, tb_lab_grading, diamond_color, diamond_clarity 
			WHERE 
			step.id_order = tb_lab_grading.id_order AND
			tb_lab_grading.color_grading = diamond_color.color_code AND
			tb_lab_grading.clarity = diamond_clarity.clarity_grade AND
			step.dia_grading != '' AND
			step.print_grading = 'Sudah' AND
			tb_lab_grading.id_order = '$id_order'");
		$this->db->where('id_order', $id_order);
		$this->db->update('step', array("print_grading"=>'Sudah'));
		$pdf = new FPDF('L','mm','A4');
		foreach ($get_data->result() as $get) {

			$date = date_create($get->create_date);
			$date = date_format($date, 'D, d-m-Y');

		$pdf->AddPage();
		$pdf->Image('asset/logo-pegadaian/Logo.png', 10, 5, 60, 20);
		$pdf->Image('asset/logo-pegadaian/logo-png-1.png', 225, 13, 50, 10);

		$pdf->SetDrawColor(130, 186, 83);
		$pdf->SetLineWidth(1);
		$pdf->Line(10, 33, 37, 33);

		$pdf->SetFont('Helvetica','B',16);
		$pdf->SetTextColor(43, 75, 14);

		$pdf->SetXY(37, 32);
		$pdf->Cell(0, 0, 'Diamond Grading Report');

		$pdf->Line(106, 33, 150, 33);
		$pdf->SetDrawColor(0, 0, 0);
		$pdf->Line(150, 33, 285, 33);

		$pdf->SetFont('Helvetica','',13);
		$pdf->SetTextColor(0, 0, 0);

		$pdf->SetXY(10, 50);
		$pdf->Cell(0, 0, 'Report No');
		$pdf->SetXY(50, 50);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(55, 50);
		$pdf->Cell(0, 0, $get->id_object);

		$pdf->SetXY(10, 62);
		$pdf->Cell(0, 0, 'Date');
		$pdf->SetXY(50, 62);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(55, 62);
		$pdf->Cell(0, 0, $date);

		$pdf->SetXY(10, 74);
		$pdf->Cell(0, 0, 'Shape & Cutting');
		$pdf->SetXY(50, 74);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(55, 74);
		$pdf->Cell(0, 0, $get->shape." ".$get->cut);

		$pdf->SetXY(10, 86);
		$pdf->Cell(0, 0, 'Measurements');
		$pdf->SetXY(50, 86);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(55, 86);
		$pdf->Cell(0, 0, $get->obj_length." x ".$get->obj_width." x ".$get->obj_height." mm");

		$pdf->SetFont('Helvetica','B',13);
		$pdf->SetXY(10, 98);
		$pdf->Cell(0, 0, 'Carat Weight');
		$pdf->SetXY(50, 98);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(55, 98);
		$pdf->Cell(0, 0, $get->obj_weight." cts");

		$pdf->SetXY(10, 110);
		$pdf->Cell(0, 0, 'Color');
		$pdf->SetXY(50, 110);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(55, 110);
		$pdf->Cell(0, 0, $get->color_grading." (".$get->color_name.")");

		$pdf->SetXY(10, 122);
		$pdf->Cell(0, 0, 'Clarity');
		$pdf->SetXY(50, 122);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(55, 122);
		$pdf->Cell(0, 0, $get->clarity." (".$get->inform.")");

		$pdf->SetXY(10, 134);
		$pdf->Cell(0, 0, 'Cut Grade');
		$pdf->SetXY(50, 134);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(55, 134);
		$pdf->Cell(0, 0, $get->cut_grade);

		$pdf->SetFont('Helvetica','',13);
		$pdf->SetXY(10, 146);
		$pdf->Cell(0, 0, 'Girdle');
		$pdf->SetXY(50, 146);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(55, 146);
		$pdf->Cell(0, 0, $get->girdle);

		$pdf->SetXY(10, 158);
		$pdf->Cell(0, 0, 'Culet');
		$pdf->SetXY(50, 158);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(55, 158);
		$pdf->Cell(0, 0, $get->culet);

		$pdf->SetXY(10, 170);
		$pdf->Cell(0, 0, 'Fluorescence');
		$pdf->SetXY(50, 170);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(55, 170);
		$pdf->Cell(0, 0, $get->fluorescence);

		$pdf->SetXY(10, 182);
		$pdf->Cell(0, 0, 'Key to Symbol(s)');
		$pdf->SetXY(50, 182);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(55, 182);
		$pdf->Cell(0, 0, $get->key);

		$pdf->Image('asset/images/'.$get->obj_image, 200, 40, 50, 55);
		$pdf->SetFont('Helvetica','',7);
		$pdf->SetXY(210, 90);
		$pdf->Cell(0, 0, 'Photo not the actual size');

		$pdf->Image('asset/diamond-cut-sketch/'.$get->shape.'.jpg', 180, 95, 90, 60);
		}
		$pdf->Output();
	}
}
?>