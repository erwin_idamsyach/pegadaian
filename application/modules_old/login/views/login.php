<div class="container">
    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
            <div class="account-wall" style="background-color: #1E1E1E;;box-shadow: 0 5px 9px rgba(0, 0, 0, 0.32)">
                <div id="my-tab-content" class="tab-content">
                    <div class="tab-pane active">
                        <div class="ojo">
                        <center>
                            <img src="<?php echo base_url();?>asset/images/LOGO-COG-FINAL.png" height="130px" style="padding: 0;">
                        </center>
                        <center><h3><font face="Calibri" style="text-transform: uppercase; font-weight: bold; color: #fff;">COG LAB</font></h3></center>
                        </div>
                        <form class="form-signin" id="form-login" action="<?php echo site_url('login/submitLogin');?>" method="POST">
                            <input type="text" id="username" class="form-control" placeholder="Username" required autofocus>
                            <input type="password" id="password" class="form-control" placeholder="Password" required style="margin-top:10px;">
                            <select name="store_op" id="store" class="form-control" style="margin-top:5px;margin-bottom:10px;">
                                <option value="004">--Select Area--</option>
                                <?php
                                $get_store = $this->db->query("SELECT * FROM tb_store where kode!='004' AND delete_by =''");
                                foreach ($get_store->result() as $sto) {
                                    ?>
                                <option value="<?php echo $sto->kode ?>"><?php echo $sto->store ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                            <div id="pesan"></div>
                            <input type="submit" id="login" class="btn btn-lg btn-default btn-block" value="Sign In"  style="margin-top:10px;" />
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>