<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Login extends CI_Controller{
    function __construct(){
		parent::__construct();

		if(isLogin()){
			redirect('home');
		}
	}

    public function index()
	{ 
		
	}
	/* Edited by Erwin Idamsyach, @April 21, 2016*/
	function submitLogin(){
		if($_POST['store'] == ''){
			$store = '';
		}else{
			$store = $_POST['store'];
		}

        $sql = "SELECT
					tb_login.*, tb_store.store, tb_store.location, tb_store.alamat, tb_store.telepon, tb_store.fax, tb_store.kode_unit, tb_store.flag
				FROM
					tb_login, tb_store
                WHERE tb_login.store = tb_store.kode AND username='".$_POST['username']."' AND password=MD5('".$_POST['password']."') AND tb_login.store='$store' AND tb_login.delete_by='' LIMIT 0,1";
		$rows = $this->db->query($sql);
		
		$msg = "";
		if($rows->num_rows()>0){
			$query = "SELECT
					tb_login.*, tb_store.store, tb_store.location
				FROM
					tb_login, tb_store
                WHERE tb_login.store = tb_store.kode AND username='".$_POST['username']."' AND password=MD5('".$_POST['password']."') AND tb_store.kode='$store' AND tb_login.delete_by='' LIMIT 0,1";
            $que   = $this->db->query($query);
			$data["filelist"] = "home/home";
			$data["title"] = "ASPERINDO";
			$data["menu"] = "home";
			
			$getData = $rows->result_array();
			$getData = $getData[0];
			$arrayUserData = array(
					"id" 			=> $getData["id"],
					"username" 		=> $getData["username"],
					"password" 		=> $getData["password"],
					"nama" 			=> $getData["nama"],
					"access_level" 	=> $getData["access_level"],
					"last_login" 	=> $getData["last_login"],
					"nama_store"    => $getData["store"],
					"kode_store"    => $store,
					"lokasi"    => $getData["location"],
					"alamat"    => $getData["alamat"],
					"telepon"    => $getData["telepon"],
					"fax"    => $getData["fax"],
					"kode_unit"    => $getData["kode_unit"],
					"flag_store"    => $getData["flag"]
			);
			setSession($arrayUserData);
			
			echo json_encode(array("success"=>true));
			
		}else{
			$cek_store = $this->db->query("SELECT 
				*
				FROM tb_login 
				WHERE 
				username='".$_POST['username']."' AND 
				password=MD5('".$_POST['password']."') AND
				store != '".$_POST['store']."'");
			if($cek_store->num_rows()>0){
				echo json_encode(array("wrong_store"=>true));
			}else{
				echo json_encode(array("success"=>false, "sql"=>$sql));
			}
		}
    }
}