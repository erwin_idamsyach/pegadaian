<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
* Controller Master Agama
* Create By : Hadi Setiawan
* 11 Mei 2016
*/
class gemstone extends CI_Controller{
	public $table = "master_all";
	public $controls = "gemstone";
	
	function __construct(){
		parent::__construct();

		if(!isLogin()){
			goLogin();
		}
	}
	
	public function index(){
		$data['filelist'] = $this->controls.'/front';
		$data['title'] = 'Master Lab Desk';
		$data['title_menu'] = 'Admin';
		$data['menu'] = 'gemstone';
		$data['controls'] = $this->controls;
		$data['table'] = $this->table;

		getHTMLWeb($data);
	}

	public function addData(){
		$name = $this->input->post('nama');
		$ri = $this->input->post('ri');
		$ri2 = $this->input->post('ri2');
		$sg = $this->input->post('sg');
		$sg2 = $this->input->post('sg2');
		$hardness = $this->input->post('hardness');
		$hardness2 = $this->input->post('hardness2');

		$data = array(
			'ri_start' => $ri,
			'ri_end' => $ri2,
			'sg_start' => $sg,
			'sg_end' => $sg2,
			'hard_start' => $hardness,
			'hard_end' => $hardness2,
			'gemstone' => $name,
			'create_by' => sessionValue('id'),
			'create_date' => date('Y-m-d H:i:s')	
			);
		$this->db->insert($this->table, $data);
		redirect(site_url($this->controls));
	}

	public function editData(){
		$id  = $this->uri->segment(3);

		$data['filelist'] = $this->controls.'/front';
		$data['title'] = 'Master Lab Desk';
		$data['title_menu'] = 'Admin';
		$data['menu'] = 'gemstone';		
		$data['controls'] = $this->controls;
		$data['table'] = $this->table;
		
		$data['id'] = $id;

		getHTMLWeb($data);
	}

	public function updateData(){
		$id   = $this->input->post('id');
		$name = $this->input->post('nama');
		$ri = $this->input->post('ri');
		$ri2 = $this->input->post('ri2');
		$sg = $this->input->post('sg');
		$sg2 = $this->input->post('sg2');
		$hardness = $this->input->post('hardness');
		$hardness2 = $this->input->post('hardness2');

		$data = array(
			'ri_start' => $ri,
			'ri_end' => $ri2,
			'sg_start' => $sg,
			'sg_end' => $sg2,
			'hard_start' => $hardness,
			'hard_end' => $hardness2,
			'gemstone' => $name,
			'update_by' => sessionValue('id'),
			'update_date' => date('Y-m-d H:i:s')
			);

		$this->db->where('id', $id);
		$this->db->update($this->table, $data);
		redirect(site_url($this->controls));
	}

	public function deleteData(){
		$id = $this->uri->segment(3);

		$data = array(
			'delete_by' => sessionValue('id'),
			'delete_date' => date('Y-m-d H:i:s')
			);
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);
		redirect(site_url($this->controls));
	}
}
?>