<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
* Controller Profile
* Create By : Erwin Idamsyach Putra
* 23 April 2016
*/
class profile extends CI_Controller{
	function __construct(){
		parent::__construct();

		if(!isLogin()){
			goLogin();
		}
	}
	
	public function index(){
		$data['filelist'] = 'profile/front';
		$data['title'] = 'Profile';
		$data['menu'] = 'profile';
		$get_access = $this->db->query("SELECT * FROM param_access");
			foreach ($get_access->result() as $acc) {
				# code...
				if (sessionValue('access_level') == $acc->access_level) {
					$data["title_menu"] = $acc->access_call;
				}
			}
		getHTMLWeb($data);
	}
	
	function edit(){
		$name   = $this->input->post('name');
        $old_password   = $this->input->post('old_password');
        $new_password   = $this->input->post('new_password');
        $email   = $this->input->post('email');
        $phone   = $this->input->post('phone');
		
		$sql = $this->db->query("SELECT * FROM tb_login where id = '".checkSession('id')."'");
		$sqlPassword = "";
		foreach ($sql->result() as $tmp) {
			$sqlPassword = $tmp->password;
		}
		
		$success = false;
		$msg = "";
		if($new_password==""){
			$data = array(
				'nama'    => $name,
				'phone'         => $phone,
				'email'         => $email,
			);
			$this->db->where('id', checkSession('id'));
			if($this->db->update('tb_login', $data)){
				$success = true;
			}else{
				$msg = "Error Edit Data.";
			}
		}else{
			if($sqlPassword == md5($old_password)){
				$data = array(
					'nama'    => $name,
					'password'    => md5($new_password),
					'phone'         => $phone,
					'email'         => $email,
				);
				$this->db->where('id', checkSession('id'));
				if($this->db->update('tb_login', $data)){
					$success = true;
				}else{
					$msg = "Error Edit Data.";
				}
			}else{
				$msg = "Old Password tidak sesuai";
			}
		}
		
		echo json_encode(array("success"=>$success, "msg"=>$msg));
		/*$data['filelist'] = 'profile/front';
		$data['title'] = 'Profile';
		$data['menu'] = 'profile';
		$get_access = $this->db->query("SELECT * FROM param_access");
			foreach ($get_access->result() as $acc) {
				if (sessionValue('access_level') == $acc->access_level) {
					$data["title_menu"] = $acc->access_call;
				}
			}
		getHTMLWeb($data);*/
	}
	
	public function changePassword(){
		$data['filelist'] = 'profile/front';
		$data['title'] = 'Profile';
		$data['menu'] = 'profile';
		$get_access = $this->db->query("SELECT * FROM param_access");
			foreach ($get_access->result() as $acc) {
				# code...
				if (sessionValue('access_level') == $acc->access_level) {
					$data["title_menu"] = $acc->access_call;
				}
			}
		$this->load->view('profile/change_password', $data);
	}
	
	function saveChangePassword(){
        $old_password   = $this->input->post('change_old_password');
        $new_password   = $this->input->post('change_new_password');
		
		$sql = $this->db->query("SELECT * FROM tb_login where id = '".checkSession('id')."'");
		$sqlPassword = "";
		foreach ($sql->result() as $tmp) {
			$sqlPassword = $tmp->password;
		}
		
		$success = false;
		$msg = "";
		
		if($sqlPassword == md5($old_password)){
			$data = array(
				'password'    => md5($new_password)
			);
			$this->db->where('id', checkSession('id'));
			if($this->db->update('tb_login', $data)){
				$success = true;
			}else{
				$msg = "Error Edit Data.";
			}
		}else{
			$msg = "Old Password tidak sesuai";
		}
		
		echo json_encode(array("success"=>$success, "msg"=>$msg));
	}
}
?>