	<script type="text/javascript">
		$(document).ready(function(){
			$('#form-change-password').trigger("reset");
			$("#form-change-password").on('submit',function(e){
			e.preventDefault();
			var formatData = new FormData($(this)[0]);
				swal({
				 title: "",
				 text: "Apakah Anda akan menyimpan data?",
				 type: "warning",
				 showCancelButton: true,
				 confirmButtonColor: "#DD6B55",
				 confirmButtonText: "Ya",
				 cancelButtonText: "Tidak",
				 closeOnConfirm: false }, function(){
					$.ajax({
						type : 'POST',
						url : base_url+'profile/saveChangePassword',
						data : formatData,
						async: false,
						cache: false,
						contentType: false,
						processData: false,
						success:function(html){
							var data = eval ("(" + html + ")");
							if(data.success){
								swal({
								 title: "Berhasil Menyimpan data!",
								 text: "Klik tombol di bawah.",
								 type: "success",
								 showCancelButton: false,
								 confirmButtonColor: "#257DB6",
								 confirmButtonText: "Ok!",
								 closeOnConfirm: false
								},
								function(){
									swal.close();
									$('#modalChangePassword').modal('hide');
								});
							}else{
								swal({
								 title: data.msg,
								 text: "Klik tombol di bawah.",
								 type: "warning",
								 showCancelButton: false,
								 confirmButtonColor: "#257DB6",
								 confirmButtonText: "Ok!",
								 closeOnConfirm: false
								});
							}
						}
					});
				});
			});
		});
	</script>
    <!-- Content Wrapper. Contains page content -->

        <!-- Main content -->
        <section class="content">
          <div class="index">
            <div class="box">
              <div class="box-header">
                <b>Change Password</b>
                <div style="border:1px solid black;margin-bottom:0px;"></div>
              </div>
              <div class="box-body">
					<form id="form-change-password" method="post" enctype="multipart/form-data">
					<div class="row">		
						<div class="col-md-12">
						  <div class="form-group">
							<label>Old Password <font color="red">*</font></label>
							<input type="password" name="change_old_password" id="change_old_password" class="form-control" placeholder="New Password" >
						  </div>
						</div>	
						<div class="col-md-12">
						  <div class="form-group">
							<label>New Password <font color="red">*</font></label>
							<input type="password" name="change_new_password" id="change_new_password" class="form-control" placeholder="Old Password" >
						  </div>
						</div>
					</div>
					<div class="row">	
						<div class="col-md-12" style="margin-top:20px;">
							<button type="submit" class="btn btn-default"><i class="fa fa-save"></i> SIMPAN</button>
							<button type="button" class="btn btn-default"  data-dismiss="modal"><i class="fa fa-refresh"></i> Close</button>
						</div>
					</div>
					</form>
              </div>
            </div>
          </div>

        </section><!-- /.content -->
      <!-- /.content-wrapper -->