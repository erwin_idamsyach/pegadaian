<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class listcif extends CI_Controller{
    function listcif(){
        parent::__construct();
		
		if(!isLogin()){
			goLogin();
		}
        $this->load->library('fpdf');
        $this->load->library('Barcode39');
    }
    
    function index($text=NULL){
        $data["filelist"] = "listcif/listcif";
        $data["title"] = "Front";
        $data["title_menu"] = "Front";
        $data["menu"] = "listcif";
		
        getHTMLWeb($data);
    }

    public function kota($id)
    {
        $this->load->view('get_kota', array('id'=>$id));
    }

    public function kecam($id)
    {
        $this->load->view('get_keca', array('id'=>$id));
    }

    public function viewCif($id)
    {
		$data["filelist"] = "listcif/editcif";
        $data["title"] = "Front";
        $data["title_menu"] = "Front";
        $data["menu"] = "listcif";
        $data["id"] = $id;
		
		$sql = $this->db->query("SELECT tb_member.*, tb_store.store, master_gelar.gelar, master_agama.agama, master_identitas.identitas,
								master_jenis_kelamin.jenis_kelamin, master_pendidikan.pendidikan, master_perkawinan.perkawinan, master_kewarganegaraan.kewarganegaraan,
								master_sumber_dana.sumber_dana, master_penghasilan.penghasilan, master_pekerjaan.pekerjaan,
								kecamatan.kecamatanNama kec1, kabupaten.kabupatenNama kota1, provinsi.provinsiNama prov1,
								master_status_tempat_tinggal.status_tempat_tinggal,
								master_kecam2.kecamatanNama kec2, master_kokab2.kabupatenNama kota2, master_provinsi2.provinsiNama prov2,
								master_alamat_surat_menyurat.alamat_surat_menyurat, master_produk.produk
									FROM tb_member
										LEFT JOIN provinsi ON provinsi.provinsiId = tb_member.province
										LEFT JOIN kabupaten ON kabupaten.kabupatenId = tb_member.city
										LEFT JOIN kecamatan ON kecamatan.kecamatanId = tb_member.district 
										LEFT JOIN kecamatan master_kecam2 ON master_kecam2.kecamatanId = tb_member.district2
										LEFT JOIN kabupaten master_kokab2 ON kabupaten.kabupatenId = tb_member.city2
										LEFT JOIN provinsi master_provinsi2 ON master_provinsi2.provinsiId = tb_member.province2
										LEFT JOIN tb_store ON tb_store.kode = tb_member.cabang
										LEFT JOIN master_gelar ON master_gelar.id = tb_member.gelar
										LEFT JOIN master_agama ON master_agama.id = tb_member.agama
										LEFT JOIN master_identitas ON master_identitas.id = tb_member.identitas
										LEFT JOIN master_jenis_kelamin ON master_jenis_kelamin.id = tb_member.jenis_kelamin
										LEFT JOIN master_pendidikan ON master_pendidikan.id = tb_member.pendidikan
										LEFT JOIN master_perkawinan ON master_perkawinan.id = tb_member.perkawinan
										LEFT JOIN master_kewarganegaraan ON master_kewarganegaraan.id = tb_member.kewarganegaraan
										LEFT JOIN master_sumber_dana ON master_sumber_dana.id = tb_member.sumber_dana
										LEFT JOIN master_penghasilan ON master_penghasilan.id = tb_member.penghasilan
										LEFT JOIN master_pekerjaan ON master_pekerjaan.id = tb_member.pekerjaan
										LEFT JOIN master_status_tempat_tinggal ON master_status_tempat_tinggal.id = tb_member.status_tempat_tinggal
										LEFT JOIN master_alamat_surat_menyurat ON master_alamat_surat_menyurat.id = tb_member.alamat_surat_menyurat
										LEFT JOIN master_produk ON master_produk.id = tb_member.produk
                                    WHERE tb_member.kode='A' AND ISNULL(tb_member.delete_by) AND tb_member.id_member='".$id."'");
		$tmp = $sql->result_array();
		$data["tmp"] = $tmp[0];
		
		$sqlKota = $this->db->query("SELECT * FROM master_kokab a
                                        WHERE a.kota_id='".$tmp[0]['city']."'");
		$data["kota"] = "";
		foreach($sqlKota->result_array() as $tmpKota){
			$data["kota"] = $tmpKota["kokab_nama"];
		}

		$sqlKota2 = $this->db->query("SELECT * FROM master_kokab a
                                        WHERE a.kota_id='".$tmp[0]['city2']."'");
		$data["kota2"] = "";
		foreach($sqlKota2->result_array() as $tmpKota2){
			$data["kota2"] = $tmpKota2["kokab_nama"];
		}
		
		$sqlKecamatan = $this->db->query("SELECT * FROM master_kecam a
                                        WHERE a.kecam_id='".$tmp[0]['district']."'");
		$data["kecamatan"] = "";
		foreach($sqlKecamatan->result_array() as $tmpKecamatan){
			$data["kecamatan"] = $tmpKecamatan["nama_kecam"];
		}
		
		$sqlKecamatan2 = $this->db->query("SELECT * FROM master_kecam a
                                        WHERE a.kecam_id='".$tmp[0]['district2']."'");
		$data["kecamatan2"] = "";
		foreach($sqlKecamatan2->result_array() as $tmpKecamatan2){
			$data["kecamatan2"] = $tmpKecamatan2["nama_kecam"];
		}
		
        $this->load->view('listcif/viewcif', $data);
    }
	
	public function viewCifCor($id)
    {
		$data["filelist"] = "listcif/editcif";
        $data["title"] = "Front";
        $data["title_menu"] = "Front";
        $data["menu"] = "listcif";
        $data["id"] = $id;
		
		$sql = $this->db->query("SELECT tb_member.*,
								master_kecam.nama_kecam kec1, master_kokab.kokab_nama kota1, master_provinsi.provinsi_nama prov1,
								master_bentuk_perusahaan.bentuk_perusahaan
									FROM 
								tb_member 
								LEFT JOIN master_kecam ON master_kecam.kecam_id = tb_member.district
								LEFT JOIN master_kokab ON master_kokab.kota_id = tb_member.city
								LEFT JOIN master_provinsi ON master_provinsi.provinsi_id = tb_member.province
								LEFT JOIN master_bentuk_perusahaan ON master_bentuk_perusahaan.id = tb_member.corp_form
                                        WHERE tb_member.kode='B' AND ISNULL(tb_member.delete_by) AND tb_member.id_member='".$id."'");
		$tmp = $sql->result_array();
		$data["tmp"] = $tmp[0];
		
		$sqlKota = $this->db->query("SELECT * FROM master_kokab a
                                        WHERE a.kota_id='".$tmp[0]['city']."'");
		$data["kota"] = "";
		foreach($sqlKota->result_array() as $tmpKota){
			$data["kota"] = $tmpKota["kokab_nama"];
		}
		
		$sqlKecamatan = $this->db->query("SELECT * FROM master_kecam a
                                        WHERE a.kecam_id='".$tmp[0]['district']."'");
		$data["kecamatan"] = "";
		foreach($sqlKecamatan->result_array() as $tmpKecamatan){
			$data["kecamatan"] = $tmpKecamatan["nama_kecam"];
		}
		
        $this->load->view('listcif/viewcifcor', $data);
    }

    public function editCif($id)
    {
		$data["filelist"] = "listcif/editcif";
        $data["title"] = "Front";
        $data["title_menu"] = "Front";
        $data["menu"] = "listcif";
        $data["id"] = $id;
		
		$sql = $this->db->query("SELECT a.* FROM tb_member a
										LEFT JOIN master_provinsi b ON b.provinsi_id = a.province
										LEFT JOIN master_kokab c ON c.kota_id = a.city
										LEFT JOIN master_kecam d ON d.kecam_id = a. district 
                                        WHERE a.kode='A' AND ISNULL(delete_by) AND id_member='".$id."'");
		$tmp = $sql->result_array();
		$data["tmp"] = $tmp[0];
		
		$sqlKota = $this->db->query("SELECT * FROM kabupaten a
                                        WHERE a.kabupatenId='".$tmp[0]['city']."'");
		$data["kota"] = "";
		foreach($sqlKota->result_array() as $tmpKota){
			$data["kota"] = $tmpKota["kabupatenNama"];
		}

		$sqlKota2 = $this->db->query("SELECT * FROM kabupaten a
                                        WHERE a.kabupatenId='".$tmp[0]['city2']."'");
		$data["kota2"] = "";
		foreach($sqlKota2->result_array() as $tmpKota2){
			$data["kota2"] = $tmpKota2["kabupatenNama"];
		}
		
		$sqlKecamatan = $this->db->query("SELECT * FROM kecamatan a
                                        WHERE a.kecamatanId='".$tmp[0]['district']."'");
		$data["kecamatan"] = "";
		foreach($sqlKecamatan->result_array() as $tmpKecamatan){
			$data["kecamatan"] = $tmpKecamatan["kecamatanNama"];
		}
		
		$sqlKecamatan2 = $this->db->query("SELECT * FROM kecamatan a
                                        WHERE a.kecamatanId='".$tmp[0]['district2']."'");
		$data["kecamatan2"] = "";
		foreach($sqlKecamatan2->result_array() as $tmpKecamatan2){
			$data["kecamatan2"] = $tmpKecamatan2["kecamatanNama"];
		}
		
		$sqlKelurahan = $this->db->query("SELECT * FROM desa a
                                        WHERE a.desaId='".$tmp[0]['kel']."'");
		$data["kelurahan"] = "";
		foreach($sqlKelurahan->result_array() as $tmpKelurahan){
			$data["kelurahan"] = $tmpKelurahan["desaNama"];
		}
		
		$sqlKelurahan = $this->db->query("SELECT * FROM desa a
                                        WHERE a.desaId='".$tmp[0]['kel2']."'");
		$data["kelurahan2"] = "";
		foreach($sqlKelurahan->result_array() as $tmpKelurahan){
			$data["kelurahan2"] = $tmpKelurahan["desaNama"];
		}
		
        getHTMLWeb($data);
    }
	
	public function edit_cif_individu()
    {
        $no_cif = $this->input->post('no_cif');
        //$cabang = $this->input->post('cabang');
        $nama_depan = $this->input->post('nama_depan');
        //$nama_tengah = $this->input->post('nama_tengah');
        //$nama_belakang = $this->input->post('nama_belakang');
        $nama_ibu = $this->input->post('nama_ibu');
        //$gelar = $this->input->post('gelar');
        $tempat_lahir = $this->input->post('tempat_lahir');
        $tanggal_lahir = $this->input->post('tanggal_lahir');
        $agama = $this->input->post('agama');
        $identitas = $this->input->post('identitas');
        $no_identitas = $this->input->post('no_identitas');
        $masa_berlaku = $this->input->post('masa_berlaku');
        $jenis_kelamin = $this->input->post('jenis_kelamin');
        $pendidikan = $this->input->post('pendidikan');
        $perkawinan = $this->input->post('perkawinan');
        $nama_pasangan = $this->input->post('nama_pasangan');
        $telpon = $this->input->post('telpon');
        $handphone = $this->input->post('handphone');
        $email = $this->input->post('email');
        $no_npwp = $this->input->post('no_npwp');
        $kewarganegaraan = $this->input->post('kewarganegaraan');
        $kewarganegaraan_lainnya = $this->input->post('kewarganegaraan_lainnya');
        $sumber_dana = $this->input->post('sumber_dana');
        $penghasilan = $this->input->post(penghasilan);
        $pekerjaan = $this->input->post('pekerjaan');
        $propinsi = $this->input->post('propinsi');
        $kota = $this->input->post('kota');
        $kecamatan = $this->input->post('kecamatan');
        $kelurahan = $this->input->post('kelurahan');
        $kode_pos = $this->input->post('kode_pos');
        $status_tempat_tinggal = $this->input->post('status_tempat_tinggal');
        $menempati_sejak = $this->input->post('menempati_sejak');
        $alamat_domisili = $this->input->post('alamat_domisili');
        $propinsi2 = $this->input->post('propinsi2');
        $kota2 = $this->input->post('kota2');
        $kecamatan2 = $this->input->post('kecamatan2');
        $kelurahan2 = $this->input->post('kelurahan2');
        $kode_pos2 = $this->input->post('kode_pos2');
        $alamat_surat_menyurat = $this->input->post('alamat_surat_menyurat');
        //$produk = $this->input->post('produk');
		$alamat = $this->input->post('alamat');
		
        date_default_timezone_set('Asia/Jakarta');
        $tgl = date('Y-m-d H:i:s');
		
		$sqlCheck = $this->db->query("SELECT id_member FROM tb_member where no_identitas='".$no_identitas."' AND id_member<>'".$no_cif."'");
		$numCheck = $sqlCheck->num_rows();
		
		$success = false;
		$msg = "";
		if($numCheck>0){
			$msg = "Data sudah tersedia.";
		}else{
			if($menempati_sejak==""){
				$menempati_sejak = NULL;
			}else{
				$menempati_sejak = date('Y-m-d', strtotime($menempati_sejak));
			}
			
			$data = array(
				//'cabang' => $cabang,
				'first_name' => $nama_depan,
				//'middle_name' => $nama_tengah,
				//'last_name' => $nama_belakang,			
				'nama_ibu' => $nama_ibu,
				//'gelar' => $gelar,
				'tempat_lahir' => $tempat_lahir,
				'tanggal_lahir' => date('Y-m-d', strtotime($tanggal_lahir)),
				'agama' => $agama,	
				'identitas' => $identitas,
				'no_identitas' => $no_identitas,
				'masa_berlaku' => date('Y-m-d', strtotime($masa_berlaku)),
				'jenis_kelamin' => $jenis_kelamin,
				'pendidikan' => $pendidikan,
				'perkawinan' => $perkawinan,
				'nama_pasangan' => $nama_pasangan,
				'phone' => $handphone,	
				'telpon_rumah' => $telpon,
				'no_npwp' => $no_npwp,
				'kewarganegaraan' => $kewarganegaraan,
				'kewarganegaraan_lainnya' => $kewarganegaraan_lainnya,
				'sumber_dana' => $sumber_dana,
				'penghasilan' => $penghasilan,
				'pekerjaan' => $pekerjaan,
				'status_tempat_tinggal' => $status_tempat_tinggal,
				'menempati_sejak' => $menempati_sejak,
				'alamat_surat_menyurat' => $alamat_surat_menyurat,
				//'produk' => $produk,				
				'province' => $propinsi,
				'city' => $kota,
				'district' => $kecamatan,
				'kel' => $kelurahan,
				'postal_code' => $kode_pos,
				'address' => $alamat,
				'province2' => $propinsi2,
				'city2' => $kota2,
				'district2' => $kecamatan2,
				'kel2' => $kelurahan2,
				'postal_code2' => $kode_pos,
				'address2' => $alamat_domisili,
				'email' => $email,
				'update_date' => $tgl,
				'update_by' => checkSession('id')
			);
			$this->db->where('id_member', $no_cif);
			if($this->db->update('tb_member', $data)){
				$success = true;
				$msg = "";
			}else{
				$msg = "Data can't be inserted";
			}
		
		}
		
		echo json_encode(array("success"=>$success, "msg"=>$msg));
    }

    public function editCifCor($id)
    {
		$data["filelist"] = "listcif/editcifcor";
        $data["title"] = "Front";
        $data["title_menu"] = "Front";
        $data["menu"] = "listcif";
        $data["id"] = $id;
		
		$sql = $this->db->query("SELECT * FROM tb_member a
										LEFT JOIN master_provinsi b ON b.provinsi_id = a.province
										LEFT JOIN master_kokab c ON c.kota_id = a.city
										LEFT JOIN master_kecam d ON d.kecam_id = a. district 
                                        WHERE a.kode='B' AND ISNULL(delete_by) AND id_member='".$id."'");
		$tmp = $sql->result_array();
		$data["tmp"] = $tmp[0];
		
		$sqlKota = $this->db->query("SELECT * FROM master_kokab a
                                        WHERE a.kota_id='".$tmp[0]['city']."'");
		$data["kota"] = "";
		foreach($sqlKota->result_array() as $tmpKota){
			$data["kota"] = $tmpKota["kokab_nama"];
		}
		
		$sqlKecamatan = $this->db->query("SELECT * FROM master_kecam a
                                        WHERE a.kecam_id='".$tmp[0]['district']."'");
		$data["kecamatan"] = "";
		foreach($sqlKecamatan->result_array() as $tmpKecamatan){
			$data["kecamatan"] = $tmpKecamatan["nama_kecam"];
		}
		
        getHTMLWeb($data);
    }
	
	public function edit_cif_coorporate()
    {
        $no_cor = $this->input->post('no_cor');
        $nama_perusahaan = $this->input->post('nama_perusahaan');
        $tanggal_pendirian = $this->input->post('tanggal_pendirian');
        $telpon_perusahaan = $this->input->post(telpon_perusahaan);
        $alamat_perusahaan = $this->input->post(alamat_perusahaan);
        $propinsi_perusahaan = $this->input->post(propinsi_perusahaan);
        $kota_perusahaan = $this->input->post('kota_perusahaan');
        $kecamatan_perusahaan = $this->input->post('kecamatan_perusahaan');
        $kode_pos_perusahaan = $this->input->post('kode_pos_perusahaan');
        $email_perusahaan = $this->input->post('email_perusahaan');
        $no_npwp_perusahaan = $this->input->post('no_npwp_perusahaan');
        $no_rekening_perusahaan = $this->input->post('no_rekening_perusahaan');
        $siup_perusahaan = $this->input->post('siup_perusahaan');
        $bidang_usaha_perusahaan = $this->input->post('bidang_usaha_perusahaan');
        $tdp_perusahaan = $this->input->post(tdp_perusahaan);
        $nama_izin_usaha_perusahaan = $this->input->post('nama_izin_usaha_perusahaan');
        $no_izin_usaha_perusahaan = $this->input->post('no_izin_usaha_perusahaan');
        $bentuk_perusahaan = $this->input->post('bentuk_perusahaan');
        $bentuk_perusahaan_lainnya = $this->input->post('bentuk_perusahaan_lainnya');
        $tujuan_transaksi_perusahaan = $this->input->post('tujuan_transaksi_perusahaan');
		
        date_default_timezone_set('Asia/Jakarta');
        $tgl = date('Y-m-d H:i:s');
		
		$sqlCheck = $this->db->query("SELECT id_member FROM tb_member where no_npwp='".$no_npwp_perusahaan."' AND id_member<>'".$no_cor."'");
		$numCheck = $sqlCheck->num_rows();
		
		$success = false;
		$msg = "";
		if($numCheck>0){
			$msg = "Data sudah tersedia.";
		}else{	
			$data = array(
				'corp_name' => $nama_perusahaan,
				'corp_form' => $bentuk_perusahaan,
				'corp_form_other' => $bentuk_perusahaan_lainnya,
				'phone' => $telpon_perusahaan,	
				'no_npwp' => $no_npwp_perusahaan,
				'kewarganegaraan' => $kewarganegaraan,
				'tanggal_lahir' => date('Y-m-d', strtotime($tanggal_pendirian)),
				'province' => $propinsi_perusahaan,
				'city' => $kota_perusahaan,
				'district' => $kecamatan_perusahaan,
				'postal_code' => $kode_pos_perusahaan,
				'address' => $alamat_perusahaan,
				'email' => $email_perusahaan,
				'corp_norek' => $no_rekening_perusahaan,
				'corp_siup' => $siup_perusahaan,
				'corp_bidang_usaha' => $bidang_usaha_perusahaan,
				'corp_tdp' => $tdp_perusahaan,
				'corp_nama_izin_usaha' => $nama_izin_usaha_perusahaan,
				'corp_no_izin_usaha' => $no_izin_usaha_perusahaan,
				'corp_tujuan_transaksi' => $tujuan_transaksi_perusahaan,
				'update_date' => $tgl,
				'update_by' => checkSession('id')
			);
			$this->db->where('id_member', $no_cor);
			if($this->db->update('tb_member', $data)){
				$success = true;
				$msg = "";
			}else{
				$msg = "Data can't be inserted";
			}
		}
		
		echo json_encode(array("success"=>$success, "msg"=>$msg));
    }

    public function delete($id)
    {
		$data = array(
            'delete_date' => date('Y-m-d H:i:s'),
            'delete_by' => checkSession('id')
        );
        $this->db->where('id_member', $id);
        $this->db->update('tb_member', $data);
    }

    public function save_cif_individu()
    {
        $no_cif = $this->input->post('no_cif');
        $cabang = $this->input->post('cabang');
        $nama_depan = $this->input->post('nama_depan');
        $nama_tengah = $this->input->post('nama_tengah');
        $nama_belakang = $this->input->post('nama_belakang');
        $nama_ibu = $this->input->post('nama_ibu');
        $gelar = $this->input->post('gelar');
        $tempat_lahir = $this->input->post('tempat_lahir');
        $tanggal_lahir = $this->input->post('tanggal_lahir');
        $agama = $this->input->post('agama');
        $identitas = $this->input->post('identitas');
        $no_identitas = $this->input->post('no_identitas');
        $masa_berlaku = $this->input->post('masa_berlaku');
        $jenis_kelamin = $this->input->post('jenis_kelamin');
        $pendidikan = $this->input->post('pendidikan');
        $perkawinan = $this->input->post('perkawinan');
        $nama_pasangan = $this->input->post('nama_pasangan');
        $telpon = $this->input->post('telpon');
        $handphone = $this->input->post('handphone');
        $email = $this->input->post('email');
        $no_npwp = $this->input->post('no_npwp');
        $kewarganegaraan = $this->input->post('kewarganegaraan');
        $kewarganegaraan_lainnya = $this->input->post('kewarganegaraan_lainnya');
        $sumber_dana = $this->input->post('sumber_dana');
        $penghasilan = $this->input->post(penghasilan);
        $pekerjaan = $this->input->post('pekerjaan');
        $propinsi = $this->input->post('propinsi');
        $kota = $this->input->post('kota');
        $kecamatan = $this->input->post('kecamatan');
        $kode_pos = $this->input->post('kode_pos');
        $status_tempat_tinggal = $this->input->post('status_tempat_tinggal');
        $menempati_sejak = $this->input->post('menempati_sejak');
        $alamat_domisili = $this->input->post('alamat_domisili');
        $propinsi2 = $this->input->post('propinsi2');
        $kota2 = $this->input->post('kota2');
        $kecamatan2 = $this->input->post('kecamatan2');
        $kode_pos2 = $this->input->post('kode_pos2');
        $alamat_surat_menyurat = $this->input->post('alamat_surat_menyurat');
        $produk = $this->input->post('produk');
		
        date_default_timezone_set('Asia/Jakarta');
        $tgl = date('Y-m-d');

        $sql = $this->db->query("SELECT id_member FROM tb_member where kode='A'");
		$cif_id = $sql->num_rows();
		
        if ($cif_id > 999){
            $idd = "CIF".($cif_id+1);
        }else if ($cif_id > 99){
            $idd = "CIF0".($cif_id+1);
        }else if ($cif_id > 9){
			$idd = "CIF00".($cif_id+1);
		}else{
			$idd = "CIF000".($cif_id+1);
		}

        $data = array(
            'id_member' => $idd,
            'kode' => 'A',
            'cabang' => $cabang,
            'first_name' => $nama_depan,
            'middle_name' => $nama_tengah,
            'last_name' => $nama_belakang,			
            'nama_ibu' => $nama_ibu,
            'gelar' => $gelar,
            'tempat_lahir' => $tempat_lahir,
            'tanggal_lahir' => date('Y-m-d', strtotime($tanggal_lahir)),
            'agama' => $agama,	
            'identitas' => $identitas,
            'no_identitas' => $no_identitas,
            'masa_berlaku' => date('Y-m-d', strtotime($masa_berlaku)),
            'jenis_kelamin' => $jenis_kelamin,
            'pendidikan' => $pendidikan,
            'perkawinan' => $perkawinan,
            'nama_pasangan' => $nama_pasangan,
            'phone' => $handphone,	
            'telpon_rumah' => $telpon,
            'no_npwp' => $no_npwp,
            'kewarganegaraan' => $kewarganegaraan,
            'kewarganegaraan_lainnya' => $kewarganegaraan_lainnya,
            'sumber_dana' => $sumber_dana,
            'penghasilan' => $penghasilan,
            'pekerjaan' => $pekerjaan,
            'status_tempat_tinggal' => $status_tempat_tinggal,
            'menempati_sejak' => date('Y-m-d', strtotime($menempati_sejak)),
            'alamat_surat_menyurat' => $alamat_surat_menyurat,
            'produk' => $produk,				
            'province' => $propinsi,
            'city' => $kota,
            'district' => $kecamatan,
            'postal_code' => $kode_pos,
            'address' => $alamat,
			'province2' => $propinsi2,
            'city2' => $kota2,
            'district2' => $kecamatan2,
            'postal_code2' => $kode_pos,
            'address2' => $alamat_domisili,
            'email' => $email,
            'member_from' => $tgl,
            'create_date' => $tgl,
            'create_by' => checkSession('id')
        );
        $this->db->insert('tb_member', $data);

    }

    public function edit_mem_indi($id)
    {
        $fir_name   = $this->input->post('fir_name');
        $mid_name   = $this->input->post('mid_name');
        $las_name   = $this->input->post('las_name');
        $phone      = $this->input->post('phone');
        $prov       = $this->input->post('prov');
        $kota       = $this->input->post('kota');
        $keca       = $this->input->post('keca');
        $postal     = $this->input->post('postal');
        $address    = $this->input->post('address');
        $email      = $this->input->post('email');
        date_default_timezone_set('Asia/Jakarta');
        $tgl = date('Y-m-d');

        $data = array(
            'first_name'    => $fir_name,
            'middle_name'   => $mid_name,
            'last_name'     => $las_name,
            'phone'         => $phone,
            'province'      => $prov,
            'city'          => $kota,
            'district'      => $keca,
            'postal_code'   => $postal,
            'address'       => $address,
            'email'         => $email,
        );
        $this->db->where('id_member', $id);
        $this->db->update('tb_member', $data);
    }

    public function print_member(){
        $id = $this->uri->segment(3);
        $bc = new Barcode39($id);
        // set barcode bar thickness (thick bars) 
        $bc->barcode_bar_thick = 5; 
        // set barcode bar thickness (thin bars) 
        $bc->barcode_bar_thin = 2; 
		$bc->barcode_text_size = 5;
        $bc->draw("asset/barcode-member/".$id.".gif");
        $que = $this->db->query("SELECT * FROM tb_member WHERE id_member='$id'");
        foreach ($que->result() as $get) {
        
        $pdf = new FPDF('L','cm',array(9, 5.5));
        $pdf->SetAutoPageBreak(true, 0);
        $pdf->AddPage();
        $pdf->SetFont('Helvetica','B',10);
        $pdf->SetXY(0.3, 2.5);
        $pdf->Cell(0, 0, strtoupper($get->first_name)." ".strtoupper($get->middle_name)." ".strtoupper($get->last_name));
        $pdf->Ln();
        $pdf->SetXY(0.3, 3);
        $pdf->Cell(0, 0, $get->id_member);
        $pdf->Image("asset/barcode-member/".$id.".gif", 0.3, 3.45);
        }
        $pdf->Output();
    }
	
	// Hadi 
	function printView($id=NULL){
        //$bc = new Barcode39($id);
        // set barcode bar thickness (thick bars) 
        //$bc->barcode_bar_thick = 5; 
        // set barcode bar thickness (thin bars) 
        //$bc->barcode_bar_thin = 2; 
        //$bc->draw("asset/barcode-member/".$id.".gif");
		
        $pdf = new FPDF('P','cm','A4');
		$this->fpdf->SetMargins(2,1,2);
		$pdf->SetAutoPageBreak(true, 0);
		
		$pdf->AddPage();
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(19, 1, 'FORMULIR DATA NASABAH','',0,'C');
			
        $que = $this->db->query("SELECT tb_member.*, tb_store.store, master_gelar.gelar, master_agama.agama, master_identitas.identitas,
								master_jenis_kelamin.jenis_kelamin, master_pendidikan.pendidikan, master_perkawinan.perkawinan, master_kewarganegaraan.kewarganegaraan,
								master_sumber_dana.sumber_dana, master_penghasilan.penghasilan, master_pekerjaan.pekerjaan,
								kecamatan.kecamatanNama kec1, kabupaten.kabupatenNama kota1, provinsi.provinsiNama prov1,
								master_status_tempat_tinggal.status_tempat_tinggal,
								master_kecam2.kecamatanNama kec2, master_kokab2.kabupatenNama kota2, master_provinsi2.provinsiNama prov2,
								master_alamat_surat_menyurat.alamat_surat_menyurat, master_produk.produk
									FROM 
								tb_member 
								LEFT JOIN kecamatan ON kecamatan.kecamatanId = tb_member.district
								LEFT JOIN kabupaten ON kabupaten.kabupatenId = tb_member.city
								LEFT JOIN provinsi ON provinsi.provinsiId = tb_member.province
								LEFT JOIN kecamatan master_kecam2 ON master_kecam2.kecamatanId = tb_member.district2
								LEFT JOIN kabupaten master_kokab2 ON master_kokab2.kabupatenId = tb_member.city2
								LEFT JOIN provinsi master_provinsi2 ON master_provinsi2.provinsiId = tb_member.province2
								LEFT JOIN tb_store ON tb_store.kode = tb_member.cabang
								LEFT JOIN master_gelar ON master_gelar.id = tb_member.gelar
								LEFT JOIN master_agama ON master_agama.id = tb_member.agama
								LEFT JOIN master_identitas ON master_identitas.id = tb_member.identitas
								LEFT JOIN master_jenis_kelamin ON master_jenis_kelamin.id = tb_member.jenis_kelamin
								LEFT JOIN master_pendidikan ON master_pendidikan.id = tb_member.pendidikan
								LEFT JOIN master_perkawinan ON master_perkawinan.id = tb_member.perkawinan
								LEFT JOIN master_kewarganegaraan ON master_kewarganegaraan.id = tb_member.kewarganegaraan
								LEFT JOIN master_sumber_dana ON master_sumber_dana.id = tb_member.sumber_dana
								LEFT JOIN master_penghasilan ON master_penghasilan.id = tb_member.penghasilan
								LEFT JOIN master_pekerjaan ON master_pekerjaan.id = tb_member.pekerjaan
								LEFT JOIN master_status_tempat_tinggal ON master_status_tempat_tinggal.id = tb_member.status_tempat_tinggal
								LEFT JOIN master_alamat_surat_menyurat ON master_alamat_surat_menyurat.id = tb_member.alamat_surat_menyurat
								LEFT JOIN master_produk ON master_produk.id = tb_member.produk
								WHERE id_member='".$id."'");
        
		$pdf->SetFont('Arial','',9);
		
		foreach ($que->result() as $get) {
			$pdf->Ln();
			$pdf->SetFont('Arial','BI',9);
			$pdf->Cell(19, 0.5, 'CUSTOMER INFORMATION FILE (CIF)','B',0,'L');
			
			$pdf->SetFont('Arial','',9);
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'CABANG','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, $get->store,'',0,'L');
			
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'NOMOR CIF','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, $get->id_member,'',0,'L');
			
			$pdf->Ln(0.8);
			$pdf->SetFont('Arial','BI',9);
			$pdf->Cell(19, 0.5, 'DATA PRIBADI','B',0,'L');
			
			$pdf->SetFont('Arial','',9);
			
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'NAMA LENGKAP','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, $get->first_name.' '.$get->middle_name.' '.$get->last_name,'',0,'L');
			
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'NAMA IBU KANDUNG','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, $get->nama_ibu,'',0,'L');
			
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'TEMPAT LAHIR','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(4, 0.5, $get->tempat_lahir,'',0,'L');
			$pdf->Cell(4, 0.5, 'TANGGAL LAHIR','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(4, 0.5, date('m/d/Y', strtotime($get->tanggal_lahir)),'',0,'L');
			
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'AGAMA','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, $get->agama,'',0,'L');
			
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'IDENTITAS YG DIPAKAI','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, $get->identitas,'',0,'L');
			
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'NO. KTP/SIM/PASPOR','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, $get->no_identitas,'',0,'L');
			
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'MASA BERLAKU','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, date('m/d/Y', strtotime($get->masa_berlaku)),'',0,'L');
			
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'JENIS KELAMIN','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, $get->jenis_kelamin,'',0,'L');
			
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'PENDIDIKAN TERAKHIR','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, $get->pendidikan,'',0,'L');
			
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'STATUS PERKAWINAN','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(4, 0.5, $get->perkawinan,'',0,'L');
			$pdf->Cell(4, 0.5, 'NAMA PASANGAN','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(4, 0.5, (($get->nama_pasangan!="")?$get->nama_pasangan:'-'),'',0,'L');
			
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'NO. TELPON RUMAH','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(4, 0.5, (($get->telpon_rumah!="")?$get->telpon_rumah:'-'),'',0,'L');
			$pdf->Cell(4, 0.5, 'NO. HP','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(4, 0.5, (($get->phone!="")?$get->phone:'-'),'',0,'L');
			
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'EMAIL','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(4, 0.5, (($get->email!="")?$get->email:'-'),'',0,'L');
			$pdf->Cell(4, 0.5, 'NO. NPWP','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(4, 0.5, (($get->no_npwp!="")?$get->no_npwp:'-'),'',0,'L');
			
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'KEWARGANEGARAAN','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, $get->kewarganegaraan,'',0,'L');
			$pdf->Cell(4, 0.5, '','',0,'L');
			$pdf->Cell(0.5, 0.5, '','',0,'L');
			$pdf->Cell(4, 0.5, $get->kewarganegaraan_lainnya,'',0,'L');
			
			$pdf->Ln(0.8);
			$pdf->SetFont('Arial','BI',9);
			$pdf->Cell(19, 0.5, 'DATA KEUANGAN','B',0,'L');
			
			$pdf->SetFont('Arial','',9);			
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'SUMBER DANA','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, $get->sumber_dana,'',0,'L');
			
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'PENGHASILAN','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, $get->penghasilan,'',0,'L');
			
			$pdf->Ln(0.8);
			$pdf->SetFont('Arial','BI',9);
			$pdf->Cell(19, 0.5, 'DATA KERJAAN','B',0,'L');
			
			$pdf->SetFont('Arial','',9);			
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'PEKERJAAN','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, $get->pekerjaan,'',0,'L');
			
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'ALAMAT PEKERJAAN','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, '-','',0,'L');
			
			$pdf->Ln(0.8);
			$pdf->SetFont('Arial','BI',9);
			$pdf->Cell(19, 0.5, 'DATA ALAMAT','B',0,'L');
			
			$pdf->SetFont('Arial','',9);			
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'ALAMAT (Identitas)','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, $get->address,'',0,'L');		
			$pdf->Ln();
			$pdf->Cell(4, 0.5, '','',0,'L');
			$pdf->Cell(0.5, 0.5, '','',0,'L');
			$pdf->Cell(14, 0.5, 'Kode Pos : '.$get->postal_code.', '.$get->kec1.', '.$get->kota1.', '.$get->prov1,'',0,'L');
			
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'MENEMPATI SEJAK','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, (($get->menempati_sejak!="")?date('m/d/Y', strtotime($get->menempati_sejak)):'-'),'',0,'L');
				
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'STATUS TMPT TINGGAL','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, $get->status_tempat_tinggal,'',0,'L');
					
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'ALAMAT (Domisili)','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, $get->address2,'',0,'L');		
			$pdf->Ln();
			$pdf->Cell(4, 0.5, '','',0,'L');
			$pdf->Cell(0.5, 0.5, '','',0,'L');
			$pdf->Cell(14, 0.5, 'Kode Pos : '.$get->postal_code.', '.$get->kec2.', '.$get->kota2.', '.$get->prov2,'',0,'L');
				
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'ALMT SURAT MENYURAT','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, $get->alamat_surat_menyurat,'',0,'L');
			
        }
        
		$pdf->Output();
    }
	
	function printViewCor($id=NULL){
		
        $pdf = new FPDF('P','cm','A4');
		$this->fpdf->SetMargins(2,1,2);
		$pdf->SetAutoPageBreak(true, 0);
		
		$pdf->AddPage();
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(19, 1, 'FORMULIR DATA NASABAH KORPORASI','',0,'C');
			
        $que = $this->db->query("SELECT tb_member.*,
								master_kecam.nama_kecam kec1, master_kokab.kokab_nama kota1, master_provinsi.provinsi_nama prov1,
								master_bentuk_perusahaan.bentuk_perusahaan
									FROM 
								tb_member 
								LEFT JOIN master_kecam ON master_kecam.kecam_id = tb_member.district
								LEFT JOIN master_kokab ON master_kokab.kota_id = tb_member.city
								LEFT JOIN master_provinsi ON master_provinsi.provinsi_id = tb_member.province
								LEFT JOIN master_bentuk_perusahaan ON master_bentuk_perusahaan.id = tb_member.corp_form
								WHERE id_member='".$id."'");
        
		$pdf->SetFont('Arial','',9);
		
		foreach ($que->result() as $get) {
			$pdf->Ln();
			$pdf->SetFont('Arial','BI',9);
			$pdf->Cell(19, 0.5, 'NASABAH KORPORASI','B',0,'L');
			
			$pdf->SetFont('Arial','',9);
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'NOMOR CIF','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, $get->id_member,'',0,'L');
			
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'NAMA PERUSAHAAN','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, $get->corp_name,'',0,'L');
			
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'TANGGAL PENDIRIAN','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, date('m/d/Y', strtotime($get->tanggal_lahir)),'',0,'L');
			
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'ALAMAT','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, $get->address,'',0,'L');		
			$pdf->Ln();
			$pdf->Cell(4, 0.5, '','',0,'L');
			$pdf->Cell(0.5, 0.5, '','',0,'L');
			$pdf->Cell(14, 0.5, 'Kode Pos : '.$get->postal_code.', '.$get->kec1.', '.$get->kota1.', '.$get->prov1,'',0,'L');
			
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'EMAIL PERUSAHAAN','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, (($get->email!="")?$get->email:'-'),'',0,'L');
			
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'NO. NPWP','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, $get->no_npwp,'',0,'L');
			
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'NO. REKENING BANK','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, $get->corp_norek,'',0,'L');
			
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'SIUP','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, $get->corp_siup,'',0,'L');
			
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'BIDANG USAHA','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, $get->corp_bidang_usaha,'',0,'L');
			
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'TDP','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, $get->corp_tdp,'',0,'L');
			
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'NAMA IZIN USAHA','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, $get->corp_nama_izin_usaha,'',0,'L');
			
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'NO. IZIN USAHA','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, $get->corp_no_izin_usaha,'',0,'L');
			
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'BENTUK PERUSAHAAN','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, $get->bentuk_perusahaan,'',0,'L');
			$pdf->Cell(4, 0.5, '','',0,'L');
			$pdf->Cell(0.5, 0.5, '','',0,'L');
			$pdf->Cell(4, 0.5, $get->corp_form_other,'',0,'L');
				
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'TUJUAN TRANSAKSI','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, (($get->corp_tujuan_transaksi!="")?$get->corp_tujuan_transaksi:'-'),'',0,'L');
			
        }
        
		$pdf->Output();
    }
}
