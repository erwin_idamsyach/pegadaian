	   <script type="text/javascript">
        jQuery(function($){
           $("#kode_pos").mask("99999",{placeholder:"_____"});
           $("#kode_pos2").mask("99999",{placeholder:"_____"});
		   $("#telpon").mask("99999999",{placeholder:""});
           $("#handphone").mask("99999999999",{placeholder:""});
		   
		   $('.datepicker').datepicker();
			
        });
		
		function backToList(){
			location.href = base_url+"listcif";
		}
		
		function edit_cif_individu() {
		  var no_cif = $('#no_cif').val();
		  //var cabang = $('#cabang').val();
		  var nama_depan = $('#nama_depan').val();
		  //var nama_tengah = $('#nama_tengah').val();
		  //var nama_belakang = $('#nama_belakang').val();
		  var nama_ibu = $('#nama_ibu').val();
		  //var gelar = $('#gelar').val();
		  var tempat_lahir = $('#tempat_lahir').val();
		  var tanggal_lahir = $('#tanggal_lahir').val();
		  var agama = $('#agama').val();
		  var identitas = $('#identitas').val();
		  var no_identitas = $('#no_identitas').val();
		  var masa_berlaku = $('#masa_berlaku').val();
		  var jenis_kelamin = $('#jenis_kelamin').val();
		  var pendidikan = $('#pendidikan').val();
		  var perkawinan = $('#perkawinan').val();
		  var nama_pasangan = $('#nama_pasangan').val();
		  var telpon = $('#telpon').val();
		  var handphone = $('#handphone').val();
		  var email = $('#email').val();
		  var no_npwp = $('#no_npwp').val();
		  var kewarganegaraan = $('#kewarganegaraan').val();
		  var kewarganegaraan_lainnya = $('#kewarganegaraan_lainnya').val();
		  var sumber_dana = $('#sumber_dana').val();
		  var penghasilan = $('#penghasilan').val();
		  var pekerjaan = $('#pekerjaan').val();
		  var alamat = $('#alamat').val();
		  var propinsi = $('#propinsi').val();
		  var kota = $('#kota').val();
		  var kecamatan = $('#kecamatan').val();
		  var kelurahan = $('#kelurahan').val();
		  var kode_pos = $('#kode_pos').val();
		  var status_tempat_tinggal = $('#status_tempat_tinggal').val();
		  var menempati_sejak = $('#menempati_sejak').val();
		  var alamat_domisili = $('#alamat_domisili').val();
		  var propinsi2 = $('#propinsi2').val();
		  var kota2 = $('#kota2').val();
		  var kecamatan2 = $('#kecamatan2').val();
		  var kelurahan2 = $('#kelurahan2').val();
		  var kode_pos2 = $('#kode_pos2').val();
		  var alamat_surat_menyurat = $('#alamat_surat_menyurat').val();
		  //var produk = $('#produk').val();
		  
		  if(no_cif == "" || nama_depan == "" || nama_ibu == "" || tempat_lahir == "" || agama == "" || identitas == "" || no_identitas == ""){
			return false;
		  }else{
			swal({
				 title: "",
				 text: "Apakah Anda akan menyimpan data?",
				 type: "warning",
				 showCancelButton: true,
				 confirmButtonColor: "#DD6B55",
				 confirmButtonText: "Ya",
				 cancelButtonText: "Tidak",
				 closeOnConfirm: false }, function(){
					$.ajax({
					  type : "POST",
					  url  : base_url+"listcif/edit_cif_individu",
					  data : {
						'no_cif' : no_cif,
						//'cabang' : cabang,
						'nama_depan' : nama_depan,
						//'nama_tengah' : nama_tengah,
						//'nama_belakang' : nama_belakang,
						'nama_ibu'    : nama_ibu,
						//'gelar'     : gelar,
						'tempat_lahir'     : tempat_lahir,
						'tanggal_lahir'     : tanggal_lahir,
						'agama'     : agama,
						'identitas'     : identitas,
						'no_identitas'     : no_identitas,
						'masa_berlaku'     : masa_berlaku,
						'jenis_kelamin'     : jenis_kelamin,
						'pendidikan'     : pendidikan,
						'perkawinan'     : perkawinan,
						'nama_pasangan'     : nama_pasangan,
						'telpon'     : telpon,
						'handphone'     : handphone,
						'email'     : email,
						'no_npwp'     : no_npwp,
						'kewarganegaraan'     : kewarganegaraan,
						'kewarganegaraan_lainnya'     : kewarganegaraan_lainnya,
						'sumber_dana'     : sumber_dana,
						'penghasilan'     : penghasilan,
						'pekerjaan'     : pekerjaan,
						'alamat'     : alamat,
						'propinsi'     : propinsi,
						'kota'     : kota,
						'kecamatan'     : kecamatan,
						'kelurahan'     : kelurahan,
						'kode_pos'     : kode_pos,
						'status_tempat_tinggal'     : status_tempat_tinggal,
						'menempati_sejak'     : menempati_sejak,
						'alamat_domisili'     : alamat_domisili,
						'propinsi2'     : propinsi2,
						'kota2'     : kota2,
						'kecamatan2'     : kecamatan2,
						'kelurahan2'     : kelurahan2,
						'kode_pos2'     : kode_pos2,
						'alamat_surat_menyurat'     : alamat_surat_menyurat
						//'produk'     : produk
					  },
					  success:function(html){
						var data = eval ("(" + html + ")");
						if(data.success){
							swal({
							  title: "Berhasil Menyimpan data!",
							  text: "Klik tombol di bawah.",
							  type: "success",
							  showCancelButton: false,
							  confirmButtonColor: "#257DB6",
							  confirmButtonText: "Ok!",
							  closeOnConfirm: false
							},
							function(){
							  location.href = base_url+"listcif";
							});
						}else{
							swal.close();
							popOverMsg('no_identitas', data.msg);
						}
						
					  }
					});
				});	
			return false;
		  }
		}
      </script>
	  
    <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>&nbsp;</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-user"></i> Reg. CIF</a></li>
            <li class="active">Edit CIF</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="index">
            <div class="box">
              <div class="box-header">
                <b>EDIT CIF INDIVIDUAL</b>
                <div style="border:1px solid black;margin-bottom:0px;"></div>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div>
              </div>
              <div class="box-body">
				<div>
					<form method="POST" onSubmit="return edit_cif_individu()" >
					  <div class="row">
						<div class="col-md-12">
						<i>CUSTOMER INFORMATION FILE(CIF)</i>
						<div style="border:1px solid black;margin-bottom:10px;"></div>
						</div>
					  </div>
					  <div class="row">		
						<div class="col-md-3">
						  <div class="form-group">
							<label>Nomor CIF <font color="red">*</font></label>
							<input type="text" name="no_cif" id="no_cif" class="form-control" placeholder="Nomor CIF" value="<?php echo $id;?>" readonly required>
						  </div>
						</div>
					  </div>
					  <div class="row">
						<div class="col-md-12">
						<i>DATA PRIBADI</i>
						<div style="border:1px solid black;margin-bottom:10px;"></div>
						</div>
					  </div>
					  <div class="row">
						<div class="col-md-9">
						  <div class="form-group">
							<label>Nama Lengkap <font color="red">*</font></label>
							<input type="text" name="nama_depan" id="nama_depan" class="form-control" placeholder="Nama Lengkap" value="<?php echo $tmp['first_name'];?>" required>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Nama Ibu Kandung <font color="red">*</font></label>
							<input type="text" name="nama_ibu" id="nama_ibu" class="form-control" placeholder="Nama Ibu Kandung" value="<?php echo $tmp['nama_ibu'];?>" required>
						  </div>
						</div>
						<div class="col-md-6">
						  <div class="form-group">
							<label>Tempat Lahir <font color="red">*</font></label>
							<input type="text" name="tempat_lahir" id="tempat_lahir" class="form-control" placeholder="Tempat Lahir" value="<?php echo $tmp['tempat_lahir'];?>" required>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Tanggal Lahir <font color="red">*</font></label>
							<input type="text" name="tanggal_lahir" id="tanggal_lahir" class="form-control datepicker" placeholder="Tanggal Lahir" value="<?php echo date('m/d/Y', strtotime($tmp['tanggal_lahir']));?>" required>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Agama <font color="red">*</font></label>
							<select name="agama" id="agama" class="form-control" required>
							  <option value="">Select Agama</option>
							<?php
							  $get_pro = mysql_query("SELECT * FROM master_agama WHERE ISNULL(delete_by) order by id");
							  while ($do = mysql_fetch_array($get_pro)) {
								  if($do['id']==$tmp['agama']){
									  $select_agama = "selected";
								  }else{
									  $select_agama = "";
								  }
							?>
							  <option value="<?php echo $do['id'] ?>" <?php echo $select_agama;?>><?php echo $do['agama'] ?></option>
							<?php
							  }
							?>
							</select>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Identitas <font color="red">*</font></label>
							<select name="identitas" id="identitas" class="form-control" >
							  <option value="">Select Identitas</option>
							<?php
							  $get_pro = mysql_query("SELECT * FROM master_identitas WHERE ISNULL(delete_by) order by id");
							  while ($do = mysql_fetch_array($get_pro)) {
								  if($do['id']==$tmp['agama']){
									  $select_identitas = "selected";
								  }else{
									  $select_identitas = "";
								  }
							?>
							  <option value="<?php echo $do['id'] ?>" <?php echo $select_identitas;?>><?php echo $do['identitas'] ?></option>
							<?php
							  }
							?>
							</select>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group no_identitas">
							<label>No. KTP/SIM/PASSPOR <font color="red">*</font></label></label>
							<input type="text" name="no_identitas" id="no_identitas" class="form-control" placeholder="No. KTP/SIM/PASSPOR" value="<?php echo $tmp['no_identitas'];?>">
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Masa Berlaku <font color="red">*</font></label>
							<input type="text" name="masa_berlaku" id="masa_berlaku" class="form-control datepicker" placeholder="Masa Berlaku" value="<?php echo date('m/d/Y', strtotime($tmp['masa_berlaku']));?>" >
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Jenis Kelamin <font color="red">*</font></label>
							<select name="jenis_kelamin" id="jenis_kelamin" class="form-control" >
							  <option value="">Select Jenis Kelamin</option>
							<?php
							  $get_pro = mysql_query("SELECT * FROM master_jenis_kelamin order by id");
							  while ($do = mysql_fetch_array($get_pro)) {
								  if($do['id']==$tmp['jenis_kelamin']){
									  $select_jenis_kelamin = "selected";
								  }else{
									  $select_jenis_kelamin = "";
								  }
							?>
							  <option value="<?php echo $do['id'] ?>" <?php echo $select_jenis_kelamin;?>><?php echo $do['jenis_kelamin'] ?></option>
							<?php
							  }
							?>
							</select>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Pendidikan Terakhir <font color="red">*</font></label>
							<select name="pendidikan" id="pendidikan" class="form-control" >
							  <option value="">Select Pendidikan Terakhir</option>
							<?php
							  $get_pro = mysql_query("SELECT * FROM master_pendidikan WHERE ISNULL(delete_by) order by id");
							  while ($do = mysql_fetch_array($get_pro)) {
								  if($do['id']==$tmp['pendidikan']){
									  $select_pendidikan = "selected";
								  }else{
									  $select_pendidikan = "";
								  }
							?>
							  <option value="<?php echo $do['id'] ?>" <?php echo $select_pendidikan;?>><?php echo $do['pendidikan'] ?></option>
							<?php
							  }
							?>
							</select>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Status Perkawinan <font color="red">*</font></label>
							<select name="perkawinan" id="perkawinan" class="form-control" >
							  <option value="">Select Status Perkawinan</option>
							<?php
							  $get_pro = mysql_query("SELECT * FROM master_perkawinan WHERE ISNULL(delete_by) order by id");
							  while ($do = mysql_fetch_array($get_pro)) {
								  if($do['id']==$tmp['perkawinan']){
									  $select_perkawinan = "selected";
								  }else{
									  $select_perkawinan = "";
								  }
							?>
							  <option value="<?php echo $do['id'] ?>" <?php echo $select_perkawinan;?>><?php echo $do['perkawinan'] ?></option>
							<?php
							  }
							?>
							</select>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Nama Pasangan</label>
							<input type="text" name="nama_pasangan" id="nama_pasangan" class="form-control" placeholder="Nama Pasangan" value="<?php echo $tmp['nama_pasangan'];?>">
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>No. Telpon Rumah</label>
							<div class="input-group"><span class="input-group-addon">021</span><input class="form-control" id="telpon" name="telpon" type="text" value="<?php echo $tmp['telpon_rumah'];?>"/></div>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>No. Handphone <font color="red">*</font></label>
							<div class="input-group"><span class="input-group-addon">62</span><input class="form-control" id="handphone" name="handphone" type="text" value="<?php echo $tmp['phone'];?>" required /></div>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Email</label>
							<input type="text" name="email" id="email" class="form-control" placeholder="Email" value="<?php echo $tmp['email'];?>">
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>No. NPWP</label>
							<input type="text" name="no_npwp" id="no_npwp" class="form-control" placeholder="No. NPWP" value="<?php echo $tmp['no_npwp'];?>">
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Kewarganegaraan <font color="red">*</font></label>
							<select name="kewarganegaraan" id="kewarganegaraan" class="form-control" >
							  <option value="">Select Kewarganegaraan</option>
							<?php
							  $get_pro = mysql_query("SELECT * FROM master_kewarganegaraan WHERE ISNULL(delete_by) order by id");
							  while ($do = mysql_fetch_array($get_pro)) {
								  if($do['id']==$tmp['kewarganegaraan']){
									  $select_kewarganegaraan = "selected";
								  }else{
									  $select_kewarganegaraan = "";
								  }
							?>
							  <option value="<?php echo $do['id'] ?>" <?php echo $select_kewarganegaraan;?>><?php echo $do['kewarganegaraan'] ?></option>
							<?php
							  }
							?>
							</select>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Kewarganegaraan Lainnya</label>
							<input type="text" name="kewarganegaraan_lainnya" id="kewarganegaraan_lainnya" class="form-control" placeholder="Kewarganegaraan Lainnya" value="<?php echo $tmp['kewarganegaraan_lainnya'];?>">
						  </div>
						</div>
					  </div>
					  <div class="row">
						<div class="col-md-12">
						<i>DATA KEUANGAN</i>
						<div style="border:1px solid black;margin-bottom:10px;"></div>
						</div>
					  </div>
					  <div class="row">
						<div class="col-md-3">
						  <div class="form-group">
							<label>Sumber Dana <font color="red">*</font></label>
							<select name="sumber_dana" id="sumber_dana" class="form-control" >
							  <option value="">Select Sumber Dana</option>
							<?php
							  $get_pro = mysql_query("SELECT * FROM master_sumber_dana WHERE ISNULL(delete_by) order by id");
							  while ($do = mysql_fetch_array($get_pro)) {
								  if($do['id']==$tmp['sumber_dana']){
									  $select_sumber_dana = "selected";
								  }else{
									  $select_sumber_dana = "";
								  }
							?>
							  <option value="<?php echo $do['id'] ?>" <?php echo $select_sumber_dana;?>><?php echo $do['sumber_dana'] ?></option>
							<?php
							  }
							?>
							</select>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Rata-Rata Penghasilan </label>
							<select name="penghasilan" id="penghasilan" class="form-control" >
							  <option value="">Select Penghasilan</option>
							<?php
							  $get_pro = mysql_query("SELECT * FROM master_penghasilan WHERE ISNULL(delete_by) order by id");
							  while ($do = mysql_fetch_array($get_pro)) {
								  if($do['id']==$tmp['penghasilan']){
									  $select_penghasilan = "selected";
								  }else{
									  $select_penghasilan = "";
								  }
							?>
							  <option value="<?php echo $do['id'] ?>" <?php echo $select_penghasilan;?>><?php echo $do['penghasilan'] ?></option>
							<?php
							  }
							?>
							</select>
						  </div>
						</div>
					  </div>
					  <div class="row">
						<div class="col-md-12">
						<i>DATA PEKERJAAN</i>
						<div style="border:1px solid black;margin-bottom:10px;"></div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Pekerjaan <font color="red">*</font></label>
							<select name="pekerjaan" id="pekerjaan" class="form-control" >
							  <option value="">Select Pekerjaan</option>
							<?php
							  $get_pro = mysql_query("SELECT * FROM master_pekerjaan WHERE ISNULL(delete_by) order by id");
							  while ($do = mysql_fetch_array($get_pro)) {
								  if($do['id']==$tmp['pekerjaan']){
									  $select_pekerjaan = "selected";
								  }else{
									  $select_pekerjaan = "";
								  }
							?>
							  <option value="<?php echo $do['id'] ?>"<?php echo $select_pekerjaan;?>><?php echo $do['pekerjaan'] ?></option>
							<?php
							  }
							?>
							</select>
						  </div>
						</div>
						<div class="col-md-6">
						  <div class="form-group">
							<label>Alamat (Sesuai Identitas) <font color="red">*</font></label>
							<input type="text" name="alamat" id="alamat" class="form-control" placeholder="Alamat" value="<?php echo $tmp['address'];?>">
						  </div>
						</div><div class="col-md-3">
						  <div class="form-group">
							<label>Propinsi (Sesuai Identitas) <font color="red">*</font></label>
							<select name="province" id="propinsi" class="form-control prov" onchange="get_kota()">
							  <option value="">Select Propinsi</option>
							<?php
							  $get_pro = mysql_query("SELECT * FROM provinsi order by provinsiId");
							  while ($do = mysql_fetch_array($get_pro)) {
								  if($do['provinsiId']==$tmp['province']){
									  $select_propinsi = "selected";
								  }else{
									  $select_propinsi = "";
								  }
							?>
							  <option value="<?php echo $do['provinsiId'] ?>" <?php echo $select_propinsi;?>><?php echo $do['provinsiNama'] ?></option>
							<?php
							  }
							?>
							</select>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Kota (Sesuai Identitas) <font color="red">*</font></label>
							<select name="city" id="kota" class="form-control kota">
							  <option value="<?php echo $tmp['city'];?>"><?php echo $kota;?></option>
							</select>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Kecamatan (Sesuai Identitas) <font color="red">*</font></label>
							<select name="district" id="kecamatan" class="form-control keca">
							  <option value="<?php echo $tmp['district'];?>"><?php echo $kecamatan;?></option>
							</select>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Kelurahan (Sesuai Identitas) <font color="red">*</font></label>
							<select name="kelurahan" id="kelurahan" class="form-control desa">
							  <option value="<?php echo $tmp['kel'];?>"><?php echo $kelurahan;?></option>
							</select>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Kode Pos (Sesuai Identitas) <font color="red">*</font></label>
							<input type="text" name="kode_pos" id="kode_pos" class="form-control kode_pos" maxlength="5" placeholder="Kode Pos" value="<?php echo $tmp['postal_code'];?>">
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Status Tempat Tinggal</label>
							<select name="status_tempat_tinggal" id="status_tempat_tinggal" class="form-control" >
							  <option value="">Select Status Tempat Tinggal</option>
							<?php
							  $get_pro = mysql_query("SELECT * FROM master_status_tempat_tinggal WHERE ISNULL(delete_by) order by id");
							  while ($do = mysql_fetch_array($get_pro)) {
								  if($do['id']==$tmp['status_tempat_tinggal']){
									  $select_status_tempat_tinggal = "selected";
								  }else{
									  $select_status_tempat_tinggal = "";
								  }
							?>
							  <option value="<?php echo $do['id'] ?>"<?php echo $select_status_tempat_tinggal;?>><?php echo $do['status_tempat_tinggal'] ?></option>
							<?php
							  }
							?>
							</select>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Menempati Sejak </label>
							<input type="text" name="menempati_sejak" id="menempati_sejak" class="form-control datepicker" placeholder="Menempati Sejak" value="<?php echo ($tmp['menempati_sejak']=="")?"":date('m/d/Y', strtotime($tmp['menempati_sejak']));?>">
						  </div>
						</div>
						<div class="col-md-6">
						  <div class="form-group">
							<label>Alamat Domisili <font color="red">*</font></label>
							<input type="text" name="alamat_domisili" id="alamat_domisili" class="form-control" placeholder="Alamat" value="<?php echo $tmp['address2'];?>">
						  </div>
						</div><div class="col-md-3">
						  <div class="form-group">
							<label>Propinsi Domisili <font color="red">*</font></label>
							<select name="propinsi2" id="propinsi2" class="form-control prov2" onchange="get_kota2()">
							  <option value="">Select Propinsi</option>
							<?php
							  $get_pro = mysql_query("SELECT * FROM provinsi order by provinsiId");
							  while ($do = mysql_fetch_array($get_pro)) {
								  if($do['provinsiId']==$tmp['province2']){
									  $select_propinsi2 = "selected";
								  }else{
									  $select_propinsi2 = "";
								  }
							?>
							  <option value="<?php echo $do['provinsiId'] ?>" <?php echo $select_propinsi2;?>><?php echo $do['provinsiNama'] ?></option>
							<?php
							  }
							?>
							</select>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Kota Domisili <font color="red">*</font></label>
							<select name="kota2" id="kota2" class="form-control kota2">
							  <option value="<?php echo $tmp['city2'];?>"><?php echo $kota2;?></option>
							</select>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Kecamatan Domisili <font color="red">*</font></label>
							<select name="kecamatan2" id="kecamatan2" class="form-control keca2">
							  <option value="<?php echo $tmp['district2'];?>"><?php echo $kecamatan2;?></option>
							</select>
						  </div>
						</div>						
						<div class="col-md-3">
						  <div class="form-group">
							<label>Kelurahan Domisili <font color="red">*</font></label>
							<select name="kelurahan2" id="kelurahan2" class="form-control desa2">
							  <option value="<?php echo $tmp['kel2'];?>"><?php echo $kelurahan2;?></option>
							</select>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Kode Pos Domisili <font color="red">*</font></label>
							<input type="text" name="kode_pos2" id="kode_pos2" class="form-control kode_pos2" maxlength="5" placeholder="Kode Pos" value="<?php echo $tmp['postal_code2'];?>">
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Alamat Surat Menyurat <font color="red">*</font></label>
							<select name="alamat_surat_menyurat" id="alamat_surat_menyurat" class="form-control" >
							  <option value="">Select Alamat Surat Menyurat</option>
							<?php
							  $get_pro = mysql_query("SELECT * FROM master_alamat_surat_menyurat WHERE ISNULL(delete_by) order by id");
							  while ($do = mysql_fetch_array($get_pro)) {
								  if($do['id']==$tmp['alamat_surat_menyurat']){
									  $select_alamat_surat_menyurat = "selected";
								  }else{
									  $select_alamat_surat_menyurat = "";
								  }
							?>
							  <option value="<?php echo $do['id'] ?>" <?php echo $select_alamat_surat_menyurat;?>><?php echo $do['alamat_surat_menyurat'] ?></option>
							<?php
							  }
							?>
							</select>
						  </div>
						</div>
					  </div>
					  <div class="row">
						<div class="col-md-12">
						  <button type="submit" class="btn btn-default" onclick=""><i class="fa fa-save"></i> SIMPAN</button>
						  <button type="button" class="btn btn-default" onclick="backToList()"><i class="fa fa-refresh"></i> BACK</button>
						</div>
					  </div>
					</form>
				</div>
				
              </div>
            </div>
          </div>

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->