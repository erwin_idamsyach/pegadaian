	   <script type="text/javascript">
        jQuery(function($){
           $('#example1').DataTable();
           $('#example2').DataTable();
        });
		
		function viewCif(id, kode){
			$('#id_member_view').val(id);
			$('#kode_member_view').val(kode);
			$('.modal-body').load(base_url+"listcif/viewCif/"+id,function(result){
				$('#modalView').modal({show:true});
			});
		}
		
		function viewCifCor(id, kode){
			$('#id_member_view').val(id);
			$('#kode_member_view').val(kode);
			$('.modal-body').load(base_url+"listcif/viewCifCor/"+id,function(result){
				$('#modalView').modal({show:true});
			});
		}
		
		function printView(){
			var id = $('#id_member_view').val();
			var kode = $('#kode_member_view').val();
			
			var url = "";
			if(kode=="A"){
				url = base_url+"listcif/printView/"+id
			}else{
				url = base_url+"listcif/printViewCor/"+id
			}
			window.open(url,'_blank');
		}
		
		function editCif(id){
			location.href = base_url+"listcif/editCif/"+id;
		}
		
		function editCifCor(id){
			location.href = base_url+"listcif/editCifCor/"+id;
		}
		
		function deleteCif(id){
			swal({
             title: "",
             text: "Apakah Anda akan menghapus data?",
             type: "warning",
             showCancelButton: true,
             confirmButtonColor: "#DD6B55",
             confirmButtonText: "Ya",
             cancelButtonText: "Tidak",
             closeOnConfirm: false }, function(){
                $.ajax({
				  type : "POST",
				  url  : base_url+"listcif/delete/"+id,
				  data : {
				  },
				  success:function(html){ 
					swal({
					  title: "Berhasil Menghapus data!",
					  text: "Klik tombol di bawah.",
					  type: "success",
					  showCancelButton: false,
					  confirmButtonColor: "#257DB6",
					  confirmButtonText: "Ok!",
					  closeOnConfirm: false
					},
					function(){
					  location.href = base_url+"listcif";
					});
					
				  }
				});
            });
			
		}
      </script>
	  
    <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>&nbsp;</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-user"></i> Reg. CIF</a></li>
            <li class="active">List CIF</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          
          <div class="box">
            <div class="box-header">
              <b>LIST CIF INDIVIDUAL</b>
              <div style="border:1px solid black;margin-bottom:0px;"></div>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div>
            </div>
            <div class="box-body">
              <div class="table-responsive">
                <table class="table table-bordered table-striped" id="example1">
                  <thead>
                    <tr>
                      <th class="text-center" width="5%">No</th>
                      <th class="text-center" width="10%">Tanggal</th>
                      <th class="text-center" width="10%">No. CIF</th>
                      <th class="text-center" >Nama</th>
                      <th class="text-center" width="10%">Tanggal Lahir</th>
                      <th class="text-center" width="18%">Email</th>
                      <th class="text-center" width="18%">No. HP</th>
                      <th class="text-center" width="18%">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                      $no = 1;
                      $q = $this->db->query("SELECT * FROM tb_member a
										LEFT JOIN master_provinsi b ON b.provinsi_id = a.province
										LEFT JOIN master_kokab c ON c.kota_id = a.city
										LEFT JOIN master_kecam d ON d.kecam_id = a. district 
                                        WHERE a.kode='A' AND ISNULL(delete_by) ORDER BY create_date DESC ");
                      foreach ($q->result_array() as $i) {
                    ?>
                      <tr>
                        <td style="text-align:center"><?php echo $no++?></td>
                        <td class="text-center"><?php echo $i['create_date']?></td>
                        <td><?php echo $i['id_member']?></td>
                        <td><?php echo $i['first_name']." ".$i['middle_name']." ".$i['last_name']?></td>
                        <td class="text-center"><?php echo date('Y-m-d', strtotime($i['tanggal_lahir']))?></td>
                        <td><?php echo $i['email']?></td>
                        <td style="text-align:left">+62 <?php echo $i['phone']?></td>
                        <td class="text-right">
						  <a href="#" onclick="viewCif('<?php echo $i['id_member'] ?>','<?php echo $i['kode'] ?>')" class="btn btn-info" data-toggle="modal" data-placement="bottom" title="View" data-target="#modalView"><i class="fa fa-eye"></i></a>
						  <a href="#" onclick="editCif('<?php echo $i['id_member'] ?>')" class="btn btn-warning" data-placement="bottom" title="Edit"><i class="fa fa-edit"></i></a>
                          <a href="#" class="btn btn-danger disabled" onclick="deleteCif('<?php echo $i['id_member'];?>')" data-placement="bottom" title="Delete"><i class="fa fa-close"></i></a>
                          <a href="<?php echo site_url('listcif/print_member').'/'.$i['id_member'] ?>" class="btn btn-primary" target="_blank" data-placement="bottom" title="Print">
                          <i class="fa fa-print"></i></a>
                        </td>
                      </tr>
                    <?php
                      }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
		  
		  
          
          <div class="box">
            <div class="box-header">
              <b>LIST CIF KORPORASI</b>
              <div style="border:1px solid black;margin-bottom:0px;"></div>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div>
            </div>
            <div class="box-body">
              <div class="table-responsive">
                <table class="table table-bordered table-striped" id="example2">
                  <thead>
                    <tr>
                      <th class="text-center" width="5%">No</th>
                      <th class="text-center" width="15%">Tanggal</th>
                      <th class="text-center" width="10%">No. Korp.</th>
                      <th class="text-center" >Nama Perusahaan</th>
                      <th class="text-center" width="18%">Email</th>
                      <th class="text-center" width="18%">No. HP</th>
                      <th class="text-center" width="18%">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                      $no = 1;
                      $q = $this->db->query("SELECT * FROM tb_member a
										LEFT JOIN master_provinsi b ON b.provinsi_id = a.province
										LEFT JOIN master_kokab c ON c.kota_id = a.city
										LEFT JOIN master_kecam d ON d.kecam_id = a. district 
                                        WHERE a.kode='B' AND ISNULL(delete_by) ORDER BY create_date DESC");
                      foreach ($q->result_array() as $i) {
                    ?>
                      <tr>
                        <td style="text-align:center"><?php echo $no++?></td>
                        <td class="text-center"><?php echo $i['create_date']?></td>
                        <td><?php echo $i['id_member']?></td>
                        <td><?php echo $i['corp_name']?></td>
                        <td><?php echo $i['email']?></td>
                        <td style="text-align:left">+62 <?php echo $i['phone']?></td>
                        <td class="text-right">
						  <a href="#" onclick="viewCifCor('<?php echo $i['id_member'] ?>','<?php echo $i['kode'] ?>')" class="btn btn-info" data-toggle="modal" data-placement="bottom" title="View" data-target="#modalView"><i class="fa fa-eye"></i></a>
						  <a href="#" onclick="editCifCor('<?php echo $i['id_member'] ?>')" class="btn btn-warning" data-placement="bottom" title="Edit"><i class="fa fa-edit"></i></a>
                          <a href="#" class="btn btn-danger disabled" onclick="deleteCif('<?php echo $i['id_member'];?>')" data-placement="bottom" title="Delete"><i class="fa fa-close"></i></a>
                          <a href="<?php echo site_url('listcif/print_member').'/'.$i['id_member'] ?>" class="btn btn-primary" target="_blank" data-placement="bottom" title="Print">
                          <i class="fa fa-print"></i></a>
                        </td>
                      </tr>
                    <?php
                      }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

		<!-- Modal -->
		<div id="modalView" class="modal fade" role="dialog">
		  <div class="modal-dialog  modal-lg">

			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">&nbsp;</h4>
			  </div>
			  <div class="modal-body">
				<p>&nbsp;</p>
			  </div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-default" onclick="printView()"><i class="fa fa-print"></i> PRINT</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<input type="hidden" id="id_member_view">
				<input type="hidden" id="kode_member_view">
			  </div>
			</div>

		  </div>
		</div>