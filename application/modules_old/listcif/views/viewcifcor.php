  
    <!-- Content Wrapper. Contains page content -->
      

        <!-- Main content -->
        <section class="content">
          <div class="index">
            <div class="box">
              <div class="box-header">
                <b>VIEW CIF KORPORASI</b>
                <div style="border:1px solid black;margin-bottom:0px;"></div>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div>
              </div>
              <div class="box-body">
				<div class="nav-tabs-custom">
					  <div class="row">
						<div class="col-md-12">
						<i>NASABAH KORPORASI</i>
						<div style="border:1px solid black;margin-bottom:10px;"></div>
						</div>
					  </div>
					  <div class="row">
						<div class="col-md-3">
						  <div class="form-group">
							<label>Nomor Korporasi <font color="red">*</font></label>
							<br /><?php echo $id;?>
						  </div>
						</div>
						<div class="col-md-6">
						  <div class="form-group">
							<label>Nama Korporasi/Perusahaan <font color="red">*</font></label>
							<br /><?php echo $tmp['corp_name'];?>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Tanggal Pendirian <font color="red">*</font></label>
							<br /><?php echo date('m/d/Y', strtotime($tmp['tanggal_lahir']));?>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>No. Telpon <font color="red">*</font></label>
							<br /><?php echo (($tmp['phone']!="")?'021 '.$tmp['phone']:'-');?>
						  </div>
						</div>
						<div class="col-md-6">
						  <div class="form-group">
							<label>Alamat <font color="red">*</font></label>
							<br /><?php echo $tmp['address'];?>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Propinsi <font color="red">*</font></label>
							<br /><?php echo $tmp['prov1'];?>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Kota <font color="red">*</font></label>
							<br /><?php echo $tmp['kota1'];?>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Kecamatan <font color="red">*</font></label>
							<br /><?php echo $tmp['kec1'];?>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Kode Pos <font color="red">*</font></label>
							<br /><?php echo $tmp['postal_code'];?>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Email</label>
							<br /><?php echo (($tmp['email']!="")?$tmp['email']:'-');?>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group no_npwp_perusahaan">
							<label>No. NPWP <font color="red">*</font></label>
							<br /><?php echo $tmp['no_npwp'];?>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>No. Rekening Bank <font color="red">*</font></label>
							<br /><?php echo $tmp['corp_norek'];?>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>SIUP <font color="red">*</font></label>
							<br /><?php echo $tmp['corp_siup'];?>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Bidang Usaha <font color="red">*</font></label>
							<br /><?php echo $tmp['corp_bidang_usaha'];?>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>TDP <font color="red">*</font></label>
							<br /><?php echo $tmp['corp_tdp'];?>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Nama Izin Usaha <font color="red">*</font></label>
							<br /><?php echo $tmp['corp_nama_izin_usaha'];?>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>No. Izin Usaha <font color="red">*</font></label>
							<br /><?php echo $tmp['corp_no_izin_usaha'];?>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Bentuk Perusahaan <font color="red">*</font></label>
							<br /><?php echo $tmp['corp_form'];?>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Bentuk Perusahaan Lainnya </label>
							<br /><?php echo (($tmp['corp_form_other']!="")?$tmp['corp_form_other']:'-');?>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Tujuan Transaksi </label>
							<br /><?php echo (($tmp['corp_tujuan_transaksi']!="")?$tmp['corp_tujuan_transaksi']:'-');?>
						  </div>
						</div>
					  </div>
				</div>
              </div>
            </div>
          </div>

        </section><!-- /.content -->
      <!-- /.content-wrapper -->