            <?php
              $qu = $this->db->get_where('tb_member_individu', array('id_member'=>$id));
              foreach ($qu->result_array() as $y) {
              }
            ?>
            <div class="box">
              <div class="box-header">
                <b>EDIT MEMBER-INDIVIDUAL</b>
                <div style="border:1px solid black;margin-bottom:0px;"></div>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
              </div>
              <div class="box-body">
                <div class="col-md-12">
                  <div class="row">
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>First Name</label>
                        <input type="text" name="fir_name" id="fir_name" class="form-control" value="<?php echo $y['first_name'] ?>" placeholder="first name.." required>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Middle Name</label>
                        <input type="text" name="las_name" id="mid_name" class="form-control" value="<?php echo $y['middle_name'] ?>" placeholder="(optional)">
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Last Name</label>
                        <input type="text" name="las_name" id="las_name" class="form-control" value="<?php echo $y['last_name'] ?>" placeholder="last name..">
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Phone</label>
                        <div class="input-group"><span class="input-group-addon">62</span><input class="quantity form-control" id="phone" value="<?php echo $y['phone'] ?>" name="phone" type="number" min="0" maxlength="11"/></div>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Province</label>
                        <select name="province" class="form-control prov" onchange="get_kota()">
                        <?php
                          $por = $y['province'];
                          $ll = $this->db->query("SELECT * FROM master_provinsi where provinsi_id='$por'");
                          foreach ($ll->result_array() as $x) {
                          }
                        ?>
                          <option value="<?php echo $x['provinsi_id'] ?>"><?php echo $x['provinsi_nama'] ?></option>
                        <?php
                          $get_pro = mysql_query("SELECT * FROM master_provinsi where provinsi_id!='$por'");
                          while ($do = mysql_fetch_array($get_pro)) {
                        ?>
                          <option value="<?php echo $do['provinsi_id'] ?>"><?php echo $do['provinsi_nama'] ?></option>
                        <?php
                          }
                        ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>City</label>
                        <select name="city" class="form-control kota">
                          <?php
                            $porx = $y['city'];
                            $ll = $this->db->query("SELECT * FROM master_kokab where kota_id='$porx'");
                            foreach ($ll->result_array() as $x) {
                            }
                          ?>
                          <option value="<?php echo $x['kota_id'] ?>"><?php echo $x['kokab_nama'] ?></option>
                          <?php
                            $get_pro = mysql_query("SELECT * FROM master_kokab where provinsi_id='$por' and kota_id!='$porx'");
                            while ($do = mysql_fetch_array($get_pro)) {
                          ?>
                            <option value="<?php echo $do['kota_id'] ?>"><?php echo $do['kokab_nama'] ?></option>
                          <?php
                            }
                          ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>District</label>
                        <select name="district" class="form-control keca">
                          <?php
                            $pox = $y['district'];
                            $ll = $this->db->query("SELECT * FROM master_kecam where kecam_id='$pox'");
                            foreach ($ll->result_array() as $x) {
                            }
                          ?>
                          <option value="<?php echo $x['kecam_id'] ?>"><?php echo $x['nama_kecam'] ?></option>
                          <?php
                            $get_pro = mysql_query("SELECT * FROM master_kecam where kota_id='$porx' and kecam_id!='$pox'");
                            while ($do = mysql_fetch_array($get_pro)) {
                          ?>
                            <option value="<?php echo $do['kecam_id'] ?>"><?php echo $do['nama_kecam'] ?></option>
                          <?php
                            }
                          ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Postal Code</label>
                        <input type="text" name="postal" id="postal" class="form-control" value="<?php echo $y['postal_code'] ?>" maxlength="5" placeholder="your postal code..">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Address</label>
                        <input type="text" name="address" id="address" class="form-control" value="<?php echo $y['address'] ?>" placeholder="address..">
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Email</label>
                        <input type="text" name="email" id="email" class="form-control" value="<?php echo $y['email'] ?>" placeholder="member email..">
                      </div>
                    </div>
                    <div class="col-md-12">
                      <button type="submit" class="btn btn-primary" onclick="edit_member_indi('<?php echo $id ?>')"><i class="fa fa-save"></i> SUBMIT</button>
                      <a href="" class="btn btn-danger"><i class="fa fa-chevron-left"></i> BACK</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>