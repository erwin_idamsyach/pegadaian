<!-- Hadi -->
<script type="text/javascript">
    jQuery(function($){
        $('#example1').DataTable();
    });
	
	function editData(id){
      $.ajax({
        type : "POST",
        url  : base_url+"<?php echo $controls; ?>/editData/"+id,
        data : "id="+id,
        success:function(html){
          $('#myModalWeb').modal('show');
          $('.indexWeb').html(html);
        }
      });
    }
</script>

      <!-- Modal -->
      <div class="modal fade" id="myModalWeb" tabindex="-1" role="dialog" aria-labelledby="myModalLabelEmas">
        <div class="modal-dialog modal-md" role="document">
          <div class="modal-content">
            <div class="modal-header" style="background-color:#f8f8f8">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h6 class="modal-title" id="myModalLabelWeb"><b>EDIT <?php echo strtoupper($menu);?></b></h6>
            </div>
            <div class="modal-body">
              <div class="indexWeb">
                
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>&nbsp;</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
            <li><?php echo $title;?></li>
            <li class="active"><?php echo ucfirst($menu);?></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="box">
            <div class="box-header">
              <b>LIST <?php echo strtoupper($menu);?></b>
              <div style="border:1px solid black;margin-bottom:0px;"></div>
            </div>
            <div class="box-body">
              <div class="table-responsive">
                <table class="table table-bordered table-striped" id="example1">
                  <thead>
                    <tr>
                      <th class="text-center">No</th>
                      <th class="text-center">Url</th>
                      <th class="text-center">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                      $no = 1;
                      $q = $this->db->query("SELECT * FROM ".$table." WHERE ISNULL(delete_by)");
                      foreach ($q->result_array() as $i) {
                    ?>
                      <tr>
                        <td style="text-align:center;width:50px;"><?php echo $no++?></td>
                        <td class="text-center"><?php echo $i['url']?></td>
                        <td class="text-right" style="width:150px;">
                          <a onclick="editData(<?php echo $i['id'] ?>)" class="btn btn-warning"><i class="fa fa-edit"></i></a>
                          <a class="btn btn-danger disabled" href="#"><i class="fa fa-close"></i></a>
                        </td>
                      </tr>
                    <?php
                      }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->