<script type="text/javascript">
    jQuery(function($){
        $('#example1').DataTable();
        $(".datepicker").datepicker();
	});
</script>	

	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>&nbsp;</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
            <li>Report</li>
            <li class="active"><?php echo $menu;?></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="box box-default">
            <div class="box-header">
              <b>Report - <?php echo $menu;?></b>
              <div style="border: 1px solid black; margin-bottom: 10px;"></div>
			  
                <form action="" method="get">
                    <div class="container-fluid" style="margin-top: 10px;">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="from-group">
									<table width="100%" border="0">
									<tr>
										<td>
											<label><b>Cari Tanggal : </b></label>
										</td>
										<td>
											<input type="text" name="from" class="form-control datepicker">
										</td>
										<td>
											&nbsp;-&nbsp;
										</td>
										<td>
											<input type="text" name="to" class="form-control datepicker">
										</td>
										<td>
											&nbsp;<button type="submit" class="btn btn-primary" style="margin-top: 0px;"><i class="fa fa-search"></i></button>
										</td>
									</tr>
									</table>
                                </div>  
                            </div>
                        </div>
                    </div>
                </form>
				
              <table class="table table-bordered table-striped table-hover" id="example1">
                <thead>
                  <tr>
                    <th class="text-center" width="5%">No</th>
                    <th class="text-center" width="10%">Tanggal</th>
                    <th class="text-center">Nasabah</th>
                    <th class="text-center" width="15%">No. BPS/BPT</th>
                    <th class="text-center" width="15%">Order</th>
                    <th class="text-center" width="15%">No. Sertifikat/SHT</th>
                    <th class="text-center" width="20%">Keterangan</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $no = 1;	
				  $search = "";
				  if(!isset($_GET['from']) && !isset($_GET['to'])){
					$date_se = date('d-m-Y');
					$where = " AND DATE_FORMAT(step.create_date,'%d-%m-%Y')=DATE_FORMAT(NOW(),'%d-%m-%Y') AND status_barang='TERTINGGAL' ";
					
					$from = $date_se;
					$to = $date_se;
				  }else{
					$search = "1";
					$from = $_GET['from'];
					$to = $_GET['to'];

					$from = date_create($from);
					$from = date_format($from, "Y-m-d");
					$to = date_create($to);
					$to = date_format($to, "Y-m-d");
					$to = date('Y-m-d',strtotime($to. "+1 days"));
					
					$where = " AND step.create_date >= '$from 12:00:00 AM' AND step.create_date <= '$to 11:59:59 PM'  
							AND status_barang='TERTINGGAL' ";
					
				  }
				  
					$get_today = $this->db->query("SELECT tb_lab_desk.*, step.id_order, step.no_sertifikat,
												tb_front_desk.certificate, tb_front_desk.gem_card, tb_front_desk.dia_grading,
												tb_member.first_name,
												step.no_brief
												FROM tb_lab_desk
												LEFT JOIN tb_member ON tb_member.id_member = tb_lab_desk.id_member
												LEFT JOIN tb_front_desk ON tb_front_desk.id_object = tb_lab_desk.id_object
												LEFT JOIN step ON step.id_object = tb_lab_desk.id_object
												WHERE ISNULL(tb_lab_desk.delete_by) AND tb_front_desk.certificate!=''
												AND tb_front_desk.store='".sessionValue('kode_store')."'
												".$where);
				  
                  foreach ($get_today->result() as $get) {
                    ?>
                    <tr>
                      <td class="text-center"><?php echo $no++; ?></td>
                      <td class="text-center"><?php echo date('d-m-Y', strtotime($get->finish)); ?></td>
                      <td class="text-left"><?php echo $get->first_name; ?></td>
                      <td class="text-center"><?php echo $get->id_order; ?></td>
					  <td class="text-left"><?php echo "FULL REPORT"; ?></td>
					  <td class="text-left"><?php echo $get->no_sertifikat; ?></td>
                      <td class="text-left"><?php echo ""; ?></td>
                    </tr>
                    <?php
				  }
					$get_today = $this->db->query("SELECT tb_lab_desk.*, step.id_order, step.no_brief,
												tb_front_desk.certificate, tb_front_desk.gem_card, tb_front_desk.dia_grading,
												tb_member.first_name,
												step.no_brief
												FROM tb_lab_desk
												LEFT JOIN tb_member ON tb_member.id_member = tb_lab_desk.id_member
												LEFT JOIN tb_front_desk ON tb_front_desk.id_object = tb_lab_desk.id_object
												LEFT JOIN step ON step.id_object = tb_lab_desk.id_object
												WHERE ISNULL(tb_lab_desk.delete_by) AND tb_front_desk.gem_card!=''
												".$where);
				  
                  foreach ($get_today->result() as $get) {
					?>
                    <tr>
                      <td class="text-center"><?php echo $no++; ?></td>
                      <td class="text-center"><?php echo date('d-m-Y', strtotime($get->finish)); ?></td>
                      <td class="text-left"><?php echo $get->first_name; ?></td>
                      <td class="text-center"><?php echo $get->id_order; ?></td>
					  <td class="text-left"><?php echo "BRIEF REPORT"; ?></td>
					  <td class="text-left"><?php echo $get->no_brief; ?></td>
                      <td class="text-left"><?php echo ""; ?></td>
                    </tr>
					<?php
				  }
					$get_today = $this->db->query("SELECT tb_lab_desk.*, step.id_order, step.no_grading,
												tb_front_desk.certificate, tb_front_desk.gem_card, tb_front_desk.dia_grading,
												tb_member.first_name,
												step.no_brief
												FROM tb_lab_desk
												LEFT JOIN tb_member ON tb_member.id_member = tb_lab_desk.id_member
												LEFT JOIN tb_front_desk ON tb_front_desk.id_object = tb_lab_desk.id_object
												LEFT JOIN step ON step.id_object = tb_lab_desk.id_object
												WHERE ISNULL(tb_lab_desk.delete_by) AND tb_front_desk.dia_grading!=''
												AND tb_front_desk.store='".sessionValue('kode_store')."'
												".$where);
				  
                  foreach ($get_today->result() as $get) {
					?>
                    <tr>
                      <td class="text-center"><?php echo $no++; ?></td>
                      <td class="text-center"><?php echo date('d-m-Y', strtotime($get->finish)); ?></td>
                      <td class="text-left"><?php echo $get->first_name; ?></td>
                      <td class="text-center"><?php echo $get->id_order; ?></td>
					  <td class="text-left"><?php echo "D. GRADING"; ?></td>
					  <td class="text-left"><?php echo $get->no_grading; ?></td>
                      <td class="text-left"><?php echo ""; ?></td>
                    </tr>
					<?php
				  }
					$get_today = $this->db->query("SELECT tb_taksiran.*, step.id_order, step.no_taksiran,
												tb_member.first_name,
												step.no_brief
												FROM tb_taksiran
												LEFT JOIN tb_member ON tb_member.id_member = tb_taksiran.id_member
												LEFT JOIN step ON step.id_object = tb_taksiran.id
												LEFT JOIN tb_login ON tb_login.id = tb_taksiran.create_by
												WHERE ISNULL(tb_taksiran.delete_by)
												AND tb_login.store='".sessionValue('kode_store')."'
												".$where);
				  
                  foreach ($get_today->result() as $get) {
					?>
                    <tr>
                      <td class="text-center"><?php echo $no++; ?></td>
                      <td class="text-center"><?php echo date('d-m-Y', strtotime($get->finish)); ?></td>
                      <td class="text-left"><?php echo $get->first_name; ?></td>
                      <td class="text-center"><?php echo $get->id_order; ?></td>
					  <td class="text-left"><?php echo "J. TAKSIRAN"; ?></td>
					  <td class="text-left"><?php echo $get->no_taksiran; ?></td>
                      <td class="text-left"><?php echo ""; ?></td>
                    </tr>
					<?php
				  }
				  
				  $menu = str_replace(' ', '-',$menu);
                  ?>
                </tbody>
                <tfoot>
                  <tr>
                    <td colspan="7">
                      <div class="pull-right">
                        <a href="<?php echo site_url('report/print_report_barang_tertinggal').'/'.$from.'/'.$to.'/'.$menu.'/'.$search; ?>" target="_blank" class="btn btn-primary">PRINT REPORT</a>
                      </div>
                    </td>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->