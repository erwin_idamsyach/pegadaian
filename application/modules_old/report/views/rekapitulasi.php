<script type="text/javascript">
    jQuery(function($){
        //$('#example1').DataTable();
        $(".datepicker").datepicker();
	});
</script>	

	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>&nbsp;</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
            <li>Report</li>
            <li class="active"><?php echo $menu;?></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="box box-default">
            <div class="box-header">
              <b>Report - <?php echo $menu;?></b>
              <div style="border: 1px solid black; margin-bottom: 10px;"></div>
			  
                <form action="" method="get">
                    <div class="container-fluid" style="margin-top: 10px;">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="from-group">
									<table width="100%" border="0">
									<tr>
										<td>
											<label><b>Cari Tanggal : </b></label>
										</td>
										<td>
											<input type="text" name="from" class="form-control datepicker">
										</td>
										<td>
											&nbsp;-&nbsp;
										</td>
										<td>
											<input type="text" name="to" class="form-control datepicker">
										</td>
										<td>
											&nbsp;<button type="submit" class="btn btn-primary" style="margin-top: 0px;"><i class="fa fa-search"></i></button>
										</td>
									</tr>
									</table>
                                </div>  
                            </div>
                        </div>
                    </div>
                </form>
				
              <table class="table table-bordered table-striped table-hover" id="example1">
                <thead>
                  <tr>
                    <th class="text-center" width="5%">No</th>
                    <th class="text-center" width="15%">Produk</th>
                    <th class="text-center" width="15%">Jumlah Nasabah</th>
                    <th class="text-center" width="15%">Jumlah Transaksi</th>
                    <th class="text-center">Pendapatan</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $no = 1;
				  $search = "";
				  if(!isset($_GET['from']) && !isset($_GET['to'])){
					$date_se = date('Y-m-d');
					
					$where = " AND DATE_FORMAT(tb_lab_desk.finish,'%d-%m-%Y')=DATE_FORMAT(NOW(),'%d-%m-%Y') AND tb_front_desk.gem_card!='' ";
					$where2 = " AND DATE_FORMAT(tb_front_desk.create_date,'%d-%m-%Y')=DATE_FORMAT(NOW(),'%d-%m-%Y') AND tb_front_desk.gem_card!='' ";
					
					$where3 = " AND DATE_FORMAT(tb_lab_desk.finish,'%d-%m-%Y')=DATE_FORMAT(NOW(),'%d-%m-%Y') AND tb_front_desk.certificate!='' ";
					$where4 = " AND DATE_FORMAT(tb_front_desk.create_date,'%d-%m-%Y')=DATE_FORMAT(NOW(),'%d-%m-%Y') AND tb_front_desk.certificate!='' ";
					
					$where5 = " AND DATE_FORMAT(tb_lab_grading.finish,'%d-%m-%Y')=DATE_FORMAT(NOW(),'%d-%m-%Y') AND tb_front_desk.dia_grading!='' ";
					$where6 = " AND DATE_FORMAT(tb_front_desk.create_date,'%d-%m-%Y')=DATE_FORMAT(NOW(),'%d-%m-%Y') AND tb_front_desk.dia_grading!='' ";
					
					$whereTaksiran = " AND DATE_FORMAT(tb_taksiran.create_date,'%Y-%m-%d')=DATE_FORMAT(NOW(),'%Y-%m-%d') ";
					
					$from = $date_se;
					$to = $date_se;
				  }else{
					$search = "1";
					$from = $_GET['from'];
					$to = $_GET['to'];

					$from = date_create($from);
					$from = date_format($from, "Y-m-d");
					$to = date_create($to);
					$to = date_format($to, "Y-m-d");
					$to = date('Y-m-d',strtotime($to. "+1 days"));
					
					$where = " AND tb_lab_desk.finish >= '$from 12:00:00 AM' AND tb_lab_desk.finish <= '$to 11:59:59 PM' 
						AND tb_front_desk.gem_card!='' ";
					$where2 = " AND tb_front_desk.create_date >= '$from 12:00:00 AM' AND tb_front_desk.create_date <= '$to 11:59:59 PM' 
						AND tb_front_desk.gem_card!='' ";
						
					$where3 = " AND tb_lab_desk.finish >= '$from 12:00:00 AM' AND tb_lab_desk.finish <= '$to 11:59:59 PM' 
						AND tb_front_desk.certificate!='' ";
					$where4 = " AND tb_front_desk.create_date >= '$from 12:00:00 AM' AND tb_front_desk.create_date <= '$to 11:59:59 PM' 
						AND tb_front_desk.certificate!='' ";
						
					$where5 = " AND tb_lab_grading.finish >= '$from 12:00:00 AM' AND tb_lab_grading.finish <= '$to 11:59:59 PM' 
						AND tb_front_desk.dia_grading!='' ";
					$where6 = " AND tb_front_desk.create_date >= '$from 12:00:00 AM' AND tb_front_desk.create_date <= '$to 11:59:59 PM' 
						AND tb_front_desk.dia_grading!='' ";
						
					$whereTaksiran = " AND tb_taksiran.create_date >= '$from 12:00:00 AM' AND tb_taksiran.create_date <= '$to 11:59:59 PM' ";
				  }
				  
					$get_brief_report = $this->db->query("SELECT COUNT(tb_lab_desk.id_member) jumlah,
												SUM(tb_service.harga) harga
												FROM tb_lab_desk
												LEFT JOIN tb_member ON tb_member.id_member = tb_lab_desk.id_member
												LEFT JOIN tb_front_desk ON tb_front_desk.id_object = tb_lab_desk.id_object
												LEFT JOIN step ON step.id_object = tb_lab_desk.id_object
												JOIN tb_service ON tb_service.nama = tb_front_desk.gem_card
												WHERE ISNULL(tb_lab_desk.delete_by) 
												AND step.pay='Y'
												AND tb_front_desk.store='".sessionValue('kode_store')."'
												".$where." GROUP BY tb_lab_desk.id_member"); 
					$get_brief_report2 = $this->db->query("SELECT tb_front_desk.id_member, tb_lab_desk.obj_natural
												FROM tb_front_desk
												LEFT JOIN tb_lab_desk ON tb_lab_desk.id_object = tb_front_desk.id_object
												LEFT JOIN step ON step.id_object = tb_lab_desk.id_object
												WHERE ISNULL(tb_front_desk.delete_by) 
												AND step.pay='Y'
												AND tb_front_desk.store='".sessionValue('kode_store')."'
												".$where2);
					$jml_brief = 0;
					$jml_transaksi_brief = 0;
					$harga_brief = 0;
					$x = 0;
					foreach ($get_brief_report->result() as $get) {
						$jml_brief = $jml_brief + $get->jumlah;
					}
					foreach ($get_brief_report2->result() as $get) {
						$x++;
						$jml_transaksi_brief = $x;
						if($get->obj_natural!="NATURAL"){
							$mem_pri = mysql_query("SELECT harga FROM tb_service WHERE kode='BRS'");
							$pri_mem = mysql_fetch_array($mem_pri);
						}else{
							$mem_pri = mysql_query("SELECT harga FROM tb_service WHERE kode='BR'");
							$pri_mem = mysql_fetch_array($mem_pri);
						}
						$harga_brief = $harga_brief + $pri_mem['harga'];
					}
					
					$get_full_report = $this->db->query("SELECT COUNT(tb_lab_desk.id_member) jumlah,
												SUM(tb_service.harga) harga
												FROM tb_lab_desk
												LEFT JOIN tb_front_desk ON tb_front_desk.id_object = tb_lab_desk.id_object
												LEFT JOIN step ON step.id_object = tb_lab_desk.id_object
												JOIN tb_service ON tb_service.nama = tb_front_desk.certificate
												WHERE ISNULL(tb_lab_desk.delete_by) 
												AND step.pay='Y'
												AND tb_front_desk.store='".sessionValue('kode_store')."'
												".$where3." GROUP BY tb_lab_desk.id_member");
					$get_full_report2 = $this->db->query("SELECT tb_front_desk.id_member
												FROM tb_front_desk
												LEFT JOIN step ON step.id_object = tb_front_desk.id_object
												WHERE ISNULL(tb_front_desk.delete_by) 
												AND step.pay='Y'
												AND tb_front_desk.store='".sessionValue('kode_store')."'
												".$where4);
					$jml_full = 0;
					$jml_transaksi_full = 0;
					$harga_full = 0;
					$x = 0;
					foreach ($get_full_report->result() as $get) {
						$jml_full = $jml_full + $get->jumlah;
						$harga_full = $harga_full + $get->harga;
					}
					foreach ($get_full_report2->result() as $get) {
						$x++;
						$jml_transaksi_full = $x;
					}
					
					$get_diamond_grading = $this->db->query("SELECT tb_lab_grading.id_member,
												tb_lab_grading.obj_weight
												FROM tb_lab_grading
												LEFT JOIN tb_front_desk ON tb_front_desk.id_object = tb_lab_grading.id_object
												LEFT JOIN step ON step.id_object = tb_lab_grading.id_object
												WHERE ISNULL(tb_lab_grading.delete_by) 
												AND step.pay='Y'
												AND tb_front_desk.store='".sessionValue('kode_store')."'
												".$where5);
					$get_diamond_grading2 = $this->db->query("SELECT COUNT(tb_front_desk.id_member) jumlah
												FROM tb_front_desk
												LEFT JOIN step ON step.id_object = tb_front_desk.id_object
												WHERE ISNULL(tb_front_desk.delete_by) 
												AND step.pay='Y'
												AND tb_front_desk.store='".sessionValue('kode_store')."'
												".$where6." GROUP BY tb_front_desk.id_member");
					$jml_grading = 0;
					$jml_transaksi_grading = 0;
					$harga_grading = 0;
					$x = 0;
					foreach ($get_diamond_grading->result() as $get) {
						$x++;
						$jml_transaksi_grading = $jml_transaksi_grading + $x;
						$getHarga = $this->db->query("SELECT harga
								FROM master_harga_grading
								WHERE berat2 >= ".$get->obj_weight." ORDER BY ID ASC LIMIT 0,1");
						   
						foreach ($getHarga->result() as $tmpHarga) {
							$harga_grading = $harga_grading + $tmpHarga->harga;
						}
						//$harga_grading = $get->harga;
					}
					foreach ($get_diamond_grading2->result() as $get) {
						$jml_grading = $jml_grading + $get->jumlah;
					}
					
					// TAKSIRAN
					$getHDLE = $this->db->query("SELECT * FROM master_harga_emas WHERE id='1'");
					$hdle = 0;
					foreach ($getHDLE->result() as $hsp) {
						$hdle = $hsp->harga_emas;
					}
					$getEmas = $this->db->query("SELECT * FROM tb_taksiran
												LEFT JOIN master_logam ON master_logam.id = tb_taksiran.jenis_logam
												LEFT JOIN tb_login ON tb_login.id = tb_taksiran.create_by
												LEFT JOIN step ON step.id_object = tb_taksiran.id
												WHERE tb_taksiran.jenis_logam='1'
												AND ISNULL(tb_taksiran.delete_by) 
												AND step.pay='Y'
												AND tb_login.store='".sessionValue('kode_store')."'
												".$whereTaksiran);
										
					$harga_total = 0;
					foreach ($getEmas->result() as $key) {
						$harga_taksiran = 1.25/100*(($key->berat_bersih*($key->karatase_emas/24))*$hdle); // $key->jumlah_perhiasan*						
						$harga_total = $harga_total + $harga_taksiran;
					} 
					$getBerlian = $this->db->query("SELECT berat_total_permata FROM tb_taksiran
						LEFT JOIN step ON step.id_object = tb_taksiran.id
						WHERE tb_taksiran.id_order='".$get->id_order."' AND step.pay='Y' ");							
					foreach ($getBerlian->result() as $key) {
						if($key->berat_total_permata == ""){
							$berat_total = $berat_total + 0;
						}else{
							$berat_total = $berat_total + $key->berat_total_permata;
						}
						if($berat_total>0){
							$getHarga = $this->db->query("SELECT harga
														FROM `master_tarif_taksiran`
														WHERE berat2 >= ".$berat_total." ORDER BY ID ASC LIMIT 0,1");
														
							foreach ($getHarga->result() as $tmpHarga) {
								$harga_berlian = $harga_berlian + $tmpHarga->harga;
							}
							$harga_total = $harga_total + $harga_berlian;
						}
					}
					
					$get_taksiran = $this->db->query("SELECT COUNT(tb_taksiran.id_member) jumlah
												FROM tb_taksiran
												LEFT JOIN tb_login ON tb_login.id = tb_taksiran.create_by
												LEFT JOIN step ON step.id_object = tb_taksiran.id
												WHERE ISNULL(tb_taksiran.delete_by) 
												AND step.pay='Y'
												AND tb_login.store='".sessionValue('kode_store')."'
												".$whereTaksiran." GROUP BY tb_taksiran.id_member"); 
					$get_taksiran2 = $this->db->query("SELECT tb_taksiran.id_member
												FROM tb_taksiran
												LEFT JOIN tb_login ON tb_login.id = tb_taksiran.create_by
												LEFT JOIN step ON step.id_object = tb_taksiran.id
												WHERE ISNULL(tb_taksiran.delete_by) 
												AND step.pay='Y'
												AND tb_login.store='".sessionValue('kode_store')."'
												".$whereTaksiran);
					$jml_taksiran = 0;
					$jml_transaksi_taksiran = 0;
					$harga_taksiran = roundNearestHundredUp($harga_total);
					$x = 0;
					foreach ($get_taksiran->result() as $get) {
						$jml_taksiran = $jml_taksiran + $get->jumlah;
					}
					foreach ($get_taksiran2->result() as $get) {
						$x++;
						$jml_transaksi_taksiran = $x;
					}
                    ?>
                    <tr>
                      <td class="text-center"><?php echo $no++; ?></td>
                      <td class="text-left">Brief Report</td>
                      <td class="text-center"><?php echo $jml_brief; ?></td>
                      <td class="text-center"><?php echo $jml_transaksi_brief; ?></td>
                      <td class="text-right"><?php echo number_format($harga_brief,0,',','.'); ?></td>
                    </tr>
                    <tr>
                      <td class="text-center"><?php echo $no++; ?></td>
                      <td class="text-left">Identification Report</td>
                      <td class="text-center"><?php echo $jml_full; ?></td>
                      <td class="text-center"><?php echo $jml_transaksi_full; ?></td>
                      <td class="text-right"><?php echo number_format($harga_full,0,',','.'); ?></td>
                    </tr>
                    <tr>
                      <td class="text-center"><?php echo $no++; ?></td>
                      <td class="text-left">Diamond Grading Report</td>
                      <td class="text-center"><?php echo $jml_grading; ?></td>
                      <td class="text-center"><?php echo $jml_transaksi_grading; ?></td>
                      <td class="text-right"><?php echo number_format($harga_grading,0,',','.'); ?></td>
                    </tr>
                    <tr>
                      <td class="text-center"><?php echo $no++; ?></td>
                      <td class="text-left">Jasa Taksiran</td>
                      <td class="text-center"><?php echo $jml_taksiran; ?></td>
                      <td class="text-center"><?php echo $jml_transaksi_taksiran; ?></td>
                      <td class="text-right"><?php echo number_format($harga_taksiran,0,',','.'); ?></td>
                    </tr>
                  <?php				  
				  $menu = str_replace(' ', '-',$menu);
                  ?>
                </tbody>
                <tfoot>
                  <tr>
                    <td colspan="6">
                      <div class="pull-right">
                        <a href="<?php echo site_url('report/print_report_rekapitulasi').'/'.$from.'/'.$to.'/'.$menu.'/'.$search; ?>" target="_blank" class="btn btn-primary">PRINT REPORT</a>
                      </div>
                    </td>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->