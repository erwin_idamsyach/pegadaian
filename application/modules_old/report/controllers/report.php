<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
* Controller Report Order Day
* Create By : Erwin Idamsyach Putra
* 23 April 2016
*/
class report extends CI_Controller{
	function __construct(){
		parent::__construct();

		if(!isLogin()){
			goLogin();
		}
	}
	
	public function index(){
		$data['filelist'] = 'report/front';
		$data['title'] = 'Brief Report';
		$data['title_menu'] = 'Admin';
		$data['menu'] = 'Brief Report';

		getHTMLWeb($data);
	}
	
	public function full_report(){
		$data['filelist'] = 'report/front';
		$data['title'] = 'Identification Report';
		$data['title_menu'] = 'Admin';
		$data['menu'] = 'Full Report';

		getHTMLWeb($data);
	}
	
	public function grading(){
		$data['filelist'] = 'report/front';
		$data['title'] = 'Diamond Grading';
		$data['title_menu'] = 'Admin';
		$data['menu'] = 'Diamond Grading';

		getHTMLWeb($data);
	}
	
	public function taksiran(){
		$data['filelist'] = 'report/front';
		$data['title'] = 'Taksiran';
		$data['title_menu'] = 'Admin';
		$data['menu'] = 'Taksiran';

		getHTMLWeb($data);
	}
	
	public function rekapitulasi(){
		$data['filelist'] = 'report/rekapitulasi';
		$data['title'] = 'Rekapitulasi';
		$data['title_menu'] = 'Admin';
		$data['menu'] = 'Rekapitulasi';

		getHTMLWeb($data);
	}
	
	public function barangtertinggal(){
		$data['filelist'] = 'report/barangtertinggal';
		$data['title'] = 'Barang Tertinggal';
		$data['title_menu'] = 'Admin';
		$data['menu'] = 'Barang Tertinggal';

		getHTMLWeb($data);
	}
	
	public function gemstone(){
		$data['filelist'] = 'report/gemstone';
		$data['title'] = 'Gemstone';
		$data['title_menu'] = 'Admin';
		$data['menu'] = 'Report Gemstone';

		getHTMLWeb($data);
	}
	
	public function print_report_sertificate($from, $to, $menu, $search=""){
		$this->load->library('fpdf');
		
		$pdf = new FPDF('P','mm','A4');
		$pdf->SetFont('Arial','B', 10);
		$pdf->AddPage();
		
		$tgl2 = $to;
		
		$where = " AND step.create_date >= '".$from." 12:00:00 AM' AND step.create_date <= '".$to." 11:59:59 PM' ";
		$queryCabang = $this->db->query("SELECT tb_store.store
												FROM step
												LEFT JOIN tb_login ON tb_login.id = step.create_by
												LEFT JOIN tb_store ON tb_store.kode = tb_login.store
												WHERE ISNULL(step.delete_by)
												AND !ISNULL(step.create_by)
												".$where."
												LIMIT 0,1");
		$cabang = "";
		foreach($queryCabang->result() as $tmpCabang) {
			$cabang = $tmpCabang->store;
		}
		
		$pdf->Cell(180, 7, 'PT Pegadaian (Persero)','',0,'L');
		$pdf->Ln();
		$pdf->Cell(180, 7, 'Laboratorium Gemologi','',0,'L');
		$pdf->Ln();
		$pdf->Cell(180, 7, 'Cabang : '.$cabang,'',0,'L');
		$pdf->Ln(17);
		
		$menu = str_replace('-', ' ',$menu);
		if($menu!="Taksiran"){
		$pdf->Cell(180, 7, 'DAFTAR SERTIFIKASI', 0, 0, 'C');
		$pdf->Ln();
		}
		if($menu=="Taksiran"){
		$pdf->Cell(180, 7, 'DAFTAR JASA TAKSIRAN', 0, 0, 'C');
		}else{
		$pdf->Cell(180, 7, ($menu=="Full Report")?"IDENTIFICATION REPORT":strtoupper($menu), 0, 0, 'C');
		}
		$pdf->Ln();
		$pdf->Cell(180, 7, 'Tanggal : '.$from.' s/d '.$tgl2, 0, 0, 'C');
		$pdf->Ln(12);

		$width = array(10, 47, 20, 40, 48, 25);
		$pdf->SetFont('Arial','B', 10);
		$pdf->Cell($width[0], 7, "No", 1, 0, 'C');
		$pdf->Cell($width[1], 7, "Nasabah", 1, 0, 'C');
		$pdf->Cell($width[2], 7, "Tanggal", 1, 0, 'C');
		$pdf->Cell($width[3], 7, "No. Sertifikat", 1, 0, 'C');
		if($menu=="Diamond Grading"){
		$pdf->Cell($width[4], 7, "Berat", 1, 0, 'C');
		}else if($menu=="Taksiran"){
		$pdf->Cell($width[4], 7, "Taksiran", 1, 0, 'C');
		}else{
		$pdf->Cell($width[4], 7, "Hasil Sertifikasi", 1, 0, 'C');
		}
		$pdf->Cell($width[5], 7, "Pendapatan", 1, 0, 'C');
		$pdf->Ln();
		
		$to = date('Y-m-d',strtotime($to . "+1 days"));
		
		if($menu=="Brief Report"){
			$kode = "BR";
			if($search==""){
				$where = " AND DATE_FORMAT(tb_lab_desk.finish,'%d-%m-%Y')=DATE_FORMAT(NOW(),'%d-%m-%Y') AND tb_front_desk.gem_card!='' ";
			}else{
				$where = " AND tb_lab_desk.finish >= '".$from." 12:00:00 AM' AND tb_lab_desk.finish <= '".$to." 11:59:59 PM'
						 AND tb_front_desk.gem_card!='' ";
			}				
			$query = $this->db->query("SELECT tb_lab_desk.*,
												tb_front_desk.certificate, tb_front_desk.gem_card, tb_front_desk.dia_grading,
												tb_member.first_name, tb_lab_desk.obj_natural,
												step.no_brief,
												tb_service.harga harga_sert
												FROM tb_lab_desk
												LEFT JOIN tb_member ON tb_member.id_member = tb_lab_desk.id_member
												LEFT JOIN tb_front_desk ON tb_front_desk.id_object = tb_lab_desk.id_object
												LEFT JOIN step ON step.id_object = tb_lab_desk.id_object
												JOIN tb_service ON tb_service.nama = tb_front_desk.gem_card
												WHERE ISNULL(tb_lab_desk.delete_by)
												AND step.pay='Y'
												AND tb_front_desk.store='".sessionValue('kode_store')."' ".$where);
		}else if($menu=="Full Report"){
			$kode = "FR";
			if($search==""){
				$where = " AND DATE_FORMAT(tb_lab_desk.finish,'%d-%m-%Y')=DATE_FORMAT(NOW(),'%d-%m-%Y') AND tb_front_desk.certificate!='' ";
			}else{
				$where = " AND tb_lab_desk.finish >= '".$from." 12:00:00 AM' AND tb_lab_desk.finish <= '".$to." 11:59:59 PM'
						 AND tb_front_desk.certificate!='' ";
			}
			//$where = " AND tb_lab_desk.finish >= '".$from." 12:00:00 AM' AND tb_lab_desk.finish <= '".$to." 11:59:59 PM' ";
			$query = $this->db->query("SELECT tb_lab_desk.*,
												tb_front_desk.certificate, tb_front_desk.gem_card, tb_front_desk.dia_grading,
												tb_member.first_name,
												step.no_sertifikat,
												tb_service.harga harga_sert
												FROM tb_lab_desk
												LEFT JOIN tb_member ON tb_member.id_member = tb_lab_desk.id_member
												LEFT JOIN tb_front_desk ON tb_front_desk.id_object = tb_lab_desk.id_object
												LEFT JOIN step ON step.id_object = tb_lab_desk.id_object
												JOIN tb_service ON tb_service.nama = tb_front_desk.certificate
												WHERE ISNULL(tb_lab_desk.delete_by)
												AND step.pay='Y'
												AND tb_front_desk.store='".sessionValue('kode_store')."' ".$where);
		}else if($menu=="Diamond Grading"){
			$kode = "FR";
			if($search==""){
				$where = " AND DATE_FORMAT(tb_lab_grading.finish,'%d-%m-%Y')=DATE_FORMAT(NOW(),'%d-%m-%Y') AND tb_front_desk.dia_grading!='' ";
			}else{
				$where = " AND tb_lab_grading.finish >= '".$from." 12:00:00 AM' AND tb_lab_grading.finish <= '".$to." 11:59:59 PM'
						 AND tb_front_desk.dia_grading!='' ";
			}
			//$where = " AND tb_lab_desk.finish >= '".$from." 12:00:00 AM' AND tb_lab_desk.finish <= '".$to." 11:59:59 PM' ";
			$query = $this->db->query("SELECT tb_lab_grading.*,
												tb_front_desk.certificate, tb_front_desk.gem_card, tb_front_desk.dia_grading,
												tb_member.first_name,
												step.no_grading,
												tb_service.harga harga_sert
												FROM tb_lab_grading
												LEFT JOIN tb_member ON tb_member.id_member = tb_lab_grading.id_member
												LEFT JOIN tb_front_desk ON tb_front_desk.id_object = tb_lab_grading.id_object
												LEFT JOIN step ON step.id_object = tb_lab_grading.id_object
												JOIN tb_service ON tb_service.nama = tb_front_desk.dia_grading
												WHERE ISNULL(tb_lab_grading.delete_by)
												AND step.pay='Y'
												AND tb_front_desk.store='".sessionValue('kode_store')."' ".$where);
		}else if($menu=="Taksiran"){
			$kode = "JT";
			if($search==""){
				$where = " AND DATE_FORMAT(tb_taksiran.create_date,'%d-%m-%Y')=DATE_FORMAT(NOW(),'%d-%m-%Y') ";
			}else{
				$where = " AND tb_taksiran.create_date >= '".$from." 12:00:00 AM' AND tb_taksiran.create_date <= '".$to." 11:59:59 PM' ";
			}
			//$where = " AND tb_taksiran.create_date >= '".$from." 12:00:00 AM' AND tb_taksiran.create_date <= '".$to." 11:59:59 PM' ";
			$query = $this->db->query("SELECT tb_taksiran.*, tb_taksiran.create_date finish,
												tb_member.first_name,
												step.no_taksiran
												FROM tb_taksiran
												LEFT JOIN tb_member ON tb_member.id_member = tb_taksiran.id_member
												LEFT JOIN step ON step.id_object = tb_taksiran.id
												LEFT JOIN tb_login ON tb_login.id = tb_taksiran.create_by
												WHERE ISNULL(tb_taksiran.delete_by) 
												AND step.pay='Y'
												AND tb_login.store='".sessionValue('kode_store')."'
												".$where." GROUP BY tb_taksiran.id_order ");
		}
		$pdf->SetFont('Arial','', 10);
		$no = 1;
		$total = 0;
        foreach($query->result() as $tmp) {
			if($menu=="Brief Report"){
				$kode = $tmp->no_brief;
				if($tmp->obj_natural!="NATURAL"){
					$mem_pri = mysql_query("SELECT harga FROM tb_service WHERE kode='BRS'");
					$pri_mem = mysql_fetch_array($mem_pri);
				}else{
					$mem_pri = mysql_query("SELECT harga FROM tb_service WHERE kode='BR'");
					$pri_mem = mysql_fetch_array($mem_pri);
				}
				$tmp->harga_sert = $pri_mem['harga'];
			}else if($menu=="Full Report"){
				$kode = $tmp->no_sertifikat;
			}if($menu=="Diamond Grading"){
				$kode = $tmp->no_grading;
				$getHarga = $this->db->query("SELECT harga
								FROM master_harga_grading
								WHERE berat2 >= ".$tmp->obj_weight." ORDER BY ID ASC LIMIT 0,1");
						   
				foreach ($getHarga->result() as $tmpHarga) {
					$tmp->harga_sert = $tmpHarga->harga;
				}
			}if($menu=="Taksiran"){
				$harga_total_taksiran = 0;
						  $kode = $tmp->no_taksiran;
							$getHDLE = $this->db->query("SELECT * FROM master_harga_emas WHERE id='1'");
							$hdle = 0;
							foreach ($getHDLE->result() as $hsp) {
								$hdle = $hsp->harga_emas;
							}
							$getEmas = $this->db->query("SELECT * FROM tb_taksiran
														LEFT JOIN master_logam ON master_logam.id = tb_taksiran.jenis_logam
														WHERE tb_taksiran.id_order='".$tmp->id_order."' AND tb_taksiran.jenis_logam='1'");
							foreach ($getEmas->result() as $key) {
								$harga_taksiran = 1.25/100*(($key->berat_bersih*($key->karatase_emas/24))*$hdle); // $key->jumlah_perhiasan*
								$harga_total_taksiran = $harga_total_taksiran + $harga_taksiran;
							} 
							
							$getBerlian = $this->db->query("SELECT berat_total_permata FROM tb_taksiran
								WHERE tb_taksiran.id_order='".$tmp->id_order."' ");							
							foreach ($getBerlian->result() as $key) {
								if($key->berat_total_permata == ""){
									$berat_total = $berat_total + 0;
								}else{
									$berat_total = $berat_total + $key->berat_total_permata;
								}
								if($berat_total>0){
									$getHarga = $this->db->query("SELECT harga
																FROM `master_tarif_taksiran`
																WHERE berat2 >= ".$berat_total." ORDER BY ID ASC LIMIT 0,1");
																
									foreach ($getHarga->result() as $tmpHarga) {
										$harga_berlian = $harga_berlian + $tmpHarga->harga;
									}
									$harga_total_taksiran = $harga_total_taksiran + $harga_berlian;
								}
							}
			}
			
			$pdf->Cell($width[0], 7, $no++, 'LBR', 0, 'C');
			$pdf->Cell($width[1], 7, $tmp->first_name, 'BR', 0, 'L');
			$pdf->Cell($width[2], 7, date('d-m-Y', strtotime($tmp->finish)), 'BR', 0, 'L');
			$pdf->Cell($width[3], 7, $kode, 'BR', 0, 'L');
			if($menu=="Diamond Grading"){
			$pdf->Cell($width[4], 7, $tmp->obj_weight, 'BR', 0, 'R');
			}else if($menu=="Taksiran"){
			$pdf->Cell($width[4], 7, number_format($tmp->taksiran,0,',','.'), 'BR', 0, 'R');
			}else{
			$pdf->Cell($width[4], 7, ucwords(strtolower($tmp->variety)), 'BR', 0, 'L'); // $tmp->obj_natural.' '.
			}
			if($menu=="Taksiran"){
			$pdf->Cell($width[5], 7, number_format($harga_total_taksiran,0,',','.'), 'BR', 0, 'R');	
			$total = $total + $harga_total_taksiran;
			}else{
			$pdf->Cell($width[5], 7, number_format($tmp->harga_sert,0,',','.'), 'BR', 0, 'R');	
			$total = $total + $tmp->harga_sert;
			}
			
			$pdf->Ln();
			
			
        }
		
		
		$pdf->Cell($width[0]+$width[1]+$width[2]+$width[3]+$width[4], 7, "Total Pendapatan", 'LBR', 0, 'C');
		$pdf->Cell($width[5], 7, number_format($total,0,',','.'), 'BR', 0, 'R');
		$pdf->Ln();
		
		$pdf->Image('asset/images/Logo.png', 145, 5, 50, 25);
		$pdf->SetFont('helvetica','', 10);
		$pdf->SetXY(147, 28);
		$pdf->MultiCell(45, 5, sessionValue('alamat'),'','L');
		$pdf->Ln();
		//$pdf->SetX(147);
		//$pdf->Cell(0, 5, 'Jakarta Pusat, 10430');
		//$pdf->Ln();
				
		$width_bot = array(95,95,5);
		$pdf->SetY(230);
		$pdf->Cell($width_bot[0], 5, 'Diperiksa Tanggal : ', '', 0, 'C');
		$pdf->Cell($width_bot[1], 5, 'JAKARTA PUSAT, '. date('d-m-Y'), '', 0, 'C');
		$pdf->Ln();
		$pdf->Cell($width_bot[0], 5, 'Pengelola Cabang G-Lab', '', 0, 'C');
		$pdf->Cell($width_bot[1], 5, 'Dibuat Oleh,', '', 0, 'C');
		$pdf->Ln(15);
		$pdf->Cell($width_bot[0], 5, 'Gemologist', '', 0, 'C');
		$pdf->Cell($width_bot[1], 5, 'Staff G-Lab', '', 0, 'C');
		
		$pdf->Output();
	}
	
	public function print_report_rekapitulasi($from, $to, $menu, $search=""){
		$this->load->library('fpdf');
		
		$pdf = new FPDF('P','mm','A4');
		$pdf->SetFont('Arial','B', 10);
		$pdf->AddPage();
		
		$tgl2 = $to;		
		
		$where = " AND step.create_date >= '".$from." 12:00:00 AM' AND step.create_date <= '".$to." 11:59:59 PM' ";
		$queryCabang = $this->db->query("SELECT tb_store.store
												FROM step
												LEFT JOIN tb_login ON tb_login.id = step.create_by
												LEFT JOIN tb_store ON tb_store.kode = tb_login.store
												WHERE ISNULL(step.delete_by)
												AND !ISNULL(step.create_by)
												".$where."
												LIMIT 0,1");
		$cabang = "";
		foreach($queryCabang->result() as $tmpCabang) {
			$cabang = $tmpCabang->store;
		}
		
		$pdf->Cell(180, 7, 'PT Pegadaian (Persero)','',0,'L');
		$pdf->Ln();
		$pdf->Cell(180, 7, 'Laboratorium Gemologi','',0,'L');
		$pdf->Ln();
		$pdf->Cell(180, 7, 'Cabang : '.$cabang,'',0,'L');
		$pdf->Ln(17);
		
		$menu = str_replace('-', ' ',$menu);
		$pdf->Cell(180, 7, 'REKAPITULASI SERTIFIKASI DAN JASA TAKSIRAN', 0, 0, 'C');
		$pdf->Ln();
		$pdf->Cell(180, 7, 'Tanggal : '.$from.' s/d '.$tgl2, 0, 0, 'C');
		$pdf->Ln(12);
		
		if($search==""){
			$to = date('Y-m-d');
		}else{
			$to = date('Y-m-d',strtotime($to . "+1 days"));
		}
		
		if($search==""){
			$where = " AND DATE_FORMAT(tb_lab_desk.finish,'%d-%m-%Y')=DATE_FORMAT(NOW(),'%d-%m-%Y') AND tb_front_desk.gem_card!='' ";
			$where2 = " AND DATE_FORMAT(tb_front_desk.create_date,'%d-%m-%Y')=DATE_FORMAT(NOW(),'%d-%m-%Y') AND tb_front_desk.gem_card!='' ";
					
			$where3 = " AND DATE_FORMAT(tb_lab_desk.finish,'%d-%m-%Y')=DATE_FORMAT(NOW(),'%d-%m-%Y') AND tb_front_desk.certificate!='' ";
			$where4 = " AND DATE_FORMAT(tb_front_desk.create_date,'%d-%m-%Y')=DATE_FORMAT(NOW(),'%d-%m-%Y') AND tb_front_desk.certificate!='' ";
			
			$where5 = " AND DATE_FORMAT(tb_lab_grading.finish,'%d-%m-%Y')=DATE_FORMAT(NOW(),'%d-%m-%Y') AND tb_front_desk.dia_grading!='' ";
			$where6 = " AND DATE_FORMAT(tb_front_desk.create_date,'%d-%m-%Y')=DATE_FORMAT(NOW(),'%d-%m-%Y') AND tb_front_desk.dia_grading!='' ";
			
			$whereTaksiran = " AND DATE_FORMAT(tb_taksiran.create_date,'%Y-%m-%d')=DATE_FORMAT(NOW(),'%Y-%m-%d') ";
		}else{		
			$whereTaksiran = " AND tb_taksiran.create_date >= '$from 12:00:00 AM' AND tb_taksiran.create_date <= '$to 11:59:59 PM' ";
			
			$where = " AND tb_lab_desk.finish >= '$from 12:00:00 AM' AND tb_lab_desk.finish <= '$to 11:59:59 PM' 
						AND tb_front_desk.gem_card!='' ";
			$where2 = " AND tb_front_desk.create_date >= '$from 12:00:00 AM' AND tb_front_desk.create_date <= '$to 11:59:59 PM' 
							AND tb_front_desk.gem_card!='' ";
							
			$where3 = " AND tb_lab_desk.finish >= '$from 12:00:00 AM' AND tb_lab_desk.finish <= '$to 11:59:59 PM' 
							AND tb_front_desk.certificate!='' ";
			$where4 = " AND tb_front_desk.create_date >= '$from 12:00:00 AM' AND tb_front_desk.create_date <= '$to 11:59:59 PM' 
							AND tb_front_desk.certificate!='' ";
							
			$where5 = " AND tb_lab_grading.finish >= '$from 12:00:00 AM' AND tb_lab_grading.finish <= '$to 11:59:59 PM' 
							AND tb_front_desk.dia_grading!='' ";
			$where6 = " AND tb_front_desk.create_date >= '$from 12:00:00 AM' AND tb_front_desk.create_date <= '$to 11:59:59 PM' 
							AND tb_front_desk.dia_grading!='' ";
		}
		
					$get_brief_report = $this->db->query("SELECT COUNT(tb_lab_desk.id_member) jumlah,
												SUM(tb_service.harga) harga
												FROM tb_lab_desk
												LEFT JOIN tb_member ON tb_member.id_member = tb_lab_desk.id_member
												LEFT JOIN tb_front_desk ON tb_front_desk.id_object = tb_lab_desk.id_object
												LEFT JOIN step ON step.id_object = tb_lab_desk.id_object
												JOIN tb_service ON tb_service.nama = tb_front_desk.gem_card
												WHERE ISNULL(tb_lab_desk.delete_by) 
												AND step.pay='Y'
												AND tb_front_desk.store='".sessionValue('kode_store')."'
												".$where." GROUP BY tb_lab_desk.id_member"); 
					$get_brief_report2 = $this->db->query("SELECT tb_front_desk.id_member, tb_lab_desk.obj_natural
												FROM tb_front_desk
												LEFT JOIN tb_lab_desk ON tb_lab_desk.id_object = tb_front_desk.id_object
												LEFT JOIN step ON step.id_object = tb_lab_desk.id_object
												WHERE ISNULL(tb_front_desk.delete_by) 
												AND step.pay='Y'
												AND tb_front_desk.store='".sessionValue('kode_store')."'
												".$where2);
					$jml_brief = 0;
					$jml_transaksi_brief = 0;
					$harga_brief = 0;
					$x = 0;
					foreach ($get_brief_report->result() as $get) {
						$jml_brief = $jml_brief + $get->jumlah;
					}
					foreach ($get_brief_report2->result() as $get) {
						$x++;
						$jml_transaksi_brief = $x;
						if($get->obj_natural!="NATURAL"){
							$mem_pri = mysql_query("SELECT harga FROM tb_service WHERE kode='BRS'");
							$pri_mem = mysql_fetch_array($mem_pri);
						}else{
							$mem_pri = mysql_query("SELECT harga FROM tb_service WHERE kode='BR'");
							$pri_mem = mysql_fetch_array($mem_pri);
						}
						$harga_brief = $harga_brief + $pri_mem['harga'];
					}
					
					$get_full_report = $this->db->query("SELECT COUNT(tb_lab_desk.id_member) jumlah,
												SUM(tb_service.harga) harga
												FROM tb_lab_desk
												LEFT JOIN tb_front_desk ON tb_front_desk.id_object = tb_lab_desk.id_object
												LEFT JOIN step ON step.id_object = tb_lab_desk.id_object
												JOIN tb_service ON tb_service.nama = tb_front_desk.certificate
												WHERE ISNULL(tb_lab_desk.delete_by) 
												AND step.pay='Y' 
												AND tb_front_desk.store='".sessionValue('kode_store')."'
												".$where3." GROUP BY tb_lab_desk.id_member");
					$get_full_report2 = $this->db->query("SELECT tb_front_desk.id_member
												FROM tb_front_desk
												LEFT JOIN step ON step.id_object = tb_front_desk.id_object
												WHERE ISNULL(tb_front_desk.delete_by) 
												AND step.pay='Y'
												AND tb_front_desk.store='".sessionValue('kode_store')."'
												".$where4);
					$jml_full = 0;
					$jml_transaksi_full = 0;
					$harga_full = 0;
					$x = 0;
					foreach ($get_full_report->result() as $get) {
						$jml_full = $jml_full + $get->jumlah;
						$harga_full = $harga_full + $get->harga;
					}
					foreach ($get_full_report2->result() as $get) {
						$x++;
						$jml_transaksi_full = $x;
					}
					
					$get_diamond_grading = $this->db->query("SELECT tb_lab_grading.id_member,
												tb_lab_grading.obj_weight
												FROM tb_lab_grading
												LEFT JOIN tb_front_desk ON tb_front_desk.id_object = tb_lab_grading.id_object
												LEFT JOIN step ON step.id_object = tb_lab_grading.id_object
												WHERE ISNULL(tb_lab_grading.delete_by) 
												AND step.pay='Y'
												AND tb_front_desk.store='".sessionValue('kode_store')."'
												".$where5);
					$get_diamond_grading2 = $this->db->query("SELECT COUNT(tb_front_desk.id_member) jumlah
												FROM tb_front_desk
												LEFT JOIN step ON step.id_object = tb_front_desk.id_object
												WHERE ISNULL(tb_front_desk.delete_by) 
												AND step.pay='Y'
												".$where6."
												AND tb_front_desk.store='".sessionValue('kode_store')."'
												GROUP BY tb_front_desk.id_member");
					$jml_grading = 0;
					$jml_transaksi_grading = 0;
					$harga_grading = 0;
					$x = 0;
					foreach ($get_diamond_grading->result() as $get) {
						$x++;
						$jml_transaksi_grading = $jml_transaksi_grading + $x;
						$getHarga = $this->db->query("SELECT harga
								FROM master_harga_grading
								WHERE berat2 >= ".$get->obj_weight." ORDER BY ID ASC LIMIT 0,1");
						   
						foreach ($getHarga->result() as $tmpHarga) {
							$harga_grading = $harga_grading + $tmpHarga->harga;
						}
						//$harga_grading = $get->harga;
					}
					foreach ($get_diamond_grading2->result() as $get) {
						$jml_grading = $jml_grading + $get->jumlah;
					}
					
					// TAKSIRAN
					$getHDLE = $this->db->query("SELECT * FROM master_harga_emas WHERE id='1'");
					$hdle = 0;
					foreach ($getHDLE->result() as $hsp) {
						$hdle = $hsp->harga_emas;
					}
					$getEmas = $this->db->query("SELECT * FROM tb_taksiran
												LEFT JOIN master_logam ON master_logam.id = tb_taksiran.jenis_logam
												LEFT JOIN tb_login ON tb_login.id = tb_taksiran.create_by
												LEFT JOIN step ON step.id_object = tb_taksiran.id
												WHERE tb_taksiran.jenis_logam='1'
												AND ISNULL(tb_taksiran.delete_by) 
												AND step.pay='Y'
												AND tb_login.store='".sessionValue('kode_store')."'
												".$whereTaksiran);
										
					$harga_total = 0;
					foreach ($getEmas->result() as $key) {
						$harga_taksiran = 1.25/100*(($key->berat_bersih*($key->karatase_emas/24))*$hdle); // $key->jumlah_perhiasan*						
						$harga_total = $harga_total + $harga_taksiran;
					} 
					$getBerlian = $this->db->query("SELECT berat_total_permata FROM tb_taksiran
						LEFT JOIN step ON step.id_object = tb_taksiran.id
						WHERE tb_taksiran.id_order='".$get->id_order."' AND step.pay='Y' ");							
					foreach ($getBerlian->result() as $key) {
						if($key->berat_total_permata == ""){
							$berat_total = $berat_total + 0;
						}else{
							$berat_total = $berat_total + $key->berat_total_permata;
						}
						if($berat_total>0){
							$getHarga = $this->db->query("SELECT harga
														FROM `master_tarif_taksiran`
														WHERE berat2 >= ".$berat_total." ORDER BY ID ASC LIMIT 0,1");
														
							foreach ($getHarga->result() as $tmpHarga) {
								$harga_berlian = $harga_berlian + $tmpHarga->harga;
							}
							$harga_total = $harga_total + $harga_berlian;
						}
					} 
					
					$get_taksiran = $this->db->query("SELECT COUNT(tb_taksiran.id_member) jumlah
												FROM tb_taksiran
												LEFT JOIN tb_login ON tb_login.id = tb_taksiran.create_by
												LEFT JOIN step ON step.id_object = tb_taksiran.id
												WHERE ISNULL(tb_taksiran.delete_by) 
												AND step.pay='Y'
												AND tb_login.store='".sessionValue('kode_store')."'
												".$whereTaksiran." GROUP BY tb_taksiran.id_member"); 
					$get_taksiran2 = $this->db->query("SELECT tb_taksiran.id_member
												FROM tb_taksiran
												LEFT JOIN tb_login ON tb_login.id = tb_taksiran.create_by
												LEFT JOIN step ON step.id_object = tb_taksiran.id
												WHERE ISNULL(tb_taksiran.delete_by) 
												AND step.pay='Y'
												AND tb_login.store='".sessionValue('kode_store')."'
												".$whereTaksiran);
					$jml_taksiran = 0;
					$jml_transaksi_taksiran = 0;
					$harga_taksiran = roundNearestHundredUp($harga_total);
					$x = 0;
					foreach ($get_taksiran->result() as $get) {
						$jml_taksiran = $jml_taksiran + $get->jumlah;
					}
					foreach ($get_taksiran2->result() as $get) {
						$x++;
						$jml_transaksi_taksiran = $x;
					}

		$width = array(10, 57, 35, 35, 48, 30);
		$pdf->SetFont('Arial','B', 10);
		$pdf->Cell($width[0], 7, "No", 1, 0, 'C');
		$pdf->Cell($width[1], 7, "Produk", 1, 0, 'C');
		$pdf->Cell($width[2], 7, "Jumlah Nasabah", 1, 0, 'C');
		$pdf->Cell($width[3], 7, "Jumlah Transaksi", 1, 0, 'C');
		$pdf->Cell($width[4], 7, "Pendapatan", 1, 0, 'C');
		$pdf->Ln();
		
		$pdf->SetFont('Arial','', 10);
		$no = 1;
		$total = 0;
			
		$pdf->Cell($width[0], 7, $no++, 'LBR', 0, 'C');
		$pdf->Cell($width[1], 7, "Brief Report", 'BR', 0, 'L');
		$pdf->Cell($width[2], 7, $jml_brief.'', 'BR', 0, 'C');
		$pdf->Cell($width[3], 7, $jml_transaksi_brief.'', 'BR', 0, 'C');			
		$pdf->Cell($width[4], 7, number_format($harga_brief,0,',','.'), 'BR', 0, 'R');			
		$total = $total + $harga_brief;
		$pdf->Ln();	
		$pdf->Cell($width[0], 7, $no++, 'LBR', 0, 'C');
		$pdf->Cell($width[1], 7, "Identification Report", 'BR', 0, 'L');
		$pdf->Cell($width[2], 7, $jml_full.'', 'BR', 0, 'C');
		$pdf->Cell($width[3], 7, $jml_transaksi_full.'', 'BR', 0, 'C');			
		$pdf->Cell($width[4], 7, number_format($harga_full,0,',','.'), 'BR', 0, 'R');			
		$total = $total + $harga_full;
		$pdf->Ln();	
		$pdf->Cell($width[0], 7, $no++, 'LBR', 0, 'C');
		$pdf->Cell($width[1], 7, "Diamond Grading Report", 'BR', 0, 'L');
		$pdf->Cell($width[2], 7, $jml_grading.'', 'BR', 0, 'C');
		$pdf->Cell($width[3], 7, $jml_transaksi_grading.'', 'BR', 0, 'C');			
		$pdf->Cell($width[4], 7, number_format($harga_grading,0,',','.'), 'BR', 0, 'R');			
		$total = $total + $harga_grading;
		$pdf->Ln();	
		$pdf->Cell($width[0], 7, $no++, 'LBR', 0, 'C');
		$pdf->Cell($width[1], 7, "Jasa Taksiran", 'BR', 0, 'L');
		$pdf->Cell($width[2], 7, $jml_taksiran.'', 'BR', 0, 'C');
		$pdf->Cell($width[3], 7, $jml_transaksi_taksiran.'', 'BR', 0, 'C');			
		$pdf->Cell($width[4], 7, number_format($harga_taksiran,0,',','.'), 'BR', 0, 'R');			
		$total = $total + $harga_taksiran;
		$pdf->Ln();
		
		$pdf->Cell($width[0]+$width[1]+$width[2]+$width[3], 7, "Total Pendapatan", 'LBR', 0, 'C');
		$pdf->Cell($width[4], 7, number_format($total,0,',','.'), 'BR', 0, 'R');
		$pdf->Ln();
		
		$pdf->Image('asset/images/Logo.png', 145, 5, 50, 25);
		$pdf->SetFont('helvetica','', 10);
		$pdf->SetXY(147, 28);
		$pdf->MultiCell(45, 5, sessionValue('alamat'),'','L');
		$pdf->Ln();
		//$pdf->SetX(147);
		//$pdf->Cell(0, 5, 'Jakarta Pusat, 10430');
		//$pdf->Ln();
				
		$width_bot = array(95,95,5);
		$pdf->SetY(230);
		$pdf->Cell($width_bot[0], 5, 'Diperiksa Tanggal : ', '', 0, 'C');
		$pdf->Cell($width_bot[1], 5, 'JAKARTA PUSAT, '. date('d-m-Y'), '', 0, 'C');
		$pdf->Ln();
		$pdf->Cell($width_bot[0], 5, 'Pengelola Cabang G-Lab', '', 0, 'C');
		$pdf->Cell($width_bot[1], 5, 'Dibuat Oleh,', '', 0, 'C');
		$pdf->Ln(15);
		$pdf->Cell($width_bot[0], 5, 'Gemologist', '', 0, 'C');
		$pdf->Cell($width_bot[1], 5, 'Staff G-Lab', '', 0, 'C');
		
		$pdf->Output();
	}
	
	public function print_report_barang_tertinggal($from, $to, $menu, $search=""){
		$this->load->library('fpdf');
		
		$pdf = new FPDF('P','mm','A4');
		$pdf->SetFont('Arial','B', 10);
		$pdf->AddPage();
		
		$where = " AND step.create_date >= '".$from." 12:00:00 AM' AND step.create_date <= '".$to." 11:59:59 PM' ";
		$queryCabang = $this->db->query("SELECT tb_store.store
												FROM step
												LEFT JOIN tb_login ON tb_login.id = step.create_by
												LEFT JOIN tb_store ON tb_store.kode = tb_login.store
												WHERE ISNULL(step.delete_by)
												AND !ISNULL(step.create_by)
												".$where."
												LIMIT 0,1");
		$cabang = "";
		foreach($queryCabang->result() as $tmpCabang) {
			$cabang = $tmpCabang->store;
		}
		
		$pdf->Cell(180, 7, 'PT Pegadaian (Persero)','',0,'L');
		$pdf->Ln();
		$pdf->Cell(180, 7, 'Laboratorium Gemologi','',0,'L');
		$pdf->Ln();
		$pdf->Cell(180, 7, 'Cabang : '.$cabang,'',0,'L');
		$pdf->Ln(17);
		
		$menu = str_replace('-', ' ',$menu);
		
		$pdf->Cell(180, 7, 'DAFTAR BARANG NASABAH YANG BELUM DIAMBIL', 0, 0, 'C');
		$pdf->Ln();
		$pdf->Cell(180, 7, 'Tanggal : '.$from.' s/d '.$to, 0, 0, 'C');
		$pdf->Ln(12);

		$width = array(8, 50, 18, 32, 25, 25, 33);
		$pdf->SetFont('Arial','B', 9);
		$pdf->Cell($width[0], 7, "No", 1, 0, 'C');
		$pdf->Cell($width[2], 7, "Tanggal", 1, 0, 'C');
		$pdf->Cell($width[1], 7, "Nasabah", 1, 0, 'C');
		$pdf->Cell($width[3], 7, "No. BPS/BPT", 1, 0, 'C');
		$pdf->Cell($width[4], 7, "Order", 1, 0, 'C');
		$pdf->Cell($width[5], 7, "No. Sert./SHT", 1, 0, 'C');
		$pdf->Cell($width[6], 7, "Keterangan", 1, 0, 'C');
		$pdf->Ln();
		
		$to = date('Y-m-d',strtotime($to . "+1 days"));
		
		if($search==""){
			$where = " AND DATE_FORMAT(step.create_date,'%d-%m-%Y')=DATE_FORMAT(NOW(),'%d-%m-%Y') AND status_barang='TERTINGGAL' ";
		}else{
			$where = " AND step.create_date >= '".$from." 12:00:00 AM' AND step.create_date <= '".$to." 11:59:59 PM'  
						AND status_barang='TERTINGGAL' ";
		}
							
		$query = $this->db->query("SELECT tb_lab_desk.*, step.id_order, step.no_sertifikat,
												tb_front_desk.certificate, tb_front_desk.gem_card, tb_front_desk.dia_grading,
												tb_member.first_name,
												step.no_brief
												FROM tb_lab_desk
												LEFT JOIN tb_member ON tb_member.id_member = tb_lab_desk.id_member
												LEFT JOIN tb_front_desk ON tb_front_desk.id_object = tb_lab_desk.id_object
												LEFT JOIN step ON step.id_object = tb_lab_desk.id_object
												WHERE ISNULL(tb_lab_desk.delete_by) AND tb_front_desk.certificate!=''
												AND tb_front_desk.store='".sessionValue('kode_store')."'
												".$where);					
		
		$pdf->SetFont('Arial','', 8);
		$no = 1;
		$total = 0;
        foreach($query->result() as $tmp) {			
			$pdf->Cell($width[0], 7, $no++, 'LBR', 0, 'C');
			$pdf->Cell($width[2], 7, date('d-m-Y', strtotime($tmp->finish)), 'BR', 0, 'C');
			$pdf->Cell($width[1], 7, $tmp->first_name, 'BR', 0, 'L');
			$pdf->Cell($width[3], 7, $tmp->id_order, 'BR', 0, 'C');
			$pdf->Cell($width[4], 7, "FULL REPORT", 'BR', 0, 'L');
			$pdf->Cell($width[5], 7, $tmp->no_sertifikat, 'BR', 0, 'L');	
			$pdf->Cell($width[6], 7, "", 'BR', 0, 'L');	
			$pdf->Ln();
        }
		
		$query = $this->db->query("SELECT tb_lab_desk.*, step.id_order, step.no_brief,
												tb_front_desk.certificate, tb_front_desk.gem_card, tb_front_desk.dia_grading,
												tb_member.first_name,
												step.no_brief
												FROM tb_lab_desk
												LEFT JOIN tb_member ON tb_member.id_member = tb_lab_desk.id_member
												LEFT JOIN tb_front_desk ON tb_front_desk.id_object = tb_lab_desk.id_object
												LEFT JOIN step ON step.id_object = tb_lab_desk.id_object
												WHERE ISNULL(tb_lab_desk.delete_by) AND tb_front_desk.gem_card!=''
												AND tb_front_desk.store='".sessionValue('kode_store')."'
												".$where);					
		
        foreach($query->result() as $tmp) {			
			$pdf->Cell($width[0], 7, $no++, 'LBR', 0, 'C');
			$pdf->Cell($width[2], 7, date('d-m-Y', strtotime($tmp->finish)), 'BR', 0, 'C');
			$pdf->Cell($width[1], 7, $tmp->first_name, 'BR', 0, 'L');
			$pdf->Cell($width[3], 7, $tmp->id_order, 'BR', 0, 'C');
			$pdf->Cell($width[4], 7, "BRIEF REPORT", 'BR', 0, 'L');
			$pdf->Cell($width[5], 7, $tmp->no_brief, 'BR', 0, 'L');	
			$pdf->Cell($width[6], 7, "", 'BR', 0, 'L');	
			$pdf->Ln();
        }
		
		$query = $this->db->query("SELECT tb_lab_desk.*, step.id_order, step.no_grading,
												tb_front_desk.certificate, tb_front_desk.gem_card, tb_front_desk.dia_grading,
												tb_member.first_name,
												step.no_brief
												FROM tb_lab_desk
												LEFT JOIN tb_member ON tb_member.id_member = tb_lab_desk.id_member
												LEFT JOIN tb_front_desk ON tb_front_desk.id_object = tb_lab_desk.id_object
												LEFT JOIN step ON step.id_object = tb_lab_desk.id_object
												WHERE ISNULL(tb_lab_desk.delete_by) AND tb_front_desk.dia_grading!=''
												AND tb_front_desk.store='".sessionValue('kode_store')."'
												".$where);					
		
        foreach($query->result() as $tmp) {			
			$pdf->Cell($width[0], 7, $no++, 'LBR', 0, 'C');
			$pdf->Cell($width[2], 7, date('d-m-Y', strtotime($tmp->finish)), 'BR', 0, 'C');
			$pdf->Cell($width[1], 7, $tmp->first_name, 'BR', 0, 'L');
			$pdf->Cell($width[3], 7, $tmp->id_order, 'BR', 0, 'C');
			$pdf->Cell($width[4], 7, "D. GRADING", 'BR', 0, 'L');
			$pdf->Cell($width[5], 7, $tmp->no_grading, 'BR', 0, 'L');	
			$pdf->Cell($width[6], 7, "", 'BR', 0, 'L');	
			$pdf->Ln();
        }
		
		$query = $this->db->query("SELECT tb_taksiran.*, step.id_order, step.no_taksiran,
												tb_member.first_name,
												step.no_brief
												FROM tb_taksiran
												LEFT JOIN tb_member ON tb_member.id_member = tb_taksiran.id_member
												LEFT JOIN step ON step.id_object = tb_taksiran.id
												LEFT JOIN tb_login ON tb_login.id = tb_taksiran.create_by
												WHERE ISNULL(tb_taksiran.delete_by)
												AND tb_login.store='".sessionValue('kode_store')."'
												".$where);					
		
        foreach($query->result() as $tmp) {			
			$pdf->Cell($width[0], 7, $no++, 'LBR', 0, 'C');
			$pdf->Cell($width[2], 7, date('d-m-Y', strtotime($tmp->finish)), 'BR', 0, 'C');
			$pdf->Cell($width[1], 7, $tmp->first_name, 'BR', 0, 'L');
			$pdf->Cell($width[3], 7, $tmp->id_order, 'BR', 0, 'C');
			$pdf->Cell($width[4], 7, "J. TAKSIRAN", 'BR', 0, 'L');
			$pdf->Cell($width[5], 7, $tmp->no_taksiran, 'BR', 0, 'L');	
			$pdf->Cell($width[6], 7, "", 'BR', 0, 'L');	
			$pdf->Ln();
        }
		
		$pdf->Image('asset/images/Logo.png', 145, 5, 50, 25);
		$pdf->SetFont('helvetica','', 10);
		$pdf->SetXY(147, 28);
		$pdf->MultiCell(45, 5, sessionValue('alamat'),'','L');
		$pdf->Ln();
		//$pdf->SetX(147);
		//$pdf->Cell(0, 5, 'Jakarta Pusat, 10430');
		//$pdf->Ln();
				
		$width_bot = array(95,95,5);
		$pdf->SetY(230);
		$pdf->Cell($width_bot[0], 5, 'Diperiksa Tanggal : ', '', 0, 'C');
		$pdf->Cell($width_bot[1], 5, 'JAKARTA PUSAT, '. date('d-m-Y'), '', 0, 'C');
		$pdf->Ln();
		$pdf->Cell($width_bot[0], 5, 'Pengelola Cabang G-Lab', '', 0, 'C');
		$pdf->Cell($width_bot[1], 5, 'Dibuat Oleh,', '', 0, 'C');
		$pdf->Ln(15);
		$pdf->Cell($width_bot[0], 5, 'Gemologist', '', 0, 'C');
		$pdf->Cell($width_bot[1], 5, 'Staff G-Lab', '', 0, 'C');
		
		$pdf->Output();
	}
	
	public function print_report_gemstone($from, $to, $menu, $search=""){
		$this->load->library('fpdf');
		
		$pdf = new FPDF('P','mm','A4');
		$pdf->SetFont('Arial','B', 10);
		$pdf->AddPage();
		
		$where = " AND step.create_date >= '".$from." 12:00:00 AM' AND step.create_date <= '".$to." 11:59:59 PM' ";		
		$queryCabang = $this->db->query("SELECT tb_store.store
												FROM step
												LEFT JOIN tb_login ON tb_login.id = step.create_by
												LEFT JOIN tb_store ON tb_store.kode = tb_login.store
												WHERE ISNULL(step.delete_by)
												AND !ISNULL(step.create_by)
												".$where."
												LIMIT 0,1");
		$cabang = "";
		foreach($queryCabang->result() as $tmpCabang) {
			$cabang = $tmpCabang->store;
		}
		
		$pdf->Cell(180, 7, 'PT Pegadaian (Persero)','',0,'L');
		$pdf->Ln();
		$pdf->Cell(180, 7, 'Laboratorium Gemologi','',0,'L');
		$pdf->Ln();
		$pdf->Cell(180, 7, 'Cabang : '.$cabang,'',0,'L');
		$pdf->Ln(17);
		
		$menu = str_replace('-', ' ',$menu);
		
		$pdf->Cell(180, 7, 'DAFTAR GEMSTONE', 0, 0, 'C');
		$pdf->Ln();
		$pdf->Cell(180, 7, 'Tanggal : '.$from.' s/d '.$to, 0, 0, 'C');
		$pdf->Ln(12);

		$width = array(8, 70, 20, 25, 50, 34, 30, 30);
		$pdf->SetFont('Arial','B', 9);
		$pdf->Cell($width[0], 7, "No", 1, 0, 'C');
		$pdf->Cell($width[2], 7, "Tanggal", 1, 0, 'C');
		$pdf->Cell($width[1], 7, "Varietas", 1, 0, 'C');
		//$pdf->Cell($width[3], 7, "Warna", 1, 0, 'C');
		$pdf->Cell($width[4], 7, "Bentuk & Gosokan", 1, 0, 'C');
		//$pdf->Cell($width[5], 7, "Ukuran", 1, 0, 'C');
		$pdf->Cell($width[6], 7, "Berat", 1, 0, 'C');
		//$pdf->Cell($width[7], 7, "Keterangan", 1, 0, 'C');
		$pdf->Ln();
		
		$to = date('Y-m-d',strtotime($to . "+1 days"));
		if($search==""){
			$where = " AND DATE_FORMAT(step.create_date,'%d-%m-%Y')=DATE_FORMAT(NOW(),'%d-%m-%Y') ";
		}
		else{
			$where = " AND step.create_date >= '".$from." 12:00:00 AM' AND step.create_date <= '".$to." 11:59:59 PM' ";
		}
		$where = " AND step.create_date >= '".$from." 12:00:00 AM' AND step.create_date <= '".$to." 11:59:59 PM' ";
		$query = $this->db->query("SELECT tb_lab_desk.*, step.id_order, step.no_sertifikat, step.create_date,
												step.no_brief, color_stone.jenis_warna
												FROM step
												JOIN tb_lab_desk ON tb_lab_desk.id_object = step.id_object
												LEFT JOIN color_stone ON color_stone.code = tb_lab_desk.obj_color
												WHERE ISNULL(tb_lab_desk.delete_by) AND step.status_barang='SELESAI'
												".$where);					
		
		$pdf->SetFont('Arial','', 8);
		$no = 1;
		$total = 0;
		$y_awal = $pdf->GetY();
        foreach($query->result() as $tmp) {			
			$pdf->Cell($width[0], 7, $no++, '', 0, 'C');
			$pdf->Cell($width[2], 7, date('d-m-Y', strtotime($tmp->create_date)), '', 0, 'C');
			$pdf->Cell($width[1], 7, ucwords(strtolower($tmp->nama_batu)), '', 0, 'L');
			//$pdf->Cell($width[3], 7, ucwords($tmp->jenis_warna), '', 0, 'L');
			$pdf->Cell($width[4], 7, $tmp->obj_shape." ".$tmp->obj_cut, '', 0, 'L');
			//$pdf->Cell($width[5], 7, $tmp->obj_length.' x '.$tmp->obj_width." x ".$tmp->obj_height." mm", '', 0, 'C');	
			$pdf->Cell($width[6], 7, $tmp->obj_weight." cts", '', 0, 'R');
			////$pdf->MultiCell($width[7], 7, ucwords($tmp->note), '', 'L');		
			$pdf->Ln();
        }
		$y_akhir = $pdf->GetY();
		$height = $y_akhir - $y_awal;
		$pdf->SetY($y_awal);
		$pdf->Cell($width[0], $height, '', 'LBR', 0, 'C');
		$pdf->Cell($width[2], $height, '', 'BR', 0, 'C');
		$pdf->Cell($width[1], $height, '', 'BR', 0, 'L');
		//$pdf->Cell($width[3], $height, '', 'BR', 0, 'L');
		$pdf->Cell($width[4], $height, '', 'BR', 0, 'L');
		//$pdf->Cell($width[5], $height, '', 'BR', 0, 'C');	
		$pdf->Cell($width[6], $height, '', 'BR', 0, 'R');	
		//$pdf->Cell($width[7], $height, '', 'BR', 0, 'L');
		
		$pdf->Image('asset/images/Logo.png', 145, 5, 50, 25);
		$pdf->SetFont('helvetica','', 10);
		$pdf->SetXY(147, 28);
		$pdf->MultiCell(45, 5, sessionValue('alamat'),'','L');
		$pdf->Ln();
		//$pdf->SetX(147);
		//$pdf->Cell(0, 5, 'Jakarta Pusat, 10430');
		//$pdf->Ln();
				
		$width_bot = array(95,95,5);
		$pdf->SetY(230);
		$pdf->Cell($width_bot[0], 5, 'Diperiksa Tanggal : ', '', 0, 'C');
		$pdf->Cell($width_bot[1], 5, 'JAKARTA PUSAT, '. date('d-m-Y'), '', 0, 'C');
		$pdf->Ln();
		$pdf->Cell($width_bot[0], 5, 'Pengelola Cabang G-Lab', '', 0, 'C');
		$pdf->Cell($width_bot[1], 5, 'Dibuat Oleh,', '', 0, 'C');
		$pdf->Ln(15);
		$pdf->Cell($width_bot[0], 5, 'Gemologist', '', 0, 'C');
		$pdf->Cell($width_bot[1], 5, 'Staff G-Lab', '', 0, 'C');
		
		$pdf->Output();
	}
}
?>