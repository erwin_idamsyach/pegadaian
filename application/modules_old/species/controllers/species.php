<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
* Controller Master Species
* Create By : Erwin Idamsyach Putra
* 23 April 2016
*/
class species extends CI_Controller{
	function __construct(){
		parent::__construct();

		if(!isLogin()){
			goLogin();
		}
	}
	
	public function index(){
		$data['filelist'] = 'species/front';
		$data['title'] = 'Master Lab Desk';
		$data['title_menu'] = 'Admin';
		$data['menu'] = 'species';

		getHTMLWeb($data);
	}

	public function add_species(){
		$name = $this->input->post('species');

		$data = array(
			'species' => $name,
			'create_by' => sessionValue('nama'),
			'create_date' => date('Y-m-d')	
			);
		$this->db->insert('mstr_species', $data);
		redirect(site_url('species'));
	}

	public function edit_species(){
		$id  = $this->uri->segment(3);

		$data['filelist'] = 'species/front';
		$data['title'] = 'Master Lab Desk';
		$data['title_menu'] = 'Admin';
		$data['menu'] = 'species';
		$data['id_spec'] = $id;

		getHTMLWeb($data);
	}

	public function update_species(){
		$id   = $this->input->post('id');
		$name = $this->input->post('species');

		$data = array(
			'species' => $name,
			'update_by' => sessionValue('nama'),
			'update_date' => date('Y-m-d')
			);

		$this->db->where('id', $id);
		$this->db->update('mstr_species', $data);
		redirect(site_url('species'));
	}

	public function delete_species(){
		$id = $this->uri->segment(3);

		$data = array(
			'delete_by' => sessionValue('nama'),
			'delete_date' => date('Y-m-d')
			);
		$this->db->where('id', $id);
		$this->db->update('mstr_species', $data);
		redirect(site_url('species'));
	}
}
?>