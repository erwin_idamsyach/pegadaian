<script type="text/javascript">
	$(document).ready(function(){
			chos();
			
			$.mask.definitions['d'] = '[0-9.]';
			
			$("#weight").mask("9?ddd.dd",{placeholder:"____.00"});
			$("#length").mask("99.99",{placeholder:"__.__"});
			$("#width").mask("99.99",{placeholder:"__.__"});
			$("#height").mask("99.99",{placeholder:"__.__"});
		   
			$('#form-edit-obj').trigger("reset");
			$("#form-edit-obj").on('submit',function(e){
			e.preventDefault();
			var formatData = new FormData($(this)[0]);
				swal({
				 title: "",
				 text: "Apakah Anda akan menyimpan data?",
				 type: "warning",
				 showCancelButton: true,
				 confirmButtonColor: "#DD6B55",
				 confirmButtonText: "Ya",
				 cancelButtonText: "Tidak",
				 closeOnConfirm: false }, function(){
					$.ajax({
						type : 'POST',
						url : '<?php echo base_url()?>list_fpjs/edit_obj',
						data : formatData,
						async: false,
						cache: false,
						contentType: false,
						processData: false,
						success:function(html){
							var data = eval ("(" + html + ")");
							if(data.success){
								swal({
								 title: "Berhasil Menyimpan data!",
								 text: "Klik tombol di bawah.",
								 type: "success",
								 showCancelButton: false,
								 confirmButtonColor: "#257DB6",
								 confirmButtonText: "Ok!",
								 closeOnConfirm: false
								},
								function(){
									location.href = base_url+"list_fpjs";
								});
							}else{
								swal({
								 title: data.msg,
								 text: "Klik tombol di bawah.",
								 type: "warning",
								 showCancelButton: false,
								 confirmButtonColor: "#257DB6",
								 confirmButtonText: "Ok!",
								 closeOnConfirm: false
								});
							}
						}
					});
				});
			});
	});
		
		function backToList(){
			location.href = base_url+"list_fpjs";
		}
		
        function cekCIF(){
          var kode = $(".cif").val();
          $.ajax({
            type : "POST",
            url : base_url+"/obj_input/cekCif/"+kode,
            data: "kode="+kode,
            dataType : "json",
            success:function(response){
              if(response['status'] == "SUCCESS"){
                $(".nama").val(response['nama']);
                $(".alamat").val(response['alamat']);
                $(".telp").val(response['phone']);
                $(".form-cif").removeClass("has-error");
                $(".form-cif").addClass("has-success");
              }else{
                $(".form-cif").removeClass("has-success");
                $(".form-cif").addClass("has-error");
              }
            }
          });
        }
      </script>
      <?php
      $ix = $this->session->userdata('order');
      $get_order  = $this->db->query("SELECT * FROM step WHERE id_order='$ix'");
      $numing = $get_order->num_rows();
      ?>
      <div class="alertx modal fade" id="alertx" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-body">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <div class="row">
                  <center><h4>DATA NOT AVAILABLE</h4></center>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Modal -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" style="width:400px;">
          <div class="modal-content">
            <div class="modal-header" style="background-color:#f8f8f8">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h6 class="modal-title" id="myModalLabel"><b>VIEW OBJECT</b></h6>
            </div>
            <div class="modal-body">
              <div class="lol">
                
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>&nbsp;</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-user"></i> Home</a></li>
            <li>Reg. FPJS</li>
            <li class="active">Add FPJS</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
        <div class="index">
          <div class="box">
            <div class="box-header">
              <b>EDIT : <?php echo $user->id_order; ?></b>
              <div style="border:1px solid black;margin-bottom:-10px;"></div>
                <div class="box-tools pull-right">
                  <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
            <i>DATA NASABAH</i>
              <div style="border:1px solid black;margin-bottom:10px;"></div>
          <form method="post" id="form-edit-obj" enctype="multipart/form-data">
            <div class="row">
              <div class="col-md-3 col-sm-6">
                <div class="form-group has-feedback form-cif">
                  <label>No. CIF</label>
                  <div class="input-group">
                    <span class="input-group-addon">CIF</span>
                    <input type="hidden" name="no_trans" class="no_trans" value="<?php echo $this->session->userdata('no_trans') ?>">
                    <input type="text" name="cif" class="form-control cif" id="id_mem" placeholder="No. CIF" value="<?php 
                    $idm = substr($user->id_member, 3);echo $idm ?>" disabled>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6">
                <div class="form-group">
                  <label>Nama</label>
                  <input type="text" name="nama" class="form-control nama typeahead" value="<?php echo $user->first_name." ".$user->middle_name." ".$user->last_name ?>" disabled>
                </div>
              </div>
              <div class="col-md-3 col-sm-6">
                <div class="form-group">
                  <label>Alamat</label>
                  <input type="text" name="alamat" class="form-control alamat" value="<?php echo $user->address ?>" disabled>
                </div>
              </div>
              <div class="col-md-3 col-sm-6">
                <div class="form-group">
                  <label>No Telp/HP</label>
                  <input type="text" name="telp" class="form-control telp" value="<?php echo $user->phone ?>" disabled>
                </div>
              </div>
            </div>
          <?php
          $getObject = $this->db->query("SELECT * FROM tb_front_desk, color_stone WHERE tb_front_desk.obj_color = color_stone.id AND id_object='$id'");
          foreach ($getObject->result() as $get) {
            ?>
            <i>DATA OBYEK</i>
              <input type="hidden" name="id_obj" value="<?php echo $id ?>">
              <div style="border:1px solid black;margin-bottom:10px;"></div>
                <div class="row">
                  <div class="col-md-8">
                    <div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Weight <font color="red">*</font></label>
                          <div class="input-group"><input class="form-control" id="weight" name="weight"  value="<?php echo $get->obj_weight ?>" type="text" required /><span class="input-group-addon">ct(s)</span></div>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Length </label>
                          <div class="input-group"><input class="form-control" id="length" value="<?php echo $get->obj_length ?>" name="length" type="text" /><span class="input-group-addon">mm</span></div>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Width </label>
                          <div class="input-group"><input class="form-control" id="width" name="width" value="<?php echo $get->obj_width ?>" type="text" /><span class="input-group-addon">mm</span></div>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Height </label>
                          <div class="input-group"><input class="form-control" id="height" name="height" value="<?php echo $get->obj_height ?>" type="text" /><span class="input-group-addon">mm</span></div>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Bentuk </label>
							<select name="bentuk" id="bentuk" class="form-control">
							  <option value="">Select Bentuk</option>
							<?php
							  $get_pro = mysql_query("SELECT * FROM shape order by shape");
							  while ($do = mysql_fetch_array($get_pro)) {
								  if($do['code']==$get->obj_shape){
									  $selected_bentuk = "selected";
								  }else{
									  $selected_bentuk = "";
								  }
							?>
							  <option value="<?php echo $do['code'] ?>" <?php echo $selected_bentuk; ?>><?php echo $do['shape'] ?></option>
							<?php
							  }
							?>
							</select>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Color <font color="red">*</font></label>
                          <select name="colr" id="obj" class="form-control" onchange="chos()" required>
                            <?php
                            $col = mysql_query("SELECT * FROM color_stone GROUP BY jenis_warna");
                            while ($ge = mysql_fetch_array($col)) {
                            ?>
                            <?php
                            if($ge['jenis_warna'] == $get->jenis_warna){
                              ?>
                            <option value="<?php echo $ge['code'] ?>" selected style="text-transform: capitalize;"><?php echo $ge['jenis_warna'] ?></option>  
                            <?php
                            }else{
                            ?>
                            <option value="<?php echo $ge['code'] ?>" style="text-transform: capitalize;"><?php echo $ge['jenis_warna'] ?></option>
                            <?php
                              }
                            }
                            ?>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>Specific Color </label>
                          <div style="min-height: 100px;">
                            <div class="btn-group" id="col-area" data-toggle="buttons">
                              <?php
                              $sql = mysql_query("SELECT * FROM color_stone WHERE jenis_warna='Grey'");
                              while ($do =mysql_fetch_array($sql)) {
                              ?>
                                <label class="btn btn-default" style="white-space:normal; background-color: <?php echo $do['rgb_code'] ?>; width: 110px; height: 110px;">
                                <input type="radio" class="coll" name="spe_col" id="specol" autocomplete="off" value="<?php echo $do['id'] ?>" >
                                <span class="fa fa-check fa-1x"></span><br/>
                                <center>
                                  <font style="mix-blend-mode: difference"><?php echo $do['code'] ?></font><br>
                                  <font style="mix-blend-mode: difference"><?php echo $do['color'] ?></font>
                                </center>
                              </label>
                              <?php
                              }
                              ?>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-12">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                            <label>Image (Maximal size 300 Kb)</label>
                            <input type="file" class="drop form-control" name="gambar" data-default-file="<?php echo base_url() ?>asset/images/<?php echo $get->obj_image ?>" id="name">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Comment(s)</label>
                      <textarea name="comment" class="form-control"><?php echo $get->comment ?></textarea>
                    </div>
                  </div>
                  <input class="hidden" type="text" name="gambarout" id="gambarout">
                  <iframe class="hidden" name="iframeUploadImg" id="iframeUploadImg"></iframe>
                  <div class="col-md-12" style="margin-top:20px;">
                    <button type="submit" class="btn btn-default"><i class="fa fa-save"></i> SIMPAN</button>
                    <button type="button" onclick="backToList()" class="btn btn-default"><i class="fa fa-refresh"></i> BACK</button>
                  </div>
                </div>
              </form>
                    </div>
              <?php
          }
          ?>
                </div>
            </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->