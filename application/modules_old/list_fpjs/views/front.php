<script>
$(function(){
    $("#example1").DataTable();
    $(".datepicker").datepicker();
});
function sweets(key){
    swal({
             title: "",
             text: "Apakah Anda akan menghapus data?",
             type: "warning",
             showCancelButton: true,
             confirmButtonColor: "#DD6B55",
             confirmButtonText: "Ya",
             cancelButtonText: "Tidak",
             closeOnConfirm: false }, function(){
                location.href = base_url+"list_fpjs/delete_data/"+key;
            });
}
function sweetsOrd(ord){
    swal({
             title: "",
             text: "Apakah Anda akan menghapus data?",
             type: "warning",
             showCancelButton: true,
             confirmButtonColor: "#DD6B55",
             confirmButtonText: "Ya",
             cancelButtonText: "Tidak",
             closeOnConfirm: false }, function(){
                location.href = base_url+"list_fpjs/delete_order/"+ord;
            });
}
</script>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>&nbsp;</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-user"></i> Home</a></li>
            <li>Reg. BPS</li>
            <li class="active">List BPS</li>
          </ol>
        </section>
        <section class="content">
        	<div class="box box-default">
        		<div class="box-header">
        			<b>LIST BPS</b>
        			<div style="border: 1px solid black; margin-bottom: 10px"></div>
        		</div>
                <form action="" method="get">
                    <div class="container-fluid" style="margin-top: 10px;">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="from-group">
									<table width="100%" border="0">
									<tr>
										<td>
											<label><b>Cari Tanggal : </b></label>
										</td>
										<td>
											<input type="text" name="from" class="form-control datepicker">
										</td>
										<td>
											&nbsp;-&nbsp;
										</td>
										<td>
											<input type="text" name="to" class="form-control datepicker">
										</td>
										<td>
											&nbsp;<button type="submit" class="btn btn-primary" style="margin-top: 0px;"><i class="fa fa-search"></i></button>
										</td>
									</tr>
									</table>
                                </div>  
                            </div>
                        </div>
                    </div>
                </form>
        		<div class="box-body">
        			<table class="table table-striped table-bordered table-hover" id="example1">
        				<thead>
        					<tr>
        						<th class="text-center" width="5%">No</th>
                                <th class="text-center" width="15%">Tanggal</th>
        						<th class="text-center">ID FPJS</th>
        						<th class="text-center" width="18%">ID Member</th>
        						<th class="text-center" width="18%">Total Speciment</th>
        						<th class="text-center" width="18%">Action</th>
        					</tr>
        				</thead>
        				<tbody>
        					<?php
        					$no = 1;
                            if(!isset($_GET['from']) && !isset($_GET['to'])){
                            $date = date("Y-m-d");
        					$getfpjs = $this->db->query("SELECT * FROM tb_front_desk WHERE DATE_FORMAT(create_date,'%d-%m-%Y')=DATE_FORMAT('$date','%d-%m-%Y') AND ISNULL(delete_by) AND store='".sessionValue('kode_store')."' GROUP BY id_order ORDER BY create_date DESC");
        					foreach ($getfpjs->result() as $get) {
        						?>
        					<tr>
        						<td class="text-center"><?php echo $no++; ?></td>
                                <td><?php echo $get->input_date ?></td>
        						<td><?php echo $get->id_order; ?></td>
        						<td><?php echo $get->id_member; ?></td>
        						<td>
                                <center>
                                    <?php
                                        $con = $this->db->query("SELECT COUNT(id_object) as total_speciment FROM tb_front_desk
                                            WHERE id_order='".$get->id_order."'
											AND store='".sessionValue('kode_store')."'
											AND ISNULL(delete_by)");
                                        foreach ($con->result() as $tot) {
                                            echo $tot->total_speciment;
                                        }
                                    ?>
                                </center>
        						</td>
        						<td class="text-right">
                                <?php
                                $idn = str_replace('/', '-', $get->id_order);
                                ?>
        							<a href="#" onclick="viewData('<?php echo $idn ?>')" class="btn btn-info" data-toggle="tooltip" data-placement="bottom" title="View"><i class="fa fa-eye"></i></a>
        							<a href="#" class="btn btn-warning disabled" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="fa fa-edit"></i></a>
        							<a href="#" class="btn btn-danger" onclick="sweetsOrd('<?php echo $idn ?>')" data-toggle="tooltip" data-placement="bottom" title="Delete"><i class="fa fa-trash"></i></a>
        							<a href="<?php $ord= str_replace('/', '-', $get->id_order); echo site_url('list_fpjs/printFPJS').'/'.$ord ?>" class="btn btn-primary" target="_blank" data-toggle="tooltip" data-placement="bottom" title="Print"><i class="fa fa-print"></i></a>
        						</td>
        					</tr>
        						<?php
        					}
                        }else{
                            $from = $_GET['from'];
                            $to = $_GET['to'];

                            $from = date_create($from);
                            $from = date_format($from, "Y-m-d");
                            $to = date_create($to);
                            $to = date_format($to, "Y-m-d");
							$to = date('Y-m-d',strtotime($to. "+1 days"));
                            
                            $sql = $this->db->query("SELECT * FROM tb_front_desk WHERE create_date >= '$from 12:00:00 AM' AND create_date <= '$to 11:59:59 PM' AND ISNULL(delete_by) AND store='".sessionValue('kode_store')."' GROUP BY id_order ORDER BY create_date DESC");
                            foreach ($sql->result() as $get) {
                                ?>
                            <tr>
                                <td class="text-center"><?php echo $no++; ?></td>
                                <td><?php echo $get->input_date ?></td>
                                <td><?php echo $get->id_order; ?></td>
                                <td><?php echo $get->id_member; ?></td>
                                <td>
                                <center>
                                    <?php
                                        $con = $this->db->query("SELECT COUNT(*) as total_speciment FROM tb_front_desk
                                            WHERE id_order='".$get->id_order."' 
											AND store='".sessionValue('kode_store')."'
											AND ISNULL(delete_by)");
                                        foreach ($con->result() as $tot) {
                                            echo $tot->total_speciment;
                                        }
                                    ?>
                                </center>
                                </td>
                                <td class="text-right">
                                <?php
                                $idn = str_replace('/', '-', $get->id_order);
                                ?>
                                    <a href="#" onclick="viewData('<?php echo $idn ?>')" class="btn btn-info" data-toggle="tooltip" data-placement="bottom" title="View"><i class="fa fa-eye"></i></a>
                                    <a href="#" class="btn btn-warning disabled" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="fa fa-edit"></i></a>
                                    <a href="#" class="btn btn-danger" onclick="sweetsOrd('<?php echo $idn ?>')" data-toggle="tooltip" data-placement="bottom" title="Delete"><i class="fa fa-trash"></i></a>
                                    <a href="<?php $ord= str_replace('/', '-', $get->id_order); echo site_url('list_fpjs/printFPJS').'/'.$ord ?>" class="btn btn-primary" target="_blank" data-toggle="tooltip" data-placement="bottom" title="Print"><i class="fa fa-print"></i></a>
                                </td>
                            </tr>
                                <?php
                            }
                        }
        					?>
                        
        				</tbody>
        			</table>
        		</div>
        	</div>
        </section>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="modal-view">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">&nbsp;</h4>
      </div>
      <div class="modal-body">
        <div class="area-resp"></div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
    function viewData(key){
        $.ajax({
            type : "post",
            url : "./list_fpjs/view_data",
            data : "key="+key,
            success:function(response){
                $("#modal-view").modal('show');
                $(".area-resp").html(response);
            }
        });
    }
</script>