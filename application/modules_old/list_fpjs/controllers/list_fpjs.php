<?php if (! defined('BASEPATH')){ exit('No direct script allowed'); }
/*
* Controller List FPJS
* Create by Erwin Idamsyach @ 2 May 2016
*/
class list_fpjs extends CI_Controller{
	function list_fpjs(){
        parent::__construct();
		
		if(!isLogin()){
			goLogin();
		}
        $this->load->library('fpdf');
        $this->load->library('Barcode39');
    }
	
	public function index(){
		$data['filelist'] = 'list_fpjs/front';
		$data['title'] = 'List Formulir Permintaan Jasa Sertifikasi (FPJS)';
		$data['title_menu'] = 'Front';
		$data['menu'] = 'list_fpjs';

		getHTMLWeb($data);
	}
	public function print_bps(){
		$ord = $this->uri->segment(3);
		$ord = str_replace('-', '/', $ord);
		$data['ord'] = $ord;
		$this->load->view('print-order', $data);
	}
	public function view_data(){
		$key = $this->input->post('key');
		$data['key'] = $key;
		$this->load->view('view', $data);
	}
	public function delete_data(){
		$id = $this->uri->segment(3);
		$this->db->where('id_object', $id);
		$this->db->update('tb_front_desk', array("delete_by"=>sessionValue('nama'), "delete_date"=>date("Y-m-d H:i:s")));
		$this->db->where('id_object', $id);
		$this->db->update('step', array("delete_by"=>sessionValue('nama'), "delete_date"=>date("Y-m-d H:i:s")));
		redirect(site_url('list_fpjs'));
	}
	public function delete_order(){
		$id = $this->uri->segment(3);
		$id = str_replace("-", "/", $id);
		$this->db->where('id_order', $id);
		$this->db->update('tb_front_desk', array("delete_by"=>sessionValue('nama'), "delete_date"=>date("Y-m-d H:i:s")));
		$this->db->where('id_order', $id);
		$this->db->update('step', array("delete_by"=>sessionValue('nama'), "delete_date"=>date("Y-m-d H:i:s")));
		redirect(site_url('list_fpjs'));
	}
	public function editobject(){
		$id = $this->uri->segment(3);
		$data['filelist'] = 'list_fpjs/form-edit';
		$data['title'] = 'List Formulir Permintaan Jasa Sertifikasi (FPJS)';
		$data['title_menu'] = 'Front';
		$data['menu'] = 'list_fpjs';
		$data['id'] = $id;	
		
		$getUser = $this->db->query("SELECT tb_member.id_member, tb_member.first_name, tb_member.middle_name, tb_member.last_name, tb_member.address, tb_member.phone, tb_front_desk.id_order FROM tb_member, tb_front_desk
          WHERE
          tb_member.id_member = tb_front_desk.id_member AND tb_front_desk.id_object='$id'");
		$tmp = $getUser->result();
		$data['user'] = $tmp[0];	
		
		getHTMLWeb($data);
	}
	
	// Hadi
	function printFPJS($id=NULL){
		
        $pdf = new FPDF('P','cm','A4');
		$pdf->SetAutoPageBreak(false, 0);
		
		$pdf->AddPage();
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(19, 1, 'FORM PERMINTAAN JASA SERTIFIKASI','',0,'C');
			
		$id = str_replace('-', '/', $id);
		
        $que = $this->db->query("SELECT tb_front_desk.id_order, tb_front_desk.create_date, tb_member.kode,
								tb_member.first_name, tb_member.middle_name, tb_member.last_name, tb_member.corp_name, tb_member.address, tb_member.postal_code, tb_member.phone,
								kecamatan.kecamatanNama kec1, kabupaten.kabupatenNama kota1, provinsi.provinsiNama prov1
									FROM 
								tb_front_desk
								LEFT JOIN tb_member ON tb_member.id_member = tb_front_desk.id_member
								LEFT JOIN kecamatan ON kecamatan.kecamatanId = tb_member.district
								LEFT JOIN kabupaten ON kabupaten.kabupatenId = tb_member.city
								LEFT JOIN provinsi ON provinsi.provinsiId = tb_member.province
								WHERE id_order='$id' AND store='".sessionValue('kode_store')."' GROUP BY id_order");
        
		foreach ($que->result() as $get) {
			$pdf->Ln();
			$pdf->SetFont('Arial','BI',9);
			$pdf->Cell(19, 0.5, 'DATA PELANGGAN','B',0,'L');
			
			$pdf->SetFont('Arial','',9);			
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'No. Order','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, $get->id_order,'',0,'L');
			
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'Tanggal','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, $get->create_date,'',0,'L');
			
			if($get->kode=="A"){
				$nama = $get->first_name." ".$get->middle_name." ".$get->last_name;
			}else{
				$nama = $get->corp_name;
			}
			
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'Nama','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, $nama,'',0,'L');
			
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'Alamat','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, $get->address,'',0,'L');
			$pdf->Ln();
			$pdf->Cell(4, 0.5, '','',0,'L');
			$pdf->Cell(0.5, 0.5, '','',0,'L');
			$pdf->Cell(14, 0.5, 'Kode Pos : '.$get->postal_code.', '.$get->kec1.', '.$get->kota1.', '.$get->prov1,'',0,'L');
			
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'No. Telp / HP','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, (($get->phone!="")?$get->phone:'-'),'',0,'L');		
        }
			
        $que = $this->db->query("SELECT tb_front_desk.*
									FROM 
								tb_front_desk
								LEFT JOIN tb_member ON tb_member.id_member = tb_front_desk.id_member
								WHERE id_order='$id' AND store='".sessionValue('kode_store')."' ");
        
        $pdf->Ln(0.8);
			$pdf->SetFont('Arial','BI',9);
			$pdf->Cell(19, 0.5, 'KETERANGAN BATU YANG DISERAHKAN','B',0,'L');

		$pdf->SetFont('Arial','',9);
			$pdf->SetXY(1, 7);
			$pdf->Cell(1.4, 0.5, "NO", 1, 0, "C");
			$pdf->SetXY(2.4, 7);
			$pdf->Cell(4, 0.5, "ID Object ", 1, 0, "C");
			$pdf->SetXY(6.4, 7);
			$pdf->Cell(2, 0.5, "Weight", 1, 0, "C");
			$pdf->SetXY(8.4, 7);
			$pdf->Cell(2, 0.5, "Length", 1, 0, "C");
			$pdf->SetXY(10.4, 7);
			$pdf->Cell(2, 0.5, "Width", 1, 0, "C");
			$pdf->SetXY(12.4, 7);
			$pdf->Cell(2, 0.5, "Height", 1, 0, "C");
			$pdf->SetXY(14.4, 7);
			$pdf->Cell(5.6, 0.5, "Order", 1, 0, "C");
			$pdf->Ln();
		$no = 1;
		foreach ($que->result() as $get) {
		$pdf->SetFillColor(255, 255, 255);
		if($get->certificate != ''){
			$cert = "Identification Report, ";
		}else{
			$cert = "";
		}

		if($get->gem_card != ''){
			$gem = "Brief Report, ";
		}else{
			$gem = "";
		}

		if($get->dia_grading != ''){
			$dg  = "D. Grading Report, ";
		}else{
			$dg = "";
		}

		$ord = $cert."".$gem."".$dg;

			$pdf->SetX(1);
			$pdf->Cell(1.4, 0.5, $no++, 1, 0, "C");
			$pdf->SetX(2.4);
			$pdf->Cell(4, 0.5, $get->id_object, 1, 0, "C");
			$pdf->SetX(6.4);
			$pdf->Cell(2, 0.5, $get->obj_weight." cts", 1, 0, "C");
			$pdf->SetX(8.4);
			$pdf->Cell(2, 0.5, $get->obj_length." mm", 1, 0, "C");
			$pdf->SetX(10.4);
			$pdf->Cell(2, 0.5, $get->obj_width." mm", 1, 0, "C");
			$pdf->SetX(12.4);
			$pdf->Cell(2, 0.5, $get->obj_height." mm", 1, 0, "C");
			$pdf->SetX(14.4);
			$pdf->Cell(5.6, 0.5, substr($ord,0,-2), 1, 0, "C");
			$pdf->Ln();
			/*$pdf->SetFont('Arial','',9);
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'Warna','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, '','',0,'L');
			
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'Bentuk','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, '','',0,'L');
			
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'Berat','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, '','',0,'L');
			
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'Jumlah','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, '','',0,'L');
			
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'Order','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, '','',0,'L');
			
			$pdf->Ln();
			$pdf->Cell(4, 0.5, 'Catatan','',0,'L');
			$pdf->Cell(0.5, 0.5, ':','',0,'L');
			$pdf->Cell(14, 0.5, '','',0,'L');*/
        }
		
		$pdf->Output();
    }
    public function color()
    {
        $main = $this->input->post('main');
        $this->load->view('get_color', array('main'=>$main));
    }
    public function edit_obj()
    {
    	$id_obj  = $this->input->post('id_obj');

        $weight  = $this->input->post('weight');
        $length  = $this->input->post('length');
        $width   = $this->input->post('width');
        $height  = $this->input->post('height');
        $bentuk  = $this->input->post('bentuk');
        $color   = $this->input->post('spe_col');
        $grading = $this->input->post('DG');
        $comment = $this->input->post('comment');
        $user    = sessionValue('username');
        $kode    = sessionValue('kode_store');
		
		$success = false;
		$msg = "";
		$now = date('Y-m-d');
        if($_FILES['gambar']['size'] != 0){
        	if($_FILES['gambar']['name'])
			{
				//if no errors...
				if(!$_FILES['gambar']['error'])
				{
					$valid_file = true;
					//now is the time to modify the future file name and validate the file
					$new_file_name = $id_obj.".jpg"; //rename file
					if($_FILES['gambar']['size'] > (307200)) //can't be larger than 1 MB
					{
						$valid_file = false;
						//$message = 'Oops!  Your file\'s size is to large.';
						$msg = "Ukuran gambar terlalu besar.";
					}
						
					//if the file has passed the test
					if($valid_file){
						//move it to where we want it to be
						$moved = move_uploaded_file($_FILES['gambar']['tmp_name'], './asset/images/'.$new_file_name);
						if($moved){
							$success = true;
						}else{
							$msg = "Image can't upload";
						}
						//$message = 'Congratulations!  Your file was accepted.';
					}
						$message = $_FILES['gambar']['name'];
						//echo "<script type='text/javascript'>parent.document.getElementById('gambarout').value='".$_FILES['gambar']['name']."';</script>";
				}else{
					//set that to be the returned message
					$message = 'Error: Ooops!  Your upload triggered the following error:  '.$_FILES['gambar']['error'];
				}
			}else{
				$success = true;
			}
		
			$data = array(
				'obj_weight'    => $weight,
				'obj_length'    => $length,
				'obj_width'     => $width,
				'obj_height'    => $height,
				'obj_shape'    => $bentuk,
				'obj_color'     => $color,
				'obj_image'     => $new_file_name,
				'update_by'     => $user,
				'update_date'   => $now,
				'comment'		=> $comment
				);
			}else{
				$success = true;
				
				$data = array(
				'obj_weight'    => $weight,
				'obj_length'    => $length,
				'obj_width'     => $width,
				'obj_height'    => $height,
				'obj_shape'    => $bentuk,
				'obj_color'     => $color,
				'update_by'     => $user,
				'update_date'   => $now,
				'comment'		=> $comment
			);
        }
        $this->db->where('id_object', $id_obj);
        if($success==true){
			$this->db->update('tb_front_desk', $data);
		}
        
		echo json_encode(array("success"=>$success, "msg"=>$msg));
	}        
}
?>