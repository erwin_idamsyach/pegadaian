<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
* Controller Finished Print Gem Card
* Create By : Erwin Idamsyach Putra
* 23 April 2016
*/
class Print_gem_done extends CI_Controller{
	public function index(){
		$data['filelist'] = 'print_gem_done/front';
		$data['title'] = 'Finished Print Gem Card';
		$data['title_menu'] = 'Print';
		$data['menu'] = 'print-gem-done';

		getHTMLWeb($data);
	}

	public function print_gem_card(){
		$this->load->library('fpdf');
		$id_obj = $this->uri->segment(3);

		$get_data = $this->db->query("SELECT * FROM tb_lab_desk, color_stone WHERE 
			tb_lab_desk.obj_color = color_stone.code AND
			id_object='$id_obj'");

		$this->db->where('id_object', $id_obj);
		$this->db->update('step', array('print_gem'=>'Sudah'));

		$pdf = new FPDF('L','mm','A4');

		foreach ($get_data->result() as $get) {

		$create_date = date_create($get->finish);
		$day 		 = date_format($create_date, 'd');
		$month 		 = date_format($create_date, 'm');
		$year 		 = date_format($create_date, 'Y');
		switch ($month) {
			case '01':
				$month = "Januari";
				break;
			case '02':
				$month = "Februari";
				break;
			case '03':
				$month = "Maret";
				break;
			case '04':
				$month = "April";
				break;
			case '05':
				$month = "Mei";
				break;
			case '06':
				$month = "Juni";
				break;
			case '07':
				$month = "Juli";
				break;
			case '08':
				$month = "Agustus";
				break;
			case '09':
				$month = "September";
				break;
			case '10':
				$month = "Oktober";
				break;
			case '11':
				$month = "November";
				break;
			case '12':
				$month = "Desember";
				break;
			default:
				$month = $month;
				break;
		}
		$natural = ucfirst(strtolower($get->obj_natural));
		$variety = ucfirst(strtolower($get->variety));
		$species = ucfirst(strtolower($get->nama_batu));

		if($get->comment == ''){
			$comment = '-';
		}else{
			$comment = $get->comment;
		}

		$pdf->AddPage();
		$pdf->Image('asset/logo-pegadaian/Logo.png', 10, 5, 70, 30);
		$pdf->Image('asset/logo-pegadaian/Logo-Pegadaian-PNG.png', 225, 5, 65, 30);

		$pdf->SetY(35);
		$pdf->SetFont('Helvetica','B',22);
		$pdf->Cell(0, 0, 'GEMSTONE BRIEF REPORT', 0, 0, 'C');

		$pdf->SetDrawColor(10, 185, 0);
		$pdf->SetLineWidth(1);
		$pdf->Line(5, 42, 290, 42);
		$pdf->Image('asset/images/'.$get->obj_image, 6, 51, 88, 88);

		$pdf->SetFont('Helvetica','',12);
		$pdf->SetXY(36, 146);
		$pdf->Cell(0, 0, $day." ".$month." ".$year, 0, 0, 'L');

		$pdf->SetXY(32, 170);
		$pdf->Cell(0, 0, 'J. Hendro Susanto', 0, 0, 'L');

		$pdf->Line(27, 173, 77, 173);

		$pdf->SetXY(29, 177);
		$pdf->Cell(0, 0, 'Accredited Gemologist', 0, 0, 'L');

		$pdf->SetFont('Helvetica','',20);
		$pdf->SetXY(108, 55);
		$pdf->Cell(0, 0, 'Report No');
		$pdf->SetXY(180, 55);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(195, 55);
		$pdf->Cell(0, 0, $get->id_object);

		$pdf->SetXY(108, 69);
		$pdf->Cell(0, 0, 'Color');
		$pdf->SetXY(180, 69);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(195, 69);
		$pdf->Cell(0, 0, $get->color);

		$pdf->SetXY(108, 83);
		$pdf->Cell(0, 0, 'Shape & Cut');
		$pdf->SetXY(180, 83);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(195, 83);
		$pdf->Cell(0, 0, $get->obj_shape." ".$get->obj_cut);

		$pdf->SetXY(108, 97);
		$pdf->Cell(0, 0, 'Measurement');
		$pdf->SetXY(180, 97);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(195, 97);
		$pdf->Cell(0, 0, $get->obj_width.' - '.$get->obj_height." x ".$get->obj_length." mm");
		
		$pdf->SetXY(108, 111);
		$pdf->Cell(0, 0, 'Weight');
		$pdf->SetXY(180, 111);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(195, 111);
		$pdf->Cell(0, 0, $get->obj_weight." cts");

		$pdf->SetXY(108, 125);
		$pdf->Cell(0, 0, 'Identification Result');
		$pdf->SetXY(180, 125);
		$pdf->Cell(0, 0, ':');
		$pdf->Ln();

		$pdf->SetTextColor(10, 160, 55);
		$pdf->SetFont('Helvetica','B',22);
		$pdf->SetXY(108, 140);
		$pdf->Cell(0, 0, $natural." ".$variety." ".$species, 0, 0, 'C');

		$pdf->SetTextColor(0,0,0);
		$pdf->SetFont('Helvetica','',18);
		$pdf->SetXY(108, 155);
		$pdf->Cell(0, 0, 'Comment (s)');
		$pdf->SetXY(180, 155);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(195, 151);
		$pdf->MultiCell(80, 8, $comment);

		}
		$pdf->Output();
	}

	public function print_multiple_gem(){
		$this->load->library('fpdf');
		$id_order = $this->uri->segment(3);

		$get_data = $this->db->query("SELECT * FROM step, tb_lab_desk, color_stone WHERE 
			step.id_object = tb_lab_desk.id_object AND
			step.gem_card != '' AND
			tb_lab_desk.obj_color = color_stone.code AND
			step.delete_by = '' AND
			step.id_order='$id_order'");

		$this->db->where('id_order', $id_order);
		$this->db->update('step', array('print_gem'=>'Sudah'));

		$pdf = new FPDF('L','mm','A4');

		foreach ($get_data->result() as $get) {

		$create_date = date_create($get->finish);
		$day 		 = date_format($create_date, 'd');
		$month 		 = date_format($create_date, 'm');
		$year 		 = date_format($create_date, 'Y');
		switch ($month) {
			case '01':
				$month = "Januari";
				break;
			case '02':
				$month = "Februari";
				break;
			case '03':
				$month = "Maret";
				break;
			case '04':
				$month = "April";
				break;
			case '05':
				$month = "Mei";
				break;
			case '06':
				$month = "Juni";
				break;
			case '07':
				$month = "Juli";
				break;
			case '08':
				$month = "Agustus";
				break;
			case '09':
				$month = "September";
				break;
			case '10':
				$month = "Oktober";
				break;
			case '11':
				$month = "November";
				break;
			case '12':
				$month = "Desember";
				break;
			default:
				$month = $month;
				break;
		}
		$natural = ucfirst(strtolower($get->obj_natural));
		$variety = ucfirst(strtolower($get->variety));
		$species = ucfirst(strtolower($get->nama_batu));

		if($get->comment == ''){
			$comment = '-';
		}else{
			$comment = $get->comment;
		}

		$pdf->AddPage();
		$pdf->Image('asset/logo-pegadaian/Logo.png', 10, 5, 70, 30);
		$pdf->Image('asset/logo-pegadaian/Logo-Pegadaian-PNG.png', 225, 5, 65, 30);

		$pdf->SetY(35);
		$pdf->SetFont('Helvetica','B',22);
		$pdf->Cell(0, 0, 'GEMSTONE BRIEF REPORT', 0, 0, 'C');

		$pdf->SetDrawColor(10, 185, 0);
		$pdf->SetLineWidth(1);
		$pdf->Line(5, 42, 290, 42);
		$pdf->Image('asset/images/'.$get->obj_image, 6, 51, 88, 88);

		$pdf->SetFont('Helvetica','',12);
		$pdf->SetXY(36, 146);
		$pdf->Cell(0, 0, $day." ".$month." ".$year, 0, 0, 'L');

		$pdf->SetXY(32, 170);
		$pdf->Cell(0, 0, 'J. Hendro Susanto', 0, 0, 'L');

		$pdf->Line(27, 173, 77, 173);

		$pdf->SetXY(29, 177);
		$pdf->Cell(0, 0, 'Accredited Gemologist', 0, 0, 'L');

		$pdf->SetFont('Helvetica','',20);
		$pdf->SetXY(108, 55);
		$pdf->Cell(0, 0, 'Report No');
		$pdf->SetXY(180, 55);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(195, 55);
		$pdf->Cell(0, 0, $get->id_object);

		$pdf->SetXY(108, 69);
		$pdf->Cell(0, 0, 'Color');
		$pdf->SetXY(180, 69);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(195, 69);
		$pdf->Cell(0, 0, $get->color);

		$pdf->SetXY(108, 83);
		$pdf->Cell(0, 0, 'Shape & Cut');
		$pdf->SetXY(180, 83);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(195, 83);
		$pdf->Cell(0, 0, $get->obj_shape." ".$get->obj_cut);

		$pdf->SetXY(108, 97);
		$pdf->Cell(0, 0, 'Measurement');
		$pdf->SetXY(180, 97);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(195, 97);
		$pdf->Cell(0, 0, $get->obj_width.' - '.$get->obj_height." x ".$get->obj_length." mm");
		
		$pdf->SetXY(108, 111);
		$pdf->Cell(0, 0, 'Weight');
		$pdf->SetXY(180, 111);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(195, 111);
		$pdf->Cell(0, 0, $get->obj_weight." cts");

		$pdf->SetXY(108, 125);
		$pdf->Cell(0, 0, 'Identification Result');
		$pdf->SetXY(180, 125);
		$pdf->Cell(0, 0, ':');
		$pdf->Ln();

		$pdf->SetTextColor(10, 160, 55);
		$pdf->SetFont('Helvetica','B',22);
		$pdf->SetXY(108, 140);
		$pdf->Cell(0, 0, $natural." ".$variety." ".$species, 0, 0, 'C');

		$pdf->SetTextColor(0,0,0);
		$pdf->SetFont('Helvetica','',18);
		$pdf->SetXY(108, 155);
		$pdf->Cell(0, 0, 'Comment (s)');
		$pdf->SetXY(180, 155);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(195, 151);
		$pdf->MultiCell(80, 8, $comment);

		}
		$pdf->Output();
	}
}
?>