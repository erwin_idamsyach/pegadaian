<?php

class print_taksiran extends CI_Controller{
	function __construct(){
		parent::__construct();

		if(!isLogin()){
			goLogin();
		}
	}
	
	function print_sht(){
	$this->load->library('fpdf');
	$this->load->library("terbilang");
	$this->load->library('Barcode39');
	$a = new Terbilang();
	$harga_total = 0;
	$id = $this->uri->segment(3);
	$id_order = $this->uri->segment(4);
	$id_order = str_replace('-', '/', $id_order); 
	
	$getNumNoCert = $this->db->query("SELECT COUNT(no_taksiran) no_taksiran, no_taksiran noCert FROM step WHERE id_object='".$id."'");
	$tmpNumNoCert = $getNumNoCert->result_array();
		
	if($tmpNumNoCert[0]['no_taksiran']==0){
	
		$getNoCert = $this->db->query("SELECT no_taksiran FROM step ORDER BY no_taksiran DESC LIMIT 0, 1");
		foreach ($getNoCert->result() as $no_cert) {
			$noCert = $no_cert->no_taksiran;
		}
		if(!isset($noCert)){
			$noCert = 0;
		}else{}
		$noCert = substr($noCert, 3);
		$noCert = $noCert+1;
			$getServices = $this->db->query("SELECT kode_produk FROM tb_service WHERE kode='TE' LIMIT 0, 1");
			$kode_produk = "";
			foreach ($getServices->result() as $tmpService) {
				$kode_produk = $tmpService->kode_produk;
			}
			$kode_produk = sessionValue('kode_unit')."-".date('y')."-".$kode_produk."-";
			$noCert = generateNoReport($kode_produk, $noCert);
		$this->db->where("id_order", $id_order);
		$this->db->update("step", array("no_taksiran" => $noCert, 'update_by'  => sessionValue('id'), 'update_date' => date('Y-m-d H:i:s')));
			
		$bc = new Barcode39($noCert);
		$bc->barcode_bar_thick = 2; 
		$bc->draw("asset/barcode-fpjt/".$noCert.".gif");
	}else{
		$noCert = $tmpNumNoCert[0]['noCert'];
	}
	
	$data = $this->db->query("SELECT tb_taksiran.id, tb_taksiran.id_order, tb_taksiran.foto, tb_taksiran.berat_perhiasan, tb_taksiran.jumlah_perhiasan, tb_taksiran.jumlah_permata, tb_taksiran.keterangan, tb_taksiran.keterangan_berlian, tb_taksiran.berat_bersih, tb_taksiran.berat_kotor,
							master_permata.permata, master_jenis_permata.jenis, master_logam.jenis_logam, 
							color_stone.color, tb_taksiran.karatase_emas
							FROM tb_taksiran
						LEFT JOIN master_permata ON master_permata.id = tb_taksiran.jenis_permata
						LEFT JOIN master_jenis_permata ON master_jenis_permata.id = tb_taksiran.jenis_perhiasan
						LEFT JOIN color_stone ON color_stone.code = tb_taksiran.warna_permata
						LEFT JOIN master_logam ON tb_taksiran.jenis_logam = master_logam.id
						WHERE tb_taksiran.id_order='$id_order' AND !ISNULL(tb_taksiran.create_by)");


$sub = 0;
$pdf = new FPDF('P','mm','A4');
$pdf->SetAutoPageBreak(false, 0);
$pdf->AddPage();

// Hadi		
$pdf->Image('asset/images/Logo.png', 8, 10, 40, 20);
$pdf->Image('asset/barcode-fpjt/'.$noCert.'.gif', 163, 10, 40, 20);

$query = $this->db->query("SELECT tb_fpjt.create_date, tb_member.first_name, tb_member.middle_name, tb_member.last_name, tb_member.corp_name, tb_member.kode, tb_member.address, tb_member.id_member, tb_member.postal_code,
							kecamatan.kecamatanNama kec1, kabupaten.kabupatenNama kota1, provinsi.provinsiNama prov1
							FROM tb_fpjt 
						LEFT JOIN tb_member ON tb_member.id_member = tb_fpjt.id_member
						LEFT JOIN kecamatan ON kecamatan.kecamatanId = tb_member.district
						LEFT JOIN kabupaten ON kabupaten.kabupatenId = tb_member.city
						LEFT JOIN provinsi ON provinsi.provinsiId = tb_member.province
						WHERE tb_fpjt.id='$id' LIMIT 0,1");
$tmpNama = $query->result_array();
$penerima = sessionValue('nama');
$nasabah = $tmpNama[0]['first_name'];
$tgl = date('d', strtotime($tmpNama[0]['create_date'])).bulan(date('m', strtotime($tmpNama[0]['create_date'])))." ".date('Y', strtotime($tmpNama[0]['create_date']));
$hari = hari(date('N', strtotime($tmpNama[0]['create_date'])));
// End Hadi

$pdf->SetFont('arial','BU',12);
$pdf->SetXY(0,18);
$pdf->Cell(200,0,'SURAT KETERANGAN HASIL PENGUJIAN',0,0,'C');
$pdf->SetFont('arial','B',12);
$pdf->SetXY(0,23);
$pdf->Cell(200,0,'No. : '.$noCert ,0,0,'C');

$width = array(50, 30, 35, 20, 40);
$pos_x = array(50, 35, 22.5, 22.5, 20, 40);
$pdf->SetFont('arial','',11);
//$pdf->Line(117.39, 64.5, 117.39, 72);
$pdf->SetY(47);
$pdf->SetFont('arial','',11);
$pdf->MultiCell(190, 5, "Sesuai dengan permintaan dari Bapak/Ibu ".$nasabah." pada hari ".$hari." tanggal ".$tgl.", bersama ini Pegadaian G-LAB, alamat Jalan Kramat Raya Nomor 162 menyatakan bahwa perhiasan/barang yang diterangkan di bawah ini, setelah diuji dan ditaksir memiliki kualifikasi sebagai berikut :","","J");
$pdf->Ln();

$pdf->SetFont('arial','B',11);
$pdf->Cell($width[0], 15, "Keterangan Barang", 'LBRT', 0, 'C');
$pdf->Cell($width[1], 15, "Jenis Logam", 'BRT', 0, 'C');
$pdf->Cell($width[3], 15, "Kadar(%)", 'BRT', 0, 'C');
$getX = $pdf->GetX();
$getY = $pdf->GetY();
$pdf->Cell($width[2], 7.5, "Berat (gram)", 'BRT', 0, 'C');
$getX2 = $pdf->GetX();
$getY2 = $pdf->GetY();
$pdf->Cell($width[3], 7.5, "Jenis", 'TR', 0, 'C');
$pdf->Cell($width[4], 15, "Ukuran, Kadar, DLL", 'BRT', 0, 'C');
$pdf->Ln();
$pdf->SetXY($getX, $getY+7.5);
$pdf->Cell($width[2]/2, 7.5, "Kotor", 'BR', 0, "C");
$pdf->Cell($width[2]/2, 7.5, "Bersih", 'BR', 0, "C");
$pdf->SetXY($getX2, $getY2+7.5);
$pdf->Cell($width[3], 7.5, "Permata", 'BR', 0, 'C');
$pdf->Ln();

$pdf->SetFont('arial','',10);

$cer = 0;
$mem = 0;
$dim = 0;
$no = 1;
$ong = 1;
$harga = 100000;
$harga_permata = 50000;
foreach($data->result_array() as $g){
	$x2 = $pdf->GetX();
	$y2 = $pdf->GetY();
	$pdf->MultiCell($width[0], 5, $g['jumlah_perhiasan'].' '.$g['jenis'].' '.$g['keterangan'], 'LR', 'L');
	$pdf->SetXY($x2+$width[0], $y2);
	$pdf->Cell($width[1], 5, $g['jenis_logam'], 'R', 0, 'C');
	$pdf->Cell($width[3], 5, number_format(($g['karatase_emas']/24)*100,2,',','.').' %', 'R', 0, 'R');
	$pdf->Cell($width[2]/2, 5, $g['berat_kotor'], 'R', 0, 'R');
	$pdf->Cell($width[2]/2, 5, $g['berat_bersih'], 'R', 0, 'R');
	$pdf->Cell($width[3], 5, $g['permata'], 'R', 0, 'C');
	$pdf->Cell($width[4], 5, $g['karatase_emas'].' karat', 'R', 0, 'L');
	$pdf->Ln();
	$pdf->Cell($width[0], 7, '', 'LR', 0, 'L');	
	$pdf->Cell($width[1], 7, '', 'R', 0, 'C');
	$pdf->Cell($width[3], 7, '', 'R', 0, 'R');
	$pdf->Cell($width[2]/2, 7, '', 'R', 0, 'R');
	$pdf->Cell($width[2]/2, 7, '', 'R', 0, 'R');
	$pdf->Cell($width[3], 7, '', 'R', 0, 'C');
	$pdf->MultiCell($width[4], 7, $g['keterangan_berlian'], 'R', 'L');
	$pdf->Cell($width[0], 7, '', 'LR', 0, 'L');	
	$pdf->Cell($width[1], 7, '', 'R', 0, 'C');
	$pdf->Cell($width[3], 7, '', 'R', 0, 'R');
	$pdf->Cell($width[2]/2, 7, '', 'R', 0, 'R');
	$pdf->Cell($width[2]/2, 7, '', 'R', 0, 'R');
	$pdf->Cell($width[3], 7, '', 'R', 0, 'C');
	$pdf->Cell($width[4], 7, '', 'R', 0, 'C');
	$pdf->Ln();
}

$ppn = $sub*10/100;
$total = $sub+$ppn;

$pdf->SetFont('arial','',10);

$pdf->Cell($width[0], 7, '', 'T', 0, 'L');
$pdf->Cell($width[1], 7, '', 'T', 0, 'L');
$pdf->Cell($width[3], 7, '', 'T', 0, 'L');
$pdf->Cell($width[2], 7, '', 'T', 0, 'L');
$pdf->Cell($width[3], 7, '', 'T', 0, 'L');
$pdf->Cell($width[4], 7, '', 'T', 0, 'L');
$pdf->Ln(10);

$pdf->Cell(5, 7, "Biaya Pengujian :", 0, 0, "L");
$getHDLE = $this->db->query("SELECT * FROM master_harga_emas WHERE id='1'");
$hdle = 0;
foreach ($getHDLE->result() as $hsp) {
	$hdle = $hsp->harga_emas;
}
$getEmas = $this->db->query("SELECT * FROM tb_taksiran
							LEFT JOIN master_logam ON master_logam.id = tb_taksiran.jenis_logam
							WHERE tb_taksiran.id_order='$id_order' AND tb_taksiran.jenis_logam='1'");

$pdf->Ln();
$pdf->Cell(10, 7, " Emas :", 0, 0, "L");							
$pdf->Ln();
					
$harga_total = 0;
foreach ($getEmas->result() as $key) {
	$harga_taksiran = 1.25/100*(($key->berat_bersih*($key->karatase_emas/24))*$hdle); // $key->jumlah_perhiasan*
	
	$pdf->Cell(10, 7, "-  1,25% x ".$key->berat_bersih." gr x ".$key->karatase_emas."karat/24 x Rp. ".number_format($hdle, 0, ",", "."), 0, 0, "L");
	$pdf->Cell(148, 7, ": RP", 0, 0, "R");
	$pdf->Cell(20, 7, number_format($harga_taksiran, 0, ",", "."), 0, 0, "R");
	$pdf->Ln();
	
	$harga_total = $harga_total + $harga_taksiran;
} 


$pdf->Cell(10, 7, " Berlian :", 0, 0, "L");							
$pdf->Ln();

$getBerlian = $this->db->query("SELECT * FROM tb_taksiran
								LEFT JOIN master_logam ON master_logam.id = tb_taksiran.jenis_logam
								WHERE tb_taksiran.id_order='$id_order'");
								
$harga_berlian = 0;
$berat_total = 0;	
$gemolog = "";
foreach ($getBerlian->result() as $key) {
	if($key->berat_total_permata == ""){
		$berat_total = $berat_total + 0;
	}else{
		$berat_total = $berat_total + $key->berat_total_permata;
	}
	
	if($berat_total>0){
		$getHarga = $this->db->query("SELECT harga
									FROM `master_tarif_taksiran`
									WHERE berat2 >= ".$berat_total." ORDER BY ID ASC LIMIT 0,1");
									
		foreach ($getHarga->result() as $tmpHarga) {
			$harga_berlian = $harga_berlian + $tmpHarga->harga;
		}
	}
	$gemolog = $key->gemolog;
}
$harga_total = $harga_total + $harga_berlian;

$pdf->Cell(10, 7, "-  Ukuran total ".$berat_total." carat ", 0, 0, "L");
$pdf->Cell(148, 7, ": RP", 0, 0, "R");
$pdf->Cell(20, 7, number_format($harga_berlian, 0, ",", "."), 0, 0, "R");
$pdf->Ln();

$harga_total_terbilang = roundNearestHundredUp($harga_total);
	
$pdf->SetFont('arial','B',10);
$pdf->Cell(10, 7, "Jumlah Total", "T", 0, "L");
	$pdf->Cell(148, 7, ": RP", "T", 0, "R");
	$pdf->Cell(20, 7, number_format(roundNearestHundredUp($harga_total), 0, ",", "."), "T", 0, "R");
	$pdf->Ln(10);
	$pdf->SetFont('arial','UI',10);
	$pdf->Cell(151, 7, "Terbilang : ".ucfirst($a->terbilang($harga_total_terbilang))."rupiah", 0, 0, "L");
	$pdf->Ln(12);
$pdf->SetFont('arial','',10);
$pdf->MultiCell(0, 7, "Taksiran harga emas didasarkan pada Harga Dasar Lelang Emas PT Pegadaian (Persero) pada tanggal pemeriksaan. Pengujian menggunakan kaidah-kaidah yang dipakai di PT Pegadaian (Persero) yaitu analisa kimia dan analisa berat jenis. Surat Keterangan Hasil Pengujian ini  bukan merupakan rekomendasi untuk dasar transaksi jual beli.");
$pdf->Ln(5);
$pdf->Cell(180, 7, "Demikian disampaikan untuk dapat digunakan seperlunya.");

$pdf->SetFont('arial','',10);

$width_bot = array(140,95,5);
$pdf->Ln(20);
$pdf->Cell($width_bot[0], 7, '', '', 0, 'C');
$pdf->Cell($width_bot[1], 7, 'Jakarta, '.$tgl, '', 0, 'L');
$pdf->Ln();
$pdf->Cell($width_bot[0], 7, '', '', 0, 'C');
//$pdf->Cell($width_bot[1], 7, 'Profesional Pengelola G-Lab', '', 0, 'L');
$pdf->Ln(15);
$pdf->Cell($width_bot[0], 5, '', '', 0, 'C');
$pdf->Cell($width_bot[1], 5, '('.$gemolog.')', '', 0, 'L');
$pdf->Ln();
$pdf->Output();

	// Hadi
	/*$dataUpdate = array(
                    "taksiran" => $harga_total, 
                    "update_by"  => sessionValue('id'),
                    "update_date"      => date('Y-m-d H:i:s')
                    );
	$this->db->where('id', $id);
	$this->db->update("tb_taksiran", $dataUpdate);*/
	
	$dataUpdate2 = array(
                    "print" => "Sudah", 
                    "status" => "NOT APPROVED",
                    "update_by"  => sessionValue('id'),
                    "update_date"      => date('Y-m-d H:i:s')
                    );
	$this->db->where('id_order', $id_order);
	$this->db->update("step", $dataUpdate2);
	
	}
}
?>