<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class tambahcif extends CI_Controller{
	function __construct(){
		parent::__construct();

		if(!isLogin()){
			redirect('');
		}
	}
	
    function tambahcif(){
        parent::__construct();
		
		if(!isLogin()){
			goLogin();
		}
        $this->load->library('fpdf');
        $this->load->library('Barcode39');
    }
    
    function index($text=NULL){
        $data["filelist"] = "tambahcif/tambahcif";
        $data["title"] = "Front";
        $data["title_menu"] = "Front";
        $data["menu"] = "tambahcif";
		
		$sql = $this->db->query("SELECT id_member FROM tb_member where kode='A'");
		$cif_id = $sql->num_rows();
		
		$data["cif_number"] = generateCodeCIF($cif_id);
		
		$sql2 = $this->db->query("SELECT id_member FROM tb_member where kode='B'");
		$cor_id = $sql2->num_rows();
		
		$data["cor_number"] = generateCodeCOR($cor_id);
		
        getHTMLWeb($data);
    }

    public function kota($id)
    {
        $this->load->view('get_kota', array('id'=>$id));
    }

    public function kecam($id)
    {
        $this->load->view('get_keca', array('id'=>$id));
    }

    public function kelur($id)
    {
        $this->load->view('get_kelur', array('id'=>$id));
    }

    public function kode_pos($id)
    {
        $this->load->view('get_kode_pos', array('id'=>$id));
    }

    public function edit_indiv($id)
    {
        $this->load->view('edit', array('id'=>$id));
    }

    public function delete($id)
    {
        $this->db->delete('tb_member', array('id_member'=>$id));
    }

    public function save_cif_individu()
    {
        $no_cif = $this->input->post('no_cif');
        //$cabang = $this->input->post('cabang');
        $nama_depan = $this->input->post('nama_depan');
        //$nama_tengah = $this->input->post('nama_tengah');
        //$nama_belakang = $this->input->post('nama_belakang');
        $nama_ibu = $this->input->post('nama_ibu');
        //$gelar = $this->input->post('gelar');
        $tempat_lahir = $this->input->post('tempat_lahir');
        $tanggal_lahir = $this->input->post('tanggal_lahir');
        $agama = $this->input->post('agama');
        $identitas = $this->input->post('identitas');
        $no_identitas = $this->input->post('no_identitas');
        $masa_berlaku = $this->input->post('masa_berlaku');
        $jenis_kelamin = $this->input->post('jenis_kelamin');
        $pendidikan = $this->input->post('pendidikan');
        $perkawinan = $this->input->post('perkawinan');
        $nama_pasangan = $this->input->post('nama_pasangan');
        $telpon = $this->input->post('telpon');
        $handphone = $this->input->post('handphone');
        $email = $this->input->post('email');
        $no_npwp = $this->input->post('no_npwp');
        $kewarganegaraan = $this->input->post('kewarganegaraan');
        $kewarganegaraan_lainnya = $this->input->post('kewarganegaraan_lainnya');
        $sumber_dana = $this->input->post('sumber_dana');
        $penghasilan = $this->input->post('penghasilan');
        $pekerjaan = $this->input->post('pekerjaan');
        $propinsi = $this->input->post('propinsi');
        $kota = $this->input->post('kota');
        $kecamatan = $this->input->post('kecamatan');
        $kelurahan = $this->input->post('kelurahan');		
        $kode_pos = $this->input->post('kode_pos');
        $status_tempat_tinggal = $this->input->post('status_tempat_tinggal');
        $menempati_sejak = $this->input->post('menempati_sejak');
        $alamat_domisili = $this->input->post('alamat_domisili');
        $propinsi2 = $this->input->post('propinsi2');
        $kota2 = $this->input->post('kota2');
        $kecamatan2 = $this->input->post('kecamatan2');
        $kelurahan2 = $this->input->post('kelurahan2');
        $kode_pos2 = $this->input->post('kode_pos2');
        $alamat_surat_menyurat = $this->input->post('alamat_surat_menyurat');
        //$produk = $this->input->post('produk');
		$alamat = $this->input->post('alamat');
		
        date_default_timezone_set('Asia/Jakarta');
        $tgl = date('Y-m-d H:i:s');
		
		$sqlCheck = $this->db->query("SELECT id_member FROM tb_member where no_identitas='".$no_identitas."'");
		$numCheck = $sqlCheck->num_rows();
		
		$success = false;
		$msg = "";
		if($numCheck>0){
			$msg = "Data sudah tersedia.";
		}else{		
			if($menempati_sejak==""){
				$menempati_sejak = NULL;
			}else{
				$menempati_sejak = date('Y-m-d', strtotime($menempati_sejak));
			}
			
			$sql = $this->db->query("SELECT id_member FROM tb_member where kode='A'");
			$cif_id = $sql->num_rows();
			$code = generateCodeCIF($cif_id);

			$data = array(
				'id_member' => $code,
				'kode' => 'A',
				'cabang' => sessionValue('kode_store'),
				'first_name' => $nama_depan,
				//'middle_name' => $nama_tengah,
				//'last_name' => $nama_belakang,			
				'nama_ibu' => $nama_ibu,
				//'gelar' => $gelar,
				'tempat_lahir' => $tempat_lahir,
				'tanggal_lahir' => date('Y-m-d', strtotime($tanggal_lahir)),
				'agama' => $agama,	
				'identitas' => $identitas,
				'no_identitas' => $no_identitas,
				'masa_berlaku' => date('Y-m-d', strtotime($masa_berlaku)),
				'jenis_kelamin' => $jenis_kelamin,
				'pendidikan' => $pendidikan,
				'perkawinan' => $perkawinan,
				'nama_pasangan' => $nama_pasangan,
				'phone' => $handphone,	
				'telpon_rumah' => $telpon,
				'no_npwp' => $no_npwp,
				'kewarganegaraan' => $kewarganegaraan,
				'kewarganegaraan_lainnya' => $kewarganegaraan_lainnya,
				'sumber_dana' => $sumber_dana,
				'penghasilan' => $penghasilan,
				'pekerjaan' => $pekerjaan,
				'status_tempat_tinggal' => $status_tempat_tinggal,
				'menempati_sejak' => $menempati_sejak,
				'alamat_surat_menyurat' => $alamat_surat_menyurat,
				//'produk' => $produk,				
				'province' => $propinsi,
				'city' => $kota,
				'district' => $kecamatan,
				'kel' => $kelurahan,
				'postal_code' => $kode_pos,
				'address' => $alamat,
				'province2' => $propinsi2,
				'city2' => $kota2,
				'district2' => $kecamatan2,
				'kel2' => $kelurahan2,
				'postal_code2' => $kode_pos,
				'address2' => $alamat_domisili,
				'email' => $email,
				'member_from' => $tgl,
				'create_date' => $tgl,
				'create_by' => checkSession('id')
			);
			if($this->db->insert('tb_member', $data)){
				$success = true;
				$msg = "";
			}else{
				$msg = "Data can't be inserted";
			}
		
		}
		
		echo json_encode(array("success"=>$success, "msg"=>$msg));
    }
	
	public function save_cif_coorporate()
    {
        $no_cor = $this->input->post('no_cor');
        $nama_perusahaan = $this->input->post('nama_perusahaan');
        $tanggal_pendirian = $this->input->post('tanggal_pendirian');
        $telpon_perusahaan = $this->input->post('telpon_perusahaan');
        $alamat_perusahaan = $this->input->post('alamat_perusahaan');
        $propinsi_perusahaan = $this->input->post('propinsi_perusahaan');
        $kota_perusahaan = $this->input->post('kota_perusahaan');
        $kecamatan_perusahaan = $this->input->post('kecamatan_perusahaan');
        $kode_pos_perusahaan = $this->input->post('kode_pos_perusahaan');
        $email_perusahaan = $this->input->post('email_perusahaan');
        $no_npwp_perusahaan = $this->input->post('no_npwp_perusahaan');
        $no_rekening_perusahaan = $this->input->post('no_rekening_perusahaan');
        $siup_perusahaan = $this->input->post('siup_perusahaan');
        $bidang_usaha_perusahaan = $this->input->post('bidang_usaha_perusahaan');
        $tdp_perusahaan = $this->input->post('tdp_perusahaan');
        $nama_izin_usaha_perusahaan = $this->input->post('nama_izin_usaha_perusahaan');
        $no_izin_usaha_perusahaan = $this->input->post('no_izin_usaha_perusahaan');
        $bentuk_perusahaan = $this->input->post('bentuk_perusahaan');
        $bentuk_perusahaan_lainnya = $this->input->post('bentuk_perusahaan_lainnya');
        $tujuan_transaksi_perusahaan = $this->input->post('tujuan_transaksi_perusahaan');
		
        date_default_timezone_set('Asia/Jakarta');
        $tgl = date('Y-m-d H:i:s');
		
		$sqlCheck = $this->db->query("SELECT id_member FROM tb_member where no_npwp='".$no_npwp_perusahaan."'");
		$numCheck = $sqlCheck->num_rows();
		
		$success = false;
		$msg = "";
		if($numCheck>0){
			$msg = "Data sudah tersedia.";
		}else{	
			$sql = $this->db->query("SELECT id_member FROM tb_member where kode='B'");
			$cor_id = $sql->num_rows();
			
			$code = generateCodeCOR($cor_id);

			$data = array(
				'id_member' => $code,
				'kode' => 'B',
				'corp_name' => $nama_perusahaan,
				'corp_form' => $bentuk_perusahaan,
				'corp_form_other' => $bentuk_perusahaan_lainnya,
				'phone' => $telpon_perusahaan,	
				'no_npwp' => $no_npwp_perusahaan,
				'kewarganegaraan' => $kewarganegaraan,
				'tanggal_lahir' => date('Y-m-d', strtotime($tanggal_pendirian)),
				'province' => $propinsi_perusahaan,
				'city' => $kota_perusahaan,
				'district' => $kecamatan_perusahaan,
				'postal_code' => $kode_pos_perusahaan,
				'address' => $alamat_perusahaan,
				'email' => $email_perusahaan,
				'corp_norek' => $no_rekening_perusahaan,
				'corp_siup' => $siup_perusahaan,
				'corp_bidang_usaha' => $bidang_usaha_perusahaan,
				'corp_tdp' => $tdp_perusahaan,
				'corp_nama_izin_usaha' => $nama_izin_usaha_perusahaan,
				'corp_no_izin_usaha' => $no_izin_usaha_perusahaan,
				'corp_tujuan_transaksi' => $tujuan_transaksi_perusahaan,
				'member_from' => $tgl,
				'create_date' => $tgl,
				'create_by' => checkSession('id')
			);
			if($this->db->insert('tb_member', $data)){
				$success = true;
				$msg = "";
			}else{
				$msg = "Data can't be inserted";
			}
		}
		
		echo json_encode(array("success"=>$success, "msg"=>$msg));
    }

    public function edit_mem_indi($id)
    {
        $fir_name   = $this->input->post('fir_name');
        $mid_name   = $this->input->post('mid_name');
        $las_name   = $this->input->post('las_name');
        $phone      = $this->input->post('phone');
        $prov       = $this->input->post('prov');
        $kota       = $this->input->post('kota');
        $keca       = $this->input->post('keca');
        $postal     = $this->input->post('postal');
        $address    = $this->input->post('address');
        $email      = $this->input->post('email');
        date_default_timezone_set('Asia/Jakarta');
        $tgl = date('Y-m-d');

        $data = array(
            'first_name'    => $fir_name,
            'middle_name'   => $mid_name,
            'last_name'     => $las_name,
            'phone'         => $phone,
            'province'      => $prov,
            'city'          => $kota,
            'district'      => $keca,
            'postal_code'   => $postal,
            'address'       => $address,
            'email'         => $email,
        );
        $this->db->where('id_member', $id);
        $this->db->update('tb_member', $data);
    }

    public function print_member(){
        $id = $this->uri->segment(3);
        $bc = new Barcode39($id);
        // set barcode bar thickness (thick bars) 
        $bc->barcode_bar_thick = 5; 
        // set barcode bar thickness (thin bars) 
        $bc->barcode_bar_thin = 2; 
        $bc->draw("asset/barcode-member/".$id.".gif");
        $que = $this->db->query("SELECT * FROM tb_member WHERE id_member='$id'");
        foreach ($que->result() as $get) {
        
        $pdf = new FPDF('L','cm',array(9, 5.5));
        $pdf->SetAutoPageBreak(true, 0);
        $pdf->AddPage();
        $pdf->SetFont('Helvetica','B',10);
        $pdf->SetXY(0.3, 2.5);
        $pdf->Cell(0, 0, strtoupper($get->first_name)." ".strtoupper($get->middle_name)." ".strtoupper($get->last_name));
        $pdf->Ln();
        $pdf->SetXY(0.3, 3);
        $pdf->Cell(0, 0, $get->id_member);
        $pdf->Image("asset/barcode-member/".$id.".gif", 0.3, 3.45);
        }
        $pdf->Output();
    }
}
