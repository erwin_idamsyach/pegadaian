	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>&nbsp;</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="box box-default">
            <div class="box-body">
              <b>PRINT INDIVIDUALY</b>
              <div style="border: 1px solid black; margin-bottom: 10px;"></div>
              <table class="table table-bordered table-striped table-hover" id="example1">
                <thead>
                  <tr>
                    <th class="text-center">No</th>
                    <th class="text-center">ID Object</th>
                    <th class="text-center">Identification Result</th>
                    <th class="text-center">Create Date</th>
                    <th class="text-center">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                    $no = 1;
                    $get_gem_card = $this->db->query("SELECT step.id_object, step.id_order,
                      tb_lab_desk.obj_natural, tb_lab_desk.variety, tb_lab_desk.nama_batu, step.create_date
                      FROM
                      tb_lab_desk, step
                      WHERE
                      step.id_object = tb_lab_desk.id_object AND
                      step.store='".sessionValue('kode_store')."' AND
                      step.gem_card != '' AND
                      step.print_gem = 'Belum' AND step.step='LAB_DESK' AND
                      step.delete_by = ''");
                    foreach ($get_gem_card->result() as $get) {
                      ?>
                      <tr>
                        <td class="text-center"><?php echo $no++ ?></td>
                        <td><?php echo $get->id_object ?></td>
                        <td><?php echo $get->obj_natural." ".$get->variety." ".$get->nama_batu ?></td>
                        <td><?php $dd = date_create($get->create_date); echo date_format($dd, 'D, d-m-Y') ?></td>
                        <td>
                          <div class="pull-right">
                            <a href="<?php echo site_url('ready/print_gem_card').'/'.$get->id_object ?>" class="btn btn-info" target='_blank' data-toggle="tooltip" data-placement="bottom" title="Print Gem Card"><i class="fa fa-print"></i></a>&nbsp;&nbsp;
                          </div>
                        </td>
                      </tr>
                      <?php
                    }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
          <div class="box box-default">
            <div class="box-body">
              <b>PRINT BY ID ORDER</b>
              <div style="border: 1px solid black; margin-bottom: 10px;"></div>
              <table class="table table-bordered table-striped table-hover" id="example2">
                <thead>
                  <tr>
                    <th class="text-center">No</th>
                    <th class="text-center">ID Order</th>
                    <th class="text-center">Total Gem Card Requested</th>
                    <th class="text-center">Create Date</th>
                    <th class="text-center">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                    $no = 1;
                    $get_gem_card = $this->db->query("SELECT * FROM step
                      WHERE
                      step.step='LAB_DESK' AND
                      step.print_gem='Belum' AND
                      step.store='".sessionValue('kode_store')."' AND
                      delete_by=''
                      GROUP BY id_order");
                    foreach ($get_gem_card->result() as $get) {
                      ?>
                      <tr>
                        <td class="text-center"><?php echo $no++ ?></td>
                        <td><?php echo $get->id_order ?></td>
                        <td class="text-center">
                          <?php
                          $count = $this->db->query("SELECT COUNT(*) as total FROM `step` WHERE id_order='".$get->id_order."' AND step.step='LAB_DESK' AND step.print_gem='Belum' AND gem_card != '' AND delete_by=''");
                          foreach ($count->result() as $ne) {
                            echo $ne->total;
                          }
                          ?>
                        </td>
                        <td>
                          <?php
                          $dd = date_create($get->create_date); echo date_format($dd, 'D, d-m-Y');
                          ?>
                        </td>
                        <td>
                          <div class="pull-right">
                            <a href="<?php echo site_url('ready/print_multiple_gem').'/'.$get->id_order ?>" class="btn btn-info" target="_blank" data-toggle="tooltip" data-placement="bottom" title="Print Gem Card"><i class="fa fa-print"></i></a>&nbsp;&nbsp;
                          </div>
                        </td>
                      </tr>
                      <?php
                    }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->