<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*

* Controller Print Gem Card

* Create By : Erwin Idamsyach Putra

* 23 April 2016

*/

class ready extends CI_Controller{

	function __construct(){

		parent::__construct();



		if(!isLogin()){

			goLogin();

		}

	}

	

	public function index(){

		$data['filelist'] = 'ready/front';

		$data['title'] = 'Print Report';

		$data['title_menu'] = 'Print';

		$data['menu'] = 'print-gem';



		getHTMLWeb($data);

	}



	public function print_gem_card(){

		$this->load->library('fpdf');

        $this->load->library('Barcode39');

		

		$id_obj = $this->uri->segment(3);

		

		$getNumNoCert = $this->db->query("SELECT COUNT(no_brief) no_brief, no_brief noCert FROM step WHERE id_object='".$id_obj."'");

		$tmpNumNoCert = $getNumNoCert->result_array();

		

		if($tmpNumNoCert[0]['no_brief']==0){

			$getNoCert = $this->db->query("SELECT no_brief FROM step ORDER BY no_brief DESC LIMIT 0, 1");

			foreach ($getNoCert->result() as $no_cert) {

				$noCert = $no_cert->no_brief;

			}

			if(!isset($noCert)){		

				$noCert = 0;

			}else{ }

			 

			$noCert = substr($noCert, 3);

			$noCert = $noCert+1;
			$getServices = $this->db->query("SELECT kode_produk FROM tb_service WHERE kode='BR' LIMIT 0, 1");
			$kode_produk = "";
			foreach ($getServices->result() as $tmpService) {
				$kode_produk = $tmpService->kode_produk;
			}
			$kode_produk = sessionValue('kode_unit')."-".date('y')."-".$kode_produk."-";

			$noCert = generateNoReport($kode_produk, $noCert);

			$this->db->where("id_object", $id_obj);

			$this->db->update("step", array("no_brief" => $noCert, 'update_by'  => sessionValue('id'), 'update_date' => date('Y-m-d H:i:s')));

			

			$bc = new Barcode39($noCert);
			$bc->barcode_text_size = 2;
			$bc->barcode_bar_thick = 2; 

			$bc->draw("asset/barcode-fpjs/".$noCert.".gif");

		}else{

			$noCert = $tmpNumNoCert[0]['noCert'];

		}

		

		$get_data = $this->db->query("SELECT tb_lab_desk.*, color_stone.jenis_warna, color_stone.jenis_warna, tb_login.nama approve_name, tb_login.tanda_tangan
			FROM tb_lab_desk		
			LEFT JOIN color_stone ON color_stone.code = tb_lab_desk.obj_color 
			LEFT JOIN step ON step.id_object = tb_lab_desk.id_object
			LEFT JOIN tb_login ON tb_login.id = step.approve_by
			WHERE tb_lab_desk.id_object='$id_obj'");



		$this->db->where('id_object', $id_obj);

		$this->db->update('step', array('print_gem'=>'Sudah', 'status'=>'NOT APPROVED', 'update_by'  => sessionValue('id'), 'update_date' => date('Y-m-d H:i:s')));



		$pdf = new FPDF('L','mm','A4');

		$pdf->AddFont('ronnia','','ronnia.php');
		$pdf->AddFont('ronnia','B','ronniab.php');
		$pdf->AddFont('ronnia','BI','ronniabi.php');
		$pdf->AddFont('ronnia','I','ronniai.php');

		foreach ($get_data->result() as $get) {



		$create_date = date_create($get->finish);

		$day 		 = date_format($create_date, 'd');

		$month 		 = date_format($create_date, 'm');

		$year 		 = date_format($create_date, 'Y');

		switch ($month) {

			case '01':

				$month = "Januari";

				break;

			case '02':

				$month = "Februari";

				break;

			case '03':

				$month = "Maret";

				break;

			case '04':

				$month = "April";

				break;

			case '05':

				$month = "Mei";

				break;

			case '06':

				$month = "Juni";

				break;

			case '07':

				$month = "Juli";

				break;

			case '08':

				$month = "Agustus";

				break;

			case '09':

				$month = "September";

				break;

			case '10':

				$month = "Oktober";

				break;

			case '11':

				$month = "November";

				break;

			case '12':

				$month = "Desember";

				break;

			default:

				$month = $month;

				break;

		}

		$natural = ucwords(strtolower($get->obj_natural));

		$variety = ucwords(strtolower($get->variety));

		$species = ucwords(strtolower($get->nama_batu));



		if($get->comment == ''){

			$comment = '';

		}else{

			$comment = $get->comment;

		}



		$pdf->AddPage();

		

		// Hadi		
		$pdf->SetLineWidth(1);
		$pdf->Rect(14, 39, 85, 85, 'D');
		/*$pdf->Image('asset/barcode-fpjs/'.$noCert.'.gif', 1, 13, 85, 22);*/
		
		$pdf->SetFont('ronnia','',23.7);
		
		/*$pdf->SetXY(1, 38);
		$pdf->Cell(80, 0, $noCert, 0, 0, 'L');*/

		$pdf->SetXY(0, 134);

		$pdf->Cell(111, 0, $noCert, 0, 0, 'C');

		$pdf->SetXY(0, 181);
		$pdf->SetFont('ronnia','U',25);
		$pdf->Cell(87, 0, "", 0, 0, 'C');
		$pdf->SetFont('ronnia','',25);
		// Tanda Tangan
		/*if($get->tanda_tangan!=""){
			$pdf->Image('asset/signatures/'.$get->tanda_tangan, 23, 157.5, 41, 19.5);
		}*/
		$pdf->SetLineWidth(0);
		//$pdf->Line(1, 185, 87, 185);



		$pdf->SetXY(0, 189);

		/*$pdf->Cell(87, 0, 'Accredited Gemologist', 0, 0, 'C');*/
		// End Hadi

		

		//$pdf->Image('asset/images/Logo.png', 210, 5, 65, 40);

		$x_pos = 98;

		$pdf->SetXY($x_pos, 65);

		$pdf->SetFont('ronnia','U',28);

		/*$pdf->Cell(0, 5, 'Gemstone Brief Report', 0, 0, 'L');*/

		
		if($get->obj_image!=""){

		//$pdf->SetDrawColor(10, 185, 0);

		//$pdf->SetLineWidth(1);

		//$pdf->Line(5, 42, 290, 42);

		$pdf->Image('asset/images/'.$get->obj_image, 15, 40, 83, 83);

		}

		

		if(strtolower($natural)=="none"){

			$natural = "";

		}

		if(strtolower($variety)=="none"){

			$variety = "";

		}

		if(strtolower($species)=="none"){

			$species = "";

		}

		

		if($natural!=""){

			$identifikasi = ucwords(strtolower($natural))." ";

		}else{

			$identifikasi = "";

		}

		if($variety!=""){

			$identifikasi .= ucwords(strtolower($variety))." ";

		}else{

			$identifikasi .= "";

		}

		if($species!=""){

			$identifikasi2 = ucwords(strtolower($species));

		}else{

			$identifikasi2 = "";

		}

		$pdf->Ln(16);
		
		$pdf->SetX($x_pos);

		$x = $pdf->GetX();

		$y = $pdf->GetY();

		$pdf->SetFont('ronnia','',36);
		$pdf->setXY(113, 42);
		$pdf->MultiCell(80, 11, $natural."\n".$variety); // $natural." ".


		$pdf->Ln(16);

		$pdf->SetX($x_pos);

		$x = $pdf->GetX();

		$y = $pdf->GetY();

		$pdf->SetFont('ronnia','',25);

		$pdf->SetX($x_pos+15);

		$pdf->SetFont('ronnia','',25);

		$pdf->Cell(0, 0, ucwords($get->jenis_warna));


		$pdf->Ln(12);

		$pdf->SetX($x_pos);

		$x = $pdf->GetX();

		$y = $pdf->GetY();

		$pdf->SetX($x_pos+15);

		$pdf->SetFont('ronnia','',25);

		$pdf->Cell(0, 0, $get->obj_shape." ".$get->obj_cut);


		$pdf->Ln(12);

		$pdf->SetX($x_pos);

		$x = $pdf->GetX();

		$y = $pdf->GetY();

		$pdf->SetX($x_pos+15);

		$pdf->SetFont('ronnia','',25);

		$pdf->Cell(0, 0, "est. ".$get->obj_length.' x '.$get->obj_width." x ".$get->obj_height." mm");


		$pdf->Ln(12);

		$pdf->SetX($x_pos);

		$x = $pdf->GetX();

		$y = $pdf->GetY();

		$pdf->SetX($x_pos+15);

		$pdf->SetFont('ronnia','',25);

		$pdf->Cell(0, 0, "est. ".$get->obj_weight." cts");

		

		if(strtolower($get->note)=="none"){

			$get->note = "";

		}

		//$pdf->SetTextColor(0,0,0);
		$nt = $get->note;

		}if($nt == "" || $nt == null){
			$nt = "-";
		}else{

		}

		$pdf->Ln(12);

		$pdf->SetX($x_pos);

		$x = $pdf->GetX();

		$y = $pdf->GetY();

		$pdf->SetX($x_pos+15);

		$pdf->SetFont('ronnia','',25);		
		
		$pdf->MultiCell(80, 12, $nt,0,"L");

		//$pdf->Ln();


		$pdf->Output();

	}

}

?>