
            <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <section class="content">
          <div class="box box-warning">
            <div class="box-body">
              <div class="col-md-12">
                <div class="row">
                  <div class="form-group">
                    <div class="col-md-12 text-right">            
                      <h4><b>F03</b></h4>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-12">
                <div class="row">
                  <div class="form-group">
                    <div class="col-md-12">
                      <h5><b>Taksiran Emas</b></h5>
                      <hr style="border:1px solid black;margin-top:-5px;">
                    </div>
                  </div>
                </div>
                <div id="physy">
                  <div class="col-md-12" style="width:100%;">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="row">
                          <div class="col-md-4">
                            <div class="form-group">
                              <form action="data-ajax/save_img.php" id="frmuploadImgx" method="post" target="iframeUploadImgx" enctype="multipart/form-data">
                              <input type="file" class="drop form-control" name="gambar" id="drop" onchange="submitImagex()">
                              </form>
                            </div>
                          </div>
                          <form action="<?php echo base_url() ?>obj_today/update_gd/" method="post">
                          <input class="hidden" type="text" name="gambaroutx" id="gambaroutx">
                          <iframe class="hidden" name="iframeUploadImgx" id="iframeUploadImgx"></iframe> 
                          <div class="col-md-4">
                            <div class="form-group has-success has-feedback">
                            <label class="control-label" for="inputSuccess">Harga Emas (/gr) (*)</label>
                            <input type="text" class="form-control" id="id" name="girdle" value="" placeholder="Enter ..." required>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="form-group has-success has-feedback">
                            <label class="control-label" for="inputSuccess">Karatase Emas (*)</label>
                            <input type="text" class="form-control" id="id" name="culet" value="" placeholder="Enter ..." required>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="form-group has-success has-feedback">
                            <label class="control-label" for="inputSuccess">Berat Emas (*)</label>
                            <input type="text" class="form-control" id="id" name="fluorescence" value="" placeholder="Enter ..." required>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="form-group has-success has-feedback">
                            <label class="control-label" for="inputSuccess">Harga Taksiran (*)</label>
                            <input type="text" class="form-control" id="id" name="key" value="" placeholder="Enter ..." required>
                            </div>
                          </div>
                          <div class="col-md-12">
                            <div class="form-group">
                              <div class="col-md-12">
                                <button type="submit" name="submit" class="btn btn-primary pull-right" style="margin-bottom:20px;margin-right:10px;"><i class="fa fa-save"></i> Save </button>
                                <button type="reset" class="btn btn-default pull-right" style="margin-bottom:20px;margin-right:10px;"> <i class="fa fa-refresh"></i> Clear</button>
                                <a href="" class="btn btn-danger pull-right" style="margin-bottom:20px;margin-right:10px;"><i class="fa fa-chevron-left"></i> Back</a>
                              </div>
                            </div>
                          </div>
                        </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
        <script src="../admin/dropify-master/dist/js/dropify.min.js"></script>
        <script>
          $("#drop").dropify();
          function submitImagex(){
            $( "#frmuploadImgx" ).submit();
          }
        </script>