<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class obj_finish extends CI_Controller{
    function obj_finish(){
        parent::__construct();
		
		if(!isLogin()){
			goLogin();
		}
    }
    
    function index($text=NULL){
        $data["filelist"] = "obj_finish/obj_finish";
        $data["title"] = "Lab";
        $data["title_menu"] = "Lab";
        $data["menu"] = "finish_sertifikasi";
		
        getHTMLWeb($data);
    }

    function grading(){
        $data["filelist"] = "obj_finish/grading";
        $data["title"] = "Lab";
        $data["title_menu"] = "Lab";
        $data["menu"] = "finish_grading";
        
        getHTMLWeb($data);
    }

    function taksiran(){
        $data["filelist"] = "obj_finish/taksiran_front";
        $data["title"] = "Lab";
        $data["title_menu"] = "Lab";
        $data["menu"] = "finish_taksiran";
        
        getHTMLWeb($data);
    }
	
	function taksiran_detail($id_order=NULL){
		$id_order = str_replace('-','/',$id_order);
        $data["id_order"] = $id_order;
		
        $data["filelist"] = "obj_finish/taksiran";
        $data["title"] = "Lab";
        $data["title_menu"] = "Lab";
        $data["menu"] = "finish_taksiran";
        
        getHTMLWeb($data);
    }

    function delete_obj($id)
    {
        $user = sessionValue('username');
        date_default_timezone_set('Asia/Jakarta');
        $now  = date('Y-m-d');
        $data = array(
            'delete_by'     => $user,
            'delete_date'   => $now
        );
        $this->db->where('id_object', $id);
        $this->db->update('tb_front_desk', $data);

        $this->db->where('id_object', $id);
        $this->db->update('step', $data);

        redirect('obj_finish');
    }
}