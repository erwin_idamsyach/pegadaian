<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
* Controller Master Fracture
* Create By : Erwin Idamsyach Putra
* 23 April 2016
*/
class fracture extends CI_Controller{
	function __construct(){
		parent::__construct();

		if(!isLogin()){
			goLogin();
		}
	}
	
	public function index(){
		$data['filelist'] = 'fracture/front';
		$data['title'] = 'Master Lab Desk';
		$data['title_menu'] = 'Admin';
		$data['menu'] = 'fracture';

		getHTMLWeb($data);
	}

	public function add_fracture(){
		$fracture = $this->input->post('fracture');
		$descr = $this->input->post('descr');

		$data = array(
			'fracture' => $fracture,
			'penjelasan' => $descr,
			'create_by' => sessionValue('nama'),
			'create_date' => date('Y-m-d')	
			);
		$this->db->insert('fracture', $data);
		redirect(site_url('fracture'));
	}

	public function edit_fracture(){
		$id  = $this->uri->segment(3);

		$data['filelist'] = 'fracture/front';
		$data['title'] = 'Master Lab Desk';
		$data['title_menu'] = 'Admin';
		$data['menu'] = 'fracture';
		$data['id_fracture'] = $id;

		getHTMLWeb($data);
	}

	public function update_fracture(){
		$id   = $this->input->post('id');
		$fracture = $this->input->post('fracture');
		$descr = $this->input->post('descr');

		$data = array(
			'fracture' => $fracture,
			'penjelasan' => $descr,
			'update_by' => sessionValue('nama'),
			'update_date' => date('Y-m-d')
			);

		$this->db->where('id', $id);
		$this->db->update('fracture', $data);
		redirect(site_url('fracture'));
	}

	public function delete_fracture(){
		$id = $this->uri->segment(3);

		$data = array(
			'delete_by' => sessionValue('nama'),
			'delete_date' => date('Y-m-d')
			);
		$this->db->where('id', $id);
		$this->db->update('fracture', $data);
		redirect(site_url('fracture'));
	}
}
?>