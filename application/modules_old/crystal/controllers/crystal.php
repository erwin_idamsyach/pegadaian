<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
* Controller Master Crystal
* Create By : Erwin Idamsyach Putra
* 23 April 2016
*/
class crystal extends CI_Controller{
	function __construct(){
		parent::__construct();

		if(!isLogin()){
			goLogin();
		}
	}
	
	public function index(){
		$data['filelist'] = 'crystal/front';
		$data['title'] = 'Master Lab Desk';
		$data['title_menu'] = 'Admin';
		$data['menu'] = 'crystal';

		getHTMLWeb($data);
	}

	public function add_crystal(){
		$name = $this->input->post('crystal');

		$data = array(
			'crystal_gems' => $name,
			'create_by' => sessionValue('nama'),
			'create_date' => date('Y-m-d')	
			);
		$this->db->insert('crystal', $data);
		redirect(site_url('crystal'));
	}

	public function edit_crystal(){
		$id  = $this->uri->segment(3);

		$data['filelist'] = 'crystal/front';
		$data['title'] = 'Crystal';
		$data['title_menu'] = 'Admin';
		$data['menu'] = 'crystal';
		$data['id_crystal'] = $id;

		getHTMLWeb($data);
	}

	public function update_crystal(){
		$id   = $this->input->post('id');
		$name = $this->input->post('crystal');

		$data = array(
			'crystal_gems' => $name,
			'update_by' => sessionValue('nama'),
			'update_date' => date('Y-m-d')
			);

		$this->db->where('id', $id);
		$this->db->update('crystal', $data);
		redirect(site_url('crystal'));
	}

	public function delete_crystal(){
		$id = $this->uri->segment(3);

		$data = array(
			'delete_by' => sessionValue('nama'),
			'delete_date' => date('Y-m-d')
			);
		$this->db->where('id', $id);
		$this->db->update('crystal', $data);
		redirect(site_url('crystal'));
	}
}
?>