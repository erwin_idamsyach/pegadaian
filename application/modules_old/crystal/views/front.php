<!-- front.php, Create By : Erwin Idamsyach Putra -->
<!-- Hadi -->
<script type="text/javascript">
    jQuery(function($){
        $('#example1').DataTable();
    });
</script>
	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>&nbsp;</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
            <li><?php echo $title;?></li>
            <li class="active">Crystal</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
        	<?php
        	if(!isset($id_crystal)){
        		?>
			<div class="box box-default">
				<div class="box-body">
					<b>ADD CRYSTAL</b>
					<div style="border: 1px solid black; margin-bottom: 10px"></div>
					<form action="<?php echo site_url('crystal/add_crystal') ?>" method="post">
					<div class="row">
						<div class="col-md-3 col-sm-6">
							<div class="form-group">
								<label>Crystal Type</label>
								<input type="text" name="crystal" class="form-control" placeholder="crystal..">
							</div>
						</div>
						<div class="col-md-9">
							<div class="pull-left">
								<button type="submit" class="btn btn-primary" style="margin-top: 25px;">Submit</button>
							</div>
						</div>
					</div>
					</form>
				</div>
			</div>
			<?php
        	}else{
        		$get_crystal_edit = $this->db->query("SELECT * FROM crystal WHERE id='$id_crystal'");
        		foreach ($get_crystal_edit->result() as $ed) {
        			?>
        	<div class="box box-default">
				<div class="box-body">
					<b>EDIT CRYSTAL</b>
					<div style="border: 1px solid black; margin-bottom: 10px"></div>
					<form action="<?php echo site_url('crystal/update_crystal') ?>" method="post">
					<div class="row">
						<div class="col-md-3 col-sm-6">
							<div class="form-group">
								<label>Crystal Type</label>
								<input type="hidden" name="id" value="<?php echo $ed->id ?>">
								<input type="text" name="crystal" class="form-control" placeholder="crystal.." value="<?php echo $ed->crystal_gems ?>">
							</div>
						</div>
						<div class="col-md-9">
							<div class="pull-left">
								<button type="submit" class="btn btn-primary" style="margin-top: 25px;">Submit</button>
							</div>
						</div>
					</div>
					</form>
				</div>
			</div>
        		<?php
        		}
        	}
        	?>
			<div class="box box-default">
				<div class="box-body">
					<b>DATA CRYSTAL</b>
					<div style="border: 1px solid black; margin-bottom: 10px"></div>
					<table class="table table-bordered table-striped table-hover" id="example1" width="100%">
						<thead>
							<tr>
								<th class="text-center" width="10%">No</th>
								<th class="text-center">Crystal</th>
								<th class="text-center" width="10%">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$no = 1;
							$get_store = $this->db->query("SELECT * FROM crystal WHERE delete_by =''");
							foreach ($get_store->result() as $store) {
								?>
							<tr>
								<td class="text-center"><?php echo $no++; ?></td>
								<td><?php echo $store->crystal_gems ?></td>
								<td>
									<div class="pull-right">
										<a href="<?php echo site_url('crystal/edit_crystal').'/'.$store->id ?>" class="btn btn-warning" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;&nbsp;

										<a href="<?php echo site_url('crystal/delete_crystal').'/'.$store->id ?>" class="btn btn-danger" data-toggle="tooltip" data-placement="bottom" onclick="return confirm('Are you sure to delete this data?');" title="Delete"><i class="fa fa-trash"></i></a>
									</div>
								</td>
							</tr>
								<?php
							}
							?>
						</tbody>
					</table>
				</div>
			</div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->