<?php
	$sql = $this->db->get_where($table, array('id'=>$id));
	foreach ($sql->result_array() as $tmp) {
	}
?>
<form id="form-hargaemas-edit" method="post">
<div class="row">
	<div class="col-md-4">
		<div class="form-group">
			<label>Name</label>
			<input type="text" name="name" value="Emas" class="name form-control" disabled>
			<input type="hidden" name="id" value="<?php echo $id; ?>">
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			<label>Price</label>
			<input type="text" name="price" value="<?php echo number_format($tmp['harga_emas'],0,',','.'); ?>" class="form-control" onKeyUp="this.value=ThausandSeperator(this.value,2);">
		</div>
	</div>
	<div class="col-md-12">
		<button class="btn btn-default"><i class="fa fa-save"></i> SAVE</button>
	</div>
</div>
</form>

<script>
	jQuery(function($){
			$('#form-hargaemas-edit').trigger("reset");
			$("#form-hargaemas-edit").on('submit',function(e){
			e.preventDefault();
			var formatData = new FormData($(this)[0]);
				swal({
				 title: "",
				 text: "Apakah Anda akan menyimpan data?",
				 type: "warning",
				 showCancelButton: true,
				 confirmButtonColor: "#DD6B55",
				 confirmButtonText: "Ya",
				 cancelButtonText: "Tidak",
				 closeOnConfirm: false }, function(){
					$.ajax({
						type : 'POST',
						url : '<?php echo base_url().$controls; ?>/saveEditData',
						data : formatData,
						async: false,
						cache: false,
						contentType: false,
						processData: false,
						success:function(html){
							//window.location.reload();
							var data = eval ("(" + html + ")");
							if(data.success){
								swal({
								 title: "Berhasil Menyimpan data!",
								 text: "Klik tombol di bawah.",
								 type: "success",
								 showCancelButton: false,
								 confirmButtonColor: "#257DB6",
								 confirmButtonText: "Ok!",
								 closeOnConfirm: false
								},
								function(){
									location.href = base_url+"<?php echo $controls; ?>";
								});
							}else{
								swal({
								 title: data.msg,
								 text: "Klik tombol di bawah.",
								 type: "warning",
								 showCancelButton: false,
								 confirmButtonColor: "#257DB6",
								 confirmButtonText: "Ok!",
								 closeOnConfirm: false
								});
							}
						}
					});
				});
			});
	});
</script>