	<!--Load the AJAX API-->
    <script type="text/javascript" src="<?php echo base_url();?>asset/jscript/googlechart.js"></script>
	
	<?php
	$acces = sessionValue('access_level');
	if ($acces == "Front_Desk" || $acces == "Admin") {
        $sql_member_individu = $this->db->query("SELECT id_member FROM tb_member where kode='A' AND ISNULL(delete_by)");
        $sql_member_coorporate = $this->db->query("SELECT id_member FROM tb_member where kode='B' AND ISNULL(delete_by)");  

        $sql_fpjs_individu = $this->db->query("SELECT id_member FROM tb_front_desk where ISNULL(delete_by) GROUP BY id_order");
        $sql_fpjs_coorporate = $this->db->query("SELECT id_member FROM tb_front_desk where id_member='B' AND ISNULL(delete_by) GROUP BY id_order");

		$sql_fpjt_individu = $this->db->query("SELECT id_member FROM tb_fpjt where ISNULL(delete_by) GROUP BY id_order");
        $sql_fpjt_coorporate = $this->db->query("SELECT id_member FROM tb_fpjt where id_member='B' AND (ISNULL(delete_by) OR delete_by='') GROUP BY id_order"); 
	
	?>
    <script type="text/javascript">
	  var total_member_individu = "<?php echo $sql_member_individu->num_rows();?>";
	  var total_member_cooporate = "<?php echo $sql_member_coorporate->num_rows();?>";
	  
	  var total_fpjs_individu = "<?php echo $sql_fpjs_individu->num_rows();?>";
	  var total_fpjs_cooporate = "<?php echo $sql_fpjs_coorporate->num_rows();?>";
	  
	  var total_fpjt_individu = "<?php echo $sql_fpjt_individu->num_rows();?>";
	  var total_fpjt_cooporate = "<?php echo $sql_fpjt_coorporate->num_rows();?>";
	  
      // Load the Visualization API and the corechart package.
      google.charts.load('current', {'packages':['corechart']});

      // Set a callback to run when the Google Visualization API is loaded.
      google.charts.setOnLoadCallback(drawChart);

      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {

	    // Create the data table.
        var data = google.visualization.arrayToDataTable([
			['Element', 'Data CIF', { role: 'style' }, { role: 'annotation' } ],
			['Individual', parseInt(total_member_individu), 'stroke-color: #703593; stroke-width: 4; fill-color: #C5A5CF', total_member_individu],
			['Korporasi', parseInt(total_member_cooporate), 'stroke-color: #871B47; stroke-opacity: 0.6; stroke-width: 4; fill-color: #C5A5CF; fill-opacity: 0.2', total_member_cooporate]
		  ]);

        // Set chart options
        var options = {'title':' ',
                       'width':390,
                       'height':270};

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.ColumnChart(document.getElementById('sales-chart'));
        chart.draw(data, options);
		
        // Create the data table.
        var data = google.visualization.arrayToDataTable([
			['Element', 'Data FPJS', { role: 'style' }, { role: 'annotation' } ],
			['Individual', parseInt(total_fpjs_individu), 'stroke-color: #703593; stroke-width: 4; fill-color: #C5A5CF', total_fpjs_individu],
			['Korporasi', parseInt(total_fpjs_cooporate), 'stroke-color: #871B47; stroke-opacity: 0.6; stroke-width: 4; fill-color: #C5A5CF; fill-opacity: 0.2', total_fpjs_cooporate]
		  ]);

        // Set chart options
        var options = {'title':' ',
                       'width':390,
                       'height':270};

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.ColumnChart(document.getElementById('member'));
        chart.draw(data, options);
		
        // Create the data table.
        var data = google.visualization.arrayToDataTable([
			['Element', 'Data FPJT', { role: 'style' }, { role: 'annotation' } ],
			['Individual', parseInt(total_fpjt_individu), 'stroke-color: #703593; stroke-width: 4; fill-color: #C5A5CF', total_fpjt_individu],
			['Korporasi', parseInt(total_fpjt_cooporate), 'stroke-color: #871B47; stroke-opacity: 0.6; stroke-width: 4; fill-color: #C5A5CF; fill-opacity: 0.2', total_fpjt_cooporate]
		  ]);

        // Set chart options
        var options = {'title':' ',
                       'width':390,
                       'height':270};

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.ColumnChart(document.getElementById('fpjt'));
        chart.draw(data, options);
      }
    </script>
	<?php }else if ($acces == "Lab_Desk" || $acces == "Gemologist") { 
		$sql_permintaan1 = $this->db->query("SELECT id_member FROM step where step='FRONT_DESK' AND request='FPJS' AND certificate<>'' AND status='NOT APPROVED' AND ISNULL(delete_by)");
        $sql_permintaan2 = $this->db->query("SELECT id_member FROM step where step='FRONT_DESK' AND request='FPJS' AND dia_grading<>'' AND status='NOT APPROVED' AND ISNULL(delete_by)");  
        $sql_permintaan3 = $this->db->query("SELECT id_member FROM step where step='FRONT_DESK' AND request='FPJT' AND status='NOT APPROVED' AND ISNULL(delete_by)");  

        $sql_permintaan_selesai1 = $this->db->query("SELECT id_member FROM step where step='LAB_DESK' AND request='FPJS' AND certificate<>'' AND status='NOT APPROVED' AND ISNULL(delete_by)");
        $sql_permintaan_selesai2 = $this->db->query("SELECT id_member FROM step where step='LAB_DESK' AND request='FPJS' AND dia_grading<>'' AND status='NOT APPROVED' AND ISNULL(delete_by)");  
        $sql_permintaan_selesai3 = $this->db->query("SELECT id_member FROM step where step='LAB_DESK' AND request='FPJT' AND status='NOT APPROVED' AND ISNULL(delete_by)");  

		$total_permintaan_cetak1 = $this->db->query("SELECT id_member FROM step where step='LAB_DESK' AND request='FPJS' AND certificate<>'' AND status='APPROVED' AND ISNULL(delete_by)");
        $total_permintaan_cetak2 = $this->db->query("SELECT id_member FROM step where step='LAB_DESK' AND request='FPJS' AND dia_grading<>'' AND status='APPROVED' AND ISNULL(delete_by)");  
        $total_permintaan_cetak3 = $this->db->query("SELECT id_member FROM step where step='LAB_DESK' AND request='FPJT' AND status='APPROVED' AND ISNULL(delete_by)");  
	?>
	
    <script type="text/javascript">
	  var total_permintaan1 = "<?php echo $sql_permintaan1->num_rows();?>";
	  var total_permintaan2 = "<?php echo $sql_permintaan2->num_rows();?>";
	  var total_permintaan3 = "<?php echo $sql_permintaan3->num_rows();?>";
	  
	  var total_permintaan_selesai1 = "<?php echo $sql_permintaan_selesai1->num_rows();?>";
	  var total_permintaan_selesai2 = "<?php echo $sql_permintaan_selesai2->num_rows();?>";
	  var total_permintaan_selesai3 = "<?php echo $sql_permintaan_selesai3->num_rows();?>";
	  
	  var total_permintaan_cetak1 = "<?php echo $total_permintaan_cetak1->num_rows();?>";
	  var total_permintaan_cetak2 = "<?php echo $total_permintaan_cetak2->num_rows();?>";
	  var total_permintaan_cetak3 = "<?php echo $total_permintaan_cetak3->num_rows();?>";
	  
      // Load the Visualization API and the corechart package.
      google.charts.load('current', {'packages':['corechart']});

      // Set a callback to run when the Google Visualization API is loaded.
      google.charts.setOnLoadCallback(drawChart);

      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {

	    // Create the data table.
        var data = google.visualization.arrayToDataTable([
			['Element', 'Data CIF', { role: 'style' }, { role: 'annotation' } ],
			['Sertifikat', parseInt(total_permintaan1), 'stroke-color: #703593; stroke-width: 4; fill-color: #C5A5CF', total_permintaan1],
			['Diamond Grading', parseInt(total_permintaan2), 'stroke-color: #871B47; stroke-opacity: 0.6; stroke-width: 4; fill-color: #C5A5CF; fill-opacity: 0.2', total_permintaan2],
			['Taksiran', parseInt(total_permintaan3), 'stroke-color: #703593; stroke-width: 4; fill-color: #C5A5CF', total_permintaan3]
		  ]);

        // Set chart options
        var options = {'title':' ',
                       'width':390,
                       'height':270};

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.ColumnChart(document.getElementById('sales-chart'));
        chart.draw(data, options);
		
        // Create the data table.
        var data = google.visualization.arrayToDataTable([
			['Element', 'Data FPJS', { role: 'style' }, { role: 'annotation' } ],
			['Sertifikat', parseInt(total_permintaan_selesai1), 'stroke-color: #703593; stroke-width: 4; fill-color: #C5A5CF', total_permintaan_selesai1],
			['Diamond Grading', parseInt(total_permintaan_selesai2), 'stroke-color: #871B47; stroke-opacity: 0.6; stroke-width: 4; fill-color: #C5A5CF; fill-opacity: 0.2', total_permintaan_selesai2],
			['Taksiran', parseInt(total_permintaan_selesai3), 'stroke-color: #703593; stroke-width: 4; fill-color: #C5A5CF', total_permintaan_selesai3]
		  ]);

        // Set chart options
        var options = {'title':' ',
                       'width':390,
                       'height':270};

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.ColumnChart(document.getElementById('member'));
        chart.draw(data, options);
		
        // Create the data table.
        var data = google.visualization.arrayToDataTable([
			['Element', 'Data FPJT', { role: 'style' }, { role: 'annotation' } ],
			['Sertifikat', parseInt(total_permintaan_cetak1), 'stroke-color: #703593; stroke-width: 4; fill-color: #C5A5CF', total_permintaan_cetak1],
			['Diamond Grading', parseInt(total_permintaan_cetak2), 'stroke-color: #871B47; stroke-opacity: 0.6; stroke-width: 4; fill-color: #C5A5CF; fill-opacity: 0.2', total_permintaan_cetak2],
			['Taksiran', parseInt(total_permintaan_cetak3), 'stroke-color: #703593; stroke-width: 4; fill-color: #C5A5CF', total_permintaan_cetak3]
		  ]);

        // Set chart options
        var options = {'title':' ',
                       'width':390,
                       'height':270};

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.ColumnChart(document.getElementById('fpjt'));
        chart.draw(data, options);
      }
    </script>
	<?php } ?>

	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>&nbsp;</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <?php
            $acces = sessionValue('access_level');
            $kode  = sessionValue('kode_store');
            date_default_timezone_set('Asia/Jakarta');
            $date  = date('Y-m-d');
            if ($acces == "Front_Desk" || $acces == "Admin") {
          ?>
            <div class="row">
              <div class="col-md-4">
                <!-- DONUT CHART -->
                <div class="box box-danger">
                  <div class="box-header with-border">
                    <h3 class="box-title"><b>TOTAL DATA CIF</b></h3>
                    <div class="box-tools pull-right">
                      <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                  </div>
                  <div class="box-body chart-responsive">
                    <div class="chart" id="sales-chart" style="height: 270px; position: relative;"></div>
                  </div><!-- /.box-body -->
                </div><!-- /.box -->
              </div>
              <div class="col-md-4">
                <!-- DONUT CHART -->
                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title"><b>TOTAL DATA FPJS</b></h3>
                    <div class="box-tools pull-right">
                      <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                  </div>
                  <div class="box-body chart-responsive">
                    <div class="chart" id="member" style="height: 270px; position: relative;"></div>
                  </div><!-- /.box-body -->
                </div><!-- /.box -->
              </div>
			  <div class="col-md-4">
                <!-- DONUT CHART -->
                <div class="box box-success">
                  <div class="box-header with-border">
                    <h3 class="box-title"><b>TOTAL DATA FPJT</b></h3>
                    <div class="box-tools pull-right">
                      <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                  </div>
                  <div class="box-body chart-responsive">
                    <div class="chart" id="fpjt" style="height: 270px; position: relative;"></div>
                  </div><!-- /.box-body -->
                </div><!-- /.box -->
              </div>
            </div>
			
			<div class="row">
              <div class="col-md-12">
                <!-- DONUT CHART -->
                <div class="box box-danger">
                  <div class="box-header with-border">
                    <h3 class="box-title"><b>DATA CIF - INDIVIDUAL</b></h3>
                    <div class="box-tools pull-right">
                      <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                  </div>
                  <div class="box-body">
					  <div class="table-responsive">
						<table class="table table-bordered table-striped" id="example1">
						  <thead>
							<tr>
							  <th class="text-center" width="5%">No</th>
							  <th class="text-center" width="10%">No. CIF</th>
							  <th class="text-center" >Nama</th>
							  <th class="text-center" width="18%">Email</th>
							  <th class="text-center" width="18%">No. HP</th>
							  <th class="text-center" width="15%">Tanggal</th>
							</tr>
						  </thead>
						  <tbody>
							<?php
							  $no = 1;
							  $q = $this->db->query("SELECT id_member, first_name, middle_name, last_name, phone, email, create_date FROM tb_member a
												LEFT JOIN master_provinsi b ON b.provinsi_id = a.province
												LEFT JOIN master_kokab c ON c.kota_id = a.city
												LEFT JOIN master_kecam d ON d.kecam_id = a. district 
												WHERE a.kode='A' AND ISNULL(delete_by) ORDER BY create_date DESC LIMIT 0,5");
							  foreach ($q->result_array() as $i) {
							?>
							  <tr>
								<td style="text-align:center"><?php echo $no++?></td>
								<td><?php echo $i['id_member']?></td>
								<td><?php echo $i['first_name']." ".$i['middle_name']." ".$i['last_name']?></td>
								<td><?php echo $i['email']?></td>
								<td style="text-align:left">+62 <?php echo $i['phone']?></td>
								<td class="text-center"><?php echo $i['create_date']?></td>
							  </tr>
							<?php
							  }
							?>
						  </tbody>
						</table>
					  </div><!-- /.box-body -->
				  </div><!-- /.box -->
				</div>
			  </div>
			  <div class="col-md-12">
                <!-- DONUT CHART -->
                <div class="box box-danger">
                  <div class="box-header with-border">
                    <h3 class="box-title"><b>DATA CIF - KORPORASI</b></h3>
                    <div class="box-tools pull-right">
                      <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                  </div>
                  <div class="box-body">
					  <div class="table-responsive">
						<table class="table table-bordered table-striped" id="example2">
						  <thead>
							<tr>
							  <th class="text-center" width="5%">No</th>
							  <th class="text-center" width="10%">No. Korp.</th>
							  <th class="text-center" >Nama Perusahaan</th>
							  <th class="text-center" width="18%">Email</th>
							  <th class="text-center" width="18%">No. HP</th>
							  <th class="text-center" width="15%">Tanggal</th>
							</tr>
						  </thead>
						  <tbody>
							<?php
							  $no = 1;
							  $q = $this->db->query("SELECT id_member, corp_name, phone, email, create_date FROM tb_member a
												LEFT JOIN master_provinsi b ON b.provinsi_id = a.province
												LEFT JOIN master_kokab c ON c.kota_id = a.city
												LEFT JOIN master_kecam d ON d.kecam_id = a. district 
												WHERE a.kode='B' AND ISNULL(delete_by) ORDER BY create_date DESC LIMIT 0,5");
							  foreach ($q->result_array() as $i) {
							?>
							  <tr>
								<td style="text-align:center"><?php echo $no++?></td>
								<td><?php echo $i['id_member']?></td>
								<td><?php echo $i['corp_name']?></td>
								<td><?php echo $i['email']?></td>
								<td style="text-align:left">+62 <?php echo $i['phone']?></td>
								<td class="text-center"><?php echo $i['create_date']?></td>
							  </tr>
							<?php
							  }
							?>
						  </tbody>
						</table>
					  </div><!-- /.box-body -->
				  </div><!-- /.box -->
				</div>
			  </div>
              <div class="col-md-12">
                <!-- DONUT CHART -->
                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title"><b>DATA FPJS - INDIVIDUAL</b></h3>
                    <div class="box-tools pull-right">
                      <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                  </div>
                  <div class="box-body">
					  <div class="table-responsive">
						<table class="table table-bordered table-striped" id="example2">
						  <thead>
							<tr>
							  <th class="text-center" width="5%">No</th>
							  <th class="text-center" width="50%">No. FPJS</th>
							  <th class="text-center" >No. CIF</th>
							  <th class="text-center"  width="10%">Total Speciment</th>
							  <th class="text-center" width="15%">Tanggal</th>
							</tr>
						  </thead>
						  <tbody>
							<?php
							  $no = 1;
							  $q = $this->db->query("SELECT id_order, id_member, COUNT(id_order) AS jumlah, input_date FROM tb_front_desk WHERE ISNULL(delete_by) GROUP BY id_order ORDER BY input_date DESC LIMIT 0,5");
							  foreach ($q->result_array() as $i) {
							?>
							  <tr>
								<td style="text-align:center"><?php echo $no++?></td>
								<td><?php echo $i['id_order']?></td>
								<td><?php echo $i['id_member']?></td>
								<td class="text-center"><?php echo $i['jumlah']?></td>
								<td class="text-center"><?php echo $i['input_date']?></td>
							  </tr>
							<?php
							  }
							?>
						  </tbody>
						</table>
					  </div><!-- /.box-body -->
				  </div><!-- /.box -->
                </div><!-- /.box -->
              </div>
              <div class="col-md-12">
                <!-- DONUT CHART -->
                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title"><b>DATA FPJS - KORPORASI</b></h3>
                    <div class="box-tools pull-right">
                      <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                  </div>
                  <div class="box-body">
					  <div class="table-responsive">
						<table class="table table-bordered table-striped" id="example2">
						  <thead>
							<tr>
							  <th class="text-center" width="5%">No</th>
							  <th class="text-center" width="50%">No. FPJS</th>
							  <th class="text-center" >No. CIF</th>
							  <th class="text-center"  width="10%">Total Speciment</th>
							  <th class="text-center" width="15%">Tanggal</th>
							</tr>
						  </thead>
						  <tbody>
						  </tbody>
						</table>
					  </div><!-- /.box-body -->
				  </div><!-- /.box -->
                </div><!-- /.box -->
              </div>
              <div class="col-md-12">
                <!-- DONUT CHART -->
                <div class="box box-success">
                  <div class="box-header with-border">
                    <h3 class="box-title"><b>DATA FPJT - INDIVIDUAL</b></h3>
                    <div class="box-tools pull-right">
                      <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                  </div>
                  <div class="box-body">
					  <div class="table-responsive">
						<table class="table table-bordered table-striped" id="example2">
						  <thead>
							<tr>
							  <th class="text-center" width="5%">No</th>
							  <th class="text-center" width="30%">No. FPJT</th>
							  <th class="text-center" >No. CIF</th>
							  <th class="text-center" width="30%">Jewelry</th>
							  <th class="text-center" width="10%">Total</th>
							  <th class="text-center" width="15%">Tanggal</th>
							</tr>
						  </thead>
						  <tbody>
							<?php
							  $no = 1;
							  $q = $this->db->query("SELECT * FROM tb_fpjt, master_jenis_permata
                                WHERE
                                tb_fpjt.jenis_perhiasan = master_jenis_permata.id  GROUP BY id_order ORDER BY create_date DESC LIMIT 0,5");
							  foreach ($q->result_array() as $i) {
							?>
							  <tr>
								<td style="text-align:center"><?php echo $no++?></td>
								<td><?php echo $i['id_order']?></td>
								<td><?php echo $i['id_member']?></td>
								<td><?php echo $i['jenis']?></td>
								<td class="text-center"><?php echo $i['jumlah_permata']?></td>
								<td class="text-center"><?php echo $i['create_date']?></td>
							  </tr>
							<?php
							  }
							?>
						  </tbody>
						</table>
					  </div><!-- /.box-body -->
				  </div><!-- /.box -->
                </div><!-- /.box -->
              </div>
              <div class="col-md-12">
                <!-- DONUT CHART -->
                <div class="box box-success">
                  <div class="box-header with-border">
                    <h3 class="box-title"><b>DATA FPJT - KORPORASI</b></h3>
                    <div class="box-tools pull-right">
                      <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                  </div>
                  <div class="box-body">
					  <div class="table-responsive">
						<table class="table table-bordered table-striped" id="example2">
						  <thead>
							<tr>
							  <th class="text-center" width="5%">No</th>
							  <th class="text-center" width="30%">No. FPJT</th>
							  <th class="text-center" >No. CIF</th>
							  <th class="text-center" width="30%">Jewelry</th>
							  <th class="text-center" width="10%">Total</th>
							  <th class="text-center" width="15%">Tanggal</th>
							</tr>
						  </thead>
						  <tbody>
						  </tbody>
						</table>
					  </div><!-- /.box-body -->
				  </div><!-- /.box -->
                </div><!-- /.box -->
              </div>
            </div>
          <?php
            }else if($acces == "Lab_Desk" || $acces == "Gemologist"){            
          ?>
            <div class="row">
              <div class="col-md-4">
                <!-- DONUT CHART -->
                <div class="box box-danger">
                  <div class="box-header with-border">
                    <h3 class="box-title"><b>TOTAL PERMINTAAN</b></h3>
                    <div class="box-tools pull-right">
                      <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                  </div>
                  <div class="box-body chart-responsive">
                    <div class="chart" id="sales-chart" style="height: 270px; position: relative;"></div>
                  </div><!-- /.box-body -->
                </div><!-- /.box -->
              </div>
              <div class="col-md-4">
                <!-- DONUT CHART -->
                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title"><b>TOTAL PERMINTAAN SELESAI</b></h3>
                    <div class="box-tools pull-right">
                      <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                  </div>
                  <div class="box-body chart-responsive">
                    <div class="chart" id="member" style="height: 270px; position: relative;"></div>
                  </div><!-- /.box-body -->
                </div><!-- /.box -->
              </div>
			  <div class="col-md-4">
                <!-- DONUT CHART -->
                <div class="box box-success">
                  <div class="box-header with-border">
                    <h3 class="box-title"><b>TOTAL CETAK PERMINTAAN</b></h3>
                    <div class="box-tools pull-right">
                      <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                  </div>
                  <div class="box-body chart-responsive">
                    <div class="chart" id="fpjt" style="height: 270px; position: relative;"></div>
                  </div><!-- /.box-body -->
                </div><!-- /.box -->
              </div>
            </div>
			<div class="row">
              <div class="col-md-12">
                <!-- DONUT CHART -->
                <div class="box box-danger">
                  <div class="box-header with-border">
                    <h3 class="box-title"><b>DATA PERMINTAAN - SERTIFIKASI</b></h3>
                    <div class="box-tools pull-right">
                      <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                  </div>
                  <div class="box-body">
					  <div class="table-responsive">
						<table class="table table-bordered table-striped" id="example1">
						  <thead>
							<tr>
							  <th class="text-center" width="5%">No</th>
							  <th class="text-center" width="20%">No. FPJS</th>
							  <th class="text-center" >L (mm)</th>
							  <th class="text-center" width="18%">W (mm)</th>
							  <th class="text-center" width="18%">H (mm)</th>
							  <th class="text-center" width="15%">Tanggal</th>
							</tr>
						  </thead>
						  <tbody>
							<?php
							  $no = 1;
							  $q = $this->db->query("SELECT tb_front_desk.*
													FROM tb_front_desk
													LEFT JOIN step ON step.id_object = tb_front_desk.id_object
													WHERE step.step='FRONT_DESK' AND step.status='NOT APPROVED' 
													AND step.certificate<>''
													AND ISNULL(step.delete_by) ORDER BY step.create_date DESC LIMIT 0,5");
							  foreach ($q->result_array() as $i) {
							?>
							  <tr>
								<td style="text-align:center"><?php echo $no++?></td>
								<td><?php echo $i['id_order']?></td>
								<td class="text-center"><?php echo $i['obj_length']?></td>
								<td class="text-center"><?php echo $i['obj_width']?></td>
								<td class="text-center"><?php echo $i['obj_height']?></td>
								<td class="text-center"><?php echo $i['create_date']?></td>
							  </tr>
							<?php
							  }
							?>
						  </tbody>
						</table>
					  </div><!-- /.box-body -->
				  </div><!-- /.box -->
				</div>
			  </div>
              <div class="col-md-12">
                <!-- DONUT CHART -->
                <div class="box box-danger">
                  <div class="box-header with-border">
                    <h3 class="box-title"><b>DATA PERMINTAAN - DIAMOND GRADING</b></h3>
                    <div class="box-tools pull-right">
                      <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                  </div>
                  <div class="box-body">
					  <div class="table-responsive">
						<table class="table table-bordered table-striped" id="example1">
						  <thead>
							<tr>
							  <th class="text-center" width="5%">No</th>
							  <th class="text-center" width="20%">No. FPJS</th>
							  <th class="text-center" >L (mm)</th>
							  <th class="text-center" width="18%">W (mm)</th>
							  <th class="text-center" width="18%">H (mm)</th>
							  <th class="text-center" width="15%">Tanggal</th>
							</tr>
						  </thead>
						  <tbody>
							<?php
							  $no = 1;
							  $q = $this->db->query("SELECT tb_front_desk.*
													FROM tb_front_desk
													LEFT JOIN step ON step.id_object = tb_front_desk.id_object
													WHERE step.step='FRONT_DESK' AND step.status='NOT APPROVED' 
													AND step.dia_grading<>''
													AND ISNULL(step.delete_by) ORDER BY step.create_date DESC LIMIT 0,5");
							  foreach ($q->result_array() as $i) {
							?>
							  <tr>
								<td style="text-align:center"><?php echo $no++?></td>
								<td><?php echo $i['id_order']?></td>
								<td class="text-center"><?php echo $i['obj_length']?></td>
								<td class="text-center"><?php echo $i['obj_width']?></td>
								<td class="text-center"><?php echo $i['obj_height']?></td>
								<td class="text-center"><?php echo $i['create_date']?></td>
							  </tr>
							<?php
							  }
							?>
						  </tbody>
						</table>
					  </div><!-- /.box-body -->
				  </div><!-- /.box -->
				</div>
			  </div>
              <div class="col-md-12">
                <!-- DONUT CHART -->
                <div class="box box-danger">
                  <div class="box-header with-border">
                    <h3 class="box-title"><b>DATA PERMINTAAN - TAKSIRAN</b></h3>
                    <div class="box-tools pull-right">
                      <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                  </div>
                  <div class="box-body">
					  <div class="table-responsive">
						<table class="table table-bordered table-striped" id="example1">
						  <thead>
							<tr>
							  <th class="text-center" width="5%">No</th>
							  <th class="text-center" width="20%">No. FPJS</th>
							  <th class="text-center" >Perhiasan</th>
							  <th class="text-center" width="18%">Permata</th>
							  <th class="text-center" width="15%">Tanggal</th>
							</tr>
						  </thead>
						  <tbody>
							<?php
							  $no = 1;
							  $q = $this->db->query("SELECT tb_fpjt.*, master_jenis_permata.jenis, master_permata.permata
													FROM tb_fpjt
													LEFT JOIN step ON step.id_object = tb_fpjt.id
													LEFT JOIN master_jenis_permata ON master_jenis_permata.id = tb_fpjt.jenis_perhiasan
													LEFT JOIN master_permata ON master_permata.id = tb_fpjt.jenis_permata
													WHERE step.step='FRONT_DESK' AND step.status='NOT APPROVED' 
													AND step.request='FPJT'
													AND ISNULL(step.delete_by) ORDER BY step.create_date DESC LIMIT 0,5");
							  foreach ($q->result_array() as $i) {
							?>
							  <tr>
								<td style="text-align:center"><?php echo $no++?></td>
								<td><?php echo $i['id_order']?></td>
								<td><?php echo $i['jenis']?></td>
								<td class="text-center"><?php echo $i['permata']?></td>
								<td class="text-center"><?php echo $i['create_date']?></td>
							  </tr>
							<?php
							  }
							?>
						  </tbody>
						</table>
					  </div><!-- /.box-body -->
				  </div><!-- /.box -->
				</div>
			  </div>
			  
              <div class="col-md-12">
                <!-- DONUT CHART -->
                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title"><b>DATA PERMINTAAN SELESAI - SERTIFIKASI</b></h3>
                    <div class="box-tools pull-right">
                      <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                  </div>
                  <div class="box-body">
					  <div class="table-responsive">
						<table class="table table-bordered table-striped" id="example2">
						  <thead>
							<tr>
							  <th class="text-center" width="5%">No</th>
							  <th class="text-center" width="20%">No. FPJS</th>
							  <th class="text-center" >L (mm)</th>
							  <th class="text-center" width="18%">W (mm)</th>
							  <th class="text-center" width="18%">H (mm)</th>
							  <th class="text-center" width="15%">Tanggal</th>
							</tr>
						  </thead>
						  <tbody>
							<?php
							  $no = 1;
							  $q = $this->db->query("SELECT tb_front_desk.*
													FROM tb_front_desk
													LEFT JOIN step ON step.id_object = tb_front_desk.id_object
													WHERE step.step='LAB_DESK' AND step.status='NOT APPROVED' 
													AND step.certificate<>''
													AND ISNULL(step.delete_by) ORDER BY step.create_date DESC LIMIT 0,5");
							  foreach ($q->result_array() as $i) {
							?>
							  <tr>
								<td style="text-align:center"><?php echo $no++?></td>
								<td><?php echo $i['id_order']?></td>
								<td class="text-center"><?php echo $i['obj_length']?></td>
								<td class="text-center"><?php echo $i['obj_width']?></td>
								<td class="text-center"><?php echo $i['obj_height']?></td>
								<td class="text-center"><?php echo $i['create_date']?></td>
							  </tr>
							<?php
							  }
							?>
						  </tbody>
						</table>
					  </div><!-- /.box-body -->
				  </div><!-- /.box -->
                </div><!-- /.box -->
              </div>
              <div class="col-md-12">
                <!-- DONUT CHART -->
                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title"><b>DATA PERMINTAAN SELESAI - DIAMOND GRADING</b></h3>
                    <div class="box-tools pull-right">
                      <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                  </div>
                  <div class="box-body">
					  <div class="table-responsive">
						<table class="table table-bordered table-striped" id="example1">
						  <thead>
							<tr>
							  <th class="text-center" width="5%">No</th>
							  <th class="text-center" width="20%">No. FPJS</th>
							  <th class="text-center" >L (mm)</th>
							  <th class="text-center" width="18%">W (mm)</th>
							  <th class="text-center" width="18%">H (mm)</th>
							  <th class="text-center" width="15%">Tanggal</th>
							</tr>
						  </thead>
						  <tbody>
							<?php
							  $no = 1;
							  $q = $this->db->query("SELECT tb_front_desk.*
													FROM tb_front_desk
													LEFT JOIN step ON step.id_object = tb_front_desk.id_object
													WHERE step.step='LAB_DESK' AND step.status='NOT APPROVED' 
													AND step.dia_grading<>''
													AND ISNULL(step.delete_by) ORDER BY step.create_date DESC LIMIT 0,5");
							  foreach ($q->result_array() as $i) {
							?>
							  <tr>
								<td style="text-align:center"><?php echo $no++?></td>
								<td><?php echo $i['id_order']?></td>
								<td class="text-center"><?php echo $i['obj_length']?></td>
								<td class="text-center"><?php echo $i['obj_width']?></td>
								<td class="text-center"><?php echo $i['obj_height']?></td>
								<td class="text-center"><?php echo $i['create_date']?></td>
							  </tr>
							<?php
							  }
							?>
						  </tbody>
						</table>
					  </div><!-- /.box-body -->
				  </div><!-- /.box -->
				</div>
			  </div>
              <div class="col-md-12">
                <!-- DONUT CHART -->
                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title"><b>DATA PERMINTAAN SELESAI - TAKSIRAN</b></h3>
                    <div class="box-tools pull-right">
                      <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                  </div>
                  <div class="box-body">
					  <div class="table-responsive">
						<table class="table table-bordered table-striped" id="example1">
						  <thead>
							<tr>
							  <th class="text-center" width="5%">No</th>
							  <th class="text-center" width="20%">No. FPJS</th>
							  <th class="text-center" >Perhiasan</th>
							  <th class="text-center" width="18%">Permata</th>
							  <th class="text-center" width="15%">Tanggal</th>
							</tr>
						  </thead>
						  <tbody>
							<?php
							  $no = 1;
							  $q = $this->db->query("SELECT tb_fpjt.*, master_jenis_permata.jenis, master_permata.permata
													FROM tb_fpjt
													LEFT JOIN step ON step.id_object = tb_fpjt.id
													LEFT JOIN master_jenis_permata ON master_jenis_permata.id = tb_fpjt.jenis_perhiasan
													LEFT JOIN master_permata ON master_permata.id = tb_fpjt.jenis_permata
													WHERE step.step='LAB_DESK' AND step.status='NOT APPROVED' 
													AND step.request='FPJT'
													AND ISNULL(step.delete_by) ORDER BY step.create_date DESC LIMIT 0,5");
							  foreach ($q->result_array() as $i) {
							?>
							  <tr>
								<td style="text-align:center"><?php echo $no++?></td>
								<td><?php echo $i['id_order']?></td>
								<td><?php echo $i['jenis']?></td>
								<td class="text-center"><?php echo $i['permata']?></td>
								<td class="text-center"><?php echo $i['create_date']?></td>
							  </tr>
							<?php
							  }
							?>
						  </tbody>
						</table>
					  </div><!-- /.box-body -->
				  </div><!-- /.box -->
				</div>
			  </div>
			  
              <div class="col-md-12">
                <!-- DONUT CHART -->
                <div class="box box-success">
                  <div class="box-header with-border">
                    <h3 class="box-title"><b>DATA CETAK PERMINTAAN - SERTIFIKASI</b></h3>
                    <div class="box-tools pull-right">
                      <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                  </div>
                  <div class="box-body">
					  <div class="table-responsive">
						<table class="table table-bordered table-striped" id="example2">
						  <thead>
							<tr>
							  <th class="text-center" width="5%">No</th>
							  <th class="text-center" width="20%">No. FPJS</th>
							  <th class="text-center" >L (mm)</th>
							  <th class="text-center" width="18%">W (mm)</th>
							  <th class="text-center" width="18%">H (mm)</th>
							  <th class="text-center" width="15%">Tanggal</th>
							</tr>
						  </thead>
						  <tbody>
							<?php
							  $no = 1;
							  $q = $this->db->query("SELECT tb_front_desk.*
													FROM tb_front_desk
													LEFT JOIN step ON step.id_object = tb_front_desk.id_object
													WHERE step.step='LAB_DESK' AND step.status='APPROVED' 
													AND step.certificate<>''
													AND ISNULL(step.delete_by) ORDER BY step.create_date DESC LIMIT 0,5");
							  foreach ($q->result_array() as $i) {
							?>
							  <tr>
								<td style="text-align:center"><?php echo $no++?></td>
								<td><?php echo $i['id_order']?></td>
								<td class="text-center"><?php echo $i['obj_length']?></td>
								<td class="text-center"><?php echo $i['obj_width']?></td>
								<td class="text-center"><?php echo $i['obj_height']?></td>
								<td class="text-center"><?php echo $i['create_date']?></td>
							  </tr>
							<?php
							  }
							?>
						  </tbody>
						</table>
					  </div><!-- /.box-body -->
				  </div><!-- /.box -->
                </div><!-- /.box -->
              </div>
              <div class="col-md-12">
                <!-- DONUT CHART -->
                <div class="box box-success">
                  <div class="box-header with-border">
                    <h3 class="box-title"><b>DATA CETAK PERMINTAAN - DIAMOND GRADING</b></h3>
                    <div class="box-tools pull-right">
                      <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                  </div>
                  <div class="box-body">
					  <div class="table-responsive">
						<table class="table table-bordered table-striped" id="example1">
						  <thead>
							<tr>
							  <th class="text-center" width="5%">No</th>
							  <th class="text-center" width="20%">No. FPJS</th>
							  <th class="text-center" >L (mm)</th>
							  <th class="text-center" width="18%">W (mm)</th>
							  <th class="text-center" width="18%">H (mm)</th>
							  <th class="text-center" width="15%">Tanggal</th>
							</tr>
						  </thead>
						  <tbody>
							<?php
							  $no = 1;
							  $q = $this->db->query("SELECT tb_front_desk.*
													FROM tb_front_desk
													LEFT JOIN step ON step.id_object = tb_front_desk.id_object
													WHERE step.step='LAB_DESK' AND step.status='APPROVED' 
													AND step.dia_grading<>''
													AND ISNULL(step.delete_by) ORDER BY step.create_date DESC LIMIT 0,5");
							  foreach ($q->result_array() as $i) {
							?>
							  <tr>
								<td style="text-align:center"><?php echo $no++?></td>
								<td><?php echo $i['id_order']?></td>
								<td class="text-center"><?php echo $i['obj_length']?></td>
								<td class="text-center"><?php echo $i['obj_width']?></td>
								<td class="text-center"><?php echo $i['obj_height']?></td>
								<td class="text-center"><?php echo $i['create_date']?></td>
							  </tr>
							<?php
							  }
							?>
						  </tbody>
						</table>
					  </div><!-- /.box-body -->
				  </div><!-- /.box -->
				</div>
			  </div>
              <div class="col-md-12">
                <!-- DONUT CHART -->
                <div class="box box-success">
                  <div class="box-header with-border">
                    <h3 class="box-title"><b>DATA CETAK PERMINTAAN - TAKSIRAN</b></h3>
                    <div class="box-tools pull-right">
                      <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                  </div>
                  <div class="box-body">
					  <div class="table-responsive">
						<table class="table table-bordered table-striped" id="example1">
						  <thead>
							<tr>
							  <th class="text-center" width="5%">No</th>
							  <th class="text-center" width="20%">No. FPJS</th>
							  <th class="text-center" >Perhiasan</th>
							  <th class="text-center" width="18%">Permata</th>
							  <th class="text-center" width="15%">Tanggal</th>
							</tr>
						  </thead>
						  <tbody>
							<?php
							  $no = 1;
							  $q = $this->db->query("SELECT tb_fpjt.*, master_jenis_permata.jenis, master_permata.permata
													FROM tb_fpjt
													LEFT JOIN step ON step.id_object = tb_fpjt.id
													LEFT JOIN master_jenis_permata ON master_jenis_permata.id = tb_fpjt.jenis_perhiasan
													LEFT JOIN master_permata ON master_permata.id = tb_fpjt.jenis_permata
													WHERE step.step='LAB_DESK' AND step.status='APPROVED' 
													AND step.request='FPJT'
													AND ISNULL(step.delete_by) ORDER BY step.create_date DESC LIMIT 0,5");
							  foreach ($q->result_array() as $i) {
							?>
							  <tr>
								<td style="text-align:center"><?php echo $no++?></td>
								<td><?php echo $i['id_order']?></td>
								<td><?php echo $i['jenis']?></td>
								<td class="text-center"><?php echo $i['permata']?></td>
								<td class="text-center"><?php echo $i['create_date']?></td>
							  </tr>
							<?php
							  }
							?>
						  </tbody>
						</table>
					  </div><!-- /.box-body -->
				  </div><!-- /.box -->
				</div>
			  </div>
            </div>
          <?php
            }else if($acces == "Print_Desk"){
            $xd = $this->db->query("SELECT * from tb_front_desk a, step b where a.store='$kode' and a.create_date='$date' and a.delete_by='' and a.id_object=b.id_object and b.step='PRINT_DESK'");
            $xf = $this->db->query("SELECT * from step where store='$kode' and step='PRINT_DESK' and print='Sudah' and delete_by='' and create_date='$date'");
            $xh = $this->db->query("SELECT * FROM tb_front_desk where create_date='$date' and store='$kode' and certificate!=''");
            $xg = $this->db->query("SELECT * FROM tb_front_desk where create_date='$date' and store='$kode' and gem_card!=''");
            $xi = $this->db->query("SELECT * FROM tb_front_desk where create_date='$date' and store='$kode' and dia_grading!=''");
            echo "<script>
                $(function () {
                  'use strict';

                  //DONUT CHART
                  var donut = new Morris.Donut({
                    element: 'sales-chartx',
                    resize: true,
                    colors: ['#3c8dbc', '#f56954'],
                    data: [
                      {label: 'Object Finish Print', value: ".$xf->num_rows()."},
                      {label: 'Object Finish Examination', value: ".$xd->num_rows()."}
                    ],
                    hideHover: 'auto'
                  });

                  var donut = new Morris.Donut({
                    element: 'memberx',
                    resize: true,
                    colors: ['#ff88ff', '#00a65a', '#f56954'],
                    data: [
                      {label: 'CERTIFICATE', value: ".$xh->num_rows()."},
                      {label: 'GEM CARD', value: ".$xg->num_rows()."},
                      {label: 'DIAMOND GRADING', value: ".$xi->num_rows()."}
                    ],
                    hideHover: 'auto'
                  });
                });
              </script>";
          ?>
            <div class="row">
              <div class="col-md-6">
                <!-- DONUT CHART -->
                <div class="box box-danger">
                  <div class="box-header with-border">
                    <h3 class="box-title"><b>OBJECT</b></h3>
                    <div class="box-tools pull-right">
                      <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                      <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                  </div>
                  <div class="box-body chart-responsive">
                    <div class="chart" id="sales-chartx" style="height: 300px; position: relative;"></div>
                  </div><!-- /.box-body -->
                </div><!-- /.box -->
              </div>
              <div class="col-md-6">
                <!-- DONUT CHART -->
                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title"><b>REQUEST</b></h3>
                    <div class="box-tools pull-right">
                      <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                      <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                  </div>
                  <div class="box-body chart-responsive">
                    <div class="chart" id="memberx" style="height: 300px; position: relative;"></div>
                  </div><!-- /.box-body -->
                </div><!-- /.box -->
              </div>
            </div>
          <?php
          }
          ?>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->