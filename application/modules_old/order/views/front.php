<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>&nbsp;</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">List Order</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
        	<div class="box box-default">
        		<div class="box-header">
                <b>LIST ORDER</b>
                <div style="border:1px solid black;margin-bottom:0px;"></div>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div>
              </div>
        		<div class="box-body">
        			<div class="table-responsive">
        				<table class="table table-bordered table-striped table-hover" id="example1">
        					<thead>
        						<tr>
        							<th>No</th>
        							<th>ID Order</th>
        							<th>ID Member</th>
        							<th>Total Speciment</th>
        							<th>Date</th>
        							<th>Amount</th>
        						</tr>
        					</thead>
        					<tbody>
        						<?php
        						$no = 1;
        						$get_order = $this->db->query("SELECT * FROM tb_front_desk, step WHERE
        							tb_front_desk.id_order = step.id_order AND
        							tb_front_desk.id_object = step.id_object AND
        							step.store='".sessionValue('kode_store')."'
        							GROUP BY step.id_order
        							");
        						foreach ($get_order->result() as $get) {
        							?>
        						<tr>
        							<td><?php echo $no++ ?></td>
        							<td><?php echo $get->id_order ?></td>
        							<td><?php echo $get->id_object ?></td>
        							<td>
        								<?php
        								$get_count = $this->db->query("SELECT COUNT(*) as total_speciment FROM step WHERE id_order='".$get->id_order."'");
        								foreach ($get_count->result() as $cou) {
        									echo $cou->total_speciment;
        								}
        								?>
        							</td>
        							<td>
        								<?php
        								$date = date_create($get->input_date);
        								$date = date_format('d-m-Y');
        								echo $date;
        								?>
        							</td>
        							<td class="text-right">
        								<?php
        								$price = $this->db->query("SELECT SUM(price) as total_price FROM tb_front_desk WHERE id_order='".$get->id_order."'");
        								foreach ($price->result() as $pri) {
        									echo number_format($pri->total_price, 0, ',', '.');
        								}
        								?>
        							</td>
        						</tr>
        							<?php
        						}
        						?>
        					</tbody>
        				</table>
        			</div>
        		</div>
        	</div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->