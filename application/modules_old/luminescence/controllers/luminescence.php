<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
* Controller Master Luminescence
* Create By : Erwin Idamsyach Putra
* 23 April 2016
*/
class luminescence extends CI_Controller{
	function __construct(){
		parent::__construct();

		if(!isLogin()){
			goLogin();
		}
	}
	
	public function index(){
		$data['filelist'] = 'luminescence/front';
		$data['title'] = 'Master Lab Desk';
		$data['title_menu'] = 'Admin';
		$data['menu'] = 'luminescence';

		getHTMLWeb($data);
	}

	public function add_lumines(){
		$name = $this->input->post('lumines');

		$data = array(
			'luminescence' => $name,
			'create_by' => sessionValue('nama'),
			'create_date' => date('Y-m-d')	
			);
		$this->db->insert('luminescence', $data);
		redirect(site_url('luminescence'));
	}

	public function edit_lumines(){
		$id  = $this->uri->segment(3);

		$data['filelist'] = 'luminescence/front';
		$data['title'] = 'Master Lab Desk';
		$data['title_menu'] = 'Admin';
		$data['menu'] = 'luminescence';
		$data['id_lumines'] = $id;

		getHTMLWeb($data);
	}

	public function update_lumines(){
		$id   = $this->input->post('id');
		$name = $this->input->post('lumines');

		$data = array(
			'luminescence' => $name,
			'update_by' => sessionValue('nama'),
			'update_date' => date('Y-m-d')
			);

		$this->db->where('id', $id);
		$this->db->update('luminescence', $data);
		redirect(site_url('luminescence'));
	}

	public function delete_lumines(){
		$id = $this->uri->segment(3);

		$data = array(
			'delete_by' => sessionValue('nama'),
			'delete_date' => date('Y-m-d')
			);
		$this->db->where('id', $id);
		$this->db->update('luminescence', $data);
		redirect(site_url('luminescence'));
	}
}
?>