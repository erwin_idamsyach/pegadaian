<script>
    $(function(){
        $("#example1").DataTable();
        $(".datepicker").datepicker();
    });
</script>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>&nbsp;</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-user"></i> Home</a></li>
            <li>Invoice</li>
            <li class="active">List Invoice BPT</li>
          </ol>
        </section>
        <section class="content">
        	<div class="box box-default">
        		<div class="box-header">
        			<b>LIST Invoice BPT</b>
        			<div style="border: 1px solid black; margin-bottom: 10px"></div>
        		</div>
                <form action="" method="get">
                    <div class="container-fluid" style="margin-top: 10px;">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="from-group">
									<table width="100%" border="0">
									<tr>
										<td>
											<label><b>Cari Tanggal : </b></label>
										</td>
										<td>
											<input type="text" name="from" class="form-control datepicker">
										</td>
										<td>
											&nbsp;-&nbsp;
										</td>
										<td>
											<input type="text" name="to" class="form-control datepicker">
										</td>
										<td>
											&nbsp;<button type="submit" class="btn btn-primary" style="margin-top: 0px;"><i class="fa fa-search"></i></button>
										</td>
									</tr>
									</table>
                                </div>  
                            </div>
                        </div>
                    </div>
                </form>
        		<div class="box-body">
        			<table class="table table-striped table-bordered table-hover" id="example1">
        				<thead>
        					<tr>
        						<th class="text-center" width="5%">No</th>
                                <th class="text-center" width="15%">Tanggal</th>
        						<th class="text-center">ID FPJT</th>
        						<th class="text-center" width="18%">ID Member</th>
                                <th class="text-center" width="18%">Jumlah Perhiasan</th>
        						<th class="text-center" width="18%">Action</th>
        					</tr>
        				</thead>
        				<tbody>
        					<?php
        					$no = 1;
                            if(!isset($_GET['from']) && !isset($_GET['to'])){
                            $date = date('Y-m-d');
        					$getfpjt = $this->db->query("SELECT tb_fpjt.* FROM tb_fpjt
								LEFT JOIN master_jenis_permata ON master_jenis_permata.id = tb_fpjt.jenis_perhiasan
								LEFT JOIN step ON step.id_object = tb_fpjt.id
                                WHERE
                                ISNULL(tb_fpjt.delete_by) AND step.print = 'Sudah'
								AND DATE_FORMAT(tb_fpjt.create_date,'%d-%m-%Y')=DATE_FORMAT('$date','%d-%m-%Y')
								AND step.store='".sessionValue('kode_store')."'
                                GROUP BY tb_fpjt.id_order ORDER BY tb_fpjt.create_date DESC");
        					foreach ($getfpjt->result() as $get) {
								$ord= str_replace('/', '-', $get->id_order); 
        						?>
        					<tr>
                                <td><?php echo $no++; ?></td>
                                <td><?php echo $get->input_date ?></td>
                                <td><?php echo $get->id_order ?></td>
                                <td><?php echo $get->id_member ?></td>
                                <td><?php echo $get->jumlah ?></td>
        						<td class="text-right">
                                <?php
                                $idn = str_replace('/', '-', $get->id_order);
                                ?>
        							<button class="btn btn-info disabled" data-toggle="tooltip" data-placement="bottom" title="View"><i class="fa fa-eye"></i></button>
        							<a href="#" class="btn btn-warning disabled" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="fa fa-edit"></i></a>
        							<a href="#" class="btn btn-danger disabled" data-toggle="tooltip" data-placement="bottom" title="Delete"><i class="fa fa-trash"></i></a>
        							<a href="<?php echo site_url('invoice/print_invoice_fpjt').'/'.$ord ?>" target="_blank" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="Print"><i class="fa fa-print"></i></a>
        						</td>
        					</tr>
        						<?php
        					}
                        }else{
                            $from = $_GET['from'];
                            $to = $_GET['to'];

                            $from = date_create($from);
                            $from = date_format($from, "Y-m-d");
                            $to = date_create($to);
                            $to = date_format($to, "Y-m-d");
							$to = date('Y-m-d',strtotime($to. "+1 days"));

                            $getfpjt = $this->db->query("SELECT tb_fpjt.* FROM tb_fpjt
								LEFT JOIN master_jenis_permata ON master_jenis_permata.id = tb_fpjt.jenis_perhiasan
								LEFT JOIN step ON step.id_object = tb_fpjt.id
                                WHERE
                                ISNULL(tb_fpjt.delete_by) AND step.print = 'Sudah'
								AND tb_fpjt.create_date >= '$from 12:00:00 AM' AND tb_fpjt.create_date <= '$to 11:59:59 PM'
								AND step.store='".sessionValue('kode_store')."'
                                GROUP BY tb_fpjt.id_order ORDER BY tb_fpjt.create_date DESC");
                            foreach ($getfpjt->result() as $get) {
								$ord= str_replace('/', '-', $get->id_order);
                                ?>
                            <tr>
                                <td><?php echo $no++; ?></td>
                                <td><?php echo $get->input_date ?></td>
                                <td><?php echo $get->id_order ?></td>
                                <td><?php echo $get->id_member ?></td>
                                <td><?php echo $get->jumlah ?></td>
                                <td class="text-right">
                                <?php
                                $idn = str_replace('/', '-', $get->id_order);
                                ?>
                                    <button class="btn btn-info disabled" data-toggle="tooltip" data-placement="bottom" title="View"><i class="fa fa-eye"></i></button>
                                    <a href="#" class="btn btn-warning disabled" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="fa fa-edit"></i></a>
                                    <a href="#" class="btn btn-danger disabled" data-toggle="tooltip" data-placement="bottom" title="Delete"><i class="fa fa-trash"></i></a>
                                    <a href="<?php echo site_url('invoice/print_invoice_fpjt').'/'.$ord ?>" target="_blank" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="Print"><i class="fa fa-print"></i></a>
                                </td>
                            </tr>
                                <?php
                            }
                        }
                        ?>
        				</tbody>
        			</table>
        		</div>
        	</div>
        </section>
</div>