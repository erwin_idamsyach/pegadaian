<?php
$ord = str_replace('-', '/', $ord);
$data = $this->db->query("SELECT tb_front_desk.*, step.certificate, step.gem_card, step.dia_grading, color_stone.color,
							shape.shape, tb_lab_desk.obj_natural
							FROM tb_front_desk 
							LEFT JOIN color_stone ON color_stone.code = tb_front_desk.obj_color
							LEFT JOIN step ON step.id_object = tb_front_desk.id_object
							LEFT JOIN tb_lab_desk ON tb_lab_desk.id_object = tb_front_desk.id_object
							LEFT JOIN shape ON shape.code = tb_front_desk.obj_shape
							WHERE tb_front_desk.id_order='$ord' 
							AND (step.print_gem = 'Sudah' OR step.print_cert = 'Sudah' OR step.print_grading = 'Sudah')
							AND step.store='".sessionValue('kode_store')."'
							AND ISNULL(tb_front_desk.delete_by)");
$data2 = $this->db->query("SELECT tb_front_desk.*, step.certificate, step.gem_card, step.dia_grading, color_stone.color,
							shape.shape, tb_lab_desk.obj_natural
							FROM tb_front_desk 
							LEFT JOIN color_stone ON color_stone.code = tb_front_desk.obj_color
							LEFT JOIN step ON step.id_object = tb_front_desk.id_object
							LEFT JOIN tb_lab_desk ON tb_lab_desk.id_object = tb_front_desk.id_object
							LEFT JOIN shape ON shape.code = tb_front_desk.obj_shape
							WHERE tb_front_desk.id_order='$ord' 
							AND (step.print_gem = 'Sudah' OR step.print_cert = 'Sudah' OR step.print_grading = 'Sudah')
							AND step.store='".sessionValue('kode_store')."'
							AND ISNULL(tb_front_desk.delete_by)");
$dataUser = $this->db->query("SELECT tb_member.first_name, tb_member.middle_name, tb_member.last_name, 
	tb_member.corp_name, tb_member.address, tb_member.postal_code, tb_front_desk.id_order, tb_front_desk.id_member,
	kecamatan.kecamatanNama kec1, kabupaten.kabupatenNama kota1, provinsi.provinsiNama prov1
 FROM tb_member
 LEFT JOIN tb_front_desk ON tb_front_desk.id_member = tb_member.id_member 
 LEFT JOIN kecamatan ON kecamatan.kecamatanId = tb_member.district
 LEFT JOIN kabupaten ON kabupaten.kabupatenId = tb_member.city
 LEFT JOIN provinsi ON provinsi.provinsiId = tb_member.province
 WHERE
 tb_member.id_member = tb_front_desk.id_member AND
 tb_front_desk.id_order = '$ord'
 GROUP BY tb_front_desk.id_order");
$cer_pri = mysql_query("SELECT * FROM tb_service WHERE kode='FR'");
$pri_cer = mysql_fetch_array($cer_pri);

$mem_pri = mysql_query("SELECT * FROM tb_service WHERE kode='BR'");
$pri_mem = mysql_fetch_array($mem_pri);

$dim_pri = mysql_query("SELECT * FROM tb_service WHERE kode='DG'");
$pri_dim = mysql_fetch_array($dim_pri);



$sub = 0;
$pdf = new FPDF('P','mm','A4');
$pdf->SetAutoPageBreak(false, 0);
$pdf->AddPage();

foreach($data2->result_array() as $g){
$dudate  = date_create($g['create_date']);
$day  = date_format($dudate, "d");
$month  = date_format($dudate, "m");
$year  = date_format($dudate, "Y");
}

$pdf->SetFont('arial','B',16);
$pdf->SetXY(0,18);
$pdf->Cell(190,0,'Tagihan Pembayaran Sertifikasi',0,0,'C');
$pdf->SetFillColor(46, 46, 46);
$pdf->Rect(145, 10, 165, 13, 'F');
$pdf->Image('./asset/images/LOGO-COG-FINAL.png', 138, 2, 50, 30);

$pdf->SetFont('arial','B',10);
$pdf->SetXY(140, 29);
/*$pdf->Cell(0, 0, 'Pegadaian Gemological Laboratory');*/

$pdf->SetFont('arial','',10);
$pdf->SetXY(140, 30);
$pdf->MultiCell(70, 5, "",'','L');
//$pdf->Ln();
//$pdf->SetXY(140, 37);
//$pdf->Cell(0, 0, 'Indonesia');

$pdf->SetY(45);
$pdf->Cell(20, 5, 'No. ');
$pdf->Cell(10, 5, ':');
$pdf->Cell(10, 5, $ord);
$pdf->Ln();

$pdf->Cell(20, 5, 'Tanggal');
$pdf->Cell(10, 5, ':');
$pdf->Cell(10, 5, $day."".bulan($month)." ".$year);
$pdf->Ln();

$pdf->Rect(110, 42, 90, 30,'D');

$nasabah = "";
$penerima = sessionValue('nama');
foreach ($dataUser->result() as $get) {
	$nasabah = $get->first_name." ".$get->last_name;
	# code...
	$pdf->SetXY(113, 47);
	$pdf->Cell(0, 0, $get->first_name." ".$get->last_name);
	$pdf->SetXY(113, 52);
	$pdf->Cell(0, 0, $get->address);
	$pdf->SetXY(113, 57);
	$pdf->Cell(0, 0, $get->kec1.' - '.str_replace('Kota ','', $get->kota1));
	$pdf->SetXY(113, 62);
	$pdf->Cell(0, 0, $get->postal_code);
	$pdf->SetXY(113, 67);
	$pdf->Cell(0, 0, $get->prov1);
	
	$new_ord = substr($get->id_order, -20, 9);
	$new_ord = str_replace("/", "-", $new_ord);
	$pdf->Image('./asset/barcode-fpjs/'.$get->id_member.$new_ord.'.gif', 10, 6, 35, 20);
}

$width = array(7, 20, 8, 23, 25, 22, 29, 26, 30);
$pdf->SetFont('arial','',11);
$pdf->SetY(80);
$pdf->Cell($width[0], 10, "No", 1, 0, 'C');
$pdf->Cell($width[1], 10, "Object ID", 1, 0, 'C');
$pdf->Cell($width[2], 10, "Qty", 1, 0, 'C');
$pdf->Cell($width[3], 10, "Berat", 1, 0, 'C');
$pdf->Cell($width[4], 10, "Bentuk", 1, 0, 'C');
$pdf->Cell($width[5], 10, "Warna", 1, 0, 'C');
$pdf->Cell($width[8], 10, "Keterangan", 1, 0, 'C');
$pdf->Cell($width[6], 10, "Order", 1, 0, 'C');
$pdf->Cell($width[7], 10, "Total", 1, 0, 'C');
$pdf->Ln();

//$pdf->Rect(10, 85, $width[0], 130, 'D');
//$pdf->Rect(30, 85, $width[1], 130, 'D');
//$pdf->Rect(110, 85, $width[2], 130, 'D');
//$pdf->Rect(125, 85, $width[3], 130, 'D');
//$pdf->Rect(155, 85, $width[4], 130, 'D');

$pdf->SetFont('arial','',10);

$cer = 0;
$mem = 0;
$dim = 0;
$no = 1;
$ong = 1;
$height = 7;
$status = false;
$x1 = $pdf->GetX();
$y1 = $pdf->GetY();
foreach($data->result_array() as $g){
	$exp = explode(';', $g['color']);
	if(count($exp)>0){
		$color_spek = $exp[0];
		$exp_spek = explode(' ', $color_spek);
		if(count($exp)>1){
			//$color_spek = $exp_spek[0].' '.$exp_spek[1];
		}else{
			//$color_spek = $exp[0];
		}
	}else{
		$color_spek = $g['color'];
	}
	$x = $pdf->GetX();
	$pdf->Cell($width[0], $height, $no++, '', 0, 'C');
	$pdf->Cell($width[1], $height, $g['id_object'], '', 0, 'C');
	$pdf->Cell($width[2], $height, '1', '', 0, 'C');
	$pdf->Cell($width[3], $height, $g['obj_weight'].' ct(s)', '', 0, 'R');
	$pdf->Cell($width[4], $height, $g['shape'], '', 0, 'L');
	$x_color = $pdf->GetX();
	$y_color = $pdf->GetY();
	$pdf->MultiCell($width[5], $height, ucfirst($color_spek), '', 'L');
	$yy_color = $pdf->GetY();
	$pdf->SetXY($x_color+$width[5], $y_color);
	$xx = $pdf->GetX();
	$yy = $pdf->GetY();
	$pdf->MultiCell($width[8], $height, $g['comment'], '', 'L');
	$y_comment = $pdf->GetY();
	$pdf->SetXY($xx+$width[8], $yy);
	if($g['certificate'] != ''){
		$xx = $pdf->GetX();
		$yy = $pdf->GetY();
		$pdf->MultiCell($width[6], $height, ucwords(strtolower($pri_cer['nama'])), '', 'L');
		$y = $pdf->GetY();
		$pdf->SetXY($xx+$width[6], $yy);
		$pdf->Cell(5, $height, 'Rp. ', '', 0, 'L');
		$pdf->Cell($width[7]-5, $height, number_format($pri_cer['harga'],0,',','.'), '', 0, 'R');
		$sub = $sub+$pri_cer['harga'];
	}else{
		if($g['gem_card'] != ''){
			$status = true;
			if($pri_mem['nama']=="BRIEF REPORT NATURAL"){
				$pri_mem['nama'] = "BRIEF REPORT";
			}else if($pri_mem['nama']=="BRIEF REPORT SYNTHETIC"){
				$pri_mem['nama'] = "BRIEF REPORT";
			}
			
			if($g['obj_natural']!="NATURAL"){
				$mem_pri = mysql_query("SELECT * FROM tb_service WHERE kode='BRS'");
				$pri_mem = mysql_fetch_array($mem_pri);
			}else{
				$mem_pri = mysql_query("SELECT * FROM tb_service WHERE kode='BR'");
				$pri_mem = mysql_fetch_array($mem_pri);
			}
			
			$xx = $pdf->GetX();
			$yy = $pdf->GetY();
			$pdf->MultiCell($width[6], $height, ucwords(strtolower($pri_mem['nama'])), '', 'L');
			$y = $pdf->GetY();
			$pdf->SetXY($xx+$width[6], $yy);
			$pdf->Cell(5, $height, 'Rp. ', '', 0, 'L');
			$pdf->Cell($width[7]-5, $height, number_format($pri_mem['harga'],0,',','.'), '', 0, 'R');
			$sub = $sub+$pri_mem['harga'];
		}else if($g['dia_grading'] != ''){
			$status = true;
			$getHarga = $this->db->query("SELECT harga
								FROM master_harga_grading
								WHERE berat2 >= ".$g['obj_weight']." ORDER BY ID ASC LIMIT 0,1");
			$harga_grading = 0;
			foreach ($getHarga->result() as $tmpHarga) {
				$harga_grading = $tmpHarga->harga;
			}
			$xx = $pdf->GetX();
			$yy = $pdf->GetY();
			$pdf->MultiCell($width[6], $height, ucwords(strtolower($pri_dim['nama'])), '', 'L');
			$y = $pdf->GetY();
			$pdf->SetXY($xx+$width[6], $yy);
			$pdf->Cell(5, $height, 'Rp. ', '', 0, 'L');
			$pdf->Cell($width[7]-5, $height, number_format($harga_grading,0,',','.'), '', 0, 'R');
			$sub = $sub+$harga_grading;
		}
	}
	if($g['gem_card'] != '' && $status==false){
		//$pdf->Ln();
		$pdf->SetXY($x, $y);
		$pdf->Cell($width[0], $height, '', '', 0, 'C');
		$pdf->Cell($width[1], $height, '', '', 0, 'C');
		$pdf->Cell($width[2], $height, '', '', 0, 'C');
		$pdf->Cell($width[3], $height, '', '', 0, 'R');
		$pdf->Cell($width[4], $height, '', '', 0, 'L');
		$pdf->Cell($width[5], $height, '', '', 0, 'L');
		$pdf->Cell($width[8], $height, '', '', 0, 'L');	
	
		if($pri_mem['nama']=="BRIEF REPORT NATURAL"){
			$pri_mem['nama'] = "BRIEF REPORT";
		}else if($pri_mem['nama']=="BRIEF REPORT SYNTHETIC"){
			$pri_mem['nama'] = "BRIEF REPORT";
		}
			
		if($g['obj_natural']!="NATURAL"){
			$mem_pri = mysql_query("SELECT * FROM tb_service WHERE kode='BRS'");
			$pri_mem = mysql_fetch_array($mem_pri);
		}else{
			$mem_pri = mysql_query("SELECT * FROM tb_service WHERE kode='BR'");
			$pri_mem = mysql_fetch_array($mem_pri);
		}
		$xx = $pdf->GetX();
		$yy = $pdf->GetY();
		$pdf->MultiCell($width[6], $height, ucwords(strtolower($pri_mem['nama'])), '', 'L');
		$y = $pdf->GetY();
		$pdf->SetXY($xx+$width[6], $yy);
		$pdf->Cell(5, $height, 'Rp. ', '', 0, 'L');
		$pdf->Cell($width[7]-5, $height, number_format($pri_mem['harga'],0,',','.'), '', 0, 'R');
		$sub = $sub+$pri_mem['harga'];
	}
	if($g['dia_grading'] != '' && $status==false){
		//$pdf->Ln();
		$pdf->SetXY($x, $y);
		$pdf->Cell($width[0], $height, '', '', 0, 'C');
		$pdf->Cell($width[1], $height, '', '', 0, 'C');
		$pdf->Cell($width[2], $height, '', '', 0, 'C');
		$pdf->Cell($width[3], $height, '', '', 0, 'R');
		$pdf->Cell($width[4], $height, '', '', 0, 'L');
		$pdf->Cell($width[5], $height, '', '', 0, 'L');
		$pdf->Cell($width[8], $height, '', '', 0, 'L');	
	
		$getHarga = $this->db->query("SELECT harga
								FROM master_harga_grading
								WHERE berat2 >= ".$g['obj_weight']." ORDER BY ID ASC LIMIT 0,1");
		$harga_grading = 0;
		foreach ($getHarga->result() as $tmpHarga) {
			$harga_grading = $tmpHarga->harga;
		}
		$xx = $pdf->GetX();
		$yy = $pdf->GetY();
		$pdf->MultiCell($width[6], $height, ucwords(strtolower($pri_dim['nama'])), '', 'L');
		$y = $pdf->GetY();
		$pdf->SetXY($xx+$width[6], $yy);
		$pdf->Cell(5, $height, 'Rp. ', '', 0, 'L');
		$pdf->Cell($width[7]-5, $height, number_format($harga_grading,0,',','.'), '', 0, 'R');
		$sub = $sub+$harga_grading;
	}
	$pdf->SetXY($x, $y);
}
$y_akhir = $pdf->GetY();
if($y_comment>$y_akhir && $y_comment>$yy_color){
	$y_akhir = $y_comment;
}else if($yy_color>$y_akhir && $yy_color>$y_comment){
	$y_akhir = $yy_color;
}

$y_height = $y_akhir - $y1;

// Baris
$pdf->SetXY($x1, $y1);
$pdf->Cell($width[0], $y_height, '', 'LR', 0, 'C');
$pdf->Cell($width[1], $y_height, '', 'R', 0, 'C');
$pdf->Cell($width[2], $y_height, '', 'R', 0, 'C');
$pdf->Cell($width[3], $y_height, '', 'R', 0, 'R');
$pdf->Cell($width[4], $y_height, '', 'R', 0, 'L');
$pdf->Cell($width[5], $y_height, '', 'R', 0, 'L');
$pdf->Cell($width[8], $y_height, '', 'R', 0, 'L');
$pdf->Cell($width[6], $y_height, '', 'R', 0, 'L');
$pdf->Cell($width[7], $y_height, '', 'R', 0, 'L');

$pdf->SetY($y_akhir);

$ppn = $sub*10/100;
$total = $sub+$ppn;

$pdf->SetFont('arial','B',10);

$pdf->Cell($width[0], 7, '', 'T', 0, 'C');
$pdf->Cell($width[1], 7, '', 'T', 0, 'L');
$pdf->Cell($width[2], 7, '', 'T', 0, 'L');
$pdf->Cell($width[3], 7, '', 'T', 0, 'L');
$pdf->Cell($width[4], 7, '', 'T', 0, 'L');
$pdf->Cell($width[5], 7, '', 'T', 0, 'L');
$pdf->Cell($width[8], 7, '', 'RT', 0, 'C');
$pdf->Cell($width[6], 7, 'Sub Total', 'RT');
$pdf->Cell(5, $height, 'Rp. ', 'T', 0, 'L');
$pdf->Cell($width[7]-5, 7, number_format($sub,0,',','.'), 'RT', 0, 'R');
$pdf->Ln();
/*
$pdf->Cell($width[0], 7, '', '', 0, 'C');
$pdf->Cell($width[1], 7, '', '', 0, 'L');
$pdf->Cell($width[2], 7, '', '', 0, 'L');
$pdf->Cell($width[3], 7, '', '', 0, 'L');
$pdf->Cell($width[4], 7, '', 'R', 0, 'C');
$pdf->Cell($width[5], 7, 'PPN', 'R');
$pdf->Cell($width[6], 7, number_format($ppn,0,',','.'), 'R', 0, 'R');
$pdf->Ln();*/

$pdf->Cell($width[0], 7, '', '', 0, 'C');
$pdf->Cell($width[1], 7, '', '', 0, 'L');
$pdf->Cell($width[2], 7, '', '', 0, 'L');
$pdf->Cell($width[3], 7, '', '', 0, 'L');
$pdf->Cell($width[4], 7, '', '', 0, 'L');
$pdf->Cell($width[5], 7, '', '', 0, 'L');
$pdf->Cell($width[8], 7, '', 'R', 0, 'C');
$pdf->Cell($width[6], 7, 'Total', 'BR');
$pdf->Cell(5, $height, 'Rp. ', 'B', 0, 'L');
$pdf->Cell($width[7]-5, 7, number_format($sub,0,',','.'), 'BR', 0, 'R');
$pdf->Ln();

$pdf->SetFont('arial','',10);
$pdf->Cell($width[0], 7, '', '', 0, 'C');
$pdf->Cell($width[1], 7, '', '', 0, 'L');
$pdf->Cell($width[2], 7, '', '', 0, 'L');
$pdf->Cell($width[3], 7, '', '', 0, 'L');
$pdf->Cell($width[4], 7, '', '', 0, 'L');
$pdf->Cell($width[5], 7, '', '', 0, 'L');
$pdf->Cell($width[8], 7, '', '', 0, 'C');
$pdf->Cell($width[6], 7, 'Harga sudah termasuk PPN 10%', '');
$pdf->Ln();

$width_bot = array(95,95,5);
/*$pdf->SetY(160);
$pdf->Cell($width_bot[0], 7, 'Penerima,', '', 0, 'C');
$pdf->Cell($width_bot[1], 7, 'Nasabah,', '', 0, 'C');
$pdf->Ln(15);
$pdf->Cell($width_bot[0], 5, '('.$penerima.')', '', 0, 'C');
$pdf->Cell($width_bot[1], 5, '('.$nasabah.')', '', 0, 'C');
$pdf->Ln();
$pdf->Cell($width_bot[0], 5, '', '', 0, 'C');
$pdf->Cell($width_bot[1], 5, '', '', 0, 'C');
$pdf->Ln(15);*/

/*$pdf->Cell($width_bot[0], 5, 'Pemberian Kuasa, ', 'LRT', 0, 'C');
$pdf->Cell($width_bot[2], 5, '', 'T', 0, 'L');
$pdf->Cell($width_bot[1]-$width_bot[2], 5, 'Kami yang bertandatangan di halaman depan Bukti', 'RT', 0, 'L');
$pdf->Ln();
$pdf->Cell($width_bot[0], 5, '', 'LR', 0, 'C');
$pdf->Cell($width_bot[2], 5, '', '', 0, 'L');
$pdf->Cell($width_bot[1]-$width_bot[2], 5, 'Permintaan Sertifikasi (BPS) ini, bertindak untuk', 'R', 0, 'L');
$pdf->Ln();
$pdf->Cell($width_bot[0], 5, 'Pada tanggal....................................................................', 'LR', 0, 'L');
$pdf->Cell($width_bot[2], 5, '', '', 0, 'L');
$pdf->Cell($width_bot[1]-$width_bot[2], 5, 'dan atas nama PT Pegadaian (Persero) dengan', 'R', 0, 'L');
$pdf->Ln();
$pdf->Cell($width_bot[0], 5, '.........................................................................................', 'LR', 0, 'L');
$pdf->Cell($width_bot[2], 5, '', '', 0, 'L');
$pdf->Cell($width_bot[1]-$width_bot[2], 5, 'Nasabah, menyepakati ketentuan sebagai berikut :', 'R', 0, 'L');
$pdf->Ln();
$pdf->Cell($width_bot[0], 5, 'Dengan ini saya memberikan kuasa untuk mengambil', 'LR', 0, 'L');
$pdf->Cell($width_bot[2], 5, '', '', 0, 'L');
$pdf->Cell($width_bot[1]-$width_bot[2], 5, '', 'R', 0, 'L');
$pdf->Ln();
$pdf->Cell($width_bot[0], 5, 'batu adi dan sertifikat, kepada : ', 'LR', 0, 'L');
$pdf->Cell($width_bot[2], 5, '1.', '', 0, 'L');
$pdf->Cell($width_bot[1]-$width_bot[2], 5, 'Batas waktu pengambilan hasil sertifikasi maksimal ', 'R', 0, 'L');
$pdf->Ln();
$pdf->Cell($width_bot[0], 5, '', 'LR', 0, 'L');
$pdf->Cell($width_bot[2], 5, '', '', 0, 'L');
$pdf->Cell($width_bot[1]-$width_bot[2], 5, '7 (tujuh) hari dari tanggal permintaan sertifikasi,', 'R', 0, 'L');
$pdf->Ln();
$pdf->Cell(25, 5, 'Nama', 'L', 0, 'L');
$pdf->Cell($width_bot[0]-25, 5, ':', 'R', 0, 'L');
$pdf->Cell($width_bot[2], 5, '', '', 0, 'L');
$pdf->Cell($width_bot[1]-$width_bot[2], 5, 'dan dapat dikuasakan dengan menyertakan KTP Asli', 'R', 0, 'L');
$pdf->Ln();
$pdf->Cell(25, 5, 'No. KTP/SIM', 'L', 0, 'L');
$pdf->Cell($width_bot[0]-25, 5, ':', 'R', 0, 'L');
$pdf->Cell($width_bot[2], 5, '', '', 0, 'L');
$pdf->Cell($width_bot[1]-$width_bot[2], 5, 'pemilik dan yang dikuasakan.', 'R', 0, 'L');
$pdf->Ln();
$pdf->Cell(25, 5, 'Alamat', 'L', 0, 'L');
$pdf->Cell($width_bot[0]-25, 5, ':', 'R', 0, 'L');
$pdf->Cell($width_bot[2], 5, '2.', '', 0, 'L');
$pdf->Cell($width_bot[1]-$width_bot[2], 5, 'Apabila Bukti Permintaan Sertifkasi (BPS) ini hilang', 'R', 0, 'L');
$pdf->Ln();
$pdf->Cell($width_bot[0], 5, '', 'LR', 0, 'L');
$pdf->Cell($width_bot[2], 5, '', '', 0, 'L');
$pdf->Cell($width_bot[1]-$width_bot[2], 5, 'harap segera melapor ke Pegadaian G-Lab tempat', 'R', 0, 'L');
$pdf->Ln();
$pdf->Cell($width_bot[0]/2, 5, 'Pemberi Hak,', 'L', 0, 'C');
$pdf->Cell($width_bot[0]/2, 5, 'Penerima Hak,', 'R', 0, 'C');
$pdf->Cell($width_bot[2], 5, '', '', 0, 'L');
$pdf->Cell($width_bot[1]-$width_bot[2], 5, 'BPS ini diterbitkan dengan menyertakan surat', 'R', 0, 'L');
$pdf->Ln();
$pdf->Cell($width_bot[0], 5, '', 'LR', 0, 'L');
$pdf->Cell($width_bot[2], 5, '', '', 0, 'L');
$pdf->Cell($width_bot[1]-$width_bot[2], 5, 'kehilangan dari kepolisian', 'R', 0, 'L');
$pdf->Ln();
$pdf->Cell($width_bot[0], 5, '', 'LR', 0, 'L');
$pdf->Cell($width_bot[2], 5, '', '', 0, 'L');
$pdf->Cell($width_bot[1]-$width_bot[2], 5, '', 'R', 0, 'L');
$pdf->Ln();
$pdf->Cell($width_bot[0]/2, 5, '(................................)', 'L', 0, 'C');
$pdf->Cell($width_bot[0]/2, 5, '(................................)', 'R', 0, 'C');
$pdf->Cell($width_bot[2], 5, '', '', 0, 'L');
$pdf->Cell($width_bot[1]-$width_bot[2], 5, 'Demikian perjanjian ini berlaku dan mengikat para', 'R', 0, 'L');
$pdf->Ln();
$pdf->Cell($width_bot[0], 5, '', 'LR', 0, 'L');
$pdf->Cell($width_bot[2], 5, '', '', 0, 'L');
$pdf->Cell($width_bot[1]-$width_bot[2], 5, 'pihak sejak BPS ini ditandatangani pleh kedua belah', 'R', 0, 'L');
$pdf->Ln();
$pdf->Cell($width_bot[0], 5, '(Lampirkan fotocopy KTP masing-masing, konfirmasi', 'LR', 0, 'L');
$pdf->Cell($width_bot[2], 5, '', '', 0, 'L');
$pdf->Cell($width_bot[1]-$width_bot[2], 5, 'pihak pada kolom yang tersedia di halaman depan. ', 'R', 0, 'L');
$pdf->Ln();
$pdf->Cell($width_bot[0], 5, 'Nasabah)', 'LBR', 0, 'L');
$pdf->Cell($width_bot[1], 5, '', 'BR', 0, 'L');
*/
$pdf->Output();
?>