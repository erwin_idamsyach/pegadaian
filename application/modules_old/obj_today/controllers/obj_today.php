<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class obj_today extends CI_Controller{
    function obj_today(){
        parent::__construct();
		
		if(!isLogin()){
			goLogin();
		}
    }
    
    function index($text=NULL){
        $data["filelist"] = "obj_today/obj_today";
        $data["title"] = "Lab";
        $data["title_menu"] = "Lab";
        $data["menu"] = "obj_today";
		
        getHTMLWeb($data);
    }

    function view_obj($id)
    {
        $this->load->view('view_object', array('id'=>$id));
    }

    function finalx()
    {
        $hardness = $this->input->post('hard');
        $ri_start = $this->input->post('ri');
        $sg_end   = $this->input->post('sg');
        $this->load->view('final', array('hardness'=>$hardness,'ri_start'=>$ri_start, 'sg_end'=>$sg_end));
    }

    function ambilbatu()
    {
        $batu = $this->input->post('batu');
        $this->load->view('ambil', array('batu'=>$batu));
    }

    function edit_dg($id)
    {
        $l = $this->db->get_where('tb_lab_grading', array('id_object'=>$id));
        if($l->num_rows() == 0){
            $q = $this->db->query("SELECT * FROM tb_front_desk where id_object='$id'");
            foreach ($q->result_array() as $r) {
            }
            $data = array(
                'id_member'     => $r['id_member'],
                'id_order'      => $r['id_order'],
                'id_object'     => $r['id_object'],
                'obj_weight'    => $r['obj_weight'],
                'obj_height'    => $r['obj_height'],
                'obj_width'     => $r['obj_width'],
                'obj_length'    => $r['obj_length'],
                'obj_color'     => $r['obj_color'],
                'obj_image'     => $r['obj_image']
            );
            $this->db->insert('tb_lab_grading', $data);
        }else{

        }
        $this->load->view('edit2', array('id'=>$id));
    }

    function edit_obj($id)
    {
        $l = $this->db->get_where('tb_lab_desk', array('id_object'=>$id));
        if($l->num_rows() == 0){
            $q = $this->db->query("SELECT * FROM tb_front_desk where id_object='$id'");
            foreach ($q->result_array() as $r) {
            }
            $data = array(
                'id_member'     => $r['id_member'],
                'id_object'     => $r['id_object'],
                'obj_weight'    => $r['obj_weight'],
                'obj_height'    => $r['obj_height'],
                'obj_width'     => $r['obj_width'],
                'obj_length'    => $r['obj_length'],
                'obj_color'     => $r['obj_color'],
                'obj_image'     => $r['obj_image']
            );
            $this->db->insert('tb_lab_desk', $data);
        }else{

        }
        $this->load->view('edit', array('id'=>$id));
    }

    function delete_obj($id)
    {
        $user = sessionValue('username');
        date_default_timezone_set('Asia/Jakarta');
        $now  = date('Y-m-d');
        $data = array(
            'delete_by'     => $user,
            'delete_date'   => $now
        );
        $this->db->where('id_object', $id);
        $this->db->update('tb_front_desk', $data);

        $this->db->where('id_object', $id);
        $this->db->update('step', $data);

        redirect('obj_today');
    }

    function update_gd($id)
    {
        date_default_timezone_set('Asia/Jakarta');
        $tgl = date('Y-m-d');
        $now = date('Y-m-d H:i:s');
        $shape          = $this->input->post('shape');
        $cut            = $this->input->post('cut');    
        $color          = $this->input->post('color');
        $clarity        = $this->input->post('clarity');
        $cut_grade      = $this->input->post('cut_grade');
        $girdle         = $this->input->post('girdle');
        $culet          = $this->input->post('culet');
        $fluorescence   = $this->input->post('fluorescence');
        $key            = $this->input->post('key');

        $data = array(
            'shape'         => $shape,
            'cut'           => $cut,
            'color_grading' => $color,
            'clarity'       => $clarity,
            'cut_grade'     => $cut_grade,
            'girdle'        => $girdle,
            'culet'         => $culet,
            'fluorescence'  => $fluorescence,
            'key'           => $key,
            'finish'        => $now
        );
        $this->db->where('id_object', $id);
        $this->db->update('tb_lab_grading', $data);

        $qi = $this->db->get_where('step', array('id_object'=>$id));
        foreach ($qi->result_array() as $k) {
        }
        if($k['certificate'] == "" && $k['gem_card'] == "" && $k['dia_grading'] != ""){
            $dax = array(
            'step'      => 'LAB_DESK',
            'check_dg'  => 'Sudah'
            );
            $this->db->where('id_object', $id);
            $this->db->update('step', $dax);
        }else{
            $dax = array(
            'check_dg'  => 'Sudah'
            );
            $this->db->where('id_object', $id);
            $this->db->update('step', $dax);
        }
        
        redirect('obj_today');
    }

    function update_obj($id)
    {
        date_default_timezone_set('Asia/Jakarta');
        $tgl = date('Y-m-d');
        $now = date('Y-m-d H:i:s');
        $shape          = $this->input->post('shape');
        $cut            = $this->input->post('cut');    
        $cut2           = $this->input->post('cut2');
        $hardness       = $this->input->post('hardness');
        $gravity        = $this->input->post('gravity');
        $cleavage       = $this->input->post('cleavage');
        $fracture       = $this->input->post('fracture');
        $luminescence   = $this->input->post('luminescence');
        $tenacity       = $this->input->post('tenacity');
        $radio          = $this->input->post('radio');
        $crystal_gems   = $this->input->post('crystal_gems');
        $transparent    = $this->input->post('transparent');
        $ri_start       = $this->input->post('ri_start');
        $luster         = $this->input->post('luster');
        $pleochroism    = $this->input->post('pleochroism');
        $birefringence  = $this->input->post('birefringence');
        $dispersion     = $this->input->post('dispersion');
        $mineral        = $this->input->post('mineral');
        $strunz         = $this->input->post('strunz');
        $related        = $this->input->post('related');
        $member         = $this->input->post('member');
        $synonyms       = $this->input->post('synonyms');
        $crystallography= $this->input->post('crystallography');
        $crystal        = $this->input->post('crystal');
        $type           = $this->input->post('type');
        $twinning       = $this->input->post('twinning');
        $geological     = $this->input->post('geological');
        $common_asso    = $this->input->post('common_asso');
        $common_impu    = $this->input->post('common_impu');
        $year           = $this->input->post('year');
        $natural        = $this->input->post('natural');
        $phenomenal     = $this->input->post('phenomenal');
        $varieties      = $this->input->post('varieties');
        $batu           = $this->input->post('batu');
        $note           = $this->input->post('note');
        $note2          = $this->input->post('note2');
        $note3          = $this->input->post('note3');
        $comment        = $this->input->post('comment');

        $data = array(
            'phenomenal'    => $phenomenal,
            'crystal_gems'  => $crystal_gems,
            'obj_cut'       => $cut,
            'obj_cut2'      => $cut2,
            'obj_shape'     => $shape,
            'obj_natural'   => $natural,
            'ri_start'      => $ri_start,
            'cleavage'      => $cleavage,
            'fracture'      => $fracture,
            'tenacity'      => $tenacity,
            'luminescence'  => $luminescence,
            'radioactivity' => $radio,
            'hardness'      => $hardness,
            'sg'            => $gravity,
            'mineral'       => $mineral,
            'strunz'        => $strunz,
            'related'       => $related,
            'member'        => $member,
            'synonyms'      => $synonyms,
            'variety'       => $varieties,
            'pleochroism'   => $pleochroism,
            'luster'        => $luster,
            'birefringence' => $birefringence,
            'dispersion'    => $dispersion,
            'transparancy'  => $transparent,
            'crystallography'=> $crystallography,
            'crystal_habit' => $crystal,
            'twinning'      => $twinning,
            'geological_setting'  => $geological,
            'common_asso'   => $common_asso,
            'common_impu'   => $common_impu,
            'year_discovered'=> $year,
            'type_local'    => $type,
            'input_date'    => $tgl,
            'nama_batu'     => $batu,
            'note'          => $note,
            'note2'         => $note2,
            'note3'         => $note3,
            'comment'       => $comment,
            'finish'        => $now
        );
        $this->db->where('id_object', $id);
        $this->db->update('tb_lab_desk', $data);

        $dax = array(
            'step'  => 'LAB_DESK'
        );
        $this->db->where('id_object', $id);
        $this->db->update('step', $dax);

        redirect('obj_today');
    }
}
