      <div class="alertx modal fade" id="alertx" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-body">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <div class="row">
                  <center><h4>DATA NOT AVAILABLE</h4></center>
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- Modal -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" style="width:400px;">
          <div class="modal-content">
            <div class="modal-header" style="background-color:#f8f8f8">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h6 class="modal-title" id="myModalLabel"><b>VIEW OBJECT</b></h6>
            </div>
            <div class="modal-body">
              <div class="lol">
                
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="index">
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>&nbsp;</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-user"></i> Home</a></li>
            <li class="active">List Entry Today</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="box">
            <div class="box-header">
              <h5><b>LIST OBJECT ENTRY TODAY (NON DIAMOND GRADING)</b></h5>
              <hr style="border:1px solid black;margin-bottom:-10px;margin-top:-5px;">
              <br>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><span class="fa fa-minus"></span></button>
                <button class="btn btn-box-tool" data-widget="remove"><span class="fa fa-times"></span></button>
              </div>
            </div><!-- /.box-header -->
            <div class="box-body" style="margin-top:-15px;">
              <?php
                date_default_timezone_get('Asia/Jakarta');
                $tgl = date('Y-m-d');
                $no = 1;
                $per_page = 5;
                $kode = sessionValue('kode_store');
                $u = $this->db->query("SELECT * FROM tb_front_desk a, color_stone c, step b where b.step='FRONT_DESK' and a.store='$kode' and b.certificate!='' and a.input_date LIKE '%$tgl%' and a.delete_by='' and a.obj_color=c.id and a.id_object=b.id_object order by a.input_date");
              ?>
                <table id="example2" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th style="text-align:center">NO</th>
                      <th style="text-align:center">ID</th>
                      <th style="text-align:center">COLOR</th>
                      <th style="text-align:center">CTS</th>
                      <th style="text-align:center">L (mm)</th>
                      <th style="text-align:center">W (mm)</th>
                      <th style="text-align:center">H (mm)</th>
                      <th style="text-align:center">TIME IN</th>
                      <th class="text-center">ACTION</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php
                    foreach ($u->result_array() as $i) {
                    $tglx = date('H:i A', strtotime($i['input_date']));
                  ?>
                    <tr>
                      <td style="text-align:center"><?php echo $no++?></td>
                      <td><?php echo $i['id_object']?></td>
                      <td style="text-align:center"><a class="btn btn-default btn-lg" style="background-color:#<?php echo $i['rgb_code']?>;"></a></td>
                      <td style="text-align:center"><?php echo $i['obj_weight']?></td>
                      <td style="text-align:center"><?php echo $i['obj_length']?></td>
                      <td style="text-align:center"><?php echo $i['obj_width']?></td>
                      <td style="text-align:center"><?php echo $i['obj_height']?></td>
                      <td style="text-align:center"><?php echo $tglx?></td>
                      <td style="text-align:right;width:200px;">
                        <a onclick="view('<?php echo $i['id_object'] ?>')" class="btn btn-info" data-toggle="tooltip" data-placement="bottom" title="View Object"><i class="fa fa-eye"></i></a>
                        <a onclick="gocedit('<?php echo $i['id_object'] ?>')" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Start Examintion"><i class="fa fa-pencil"></i></a>
                        <a href="./obj_today/delete_obj/<?php echo $i['id_object'] ?>" class="btn btn-danger" onclick="return confirm('Apakah anda yakin akan menghapus data ini?')" data-toggle="tooltip" data-placement="bottom" title="Delete Object"><i class="fa fa-close"></i></a>
                      </td>
                    </tr>
                    <?php
                      }
                    ?>
                  </tbody>
                </table>
              </div>
            </div><!-- /.box-body -->
            <div class="box">
            <div class="box-header">
              <h5><b>LIST OBJECT ENTRY TODAY (DIAMOND GRADING)</b></h5>
              <hr style="border:1px solid black;margin-bottom:-10px;margin-top:-5px;">
              <br>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><span class="fa fa-minus"></span></button>
                <button class="btn btn-box-tool" data-widget="remove"><span class="fa fa-times"></span></button>
              </div>
            </div><!-- /.box-header -->
            <div class="box-body" style="margin-top:-15px;">
              <?php
                $no = 1;
                $per_page = 5;
                $kode = sessionValue('kode_store');
                $u = $this->db->query("SELECT * FROM tb_front_desk a, color_stone c, step b where b.dia_grading!='' and b.check_dg='' and a.store='$kode' and a.input_date LIKE '%$tgl%' and a.delete_by='' and a.obj_color=c.id and a.id_object=b.id_object order by a.input_date");
              ?>
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th style="text-align:center">NO</th>
                      <th style="text-align:center">ID</th>
                      <th style="text-align:center">COLOR</th>
                      <th style="text-align:center">CTS</th>
                      <th style="text-align:center">L (mm)</th>
                      <th style="text-align:center">W (mm)</th>
                      <th style="text-align:center">H (mm)</th>
                      <th style="text-align:center">TIME IN</th>
                      <th class="text-center">ACTION</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php
                    foreach ($u->result_array() as $i) {
                    $tglx = date('H:i A', strtotime($i['input_date']));
                  ?>
                    <tr>
                      <td style="text-align:center"><?php echo $no++?></td>
                      <td><?php echo $i['id_object']?></td>
                      <td style="text-align:center"><a class="btn btn-default btn-lg" style="background-color:#<?php echo $i['rgb_code']?>;"></a></td>
                      <td style="text-align:center"><?php echo $i['obj_weight']?></td>
                      <td style="text-align:center"><?php echo $i['obj_length']?></td>
                      <td style="text-align:center"><?php echo $i['obj_width']?></td>
                      <td style="text-align:center"><?php echo $i['obj_height']?></td>
                      <td style="text-align:center"><?php echo $tglx?></td>
                      <td style="text-align:right;width:200px;">
                        <a onclick="view('<?php echo $i['id_object'] ?>')" class="btn btn-info" data-toggle="tooltip" data-placement="bottom" title="View Object"><i class="fa fa-eye"></i></a>
                        <a onclick="godadit('<?php echo $i['id_object'] ?>')" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Start Examintion"><i class="fa fa-pencil"></i></a>
                        <a href="./obj_today/delete_obj/<?php echo $i['id_object'] ?>" class="btn btn-danger" onclick="return confirm('Apakah anda yakin akan menghapus data ini?')" data-toggle="tooltip" data-placement="bottom" title="Delete Object"><i class="fa fa-close"></i></a>
                      </td>
                    </tr>
                    <?php
                      }
                    ?>
                  </tbody>
                </table>
              </div>
            </div><!-- /.box-body -->
          </section><!-- /.content -->
        </div><!-- /.content-wrapper -->
      </div>