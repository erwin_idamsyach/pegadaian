<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
* Controller Master Agama
* Create By : Hadi Setiawan
* 11 Mei 2016
*/
class hargaberlian extends CI_Controller{
	public $table = "master_tarif_taksiran";
	public $controls = "hargaberlian";
	
	function __construct(){
		parent::__construct();

		if(!isLogin()){
			goLogin();
		}
	}
	
	public function index(){
		$data['filelist'] = $this->controls.'/front';
		$data['title'] = 'Parameter';
		$data['title_menu'] = 'Admin';
		$data['menu'] = 'Harga Berlian';
		$data['controls'] = $this->controls;
		$data['table'] = $this->table;

		getHTMLWeb($data);
	}

	public function addData(){
		$name = $this->input->post('nama');
		$berat = $this->input->post('berat');
		$berat2 = $this->input->post('berat2');

		$data = array(
			'berat1' => $berat,
			'berat2' => $berat2,
			'harga' => convertToNumber($name),
			'create_by' => sessionValue('id'),
			'create_date' => date('Y-m-d H:i:s')	
			);
		$this->db->insert($this->table, $data);
		redirect(site_url($this->controls));
	}

	public function editData(){
		$id  = $this->uri->segment(3);

		$data['filelist'] = $this->controls.'/front';
		$data['title'] = 'Parameter';
		$data['title_menu'] = 'Admin';
		$data['menu'] = 'Harga Berlian';		
		$data['controls'] = $this->controls;
		$data['table'] = $this->table;
		
		$data['id'] = $id;

		getHTMLWeb($data);
	}

	public function updateData(){
		$id   = $this->input->post('id');
		$name = $this->input->post('nama');
		$berat = $this->input->post('berat');
		$berat2 = $this->input->post('berat2');

		$data = array(
			'berat1' => $berat,
			'berat2' => $berat2,
			'harga' => convertToNumber($name),
			'update_by' => sessionValue('id'),
			'update_date' => date('Y-m-d H:i:s')
			);

		$this->db->where('id', $id);
		$this->db->update($this->table, $data);
		redirect(site_url($this->controls));
	}

	public function deleteData(){
		$id = $this->uri->segment(3);

		$data = array(
			'delete_by' => sessionValue('id'),
			'delete_date' => date('Y-m-d H:i:s')
			);
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);
		redirect(site_url($this->controls));
	}
}
?>