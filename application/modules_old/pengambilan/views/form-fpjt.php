<script type="text/javascript">
	$(document).ready(function(){
			$('#form-pengambilan-fpjs').trigger("reset");
			$("#form-pengambilan-fpjs").on('submit',function(e){
			e.preventDefault();
			var formatData = new FormData($(this)[0]);
				swal({
				 title: "",
				 text: "Apakah Anda akan menyimpan data?",
				 type: "warning",
				 showCancelButton: true,
				 confirmButtonColor: "#DD6B55",
				 confirmButtonText: "Ya",
				 cancelButtonText: "Tidak",
				 closeOnConfirm: false }, function(){
					$.ajax({
						type : 'POST',
						url : '<?php echo base_url()?>pengambilan/pengambilan_barang',
						data : formatData,
						async: false,
						cache: false,
						contentType: false,
						processData: false,
						success:function(html){
							var data = eval ("(" + html + ")");
							if(data.success){
								swal({
								 title: "Berhasil Menyimpan data!",
								 text: "Klik tombol di bawah.",
								 type: "success",
								 showCancelButton: false,
								 confirmButtonColor: "#257DB6",
								 confirmButtonText: "Ok!",
								 closeOnConfirm: false
								},
								function(){
									location.href = base_url+"pengambilan/fpjt";
								});
							}else{
								swal({
								 title: data.msg,
								 text: "Klik tombol di bawah.",
								 type: "warning",
								 showCancelButton: false,
								 confirmButtonColor: "#257DB6",
								 confirmButtonText: "Ok!",
								 closeOnConfirm: false
								});
							}
						}
					});
				});
			});
	});
		
		function backToList(){
			location.href = base_url+"pengambilan/fpjt";
		}
		
      </script>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>&nbsp;</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-user"></i> Home</a></li>
            <li>Pengambilan</li>
            <li class="active">Form Pengambilan FPJS</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
        <div class="index">
          <div class="box">
            <div class="box-header">
              <b>No. FPJT : <?php echo $tmp->id_order; ?></b>
              <div style="border:1px solid black;margin-bottom:-10px;"></div>
                <div class="box-tools pull-right">
                  <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
            <i>DATA NASABAH</i>
              <div style="border:1px solid black;margin-bottom:10px;"></div>
          <form method="post" id="form-pengambilan-fpjs" enctype="multipart/form-data">
			<table width="100%">
				<tr>
					<td width="10%"><b>No. CIF</b></td>
					<td width="10">:</td>
					<td><?php echo $tmp->id_member;?></td>
				</tr>
				<tr>
					<td><b>Nama</b></td>
					<td>:</td>
					<td><?php echo $tmp->first_name." ".$tmp->middle_name." ".$tmp->last_name;?></td>
				</tr>
				<tr>
					<td><b>Alamat</b></td>
					<td>:</td>
					<td><?php echo $tmp->address." ".$tmp->postal_code.", ".$tmp->kec1." - ".str_replace('Kota ','', $tmp->kota1);?></td>
				</tr>
				<tr>
					<td><b>&nbsp;</b></td>
					<td>&nbsp;</td>
					<td><?php echo $tmp->prov1;?></td>
				</tr>
				<tr>
					<td><b>No Telp / HP</b></td>
					<td>:</td>
					<td><?php echo ($tmp->phone)?$tmp->phone:"-";?></td>
				</tr>
				<tr>
					<td><b>Status</b></td>
					<td>:</td>
					<td><?php echo ($tmp->pay=="Y")?"Bayar":"Tidak Teridentifikasi";?></td>
				</tr>
			</table>
			<br>
			<i>DATA OBYEK</i>
            <div style="border:1px solid black;margin-bottom:10px;"></div>
			<table width="100%">
				<thead>
        			<tr>
        				<th class="text-center" width="5%">No</th>
						<th class="text-center" >Description</th>
						<th class="text-center" width="12%">Qty</th>
						<th class="text-center" width="15%">&nbsp;</th>
						<th class="text-center" width="15%">&nbsp;</th>
        			</tr>
        		</thead>
				<tbody>
				<?php
				$getHDLE = $this->db->query("SELECT * FROM master_harga_emas WHERE id='1'");
				$hdle = 0;
				foreach ($getHDLE->result() as $hsp) {
					$hdle = $hsp->harga_emas;
				}
				
				$getEmas = $this->db->query("SELECT * FROM tb_taksiran
							LEFT JOIN master_logam ON master_logam.id = tb_taksiran.jenis_logam
							WHERE tb_taksiran.id_order='".$tmp->id_order."' AND tb_taksiran.jenis_logam='1'");
							
				$harga_total = 0;
				foreach ($getEmas->result() as $key) {
					$harga_taksiran = 1.25/100*(($key->berat_bersih*($key->karatase_emas/24))*$hdle);					
					$harga_total = $harga_total + $harga_taksiran;
				} 
				
				$getBerlian = $this->db->query("SELECT * FROM tb_taksiran
								LEFT JOIN master_logam ON master_logam.id = tb_taksiran.jenis_logam
								WHERE tb_taksiran.id_order='".$tmp->id_order."'");
				
				$harga_berlian = 0;
				$berat_total = 0;	
				$gemolog = "";
				foreach ($getBerlian->result() as $key) {
					if($key->berat_total_permata == ""){
						$berat_total = $berat_total + 0;
					}else{
						$berat_total = $berat_total + $key->berat_total_permata;
					}
					
					$getHarga = $this->db->query("SELECT harga
												FROM `master_tarif_taksiran`
												WHERE berat2 >= ".$berat_total." ORDER BY ID ASC LIMIT 0,1");
												
					foreach ($getHarga->result() as $tmpHarga) {
						$harga_berlian = $harga_berlian + $tmpHarga->harga;
						
					}
				}
				$harga_total = $harga_total + $harga_berlian;
				
				$dataDetail = $this->db->query("SELECT tb_taksiran.*, master_permata.permata, master_jenis_permata.jenis jenis_perhiasan,
							color_stone.color FROM tb_taksiran
							LEFT JOIN master_permata ON master_permata.id = tb_taksiran.jenis_permata
							LEFT JOIN master_jenis_permata ON master_jenis_permata.id = tb_taksiran.jenis_perhiasan
							LEFT JOIN color_stone ON color_stone.code = tb_taksiran.warna_permata
							LEFT JOIN step ON step.id_object = tb_taksiran.id
							WHERE step.print = 'Sudah'
							AND tb_taksiran.id_order='".$tmp->id_order."' AND ISNULL(tb_taksiran.delete_by)");
				$no = 1;
				$total = 0;
				foreach ($dataDetail->result() as $tmpDetail) {			
					$exp = explode(';', $tmpDetail->color);
					if(count($exp)>0){
						$color_spek = $exp[0];
					}else{
						$color_spek = $tmpDetail->color;
					}
					
					if($tmpDetail->jumlah_permata<1){
						$harga_permata = 0;
					}
					
					$amount_permata = $harga_permata*$tmpDetail->jumlah_permata;
					$amount = $harga*$tmpDetail->jumlah;
				?>
					<tr>
						<td class="text-center" valign="top"><?php echo $no++; ?></td>
						<td class="text-center">
							<table width="100%">
								<tr>
									<td width="15%">Jenis Perhiasan</td>
									<td width="10">:</td>
									<td><?php echo $tmpDetail->jenis_perhiasan; ?></td>
								</tr>
								<tr>
									<td>&nbsp;&nbsp; Berat</td>
									<td>:</td>
									<td><?php echo ($tmpDetail->berat_perhiasan)?$tmpDetail->berat_perhiasan:"0"; ?> gr</td>
								</tr>
								<tr>
									<td>&nbsp;&nbsp; Permata</td>
									<td>:</td>
									<td><?php echo $tmpDetail->permata; ?></td>
								</tr>
								<tr>
									<td>&nbsp;&nbsp; Warna</td>
									<td>:</td>
									<td><?php echo $color_spek; ?></td>
								</tr>
								<tr>
									<td colspan="3">Jumlah Permata</td>
								</tr>
							</table>
						</td>
						<td class="text-center" valign="top">
							<table width="100%">
								<tr>
									<td class="text-center"><?php echo $tmpDetail->jumlah_perhiasan; ?></td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td class="text-center"><?php echo $tmpDetail->jumlah_permata; ?></td>
								</tr>
							</table>
						</td>
						<td class="text-center" valign="top">
							<table width="100%">
								<tr>
									<td class="text-right">&nbsp;</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td class="text-right">&nbsp;</td>
								</tr>
							</table>
						</td>
						<td class="text-center" valign="top">
							<table width="100%">
								<tr>
									<td class="text-right">&nbsp;</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td class="text-right">&nbsp;</td>
								</tr>
							</table>
						</td>
					</tr>
				<?php 
					$total = $total + $harga;
					$total = $total + $amount_permata;
				} 
				$ppn = $total*10/100;
				$total_harga = $total; //+$ppn;
				
				$harga_total = roundNearestHundredUp($harga_total);
				?>
				</tbody>
				<tfoot style="border-top:2px solid black;margin-bottom:10px;">
        			<tr>
        				<th colspan="3">&nbsp;</th>
        				<th class="text-right">Sub Total</th>
						<th class="text-right"><?php echo number_format($harga_total,0,',','.'); ?></th>
        			</tr>
        			<!--<tr>
        				<th colspan="3">&nbsp;</th>
        				<th class="text-right">PPN</th>
						<th class="text-right"><?php //echo number_format($ppn,0,',','.'); ?></th>
        			</tr>-->
        			<tr>
        				<th colspan="3">&nbsp;</th>
        				<th class="text-right">Total</th>
						<th class="text-right"><?php echo number_format($harga_total,0,',','.'); ?></th>
        			</tr>
        			<tr>
        				<th colspan="3">&nbsp;</th>
        				<th class="text-right">&nbsp;</th>
						<th class="text-right">&nbsp;</th>
        			</tr>
        			<tr>
        				<th colspan="3">&nbsp;</th>
        				<th class="text-right">Jumlah Bayar</th>
						<th class="text-right" style="padding-left:15px;">
						<input type="hidden" name="id_order" id="id_order" value="<?php echo $tmp->id_order; ?>">
						<input type="hidden" name="total_bayar" id="total_bayar" value="<?php echo $total_harga; ?>">
						<?php
						$id_bayar = "";
						$jumlah_bayar = 0;
						foreach($getDataCash->result() as $tmpCash){
							$id_bayar = $tmpCash->id;
							$jumlah_bayar = $tmpCash->jumlah;
						}
						?>
						<input type="hidden" name="id_bayar" id="id_bayar" value="<?php echo $id_bayar; ?>">
						<input type="hidden" name="jumlah_bayar" id="jumlah_bayar" value="<?php echo $jumlah_bayar; ?>">
						<input type="text" name="bayar" id="bayar" class="form-control text-right" placeholder="Jumlah Bayar" value="<?php echo number_format($jumlah_bayar,0,',','.'); ?>" onKeyUp="this.value=ThausandSeperator(this.value,2);" readonly>
						</th>
        			</tr>
        			<tr>
        				<th colspan="3">&nbsp;</th>
        				<th class="text-right">No. Order</th>
						<th class="text-right" style="padding-left:15px;">
						<input type="hidden" name="tmp_order_no" id="tmp_order_no" value="<?php echo $tmp->id_order; ?>">
						<input type="text" name="order_no" id="order_no" class="form-control" placeholder="No. Order" required>
						</th>
        			</tr>
        			<tr>
        				<th colspan="3">&nbsp;</th>
						<th colspan="2" class="text-right" style="padding-left:10px; vertical-align: middle;">
						<div class="col-md-12 text-right" style="margin-top:20px; padding-right: 0px; float: right;">
							<button type="button" class="btn btn-default" onclick="backToList()"><i class="fa fa-refresh"></i> BACK</button>
							<button type="submit" class="btn btn-default"><i class="fa fa-save"></i> AMBIL</button>
						</div>
						</th>
        			</tr>
				</tfoot>
			</table>
			</form>
              </div>
            </div>
          </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->