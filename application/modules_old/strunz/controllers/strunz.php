<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
* Controller Master Agama
* Create By : Hadi Setiawan
* 11 Mei 2016
*/
class strunz extends CI_Controller{
	public $table = "strunz";
	public $controls = "strunz";
	
	function __construct(){
		parent::__construct();

		if(!isLogin()){
			goLogin();
		}
	}
	
	public function index(){
		$data['filelist'] = $this->controls.'/front';
		$data['title'] = 'Master Lab Desk';
		$data['title_menu'] = 'Admin';
		$data['menu'] = 'strunz';
		$data['controls'] = $this->controls;
		$data['table'] = $this->table;

		getHTMLWeb($data);
	}

	public function addData(){
		$code = $this->input->post('code');
		$name = $this->input->post('nama');

		$data = array(
			'primary' => $code,
			'strunz' => $name,
			'create_by' => sessionValue('id'),
			'create_date' => date('Y-m-d H:i:s')	
			);
		$this->db->insert($this->table, $data);
		redirect(site_url($this->controls));
	}

	public function editData(){
		$id  = $this->uri->segment(3);

		$data['filelist'] = $this->controls.'/front';
		$data['title'] = 'Master Lab Desk';
		$data['title_menu'] = 'Admin';
		$data['menu'] = 'strunz';		
		$data['controls'] = $this->controls;
		$data['table'] = $this->table;
		
		$data['id'] = $id;

		getHTMLWeb($data);
	}

	public function updateData(){
		$id   = $this->input->post('id');
		$code = $this->input->post('code');
		$name = $this->input->post('nama');

		$data = array(
			'primary' => $code,
			'strunz' => $name,
			'update_by' => sessionValue('id'),
			'update_date' => date('Y-m-d H:i:s')
			);

		$this->db->where('id', $id);
		$this->db->update($this->table, $data);
		redirect(site_url($this->controls));
	}

	public function deleteData(){
		$id = $this->uri->segment(3);

		$data = array(
			'delete_by' => sessionValue('id'),
			'delete_date' => date('Y-m-d H:i:s')
			);
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);
		redirect(site_url($this->controls));
	}
}
?>