<?php
$this->load->library("terbilang");
$a = new Terbilang();

$id_order = str_replace('-', '/', $ord);

/*$data = $this->db->query("SELECT tb_fpjt.id, tb_fpjt.id_order, tb_fpjt.foto, tb_fpjt.jumlah, tb_fpjt.jumlah_permata, 
							master_permata.permata, master_jenis_permata.jenis jenis_perhiasan,
							color_stone.color
							FROM tb_fpjt 
						LEFT JOIN master_permata ON master_permata.id = tb_fpjt.jenis_permata
						LEFT JOIN master_jenis_permata ON master_jenis_permata.id = tb_fpjt.jenis_perhiasan
						LEFT JOIN color_stone ON color_stone.code = tb_fpjt.warna_permata
						LEFT JOIN step ON step.id_object = tb_fpjt.id
						WHERE tb_fpjt.id_order='$ord'
						AND step.print = 'Sudah'");*/
$data = $this->db->query("SELECT tb_taksiran.id, tb_taksiran.id_order, tb_taksiran.foto, tb_taksiran.berat_perhiasan, tb_taksiran.jumlah_perhiasan, tb_taksiran.jumlah_permata, tb_taksiran.keterangan, tb_taksiran.keterangan_berlian, tb_taksiran.berat_bersih, tb_taksiran.berat_kotor,
							master_permata.permata, master_jenis_permata.jenis, master_logam.jenis_logam, 
							color_stone.color, tb_taksiran.karatase_emas
							FROM tb_taksiran
						LEFT JOIN master_permata ON master_permata.id = tb_taksiran.jenis_permata
						LEFT JOIN master_jenis_permata ON master_jenis_permata.id = tb_taksiran.jenis_perhiasan
						LEFT JOIN color_stone ON color_stone.code = tb_taksiran.warna_permata
						LEFT JOIN master_logam ON tb_taksiran.jenis_logam = master_logam.id
						LEFT JOIN step ON step.id_object = tb_taksiran.id
						WHERE tb_taksiran.id_order='$id_order' AND !ISNULL(tb_taksiran.create_by)
						AND step.store='".sessionValue('kode_store')."'
						AND step.print = 'Sudah'");

$sub = 0;
$pdf = new FPDF('P','mm','A4');
$pdf->SetAutoPageBreak(false, 0);
$pdf->AddPage();

$pdf->SetFont('arial','B',16);
$pdf->SetXY(0,18);
$pdf->Cell(190,0,'Bukti Pembayaran Taksiran',0,0,'C');

$pdf->Image('./asset/images/Logo.png', 138, 2, 50, 30);

$pdf->SetFont('arial','B',10);
$pdf->SetXY(140, 29);
/*$pdf->Cell(0, 0, 'Pegadaian Gemological Laboratory');*/

$pdf->SetFont('arial','',10);
$pdf->SetXY(140, 33);
$pdf->Cell(0, 0, 'Jalan Kramat Raya No. 162, Jakarta');
$pdf->SetXY(140, 37);
$pdf->Cell(0, 0, 'Indonesia');

$pdf->SetXY(7, 45);
$pdf->Cell(0, 0, 'No. FPJT');
$pdf->SetXY(25, 45);
$pdf->Cell(0, 0, ':');
$pdf->SetXY(28, 45);
$pdf->Cell(0, 0, $id_order);

$dudate  = date_create(date('D, d-m-Y'));
$day  = date_format($dudate, "d");
$month  = date_format($dudate, "m");
$year  = date_format($dudate, "Y");

$pdf->SetXY(7, 49);
$pdf->Cell(0, 0, 'Tanggal');
$pdf->SetXY(25, 49);
$pdf->Cell(0, 0, ':');
$pdf->SetXY(28, 49);
$pdf->Cell(0, 0, $day."".bulan($month)." ".$year);

$pdf->Rect(110, 42, 90, 30,'D');

$width = array(50, 30, 35, 20, 40);
$pos_x = array(50, 35, 22.5, 22.5, 20, 40);
$pdf->SetFont('arial','',11);
//$pdf->Line(117.39, 64.5, 117.39, 72);
$pdf->SetY(77);
/*$pdf->SetFont('arial','',11);
$pdf->MultiCell(180, 5, "Sesuai dengan permintaan dari ".$nasabah." pada hari ".$hari." tanggal ".$tgl.", bersama ini Pegadaian G-LAB, alamat Jalan Kramat Raya Nomor 162 menyatakan bahwa perhiasan/barang yang diterangkan di bawah ini, setelah diuji dan ditaksir memiliki kualifikasi sebagai berikut :");
$pdf->Ln();

$pdf->SetFont('arial','B',11);
$pdf->Cell($width[0], 15, "Keterangan Barang", 'LBRT', 0, 'C');
$pdf->Cell($width[1], 15, "Jenis Logam", 'BRT', 0, 'C');
$pdf->Cell($width[3], 15, "Kadar(%)", 'BRT', 0, 'C');
$getX = $pdf->GetX();
$getY = $pdf->GetY();
$pdf->Cell($width[2], 7.5, "Berat (gram)", 'BRT', 0, 'C');
$getX2 = $pdf->GetX();
$getY2 = $pdf->GetY();
$pdf->Cell($width[3], 7.5, "Jenis", 'TR', 0, 'C');
$pdf->Cell($width[4], 15, "Ukuran, Kadar, DLL", 'BRT', 0, 'C');
$pdf->Ln();
$pdf->SetXY($getX, $getY+7.5);
$pdf->Cell($width[2]/2, 7.5, "Kotor", 'BR', 0, "C");
$pdf->Cell($width[2]/2, 7.5, "Bersih", 'BR', 0, "C");
$pdf->SetXY($getX2, $getY2+7.5);
$pdf->Cell($width[3], 7.5, "Permata", 'BR', 0, 'C');
$pdf->Ln();*/

$pdf->SetFont('arial','',10);

$cer = 0;
$mem = 0;
$dim = 0;
$no = 1;
$ong = 1;
$harga = 100000;
$harga_permata = 50000;
foreach($data->result_array() as $g){
	/*$x2 = $pdf->GetX();
	$y2 = $pdf->GetY();
	$pdf->MultiCell($width[0], 5, $g['jumlah_perhiasan'].' '.$g['jenis'].' '.$g['keterangan'], 'LR', 'L');
	$pdf->SetXY($x2+$width[0], $y2);
	$pdf->Cell($width[1], 5, $g['jenis_logam'], 'R', 0, 'C');
	$pdf->Cell($width[3], 5, number_format(($g['karatase_emas']/24)*100,2,',','.').' %', 'R', 0, 'R');
	$pdf->Cell($width[2]/2, 5, $g['berat_kotor'], 'R', 0, 'R');
	$pdf->Cell($width[2]/2, 5, $g['berat_bersih'], 'R', 0, 'R');
	$pdf->Cell($width[3], 5, $g['jenis'], 'R', 0, 'C');
	$pdf->Cell($width[4], 5, $g['karatase_emas'].' karat', 'R', 0, 'L');
	$pdf->Ln();
	$pdf->Cell($width[0], 7, '', 'LR', 0, 'L');	
	$pdf->Cell($width[1], 7, '', 'R', 0, 'C');
	$pdf->Cell($width[3], 7, '', 'R', 0, 'R');
	$pdf->Cell($width[2]/2, 7, '', 'R', 0, 'R');
	$pdf->Cell($width[2]/2, 7, '', 'R', 0, 'R');
	$pdf->Cell($width[3], 7, '', 'R', 0, 'C');
	$pdf->MultiCell($width[4], 7, $g['keterangan_berlian'], 'R', 'L');
	$pdf->Cell($width[0], 7, '', 'LR', 0, 'L');	
	$pdf->Cell($width[1], 7, '', 'R', 0, 'C');
	$pdf->Cell($width[3], 7, '', 'R', 0, 'R');
	$pdf->Cell($width[2]/2, 7, '', 'R', 0, 'R');
	$pdf->Cell($width[2]/2, 7, '', 'R', 0, 'R');
	$pdf->Cell($width[3], 7, '', 'R', 0, 'C');
	$pdf->Cell($width[4], 7, '', 'R', 0, 'C');
	$pdf->Ln();*/
}

$ppn = $sub*10/100;
$total = $sub+$ppn;

$pdf->SetFont('arial','',10);

/*$pdf->Cell($width[0], 7, '', 'T', 0, 'L');
$pdf->Cell($width[1], 7, '', 'T', 0, 'L');
$pdf->Cell($width[3], 7, '', 'T', 0, 'L');
$pdf->Cell($width[2], 7, '', 'T', 0, 'L');
$pdf->Cell($width[3], 7, '', 'T', 0, 'L');
$pdf->Cell($width[4], 7, '', 'T', 0, 'L');
$pdf->Ln(10);*/

$pdf->Cell(5, 7, "Biaya Pengujian :", 0, 0, "L");
$getHDLE = $this->db->query("SELECT * FROM master_harga_emas WHERE id='1'");
$hdle = 0;
foreach ($getHDLE->result() as $hsp) {
	$hdle = $hsp->harga_emas;
}
$getEmas = $this->db->query("SELECT * FROM tb_taksiran
							LEFT JOIN master_logam ON master_logam.id = tb_taksiran.jenis_logam
							WHERE tb_taksiran.id_order='$id_order' AND tb_taksiran.jenis_logam='1'");

$pdf->Ln();
$pdf->Cell(10, 7, " Emas :", 0, 0, "L");							
$pdf->Ln();
					
$harga_total = 0;
foreach ($getEmas->result() as $key) {
	$harga_taksiran = 1.25/100*(($key->berat_bersih*($key->karatase_emas/24))*$hdle); // $key->jumlah_perhiasan*
	
	$pdf->Cell(10, 7, "-  1,25% x ".$key->berat_bersih." gr x ".$key->karatase_emas."karat/24 x Rp. ".number_format($hdle, 0, ",", "."), 0, 0, "L");
	$pdf->Cell(148, 7, ": RP", 0, 0, "R");
	$pdf->Cell(20, 7, number_format($harga_taksiran, 0, ",", "."), 0, 0, "R");
	$pdf->Ln();
	
	$harga_total = $harga_total + $harga_taksiran;
} 


$pdf->Cell(10, 7, " Berlian :", 0, 0, "L");							
$pdf->Ln();

$getBerlian = $this->db->query("SELECT * FROM tb_taksiran
								LEFT JOIN master_logam ON master_logam.id = tb_taksiran.jenis_logam
								WHERE tb_taksiran.id_order='$id_order'");
								
$harga_berlian = 0;
$berat_total = 0;	
$gemolog = "";
foreach ($getBerlian->result() as $key) {
	if($key->berat_total_permata == ""){
		$berat_total = $berat_total + 0;
	}else{
		$berat_total = $berat_total + $key->berat_total_permata;
	}
	
	if($berat_total>0){
		$getHarga = $this->db->query("SELECT harga
									FROM `master_tarif_taksiran`
									WHERE berat2 >= ".$berat_total." ORDER BY ID ASC LIMIT 0,1");
									
		foreach ($getHarga->result() as $tmpHarga) {
			$harga_berlian = $harga_berlian + $tmpHarga->harga;
		}
	}
	$gemolog = $key->gemolog;
}
$harga_total = $harga_total + $harga_berlian;

$pdf->Cell(10, 7, "-  Ukuran total ".$berat_total." carat ", 0, 0, "L");
$pdf->Cell(148, 7, ": RP", 0, 0, "R");
$pdf->Cell(20, 7, number_format($harga_berlian, 0, ",", "."), 0, 0, "R");
$pdf->Ln();

$harga_total_terbilang = roundNearestHundredUp($harga_total);
	
$pdf->SetFont('arial','B',10);
$pdf->Cell(10, 7, "Jumlah Total", 0, 0, "L");
	$pdf->Cell(148, 7, ": RP", "T	", 0, "R");
	$pdf->Cell(20, 7, number_format($harga_total_terbilang, 0, ",", "."), "T", 0, "R");
	$pdf->Ln(10);
	$pdf->SetFont('arial','UI',10);
	$pdf->Cell(151, 7, "Dengan angka : ".ucwords($a->terbilang($harga_total_terbilang)), 0, 0, "L");
	$pdf->Ln(12);
$pdf->SetFont('arial','',10);
//$pdf->MultiCell(0, 5, "Taksiran harga emas didasarkan pada Harga Dasar Lelang Emas PT Pegadaian (Persero) pada tanggal pemeriksaan. Pengujian menggunakan kaidah-kaidah yang dipakai di PT Pegadaian (Persero) yaitu analisa kimia dan analisa berat jenis. Surat Keterangan Hasil Pengujian ini  bukan merupakan rekomendasi untuk dasar transaksi jual beli.");
//$pdf->Cell(180, 5, "Demikian disampaikan untuk dapat digunakan seperlunya.");

$query = $this->db->query("SELECT tb_member.first_name, tb_member.middle_name, tb_member.last_name, tb_member.corp_name, tb_member.kode, tb_member.address, tb_member.id_member, tb_member.postal_code,
							kecamatan.kecamatanNama kec1, kabupaten.kabupatenNama kota1, provinsi.provinsiNama prov1
							FROM tb_fpjt 
						LEFT JOIN tb_member ON tb_member.id_member = tb_fpjt.id_member
						LEFT JOIN kecamatan ON kecamatan.kecamatanId = tb_member.district
						LEFT JOIN kabupaten ON kabupaten.kabupatenId = tb_member.city
						LEFT JOIN provinsi ON provinsi.provinsiId = tb_member.province
						WHERE tb_fpjt.id_order='$id_order' LIMIT 0,1");
$nama = "";

$nasabah = "";
$penerima = sessionValue('nama');

$pdf->SetFont('arial','',10);
foreach($query->result() as $tmp){
	if($tmp->kode=="A"){
		$nama = $tmp->first_name." ".$tmp->middle_name." ".$tmp->last_name;
	}else{
		$nama = $tmp->corp_name;
	}
	
	$nasabah = $nama;
	
	$pdf->Ln(12);
	//$pdf->SetY(77);
	$width_bot = array(95,95,5);
	$pdf->Cell($width_bot[0], 7, 'Penerima,', '', 0, 'C');
	$pdf->Cell($width_bot[1], 7, 'Nasabah,', '', 0, 'C');
	$pdf->Ln(15);
	$pdf->Cell($width_bot[0], 5, '('.$penerima.')', '', 0, 'C');
	$pdf->Cell($width_bot[1], 5, '('.$nasabah.')', '', 0, 'C');
	$pdf->Ln();
	$pdf->Cell($width_bot[0], 5, '', '', 0, 'C');
	$pdf->Cell($width_bot[1], 5, '', '', 0, 'C');
	
	$pdf->SetXY(113, 47);
	$pdf->Cell(0, 0, $nama);
	$pdf->SetXY(113, 52);
	$pdf->Cell(0, 0, $tmp->address);
	$pdf->SetXY(113, 57);
	$pdf->Cell(0, 0, $tmp->kec1.' - '.str_replace('Kota ','', $tmp->kota1));
	$pdf->SetXY(113, 62);
	$pdf->Cell(0, 0, $tmp->postal_code);
	$pdf->SetXY(113, 67);
	$pdf->Cell(0, 0, $tmp->prov1);
		
	$new_ord = substr($id_order, -20, 9);
	$new_ord = str_replace("/", "-", $new_ord);
	$pdf->Image('./asset/barcode-fpjt/'.$tmp->id_member.$new_ord.'.gif', 10, 6, 35, 20);
}

$pdf->Output();
?>