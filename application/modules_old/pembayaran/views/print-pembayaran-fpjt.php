<?php
$terbilang = new Terbilang();
$ord = str_replace('-', '/', $ord);
$id_order = $ord;

$pdf = new FPDF('P','mm','A4');
$pdf->SetAutoPageBreak(false, 0);

$data = $this->db->query("SELECT tb_taksiran.id, tb_taksiran.id_order, tb_taksiran.foto, tb_taksiran.berat_perhiasan, tb_taksiran.jumlah_perhiasan, tb_taksiran.jumlah_permata, tb_taksiran.keterangan, tb_taksiran.keterangan_berlian, tb_taksiran.berat_bersih, tb_taksiran.berat_kotor,
							master_permata.permata, master_jenis_permata.jenis, master_logam.jenis_logam, 
							color_stone.color, tb_taksiran.karatase_emas, step.no_taksiran
							FROM tb_taksiran
						LEFT JOIN master_permata ON master_permata.id = tb_taksiran.jenis_permata
						LEFT JOIN master_jenis_permata ON master_jenis_permata.id = tb_taksiran.jenis_perhiasan
						LEFT JOIN color_stone ON color_stone.code = tb_taksiran.warna_permata
						LEFT JOIN master_logam ON tb_taksiran.jenis_logam = master_logam.id
						LEFT JOIN step ON step.id_object = tb_taksiran.id
						WHERE tb_taksiran.id_order='$id_order' AND !ISNULL(tb_taksiran.create_by)
						AND step.store='".sessionValue('kode_store')."'
						AND step.print = 'Sudah'");
						
$dataUser = $this->db->query("SELECT tb_member.first_name, tb_member.middle_name, tb_member.last_name
							FROM tb_fpjt 
						LEFT JOIN tb_member ON tb_member.id_member = tb_fpjt.id_member
						WHERE tb_fpjt.id_order='$id_order' LIMIT 0,1");
$nasabah = "";
foreach ($dataUser->result() as $get) {
	$nasabah = $get->first_name." ".$get->last_name;
}

$getHDLE = $this->db->query("SELECT * FROM master_harga_emas WHERE id='1'");
$hdle = 0;
foreach ($getHDLE->result() as $hsp) {
	$hdle = $hsp->harga_emas;
}

$getEmas = $this->db->query("SELECT * FROM tb_taksiran
							LEFT JOIN master_logam ON master_logam.id = tb_taksiran.jenis_logam
							WHERE tb_taksiran.id_order='$id_order' AND tb_taksiran.jenis_logam='1'");
					
$harga_total = 0;
foreach ($getEmas->result() as $key) {
	$harga_taksiran = 1.25/100*(($key->berat_bersih*($key->karatase_emas/24))*$hdle); // $key->jumlah_perhiasan*
	
	$harga_total = $harga_total + $harga_taksiran;
} 

$getBerlian = $this->db->query("SELECT * FROM tb_taksiran
								LEFT JOIN master_logam ON master_logam.id = tb_taksiran.jenis_logam
								WHERE tb_taksiran.id_order='$id_order'");
								
$harga_berlian = 0;
$berat_total = 0;	
foreach ($getBerlian->result() as $key) {
	if($key->berat_total_permata == ""){
		$berat_total = $berat_total + 0;
	}else{
		$berat_total = $berat_total + $key->berat_total_permata;
	}
	
	if($berat_total>0){
		$getHarga = $this->db->query("SELECT harga
									FROM `master_tarif_taksiran`
									WHERE berat2 >= ".$berat_total." ORDER BY ID ASC LIMIT 0,1");
									
		foreach ($getHarga->result() as $tmpHarga) {
			$harga_berlian = $harga_berlian + $tmpHarga->harga;
		}
	}
}

$harga_total = $harga_total + $harga_berlian;
$harga_total = roundNearestHundredUp($harga_total);

$batu_adi = "";
$sub = 0;
foreach($data->result_array() as $tmp){
	$dudate  = date_create($tmp['create_date']);
	$day  = date_format($dudate, "d");
	$month  = date_format($dudate, "m");
	$year  = date_format($dudate, "Y");
	
	$batu_adi = $tmp['no_taksiran'];
	
	if($tmp['certificate'] != ''){
		$sub = $sub+$pri_cer['harga'];
	}
	if($tmp['gem_card'] != ''){
		$sub = $sub+$pri_mem['harga'];
	}
	if($tmp['dia_grading'] != ''){		
		$getHarga = $this->db->query("SELECT harga
								FROM master_harga_grading
								WHERE berat2 >= ".$tmp['obj_weight']." ORDER BY ID ASC LIMIT 0,1");
		$harga_grading = 0;
		foreach ($getHarga->result() as $tmpHarga) {
			$harga_grading = $tmpHarga->harga;
		}
		
		$sub = $sub+$harga_grading;
	}
}

//if($batu_adi!=""){
//	$batu_adi = substr($batu_adi, 0, -2);
//}

$pdf->AddPage();

//$pdf->Line(5, 140, 205, 140);
$pdf->SetXY(5,140);
$pdf->SetFont('arial','',11);
$pdf->Cell(200,0,'-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  - ',0,0,'C');

for($i=0;$i<2;$i++){

$pdf->SetLineWidth(0);
$pdf->SetDrawColor(0, 0, 0);

if($i==0){
	$y_position = array(18, 12, 40, 9, 5, 32);
}else{
	$y_position = array(158, 152, 180, 149, 145, 172);
}

$pdf->SetFont('arial','B',16);
$pdf->SetXY(0,$y_position[0]);
$pdf->Cell(210,0,'Bukti Pembayaran',0,0,'C');

$pdf->Rect(160, $y_position[3], 38, 17,'D');

$pdf->SetFont('arial','I',10);
$pdf->SetXY(165, $y_position[1]);
$pdf->Cell(35, 5, 'Lembar');
$pdf->Ln();
$pdf->SetX(165);

if($i==0){
$pdf->Cell(35, 5, 'Untuk Nasabah');
}else{
$pdf->Cell(35, 5, 'Untuk Kasir');
}

$penerima = sessionValue('nama');

$width = array(40, 10, 135, 80);
$height = 10;
$pdf->SetFont('arial','',12);
$pdf->SetXY(20,$y_position[2]);
$pdf->Cell($width[0], $height, 'No. Invoice');
$pdf->Cell($width[1], $height, ':', '', '', 'C');
$pdf->Cell($width[2], $height, $ord);
$pdf->Ln();

$pdf->SetX(20);
$pdf->Cell($width[0], $height, 'Sudah diterima dari');
$pdf->Cell($width[1], $height, ':', '', '', 'C');
$pdf->Cell($width[2], $height, $nasabah);
$pdf->Ln();

$pdf->SetX(20);
$pdf->Cell($width[0], $height, 'Untuk pembayaran');
$pdf->Cell($width[1], $height, ':', '', '', 'C');
$pdf->MultiCell($width[2], $height, 'Jasa Taksiran Nomor : '.$batu_adi);
//$pdf->Ln();

/*$pdf->SetX(20);
$pdf->Cell($width[0], $height, '');
$pdf->Cell($width[1], $height, '', '', '', 'C');
$pdf->Cell($width[2], $height, '');
$pdf->Ln();*/

$pdf->SetX(20);
$pdf->Cell($width[0], $height, 'Uang sejumlah');
$pdf->Cell($width[1], $height, ':', '', '', 'C');
$pdf->Cell($width[3], $height, 'Rp. '.number_format($harga_total,0,',','.').',-', 'LBRT', '', 'C');
$pdf->Ln();

$pdf->SetX(20);
$pdf->Cell($width[0], $height, 'Terbilang');
$pdf->Cell($width[1], $height, ':', '', '', 'C');
$pdf->Cell($width[2], $height, '#'.ucfirst($terbilang->terbilang($harga_total)).' rupiah#');
$pdf->Ln();

$pdf->SetX(20);
$pdf->Cell($width[0], $height, 'Harga sudah termasuk PPN 10%');
$pdf->Ln(15);

$width_bot = array(110,95,5);
//$pdf->SetY(160);
$pdf->Cell($width_bot[0], 7, '', '', 0, 'C');
$pdf->Cell($width_bot[1], 7, 'Jakarta, '.$day."".bulan($month)." ".$year, '', 0, 'C');
$pdf->Ln();
$pdf->Cell($width_bot[0], 5, '', '', 0, 'C');
$pdf->Cell($width_bot[1], 5, 'Kasir Pegadaian Glab', '', 0, 'C');
$pdf->Ln(15);
$y_akhir = $pdf->GetY();
$pdf->Cell($width_bot[0], 5, '', '', 0, 'C');
$pdf->Cell($width_bot[1], 5, $penerima, '', 0, 'C');
$pdf->Ln();

$pdf->SetXY(15,$y_akhir-5);
$pdf->SetFont('arial','I',12);
$pdf->Cell($width_bot[0], 5, '"Di mata kami, tiap butir permata', '', 0, 'L');
$pdf->Ln();
$pdf->SetX(15);
$pdf->Cell($width_bot[0], 5, 'takkan pernah sama!"', '', 0, 'L');

// LOGO
$pdf->Image('./asset/images/Logo.png', 3, $y_position[4], 40, 26);
$pdf->SetLineWidth(1);
$pdf->SetDrawColor(130, 186, 83);
$pdf->Line(5, $y_position[5], 105, $y_position[5]);
$pdf->SetDrawColor(16,95,62);
$pdf->Line(105, $y_position[5], 205, $y_position[5]);
}

$pdf->Output();
?>