<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
* Controller Master Cut
* Create By : Erwin Idamsyach Putra
* 23 April 2016
*/
class cut extends CI_Controller{
	function __construct(){
		parent::__construct();

		if(!isLogin()){
			goLogin();
		}
	}
	
	public function index(){
		$data['filelist'] = 'cut/front';
		$data['title'] = 'Master Lab Desk';
		$data['title_menu'] = 'Admin';
		$data['menu'] = 'cut';

		getHTMLWeb($data);
	}

	public function add_cut(){
		$code = $this->input->post('code');
		$name = $this->input->post('cut');

		$data = array(
			'code' => $code,
			'cut' => $name,
			'create_by' => sessionValue('nama'),
			'create_date' => date('Y-m-d')	
			);
		$this->db->insert('cut', $data);
		redirect(site_url('cut'));
	}

	public function edit_cut(){
		$id  = $this->uri->segment(3);

		$data['filelist'] = 'cut/front';
		$data['title'] = 'Master Lab Desk';
		$data['title_menu'] = 'Admin';
		$data['menu'] = 'cut';
		$data['id_cut'] = $id;

		getHTMLWeb($data);
	}

	public function update_cut(){
		$id   = $this->input->post('id');
		$code = $this->input->post('code');
		$name = $this->input->post('cut');

		$data = array(
			'code' => $code,
			'cut' => $name,
			'update_by' => sessionValue('nama'),
			'update_date' => date('Y-m-d')
			);

		$this->db->where('id', $id);
		$this->db->update('cut', $data);
		redirect(site_url('cut'));
	}

	public function delete_cut(){
		$id = $this->uri->segment(3);

		$data = array(
			'delete_by' => sessionValue('nama'),
			'delete_date' => date('Y-m-d')
			);
		$this->db->where('id', $id);
		$this->db->update('cut', $data);
		redirect(site_url('cut'));
	}
}
?>