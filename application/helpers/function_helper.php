<?php 
function getHTMLWeb($data=null){
	$CI =& get_instance();
	
	$CI->load->view('skin/header', $data);
	$CI->load->view('skin/menu');
	$CI->load->view('skin/body');
	$CI->load->view('skin/footer');
}

function goLogin(){
	$data["filelist"] = "login/login";
	$data["title"] = "COG Laboratory";
	getHTMLWeb($data);
}

function get_the_current_url() {
    $protocol = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http");
    $base_url = $protocol . "://" . $_SERVER['HTTP_HOST'];
    $complete_url = $base_url . $_SERVER["REQUEST_URI"];
    return $complete_url;
}

function checkSession($sessName=null){
	$CI =& get_instance();
	
	if(trim($sessName)=="")
		return $CI->session->userdata();
	else 
		return $CI->session->userdata("$sessName");
}

function isLogin(){
	if(checkSession("username")=="")
		return FALSE;
	else
		return TRUE;
}

function escapeStr($str){
	$CI =& get_instance();
	
	return $CI->db->escape_str($str);
}

function postInput($str){
	$CI =& get_instance();
	
	return $CI->input->post("$str");
}

function loadModel($model){
	$CI =& get_instance();
	
	if(is_array($model)){
		foreach ($model as $each){
			loadModel($each);
		}
	}else{	
		return $CI->load->model("$model");
	}
}

function loadLibrary($library){
	$CI =& get_instance();
	
	if(is_array($library)){
		foreach ($library as $each){
			loadLibrary($each);
		}
	}else{	
		return $CI->load->library("$library");
	}
}

function loadView($view,$data=null){
	$CI =& get_instance();
	
	if(is_array($view)){
		foreach ($view as $each){
			loadView($each,$data);
		}
	}else{	
		return $CI->load->view("$view",$data);
	}
}

function getLastId(){
	$CI =& get_instance();
	return $CI->db->insert_id();
}

function setSession($arrayUserData){
	$CI =& get_instance();
	
	return $CI->session->set_userdata($arrayUserData);
}

function sessionValue($sessionName){
	$CI =& get_instance();
	
	return $CI->session->userdata("$sessionName");
}

function ajaxProcessStatus($grid=null,$text="Loading..."){
	$str = "<td colspan='100'>$text</td> />";
	if(trim($grid)==""){
		$str = "<div class='row-fluid'><div class='span12' style='text-align: center;'>$text</div></div>";
	}
	
	return $str;
}

function mysqldatetime(){
	$CI =& get_instance();
	$datestring = "%Y-%m-%d %H:%i:%s";
	$time = time();
	return mdate($datestring, $time);
}

function getSeachQuery($table,$keyword){
    $CI =& get_instance();
    $fields = $CI->db->list_fields($table);
    $likeQuery = "";
    foreach ($fields as $field)
    {
       $likeQuery.="LOWER(".$table.".".$field.") LIKE LOWER('%".$keyword."%') OR ";
    }
    $likeQuery.="false";
    return $likeQuery;
}

function getSeachFormArray($fields,$keyword){
	$likeQuery = "";
    foreach ($fields as $field)
    {
       $likeQuery.="LOWER(".$field.") LIKE LOWER('%".$keyword."%') OR ";
    }
    $likeQuery.="false";
    return $likeQuery;
}

function hari($hari)
{
    switch ($hari)
    {
        case 7: $hari = "Minggu";
                return $hari;
                break;
        case 1: $hari = "Senin";
                return $hari;
                break;
        case 2: $hari = "Selasa";
                return $hari;
                break;
        case 3: $hari = "Rabu";
                return $hari;
                break;
        case 4: $hari = "Kamis";
                return $hari;
                break;
        case 5: $hari = "Jum'at";
                return $hari;
                break;
        case 6: $hari = "Sabtu";
                return $hari;
                break;
    }
}
 
function bulan($bulan)
{
   switch ($bulan)
   {
        case "01" : $bulan = " Januari";
                return $bulan;
                break;
        case "02" : $bulan = " Februari";
                return $bulan;
                break;
        case "03" : $bulan = " Maret";
                  return $bulan;
                  break;
        case "04" : $bulan = " April";
                  return $bulan;
                  break;
        case "05" : $bulan = " Mei";
                  return $bulan;
                  break;
        case "06" : $bulan = " Juni";
                  return $bulan;
                  break;
        case "07" : $bulan = " Juli";
                  return $bulan;
                  break;
        case "08" : $bulan = " Agustus";
                  return $bulan;
                  break;
        case "09" : $bulan = " September";
                  return $bulan;
                  break;
        case "10" : $bulan = " Oktober";
                  return $bulan;
                  break;
        case "11" : $bulan = " November";
                  return $bulan;
                  break;
        case "12" : $bulan = " Desember";
                  return $bulan;
                  break;
    }
}
 
function hari_tanggalindo($hari,$tanggal,$bulan,$tahun)
{
   $hari_sekarang = hari($hari);
   $bln_sekarang = bulan($bulan);
   return $hari_sekarang.", ".$tanggal." ".$bln_sekarang." ".$tahun;
}
 
function tanggal_indo($tanggal)
{
  $hari = substr($tanggal,6,2);
  $bulan = substr($tanggal,4,2);
  $tahun = substr($tanggal,0,4);
  return  "$hari ".bulan($bulan)." $tahun";
}

function email_pull() {
    $CI =& get_instance();
    
    // load the Email_reader library from previous post
    $CI->load->library('email_reader');

    // this method is run on a cronjob and should process all emails in the inbox
    while (1) {
        // get an email
        $email = $CI->email_reader->get();

        // if there are no emails, jump out
        if (count($email) <= 0) {
            break;
        }

        $attachments = array();
        // check for attachments
        if (isset($email['structure']->parts) && count($email['structure']->parts)) {
            // loop through all attachments
            for ($i = 0; $i < count($email['structure']->parts); $i++) {
                // set up an empty attachment
                $attachments[$i] = array(
                    'is_attachment' => FALSE,
                    'filename'      => '',
                    'name'          => '',
                    'attachment'    => ''
                );

                // if this attachment has idfparameters, then proceed
                if ($email['structure']->parts[$i]->ifdparameters) {
                    foreach ($email['structure']->parts[$i]->dparameters as $object) {
                        // if this attachment is a file, mark the attachment and filename
                        if (strtolower($object->attribute) == 'filename') {
                            $attachments[$i]['is_attachment'] = TRUE;
                            $attachments[$i]['filename']      = $object->value;
                        }
                    }
                }

                // if this attachment has ifparameters, then proceed as above
                if ($email['structure']->parts[$i]->ifparameters) {
                    foreach ($email['structure']->parts[$i]->parameters as $object) {
                        if (strtolower($object->attribute) == 'name') {
                            $attachments[$i]['is_attachment'] = TRUE;
                            $attachments[$i]['name']          = $object->value;
                        }
                    }
                }

                // if we found a valid attachment for this 'part' of the email, process the attachment
                if ($attachments[$i]['is_attachment']) {
                    // get the content of the attachment
                    $attachments[$i]['attachment'] = imap_fetchbody($CI->email_reader->conn, $email['index'], $i+1);

                    // check if this is base64 encoding
                    if ($email['structure']->parts[$i]->encoding == 3) { // 3 = BASE64
                        $attachments[$i]['attachment'] = base64_decode($attachments[$i]['attachment']);
                    }
                    // otherwise, check if this is "quoted-printable" format
                    elseif ($email['structure']->parts[$i]->encoding == 4) { // 4 = QUOTED-PRINTABLE
                        $attachments[$i]['attachment'] = quoted_printable_decode($attachments[$i]['attachment']);
                    }
                }
            }
        }

        // for My Slow Low, check if I found an image attachment
        $found_img = FALSE;
        foreach ($attachments as $a) {
            if ($a['is_attachment'] == 1) {
                // get information on the file
                $finfo = pathinfo($a['filename']);

                // check if the file is a jpg, png, or gif
                if (preg_match('/(jpg|gif|png)/i', $finfo['extension'], $n)) {
                    $found_img = TRUE;
                    // process the image (save, resize, crop, etc.)
                    $fname = $n[1];

                    break;
                }
            }
        }

        // if there was no image, move the email to the Rejected folder on the server
        if ( ! $found_img) {
            $CI->email_reader->move($email['index'], 'INBOX.Rejected');
            continue;
        }

        // get content from the email that I want to store
        $addr   = $email['header']->from[0]->mailbox."@".$email['header']->from[0]->host;
        $sender = $email['header']->from[0]->mailbox;
        $text   = ( ! empty($email['header']->subject) ? $email['header']->subject : '');

        // move the email to Processed folder on the server
        $CI->email_reader->move($email['index'], 'INBOX.Processed');

        $query = $CI->db->query("SELECT * FROM get_mail WHERE description = '$text'");
        if($query->num_rows == 0){
            // add the data to the database
            $data = array(
                'username'    => $sender,
                'email'       => $addr,
                'photo'       => $fname,
                'description' => ($text == '' ? NULL : $text)
            );
            $CI->db->insert("get_mail",$data);
        }
        // don't slam the server
        sleep(1);
        die();
    }

    // close the connection to the IMAP server
    $CI->email_reader->close();
}

// Get content web
function GetBetween($content,$start,$end){
    $r = explode($start, $content);
    if (isset($r[1])){
        $r = explode($end, $r[1]);
        return $r[0];
    }
    return '';
}
// End get content web

function getVisitor($url){
		$CI =& get_instance();
		
		$query_visit = $CI->db->query("SELECT * FROM page_visitor WHERE PAGE='".$url."'"); // $this->uri->uri_string()
		$run_query_visit = $query_visit->result_array();
		if($query_visit->num_rows()>0){
			foreach($run_query_visit as $tmpVisit){
				$dataVisit=array(
                    'VISITOR' => $tmpVisit['VISITOR'] + 1
                );
				$CI->db->where('ID',$tmpVisit['ID']);
				$result =$CI->db->update('page_visitor',$dataVisit);
			}
		}else{
			$dataUrl = substr($url,-3);
			if(strtoupper($dataUrl)!="PNG"){
			$dataVisit=array(
                    'PAGE' => $url,
                    'VISITOR' => 1
            );
			$result =$CI->db->insert('page_visitor',$dataVisit);
			}
		}
}

function getCountNews($option=NULL){
    $CI =& get_instance();
    
    $query = $CI->db->query('SELECT COUNT(ID) JUMLAH FROM news WHERE FLAG="APPROVED" AND (PAGE="1" OR ISNULL(PAGE)) '.$option.' ');
    return $query;
}

function getCountSubTopik($option=NULL){
    $CI =& get_instance();
    
    $query = $CI->db->query('SELECT COUNT(ID) JUMLAH FROM sub_menus WHERE FLAG="RELEASED" '.$option.' ');
    return $query;
}

function getBanner($option=NULL){
    $CI =& get_instance();
    
    $query = $CI->db->query('SELECT * FROM banner WHERE FLAG="APPROVED" '.$option.' ');
    return $query;
}

function getIklan($option=NULL){
    $CI =& get_instance();
    
    $query = $CI->db->query('SELECT * FROM iklan WHERE FLAG!="DELETED" AND FLAG_APPROVE="APPROVED" '.$option.' ');
    return $query;
}

function fb_comment_count($url)
{
	$json = json_decode(file_get_contents('https://graph.facebook.com/?ids=' . $url));
	return isset($json->$url->comments) ? $json->$url->comments : 0;
}

function getExtension($str) {

         $i = strrpos($str,".");
         if (!$i) { return ""; } 

         $l = strlen($str) - $i;
         $ext = substr($str,$i+1,$l);
         return $ext;
 }
 
// Hadi
function convertToNumber($number){
	$tmp = explode(",", $number);
	return str_replace(".", "", $tmp[0]);
}

function generateCodeCIF($code){
	if ($code > 999999){
        $value = "CIF".($code+1);
    }else if ($code > 99999){
        $value = "CIF0".($code+1);
    }else if ($code > 9999){
		$value = "CIF00".($code+1);
	}else if ($code > 999){
		$value = "CIF000".($code+1);
	}else if ($code > 99){
		$value = "CIF0000".($code+1);
	}else if ($code > 9){
		$value = "CIF00000".($code+1);
	}else{
		$value = "CIF000000".($code+1);
	}
	
	return $value;
}

function generateCodeCOR($code){
	if ($code > 999999){
        $value = "COR".($code+1);
    }else if ($code > 99999){
        $value = "COR0".($code+1);
    }else if ($code > 9999){
		$value = "COR00".($code+1);
	}else if ($code > 999){
		$value = "COR000".($code+1);
	}else if ($code > 99){
		$value = "COR0000".($code+1);
	}else if ($code > 9){
		$value = "COR00000".($code+1);
	}else{
		$value = "COR000000".($code+1);
	}
	
	return $value;
}

function generateNoReport($prefix, $nomor){
    //$nomor = $nomor+1;
    if($nomor < 10){
        $id = $prefix."00000".$nomor;
    }else if($nomor < 100){
        $id = $prefix."0000".$nomor;
    }else if($nomor < 1000){
        $id = $prefix."000".$nomor;
    }else if($nomor < 10000){
        $id = $prefix."00".$nomor;
    }else if($nomor < 100000){
        $id = $prefix."0".$nomor;
    }else{
        $id = $prefix.$nomor;
    }
	
	$id = $id."-".rand(1, 9);

    return $id;
}

function setQarcode($folder,$kode=0, $data){
	$CI =& get_instance();
	$CI->load->library('ciqrcode');
	
	$params['data'] = $data;
	$params['level'] = 'H';
	$params['size'] = 5;
	$params['savename'] = FCPATH.'asset/'.$folder.'/'.$kode.'.png';
	$CI->ciqrcode->generate($params);
}

function roundNearestHundredUp($number){
	return ceil( $number / 100 ) * 100;
}