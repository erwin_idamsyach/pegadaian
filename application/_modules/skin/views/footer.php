<?php if(isLogin()){ ?>
	  <footer class="main-footer">
        <strong>Copyright &copy; <?php echo date('Y') ?> <a href="http://icsp.co.id">ICSP</a>.</strong>
      </footer>

      <!-- Control Sidebar -->
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo base_url();?>asset/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>asset/plugins/dropify-master/dist/js/dropify.min.js"></script>
    <script>
      $(document).ready(function(){
        $(".btn").tooltip();
        $(".drop").dropify();
      });
      function submitImage(){
        $( "#frmuploadImg" ).submit();
      }
    </script>
    <!-- Morris.js charts -->
    
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url();?>asset/plugins/dist/js/demo.js"></script>
    <script src="<?php echo base_url();?>asset/plugins/dist/js/app.min.js"></script>
	
  </body>
</html>
<?php }?>
</body>
</html>