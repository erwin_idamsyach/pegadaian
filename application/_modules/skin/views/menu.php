
<?php if(isLogin()){ ?>
<body class="hold-transition skin-blue sidebar-mini"><!-- onload="open_on_entrance()" -->
	<script type="text/javascript">
        function chagePasswordLogin(){
			$('#modalChangePassword-body').load(base_url+"profile/changePassword",function(result){
				$('#modalChangePassword').modal({show:true});
			});
		}
	</script>	
	<!-- Modal -->
		<div id="modalChangePassword" class="modal fade" role="dialog">
		  <div class="modal-dialog  modal-sm">

			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">&nbsp;</h4>
			  </div>
			  <div id="modalChangePassword-body">
				<p>&nbsp;</p>
			  </div>
			</div>

		  </div>
		</div>
	<!-- End Modal -->

    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="<?php base_url(); ?>" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->

          <span class="logo-mini">    
            <img src="<?php echo base_url();?>asset/images/LOGO-COG-FINAL.png" height="35px" style="margin-top: 10px;">
          </span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg text-left">
            <img src="<?php echo base_url();?>asset/images/LOGO-COG-FINAL.png" height="35px">
            <b><?php echo ($title_menu=="Gemologist")?"Lab":$title_menu;?></b>Desks
          </span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
              <!-- Notifications: style can be found in dropdown.less -->
              <!-- User Account: style can be found in dropdown.less -->
              <!-- Control Sidebar Toggle Button -->
              <!--<li>
                <a href="<?php //echo site_url('logout/logout');?>"><i class="fa fa-sign-out"></i> Logout</a>
              </li>-->
              <li><a href="#" onclick="chagePasswordLogin()"><i class="fa fa-pencil"></i> Change Password</a></li>
              <li>
                <a href="#">
                  <?php
                  $tanggal = date("D, d-M-Y");
                  echo $tanggal;
                  ?>
                </a>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?php echo base_url();?>/asset/images/User-icon.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p>Hello, <?php echo sessionValue('nama'); ?></p>
			  <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <ul class="sidebar-menu">
            <li class="header">NAVIGASI</li>
			
            <li <?php echo ($menu=="home")?"class='active'":"";?>>
              <a href="<?php echo site_url('home') ?>">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span></i>
              </a>
            </li>
            <?php
            $acc = sessionValue('access_level');
            if($acc == "Front_Desk"){
              ?>
            <!-- <li <?php echo ($menu=="reg_customer" || $menu=="corporate")?"class='treeview active'":"class='treeview'";?>>
              <a href="#">
                <i class="fa fa-user-plus"></i>
                <span>Reg. Customer</span>
                <div class="pull-right">
                  <i class="fa fa-chevron-right right-caret"></i>
                </div>
              </a>
              <ul class="treeview-menu">
                <li <?php echo ($menu=="reg_customer")?"class='treeview active'":"";?>><a href="<?php echo site_url('reg_customer') ?>"><i class="fa fa-circle-o"></i>Reg. Customer Individual</a></li>
                <li <?php echo ($menu=="corporate")?"class='treeview active'":"";?>><a href="<?php echo site_url('corporate') ?>"><i class="fa fa-circle-o"></i>Reg. Customer Corporate</a></li>
              </ul>
            </li> -->
            <li <?php echo ($menu=="tambahcif"||$menu=="listcif")?"class='treeview active'":"class='treeview'" ?>>
              <a href="#">
                <i class="fa fa-user-plus"></i> <span>Reg. CIF</span>
                <div class="pull-right">
                  <i class="fa fa-chevron-right"></i>
                </div>
              </a>
              <ul class="treeview-menu">
                <li <?php echo ($menu=="tambahcif")?"class='active'":"" ?>>
                  <a href="<?php echo site_url('tambahcif') ?>">
                    <i class="fa fa-circle-o"></i> Add CIF
                  </a>
                </li>
                <li <?php echo ($menu=="listcif")?"class='active'":"" ?>>
                  <a href="<?php echo site_url('listcif') ?>">
                    <i class="fa fa-circle-o"></i> List CIF
                  </a>
                </li>
              </ul>
            </li>
            <li <?php echo ($menu=="input_object"||$menu=="list_fpjs")?"class='treeview active'":"class='treeview'";?>>
              <a href="#">
                <i class="fa fa-diamond"></i> <span>Reg. BPS</span>
                <div class="pull-right">
                  <i class="fa fa-chevron-right right-caret"></i>
                </div>
              </a>
              <ul class="treeview-menu">
                <li <?php echo ($menu=="input_object")?"class='active'":"" ?>>
                  <a href="<?php echo site_url('obj_input') ?>">
                    <i class="fa fa-circle-o"></i> Add BPS
                  </a>
                </li>
                <li <?php echo ($menu=="list_fpjs")?"class='active'":"" ?>>
                  <a href="<?php echo site_url('list_fpjs') ?>">
                    <i class="fa fa-circle-o"></i> List BPS
                  </a>
                </li>
              </ul>
            </li>
            <li <?php echo ($menu=="add-fpjt"||$menu=="list_fpjt")?"class='treeview active'":"class='treeview'" ?>>
              <a href="#">
                <i class="fa fa-diamond"></i> <span>Reg. BPT</span>
                <div class="pull-right">
                  <i class="fa fa-chevron-right right-caret"></i>
                </div>
              </a>
              <ul class="treeview-menu">
                <li <?php echo ($menu=='add-fpjt')?"class='active'":"" ?>>
                  <a href="<?php echo site_url('add_fpjt') ?>">
                    <i class="fa fa-circle-o"></i> Add BPT
                  </a>
                </li>
                <li <?php echo ($menu=='list_fpjt')?"class='active'":"" ?>>
                  <a href="<?php echo site_url('list_fpjt') ?>">
                    <i class="fa fa-circle-o"></i> List BPT
                  </a>
                </li>
              </ul>
            </li>     
            <li <?php echo ($menu=="invoice"||$menu=="invoice_fpjt")?"class='treeview active'":"class='treeview'" ?>>
              <a href="#">
                <i class="fa fa-print"></i> <span>Cetak Invoice</span>
                <div class="pull-right">
                  <i class="fa fa-chevron-right right-caret"></i>
                </div>
              </a>
              <ul class="treeview-menu">
                <li <?php echo ($menu=='invoice')?"class='active'":"" ?>>
                  <a href="<?php echo site_url('invoice') ?>">
                    <i class="fa fa-circle-o"></i> BPS
                  </a>
                </li>
                <li <?php echo ($menu=='invoice_fpjt')?"class='active'":"" ?>>
                  <a href="<?php echo site_url('invoice/fpjt') ?>">
                    <i class="fa fa-circle-o"></i> BPT
                  </a>
                </li>
              </ul>
            </li>    
            <li <?php echo ($menu=="pembayaran"||$menu=="pembayaran_fpjt")?"class='treeview active'":"class='treeview'" ?>>
              <a href="#">
                <i class="fa fa-money"></i> <span>Pembayaran</span>
                <div class="pull-right">
                  <i class="fa fa-chevron-right right-caret"></i>
                </div>
              </a>
              <ul class="treeview-menu">
                <li <?php echo ($menu=='pembayaran')?"class='active'":"" ?>>
                  <a href="<?php echo site_url('pembayaran') ?>">
                    <i class="fa fa-circle-o"></i> BPS
                  </a>
                </li>
                <li <?php echo ($menu=='pembayaran_fpjt')?"class='active'":"" ?>>
                  <a href="<?php echo site_url('pembayaran/fpjt') ?>">
                    <i class="fa fa-circle-o"></i> BPT
                  </a>
                </li>
              </ul>
            </li>
			<li <?php echo ($menu=="pengambilan"||$menu=="pengambilan_fpjt")?"class='treeview active'":"class='treeview'" ?>>
              <a href="#">
                <i class="fa fa-check"></i> <span>Pengambilan Barang</span>
                <div class="pull-right">
                  <i class="fa fa-chevron-right right-caret"></i>
                </div>
              </a>
              <ul class="treeview-menu">
                <li <?php echo ($menu=='pengambilan')?"class='active'":"" ?>>
                  <a href="<?php echo site_url('pengambilan') ?>">
                    <i class="fa fa-circle-o"></i> BPS
                  </a>
                </li>
                <li <?php echo ($menu=='pengambilan_fpjt')?"class='active'":"" ?>>
                  <a href="<?php echo site_url('pengambilan/fpjt') ?>">
                    <i class="fa fa-circle-o"></i> BPT
                  </a>
                </li>
              </ul>
            </li>
              <li <?php echo ($menu=="Brief Report" || $menu=="Full Report" || $menu=="Diamond Grading" || $menu=="Taksiran" || $menu=="Rekapitulasi" || $menu=="Barang Tertinggal" || $menu=="Report Gemstone")?"class='treeview active'":"class='treeview'";?>>
                <a href="#">
                  <i class="fa fa-file-pdf-o"></i> <span>Report</span>
                  <div class="pull-right">
                    <i class="fa fa-chevron-right right-caret"></i>
                  </div>
                </a>
                <ul class="treeview-menu">
                  <li <?php echo ($menu=="Brief Report")?"class='active'":"";?>>
                    <a href="<?php echo site_url('report') ?>">
                      <i class="fa fa-circle-o"></i> Brief Report
                    </a>
                  </li>
                  <li <?php echo ($menu=="Full Report")?"class='active'":"";?>>
                    <a href="<?php echo site_url('report/full_report') ?>">
                      <i class="fa fa-circle-o"></i> Identification Report
                    </a>
                  </li>
                  <li <?php echo ($menu=="Diamond Grading")?"class='active'":"";?>>
                    <a href="<?php echo site_url('report/grading') ?>">
                      <i class="fa fa-circle-o"></i> Diamond Grading
                    </a>
                  </li>
                  <li <?php echo ($menu=="Taksiran")?"class='active'":"";?>>
                    <a href="<?php echo site_url('report/taksiran') ?>">
                      <i class="fa fa-circle-o"></i> Taksiran
                    </a>
                  </li>
                  <li <?php echo ($menu=="Rekapitulasi")?"class='active'":"";?>>
                    <a href="<?php echo site_url('report/rekapitulasi') ?>">
                      <i class="fa fa-circle-o"></i> Rekapitulasi
                    </a>
                  </li>
                  <li <?php echo ($menu=="Barang Tertinggal")?"class='active'":"";?>>
                    <a href="<?php echo site_url('report/barangtertinggal') ?>">
                      <i class="fa fa-circle-o"></i> Barang Tertinggal
                    </a>
                  </li>
                  <li <?php echo ($menu=="Report Gemstone")?"class='active'":"";?>>
                    <a href="<?php echo site_url('report/gemstone') ?>">
                      <i class="fa fa-circle-o"></i> Gemstone
                    </a>
                  </li>
                </ul>
              </li>      
              <?php
            }else if($acc == "Lab_Desk" || $acc == "Gemologist"){
              ?>
			<li <?php echo ($menu=="sertifikasi"||$menu=="grading"||$menu=="taksiran")?"class='treeview active'":"class='treeview'";?>>
              <a href="#">
                <i class="fa fa-diamond"></i> <span>Daftar Permintaan</span>
                <div class="pull-right">
                  <i class="fa fa-chevron-right right-caret"></i>
                </div>
              </a>
              <ul class="treeview-menu">
                <li <?php echo ($menu=="sertifikasi")?"class='active'":"" ?>>
                  <a href="<?php echo site_url('object/sertifikasi') ?>">
                    <i class="fa fa-circle-o"></i> Sertifikasi
                  </a>
                </li>
                <li <?php echo ($menu=="grading")?"class='active'":"" ?>>
                  <a href="<?php echo site_url('object/grading') ?>">
                    <i class="fa fa-circle-o"></i> Diamond Grading
                  </a>
                </li>
                <li <?php echo ($menu=="taksiran")?"class='active'":"" ?>>
                  <a href="<?php echo site_url('object/taksiran') ?>">
                    <i class="fa fa-circle-o"></i> Taksiran
                  </a>
                </li>
              </ul>
            </li>
      <li <?php echo ($menu=="finish_sertifikasi"||$menu=="finish_grading"||$menu=="finish_taksiran")?"class='treeview active'":"class='treeview'";?>>
              <a href="#">
                <i class="fa fa-flag"></i> <span>Daftar Permintaan Selesai</span>
                <div class="pull-right">
                  <i class="fa fa-chevron-right right-caret"></i>
                </div>
              </a>
              <ul class="treeview-menu">
                <li <?php echo ($menu=="finish_sertifikasi")?"class='active'":"" ?>>
                  <a href="<?php echo site_url('obj_finish') ?>">
                    <i class="fa fa-circle-o"></i> Sertifikasi
                  </a>
                </li>
                <li <?php echo ($menu=="finish_grading")?"class='active'":"" ?>>
                  <a href="<?php echo site_url('obj_finish/grading') ?>">
                    <i class="fa fa-circle-o"></i> Diamond Grading
                  </a>
                </li>
                <li <?php echo ($menu=="finish_taksiran")?"class='active'":"" ?>>
                  <a href="<?php echo site_url('obj_finish/taksiran') ?>">
                    <i class="fa fa-circle-o"></i> Taksiran
                  </a>
                </li>
              </ul>
            </li>
			<?php if($acc == "Gemologist"){ ?>
			<li <?php echo ($menu=="app_sertifikasi"||$menu=="app_grading"||$menu=="app_taksiran")?"class='treeview active'":"class='treeview'";?>>
              <a href="#">
                <i class="fa fa-check"></i> <span>Daftar Persetujuan</span>
                <div class="pull-right">
                  <i class="fa fa-chevron-right right-caret"></i>
                </div>
              </a>
              <ul class="treeview-menu">
                <li <?php echo ($menu=="app_sertifikasi")?"class='active'":"" ?>>
                  <a href="<?php echo site_url('approve/sertifikasi') ?>">
                    <i class="fa fa-circle-o"></i> Sertifikasi
                  </a>
                </li>
                <li <?php echo ($menu=="app_grading")?"class='active'":"" ?>>
                  <a href="<?php echo site_url('approve/grading') ?>">
                    <i class="fa fa-circle-o"></i> Diamond Grading
                  </a>
                </li>
                <li <?php echo ($menu=="app_taksiran")?"class='active'":"" ?>>
                  <a href="<?php echo site_url('approve/taksiran') ?>">
                    <i class="fa fa-circle-o"></i> Taksiran
                  </a>
                </li>
              </ul>
            </li>
			<?php } ?>
			<li <?php echo ($menu=="print_sertifikasi"||$menu=="print_grading"||$menu=="print_taksiran")?"class='treeview active'":"class='treeview'";?>>
              <a href="#">
                <i class="fa fa-print"></i> <span>Daftar Cetak</span>
                <div class="pull-right">
                  <i class="fa fa-chevron-right right-caret"></i>
                </div>
              </a>
              <ul class="treeview-menu">
                <li <?php echo ($menu=="print_sertifikasi")?"class='active'":"" ?>>
                  <a href="<?php echo site_url('cetak/sertifikasi') ?>">
                    <i class="fa fa-circle-o"></i> Sertifikasi
                  </a>
                </li>
                <li <?php echo ($menu=="print_grading")?"class='active'":"" ?>>
                  <a href="<?php echo site_url('cetak/grading') ?>">
                    <i class="fa fa-circle-o"></i> Diamond Grading
                  </a>
                </li>
                <li <?php echo ($menu=="print_taksiran")?"class='active'":"" ?>>
                  <a href="<?php echo site_url('cetak/taksiran') ?>">
                    <i class="fa fa-circle-o"></i> Taksiran
                  </a>
                </li>
              </ul>
            </li>
			<li <?php echo ($menu=="print_sertifikasi_selesai"||$menu=="print_grading_selesai"||$menu=="print_taksiran_selesai")?"class='treeview active'":"class='treeview'";?>>
              <a href="#">
                <i class="fa fa-print"></i> <span>Daftar Cetak Ulang</span>
                <div class="pull-right">
                  <i class="fa fa-chevron-right right-caret"></i>
                </div>
              </a>
              <ul class="treeview-menu">
                <li <?php echo ($menu=="print_sertifikasi_selesai")?"class='active'":"" ?>>
                  <a href="<?php echo site_url('cetakselesai/sertifikasi') ?>">
                    <i class="fa fa-circle-o"></i> Sertifikasi
                  </a>
                </li>
                <li <?php echo ($menu=="print_grading_selesai")?"class='active'":"" ?>>
                  <a href="<?php echo site_url('cetakselesai/grading') ?>">
                    <i class="fa fa-circle-o"></i> Diamond Grading
                  </a>
                </li>
                <li <?php echo ($menu=="print_taksiran_selesai")?"class='active'":"" ?>>
                  <a href="<?php echo site_url('cetakselesai/taksiran') ?>">
                    <i class="fa fa-circle-o"></i> Taksiran
                  </a>
                </li>
              </ul>
            </li>
			<?php if($acc == "Gemologist"){ 
					if(sessionValue('lokasi') == "PUSAT"){
			?>
              <li <?php echo ($menu=="Harga Emas")?"class='active'":"";?>>
                <a href="<?php echo site_url('hargaemas'); ?>">
                  <i class="fa fa-user-plus"></i> <span>Harga Emas</span>
                </a>
              </li>
			  <?php } ?>
              <li <?php echo ($menu=="Brief Report" || $menu=="Full Report" || $menu=="Diamond Grading" || $menu=="Taksiran" || $menu=="Rekapitulasi" || $menu=="Barang Tertinggal" || $menu=="Report Gemstone")?"class='treeview active'":"class='treeview'";?>>
                <a href="#">
                  <i class="fa fa-file-pdf-o"></i> <span>Report</span>
                  <div class="pull-right">
                    <i class="fa fa-chevron-right right-caret"></i>
                  </div>
                </a>
                <ul class="treeview-menu">
                  <li <?php echo ($menu=="Brief Report")?"class='active'":"";?>>
                    <a href="<?php echo site_url('report') ?>">
                      <i class="fa fa-circle-o"></i> Brief Report
                    </a>
                  </li>
                  <li <?php echo ($menu=="Full Report")?"class='active'":"";?>>
                    <a href="<?php echo site_url('report/full_report') ?>">
                      <i class="fa fa-circle-o"></i> Identification Report
                    </a>
                  </li>
                  <li <?php echo ($menu=="Diamond Grading")?"class='active'":"";?>>
                    <a href="<?php echo site_url('report/grading') ?>">
                      <i class="fa fa-circle-o"></i> Diamond Grading
                    </a>
                  </li>
                  <li <?php echo ($menu=="Taksiran")?"class='active'":"";?>>
                    <a href="<?php echo site_url('report/taksiran') ?>">
                      <i class="fa fa-circle-o"></i> Taksiran
                    </a>
                  </li>
                  <li <?php echo ($menu=="Rekapitulasi")?"class='active'":"";?>>
                    <a href="<?php echo site_url('report/rekapitulasi') ?>">
                      <i class="fa fa-circle-o"></i> Rekapitulasi
                    </a>
                  </li>
                  <li <?php echo ($menu=="Barang Tertinggal")?"class='active'":"";?>>
                    <a href="<?php echo site_url('report/barangtertinggal') ?>">
                      <i class="fa fa-circle-o"></i> Barang Tertinggal
                    </a>
                  </li>
                  <li <?php echo ($menu=="Report Gemstone")?"class='active'":"";?>>
                    <a href="<?php echo site_url('report/gemstone') ?>">
                      <i class="fa fa-circle-o"></i> Gemstone
                    </a>
                  </li>
                </ul>
              </li>
              <?php
				}
            }else if($acc == "Print_Desk"){
              ?>
            <li <?php echo ($menu=="print-gem" || $menu=="print-cert" || $menu=="print-grading")?"class='treeview active'":"class='treeview'";?>>
              <a href="#">
                <i class="fa fa-print"></i> <span>Ready to Print</span>
              </a>
              <ul class="treeview-menu">
                <li <?php echo ($menu=="print-gem")?"class='active'":"";?>>
                  <a href="<?php echo site_url('ready') ?>">
                    <i class="fa fa-circle-o"></i> Gem Card
                  </a>
                </li>
                <li <?php echo ($menu=="print-cert")?"class='active'":"";?>>
                  <a href="<?php echo site_url('certificate') ?>">
                    <i class="fa fa-circle-o"></i> Certificate
                  </a>
                </li>
                <li <?php echo ($menu=="print-grading")?"class='active'":"";?>>
                  <a href="<?php echo site_url('grading') ?>">
                    <i class="fa fa-circle-o"></i> Diamond Grading
                  </a>
                </li>
              </ul>              
            </li>
            <li <?php echo ($menu=="print-gem-done"||$menu=="print-cert-done"||$menu=="print-grading-done")?"class=' treeview active'":"class='treeview'";?>>
              <a href="#">
                <i class="fa fa-check"></i> <span>Object Printed</span>
              </a>
              <ul class="treeview-menu">
                <li <?php echo ($menu=="print-gem-done")?"class='active'":"" ?>>
                  <a href="<?php echo site_url('print_gem_done') ?>">
                    <i class="fa fa-circle-o"></i> Gem Card
                  </a>
                </li>
                <li <?php echo ($menu=="print-cert-done")?"class='active'":"" ?>>
                  <a href="<?php echo site_url('print_cert_done') ?>">
                    <i class="fa fa-circle-o"></i> Certificate
                  </a>
                </li>
                <li <?php echo ($menu=="print-grading-done")?"class='active'":"" ?>>
                  <a href="<?php echo site_url('print_grading_done') ?>">
                    <i class="fa fa-circle-o"></i> Diamond Grading
                  </a>
                </li>
              </ul>
            </li>
              <?php
            }else if($acc == "Admin"){
              ?>
              <li <?php echo ($menu=="user_manage")?"class='active'":"";?>>
                <a href="<?php echo site_url('user_management'); ?>">
                  <i class="fa fa-user-plus"></i> <span>User Management</span>
                </a>
              </li>
              <li <?php echo ($menu=="agama"||$menu=="Alamat Surat Menyurat"||$menu=='Bentuk Perusahaan'||$menu=="identitas"||$menu=="kewarganegaraan"||$menu=="pekerjaan"||$menu=="pendidikan"||$menu=="penghasilan"||$menu=="Status Tempat Tinggal"||$menu=="Sumber Dana"||$menu=="perhiasan"||$menu=="permata")?"class='active'":"";?>>
                <a href="#">
                  <i class="fa fa-book"></i>
                  <span>Master Front Desk</span>
                  <div class="pull-right">
                    <i class="fa fa-chevron-right right-caret"></i>
                  </div>
                </a>
                <ul class="treeview-menu">
                  <li <?php echo ($menu=="agama")?"class='active'":"";?>>
                    <a href="<?php echo site_url('agama') ?>">
                      <i class="fa fa-circle-o"></i> Agama
                    </a>
                  </li>
                  <li <?php echo ($menu=="Alamat Surat Menyurat")?"class='active'":"";?>>
                    <a href="<?php echo site_url('alamatsuratmenyurat') ?>">
                      <i class="fa fa-circle-o"></i> Alamat Surat Menyurat
                    </a>
                  </li>
                  <li <?php echo ($menu=="Bentuk Perusahaan")?"class='active'":"";?>>
                    <a href="<?php echo site_url('bentukperusahaan') ?>">
                      <i class="fa fa-circle-o"></i> Bentuk Perusahaan
                    </a>
                  </li>
                  <li <?php echo ($menu=="identitas")?"class='active'":"";?>>
                    <a href="<?php echo site_url('identitas') ?>">
                      <i class="fa fa-circle-o"></i> Identitas
                    </a>
                  </li>
                  <li <?php echo ($menu=="kewarganegaraan")?"class='active'":"";?>>
                    <a href="<?php echo site_url('kewarganegaraan') ?>">
                      <i class="fa fa-circle-o"></i> Kewarganegaraan
                    </a>
                  </li>
                  <li <?php echo ($menu=="pekerjaan")?"class='active'":"";?>>
                    <a href="<?php echo site_url('pekerjaan') ?>">
                      <i class="fa fa-circle-o"></i> Pekerjaan
                    </a>
                  </li>
                  <li <?php echo ($menu=="pendidikan")?"class='active'":"";?>>
                    <a href="<?php echo site_url('pendidikan') ?>">
                      <i class="fa fa-circle-o"></i> Pendidikan
                    </a>
                  </li>
                  <li <?php echo ($menu=="penghasilan")?"class='active'":"";?>>
                    <a href="<?php echo site_url('penghasilan') ?>">
                      <i class="fa fa-circle-o"></i> Penghasilan
                    </a>
                  </li>
                  <li <?php echo ($menu=="perhiasan")?"class='active'":"";?>>
                    <a href="<?php echo site_url('perhiasan') ?>">
                      <i class="fa fa-circle-o"></i> Perhiasan
                    </a>
                  </li>
                  <li <?php echo ($menu=="permata")?"class='active'":"";?>>
                    <a href="<?php echo site_url('permata') ?>">
                      <i class="fa fa-circle-o"></i> Permata
                    </a>
                  </li>
                  <li <?php echo ($menu=="Status Tempat Tinggal")?"class='active'":"";?>>
                    <a href="<?php echo site_url('statustempattinggal') ?>">
                      <i class="fa fa-circle-o"></i> Status Tempat Tinggal
                    </a>
                  </li>
                  <li <?php echo ($menu=="Sumber Dana")?"class='active'":"";?>>
                    <a href="<?php echo site_url('sumberdana') ?>">
                      <i class="fa fa-circle-o"></i> Sumber Dana
                    </a>
                  </li>
                </ul>
              </li>
			  <li <?php echo ($menu=="color_scheme"||$menu=="crystal"||$menu=='cut'||$menu=='fracture'||$menu=="luminescence"||$menu=="luster"||$menu=="species"||$menu=="microscopic"||$menu=="gemstone"||$menu=="culet"||$menu=="fluorescence"||$menu=="girdle"||$menu=="Key of Symbol"||$menu=="logam"||$menu=="shape"||$menu=="phenomenal"||$menu=="radioactivity"||$menu=="clarity"||$menu=="Diamond Color"||$menu=="Diamond Cut"||$menu=="gemologist"||$menu=="variety"||$menu=="treatment"||$menu=="natural"||$menu=="origin"||$menu=="strunz"||$menu=="tenacity"||$menu=="transparency"||$menu=="twinning")?"class='active'":"";?>>
                <a href="#">
                  <i class="fa fa-book"></i>
                  <span>Master Lab Desk</span>
                  <div class="pull-right">
                    <i class="fa fa-chevron-right right-caret"></i>
                  </div>
                </a>
                <ul class="treeview-menu">
                  <li <?php echo ($menu=="Diamond Color")?"class='active'":"";?>>
                    <a href="<?php echo site_url('diamondcolor') ?>">
                      <i class="fa fa-circle-o"></i> Diamond Color
                    </a>
                  </li>
                  <li <?php echo ($menu=="Diamond Cut")?"class='active'":"";?>>
                    <a href="<?php echo site_url('diamondcut') ?>">
                      <i class="fa fa-circle-o"></i> Diamond Cut
                    </a>
                  </li>
                  <li <?php echo ($menu=="clarity")?"class='active'":"";?>>
                    <a href="<?php echo site_url('clarity') ?>">
                      <i class="fa fa-circle-o"></i> Clarity
                    </a>
                  </li>
                  <li <?php echo ($menu=="color_scheme")?"class='active'":"";?>>
                    <a href="<?php echo site_url('color_scheme') ?>">
                      <i class="fa fa-circle-o"></i> Color
                    </a>
                  </li>
                  <li <?php echo ($menu=="crystal")?"class='active'":"";?>>
                    <a href="<?php echo site_url('crystal') ?>">
                      <i class="fa fa-circle-o"></i> Crystal
                    </a>
                  </li>
                  <li <?php echo ($menu=="culet")?"class='active'":"";?>>
                    <a href="<?php echo site_url('culet') ?>">
                      <i class="fa fa-circle-o"></i> Culet
                    </a>
                  </li>
                  <li <?php echo ($menu=="cut")?"class='active'":"";?>>
                    <a href="<?php echo site_url('cut') ?>">
                      <i class="fa fa-circle-o"></i> Cut
                    </a>
                  </li>
                  <li <?php echo ($menu=="fracture")?"class='active'":"";?>>
                    <a href="<?php echo site_url('fracture') ?>">
                      <i class="fa fa-circle-o"></i> Fracture
                    </a>
                  </li>
                  <li <?php echo ($menu=="fluorescence")?"class='active'":"";?>>
                    <a href="<?php echo site_url('fluorescence') ?>">
                      <i class="fa fa-circle-o"></i> Fluorescence
                    </a>
                  </li>
                  <li <?php echo ($menu=="gemologist")?"class='active'":"";?>>
                    <a href="<?php echo site_url('gemologist') ?>">
                      <i class="fa fa-circle-o"></i> Gemologist
                    </a>
                  </li>
                  <li <?php echo ($menu=="gemstone")?"class='active'":"";?>>
                    <a href="<?php echo site_url('gemstone') ?>">
                      <i class="fa fa-circle-o"></i> Gemstone
                    </a>
                  </li>
                  <li <?php echo ($menu=="girdle")?"class='active'":"";?>>
                    <a href="<?php echo site_url('girdle') ?>">
                      <i class="fa fa-circle-o"></i> Girdle
                    </a>
                  </li>
                  <li <?php echo ($menu=="Key of Symbol")?"class='active'":"";?>>
                    <a href="<?php echo site_url('keysymbol') ?>">
                      <i class="fa fa-circle-o"></i> Key of Symbol
                    </a>
                  </li>
                  <li <?php echo ($menu=="logam")?"class='active'":"";?>>
                    <a href="<?php echo site_url('logam') ?>">
                      <i class="fa fa-circle-o"></i> Logam
                    </a>
                  </li>
                  <li <?php echo ($menu=="luminescence")?"class='active'":"";?>>
                    <a href="<?php echo site_url('luminescence') ?>">
                      <i class="fa fa-circle-o"></i> Luminescence
                    </a>
                  </li>
                  <li <?php echo ($menu=="luster")?"class='active'":"";?>>
                    <a href="<?php echo site_url('luster') ?>">
                      <i class="fa fa-circle-o"></i> Luster
                    </a>
                  </li>
                  <li <?php echo ($menu=="microscopic")?"class='active'":"";?>>
                    <a href="<?php echo site_url('microscopic') ?>">
                      <i class="fa fa-circle-o"></i> Microscopic
                    </a>
                  </li>
                  <li <?php echo ($menu=="natural")?"class='active'":"";?>>
                    <a href="<?php echo site_url('natural') ?>">
                      <i class="fa fa-circle-o"></i> Natural
                    </a>
                  </li>
                  <li <?php echo ($menu=="origin")?"class='active'":"";?>>
                    <a href="<?php echo site_url('origin') ?>">
                      <i class="fa fa-circle-o"></i> Origin
                    </a>
                  </li>
                  <li <?php echo ($menu=="phenomenal")?"class='active'":"";?>>
                    <a href="<?php echo site_url('phenomenal') ?>">
                      <i class="fa fa-circle-o"></i> Phenomenal
                    </a>
                  </li>
                  <li <?php echo ($menu=="radioactivity")?"class='active'":"";?>>
                    <a href="<?php echo site_url('radioactivity') ?>">
                      <i class="fa fa-circle-o"></i> Radioactivity
                    </a>
                  </li>
                  <li <?php echo ($menu=="shape")?"class='active'":"";?>>
                    <a href="<?php echo site_url('shape') ?>">
                      <i class="fa fa-circle-o"></i> Shape
                    </a>
                  </li>
                  <li <?php echo ($menu=="species")?"class='active'":"";?>>
                    <a href="<?php echo site_url('species') ?>">
                      <i class="fa fa-circle-o"></i> Species
                    </a>
                  </li>
                  <li <?php echo ($menu=="strunz")?"class='active'":"";?>>
                    <a href="<?php echo site_url('strunz') ?>">
                      <i class="fa fa-circle-o"></i> Strunz
                    </a>
                  </li>
                  <li <?php echo ($menu=="tenacity")?"class='active'":"";?>>
                    <a href="<?php echo site_url('tenacity') ?>">
                      <i class="fa fa-circle-o"></i> Tenacity
                    </a>
                  </li>
                  <li <?php echo ($menu=="transparency")?"class='active'":"";?>>
                    <a href="<?php echo site_url('transparent') ?>">
                      <i class="fa fa-circle-o"></i> Transparency
                    </a>
                  </li>
                  <li <?php echo ($menu=="treatment")?"class='active'":"";?>>
                    <a href="<?php echo site_url('treatment') ?>">
                      <i class="fa fa-circle-o"></i> Treatment
                    </a>
                  </li>
                  <li <?php echo ($menu=="twinning")?"class='active'":"";?>>
                    <a href="<?php echo site_url('twinning') ?>">
                      <i class="fa fa-circle-o"></i> Twinning
                    </a>
                  </li>
                  <li <?php echo ($menu=="variety")?"class='active'":"";?>>
                    <a href="<?php echo site_url('variety') ?>">
                      <i class="fa fa-circle-o"></i> Variety
                    </a>
                  </li>
                </ul>
              </li>
              <li <?php echo ($menu=="service"||$menu=="print_option"||$menu=="Harga Emas"||$menu=="Harga Berlian"||$menu=="Harga Grading"||$menu=="Website Verifikasi")?"class='treeview active'":"class='treeview'";?>>
              <a href="#">
                <i class="fa fa-cog"></i>
                <span>Parameter</span>
                  <div class="pull-right">
                    <i class="fa fa-chevron-right right-caret"></i>
                  </div>
              </a>
              <ul class="treeview-menu">
                <li <?php echo ($menu=="service")?"class='treeview active'":"";?>>
                  <a href="<?php echo site_url('service') ?>">
                    <i class="fa fa-circle-o"></i> Service
                  </a>
                </li>
                <li <?php echo ($menu=="print_option")?"class='treeview active'":"";?>>
                  <a href="<?php echo site_url('print_option') ?>">
                    <i class="fa fa-circle-o"></i> Print Option
                  </a>
                </li>
                <li <?php echo ($menu=="Harga Emas")?"class='treeview active'":"";?>>
                  <a href="<?php echo site_url('hargaemas') ?>">
                    <i class="fa fa-circle-o"></i> Harga Emas
                  </a>
                </li>
                <li <?php echo ($menu=="Harga Grading")?"class='treeview active'":"";?>>
                  <a href="<?php echo site_url('hargagrading') ?>">
                    <i class="fa fa-circle-o"></i> Harga Grading
                  </a>
				</li>
                <li <?php echo ($menu=="Harga Berlian")?"class='treeview active'":"";?>>
                  <a href="<?php echo site_url('hargaberlian') ?>">
                    <i class="fa fa-circle-o"></i> Harga Berlian
                  </a>
                </li>
				<li <?php echo ($menu=="Website Verifikasi")?"class='treeview active'":"";?>>
                  <a href="<?php echo site_url('urlweb') ?>">
                    <i class="fa fa-circle-o"></i> Website Verifikasi
                  </a>
                </li>
              </ul>
            </li>
              <li <?php echo ($menu=="data_store")?"class='active'":"";?>>
                <a href="<?php echo site_url('data_store'); ?>">
                  <i class="fa fa-building"></i> <span>Data Store</span>
                </a>
              </li>
              <li <?php echo ($menu=="Brief Report" || $menu=="Full Report" || $menu=="Diamond Grading" || $menu=="Taksiran" || $menu=="Rekapitulasi" || $menu=="Barang Tertinggal" || $menu=="Report Gemstone")?"class='treeview active'":"class='treeview'";?>>
                <a href="#">
                  <i class="fa fa-file-pdf-o"></i> <span>Report</span>
                  <div class="pull-right">
                    <i class="fa fa-chevron-right right-caret"></i>
                  </div>
                </a>
                <ul class="treeview-menu">
                  <li <?php echo ($menu=="Brief Report")?"class='active'":"";?>>
                    <a href="<?php echo site_url('report') ?>">
                      <i class="fa fa-circle-o"></i> Brief Report
                    </a>
                  </li>
                  <li <?php echo ($menu=="Full Report")?"class='active'":"";?>>
                    <a href="<?php echo site_url('report/full_report') ?>">
                      <i class="fa fa-circle-o"></i> Identification Report
                    </a>
                  </li>
                  <li <?php echo ($menu=="Diamond Grading")?"class='active'":"";?>>
                    <a href="<?php echo site_url('report/grading') ?>">
                      <i class="fa fa-circle-o"></i> Diamond Grading
                    </a>
                  </li>
                  <li <?php echo ($menu=="Taksiran")?"class='active'":"";?>>
                    <a href="<?php echo site_url('report/taksiran') ?>">
                      <i class="fa fa-circle-o"></i> Taksiran
                    </a>
                  </li>
                  <li <?php echo ($menu=="Rekapitulasi")?"class='active'":"";?>>
                    <a href="<?php echo site_url('report/rekapitulasi') ?>">
                      <i class="fa fa-circle-o"></i> Rekapitulasi
                    </a>
                  </li>
                  <li <?php echo ($menu=="Barang Tertinggal")?"class='active'":"";?>>
                    <a href="<?php echo site_url('report/barangtertinggal') ?>">
                      <i class="fa fa-circle-o"></i> Barang Tertinggal
                    </a>
                  </li>
                  <li <?php echo ($menu=="Report Gemstone")?"class='active'":"";?>>
                    <a href="<?php echo site_url('report/gemstone') ?>">
                      <i class="fa fa-circle-o"></i> Gemstone
                    </a>
                  </li>
                </ul>
              </li>
              <!--<li <?php echo ($menu=="report_identify_day" || $menu=="report_identify_month" || $menu=="report_identify_year" )?"class='treeview active'":"class='treeview'";?>>
                <a href="#">
                  <i class="fa fa-file-pdf-o"></i> <span>Report Identification</span>
                </a>
                <ul class="treeview-menu">
                  <li <?php echo ($menu=="report_identify_day")?"class='active'":"";?>>
                    <a href="<?php echo site_url('report_identify_day') ?>">
                      <i class="fa fa-circle-o"></i> Report Identification Today
                    </a>
                  </li>
                  <li <?php echo ($menu=="report_identify_month")?"class='active'":"";?>>
                    <a href="<?php echo site_url('report_identify_month') ?>">
                      <i class="fa fa-circle-o"></i> Report Identification Month
                    </a>
                  </li>
                  <li <?php echo ($menu=="report_identify_year")?"class='active'":"";?>>
                    <a href="<?php echo site_url('report_identify_year') ?>">
                      <i class="fa fa-circle-o"></i> Report Identification Year
                    </a>
                  </li>
                </ul>
              </li>-->
              <?php
            }
            ?>
			       <li <?php echo ($menu=="profile")?"class='active'":"" ?>>
              <a href="<?php echo site_url('profile') ?>">
                <i class="fa fa-user"></i> <span>Profile</span>
              </a>
            </li>
            <li>
              <a href="<?php echo site_url('logout') ?>">
                <i class="fa fa-sign-out"></i> Logout
              </a>
            </li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
<?php }else{ ?>
<body style="background-color: #3A3A3A">
<?php } ?>