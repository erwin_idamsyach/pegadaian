<!-- BEGIN html -->
<html>
<head>
<title><?php echo $title;?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link rel="shortcut icon" href="<?php echo base_url();?>asset/images/LOGO-COG-FINAL.png">

<?php 
if(!isLogin()){
// Login Form
?>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>asset/css/login_admin.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>asset/plugins/bootstrap-3.3.5-dist/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>asset/css/style.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>asset/plugins/font-awesome-4.5.0/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>asset/css/animate.css">

	<script src="<?php echo base_url();?>asset/plugins/jQuery/jQuery-2.1.4.min.js"></script>
	<script src="<?php echo base_url();?>asset/plugins/bootstrap-3.3.5-dist/js/bootstrap.js"></script>
	<script type="text/javascript">
    $(document).ready(function(){
            $('#username').click(function(){
                return false;
            });
            $('#password').click(function(){
                return false;
            });
	        $('.ojo').click(function(){
                return false;
            });
            $("#login").click(function(){

                var action = $("#form-login").attr('action');
                var form_data = {
                    username: $("#username").val(),
                    password: $("#password").val(),
                    store: $("#store").val(),
                    is_ajax: 1
                };
                $.ajax({
                    type: "POST",
                    url: action,
                    data: form_data,
                    success: function(response)
                    {
						var data = eval ("(" + response + ")");
                        if(data.success)
							location.reload();
                        else if(data.wrong_store)
                            $("#pesan").html('<p class="error"><i class="fa fa-warning"></i> ACCESS DENIED</p>');
                        else
                            $("#pesan").html('<p class="error"><i class="fa fa-warning"></i> ERROR : WRONG USERNAME OR PASSWORD</p>');
                    }
                });
                return false;
            });
        });
	</script>

	<style>
        #message{
            width:100%;
            text-align:center;
        }
        .error {
            padding: 5px;
            color: white;
            background:red;
            border-radius:3px;
        }
	</style>

<?php 
}else{ 
// Dasboard
?>
	<!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo base_url();?>asset/plugins/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url();?>asset/plugins/font-awesome-4.5.0/css/font-awesome.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url();?>asset/plugins/dist/css/AdminLTE.css">
    <link rel="stylesheet" href="<?php echo base_url();?>asset/plugins/dist/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>asset/plugins/dropify-master/dist/css/dropify.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url();?>asset/plugins/dist/css/skins/_all-skins.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo base_url();?>asset/plugins/iCheck/flat/blue.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="<?php echo base_url();?>asset/plugins/morris/morris.css">
    <link rel="stylesheet" href="<?php echo base_url();?>asset/plugins/datatables/dataTables.bootstrap.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="<?php echo base_url();?>asset/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
	<link rel="stylesheet" href="<?php echo base_url();?>asset/sweet-alert/sweetalert.css">
	<!-- Datepicker -->
	<link rel="stylesheet" href="<?php echo base_url();?>asset/plugins/bootstrap-datepicker/1.3.0/css/datepicker.min.css" />
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />
	
    <script src="<?php echo base_url();?>asset/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src="<?php echo base_url();?>asset/jscript/jquery.maskedinput.min.js"></script>
    <script src="<?php echo base_url();?>asset/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>asset/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>asset/sweet-alert/sweetalert.min.js"></script>
    <script src="<?php echo base_url();?>asset/plugins/morris/raphael-min.js"></script>
    <script src="<?php echo base_url();?>asset/plugins/morris/morris.min.js"></script>
    <script src="<?php echo base_url();?>asset/jscript/ajax-js.js"></script>
	<!-- Datepicker -->
	<script src="<?php echo base_url();?>asset/plugins/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
	<!-- Format Number -->
	<script src="<?php echo base_url();?>asset/jscript/number.js"></script>	
	<!-- Auto Complete -->
	<script type="text/javascript" src="<?php echo base_url();?>asset/plugins/EasyAutocomplete-1.3.5/jquery.easy-autocomplete.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>asset/plugins/EasyAutocomplete-1.3.5/easy-autocomplete.min.css">
	
	<script>
	var base_url = "<?php echo base_url()?>";
	</script>
	
    <script>
      $(function () {
        //$("#example1").DataTable();
        //$('#example2').DataTable();
        //$('#example3').DataTable();
      });
    </script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

<?php
} ?>

</head>