<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class logout extends CI_Controller{
	
	public function index(){
		parent::__construct();
		$this->session->sess_destroy();
		redirect(base_url());
	}
}