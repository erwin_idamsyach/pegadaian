<?php
$ord = str_replace('-', '/', $ord);
$data = $this->db->query("SELECT tb_fpjt.id, tb_fpjt.id_order, tb_fpjt.foto, tb_fpjt.jumlah, tb_fpjt.berat_perhiasan, tb_fpjt.jumlah_permata, tb_fpjt.keterangan,
							master_permata.permata, master_jenis_permata.jenis jenis_perhiasan,
							color_stone.color
							FROM tb_fpjt 
						LEFT JOIN master_permata ON master_permata.id = tb_fpjt.jenis_permata
						LEFT JOIN master_jenis_permata ON master_jenis_permata.id = tb_fpjt.jenis_perhiasan
						LEFT JOIN color_stone ON color_stone.code = tb_fpjt.warna_permata
						LEFT JOIN tb_login ON tb_login.id = tb_fpjt.create_by
						WHERE tb_fpjt.id_order='$ord'
						AND tb_login.store='".sessionValue('kode_store')."'");


$sub = 0;
$pdf = new FPDF('P','mm','A4');
$pdf->SetAutoPageBreak(false, 0);
$pdf->AddPage();

$pdf->SetFont('arial','B',16);
$pdf->SetXY(0,18);
$pdf->Cell(190,0,'Bukti Permintaan Taksiran',0,0,'C');

$pdf->Image('./asset/images/Logo.png', 138, 2, 50, 30);

$pdf->SetFont('arial','B',10);
$pdf->SetXY(140, 29);

$pdf->SetFont('arial','',10);
$pdf->SetXY(140, 30);
$pdf->MultiCell(70, 5, sessionValue('alamat'),'','L');

$pdf->SetXY(7, 45);
$pdf->Cell(20, 5, 'No. ');
$pdf->SetXY(25, 45);
$pdf->Cell(10, 5, ':');
$pdf->SetXY(28, 45);
$pdf->Cell(10, 5, $ord);

$dudate  = date_create(date('D, d-m-Y'));
$day  = date_format($dudate, "d");
$month  = date_format($dudate, "m");
$year  = date_format($dudate, "Y");

$pdf->SetXY(7, 49);
$pdf->Cell(20, 5, 'Tanggal');
$pdf->SetXY(25, 49);
$pdf->Cell(10, 5, ':');
$pdf->SetXY(28, 49);
$pdf->Cell(10, 5, $day."".bulan($month)." ".$year);

$pdf->Rect(110, 42, 90, 30,'D');

$width = array(20, 50, 15, 30, 75);
$pdf->SetFont('arial','',11);
$pdf->SetY(80);
$pdf->Cell($width[0], 10, "No", 1, 0, 'C');
$pdf->Cell($width[1], 10, "Jenis Perhiasan", 1, 0, 'C');
$pdf->Cell($width[2], 10, "Jumlah", 1, 0, 'C');
$pdf->Cell($width[3], 10, "Berat", 1, 0, 'C');
$pdf->Cell($width[4], 10, "Keterangan", 1, 0, 'C');
$pdf->Ln();

$pdf->SetFont('arial','',10);

$cer = 0;
$mem = 0;
$dim = 0;
$no = 1;
$ong = 1;
$harga = 100000;
$harga_permata = 50000;
foreach($data->result_array() as $g){
	if($g['jumlah_permata']<1){
		$harga_permata = 0;
	}
	$exp = explode(';', $g['color']);
	if(count($exp)>0){
		$color_spek = $exp[0];
	}else{
		$color_spek = $g['color'];
	}
	
	$amount = $harga*$g['jumlah'];
	
	$pdf->Cell($width[0], 7, $no++, 'LR', 0, 'C');
	$pdf->Cell($width[1], 7, $g['jenis_perhiasan'], 'R', 0, 'L');
	$pdf->Cell($width[2], 7, $g['jumlah'], 'R', 0, 'C');
	$pdf->Cell($width[3], 7, $g['berat_perhiasan'].' gr', 'R', 0, 'R');
	$pdf->Cell($width[4], 7, $g['keterangan'], 'R', 0, 'L');
	$pdf->Ln();
	/*
	$pdf->Cell($width[0], 7, '', 'LR', 0, 'C');
	$pdf->SetX(40);
	$pdf->Cell($width[1]-10, 7, $g['permata'], 'R');
	$pdf->Cell($width[2], 7, '', 'R', 0, 'C');
	$pdf->Cell($width[3], 7, '', 'R', 0, 'R');
	$pdf->Cell($width[4], 7, '', 'R', 0, 'R');
	$pdf->Ln();	
	$pdf->Cell($width[0], 7, '', 'LR', 0, 'C');
	$pdf->SetX(40);
	$pdf->Cell($width[1]-10, 7, $color_spek, 'R');
	$pdf->Cell($width[2], 7, '', 'R', 0, 'C');
	$pdf->Cell($width[3], 7, '', 'R', 0, 'R');
	$pdf->Cell($width[4], 7, '', 'R', 0, 'R');
	$pdf->Ln();
	$pdf->Cell($width[0], 7, '', 'LR', 0, 'C');
	$pdf->SetX(40);
	$pdf->Cell($width[1]-10, 7, 'Jumlah Permata', 'R');
	$pdf->Cell($width[2], 7, $g['jumlah_permata'], 'R', 0, 'C');
	$pdf->Cell($width[3], 7, number_format($harga_permata,0,',','.'), 'R', 0, 'R');
	$pdf->Cell($width[4], 7, number_format($harga_permata*$g['jumlah_permata'],0,',','.'), 'R', 0, 'R');
	$pdf->Ln();
	
	$sub = $sub+($amount+($harga_permata*$g['jumlah_permata']));*/
}

$ppn = $sub*10/100;
$total = $sub+$ppn;

$pdf->SetFont('arial','',10);

$pdf->Cell($width[0], 7, 'Tarif Jasa Taksiran Emas = 1.25% x Jumlah Taksiran', 'T', 0, 'L');
$pdf->Cell($width[1], 7, '', 'T', 0, 'L');
$pdf->Cell($width[2], 7, '', 'T', 0, 'C');
$pdf->Cell($width[3], 7, '', 'T');
$pdf->Cell($width[4], 7, '', 'T', 0, 'R');
$pdf->Ln();
/*
$pdf->Cell($width[0], 7, '', '', 0, 'C');
$pdf->Cell($width[1], 7, '', '', 0, 'L');
$pdf->Cell($width[2], 7, '', 'R', 0, 'C');
$pdf->Cell($width[3], 7, 'PPN', 'R');
$pdf->Cell($width[4], 7, number_format($ppn,0,',','.'), 'R', 0, 'R');
$pdf->Ln();

$pdf->Cell($width[0], 7, '', '', 0, 'C');
$pdf->Cell($width[1], 7, '', '', 0, 'L');
$pdf->Cell($width[2], 7, '', 'R', 0, 'C');
$pdf->Cell($width[3], 7, 'Total', 'BR');
$pdf->Cell($width[4], 7, number_format($total,0,',','.'), 'BR', 0, 'R');
$pdf->Ln();
*/
// Hadi
//$pdf->Rect(100, 42, 100, 25,'D');

$query = $this->db->query("SELECT tb_member.first_name, tb_member.middle_name, tb_member.last_name, tb_member.corp_name, tb_member.kode, tb_member.address, tb_member.id_member, tb_member.postal_code,
							kecamatan.kecamatanNama kec1, kabupaten.kabupatenNama kota1, provinsi.provinsiNama prov1
							FROM tb_fpjt 
						LEFT JOIN tb_member ON tb_member.id_member = tb_fpjt.id_member
						LEFT JOIN kecamatan ON kecamatan.kecamatanId = tb_member.district
						LEFT JOIN kabupaten ON kabupaten.kabupatenId = tb_member.city
						LEFT JOIN provinsi ON provinsi.provinsiId = tb_member.province
						WHERE tb_fpjt.id_order='$ord' LIMIT 0,1");
$nama = "";

$nasabah = "";
$penerima = sessionValue('nama');

$pdf->SetFont('arial','',10);
foreach($query->result() as $tmp){
	if($tmp->kode=="A"){
		$nama = $tmp->first_name." ".$tmp->middle_name." ".$tmp->last_name;
	}else{
		$nama = $tmp->corp_name;
	}
	
	$nasabah = $nama;
	
	$pdf->SetXY(113, 47);
	$pdf->Cell(0, 0, $nama);
	$pdf->SetXY(113, 52);
	$pdf->Cell(0, 0, $tmp->address);
	$pdf->SetXY(113, 57);
	$pdf->Cell(0, 0, $tmp->kec1.' - '.str_replace('Kota ','', $tmp->kota1));
	$pdf->SetXY(113, 62);
	$pdf->Cell(0, 0, $tmp->postal_code);
	$pdf->SetXY(113, 67);
	$pdf->Cell(0, 0, $tmp->prov1);
		
	$new_ord = substr($ord, -20, 9);
	$new_ord = str_replace("/", "-", $new_ord);
	$pdf->Image('./asset/barcode-fpjt/'.$tmp->id_member.$new_ord.'.gif', 10, 6, 35, 20);
}

$width_bot = array(95,95,5);
$pdf->SetY(160);
$pdf->Cell($width_bot[0], 7, 'Penerima,', '', 0, 'C');
$pdf->Cell($width_bot[1], 7, 'Nasabah,', '', 0, 'C');
$pdf->Ln(15);
$pdf->Cell($width_bot[0], 5, '('.$penerima.')', '', 0, 'C');
$pdf->Cell($width_bot[1], 5, '('.$nasabah.')', '', 0, 'C');
$pdf->Ln();
$pdf->Cell($width_bot[0], 5, '', '', 0, 'C');
$pdf->Cell($width_bot[1], 5, '', '', 0, 'C');
$pdf->Ln(15);

$pdf->Cell($width_bot[0], 5, 'Pemberian Kuasa, ', 'LRT', 0, 'C');
$pdf->Cell($width_bot[2], 5, '', 'T', 0, 'L');
$pdf->Cell($width_bot[1]-$width_bot[2], 5, 'Kami yang bertandatangan di halaman depan Bukti', 'RT', 0, 'L');
$pdf->Ln();
$pdf->Cell($width_bot[0], 5, '', 'LR', 0, 'C');
$pdf->Cell($width_bot[2], 5, '', '', 0, 'L');
$pdf->Cell($width_bot[1]-$width_bot[2], 5, 'Permintaan Taksiran (BPT) ini, bertindak untuk', 'R', 0, 'L');
$pdf->Ln();
$pdf->Cell($width_bot[0], 5, 'Pada tanggal....................................................................', 'LR', 0, 'L');
$pdf->Cell($width_bot[2], 5, '', '', 0, 'L');
$pdf->Cell($width_bot[1]-$width_bot[2], 5, 'dan atas nama PT Pegadaian (Persero) dengan', 'R', 0, 'L');
$pdf->Ln();
$pdf->Cell($width_bot[0], 5, '.........................................................................................', 'LR', 0, 'L');
$pdf->Cell($width_bot[2], 5, '', '', 0, 'L');
$pdf->Cell($width_bot[1]-$width_bot[2], 5, 'Nasabah, menyepakati ketentuan sebagai berikut :', 'R', 0, 'L');
$pdf->Ln();
$pdf->Cell($width_bot[0], 5, 'Dengan ini saya memberikan kuasa untuk mengambil', 'LR', 0, 'L');
$pdf->Cell($width_bot[2], 5, '', '', 0, 'L');
$pdf->Cell($width_bot[1]-$width_bot[2], 5, '', 'R', 0, 'L');
$pdf->Ln();
$pdf->Cell($width_bot[0], 5, 'obyek taksiran dan Surat Keterangan Hasil Pengujian,  ', 'LR', 0, 'L');
$pdf->Cell($width_bot[2], 5, '1.', '', 0, 'L');
$pdf->Cell($width_bot[1]-$width_bot[2], 5, 'Batas waktu pengambilan hasil sertifikasi maksimal ', 'R', 0, 'L');
$pdf->Ln();
$pdf->Cell($width_bot[0], 5, 'kepada :', 'LR', 0, 'L');
$pdf->Cell($width_bot[2], 5, '', '', 0, 'L');
$pdf->Cell($width_bot[1]-$width_bot[2], 5, '7 (tujuh) hari dari tanggal permintaan sertifikasi,', 'R', 0, 'L');
$pdf->Ln();
$pdf->Cell(25, 5, 'Nama', 'L', 0, 'L');
$pdf->Cell($width_bot[0]-25, 5, ':', 'R', 0, 'L');
$pdf->Cell($width_bot[2], 5, '', '', 0, 'L');
$pdf->Cell($width_bot[1]-$width_bot[2], 5, 'dan dapat dikuasakan dengan menyertakan KTP Asli', 'R', 0, 'L');
$pdf->Ln();
$pdf->Cell(25, 5, 'No. KTP/SIM', 'L', 0, 'L');
$pdf->Cell($width_bot[0]-25, 5, ':', 'R', 0, 'L');
$pdf->Cell($width_bot[2], 5, '', '', 0, 'L');
$pdf->Cell($width_bot[1]-$width_bot[2], 5, 'pemilik dan yang dikuasakan.', 'R', 0, 'L');
$pdf->Ln();
$pdf->Cell(25, 5, 'Alamat', 'L', 0, 'L');
$pdf->Cell($width_bot[0]-25, 5, ':', 'R', 0, 'L');
$pdf->Cell($width_bot[2], 5, '2.', '', 0, 'L');
$pdf->Cell($width_bot[1]-$width_bot[2], 5, 'Apabila Bukti Permintaan Taksiran (BPT) ini hilang', 'R', 0, 'L');
$pdf->Ln();
$pdf->Cell($width_bot[0], 5, '', 'LR', 0, 'L');
$pdf->Cell($width_bot[2], 5, '', '', 0, 'L');
$pdf->Cell($width_bot[1]-$width_bot[2], 5, 'harap segera melapor ke Pegadaian G-Lab tempat', 'R', 0, 'L');
$pdf->Ln();
$pdf->Cell($width_bot[0]/2, 5, 'Pemberi Hak,', 'L', 0, 'C');
$pdf->Cell($width_bot[0]/2, 5, 'Penerima Hak,', 'R', 0, 'C');
$pdf->Cell($width_bot[2], 5, '', '', 0, 'L');
$pdf->Cell($width_bot[1]-$width_bot[2], 5, 'BPT ini diterbitkan dengan menyertakan surat', 'R', 0, 'L');
$pdf->Ln();
$pdf->Cell($width_bot[0], 5, '', 'LR', 0, 'L');
$pdf->Cell($width_bot[2], 5, '', '', 0, 'L');
$pdf->Cell($width_bot[1]-$width_bot[2], 5, 'kehilangan dari kepolisian', 'R', 0, 'L');
$pdf->Ln();
$pdf->Cell($width_bot[0], 5, '', 'LR', 0, 'L');
$pdf->Cell($width_bot[2], 5, '', '', 0, 'L');
$pdf->Cell($width_bot[1]-$width_bot[2], 5, '', 'R', 0, 'L');
$pdf->Ln();
$pdf->Cell($width_bot[0]/2, 5, '(................................)', 'L', 0, 'C');
$pdf->Cell($width_bot[0]/2, 5, '(................................)', 'R', 0, 'C');
$pdf->Cell($width_bot[2], 5, '', '', 0, 'L');
$pdf->Cell($width_bot[1]-$width_bot[2], 5, 'Demikian perjanjian ini berlaku dan mengikat para', 'R', 0, 'L');
$pdf->Ln();
$pdf->Cell($width_bot[0], 5, '', 'LR', 0, 'L');
$pdf->Cell($width_bot[2], 5, '', '', 0, 'L');
$pdf->Cell($width_bot[1]-$width_bot[2], 5, 'pihak sejak BPT ini ditandatangani pleh kedua belah', 'R', 0, 'L');
$pdf->Ln();
$pdf->Cell($width_bot[0], 5, '(Lampirkan fotocopy KTP masing-masing, konfirmasi', 'LR', 0, 'L');
$pdf->Cell($width_bot[2], 5, '', '', 0, 'L');
$pdf->Cell($width_bot[1]-$width_bot[2], 5, 'pihak pada kolom yang tersedia di halaman depan. ', 'R', 0, 'L');
$pdf->Ln();
$pdf->Cell($width_bot[0], 5, 'Nasabah)', 'LBR', 0, 'L');
$pdf->Cell($width_bot[1], 5, '', 'BR', 0, 'L');

$pdf->Output();
?>