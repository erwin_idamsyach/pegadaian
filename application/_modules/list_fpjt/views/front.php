<script>
    $(function(){
        $("#example1").DataTable();
        $(".datepicker").datepicker();
    });
		
		function deleteFPJT(id){
			swal({
             title: "",
             text: "Apakah Anda akan menghapus data?",
             type: "warning",
             showCancelButton: true,
             confirmButtonColor: "#DD6B55",
             confirmButtonText: "Ya",
             cancelButtonText: "Tidak",
             closeOnConfirm: false }, function(){
                $.ajax({
				  type : "POST",
				  url  : base_url+"list_fpjt/delete/"+id,
				  data : {
				  },
				  success:function(html){ 
					swal({
					  title: "Berhasil Menghapus data!",
					  text: "Klik tombol di bawah.",
					  type: "success",
					  showCancelButton: false,
					  confirmButtonColor: "#257DB6",
					  confirmButtonText: "Ok!",
					  closeOnConfirm: false
					},
					function(){
					  location.href = base_url+"list_fpjt";
					});
					
				  }
				});
            });
			
		}
</script>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>&nbsp;</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-user"></i> Home</a></li>
            <li>Reg. BPT</li>
            <li class="active">List BPT</li>
          </ol>
        </section>
        <section class="content">
        	<div class="box box-default">
        		<div class="box-header">
        			<b>LIST BPT</b>
        			<div style="border: 1px solid black; margin-bottom: 10px"></div>
        		</div>
                <form action="" method="get">
                    <div class="container-fluid" style="margin-top: 10px;">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="from-group">
									<table width="100%" border="0">
									<tr>
										<td>
											<label><b>Cari Tanggal : </b></label>
										</td>
										<td>
											<input type="text" name="from" class="form-control datepicker">
										</td>
										<td>
											&nbsp;-&nbsp;
										</td>
										<td>
											<input type="text" name="to" class="form-control datepicker">
										</td>
										<td>
											&nbsp;<button type="submit" class="btn btn-primary" style="margin-top: 0px;"><i class="fa fa-search"></i></button>
										</td>
									</tr>
									</table>
                                </div>  
                            </div>
                        </div>
                    </div>
                </form>
        		<div class="box-body">
        			<table class="table table-striped table-bordered table-hover" id="example1">
        				<thead>
        					<tr>
        						<th class="text-center" width="5%">No</th>
                                <th class="text-center" width="15%">Tanggal</th>
        						<th class="text-center">ID FPJT</th>
        						<th class="text-center" width="18%">ID Member</th>
                                <th class="text-center" width="18%">Jumlah Perhiasan</th>
        						<th class="text-center" width="18%">Action</th>
        					</tr>
        				</thead>
        				<tbody>
        					<?php
        					$no = 1;
                            if(!isset($_GET['from']) && !isset($_GET['to'])){
                            $date = date('Y-m-d');
        					$getfpjt = $this->db->query("SELECT tb_fpjt.* 
								FROM tb_fpjt
								LEFT JOIN master_jenis_permata ON master_jenis_permata.id = tb_fpjt.jenis_perhiasan
								LEFT JOIN tb_login ON tb_login.id = tb_fpjt.create_by
                                WHERE
                                ISNULL(tb_fpjt.delete_by) AND
								DATE_FORMAT(tb_fpjt.create_date,'%d-%m-%Y')=DATE_FORMAT('$date','%d-%m-%Y')
								AND tb_login.store='".sessionValue('kode_store')."'
                                GROUP BY tb_fpjt.id_order ORDER BY tb_fpjt.input_date DESC");
        					foreach ($getfpjt->result() as $get) {
								$ord= str_replace('/', '-', $get->id_order); 
        						?>
        					<tr>
                                <td><?php echo $no++; ?></td>
                                <td><?php echo $get->input_date ?></td>
                                <td><?php echo $get->id_order ?></td>
                                <td><?php echo $get->id_member ?></td>
                                <td class="text-right"><?php echo $get->jumlah ?></td>
        						<td class="text-right">
                                <?php
                                $idn = str_replace('/', '-', $get->id_order);
                                ?>
        							<button class="btn btn-info" onclick="viewData('<?php echo $idn ?>')" data-toggle="tooltip" data-placement="bottom" title="View"><i class="fa fa-eye"></i></button>
        							<a href="#" class="btn btn-warning disabled" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="fa fa-edit"></i></a>
        							<a href="#" onclick="deleteFPJT('<?php echo $ord;?>')" class="btn btn-danger" data-toggle="tooltip" data-placement="bottom" title="Delete"><i class="fa fa-trash"></i></a>
        							<a href="<?php echo site_url('list_fpjt/printFPJT').'/'.$ord ?>" target="_blank" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="Print"><i class="fa fa-print"></i></a>
        						</td>
        					</tr>
        						<?php
        					}
                        }else{
                            $from = $_GET['from'];
                            $to = $_GET['to'];

                            $from = date_create($from);
                            $from = date_format($from, "Y-m-d");
                            $to = date_create($to);
                            $to = date_format($to, "Y-m-d");
							$to = date('Y-m-d',strtotime($to. "+1 days"));

                            $getfpjt = $this->db->query("SELECT tb_fpjt.* FROM tb_fpjt
								LEFT JOIN master_jenis_permata ON master_jenis_permata.id = tb_fpjt.jenis_perhiasan
								LEFT JOIN tb_login ON tb_login.id = tb_fpjt.create_by
								WHERE
                                tb_fpjt.create_date >= '$from 12:00:00 AM' AND tb_fpjt.create_date <= '$to 11:59:59 PM'
                                AND ISNULL(tb_fpjt.delete_by)
								AND tb_login.store='".sessionValue('kode_store')."'
                                GROUP BY tb_fpjt.id_order ORDER BY create_date DESC");
                            foreach ($getfpjt->result() as $get) {
								$ord= str_replace('/', '-', $get->id_order);
                                ?>
                            <tr>
                                <td><?php echo $no++; ?></td>
                                <td><?php echo $get->input_date ?></td>
                                <td><?php echo $get->id_order ?></td>
                                <td><?php echo $get->id_member ?></td>
                                <td class="text-right"><?php echo $get->jumlah ?></td>
                                <td class="text-right">
                                <?php
                                $idn = str_replace('/', '-', $get->id_order);
                                ?>
                                    <button class="btn btn-info" onclick="viewData('<?php echo $idn ?>')" data-toggle="tooltip" data-placement="bottom" title="View"><i class="fa fa-eye"></i></button>
                                    <a href="#" class="btn btn-warning disabled" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="fa fa-edit"></i></a>
                                    <a href="#" onclick="deleteFPJT('<?php echo $ord;?>')" class="btn btn-danger" data-toggle="tooltip" data-placement="bottom" title="Delete"><i class="fa fa-trash"></i></a>
                                    <a href="<?php echo site_url('list_fpjt/printFPJT').'/'.$ord ?>" target="_blank" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="Print"><i class="fa fa-print"></i></a>
                                </td>
                            </tr>
                                <?php
                            }
                        }
                        ?>
        				</tbody>
        			</table>
        		</div>
        	</div>
        </section>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="modal-view">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">View BPT</h4>
      </div>
      <div class="modal-body">
        <div class="area-resp"></div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
    function viewData(key, barcode){
        $.ajax({
            type: "POST",
            url: base_url+"list_fpjt/view_data",
            data: "key="+key,
            success:function(response){
                $("#modal-view").modal('show');
                $(".area-resp").html(response);
            }
        });
    }
</script>