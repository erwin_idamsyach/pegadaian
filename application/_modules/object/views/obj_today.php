      <!-- Modal -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" style="width:400px;">
          <div class="modal-content">
            <div class="modal-header" style="background-color:#f8f8f8">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h6 class="modal-title" id="myModalLabel"><b>VIEW OBJECT</b></h6>
            </div>
            <div class="modal-body">
              <div class="lol">
                
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="index">
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>&nbsp;</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-user"></i> Home</a></li>
            <li>Daftar Permintaan</li>
            <li class="active"><?php echo ucfirst($menu) ?></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <?php
          if($menu == "sertifikasi"){
          ?>
          <div class="box">
            <div class="box-header">
              <h5><b>DAFTAR PERMINTAAN SERTIFIKASI</b></h5>
              <hr style="border:1px solid black;margin-bottom:-10px;margin-top:-5px;">
              <br>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><span class="fa fa-minus"></span></button>
                <button class="btn btn-box-tool" data-widget="remove"><span class="fa fa-times"></span></button>
              </div>
            </div><!-- /.box-header -->
            <div class="box-body" style="margin-top:-15px;">
              <?php
                date_default_timezone_get('Asia/Jakarta');
                $tgl = date('Y-m-d');
                $no = 1;
                $per_page = 5;
              ?>
                <table id="example2" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th class="text-center">NO</th>
                      <th class="text-center">DATE</th>
                      <th class="text-center">ID</th>
                      <th class="text-center">COLOR</th>
                      <th class="text-center">CTS</th>
                      <th class="text-center">L (mm)</th>
                      <th class="text-center">W (mm)</th>
                      <th class="text-center">H (mm)</th>
                      <th class="text-center">ACTION</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php
                    $query = $this->db->query($sql);
                    foreach ($query->result_array() as $i) {
                    $tglx = date('H:i A', strtotime($i['input_date']));
                    if($i['certificate'] != '' || $i['gem_card'] != ''){
                  ?>
                    <tr>
                      <td style="text-align:center"><?php echo $no++?></td>
                      <td class="text-center"><?php echo $i['input_date'] ?></td>
                      <td><?php echo $i['id_object']?></td>
                      <td style="text-align:center"><a class="btn btn-default btn-lg" style="background-color:#<?php echo $i['rgb_code']?>;"></a></td>
                      <td style="text-align:center"><?php echo $i['obj_weight']?></td>
                      <td style="text-align:center"><?php echo $i['obj_length']?></td>
                      <td style="text-align:center"><?php echo $i['obj_width']?></td>
                      <td style="text-align:center"><?php echo $i['obj_height']?></td>
                      <td style="text-align:right;width:200px;">
                        <a onclick="view('<?php echo $i['id_object'] ?>')" class="btn btn-info" data-toggle="tooltip" data-placement="bottom" title="View Object"><i class="fa fa-eye"></i></a>
                        <a onclick="gocedit('<?php echo $i['id_object'] ?>')" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Start Examintion"><i class="fa fa-plus"></i></a>
                        <a href="./object/delete_obj/<?php echo $i['id_object'] ?>" class="btn btn-danger" onclick="return confirm('Apakah anda yakin akan menghapus data ini?')" data-toggle="tooltip" data-placement="bottom" title="Delete Object"><i class="fa fa-trash"></i></a>
                      </td>
                    </tr>
                    <?php
                  }else{
                    
                  }
                      }
                    ?>
                  </tbody>
                </table>
              </div>
            </div><!-- /.box-body -->
            <?php
          }else if($menu == 'grading'){
            ?>
            <div class="box">
            <div class="box-header">
              <h5><b>DAFTAR PERMINTAAN GRADING</b></h5>
              <hr style="border:1px solid black;margin-bottom:-10px;margin-top:-5px;">
              <br>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><span class="fa fa-minus"></span></button>
                <button class="btn btn-box-tool" data-widget="remove"><span class="fa fa-times"></span></button>
              </div>
            </div><!-- /.box-header -->
            <div class="box-body" style="margin-top:-15px;">
              <?php
                date_default_timezone_get('Asia/Jakarta');
                $tgl = date('Y-m-d');
                $no = 1;
                $per_page = 5;
              ?>
                <table id="example2" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th class="text-center">NO</th>
                      <th class="text-center">DATE</th>
                      <th class="text-center">ID</th>
                      <th class="text-center">COLOR</th>
                      <th class="text-center">CTS</th>
                      <th class="text-center">L (mm)</th>
                      <th class="text-center">W (mm)</th>
                      <th class="text-center">H (mm)</th>
                      <th class="text-center">ACTION</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php
                    $query = $this->db->query($sql);
                    foreach ($query->result_array() as $i) {
                    $tglx = date('H:i A', strtotime($i['input_date']));
                  ?>
                    <tr>
                      <td style="text-align:center"><?php echo $no++?></td>
                      <td class="text-center"><?php echo $i['input_date'] ?></td>
                      <td><?php echo $i['id_object']?></td>
                      <td style="text-align:center"><a class="btn btn-default btn-lg" style="background-color:#<?php echo $i['rgb_code']?>;"></a></td>
                      <td style="text-align:center"><?php echo $i['obj_weight']?></td>
                      <td style="text-align:center"><?php echo $i['obj_length']?></td>
                      <td style="text-align:center"><?php echo $i['obj_width']?></td>
                      <td style="text-align:center"><?php echo $i['obj_height']?></td>
                      <td style="text-align:right;width:200px;">
                        <a onclick="view('<?php echo $i['id_object'] ?>')" class="btn btn-info" data-toggle="tooltip" data-placement="bottom" title="View Object"><i class="fa fa-eye"></i></a>
                        <a onclick="godadit('<?php echo $i['id_object'] ?>')" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Start Examintion"><i class="fa fa-plus"></i></a>
                        <a href="./object/delete_obj/<?php echo $i['id_object'] ?>" class="btn btn-danger" onclick="return confirm('Apakah anda yakin akan menghapus data ini?')" data-toggle="tooltip" data-placement="bottom" title="Delete Object"><i class="fa fa-trash"></i></a>
                      </td>
                    </tr>
                    <?php
                      }
                    ?>
                  </tbody>
                </table>
              </div>
            </div><!-- /.box-body -->
            <?php
          }else{
            ?>
            <div class="box">
            <div class="box-header">
              <h5><b>DAFTAR PERMINTAAN TAKSIRAN</b></h5>
              <hr style="border:1px solid black;margin-bottom:-10px;margin-top:-5px;">
              <br>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><span class="fa fa-minus"></span></button>
                <button class="btn btn-box-tool" data-widget="remove"><span class="fa fa-times"></span></button>
              </div>
            </div><!-- /.box-header -->
            <div class="box-body" style="margin-top:-15px;">
              <?php
                date_default_timezone_get('Asia/Jakarta');
              ?>
                <table id="example2" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th class="text-center">NO</th>
                      <th class="text-center">DATE</th>
                      <th class="text-center">COLOR</th>
                      <th class="text-center">JENIS PERHIASAN</th>
                      <th class="text-center" width="30px">JUMLAH PERMATA</th>
                      <th class="text-center">ACTION</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php
					$no = 1;
                    $query = $this->db->query($sql);
                    foreach ($query->result_array() as $i) {
                    $tglx = date('Y-m-d H:i:s', strtotime($i['input_date']));
					$id_order = str_replace('/','-',$i['id_order']);
                  ?>
                    <tr>
                      <td style="text-align:center"><?php echo $no++?></td>
                      <td class="text-center"><?php echo $tglx ?></td>
                      <td style="text-align:center"><a class="btn btn-default btn-lg" style="background-color:#<?php echo $i['rgb_code']?>;"></a></td>
                      <td><?php echo $i['jenis'] ?></td>
                      <td class="text-center"><?php echo $i['jumlah_permata'] ?></td>
                      <td style="text-align:right;width:200px;">
                        <a onclick="view('<?php echo $i['id'] ?>')" class="btn btn-info" data-toggle="tooltip" data-placement="bottom" title="View Object"><i class="fa fa-eye"></i></a>
                        <a onclick="startTaksiran('<?php echo $i['id'] ?>','<?php echo $id_order; ?>')" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Start Examintion"><i class="fa fa-plus"></i></a>
                        <a href="./object/delete_obj/<?php echo $i['id'] ?>" class="btn btn-danger" onclick="return confirm('Apakah anda yakin akan menghapus data ini?')" data-toggle="tooltip" data-placement="bottom" title="Delete Object"><i class="fa fa-trash"></i></a>
                      </td>
                    </tr>
                    <?php
                      }
                    ?>
                  </tbody>
                </table>
              </div>
            </div><!-- /.box-body -->
            <?php
          }
            ?>
          </section><!-- /.content -->
        </div><!-- /.content-wrapper -->
      </div>
      <script>
        $("#example2").DataTable();
      </script>