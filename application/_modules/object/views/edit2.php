<?php
$ox = mysql_query("SELECT * FROM tb_lab_grading where id_object='$id'");
$yi = mysql_fetch_array($ox);
$o = mysql_query("SELECT * FROM tb_front_desk where id_object='$id'");
$m = mysql_fetch_array($o);
?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <div class="modal fade" id="myModalxv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
          <div class="modal-dialog">
            <div class="loginmodal-container">
              <center><label style="font-size:20px;">Specific Gravity</label></center><br>
                <div class="col-md-12">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Berat Kering</label>
                        <input type="text" class="form-control" id="bk" name="bk" value="<?php echo $m['obj_weight']; ?>" placeholder="Berat Kering ...">
                      </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Berat Basah</label>
                          <input type="text" class="form-control" id="bb" name="bb" value="" placeholder="Berat Basah ...">
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <input type="submit" name="login" class="login loginmodal-submit" value="Hitung" onclick="hitung()">
                        </div>
                      </div>
                      <div id="hasil_hitung">
                        
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        <!-- Main content -->
  <section class="content">

    <div class="box box-warning">
      <div class="box-body">
        <div class="col-md-12">
          <div class="row">
            <div class="form-group">
              <div class="col-md-12 text-right">            
                <h4><b>F-02 - Diamond Grading</b></h4>
              </div>
            </div>
          </div>
        </div>
      <form id="form-examination-grading" method="post" enctype="multipart/form-data">
        <div class="col-md-12">
          <div class="row">
            <div class="form-group">
              <div class="col-md-12">
                  <h5><b>DATA GEMOLOG</b></h5>
                  <hr style="border:1px solid black;margin-top:-5px;">
              </div>
            </div>
          </div>

            <div>
              <div class="col-md-12">
                <div class="row">
                  <div class="col-md-3 col-sm-6">
                    <div class="form-group has-success">
                      <label>Gemologist Name <font color="red">*</font></label>
                      <select name="gemolog" class="form-control" required>
                  <?php
                  $getGemolog = $this->db->query("SELECT nama nama_gemolog FROM tb_login WHERE store='".sessionValue('kode_store')."' AND access_level='Gemologist' AND delete_by='' ");
                  foreach ($getGemolog->result() as $gemo) {
                    if($gemo->nama_gemolog == $yi['gemolog']){
                      ?>
                      <option value="<?php echo $gemo->nama_gemolog; ?>" selected><?php echo $gemo->nama_gemolog ?></option>
                      <?php
                    }else{
                      ?>
                      <option value="<?php echo $gemo->nama_gemolog; ?>"><?php echo $gemo->nama_gemolog ?></option>
                      <?php
                    }
                  }
                  ?>
                </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <div class="col-md-12">
          <div class="row">
            <div class="form-group">
              <div class="col-md-12">
                <div class="pull-right">
                  <a onclick="front()" id="minfro"><span class="fa fa-minus fa-2x"></span></a> &nbsp;
                  <a onclick="frontx()" id="plufro"><span class="fa fa-plus fa-2x"></span></a>
                </div>
                  <h5><b>FRONT DESK</b></h5>
                  <hr style="border:1px solid black;margin-top:-5px;">
              </div>
            </div>
          </div>

          <div id="frontc">
            <div class="col-md-12">
              <div class="row">
                <div class="col-md-6" style="border:2px solid #f39c12;padding:10px;border-radius:10px;">
                  <div class="row">

                    <div class="col-lg-5 col-md-5 col-xs-12">
                      <div class="form-group has-success">
                      <?php
                      if($yi['obj_weight'] < 10){
                        $wei = "000".$yi['obj_weight'];
                      }else if($yi['obj_weight'] < 100){
                        $wei = "00".$yi['obj_weight'];
                      }else if($yi['obj_weight'] < 1000){
                        $wei = "0".$yi['obj_weight'];
                      }else{
                        $wei = $yi['obj_weight'];
                      }
                      ?>
                        <label class="control-label" for="inputSuccess">Weight <font color="red">*</font></label>
                        <input type="text" name="weight" id="obj_weight" class="form-control" placeholder="Weight" value="<?php echo $wei ?>" required>
                      </div>
                    </div>

                    <div class="col-lg-5 col-md-5 col-xs-12">
                      <div class="form-group has-success">
                      <?php
                      if($yi['obj_length'] < 10){
                        $len = "000".$yi['obj_length'];
                      }else if($yi['obj_length'] < 100){
                        $len = "00".$yi['obj_length'];
                      }else if($yi['obj_length'] < 1000){
                        $len = "0".$yi['obj_length'];
                      }else{
                        $len = $yi['obj_length'];
                      }
                      ?>
                        <label class="control-label" for="inputSuccess">Length / Diameter Max <font color="red">*</font></label>
                        <input type="text" class="form-control" name="length" id="obj_length" placeholder="Length" value="<?php echo $len ?>" onkeyup="count_dim()" required>
                      </div>
                    </div>

                    <div class="col-lg-5 col-md-5 col-xs-12">
                      <div class="form-group has-success">
                      <?php
                      if($yi['obj_width'] < 10){
                        $wid = "000".$yi['obj_width'];
                      }else if($yi['obj_width'] < 100){
                        $wid = "00".$yi['obj_width'];
                      }else if($yi['obj_width'] < 1000){
                        $wid = "0".$yi['obj_width'];
                      }else{
                        $wid = $yi['obj_width'];
                      }
                      ?>
                        <label class="control-label" for="inputSuccess"> Width / Diameter Min <font color="red">*</font></label>
                        <input type="text" class="form-control" name="width" id="obj_width" placeholder="Width" value="<?php echo $wid ?>" onkeyup="count_dim()" required>
                      </div>
                    </div>

                    <div class="col-lg-5 col-md-5 col-xs-12">
                      <div class="form-group has-success">
                      <?php
                      if($yi['obj_height'] < 10){
                        $hei = "000".$yi['obj_height'];
                      }else if($yi['obj_height'] < 100){
                        $hei = "00".$yi['obj_height'];
                      }else if($yi['obj_height'] < 1000){
                        $hei = "0".$yi['obj_height'];
                      }else{
                        $hei = $yi['obj_height'];
                      }
                      ?>
                        <label class="control-label" for="inputSuccess"> Height / Depth <font color="red">*</font></label>
                        <input type="text" class="form-control" name="height" id="obj_height" placeholder="Height" value="<?php echo $hei ?>" required>
                      </div>
                    </div>
                  </div>
                </div>
                  <div class="col-md-3">
                    <div class="form-group" style="border: 1px solid black;border-radius:10px;">
                        <center>
                          <input type="file" class="form-control drop" name="image" data-default-file="<?php echo base_url()."asset/images/".$yi['obj_image'] ?>" data-height="153px">
                        </center>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <?php
                      $q = mysql_query("SELECT * FROM tb_lab_grading a
										LEFT JOIN color_stone b ON b.code=a.obj_color 
										WHERE a.id_object='$id'");
                      $xf = mysql_fetch_array($q);
                    ?>
                    <div class="form-group" style="border:2px solid black;padding:10px;border-radius:10px;">
                      <div id="div_color" style="background-color:#<?php echo $xf['rgb_code']?>;height:148px;padding-top:50px;border-radius:10px;">
                      <center>
                        <div style="mix-blend-mode: difference;" id="div_color_text">
                          <?php $color = explode(";", $xf['color']); echo $color[0] ?>
                        </div>
                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modalCol">EDIT</button>
                      </center>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
    <!-- input states -->
      <div class="col-md-12">
        <div class="row">
          <div class="form-group">
            <div class="col-md-12">
                <h5><b>MEASUREMENT & WEIGHT</b></h5>
                <hr style="border:1px solid black;margin-top:-5px;">
            </div>
          </div>
        </div>

          <div id="physy">
            <div class="col-md-12" style="width:100%;">
              <div class="row">
                <div class="col-md-12">
                  <div class="row">
                    <div class="col-md-2 col-sm-4 col-xs-6">
                      <div class="form-group has-success">
                        <label>Diameter Average</label>
                        <input type="number" class="form-control"  step="0.01" name="diameter_average" id="diameter-avg" readonly="" value="<?php echo $yi['diameter_avg'] ?>">
                      </div>
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6">
                      <div class="form-group has-success">
                        <label>Girdle Thickness Min.</label>
                        <select name="girdle_min" class="form-control">
                          <option value="" selected disabled>--select--</option>
                          <?php
                          $getGirdle = $this->db->query("SELECT * FROM diamond_girdle WHERE ISNULL(delete_by) ORDER BY id DESC");
                          foreach ($getGirdle->result() as $gir) {
                            if($gir->girdle_grade == $yi['girdle_min']){
                              ?>
                              <option value="<?php echo $gir->girdle_grade ?>" selected><?php echo $gir->girdle_grade ?></option>
                              <?php
                            }else{
                              ?>
                              <option value="<?php echo $gir->girdle_grade ?>"><?php echo $gir->girdle_grade ?></option>
                              <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6">
                      <div class="form-group has-success">
                        <label>Girdle Thickness Max.</label>
                        <select name="girdle_max" class="form-control">
                          <option value="" selected disabled>--select--</option>
                          <?php
                          $getGirdle = $this->db->query("SELECT * FROM diamond_girdle WHERE ISNULL(delete_by) ORDER BY id DESC");
                          foreach ($getGirdle->result() as $gir) {
                            if($gir->girdle_grade == $yi['girdle_min']){
                              ?>
                              <option value="<?php echo $gir->girdle_grade ?>" selected><?php echo $gir->girdle_grade ?></option>
                              <?php
                            }else{
                              ?>
                              <option value="<?php echo $gir->girdle_grade ?>"><?php echo $gir->girdle_grade ?></option>
                              <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6">
                      <div class="form-group has-success">
                        <label>Girdle Thickness Avg.</label>
                        <select name="girdle_avg" class="form-control">
                          <option value="" selected disabled>--select--</option>
                          <?php
                          $getGirdle = $this->db->query("SELECT * FROM diamond_girdle WHERE ISNULL(delete_by) ORDER BY id DESC");
                          foreach ($getGirdle->result() as $gir) {
                            if($gir->girdle_grade == $yi['girdle_min']){
                              ?>
                              <option value="<?php echo $gir->girdle_grade ?>" selected><?php echo $gir->girdle_grade ?></option>
                              <?php
                            }else{
                              ?>
                              <option value="<?php echo $gir->girdle_grade ?>"><?php echo $gir->girdle_grade ?></option>
                              <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6">
                      <div class="form-group has-success">
                        <label>Depth Percentage</label>
                        <input type="number" class="form-control"  max="100" step="0.01" name="depth_percentage" value="<?php echo $yi['depth_percentage'] ?>">
                      </div>
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6">
                      <div class="form-group has-success">
                        <label>Actual Weight</label>
                        <input type="number" class="form-control"  step="0.01" name="actual_weight" value="<?php echo $yi['actual_weight'] ?>">
                      </div>
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6">
                      <div class="form-group has-success">
                        <label>Specific Gravity</label>
                        <input type="number" class="form-control"  step="0.01" name="specific" value="<?php echo $yi['specific_gravity'] ?>">
                      </div>
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6">
                      <div class="form-group has-success">
                        <label>Table Percentage</label>
                        <input type="number" class="form-control"  step="0.01" name="table_percentage"  value="<?php echo $yi['table_percentage'] ?>">
                      </div>
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6">
                      <div class="form-group has-success">
                        <label>Crown Angles</label>
                        <input type="number" class="form-control"  step="0.01" name="crown_angles" value="<?php echo $yi['crown_angles'] ?>">
                      </div>
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6">
                      <div class="form-group has-success">
                        <label>Pavillion Depth</label>
                        <input type="number" class="form-control"  step="0.01" name="pavillion_depth" value="<?php echo $yi['pavillion_depth'] ?>">
                      </div>
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6">
                      <div class="form-group has-success">
                        <label>Culet Size</label>
                        <select name="culet_size" class="form-control">
                          <option value="" selected disabled>--select--</option>
                          <?php
                          $getCulet = $this->db->query("SELECT * FROM diamond_culet WHERE ISNULL(delete_by)");
                          foreach ($getCulet->result() as $cule) {
                            if($cule->culet_grade == $yi['culet_size']){
                              ?>
                              <option value="<?php echo $cule->culet_grade ?>" selected><?php echo $cule->culet_grade ?></option>
                              <?php
                            }else{
                              ?>
                              <option value="<?php echo $cule->culet_grade ?>"><?php echo $cule->culet_grade ?></option>
                              <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      <div class="col-md-12">
        <div class="row">
          <div class="form-group">
            <div class="col-md-12">
                <h5><b>GRADING</b></h5>
                <hr style="border:1px solid black;margin-top:-5px;">
            </div>
          </div>
        </div>

          <div id="physy">
            <div class="col-md-12" style="width:100%;">
              <div class="row">
                <div class="col-md-12">
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group has-success">
                        <label>Cut <font color='red'>*</font></label>
                        <select class="form-control select2" style="width:100%;" id="transparent" name="cut" required>
							<option selected disabled>--select--</option>
                          <?php
							$getCut = $this->db->query("SELECT * FROM cut");
							foreach($getCut->result() as $cut){
								if($cut->cut == $yi['cut']){
									?>
									<option value="<?php echo $cut->cut ?>" selected><?php echo $cut->cut ?></option>
									<?php
								}else{
									?>
									<option value="<?php echo $cut->cut ?>"><?php echo $cut->cut ?></option>
									<?php
								}
							}
                          ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group has-success">
                        <label>Shape <font color='red'>*</font></label>
                        <select class="form-control select2" style="width:100%;" id="transparent" name="shape" required>
                          <option selected disabled>--select--</option>
                          <?php
							$getCut = $this->db->query("SELECT * FROM diamond_cut_type");
							foreach($getCut->result() as $cut){
								if($cut->cut_type == $yi['shape']){
									?>
									<option value="<?php echo $cut->cut_type ?>" selected><?php echo $cut->cut_type ?></option>
									<?php
								}else{
									?>
									<option value="<?php echo $cut->cut_type ?>"><?php echo $cut->cut_type ?></option>
									<?php
								}
							}
                          ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group has-success">
                        <label>Color Grading <font color='red'>*</font></label>
                        <select class="form-control select2" style="width:100%;" id="transparent" name="color" required>
                          <?php
                          $color = $yi['color_grading'];
                          if ($color == "") {
                            ?>
                              <option value="">---select---</option>
                            <?php
                          }else{
                          $zx = mysql_query("SELECT color_grading FROM tb_lab_grading where color_grading='$color'");
                          $yx = mysql_fetch_array($zx);
                          ?>
                          <option value="<?php echo $yx['color_grading']?>"><?php echo $yx['color_grading']?></option>
                          <?php
                          }
                          $xz = mysql_query("SELECT * FROM diamond_color where color_code!='$color' order by color_code");
                          while ($xy = mysql_fetch_array($xz)){
                            ?>
                            <option value="<?php echo $xy['color_code']?>"><?php echo $xy['color_code']." (".$xy['color_name'].")"?></option>
                            <?php
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group has-success">
                        <label>Clarity Grade <font color='red'>*</font></label>
                        <select class="form-control select2" style="width:100%;" id="transparent" name="clarity" required>
                          <?php
                          $clarity = $yi['clarity'];
                          if ($clarity == "") {
                            ?>
                              <option value="">---select---</option>
                            <?php
                          }else{
                          $zx = mysql_query("SELECT clarity FROM tb_lab_grading where clarity='$clarity'");
                          $yx = mysql_fetch_array($zx);
                          ?>
                          <option value="<?php echo $yx['clarity']?>"><?php echo $yx['clarity']?></option>
                          <?php
                          }
                          $xz = mysql_query("SELECT * FROM diamond_clarity where clarity_grade!='$clarity' order by clarity_grade");
                          while ($xy = mysql_fetch_array($xz)){
                            ?>
                            <option value="<?php echo $xy['clarity_grade']?>"><?php echo $xy['clarity_grade']." (".$xy['inform'].")"?></option>
                            <?php
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group has-success">
                        <label>Cut Grade <font color='red'>*</font></label>
                        <select class="form-control select2" style="width:100%;" id="transparent" name="cut_grade" required>
                          <?php
                          $cut_grade = $yi['cut_grade'];
                          if ($cut_grade == "") {
                            ?>
                              <option value="">---select---</option>
                            <?php
                          }else{
                          $zx = mysql_query("SELECT cut_grade FROM tb_lab_grading where cut_grade='$cut_grade'");
                          $yx = mysql_fetch_array($zx);
                          ?>
                          <option value="<?php echo $yx['cut_grade']?>"><?php echo $yx['cut_grade']?></option>
                          <?php
                          }
                          $xz = mysql_query("SELECT * FROM diamond_cut where cut_grade!='$cut_grade' order by cut_grade");
                          while ($xy = mysql_fetch_array($xz)){
                            ?>
                            <option value="<?php echo $xy['cut_grade']?>"><?php echo $xy['cut_grade']?></option>
                            <?php
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group has-success has-feedback">
                      <label class="control-label" for="inputSuccess">Girdle <font color='red'>*</font></label>
                      <select name="girdle" class="form-control">
                        <option value="" selected disabled>--select--</option>
                        <?php
                          $getGirdle = $this->db->query("SELECT * FROM diamond_girdle WHERE ISNULL(delete_by) ORDER BY id DESC");
                          foreach ($getGirdle->result() as $gir) {
                            if($gir->girdle_grade == $yi['girdle']){
                              ?>
                              <option value="<?php echo $gir->girdle_grade ?>" selected><?php echo $gir->girdle_grade ?></option>
                              <?php
                            }else{
                              ?>
                              <option value="<?php echo $gir->girdle_grade ?>"><?php echo $gir->girdle_grade ?></option>
                              <?php
                            }
                          }
                          ?>
                      </select>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group has-success has-feedback">
                      <label class="control-label" for="inputSuccess">Culet <font color='red'>*</font></label>
                      <select name="culet" class="form-control" required>
                      <option value="" selected disabled>--select--</option>
                      <?php
                          $getCulet = $this->db->query("SELECT * FROM diamond_culet WHERE ISNULL(delete_by)");
                          foreach ($getCulet->result() as $cule) {
                            if($cule->culet_grade == $yi['culet']){
                              ?>
                              <option value="<?php echo $cule->culet_grade ?>" selected><?php echo $cule->culet_grade ?></option>
                              <?php
                            }else{
                              ?>
                              <option value="<?php echo $cule->culet_grade ?>"><?php echo $cule->culet_grade ?></option>
                              <?php
                            }
                          }
                          ?>
                      </select>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group has-success has-feedback">
                      <label class="control-label" for="inputSuccess">Fluorescence <font color='red'>*</font></label>
                      <select name="fluorescence" class="form-control" required>
                      <option value="" selected disabled>--select--</option>
                      <?php
                          $getFluo = $this->db->query("SELECT * FROM diamond_fluorescence WHERE ISNULL(delete_by)");
                          foreach ($getFluo->result() as $cule) {
                            if($cule->fluorescence == $yi['fluorescence']){
                              ?>
                              <option value="<?php echo $cule->fluorescence ?>" selected><?php echo $cule->fluorescence ?></option>
                              <?php
                            }else{
                              ?>
                              <option value="<?php echo $cule->fluorescence ?>"><?php echo $cule->fluorescence ?></option>
                              <?php
                            }
                          }
                          ?>
                      </select>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group has-success has-feedback">
                      <label class="control-label" for="inputSuccess">Proportions</label>
                      <input type="text" name="proportions" class="form-control" placeholder="Proportions" value="<?php echo $yi['proportions'] ?>">
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group has-success has-feedback">
                      <label class="control-label" for="inputSuccess">Polish Grade</label>
                      <input type="text" name="polish_grade" class="form-control" placeholder="Polish grade" value="<?php echo $yi['polish_grade'] ?>">
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group has-success has-feedback">
                      <label class="control-label" for="inputSuccess">Symmetry Grade</label>
                      <input type="text" name="symmetry_grade" class="form-control" placeholder="Symmetry Grade" value="<?php echo $yi['symmetry_grade'] ?>">
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group has-success has-feedback">
                      <label class="control-label" for="inputSuccess">Keys To Symbols <font color='red'>*</font></label><br/>
                      <?php
                      $broke = explode(",", $yi['key']);
                      $getKey = $this->db->query("SELECT * FROM master_key_symbol WHERE ISNULL(delete_by)");
                      foreach ($getKey->result() as $key) {
                        if(in_array($key->key, $broke)){
                          ?>
                            <div class="checkbox-inline">
                              <label>
                                <input type="checkbox" class="form--control" name="symbol[]" value="<?php echo $key->key ?>" checked> <?php echo $key->key ?>
                              </label>
                            </div>&nbsp;
                          <?php
                        }else{
                          ?>
                            <div class="checkbox-inline">
                              <label>
                                <input type="checkbox" class="form--control" name="symbol[]" value="<?php echo $key->key ?>"> <?php echo $key->key ?>
                              </label>
                            </div>&nbsp;
                          <?php
                        }
                      }
                      ?>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

                <div class="form-group">
                  <div class="col-md-12">
                      <button type="submit" name="submit" class="btn btn-primary pull-right" style="margin-bottom:20px;margin-right:10px;"><i class="fa fa-save"></i> Save </button>
                      <button type="reset" class="btn btn-default pull-right" style="margin-bottom:20px;margin-right:10px;"> <i class="fa fa-refresh"></i> Clear</button>
                      <a href="" class="btn btn-danger pull-right" style="margin-bottom:20px;margin-right:10px;"><i class="fa fa-chevron-left"></i> Back</a>
                  </div>
                </div>
              <!-- END -->
              </form>
          <?php
          $xf = $id;
          ?>
          <div class="modal fade" id="modalEdit">
            <div class="modal-dialog modal-sm">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <div class="modal-title" id="myModalLabel">Edit Image</div>
                </div>
                <div class="modal-body">
                  <div class="row">
                    <div class="col-md-12 col-sm-12">
                      <div class="form-group">
                        <form action="data-ajax/save_img.php" id="frmuploadImgx" method="post" target="iframeUploadImgx" enctype="multipart/form-data">
                          <input type="file" class="drop form-control" name="gambar" id="drop" onchange="submitImagex()" data-default-file="./asset/images/<?php echo $yi['obj_image'] ?>">
                        </form>
                      </div>
                    </div>
                      <input class="hidden" type="text" name="gambaroutx" id="gambaroutx" value="<?php echo $yi['obj_image'] ?>">
                      <iframe class="hidden" name="iframeUploadImgx" id="iframeUploadImgx"></iframe> 
                      <div class="col-md-12 text-right">
                        <button class="btn btn-primary" onclick="edit_gambar('<?php echo $xf?>')">SAVE</button>
                      </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="modal fade" id="modalFro">
            <div class="modal-dialog modal-sm">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <div class="modal-title" id="myModalLabel">Edit Data Front</div>
                </div>
                <div class="modal-body">
                <div class="row">
                <?php
                $k = mysql_query("SELECT * FROM tb_front_desk where id_object='$id'");
                $x = mysql_fetch_array($k);
                ?>
                  <div class="col-lg-6 col-md-6 col-xs-12">
                      <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess">Weight</label>
                        <input type="number" step="0.01" name="weight" id="cts" class="form-control" value="<?php echo $x['obj_weight']?>" placeholder="Enter..." required>
                      </div>
                    </div>

                    <div class="col-lg-6 col-md-6 col-xs-12">
                      <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess">Length</label>
                        <input type="number" step="0.01" class="form-control" id="lmm" name="length" value="<?php echo $x['obj_length']?>" placeholder="Enter..." required>
                      </div>
                    </div>

                    <div class="col-lg-6 col-md-6 col-xs-12">
                      <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess"> Width</label>
                        <input type="number" step="0.01" class="form-control" id="wmm" name="width" value="<?php echo $x['obj_width']?>" placeholder="Enter..." required>
                      </div>
                    </div>

                    <div class="col-lg-6 col-md-6 col-xs-12">
                      <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess"> Height</label>
                        <input type="number" step="0.01" class="form-control" id="hmm" name="height" value="<?php echo $x['obj_height']?>" placeholder="Enter..." required>
                      </div>
                    </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <button class="btn btn-primary" type="submit" onclick="save_dimen('<?php echo $id?>')">SAVE</button>
                    </div>
                  </div>
                </div>
                </div>
              </div>
            </div>
          </div>
          <div class="modal fade" id="modalCol">
            <div class="modal-dialog modal-md">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <div class="modal-title" id="myModalLabel">Edit Color</div>
                </div>
                <div class="modal-body">
                  <div class="row">
                      <div class="col-md-6">
                        <div class="form-group has-success">
                          <label>Color</label>
                          <select name="colr" id="obj" class="form-control" onchange="chos()" required>
                            <option>Select color..</option>
                              <?php
                                $col = mysql_query("SELECT * FROM color_stone GROUP BY jenis_warna");
                                while ($ge = mysql_fetch_array($col)) {
                              ?>
                              <?php
                                if($ge['jenis_warna'] == "Select color.."){
                              ?>
                            <option value="<?php echo $ge['code'] ?>" selected style="text-transform: capitalize;"><?php echo $ge['jenis_warna'] ?></option>  
                              <?php
                                }else{
                              ?>
                            <option value="<?php echo $ge['code'] ?>" style="text-transform: capitalize;"><?php echo $ge['jenis_warna'] ?></option>
                              <?php
                                  }
                              }
                              ?>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group has-success">
                          <label>Specific Color</label>
                          <div style="min-height: 120px;">
                            <div class="btn-group" id="col-area" data-toggle="buttons">
                              -
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <button class="btn btn-primary" onclick="setColor()">SAVE</button>
                        </div>
                      </div>
                   </div>
                  </div>
                </div>
              </div>
            </div>
    
    <script>
	var id = "<?php echo $id; ?>";
  $(function(){
	$.mask.definitions['d'] = '[0-9.]';
	$("#obj_weight, #obj_length, #obj_width, #obj_height").mask("9?ddd.dd",{placeholder:"____.00"});
    $('#frontc').hide();
    $('#minfro').hide();
	
			$('#form-examination-grading').trigger("reset");
			$("#form-examination-grading").on('submit',function(e){
			e.preventDefault();
			var formatData = new FormData($(this)[0]);
				swal({
				 title: "",
				 text: "Apakah Anda akan menyimpan data?",
				 type: "warning",
				 showCancelButton: true,
				 confirmButtonColor: "#DD6B55",
				 confirmButtonText: "Ya",
				 cancelButtonText: "Tidak",
				 closeOnConfirm: false }, function(){
					$.ajax({
						type : 'POST',
						url : '<?php echo base_url() ?>object/update_gd/<?php echo $id?>',
						data : formatData,
						async: false,
						cache: false,
						contentType: false,
						processData: false,
						success:function(html){
							var data = eval ("(" + html + ")");
							if(data.success){
								swal({
								 title: "Berhasil Menyimpan data!",
								 text: "Klik tombol di bawah.",
								 type: "success",
								 showCancelButton: false,
								 confirmButtonColor: "#257DB6",
								 confirmButtonText: "Ok!",
								 closeOnConfirm: false
								},
								function(){
									window.location.reload();
								});
							}else{
								swal({
								 title: data.msg,
								 text: "Klik tombol di bawah.",
								 type: "warning",
								 showCancelButton: false,
								 confirmButtonColor: "#257DB6",
								 confirmButtonText: "Ok!",
								 closeOnConfirm: false
								});
							}
						}
					});
				});
			});
  });
  function frontx() {
    $('#minfro').show();
    $('#plufro').hide();
    $('#frontc').show(700);
  }
  function front() {
    $('#minfro').hide();
    $('#plufro').show();
    $('#frontc').hide(700);
  }
  function count_dim(){
    var min = $("#obj_width").val();
    var max = $("#obj_length").val();
    var avg = (parseFloat(min)+parseFloat(max))/2;
    $("#diameter-avg").val(avg);
  }
  function count_girdle(){
    var min = $("#gird_min").val();
    var max = $("#gird_max").val();
    var avg = (parseFloat(min)+parseFloat(max))/2;
    $("#gird_avg").val(avg);
  }
    function submit() {
      $('#coba').html('Gambar Sudah Ditetapkan');
    }
    $("#drop").dropify();

    $(document).on('keypress',function(e){
      if(e.keyCode==13){
          e.preventDefault();
      }
    });
    function submitImagex(){
      $( "#frmuploadImgx" ).submit();
    }

    function setColor(){
      var color = $(".coll:checked").val();
	  
      $.ajax({
        type : "POST",
        url : base_url+"object/color_grading",
        data : {
          "warna" : color, "id_obj" : id
        },
        success:function(response){ 
			var data = eval ("(" + response + ")");
			if(data.success){
				$("#modalCol").modal('hide');
				$("#div_color").css("background-color","#"+data.rgb_color);
				$("#div_color_text").html(data.color_spek);
			}
			
        }
      });
    }

    $(".drop").dropify();
    </script>