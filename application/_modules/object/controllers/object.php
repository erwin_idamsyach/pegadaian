<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class object extends CI_Controller{
    function object(){
        parent::__construct();
		
		if(!isLogin()){
			goLogin();
		}
    }
    
    /*function index($text=NULL){
        $menu = $this->uri->segment(2);
        echo $menu;
        if($menu == "sertifikasi"){
            echo "YES";
        }else if($menu == "grading"){

        }else if($menu == "taksiran"){

        }else{
            redirect('home');
        }

        $data["filelist"] = "object/obj_today";
        $data["title"] = "Lab";
        $data["title_menu"] = "Lab";
        $data["menu"] = "sertifikasi";
		
        getHTMLWeb($data);
    }*/
    function sertifikasi(){
        $kode               = sessionValue('kode_store');
        $data['sql']      = "SELECT * FROM tb_front_desk a
							LEFT JOIN step b ON b.id_object = a.id_object
							LEFT JOIN color_stone c ON c.code = a.obj_color
                            WHERE
                            b.step = 'FRONT_DESK' AND
                            a.store='$kode' AND ISNULL(a.delete_by)
							AND b.status='NOT APPROVED'
							AND b.status_barang='TIDAK TERTINGGAL'
                            ORDER BY a.create_date ASC";
        $data["filelist"]   = "object/obj_today";
        $data["title"]      = "Lab";
        $data["title_menu"] = "Lab";
        $data["menu"]       = "sertifikasi";
        
        getHTMLWeb($data); 
    }

    function grading(){
        $kode               = sessionValue('kode_store');
        $data['sql']      = "SELECT * FROM tb_front_desk a
							LEFT JOIN step b ON b.id_object = a.id_object
							LEFT JOIN color_stone c ON c.code = a.obj_color
                            WHERE
                            a.store='$kode'
							AND b.dia_grading != '' AND b.check_dg = ''
							AND ISNULL(b.delete_by)
							AND b.status='NOT APPROVED'
							AND b.status_barang='TIDAK TERTINGGAL'
                            ORDER BY a.create_date ASC";
        $data["filelist"]   = "object/obj_today";
        $data["title"]      = "Lab";
        $data["title_menu"] = "Lab";
        $data["menu"]       = "grading";
        
        getHTMLWeb($data); 
    }

    function taksiran(){
        $kode               = sessionValue('kode_store');
		$data['sql']      = "SELECT tb_fpjt.*,
							master_permata.permata,
							master_jenis_permata.jenis,
							color_stone.rgb_code, tb_member.first_name nama
							FROM tb_fpjt
							LEFT JOIN master_permata ON master_permata.id = tb_fpjt.jenis_permata
							LEFT JOIN master_jenis_permata ON master_jenis_permata.id = tb_fpjt.jenis_perhiasan 
							LEFT JOIN color_stone ON color_stone.code = tb_fpjt.warna_permata 
							LEFT JOIN tb_member ON tb_member.id_member = tb_fpjt.id_member 
							LEFT JOIN step ON step.id_object = tb_fpjt.id
							WHERE 
							tb_fpjt.status='BELUM'
							AND step.status='NOT APPROVED'
							AND step.status_barang='TIDAK TERTINGGAL'
							GROUP BY tb_fpjt.id_order";
							
        $data["filelist"]   = "object/taksiran";
        $data["title"]      = "Lab";
        $data["title_menu"] = "Lab";
        $data["menu"]       = "taksiran";
        
        getHTMLWeb($data);   
    }
	
	function taksiran_detail($id_order=NULL){
		$id_order = str_replace('-','/',$id_order);
		
        $kode               = sessionValue('kode_store');
		$data['sql']      = "SELECT tb_fpjt.*,
							master_permata.permata,
							master_jenis_permata.jenis,
							color_stone.rgb_code
							FROM tb_fpjt
							LEFT JOIN master_permata ON master_permata.id = tb_fpjt.jenis_permata
							LEFT JOIN master_jenis_permata ON master_jenis_permata.id = tb_fpjt.jenis_perhiasan 
							LEFT JOIN color_stone ON color_stone.code = tb_fpjt.warna_permata 
							LEFT JOIN step ON step.id_object = tb_fpjt.id
							WHERE 
							tb_fpjt.status='BELUM'
							AND step.status='NOT APPROVED'
							AND step.status_barang='TIDAK TERTINGGAL'
							AND tb_fpjt.id_order='".$id_order."'";
							
        $data["filelist"]   = "object/obj_today";
        $data["title"]      = "Lab";
        $data["title_menu"] = "Lab";
        $data["menu"]       = "taksiran";
        
        getHTMLWeb($data);   
    }

    function view_obj($id)
    {
        $this->load->view('view_object', array('id'=>$id));
    }

    function finalx()
    {
        $hardness = $this->input->post('hard');
        $ri_start = $this->input->post('ri');
        $sg_end   = $this->input->post('sg');
        $this->load->view('final', array('hardness'=>$hardness,'ri_start'=>$ri_start, 'sg_end'=>$sg_end));
    }

    function ambilbatu()
    {
        $batu = $this->input->post('batu');
        $this->load->view('ambil', array('batu'=>$batu));
    }

    function edit_dg($id)
    {
        $l = $this->db->get_where('tb_lab_grading', array('id_object'=>$id));
        if($l->num_rows() == 0){
            $q = $this->db->query("SELECT * FROM tb_front_desk where id_object='$id'");
            foreach ($q->result_array() as $r) {
            }
            $data = array(
                'id_member'     => $r['id_member'],
                'id_order'      => $r['id_order'],
                'id_object'     => $r['id_object'],
                'obj_weight'    => $r['obj_weight'],
                'obj_height'    => $r['obj_height'],
                'obj_width'     => $r['obj_width'],
                'obj_length'    => $r['obj_length'],
                'obj_color'     => $r['obj_color'],
                'obj_image'     => $r['obj_image']
            );
            $this->db->insert('tb_lab_grading', $data);
        }else{

        }
        $this->load->view('edit2', array('id'=>$id));
    }

    function edit_obj($id)
    {
        $l = $this->db->get_where('tb_lab_desk', array('id_object'=>$id));
        if($l->num_rows() == 0){
            $q = $this->db->query("SELECT * FROM tb_front_desk where id_object='$id'");
            foreach ($q->result_array() as $r) {
				if($r['obj_image']==""){
					$img = "";
				}else{
					$img = $r['obj_image'];
				}
				
				$data = array(
                'id_member'     => $r['id_member'],
                'id_object'     => $r['id_object'],
                'obj_weight'    => $r['obj_weight'],
                'obj_height'    => $r['obj_height'],
                'obj_width'     => $r['obj_width'],
                'obj_length'    => $r['obj_length'],
                'obj_color'     => $r['obj_color'],
                'obj_image'     => $img
				);
				$this->db->insert('tb_lab_desk', $data);
			}
        }else{
			
        }
        $this->load->view('edit', array('id'=>$id));
    }

    function delete_obj($id)
    {
        $user = sessionValue('username');
        date_default_timezone_set('Asia/Jakarta');
        $now  = date('Y-m-d');
        $data = array(
            'delete_by'     => $user,
            'delete_date'   => $now
        );
        $this->db->where('id_object', $id);
        $this->db->update('tb_front_desk', $data);

        $this->db->where('id_object', $id);
        $this->db->update('step', $data);

        redirect('obj_today');
    }

    function update_gd($id)
    {
        $symbol = $this->input->post('symbol');
        $all_imp = implode(",", $symbol);
        date_default_timezone_set('Asia/Jakarta');
        $tgl = date('Y-m-d');
        $now = date('Y-m-d H:i:s');
        $weight         = $this->input->post('weight');
        $length         = $this->input->post('length');
        $width          = $this->input->post('width');
        $height         = $this->input->post('height');
        $shape          = $this->input->post('shape');
        $cut            = $this->input->post('cut');    
        $color          = $this->input->post('color');
        $clarity        = $this->input->post('clarity');
        $cut_grade      = $this->input->post('cut_grade');
        $girdle         = $this->input->post('girdle');
        $culet          = $this->input->post('culet');
        $fluorescence   = $this->input->post('fluorescence');
        /*$key            = $this->input->post('key');*/
		
		$success = false;
		$msg = "";
        if($_FILES['image']['size'] > 0){
            $tmp_loc = $_FILES['image']['tmp_name'];
            $image   = $id.".jpg";
            $folder  = 'asset/images/'.$image;
            move_uploaded_file($tmp_loc, $folder);
        }else{
            $image = '';
        }

        $data = array(
            'obj_weight'    => $weight,
            'obj_length'    => $length,
            'obj_width'     => $width,
            'obj_height'    => $height,
            'obj_image'     => $image,
            'shape'         => $shape,
            'cut'           => $cut,
            'color_grading' => $color,
            'clarity'       => $clarity,
            'cut_grade'     => $cut_grade,
            'girdle'        => $girdle,
            'culet'         => $culet,
            'fluorescence'  => $fluorescence,
            'key'           => $all_imp,
            'finish'        => $now,
            'gemolog'       => $this->input->post('gemolog'),
            'actual_weight'      => $this->input->post('actual_weight'),
            //'diameter_min'       => $this->input->post('diameter_minimum'),
            //'diameter_max'       => $this->input->post('diameter_maximum'),
            'diameter_avg'       => $this->input->post('diameter_average'),
            'girdle_min'       => $this->input->post('girdle_min'),
            'girdle_max'       => $this->input->post('girdle_max'),
            'girdle_avg'       => $this->input->post('girdle_avg'),
            //'depth'            => $this->input->post('depth'),
            'depth_percentage'       => $this->input->post('depth_percentage'),
            'specific_gravity'       => $this->input->post('specific'),
            'table_percentage'       => $this->input->post('table_percentage'),
            'crown_angles'           => $this->input->post('crown_angles'),
            'pavillion_depth'        => $this->input->post('pavillion_depth'),
            'culet_size'             => $this->input->post('culet_size'),
            'proportions'            => $this->input->post('proportions'),
            'polish_grade'           => $this->input->post('polish_grade'),
            'symmetry_grade'         => $this->input->post('symmetry_grade'),
			
			'update_by' => sessionValue('id'),
			'update_date' => $now
        );
        $this->db->where('id_object', $id);
        $this->db->update('tb_lab_grading', $data);

        $qi = $this->db->get_where('step', array('id_object'=>$id));
        foreach ($qi->result_array() as $k) {
        }
        if($k['certificate'] == "" && $k['gem_card'] == "" && $k['dia_grading'] != ""){
            $dax = array(
            'step'      => 'LAB_DESK',
            'check_dg'  => 'Sudah'
            );
            $this->db->where('id_object', $id);
            $this->db->update('step', $dax);
        }else{
            $dax = array(
            'check_dg'  => 'Sudah'
            );
            $this->db->where('id_object', $id);
            $this->db->update('step', $dax);
        }
        
		$success = true;
		
        //redirect(site_url('object/grading'));
		echo json_encode(array("success"=>$success, "msg"=>$msg));
    }

    function update_obj($id)
    {
        date_default_timezone_set('Asia/Jakarta');
        $tgl = date('Y-m-d');
        $now = date('Y-m-d H:i:s');
        $weight         = $this->input->post('weight');
        $length         = $this->input->post('length');
        $width          = $this->input->post('width');
        $height         = $this->input->post('height');
        $shape          = $this->input->post('shape');
        $cut            = $this->input->post('cut');    
        $cut2           = $this->input->post('cut2');
        $hardness       = $this->input->post('hardness');
        $gravity        = $this->input->post('gravity');
        $cleavage       = $this->input->post('cleavage');
        $fracture       = $this->input->post('fracture');
        $luminescence   = $this->input->post('luminescence');
        $tenacity       = $this->input->post('tenacity');
        $radio          = $this->input->post('radio');
        $crystal_gems   = $this->input->post('crystal_gems');
        $transparent    = $this->input->post('transparent');
        $ri_start       = $this->input->post('ri_start');
        $luster         = $this->input->post('luster');
        $pleochroism    = $this->input->post('pleochroism');
        $birefringence  = $this->input->post('birefringence');
        $dispersion     = $this->input->post('dispersion');
        $mineral        = $this->input->post('mineral');
        $strunz         = $this->input->post('strunz');
        $related        = $this->input->post('related');
        $member         = $this->input->post('member');
        $synonyms       = $this->input->post('synonyms');
        $crystallography= $this->input->post('crystallography');
        $crystal        = $this->input->post('crystal');
        $type           = $this->input->post('type');
        $twinning       = $this->input->post('twinning');
        $geological     = $this->input->post('geological');
        $common_asso    = $this->input->post('common_asso');
        $common_impu    = $this->input->post('common_impu');
        $year           = $this->input->post('year');
        $natural        = $this->input->post('natural');
        $phenomenal     = $this->input->post('phenomenal');
        $varieties      = $this->input->post('varieties');
        $batu           = $this->input->post('batu');
        $note           = $this->input->post('note');
        //$note2          = $this->input->post('note2');
        //$note3          = $this->input->post('note3');
        $comment        = $this->input->post('comment');
		
		$success = false;
		$msg = "";
        if($_FILES['image']['size'] > 0){
            $tmp_loc = $_FILES['image']['tmp_name'];
            $image   = $id.".jpg";
            $folder  = 'asset/images/'.$image;
            move_uploaded_file($tmp_loc, $folder);
        }else{
            $image = '';
        }
		
		if($image==""){
			$data = array(
				'obj_weight'    => $weight,
				'obj_length'    => $length,
				'obj_width'     => $width,
				'obj_height'    => $height,
				'phenomenal'    => $phenomenal,
				'crystal_gems'  => $crystal_gems,
				'obj_cut'       => $cut,
				'obj_cut2'      => $cut2,
				'obj_shape'     => $shape,
				'obj_natural'   => $natural,
				'ri_start'      => $ri_start,
				'cleavage'      => $cleavage,
				'fracture'      => $fracture,
				'tenacity'      => $tenacity,
				'luminescence'  => $luminescence,
				'radioactivity' => $radio,
				'hardness'      => $hardness,
				'sg'            => $gravity,
				'mineral'       => $mineral,
				'strunz'        => $strunz,
				'related'       => $related,
				'member'        => $member,
				'synonyms'      => $synonyms,
				'variety'       => $varieties,
				'pleochroism'   => $pleochroism,
				'luster'        => $luster,
				'birefringence' => $birefringence,
				'dispersion'    => $dispersion,
				'transparancy'  => $transparent,
				'crystallography'=> $crystallography,
				'crystal_habit' => $crystal,
				'twinning'      => $twinning,
				'geological_setting'  => $geological,
				'common_asso'   => $common_asso,
				'common_impu'   => $common_impu,
				'year_discovered'=> $year,
				'type_local'    => $type,
				'input_date'    => $tgl,
				'nama_batu'     => $batu,
				'note'          => $note,
				'comment'       => $comment,
				'finish'        => $now,
				'approve'       => "PENDING",
				'uv'            => $this->input->post('uv'),
				'chelsea_filter' => $this->input->post('chelsea_filter'),
				'spectroscope'  => $this->input->post('spectroscope'),
				'microscopic'   => $this->input->post('microscopic'),
				'microscopic_2' => $this->input->post('microscopic_2'),
				'microscopic_3'   => $this->input->post('microscopic_3'),
				'gemolog'       => $this->input->post('gemolog'),
				'update_by' => sessionValue('id'),
				'update_date' => $now
			);

		}else{
			$data = array(
				'obj_weight'    => $weight,
				'obj_length'    => $length,
				'obj_width'     => $width,
				'obj_height'    => $height,
				'obj_image'     => $image,
				'phenomenal'    => $phenomenal,
				'crystal_gems'  => $crystal_gems,
				'obj_cut'       => $cut,
				'obj_cut2'      => $cut2,
				'obj_shape'     => $shape,
				'obj_natural'   => $natural,
				'ri_start'      => $ri_start,
				'cleavage'      => $cleavage,
				'fracture'      => $fracture,
				'tenacity'      => $tenacity,
				'luminescence'  => $luminescence,
				'radioactivity' => $radio,
				'hardness'      => $hardness,
				'sg'            => $gravity,
				'mineral'       => $mineral,
				'strunz'        => $strunz,
				'related'       => $related,
				'member'        => $member,
				'synonyms'      => $synonyms,
				'variety'       => $varieties,
				'pleochroism'   => $pleochroism,
				'luster'        => $luster,
				'birefringence' => $birefringence,
				'dispersion'    => $dispersion,
				'transparancy'  => $transparent,
				'crystallography'=> $crystallography,
				'crystal_habit' => $crystal,
				'twinning'      => $twinning,
				'geological_setting'  => $geological,
				'common_asso'   => $common_asso,
				'common_impu'   => $common_impu,
				'year_discovered'=> $year,
				'type_local'    => $type,
				'input_date'    => $tgl,
				'nama_batu'     => $batu,
				'note'          => $note,
				//'note2'         => $note2,
				//'note3'         => $note3,
				'comment'       => $comment,
				//'comment_2'     => $this->input->post('comment_2'),
				'finish'        => $now,
				'approve'       => "PENDING",
				'uv'            => $this->input->post('uv'),
				'chelsea_filter' => $this->input->post('chelsea_filter'),
				'spectroscope'  => $this->input->post('spectroscope'),
				'microscopic'   => $this->input->post('microscopic'),
				'microscopic_2' => $this->input->post('microscopic_2'),
				'microscopic_3'   => $this->input->post('microscopic_3'),
				'gemolog'       => $this->input->post('gemolog'),
				'update_by' => sessionValue('id'),
				'update_date' => $now
			);
		}
        $this->db->where('id_object', $id);
        $this->db->update('tb_lab_desk', $data);
		if(strtolower($natural)=="natural"){
			$dax = array(
				'step'  => 'LAB_DESK'
			);
		}else{
			$dax = array(
				'gem_card'  => 'BRIEF REPORT',
				'certificate'  => NULL,
				'step'  => 'LAB_DESK'
			);
		}
        
        $this->db->where('id_object', $id);
        if($this->db->update('step', $dax)){
			$success = true;
		}else{
			$msg = "Data Gagal disimpan.";
		}
		
        //redirect(site_url('object/sertifikasi'));
		echo json_encode(array("success"=>$success, "msg"=>$msg));
    }
    function startTaksiran(){
        $id = $_POST['id'];
        $id_order = $_POST['id_order'];
		
        $cekAva = $this->db->query("SELECT id FROM tb_taksiran WHERE id='$id'");
        if($cekAva->num_rows() == 0){
            $sql = $this->db->query("SELECT * FROM tb_fpjt WHERE id='$id'");
            foreach ($sql->result() as $get) {
                $data = array(
                    "id" => $get->id, 
                    "id_member" => $get->id_member,
                    'id_order'  => $get->id_order,
                    'foto'      => $get->foto,
                    'jumlah_perhiasan' => $get->jumlah,
                    'jenis_perhiasan' => $get->jenis_perhiasan,
                    "berat_perhiasan" => $get->berat_perhiasan,
                    "jenis_permata"   => $get->jenis_permata,
                    "warna_permata"   => $get->warna_permata,
                    "jumlah_permata"  => $get->jumlah_permata,
                    "keterangan"      => $get->keterangan,					
					"create_by" => sessionValue('id'),
					"create_date" => date("Y-m-d H:i:s")
                    );
            }
            $this->db->insert("tb_taksiran", $data);
        }else{

        }
        $this->load->view('edit3', array('id'=>$id, 'id_order'=>$id_order));
    }
    function update_taksiran(){
        $id = $this->uri->segment(3);
        $id_order = $this->uri->segment(4);
		
        $date = date("Y-m-d H:i:s");
        $data = array(
            "gemolog" => $this->input->post('gemolog'),
            "jenis_perhiasan" => $this->input->post("jenis_perhiasan"),
            "jenis_logam" => $this->input->post("jenis_logam"),
            "berat_kotor" => $this->input->post("berat_kotor"),
            "berat_bersih" => $this->input->post("berat_bersih"),
            "jenis_permata" => $this->input->post("jenis_permata"),
            "jumlah_permata" => $this->input->post("jumlah_permata"),
            "karatase_emas" => $this->input->post("karatase"),
            "berat_total_permata" => $this->input->post("berat_total"),
            "jumlah_perhiasan" => $this->input->post('jumlah_perhiasan'),
            "keterangan" => $this->input->post('keterangan_tambahan'),
            "keterangan_berlian" => $this->input->post('keterangan_berlian'),
            "update_by" => sessionValue('id'),
            "update_date" => $date
            );

		$success = false;
		$msg = "";
		
        $this->db->where("id", $id);
        if($this->db->update("tb_taksiran", $data)){
            $this->db->where("id", $id);
            $this->db->update("tb_fpjt", array("status" => "SUDAH"));
			
			$success = true;
        }else{
			$msg = "Data gagal disimpan.";
		}
        //redirect(site_url("object/taksiran"));
		//redirect(site_url($this->uri->segment(1)."/taksiran_detail/".$id_order));
		echo json_encode(array("success"=>$success, "msg"=>$msg));
    }
    function color_sertifikasi(){
        $warna = $this->input->post('warna');
        $id_obj = $this->input->post('id_obj');

        $this->db->where('id_object', $id_obj);
        $this->db->update('tb_lab_desk', array("obj_color" => $warna));
		
		$sql = $this->db->query("SELECT code, color, rgb_code FROM color_stone b where code='".$warna."'");
		
		$rgb_color = "";
		$color_spek = "";
		foreach($sql->result_array() as $tmp){
			$exp = explode(';', $tmp['color']);
			if(count($exp)>0){
				$color_spek = $exp[0];
			}else{
				$color_spek = $tmp['color'];
			}
			
			$rgb_color =  $tmp['rgb_code'];
		}
		
		echo json_encode(array("success"=>true, "rgb_color"=>$rgb_color, "color_spek"=>$color_spek));
    }
    function color_grading(){
        $warna = $this->input->post('warna');
        $id_obj = $this->input->post('id_obj');

        $this->db->where('id_object', $id_obj);
        $this->db->update('tb_lab_grading', array("obj_color" => $warna));
		
		$sql = $this->db->query("SELECT code, color, rgb_code FROM color_stone b where code='".$warna."'");
		
		$rgb_color = "";
		$color_spek = "";
		foreach($sql->result_array() as $tmp){
			$exp = explode(';', $tmp['color']);
			if(count($exp)>0){
				$color_spek = $exp[0];
			}else{
				$color_spek = $tmp['color'];
			}
			
			$rgb_color =  $tmp['rgb_code'];
		}		
		
		echo json_encode(array("success"=>true, "rgb_color"=>$rgb_color, "color_spek"=>$color_spek));
    }
}
?>