	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>&nbsp;</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
            <li>Report Identification</li>
            <li class="active">Report Identification Today</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="box box-default">
            <div class="box-body">
              <b>DATA IDENTIFICATION TODAY - NON DIAMOND GRADING</b>
              <div style="border: 1px solid black; margin-bottom: 10px;"></div>
              <table class="table table-bordered table-striped table-hover" id="example1">
                <thead>
                  <tr>
                    <th class="text-center">No</th>
                    <th class="text-center">ID Object</th>
                    <th class="text-center">Final Advice</th>
                    <th class="text-center">Weight</th>
                    <th class="text-center">Length</th>
                    <th class="text-center">Width</th>
                    <th class="text-center">Height</th>
                    <th class="text-center">Requested Report</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $no = 1;
                  date_default_timezone_set("Asia/Jakarta");
                  $date = date('Y-m-d');
                  $get_data = $this->db->query("SELECT * FROM tb_lab_desk, step
                    WHERE 
                    MONTH(step.create_date) = MONTH(now()) AND
                    tb_lab_desk.finish !='' AND
                    step.delete_by = '' AND
                    step.id_object = tb_lab_desk.id_object
                    ");
                  foreach ($get_data->result() as $get) {
                    ?>
                  <tr>
                    <td class="text-center"><?php echo $no++; ?></td>
                    <td><?php echo $get->id_object ?></td>
                    <td><?php echo $get->variety." ".$get->nama_batu ?></td>
                    <td class="text-center"><?php echo $get->obj_weight ?></td>
                    <td class="text-center"><?php echo $get->obj_length ?></td>
                    <td class="text-center"><?php echo $get->obj_width ?></td>
                    <td class="text-center"><?php echo $get->obj_height ?></td>
                    <td><?php echo $get->gem_card.", ".$get->certificate ?></td>
                  </tr>
                    <?php
                  }
                  ?>
                </tbody>
                <tfoot>
                  <tr>
                    <td colspan="8">
                      <div class="pull-right">
                        <a href="<?php echo site_url('report_identify_month/print_identify_month_non').'/'.$date ?>" target="_blank" class="btn btn-primary">PRINT REPORT</a>
                      </div>
                    </td>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
          <div class="box box-default">
            <div class="box-body">
              <b>DATA IDENTIFICATION TODAY - DIAMOND GRADING</b>
              <div style="border: 1px solid black; margin-bottom: 10px;"></div>
              <table class="table table-bordered table-striped table-hover" id="example2">
                <thead>
                  <tr>
                    <th class="text-center">No</th>
                    <th class="text-center">ID Object</th>
                    <th class="text-center">Weight</th>
                    <th class="text-center">Length</th>
                    <th class="text-center">Width</th>
                    <th class="text-center">Height</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $no = 1;
                  date_default_timezone_set("Asia/Jakarta");
                  $date = date('Y-m-d');
                  $get_data = $this->db->query("SELECT * FROM tb_lab_grading, step
                    WHERE 
                    MONTH(step.create_date) = MONTH(now()) AND
                    tb_lab_grading.finish !='' AND
                    step.delete_by = '' AND
                    step.dia_grading != '' AND
                    step.id_object = tb_lab_grading.id_object
                    ");
                  foreach ($get_data->result() as $get) {
                    ?>
                  <tr>
                    <td class="text-center"><?php echo $no++; ?></td>
                    <td><?php echo $get->id_object ?></td>
                    <td class="text-center"><?php echo $get->obj_weight ?></td>
                    <td class="text-center"><?php echo $get->obj_length ?></td>
                    <td class="text-center"><?php echo $get->obj_width ?></td>
                    <td class="text-center"><?php echo $get->obj_height ?></td>
                  </tr>
                    <?php
                  }
                  ?>
                </tbody>
                <tfoot>
                  <tr>
                    <td colspan="6">
                      <div class="pull-right">
                        <a href="<?php echo site_url('report_identify_month/print_identify_month_grading').'/'.$date ?>" target="_blank" class="btn btn-primary">PRINT REPORT</a>
                      </div>
                    </td>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->