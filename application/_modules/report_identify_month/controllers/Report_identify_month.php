<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
* Controller Report Identification Day
* Create By : Erwin Idamsyach Putra
* 23 April 2016
*/
class Report_identify_month extends CI_Controller{
	public function index(){
		$data['filelist'] = 'report_identify_month/front';
		$data['title'] = 'Report Identification';
		$data['title_menu'] = 'Admin';
		$data['menu'] = 'report_identify_month';

		getHTMLWeb($data);
	}
	public function print_identify_month_non(){
		$this->load->library('fpdf');
		$sub  = 0;

		$date = $this->uri->segment(3);
		$date = date_create($date);
		$para = date_format($date, 'Y-m-d');
		$num  = date_format($date, 'ym');
		$date = date_format($date, 'D, d-m-Y');

		$pdf = new FPDF('P','mm','A4');
		$pdf->SetFont('helvetica','b', 10);
		$pdf->AddPage();
		$pdf->Image('asset/logo-pegadaian/Logo.jpg', 138, 5, 60, 25);
		$pdf->SetXY(138, 32);
		$pdf->Cell(0, 0, 'Pegadaian Gemological Laboratory');
		$pdf->SetFont('helvetica','', 10);
		$pdf->SetXY(138, 36);
		$pdf->Cell(0, 0, 'Jalan Kramat Raya No. 162');
		$pdf->SetXY(138, 40);
		$pdf->Cell(0, 0, 'Jakarta Pusat, 10430');
		$pdf->Ln();

		$pdf->SetFont('helvetica','b', 14);
		$pdf->SetXY(0, 25);
		$pdf->Cell(0, 0, 'Laporan Identifikasi', 0, 0, 'C');
		$pdf->Ln();

		$pdf->SetFont('helvetica','', 12);
		$pdf->SetXY(5, 60);
		$pdf->Cell(0, 0, 'No. Laporan');
		$pdf->SetXY(35, 60);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(40, 60);
		$pdf->Cell(0, 0, 'LH-'.$num.'-02');

		$pdf->SetXY(5, 65);
		$pdf->Cell(0, 0, 'Tanggal');
		$pdf->SetXY(35, 65);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(40, 65);
		$pdf->Cell(0, 0, $date);

		$pdf->SetXY(5, 70);
		$pdf->Cell(0, 0, 'Tipe');
		$pdf->SetXY(35, 70);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(40, 70);
		$pdf->Cell(0, 0, "Non-Diamond Grading");
		$pdf->Ln();

		/*$pdf->Rect(10, 80, 15, 120);
		$pdf->Rect(25, 80, 35, 120);
		$pdf->Rect(60, 80, 50, 120);
		$pdf->Rect(110, 80, 30, 120);
		$pdf->Rect(140, 80, 60, 120);*/

		$pdf->SetFont('helvetica','', 10);
		$pdf->SetXY(5, 80);
		$pdf->Cell(20,8,'No',1,0,'C');
		$pdf->SetXY(25, 80);
		$pdf->Cell(25,8,'ID Object',1,0,'C');
		$pdf->SetXY(50, 80);
		$pdf->Cell(55,8,'Final Advice',1,0,'C');
		$pdf->SetXY(105, 80);
		$pdf->Cell(25,8,'Weight',1,0,'C');
		$pdf->SetXY(130, 80);
		$pdf->Cell(25,8,'Length',1,0,'C');
		$pdf->SetXY(155, 80);
		$pdf->Cell(25,8,'Width',1,0,'C');
		$pdf->SetXY(180, 80);
		$pdf->Cell(25,8,'Height',1,0,'C');
		$pdf->Ln();
		$no = 1;

		$get_data = $this->db->query("SELECT * FROM tb_lab_desk, step
                    WHERE 
                    MONTH(step.create_date) = MONTH(now()) AND
                    tb_lab_desk.finish !='' AND
                    step.delete_by = '' AND
                    step.id_object = tb_lab_desk.id_object
                    ");

        foreach($get_data->result() as $get) {
	        $get_sum = $this->db->query("SELECT COUNT(*) as total_speciment FROM tb_front_desk WHERE id_order='".$get->id_order."' AND delete_by=''");
	        foreach ($get_sum->result() as $sum) {
	          $spec = $sum->total_speciment;
	        }
        	$pdf->SetX(5);
			$pdf->Cell(20,8,$no++,1,0,'C');
			$pdf->SetX(25);
			$pdf->Cell(25,8,$get->id_object,1,0,'C');
			$pdf->SetX(50);
			$pdf->Cell(55,8,$get->variety." ".$get->nama_batu,1,0,'C');
			$pdf->SetX(105);
			$pdf->Cell(25,8,$get->obj_weight,1,0,'C');
			$pdf->SetX(130);
			$pdf->Cell(25,8,$get->obj_length,1,0,'C');
			$pdf->SetX(155);
			$pdf->Cell(25,8,$get->obj_width,1,0,'C');
			$pdf->SetX(180);
			$pdf->Cell(25,8,$get->obj_height,1,0,'C');
			$pdf->Ln();
        }
        $pdf->SetXY(140, 200);
        $pdf->Cell(60,8,'YEA',1,0,'C');
		$pdf->Output();
	}
}
?>