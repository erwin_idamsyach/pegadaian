<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class reg_customer extends CI_Controller{
    function reg_customer(){
        parent::__construct();
		
		if(!isLogin()){
			goLogin();
		}
        $this->load->library('fpdf');
        $this->load->library('Barcode39');
    }
    
    function index($text=NULL){
        $data["filelist"] = "reg_customer/reg_customer";
        $data["title"] = "Front";
        $data["title_menu"] = "Front";
        $data["menu"] = "reg_customer";
		
        getHTMLWeb($data);
    }

    public function kota($id)
    {
        $this->load->view('get_kota', array('id'=>$id));
    }

    public function kecam($id)
    {
        $this->load->view('get_keca', array('id'=>$id));
    }

    public function edit_indiv($id)
    {
        $this->load->view('edit', array('id'=>$id));
    }

    public function delete($id)
    {
        $this->db->delete('tb_member_individu', array('id_member'=>$id));
    }

    public function mem_indi()
    {
        $fir_name   = $this->input->post('fir_name');
        $mid_name   = $this->input->post('mid_name');
        $las_name   = $this->input->post('las_name');
        $phone      = $this->input->post('phone');
        $prov       = $this->input->post('prov');
        $kota       = $this->input->post('kota');
        $keca       = $this->input->post('keca');
        $postal     = $this->input->post('postal');
        $address    = $this->input->post('address');
        $email      = $this->input->post('email');
        date_default_timezone_set('Asia/Jakarta');
        $tgl = date('Y-m-d');

        $gid = $this->db->query("SELECT id_member FROM tb_member_individu where kode='A' ORDER BY id_member desc LIMIT 0,1");
        foreach ($gid->result_array() as $get) {
        }

        $id = $get['id_member'];
        $idd = substr($id, 1);
        $idd = $idd+1;
        $cek = strlen($idd);
        if ($cek < 2) {
            $idd = "A0000".$idd;
        }else{
            $idd = "A000".$idd;
        }

        $data = array(
            'id_member'     => $idd,
            'kode'          => 'A',
            'first_name'    => $fir_name,
            'middle_name'   => $mid_name,
            'last_name'     => $las_name,
            'phone'         => $phone,
            'province'      => $prov,
            'city'          => $kota,
            'district'      => $keca,
            'postal_code'   => $postal,
            'address'       => $address,
            'email'         => $email,
            'member_from'   => $tgl
        );
        $this->db->insert('tb_member_individu', $data);

    }

    public function edit_mem_indi($id)
    {
        $fir_name   = $this->input->post('fir_name');
        $mid_name   = $this->input->post('mid_name');
        $las_name   = $this->input->post('las_name');
        $phone      = $this->input->post('phone');
        $prov       = $this->input->post('prov');
        $kota       = $this->input->post('kota');
        $keca       = $this->input->post('keca');
        $postal     = $this->input->post('postal');
        $address    = $this->input->post('address');
        $email      = $this->input->post('email');
        date_default_timezone_set('Asia/Jakarta');
        $tgl = date('Y-m-d');

        $data = array(
            'first_name'    => $fir_name,
            'middle_name'   => $mid_name,
            'last_name'     => $las_name,
            'phone'         => $phone,
            'province'      => $prov,
            'city'          => $kota,
            'district'      => $keca,
            'postal_code'   => $postal,
            'address'       => $address,
            'email'         => $email,
        );
        $this->db->where('id_member', $id);
        $this->db->update('tb_member_individu', $data);
    }

    public function print_member(){
        $id = $this->uri->segment(3);
        $bc = new Barcode39($id);
        // set barcode bar thickness (thick bars) 
        $bc->barcode_bar_thick = 5; 
        // set barcode bar thickness (thin bars) 
        $bc->barcode_bar_thin = 2; 
        $bc->draw("asset/barcode-member/".$id.".gif");
        $que = $this->db->query("SELECT * FROM tb_member_individu WHERE id_member='$id'");
        foreach ($que->result() as $get) {
        
        $pdf = new FPDF('L','cm',array(9, 5.5));
        $pdf->SetAutoPageBreak(true, 0);
        $pdf->AddPage();
        $pdf->SetFont('Helvetica','B',10);
        $pdf->SetXY(0.3, 2.5);
        $pdf->Cell(0, 0, strtoupper($get->first_name)." ".strtoupper($get->middle_name)." ".strtoupper($get->last_name));
        $pdf->Ln();
        $pdf->SetXY(0.3, 3);
        $pdf->Cell(0, 0, $get->id_member);
        $pdf->Image("asset/barcode-member/".$id.".gif", 0.3, 3.45);
        }
        $pdf->Output();
    }
}
