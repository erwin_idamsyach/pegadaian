<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
* Controller Print Certificate
* Create By : Erwin Idamsyach Putra
* 23 April 2016
*/
class print_cert_done extends CI_Controller{
	public function index(){
		$data['filelist'] = 'print_cert_done/front';
		$data['title'] = 'Finished Print Certificate';
		$data['title_menu'] = 'Admin';
		$data['menu'] = 'print-cert-done';

		getHTMLWeb($data);
	}

	public function print_certificate(){
		$this->load->library('fpdf');
		$id_obj = $this->uri->segment(3);

		$get_data = $this->db->query("SELECT * FROM tb_lab_desk, step, color_stone WHERE
			tb_lab_desk.obj_color = color_stone.code AND
			step.id_object = tb_lab_desk.id_object AND
			step.step='LAB_DESK' AND step.print='Belum' AND
			tb_lab_desk.id_object='$id_obj'
			");
		$this->db->where('id_object', $id_obj);
		$this->db->update('step', array('print_cert'=>'Sudah'));
		$pdf = new FPDF('L','mm','A4');
		$pdf->AddPage();

		foreach ($get_data->result() as $get) {
		$col = explode(";", $get->color);
		$color   = $col[0];

		$date = date_create($get->create_date);
		$date = date_format($date, 'D, d-m-Y');
		if($get->comment == ''){
			$comment = '-';
		}else{
			$comment =  $get->comment;
		}
		$pdf->Image('asset/logo-pegadaian/logo-png-1.png', 10, 13, 50, 10);
		$pdf->Image('asset/logo-pegadaian/Logo.png', 220, 5, 60, 20);
		$pdf->SetLineWidth(1);
		$pdf->Line(5, 32, 150, 32);
		$pdf->SetDrawColor(130, 186, 83);
		$pdf->Line(150, 32, 200, 32);

		$pdf->SetFont('Helvetica','B',16);
		$pdf->SetTextColor(43, 75, 14);

		$pdf->SetXY(200, 31);
		$pdf->Cell(0, 0, 'Gemstone Identification Report');

		$pdf->Image('asset/images/'.$get->obj_image, 40, 48, 35, 35);
		$pdf->SetFont('Helvetica','',9);
		$pdf->SetTextColor(0,0,0);
		$pdf->SetXY(40, 90);
		$pdf->Cell(0,0,'Photo not the actual size');

		$pdf->SetXY(10, 115);
		$pdf->SetFont('Helvetica','B',13);
		$pdf->Cell(0, 0, 'Identification Result');

		$pdf->SetFont('Helvetica','',12);
		$pdf->SetXY(10, 125);
		$pdf->Cell(0, 0, 'Species');
		$pdf->SetXY(53, 125);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(56, 125);
		$pdf->Cell(0, 0, $get->nama_batu);

		$pdf->SetXY(10, 135);
		$pdf->Cell(0, 0, 'Variety');
		$pdf->SetXY(53, 135);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(56, 135);
		$pdf->Cell(0, 0, $get->variety);

		$pdf->SetXY(10, 145);
		$pdf->Cell(0, 0, 'Comment (s)');
		$pdf->SetXY(53, 145);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(56, 142);
		$pdf->MultiCell(70, 5, $comment);

		$pdf->SetFont('Helvetica','B',13);
		$pdf->SetXY(175, 50);
		$pdf->Cell(0, 0, 'Report No');
		$pdf->SetXY(215, 50);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(222, 50);
		$pdf->Cell(0, 0, $get->id_object);

		$pdf->SetXY(175, 62);
		$pdf->Cell(0, 0, 'Date');
		$pdf->SetXY(215, 62);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(222, 62);
		$pdf->Cell(0, 0, $date);

		$pdf->SetXY(175, 74);
		$pdf->Cell(0, 0, 'Weight');
		$pdf->SetXY(215, 74);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(222, 74);
		$pdf->Cell(0, 0, $get->obj_weight." cts");

		$pdf->SetXY(175, 86);
		$pdf->Cell(0, 0, 'Shape');
		$pdf->SetXY(215, 86);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(222, 86);
		$pdf->Cell(0, 0, $get->obj_shape);

		$pdf->SetXY(175, 98);
		$pdf->Cell(0, 0, 'Cut');
		$pdf->SetXY(215, 98);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(222, 98);
		$pdf->Cell(0, 0, $get->obj_cut);

		$pdf->SetXY(175, 110);
		$pdf->Cell(0, 0, 'Measurements');
		$pdf->SetXY(215, 110);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(222, 110);
		$pdf->Cell(0, 0, $get->obj_length." x ".$get->obj_width." x ".$get->obj_height." mm");

		$pdf->SetXY(175, 122);
		$pdf->Cell(0, 0, 'Color');
		$pdf->SetXY(215, 122);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(222, 122);
		$pdf->Cell(0, 0, $color);

		$pdf->SetXY(175, 134);
		$pdf->Cell(0, 0, 'Phenomena');
		$pdf->SetXY(215, 134);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(222, 134);
		$pdf->Cell(0, 0, $get->phenomenal);

		$pdf->SetXY(175, 146);
		$pdf->Cell(0, 0, 'Microscopic');
		$pdf->SetXY(215, 146);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(222, 146);
		$pdf->Cell(0, 0, '-');

		$pdf->SetDrawColor(190, 190, 190);
		$pdf->Line(80, 175, 120, 175);

		$pdf->Image('asset/logo-pegadaian/Logo.png', 10, 185, 40, 15);
		$pdf->SetXY(85, 188);
		$pdf->SetFont('Helvetica','',8);
		$pdf->Cell(0, 0, 'Jl. Kramat Raya 162',0,0,'L');
		}

		$pdf->Output();
	}

	public function print_multiple_cert(){
		$this->load->library('fpdf');
		$id_order = $this->uri->segment(3);

		$get_data = $this->db->query("SELECT * FROM tb_lab_desk, step, color_stone WHERE
			tb_lab_desk.obj_color = color_stone.code AND
			step.id_object = tb_lab_desk.id_object AND
			step.step='LAB_DESK' AND step.print='Belum' AND
			step.id_order='$id_order'
			");

		$this->db->where('id_order', $id_order);
		$this->db->update('step', array('print_cert'=>'Sudah'));

		$pdf = new FPDF('L','mm','A4');

		foreach ($get_data->result() as $get) {
		$col = explode(";", $get->color);
		$color   = $col[0];

		$date = date_create($get->create_date);
		$date = date_format($date, 'D, d-m-Y');
		if($get->comment == ''){
			$comment = '-';
		}else{
			$comment =  $get->comment;
		}
		$pdf->AddPage();
		$pdf->Image('asset/logo-pegadaian/logo-png-1.png', 10, 13, 50, 10);
		$pdf->Image('asset/logo-pegadaian/Logo.png', 220, 5, 60, 20);
		$pdf->SetLineWidth(1);
		$pdf->Line(5, 32, 150, 32);
		$pdf->SetDrawColor(130, 186, 83);
		$pdf->Line(150, 32, 200, 32);

		$pdf->SetFont('Helvetica','B',16);
		$pdf->SetTextColor(43, 75, 14);

		$pdf->SetXY(200, 31);
		$pdf->Cell(0, 0, 'Gemstone Identification Report');

		$pdf->Image('asset/images/'.$get->obj_image, 40, 48, 35, 35);
		$pdf->SetFont('Helvetica','',9);
		$pdf->SetTextColor(0,0,0);
		$pdf->SetXY(40, 90);
		$pdf->Cell(0,0,'Photo not the actual size');

		$pdf->SetXY(10, 115);
		$pdf->SetFont('Helvetica','B',13);
		$pdf->Cell(0, 0, 'Identification Result');

		$pdf->SetFont('Helvetica','',12);
		$pdf->SetXY(10, 125);
		$pdf->Cell(0, 0, 'Species');
		$pdf->SetXY(53, 125);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(56, 125);
		$pdf->Cell(0, 0, $get->nama_batu);

		$pdf->SetXY(10, 135);
		$pdf->Cell(0, 0, 'Variety');
		$pdf->SetXY(53, 135);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(56, 135);
		$pdf->Cell(0, 0, $get->variety);

		$pdf->SetXY(10, 145);
		$pdf->Cell(0, 0, 'Comment (s)');
		$pdf->SetXY(53, 145);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(56, 142);
		$pdf->MultiCell(70, 5, $comment);

		$pdf->SetFont('Helvetica','B',13);
		$pdf->SetXY(175, 50);
		$pdf->Cell(0, 0, 'Report No');
		$pdf->SetXY(215, 50);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(222, 50);
		$pdf->Cell(0, 0, $get->id_object);

		$pdf->SetXY(175, 62);
		$pdf->Cell(0, 0, 'Date');
		$pdf->SetXY(215, 62);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(222, 62);
		$pdf->Cell(0, 0, $date);

		$pdf->SetXY(175, 74);
		$pdf->Cell(0, 0, 'Weight');
		$pdf->SetXY(215, 74);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(222, 74);
		$pdf->Cell(0, 0, $get->obj_weight." cts");

		$pdf->SetXY(175, 86);
		$pdf->Cell(0, 0, 'Shape');
		$pdf->SetXY(215, 86);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(222, 86);
		$pdf->Cell(0, 0, $get->obj_shape);

		$pdf->SetXY(175, 98);
		$pdf->Cell(0, 0, 'Cut');
		$pdf->SetXY(215, 98);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(222, 98);
		$pdf->Cell(0, 0, $get->obj_cut);

		$pdf->SetXY(175, 110);
		$pdf->Cell(0, 0, 'Measurements');
		$pdf->SetXY(215, 110);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(222, 110);
		$pdf->Cell(0, 0, $get->obj_length." x ".$get->obj_width." x ".$get->obj_height." mm");

		$pdf->SetXY(175, 122);
		$pdf->Cell(0, 0, 'Color');
		$pdf->SetXY(215, 122);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(222, 122);
		$pdf->Cell(0, 0, $color);

		$pdf->SetXY(175, 134);
		$pdf->Cell(0, 0, 'Phenomena');
		$pdf->SetXY(215, 134);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(222, 134);
		$pdf->Cell(0, 0, $get->phenomenal);

		$pdf->SetXY(175, 146);
		$pdf->Cell(0, 0, 'Microscopic');
		$pdf->SetXY(215, 146);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(222, 146);
		$pdf->Cell(0, 0, '-');

		$pdf->SetDrawColor(190, 190, 190);
		$pdf->Line(80, 175, 120, 175);

		$pdf->Image('asset/logo-pegadaian/Logo.png', 10, 185, 40, 15);
		$pdf->SetXY(85, 188);
		$pdf->SetFont('Helvetica','',8);
		$pdf->Cell(0, 0, 'Jl. Kramat Raya 162',0,0,'L');
		}

		$pdf->Output();
	}
}
?>