<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
* Controller Print Certificate
* Create By : Erwin Idamsyach Putra
* 23 April 2016
*/
class certificate extends CI_Controller{
	function __construct(){
		parent::__construct();

		if(!isLogin()){
			goLogin();
		}
	}
	
	public function index(){
		$data['filelist'] = 'certificate/front';
		$data['title'] = 'Print Certificate';
		$data['title_menu'] = 'Admin';
		$data['menu'] = 'print-cert';

		getHTMLWeb($data);
	}

	public function print_certificate(){
		$this->load->library('fpdf');
		
		//require_once APPPATH.'libraries/fpdf-master/makefont/makefont.php';
		
		//MakeFont('D:\PROJECT\COG_LAB_PEGADAIAN\FONT\ronnia\Ronnia-Regular.ttf','cp1252');
		//MakeFont('C:\\Windows\\Fonts\\comic.ttf','cp1252');
		
		$id_obj = $this->uri->segment(3);
		
		$getUrl = $this->db->query("SELECT url FROM tb_url WHERE ISNULL(delete_by)");
		$tmpUrl = $getUrl->result();
		
		$getNumNoCert = $this->db->query("SELECT COUNT(no_sertifikat) no_sertifikat, no_sertifikat noCert FROM step WHERE id_object='".$id_obj."'");
		$tmpNumNoCert = $getNumNoCert->result_array();
		
		if($tmpNumNoCert[0]['no_sertifikat']==0){
			$getNoCert = $this->db->query("SELECT no_sertifikat FROM step ORDER BY no_sertifikat DESC LIMIT 0, 1");
			foreach ($getNoCert->result() as $no_cert) {
				$noCert = $no_cert->no_sertifikat;
			}
			if(!isset($noCert)){
				$noCert = 0;
			}else{}
			$noCert = substr($noCert, 3);
			$noCert = $noCert+1;
			$getServices = $this->db->query("SELECT kode_produk FROM tb_service WHERE kode='FR' LIMIT 0, 1");
			$kode_produk = "";
			foreach ($getServices->result() as $tmpService) {
				$kode_produk = $tmpService->kode_produk;
			}
			$kode_produk = sessionValue('kode_unit')."-".date('y')."-".$kode_produk."-";
			$noCert = generateNoReport($kode_produk, $noCert);
			$this->db->where("id_object", $id_obj);
			$this->db->update("step", array("no_sertifikat" => $noCert, 'update_by'  => sessionValue('id'), 'update_date' => date('Y-m-d H:i:s')));
			$noCert2 = $noCert;
			$noCert = $tmpUrl[0]->url."&no_sertifikat=".$noCert;
			setQarcode("barcode-fpjs", $noCert);
		}else{
			$noCert2 = $tmpNumNoCert[0]['noCert'];
			$noCert = $tmpUrl[0]->url."&no_sertifikat=".$tmpNumNoCert[0]['noCert'];
		}
		
		$get_data = $this->db->query("SELECT tb_lab_desk.*, tb_login.nama approve_name,
			color_stone.jenis_warna, color_stone.color, master_microscopic.microscopic, mic2.microscopic microscopic_2, mic3.microscopic microscopic_3
			FROM 
			tb_lab_desk
			LEFT JOIN step ON step.id_object = tb_lab_desk.id_object
			LEFT JOIN color_stone ON color_stone.code = tb_lab_desk.obj_color
			LEFT JOIN master_microscopic ON master_microscopic.id = tb_lab_desk.microscopic
			LEFT JOIN master_microscopic mic2 ON mic2.id = tb_lab_desk.microscopic_2
			LEFT JOIN master_microscopic mic3 ON mic3.id = tb_lab_desk.microscopic_3
			LEFT JOIN tb_login ON tb_login.id = step.approve_by
			WHERE step.step='LAB_DESK' AND step.print='Belum' AND
			tb_lab_desk.id_object='$id_obj'
			");
		$this->db->where('id_object', $id_obj);
		$this->db->update('step', array('print_cert'=>'Sudah', 'status'=>'NOT APPROVED', 'update_by'  => sessionValue('id'), 'update_date' => date('Y-m-d H:i:s')));
		$pdf = new FPDF('L','mm','A4');
		$pdf->SetAutoPageBreak(false, 0);
		$pdf->AddPage();
		
		$pdf->AddFont('ronnia','','ronnia.php');
		$pdf->AddFont('ronnia','B','ronniab.php');
		$pdf->AddFont('ronnia','BI','ronniabi.php');
		$pdf->AddFont('ronnia','I','ronniai.php');
		
		// Hadi		
		//$pdf->Rect(49, 47, 37, 37, 'D');
		$pdf->Image('asset/barcode-fpjs/'.$noCert.'.png', 17, 160, 25, 25);
		// End Hadi

		foreach ($get_data->result() as $get) {
		if(strpos($get->color, ";") == true){
			$col = explode(";", $get->color);
			$color   = $col[0];
		}else{
			$color 	 = $get->color;
		}

		$date = date_create($get->input_date);
		$day = date_format($date, "d");
		$month = date_format($date, "m");
		$year = date_format($date, "Y");
		
		if($get->comment == ''){
			$comment = '';
		}else{
			$comment =  $get->comment;
		}
		/*$pdf->Image('asset/logo-pegadaian/logo-png-1.png', 10, 13, 50, 10);*/

		$pdf->SetFont('ronnia','',12);
		//$pdf->SetFont('ronnia','',8);
		//$pdf->SetXY((290/2)+2, 1);
		//$pdf->Cell(0, 0, '|');
		//$pdf->SetXY((290/2)+2, 207);
		//$pdf->Cell(0, 0, '|');
		//$pdf->SetFont('ronnia','',16);
		//$pdf->SetXY(1, 207);
		//$pdf->Cell(0, 0, '+');
		//$pdf->SetXY(292, 207);
		//$pdf->Cell(0, 0, '+');
		
		$pdf->SetFont('ronnia','',24);
		$pdf->Cell(270, 0, "I D E N T I F I C A T I O N    M E M O", 0, 0, "C");

		$pdf->SetFont('ronnia','',16);
		$pdf->SetTextColor(255, 255, 255);

		$pos_y = array(45, 57, 69, 81, 93, 105, 117, 129, 141, 153, 165, 177);
		
		if($get->obj_image!=""){
			$pdf->Image('asset/images/'.$get->obj_image, 58, 30, 43, 43);
		}
		$pdf->SetFont('ronnia','',16);

		$pdf->SetTextColor(0,0,0);

		$pdf->SetXY(45, 80);
		$pdf->SetFont('ronnia','',20);
		$pdf->Cell(0, 0, ucwords(strtolower($get->obj_natural))." ".ucwords(strtolower($get->variety)));

		$pdf->SetFont('ronnia','',13);
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetXY(17, 115);
		$pdf->Cell(0, 0, 'Spesies');
		$pdf->SetXY(57, 115);
		$pdf->SetFont('ronnia','',13);
		$pdf->SetTextColor(0, 0, 0);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(59, 115);
		$pdf->Cell(0, 0, ucwords(strtolower($get->obj_natural." ".(strtolower($get->nama_batu)=="none"?"":$get->nama_batu))));		
		
		$expVar = explode('-',$get->variety);
		/*if(count($expVar)>0){
			$variety = ucwords(strtolower($expVar[0])).' '.ucwords(strtolower($expVar[1]));
		}else{
			$variety = ucwords(strtolower($get->variety));
		}*/
		$pdf->SetFont('ronnia','',13);
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetXY(17, 125);
		$pdf->Cell(0, 0, 'Varietas');
		$pdf->SetXY(57, 125);
		$pdf->SetFont('ronnia','',13);
		$pdf->SetTextColor(0, 0, 0);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(59, 125);
		$pdf->Cell(0, 0, ucwords(strtolower($get->variety)));

		$pdf->SetFont('ronnia','',13);
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetXY(17, 135);
		$pdf->Cell(0, 0, 'Komentar');
		$pdf->SetXY(57, 135);
		$pdf->SetFont('ronnia','',13);
		$pdf->SetTextColor(0, 0, 0);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(59, 133);
		$pdf->MultiCell(100, 5, ($get->note)=="none"?"":$get->note,'','L');
		$pdf->Ln();
		$pdf->SetX(56);
		$pdf->Cell(0, 0, $comment);

		$pdf->SetFont('ronnia','',13);
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetXY(175, $pos_y[0]);
		$pdf->Cell(0, 0, 'No. Laporan');		
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetFont("ronnia","",13);
		$pdf->SetXY(218, $pos_y[0]);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(222, $pos_y[0]);
		$pdf->Cell(0, 0, $noCert2);

		$pdf->SetFont('ronnia','',13);
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetXY(175, $pos_y[1]);
		$pdf->Cell(0, 0, 'Tanggal');
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetFont("ronnia","",13);
		$pdf->SetXY(218, $pos_y[1]);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(222, $pos_y[1]);
		$pdf->Cell(0, 0, $day.bulan($month)." ".$year);

		$pdf->SetFont('ronnia','',13);
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetXY(175, $pos_y[2]);
		$pdf->Cell(0, 0, 'Berat');
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetFont("ronnia","",13);
		$pdf->SetXY(218, $pos_y[2]);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(222, $pos_y[2]);
		$pdf->Cell(0, 0, $get->obj_weight." cts");

		$pdf->SetFont('ronnia','',13);
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetXY(175, $pos_y[3]);
		$pdf->Cell(0, 0, 'Bentuk');
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetFont("ronnia","",13);
		$pdf->SetXY(218, $pos_y[3]);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(222, $pos_y[3]);
		$pdf->Cell(0, 0, $get->obj_shape);

		$pdf->SetFont('ronnia','',13);
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetXY(175, $pos_y[4]);
		$pdf->Cell(0, 0, 'Model Gosokan');
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetFont("ronnia","",13);
		$pdf->SetXY(218, $pos_y[4]);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(222, $pos_y[4]);
		$pdf->Cell(0, 0, $get->obj_cut);

		$pdf->SetFont('ronnia','',13);
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetXY(175, $pos_y[5]);
		$pdf->Cell(0, 0, 'Ukuran');
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetFont("ronnia","",13);
		$pdf->SetXY(218, $pos_y[5]);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(222, $pos_y[5]);
		$pdf->Cell(0, 0, $get->obj_length." x ".$get->obj_width." x ".$get->obj_height." mm");

		$pdf->SetFont('ronnia','',13);
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetXY(175, $pos_y[6]);
		$pdf->Cell(0, 0, 'Warna');
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetFont("ronnia","",13);
		$pdf->SetXY(218, $pos_y[6]);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(222, $pos_y[6]);
		$pdf->Cell(0, 0, ucwords($get->jenis_warna));

		if($get->phenomenal == ''){ $phen = ''; }else{ $phen = ucwords(strtolower($get->phenomenal)); }

		$pdf->SetFont('ronnia','',13);
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetXY(175, $pos_y[7]);
		$pdf->Cell(0, 0, 'Fenomena');		
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetFont("ronnia","",13);
		$pdf->SetXY(218, $pos_y[7]);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(222, $pos_y[7]);
		$pdf->Cell(0, 0, $phen);

		if($get->microscopic == ""){ $misc = ''; }else{ $misc = $get->microscopic; }
		if($get->microscopic_2 == ""){ $misc2 = ''; }else{ $misc2 = $get->microscopic_2; }
		if($get->microscopic_3 == ""){ $misc3 = ''; }else{ $misc3 = $get->microscopic_3; }

		$pdf->SetFont('ronnia','',13);
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetXY(175, $pos_y[8]);
		$pdf->Cell(0, 0, 'Mikroskopik');
		$pdf->SetFont("ronnia","",13);
		$pdf->SetXY(218, $pos_y[8]);
		$pdf->Cell(0, 0, ':');
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetFont("ronnia","",13);
		$pdf->SetXY(222, $pos_y[8]);
		$pdf->Cell(0, 0, $misc);
		$pdf->SetXY(222, $pos_y[8]+7);
		$pdf->Cell(0, 0, $misc2);
		$pdf->SetXY(222, $pos_y[8]+14);
		$pdf->Cell(0, 0, $misc3);
		
		$pdf->SetFont("ronnia","",10);
		$pdf->SetXY(75, 179);
		$pdf->Cell(50, 0, "", 0, 0, "C"); // $get->gemolog
		$pdf->SetDrawColor(0, 0, 0);
		$pdf->Line(81, 181, 131, 181);
		$pdf->SetXY(81, 183);
		$pdf->Cell(53, 0, "Gemologist", 0, 0, "C");

		$store = "";
		$lokasi = sessionValue('alamat');
		$hunting = " ";
		//if(sessionValue('flag_store')=="Y"){
		//	$store = "- ".ucfirst(sessionValue('nama_store'));
		//}
		/*if(sessionValue('lokasi')=="PUSAT"){
			$hunting = " (Hunting) ".ucfirst(sessionValue('store'));
			$lokasi = "Kantor ".ucfirst(strtolower(sessionValue('lokasi')))." PT Pegadaian (Persero) ".sessionValue('alamat');
		}else{
			$store = "- ".ucfirst(sessionValue('nama_store'));
		}
		
		//$pdf->Image('asset/logo-pegadaian/Logo.png', 10, 185, 40, 15);
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetFont('ronnia','',8);
		$pdf->SetXY(5, 188);
		$pdf->Cell(0, 0, 'Laboratorium Gemologi Pegadaian '.$store,0,0,'L');
		$pdf->SetXY(5, 191);
		$pdf->Cell(0, 0, $lokasi,0,0,'L');
		$pdf->SetXY(5, 195);
		$pdf->Cell(0, 0, 'T +62 '.sessionValue('telepon').$hunting.' Fax +62 '.sessionValue('fax'),0,0,'L');
		$pdf->SetXY(80, 195);
		
		$pdf->Cell(0, 0, $tmpUrl[0]->url,0,0,'L');
		$pdf->SetXY(110, 198);
		$pdf->Cell(0, 0, '',0,0,'L');*/

		/*$pdf->SetXY(159, 191);
		$pdf->SetTextColor(0, 0, 0);
		$pdf->Cell(130, 0, "Semua informasi yang diberikan merupakan pendapat dari Laboratorium Gemologi Pegadaian pada saat", 0, 0, "C", false);
		$pdf->SetXY(159, 194);
		$pdf->Cell(130, 0, "pemeriksaan. Tidak berlaku apabila selanjutnya terdapat modifikasi terhadap barang yang telah diidentifikasi.", 0, 0, "C", false);
		
		$pdf->SetDrawColor(16,95,62);
		$pdf->Line(156, 196, 290, 196);

		//$pdf->SetTextColor(240, 183, 62);
		$pdf->SetTextColor(83, 83, 83);
		$pdf->SetFillColor(0, 0, 0);

		$pdf->SetXY(159, 197.5);
		$pdf->Cell(130, 0, "All information given represents the opinion of Pegadaian Gemological Laboratory at the time of the testing.", 0, 0, "C", false);
		$pdf->SetXY(159, 200.5);
		$pdf->Cell(130, 0, "It does not take into account of any subsequent modification of the item tested", 0, 0, "C", false);*/
		}
		
		/*$pdf->AddPage();
		
		$width = array(115,5,110,35);
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetFont('ronnia','',10);
		
		$pdf->SetTopMargin(15);
		$pdf->SetLeftMargin(15);
		$pdf->Cell($width[0], 5, 'SYARAT DAN KETENTUAN',0,0,'C');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[0], 5, 'TERMS AND CONDITION',0,0,'C');
		$pdf->Ln(7);
		
		$pdf->Cell($width[1], 5, '1.',0,0,'L');
		$pdf->Cell($width[2], 5, 'PT PEGADAIAN (Persero) selanjutnya disebut Laboratorium',0,0,'FJ');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '1.',0,0,'L');
		$pdf->Cell($width[2], 5, 'PT PEGADAIAN (Persero) hereinafter reffered to as Pegadaian',0,0,'FJ');
		$pdf->Ln();		
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'Gemologi Pegadaian di dalam Laporan Identifikasi ini.',0,0,'L');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'Gemological Laboratory in this Gemstone Identification',0,0,'FJ');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '2.',0,0,'L');
		$pdf->Cell($width[2], 5, 'Laporan Identifikasi Batu Adi ini bukan merupakan penilaian',0,0,'FJ');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'Report.',0,0,'L');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'kualitas, taksiran harga ataupun rekomendasi traksaksi.',0,0,'L');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '2.',0,0,'L');
		$pdf->Cell($width[2], 5, 'This Gemstone Identification Report is not a quality',0,0,'FJ');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '3.',0,0,'L');
		$pdf->Cell($width[2], 5, 'Laporan Identifikasi Batu Adi ini dibuat atas permintaan nasabah, berisi',0,0,'FJ');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'evaluation, an appraising or a trasaction recommendation.',0,0,'L');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'spesifikasi sebagaimana terinci di dalam laporan ini setelah melalui',0,0,'FJ');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '3.',0,0,'L');
		$pdf->Cell($width[2], 5, 'This Gemstone Identification Report is made by customer request at',0,0,'FJ');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'pemeriksaan dan pengujian standar ilmu gemologi dengan',0,0,'FJ');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'the time of the testing, represents specification as detailed in this',0,0,'FJ');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'menggunakan peralatan standar gemologi dan peralatan lain yang',0,0,'FJ');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'Report based upon the application and identification techniques',0,0,'FJ');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'diperlukan.',0,0,'L');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'using standard gemological equipment and other necessary',0,0,'FJ');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '4.',0,0,'L');
		$pdf->Cell($width[2], 5, 'Nasabah setuju dan menerima hasil pemeriksaan dan pengujian',0,0,'FJ');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'equipment.',0,0,'L');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'yang dilakukan oleh Laboratorium Gemologi Pegadaian, baik',0,0,'FJ');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '4.',0,0,'L');
		$pdf->Cell($width[2], 5, 'Customer agrees and accepts the result of inspections and tests',0,0,'FJ');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'terhadap metode, standar pengujian, istilah dan kriteria',0,0,'FJ');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'performed by Pegadaian Gemological Laboratory, either of the',0,0,'FJ');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'yang digunakan dalam pembuatan Laporan Identifikasi Batu',0,0,'FJ');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'methods, testing standards, terms and criteria used in this',0,0,'FJ');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'Adi.',0,0,'L');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'Gemstone Identification Report.',0,0,'L');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '5.',0,0,'L');
		$pdf->Cell($width[2], 5, 'Laporan Identifikasi Batu Adi ini tidak dapat digandakan baik',0,0,'FJ');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '5.',0,0,'L');
		$pdf->Cell($width[2], 5, 'This Gemstone Identification Report may not be reproduced in whole',0,0,'FJ');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'sebagian ataupun seluruhnya tanpa izin tertulis dari Laboratorium',0,0,'FJ');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'or in part without written authorization from Pegadaian Gemological',0,0,'FJ');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'Gemologi Pegadaian.',0,0,'L');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'Laboratory.',0,0,'L');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '6.',0,0,'L');
		$pdf->Cell($width[2], 5, 'Laboratorium Gemologi Pegadaian tidak bertanggung jawab atas',0,0,'FJ');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '6.',0,0,'L');
		$pdf->Cell($width[2], 5, 'Pegadaian Gemological Laboratory are not responsible for any',0,0,'FJ');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'kemungkinan adanya segala kesalahan identifikasi sebagaimana',0,0,'FJ');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'possibility of misidentification as set forth in this Gemstone',0,0,'FJ');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'tertera dalam Laporan Identifikasi Batu Adi ataupun',0,0,'FJ');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'Identification Report, or misuse of this Gemstone Identification',0,0,'FJ');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'penyalahgunaan Laporan Identifikasi Batu Adi yang berakibat',0,0,'FJ');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'Report which resulted in the loss of customers or other',0,0,'FJ');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'kerugian pada nasabah ataupun pihak lainnya.',0,0,'L');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'parties.',0,0,'L');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '7.',0,0,'L');
		$pdf->Cell($width[2], 5, 'Laboratorium Gemologi Pegadaian tidak bertanggung jawab atas',0,0,'FJ');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '7.',0,0,'L');
		$pdf->Cell($width[2], 5, 'Pegadaian Gemological Laboratory are not responsible for any',0,0,'FJ');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'segala kerusakan dan perubahan yang mungkin terjadi pada saat',0,0,'FJ');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'damage and changes that may occur during the process of',0,0,'FJ');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'proses pemeriksaan dan pengujian, nasabah setuju untuk',0,0,'FJ');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'examination and testing, customer agrees to release Pegadaian',0,0,'FJ');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'melepaskan Laboratorium Gemologi Pegadaian atas segala',0,0,'FJ');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'Gemological Laboratory for any lawsuits.',0,0,'L');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'tuntutan hukum.',0,0,'L');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '8.',0,0,'L');
		$pdf->Cell($width[2], 5, 'All expenses related to the making of this Gemstone Identification',0,0,'FJ');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '8.',0,0,'L');
		$pdf->Cell($width[2], 5, 'Segala biaya yang timbul atas pembuatan Laporan Identifikasi Batu',0,0,'FJ');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'Report by Pegadaian Gemological Laboratory is the responsibility of',0,0,'FJ');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'Adi oleh Laboratorium Gemologi Pegadaian merupakan tanggung',0,0,'FJ');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'the customer.',0,0,'L');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'jawab nasabah.',0,0,'L');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '9.',0,0,'L');
		$pdf->Cell($width[2], 5, 'This Gemstone Identification Report can not be and are not for sale.',0,0,'FJ');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '9.',0,0,'L');
		$pdf->Cell($width[2], 5, 'Laporan Identifikasi Batu Adi ini tidak dapat dan tidak untuk',0,0,'FJ');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, '',0,0,'L');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'diperjualbelikan.',0,0,'L');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, '',0,0,'L');
		$pdf->Ln(10);
		
		$pdf->SetFont('ronnia','',10);
		$pdf->Cell($width[0]+$width[0]+$width[3], 5, 'LABOTARIUM GEMOLOGI PEGADAIAN '.strtoupper($store),0,0,'C');
		$pdf->Ln();
		$pdf->Cell($width[0]+$width[0]+$width[3], 5, $lokasi,0,0,'C');
		$pdf->Ln();
		$pdf->Cell($width[0]+$width[0]+$width[3], 5, 'Telp. +62 '.sessionValue('telepon').$hunting.'Fax. +62 '.sessionValue('fax').'   '.$tmpUrl[0]->url,0,0,'C');
		$pdf->Ln();*/
		
		/*$pdf->SetFont('ronnia','',16);
		$pdf->SetXY(1, 1);
		$pdf->Cell(0, 0, '+');
		//$pdf->SetXY(290/2, 3);
		//$pdf->Cell(0, 0, '|');
		$pdf->SetXY(292, 1);
		$pdf->Cell(0, 0, '+');
		$pdf->SetXY(1, 207);
		$pdf->Cell(0, 0, '+');
		$pdf->SetXY(292, 207);
		$pdf->Cell(0, 0, '+');*/

		$pdf->Output();
	}
}
?>