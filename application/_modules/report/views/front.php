<script type="text/javascript">
    jQuery(function($){
        $('#example1').DataTable();
        $(".datepicker").datepicker();
	});
</script>	

	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>&nbsp;</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
            <li>Report</li>
            <li class="active"><?php echo ($menu=="Full Report")?"Identification Report":$menu;?></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="box box-default">
            <div class="box-header">
              <b>Report - <?php echo ($menu=="Full Report")?"Identification Report":$menu;?></b>
              <div style="border: 1px solid black; margin-bottom: 10px;"></div>
			  
                <form action="" method="get">
                    <div class="container-fluid" style="margin-top: 10px;">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="from-group">
									<table width="100%" border="0">
									<tr>
										<td>
											<label><b>Cari Tanggal : </b></label>
										</td>
										<td>
											<input type="text" name="from" class="form-control datepicker">
										</td>
										<td>
											&nbsp;-&nbsp;
										</td>
										<td>
											<input type="text" name="to" class="form-control datepicker">
										</td>
										<td>
											&nbsp;<button type="submit" class="btn btn-primary" style="margin-top: 0px;"><i class="fa fa-search"></i></button>
										</td>
									</tr>
									</table>
                                </div>  
                            </div>
                        </div>
                    </div>
                </form>
				
              <table class="table table-bordered table-striped table-hover" id="example1">
                <thead>
                  <tr>
                    <th class="text-center" width="5%">No</th>
                    <th class="text-center">Nasabah</th>
                    <th class="text-center" width="10%">Tanggal</th>
                    <th class="text-center" width="15%">No. Sertifikat</th>
					<?php if($menu=="Diamond Grading"){ ?>
                    <th class="text-center" width="20%">Berat</th>
					<?php }else if($menu=="Taksiran"){ ?>
                    <th class="text-center" width="20%">Taksiran</th>
					<?php }else{ ?>
                    <th class="text-center" width="20%">Hasil Sertifikasi</th>
					<?php } ?>
                    <th class="text-center" width="15%">Pendapatan</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $no = 1;
				  $search = "";
				  if(!isset($_GET['from']) && !isset($_GET['to'])){
					$date_se = date('Y-m-d');
					if($menu=="Brief Report"){
						$where = " AND DATE_FORMAT(tb_lab_desk.finish,'%d-%m-%Y')=DATE_FORMAT(NOW(),'%d-%m-%Y') AND tb_front_desk.gem_card!='' ";
					}else if($menu=="Full Report"){
						$where = " AND DATE_FORMAT(tb_lab_desk.finish,'%d-%m-%Y')=DATE_FORMAT(NOW(),'%d-%m-%Y') AND tb_front_desk.certificate!='' ";
					}else if($menu=="Diamond Grading"){
						$where = " AND DATE_FORMAT(tb_lab_grading.finish,'%d-%m-%Y')=DATE_FORMAT(NOW(),'%d-%m-%Y') AND tb_front_desk.dia_grading!='' ";
					}else if($menu=="Taksiran"){
						$where = " AND DATE_FORMAT(tb_taksiran.create_date,'%d-%m-%Y')=DATE_FORMAT(NOW(),'%d-%m-%Y') ";
					}
					
					
					$from = $date_se;
					$to = $date_se;
				  }else{
					$search = "1";
					$from = $_GET['from'];
					$to = $_GET['to'];

					$from = date_create($from);
					$from = date_format($from, "Y-m-d");
					$to = date_create($to);
					$to = date_format($to, "Y-m-d");
					$to = date('Y-m-d',strtotime($to. "+1 days"));
					
					if($menu=="Brief Report"){
						$where = " AND tb_lab_desk.finish >= '$from 12:00:00 AM' AND tb_lab_desk.finish <= '$to 12:59:59 PM' 
						AND tb_front_desk.gem_card!='' ";
					}else if($menu=="Full Report"){
						$where = " AND tb_lab_desk.finish >= '$from 12:00:00 AM' AND tb_lab_desk.finish <= '$to 11:59:59 PM'  
						AND tb_front_desk.certificate!='' ";
					}else if($menu=="Diamond Grading"){
						$where = " AND tb_lab_grading.finish >= '$from 12:00:00 AM' AND tb_lab_grading.finish <= '$to 11:59:59 PM'  
						AND tb_front_desk.dia_grading!='' ";
					}else if($menu=="Taksiran"){
						$where = " AND tb_taksiran.create_date >= '$from 12:00:00 AM' AND tb_taksiran.create_date <= '$to 11:59:59 PM' ";
					}
				  }
				  
                  if($menu=="Brief Report"){
						$kode = "BR";
						$get_today = $this->db->query("SELECT tb_lab_desk.*,
												tb_front_desk.certificate, tb_front_desk.gem_card, tb_front_desk.dia_grading,
												tb_member.first_name, tb_lab_desk.obj_natural,
												step.no_brief,
												tb_service.harga harga
												FROM tb_lab_desk
												LEFT JOIN tb_member ON tb_member.id_member = tb_lab_desk.id_member
												LEFT JOIN tb_front_desk ON tb_front_desk.id_object = tb_lab_desk.id_object
												LEFT JOIN step ON step.id_object = tb_lab_desk.id_object
												JOIN tb_service ON tb_service.nama = tb_front_desk.gem_card
												WHERE ISNULL(tb_lab_desk.delete_by) 
												AND step.pay='Y'
												AND tb_front_desk.store='".sessionValue('kode_store')."'
												".$where);
				  }else if($menu=="Full Report"){
						$kode = "FR";
						$get_today = $this->db->query("SELECT tb_lab_desk.*,
												tb_front_desk.certificate, tb_front_desk.gem_card, tb_front_desk.dia_grading,
												tb_member.first_name,
												step.no_sertifikat,
												tb_service.harga harga
												FROM tb_lab_desk
												LEFT JOIN tb_member ON tb_member.id_member = tb_lab_desk.id_member
												LEFT JOIN tb_front_desk ON tb_front_desk.id_object = tb_lab_desk.id_object
												LEFT JOIN step ON step.id_object = tb_lab_desk.id_object
												JOIN tb_service ON tb_service.nama = tb_front_desk.certificate
												WHERE ISNULL(tb_lab_desk.delete_by) 
												AND step.pay='Y'
												AND tb_front_desk.store='".sessionValue('kode_store')."'
												".$where);
				  }else if($menu=="Diamond Grading"){
						$kode = "DG";
						$get_today = $this->db->query("SELECT tb_lab_grading.*,
												tb_front_desk.certificate, tb_front_desk.gem_card, tb_front_desk.dia_grading,
												tb_member.first_name,
												step.no_grading,
												tb_service.harga harga
												FROM tb_lab_grading
												LEFT JOIN tb_member ON tb_member.id_member = tb_lab_grading.id_member
												LEFT JOIN tb_front_desk ON tb_front_desk.id_object = tb_lab_grading.id_object
												LEFT JOIN step ON step.id_object = tb_lab_grading.id_object
												JOIN tb_service ON tb_service.nama = tb_front_desk.dia_grading
												WHERE ISNULL(tb_lab_grading.delete_by) 
												AND step.pay='Y'
												AND tb_front_desk.store='".sessionValue('kode_store')."'
												".$where);
				  }else if($menu=="Taksiran"){
						$kode = "JT";
						$get_today = $this->db->query("SELECT tb_taksiran.*, tb_taksiran.create_date finish,
												tb_member.first_name,
												step.no_taksiran
												FROM tb_taksiran
												LEFT JOIN tb_member ON tb_member.id_member = tb_taksiran.id_member
												LEFT JOIN step ON step.id_object = tb_taksiran.id
												LEFT JOIN tb_login ON tb_login.id = tb_taksiran.create_by
												WHERE ISNULL(tb_taksiran.delete_by) 
												AND step.pay='Y'
												AND tb_login.store='".sessionValue('kode_store')."'
												".$where." GROUP BY tb_taksiran.id_order ");
				  }
                  foreach ($get_today->result() as $get) {
					  if($menu=="Brief Report"){
						  $kode = $get->no_brief;
						if($get->obj_natural!="NATURAL"){
							$mem_pri = mysql_query("SELECT harga FROM tb_service WHERE kode='BRS'");
							$pri_mem = mysql_fetch_array($mem_pri);
						}else{
							$mem_pri = mysql_query("SELECT harga FROM tb_service WHERE kode='BR'");
							$pri_mem = mysql_fetch_array($mem_pri);
						}
						$get->harga = $pri_mem['harga'];
					  }else if($menu=="Full Report"){
						  $kode = $get->no_sertifikat;
					  }if($menu=="Diamond Grading"){
						  $kode = $get->no_grading;
						  $getHarga = $this->db->query("SELECT harga
								FROM master_harga_grading
								WHERE berat2 >= ".$get->obj_weight." ORDER BY ID ASC LIMIT 0,1");
						   
						   foreach ($getHarga->result() as $tmpHarga) {
								$get->harga = $tmpHarga->harga;
						   }
					  }if($menu=="Taksiran"){
						  $harga_total_taksiran = 0;
						  $kode = $get->no_taksiran;
							$getHDLE = $this->db->query("SELECT * FROM master_harga_emas WHERE id='1'");
							$hdle = 0;
							foreach ($getHDLE->result() as $hsp) {
								$hdle = $hsp->harga_emas;
							}
							$getEmas = $this->db->query("SELECT * FROM tb_taksiran
														LEFT JOIN master_logam ON master_logam.id = tb_taksiran.jenis_logam
														WHERE tb_taksiran.id_order='".$get->id_order."' AND tb_taksiran.jenis_logam='1'");
							foreach ($getEmas->result() as $key) {
								$harga_taksiran = 1.25/100*(($key->berat_bersih*($key->karatase_emas/24))*$hdle); // $key->jumlah_perhiasan*
								$harga_total_taksiran = $harga_total_taksiran + $harga_taksiran;
							} 
							
							$getBerlian = $this->db->query("SELECT berat_total_permata FROM tb_taksiran
								WHERE tb_taksiran.id_order='".$get->id_order."' ");							
							foreach ($getBerlian->result() as $key) {
								if($key->berat_total_permata == ""){
									$berat_total = $berat_total + 0;
								}else{
									$berat_total = $berat_total + $key->berat_total_permata;
								}
								if($berat_total>0){
									$getHarga = $this->db->query("SELECT harga
																FROM `master_tarif_taksiran`
																WHERE berat2 >= ".$berat_total." ORDER BY ID ASC LIMIT 0,1");
																
									foreach ($getHarga->result() as $tmpHarga) {
										$harga_berlian = $harga_berlian + $tmpHarga->harga;
									}
									$harga_total_taksiran = $harga_total_taksiran + $harga_berlian;
								}
							}
							
							$harga_total_taksiran = roundNearestHundredUp($harga_total_taksiran);
					  }
                    ?>
                    <tr>
                      <td class="text-center"><?php echo $no++; ?></td>
                      <td class="text-left"><?php echo $get->first_name; ?></td>
                      <td class="text-center"><?php echo date('d-m-Y', strtotime($get->finish)); ?></td>
                      <td class="text-center"><?php echo $kode; ?></td>
					  <?php if($menu=="Diamond Grading"){ ?>
					  <td class="text-right"><?php echo $get->obj_weight; ?></td>
					  <?php }else if($menu=="Taksiran"){ ?>
                      <td class="text-right"><?php echo number_format($get->taksiran,0,',','.'); ?></td>
					  <?php }else{ ?>
                      <td class="text-left"><?php echo $get->obj_natural.' '.$get->variety; ?></td>
					  <?php } 
					  if($menu=="Taksiran"){
					  ?>
					  <td class="text-right"><?php echo number_format($harga_total_taksiran,0,',','.'); ?></td>
					  <?php }else{ ?>
                      <td class="text-right"><?php echo number_format($get->harga,0,',','.'); ?></td>
					  <?php } ?>
                    </tr>
                    <?php
                  }
				  
				  $menu = str_replace(' ', '-',$menu);
                  ?>
                </tbody>
                <tfoot>
                  <tr>
                    <td colspan="6">
                      <div class="pull-right">
                        <a href="<?php echo site_url('report/print_report_sertificate').'/'.$from.'/'.$to.'/'.$menu.'/'.$search; ?>" target="_blank" class="btn btn-primary">PRINT REPORT</a>
                      </div>
                    </td>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->