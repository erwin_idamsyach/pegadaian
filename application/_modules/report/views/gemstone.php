<script type="text/javascript">
    jQuery(function($){
        $('#example1').DataTable();
        $(".datepicker").datepicker();
	});
</script>	

	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>&nbsp;</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
            <li>Report</li>
            <li class="active"><?php echo $menu;?></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="box box-default">
            <div class="box-header">
              <b>Report - <?php echo $menu;?></b>
              <div style="border: 1px solid black; margin-bottom: 10px;"></div>
			  
                <form action="" method="get">
                    <div class="container-fluid" style="margin-top: 10px;">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="from-group">
									<table width="100%" border="0">
									<tr>
										<td>
											<label><b>Cari Tanggal : </b></label>
										</td>
										<td>
											<input type="text" name="from" class="form-control datepicker">
										</td>
										<td>
											&nbsp;-&nbsp;
										</td>
										<td>
											<input type="text" name="to" class="form-control datepicker">
										</td>
										<td>
											&nbsp;<button type="submit" class="btn btn-primary" style="margin-top: 0px;"><i class="fa fa-search"></i></button>
										</td>
									</tr>
									</table>
                                </div>  
                            </div>
                        </div>
                    </div>
                </form>
				
              <table class="table table-bordered table-striped table-hover" id="example1">
                <thead>
                  <tr>
                    <th class="text-center" width="5%">No</th>
                    <th class="text-center" width="10%">Tanggal</th>
                    <th class="text-center">Varietas</th>
                    <!--<th class="text-center" width="12%">Warna</th>-->
                    <th class="text-center" width="15%">Bentuk & Gosokan</th>
                    <!--<th class="text-center" width="15%">Ukuran</th>-->
                    <th class="text-center" width="12%">Berat</th>
                    <!--<th class="text-center">Komentar</th>-->
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $no = 1;	
				  $search = "";
				  if(!isset($_GET['from']) && !isset($_GET['to'])){
					$date_se = date('d-m-Y');
					$where = " AND DATE_FORMAT(step.create_date,'%d-%m-%Y')=DATE_FORMAT(NOW(),'%d-%m-%Y') ";
					
					$from = $date_se;
					$to = $date_se;
				  }else{
					$search = "1";
					$from = $_GET['from'];
					$to = $_GET['to'];

					$from = date_create($from);
					$from = date_format($from, "Y-m-d");
					$to = date_create($to);
					$to = date_format($to, "Y-m-d");
					$to = date('Y-m-d',strtotime($to. "+1 days"));
					
					$where = " AND step.create_date >= '$from 12:00:00 AM' AND step.create_date <= '$to 11:59:59 PM' ";
					
				  }
				  
					$get_today = $this->db->query("SELECT tb_lab_desk.*, step.id_order, step.no_sertifikat,
												step.no_brief, color_stone.jenis_warna, step.create_date
												FROM step
												JOIN tb_lab_desk ON tb_lab_desk.id_object = step.id_object
												LEFT JOIN color_stone ON color_stone.code = tb_lab_desk.obj_color
												WHERE ISNULL(tb_lab_desk.delete_by) AND step.status_barang='SELESAI'
												".$where);
				  
                  foreach ($get_today->result() as $get) {
                    ?>
                    <tr>
                      <td class="text-center"><?php echo $no++; ?></td>
                      <td class="text-center"><?php echo date('d-m-Y', strtotime($get->create_date)); ?></td>
                      <td class="text-left"><?php echo ucwords(strtolower($get->variety)); ?></td>
                      <!--<td class="text-left"><?php //echo ucwords($get->jenis_warna); ?></td>-->
					  <td class="text-left"><?php echo $get->obj_shape." ".$get->obj_cut; ?></td>
					  <!--<td class="text-center"><?php //echo $get->obj_length.' x '.$get->obj_width." x ".$get->obj_height." mm"; ?></td>-->
                      <td class="text-right"><?php echo $get->obj_weight." cts"; ?></td>
                      <!--<td class="text-left"><?php //echo ucwords($get->note); ?></td>-->
                    </tr>
                    <?php
				  }
				  
				  $menu = str_replace(' ', '-',$menu);
                  ?>
                </tbody>
                <tfoot>
                  <tr>
                    <td colspan="5">
                      <div class="pull-right">
                        <a href="<?php echo site_url('report/print_report_gemstone').'/'.$from.'/'.$to.'/'.$menu.'/'.$search; ?>" target="_blank" class="btn btn-primary">PRINT REPORT</a>
                      </div>
                    </td>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->