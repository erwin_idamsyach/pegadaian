<script type="text/javascript">
    jQuery(function($){
        $('#example1').DataTable();
        $(".datepicker").datepicker();
	});
</script>	

	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>&nbsp;</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
            <li>Report</li>
            <li class="active"><?php echo $menu;?></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="box box-default">
            <div class="box-header">
              <b>Report - <?php echo $menu;?></b>
              <div style="border: 1px solid black; margin-bottom: 10px;"></div>
			  
                <form action="" method="get">
                    <div class="container-fluid" style="margin-top: 10px;">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="from-group">
									<table width="100%" border="0">
									<tr>
										<td>
											<label><b>Cari Tanggal : </b></label>
										</td>
										<td>
											<input type="text" name="from" class="form-control datepicker">
										</td>
										<td>
											&nbsp;-&nbsp;
										</td>
										<td>
											<input type="text" name="to" class="form-control datepicker">
										</td>
										<td>
											&nbsp;<button type="submit" class="btn btn-primary" style="margin-top: 0px;"><i class="fa fa-search"></i></button>
										</td>
									</tr>
									</table>
                                </div>  
                            </div>
                        </div>
                    </div>
                </form>
				
              <table class="table table-bordered table-striped table-hover" id="example1">
                <thead>
                  <tr>
                    <th class="text-center" width="5%">No</th>
                    <th class="text-center">Nasabah</th>
                    <th class="text-center" width="10%">Tanggal</th>
                    <th class="text-center" width="15%">No. Sertifikat</th>
                    <th class="text-center" width="20%">Hasil Sertifikasi</th>
                    <th class="text-center" width="15%">Pendapatan</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $no = 1;
				  
				  if(!isset($_GET['from']) && !isset($_GET['to'])){
					$date_se = date('d-m-Y');
					if($menu=="Brief Report"){
						$where = " AND DATE_FORMAT(tb_lab_desk.finish,'%d-%m-%Y')=DATE_FORMAT(NOW(),'%d-%m-%Y') ";
					}else if($menu=="Full Report"){
						$where = " AND DATE_FORMAT(tb_lab_desk.finish,'%d-%m-%Y')=DATE_FORMAT(NOW(),'%d-%m-%Y') ";
					}
					
					
					$from = $date_se;
					$to = $date_se;
				  }else{
					$from = $_GET['from'];
					$to = $_GET['to'];

					$from = date_create($from);
					$from = date_format($from, "d-m-Y");
					$to = date_create($to);
					$to = date_format($to, "d-m-Y");
					$to = date('Y-m-d',strtotime($to. "+1 days"));
					
					if($menu=="Brief Report"){
						$where = " AND DATE_FORMAT(tb_lab_desk.finish,'%d-%m-%Y') >= '$from' AND DATE_FORMAT(tb_lab_desk.finish,'%d-%m-%Y') <= '$to' 
						AND tb_front_desk.gem_card!='' ";
					}else if($menu=="Full Report"){
						$where = " AND DATE_FORMAT(tb_lab_desk.finish,'%d-%m-%Y') >= '$from' AND DATE_FORMAT(tb_lab_desk.finish,'%d-%m-%Y') <= '$to' 
						AND tb_front_desk.certificate!='' ";
					}else if($menu=="Diamond Grading"){
						$where = " AND DATE_FORMAT(tb_lab_desk.finish,'%d-%m-%Y') >= '$from' AND DATE_FORMAT(tb_lab_desk.finish,'%d-%m-%Y') <= '$to' 
						AND tb_front_desk.dia_grading!='' ";
					}
				  }
				  
                  if($menu=="Brief Report"){
						$kode = "BR";
						$get_today = $this->db->query("SELECT tb_lab_desk.*,
												tb_front_desk.certificate, tb_front_desk.gem_card, tb_front_desk.dia_grading,
												tb_member.first_name,
												tb_service.harga harga
												FROM tb_lab_desk
												LEFT JOIN tb_member ON tb_member.id_member = tb_lab_desk.id_member
												LEFT JOIN tb_front_desk ON tb_front_desk.id_object = tb_lab_desk.id_object
												JOIN tb_service ON tb_service.nama = tb_front_desk.gem_card
												WHERE ISNULL(tb_lab_desk.delete_by) 
												".$where);
				  }else if($menu=="Full Report"){
						$kode = "FR";
						$get_today = $this->db->query("SELECT tb_lab_desk.*,
												tb_front_desk.certificate, tb_front_desk.gem_card, tb_front_desk.dia_grading,
												tb_member.first_name,
												tb_service.harga harga
												FROM tb_lab_desk
												LEFT JOIN tb_member ON tb_member.id_member = tb_lab_desk.id_member
												LEFT JOIN tb_front_desk ON tb_front_desk.id_object = tb_lab_desk.id_object
												JOIN tb_service ON tb_service.nama = tb_front_desk.certificate
												WHERE ISNULL(tb_lab_desk.delete_by) 
												".$where);
				  }else if($menu=="Diamond Grading"){
						$kode = "DG";
						$get_today = $this->db->query("SELECT tb_lab_desk.*,
												tb_front_desk.certificate, tb_front_desk.gem_card, tb_front_desk.dia_grading,
												tb_member.first_name,
												tb_service.harga harga
												FROM tb_lab_desk
												LEFT JOIN tb_member ON tb_member.id_member = tb_lab_desk.id_member
												LEFT JOIN tb_front_desk ON tb_front_desk.id_object = tb_lab_desk.id_object
												JOIN tb_service ON tb_service.nama = tb_front_desk.dia_grading
												WHERE ISNULL(tb_lab_desk.delete_by) 
												".$where);
				  }
                  foreach ($get_today->result() as $get) {
                    ?>
                    <tr>
                      <td class="text-center"><?php echo $no++; ?></td>
                      <td class="text-left"><?php echo $get->first_name; ?></td>
                      <td class="text-center"><?php echo date('d-m-Y', strtotime($get->finish)); ?></td>
                      <td class="text-center"><?php echo $kode; ?></td>
					  <?php if($menu=="Diamond Grading"){ ?>
					  <td class="text-right"><?php echo $get->obj_weight; ?></td>
					  <?php }else{ ?>
                      <td class="text-left"><?php echo $get->obj_natural.' '.$get->variety; ?></td>
					  <?php } ?>
                      <td class="text-right"><?php echo number_format($get->harga,0,',','.'); ?></td>
                    </tr>
                    <?php
                  }
				  
				  $menu = str_replace(' ', '-',$menu);
                  ?>
                </tbody>
                <tfoot>
                  <tr>
                    <td colspan="6">
                      <div class="pull-right">
                        <a href="<?php echo site_url('report/print_report_sertificate').'/'.$from.'/'.$to.'/'.$menu; ?>" target="_blank" class="btn btn-primary">PRINT REPORT</a>
                      </div>
                    </td>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->