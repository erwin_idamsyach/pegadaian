<?php if (! defined('BASEPATH')){ exit('No direct script allowed'); }
/*
* Controller List FPJS
* Create by Erwin Idamsyach @ 2 May 2016
*/
class invoice extends CI_Controller{
	function invoice(){
        parent::__construct();
		
		if(!isLogin()){
			goLogin();
		}
        $this->load->library('fpdf');
    }
	
	public function index(){
		$data['filelist'] = 'invoice/fpjs';
		$data['title'] = 'List Invoice (BPS)';
		$data['title_menu'] = 'Front';
		$data['menu'] = 'invoice';

		getHTMLWeb($data);
	}
	
	public function print_invoice_fpjs(){
		$ord = $this->uri->segment(3);
		$ord = str_replace('-', '/', $ord);
		$data['ord'] = $ord;
		$this->load->view('print-invoice-fpjs', $data);
	}
	
	public function view_data(){
		$key = $this->input->post('key');
		$data['key'] = $key;
		$this->load->view('view', $data);
	}
	
	public function fpjt(){
		$data['filelist'] = 'invoice/fpjt';
		$data['title'] = 'List Invoice (BPT)';
		$data['title_menu'] = 'Front';
		$data['menu'] = 'invoice_fpjt';

		getHTMLWeb($data);
	}
	
	public function print_invoice_fpjt(){		
		$ord = $this->uri->segment(3);
		$ord = str_replace('-', '/', $ord);
		$data['ord'] = $ord;
		$this->load->view('print-invoice-fpjt', $data);
	}
}
?>