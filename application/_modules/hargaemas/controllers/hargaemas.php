<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class hargaemas extends CI_Controller{
	public $table = "master_harga_emas";
	public $controls = "hargaemas";
	
    function hargaemas(){
        parent::__construct();
    
		if(!isLogin()){
		  goLogin();
		}
    }
    
    function index($text=NULL){
        $data["filelist"] = $this->controls."/hargaemas";
		$data['title'] = 'Parameter';
		$data['title_menu'] = 'Admin';
		$data['menu'] = 'Harga Emas';
		$data['controls'] = $this->controls;
		$data['table'] = $this->table;
    
        getHTMLWeb($data);
    }

    function editData($id)
    {
		$data['id'] = $id;
		$data['controls'] = $this->controls;
		$data['table'] = $this->table;
		
        $this->load->view('edit', $data);
    }

    function saveEditData()
    {
		$id = $this->input->post('id');
        $price = $this->input->post('price');

        $data = array(
            'harga_emas'  => convertToNumber($price),
			'update_by' => sessionValue('id'),
			'update_date' => date('Y-m-d H:i:s')
        );

		$success = false;
		$msg = "";
		
        $this->db->where('id', $id);
        if($this->db->update($this->table, $data)){
			$success = true;
		}else{
			$msg = "Data tidak tersimpan";
		}
		
		echo json_encode(array("success"=>$success, "msg"=>$msg));
    }
}
