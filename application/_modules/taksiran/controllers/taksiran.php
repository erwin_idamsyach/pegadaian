<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class taksiran extends CI_Controller{
    function taksiran(){
        parent::__construct();
		
		if(!isLogin()){
			goLogin();
		}
    }
    
    function index($text=NULL){
        $data["filelist"] = "taksiran/taksiran";
        $data["title"] = "Lab";
        $data["title_menu"] = "Lab";
        $data["menu"] = "taksiran";
		
        getHTMLWeb($data);
    }
}
