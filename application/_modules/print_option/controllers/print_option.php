<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
* Controller Parameter Print Option
* Create By : Erwin Idamsyach Putra
* 23 April 2016
*/
class print_option extends CI_Controller{
	function __construct(){
		parent::__construct();

		if(!isLogin()){
			goLogin();
		}
	}
	
	public function index(){
		$data['filelist'] = 'print_option/front';
		$data['title'] = 'Print Option';
		$data['title_menu'] = 'Admin';
		$data['menu'] = 'print_option';

		getHTMLWeb($data);
	}
	public function change_option(){
		$opt = $this->input->post('option');

		if($opt == "PUSAT"){
			$param = 'PUSAT';
			$where = 'CABANG';
		}else{
			$param = 'CABANG';
			$where = 'PUSAT';
		}

		$this->db->where('opsi_print', $where);
		$this->db->update('param_opsi_print', array('opsi_print'=>$param));
		redirect(site_url('print_option'));
	}
}
?>