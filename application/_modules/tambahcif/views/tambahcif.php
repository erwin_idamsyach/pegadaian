	   <script type="text/javascript">
        jQuery(function($){
           $("#kode_pos").mask("99999",{placeholder:"_____"});
           $("#kode_pos2").mask("99999",{placeholder:"_____"});
		   $("#telpon").mask("99999999",{placeholder:""});
           $("#handphone").mask("99999999999",{placeholder:""});
		   
		   $("#telpon_perusahaan").mask("99999999",{placeholder:""});
		   
		   $('.datepicker').datepicker();
			
			
			$("#alamat_sesuai").on("change", function(){
				var alamat_sesuai = $("#alamat_sesuai").val();
				if(alamat_sesuai=="Y"){
					$("#alamat_domisili").val($("#alamat").val());
					$("#propinsi2").val($("#propinsi").val());
					
					var id_kota = $(".kota").val(); 
					var id_kec = $(".keca").val();
					var id_prov = $(".prov").val();
					
					$.ajax({
						type : "POST",
						url : base_url+"tambahcif/kota/"+id_prov,
						data : "id_prov="+id_prov,
						success:function(html){ 
						  $(".kota2").html(html);
						  $("#kecamatan2").val($("#kecamatan").val());

							$.ajax({
							  type : "POST",
							  url : base_url+"tambahcif/kecam/"+id_kota,
							  data : "id_kota="+id_kota,
							  success:function(html){
								$(".keca2").html(html);
								$.ajax({
								  type : "POST",
								  url : base_url+"tambahcif/kelur/"+id_kec,
								  data : "id_kec="+id_kec,
								  success:function(html){
									$(".desa2").html(html);
									
									$("#kota2").val($("#kota").val());
									$("#kecamatan2").val($("#kecamatan").val());
									$("#kelurahan2").val($("#kelurahan").val());
								  }
								});
							  }
							  });
						
						}
					  });
					//
					//$("#kecamatan2").val($("#kecamatan").val());
					//$("#kelurahan2").val($("#kelurahan").val());
					$("#kode_pos2").val($("#kode_pos").val());
				}else{
					$("#alamat_domisili").val('');
					$("#propinsi2").val('');
					$("#kota2").val('');
					$("#kecamatan2").val('');
					$("#kelurahan2").val('');
					$("#kode_pos2").val('');
				}
			});
        });
      </script>
	  
    <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>&nbsp;</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-user"></i> Reg. CIF</a></li>
            <li class="active">Add CIF</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="index">
            <div class="box">
              <div class="box-header">
                <b>ADD CIF</b>
                <div style="border:1px solid black;margin-bottom:0px;"></div>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div>
              </div>
              <div class="box-body">
				<div class="nav-tabs-custom">
                  <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                      <a href="#tab-individu" aria-controls="home" role="tab" data-toggle="tab"><b>INDIVIDUAL</b></a>
                    </li>
                    <li role="presentation">
                      <a href="#tab-coorporate" aria-controls="profile" role="tab" data-toggle="tab"><b>KORPORASI</b></a>
                    </li>
                  </ul>
				  
				  <!-- Tab panes -->
                  <div class="tab-content">
				  
				  <div role="tabpanel" class="tab-pane fade in active" id="tab-individu">
					<div>
					<form method="POST" onSubmit="return save_cif_individu()" >
					  <div class="row">
						<div class="col-md-12">
						<i>CUSTOMER INFORMATION FILE(CIF)</i>
						<div style="border:1px solid black;margin-bottom:10px;"></div>
						</div>
					  </div>
					  <div class="row">		
						<div class="col-md-3">
						  <div class="form-group">
							<label>Nomor CIF <font color="red">*</font></label>
							<input type="text" name="no_cif" id="no_cif" class="form-control" placeholder="Nomor CIF" value="<?php echo $cif_number;?>" readonly required>
						  </div>
						</div>
					  </div>
					  <div class="row">
						<div class="col-md-12">
						<i>DATA PRIBADI</i>
						<div style="border:1px solid black;margin-bottom:10px;"></div>
						</div>
					  </div>
					  <div class="row">
						<div class="col-md-9">
						  <div class="form-group">
							<label>Nama Lengkap <font color="red">*</font></label>
							<input type="text" name="nama_depan" id="nama_depan" class="form-control" placeholder="Nama Lengkap" required>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Nama Ibu Kandung <font color="red">*</font></label>
							<input type="text" name="nama_ibu" id="nama_ibu" class="form-control" placeholder="Nama Ibu Kandung" required>
						  </div>
						</div>
						<div class="col-md-6">
						  <div class="form-group">
							<label>Tempat Lahir <font color="red">*</font></label>
							<input type="text" name="tempat_lahir" id="tempat_lahir" class="form-control" placeholder="Tempat Lahir" required>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Tanggal Lahir <font color="red">*</font></label>
							<input type="text" name="tanggal_lahir" id="tanggal_lahir" class="form-control datepicker" placeholder="Tanggal Lahir" required>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Agama <font color="red">*</font></label>
							<select name="agama" id="agama" class="form-control" required>
							  <option value="">Select Agama</option>
							<?php
							  $get_pro = mysql_query("SELECT * FROM master_agama WHERE ISNULL(delete_by) order by id");
							  while ($do = mysql_fetch_array($get_pro)) {
							?>
							  <option value="<?php echo $do['id'] ?>"><?php echo $do['agama'] ?></option>
							<?php
							  }
							?>
							</select>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Identitas <font color="red">*</font></label>
							<select name="identitas" id="identitas" class="form-control" required>
							  <option value="">Select Identitas</option>
							<?php
							  $get_pro = mysql_query("SELECT * FROM master_identitas WHERE ISNULL(delete_by) order by id");
							  while ($do = mysql_fetch_array($get_pro)) {
							?>
							  <option value="<?php echo $do['id'] ?>"><?php echo $do['identitas'] ?></option>
							<?php
							  }
							?>
							</select>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group no_identitas">
							<label>No. KTP/SIM/PASSPOR <font color="red">*</font></label></label>
							<input type="text" name="no_identitas" id="no_identitas" class="form-control" placeholder="No. KTP/SIM/PASSPOR" required>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Masa Berlaku <font color="red">*</font></label>
							<input type="text" name="masa_berlaku" id="masa_berlaku" class="form-control datepicker" placeholder="Masa Berlaku" required>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Jenis Kelamin <font color="red">*</font></label>
							<select name="jenis_kelamin" id="jenis_kelamin" class="form-control" required>
							  <option value="">Select Jenis Kelamin</option>
							<?php
							  $get_pro = mysql_query("SELECT * FROM master_jenis_kelamin order by id");
							  while ($do = mysql_fetch_array($get_pro)) {
							?>
							  <option value="<?php echo $do['id'] ?>"><?php echo $do['jenis_kelamin'] ?></option>
							<?php
							  }
							?>
							</select>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Pendidikan Terakhir <font color="red">*</font></label>
							<select name="pendidikan" id="pendidikan" class="form-control" required>
							  <option value="">Select Pendidikan Terakhir</option>
							<?php
							  $get_pro = mysql_query("SELECT * FROM master_pendidikan WHERE ISNULL(delete_by) order by id");
							  while ($do = mysql_fetch_array($get_pro)) {
							?>
							  <option value="<?php echo $do['id'] ?>"><?php echo $do['pendidikan'] ?></option>
							<?php
							  }
							?>
							</select>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Status Perkawinan <font color="red">*</font></label>
							<select name="perkawinan" id="perkawinan" class="form-control" required>
							  <option value="">Select Status Perkawinan</option>
							<?php
							  $get_pro = mysql_query("SELECT * FROM master_perkawinan WHERE ISNULL(delete_by) order by id");
							  while ($do = mysql_fetch_array($get_pro)) {
							?>
							  <option value="<?php echo $do['id'] ?>"><?php echo $do['perkawinan'] ?></option>
							<?php
							  }
							?>
							</select>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Nama Pasangan</label>
							<input type="text" name="nama_pasangan" id="nama_pasangan" class="form-control" placeholder="Nama Pasangan">
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>No. Telpon Rumah</label>
							<div class="input-group"><span class="input-group-addon">021</span><input class="form-control" id="telpon" name="telpon" type="text"/></div>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>No. Handphone <font color="red">*</font></label>
							<div class="input-group"><span class="input-group-addon">62</span><input class="form-control" id="handphone" name="handphone" type="text" required /></div>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Email</label>
							<input type="text" name="email" id="email" class="form-control" placeholder="Email">
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>No. NPWP</label>
							<input type="text" name="no_npwp" id="no_npwp" class="form-control" placeholder="No. NPWP">
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Kewarganegaraan <font color="red">*</font></label>
							<select name="kewarganegaraan" id="kewarganegaraan" class="form-control" required>
							  <option value="">Select Kewarganegaraan</option>
							<?php
							  $get_pro = mysql_query("SELECT * FROM master_kewarganegaraan WHERE ISNULL(delete_by) order by id");
							  while ($do = mysql_fetch_array($get_pro)) {
							?>
							  <option value="<?php echo $do['id'] ?>"><?php echo $do['kewarganegaraan'] ?></option>
							<?php
							  }
							?>
							</select>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Kewarganegaraan Lainnya</label>
							<input type="text" name="kewarganegaraan_lainnya" id="kewarganegaraan_lainnya" class="form-control" placeholder="Kewarganegaraan Lainnya">
						  </div>
						</div>
					  </div>
					  <div class="row">
						<div class="col-md-12">
						<i>DATA KEUANGAN</i>
						<div style="border:1px solid black;margin-bottom:10px;"></div>
						</div>
					  </div>
					  <div class="row">
						<div class="col-md-3">
						  <div class="form-group">
							<label>Sumber Dana <font color="red">*</font></label>
							<select name="sumber_dana" id="sumber_dana" class="form-control" required>
							  <option value="">Select Sumber Dana</option>
							<?php
							  $get_pro = mysql_query("SELECT * FROM master_sumber_dana WHERE ISNULL(delete_by) order by id");
							  while ($do = mysql_fetch_array($get_pro)) {
							?>
							  <option value="<?php echo $do['id'] ?>"><?php echo $do['sumber_dana'] ?></option>
							<?php
							  }
							?>
							</select>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Rata-Rata Penghasilan</label>
							<select name="penghasilan" id="penghasilan" class="form-control">
							  <option value="">Select Penghasilan</option>
							<?php
							  $get_pro = mysql_query("SELECT * FROM master_penghasilan WHERE ISNULL(delete_by) order by id");
							  while ($do = mysql_fetch_array($get_pro)) {
							?>
							  <option value="<?php echo $do['id'] ?>"><?php echo $do['penghasilan'] ?></option>
							<?php
							  }
							?>
							</select>
						  </div>
						</div>
					  </div>
					  <div class="row">
						<div class="col-md-12">
						<i>DATA PEKERJAAN</i>
						<div style="border:1px solid black;margin-bottom:10px;"></div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Pekerjaan <font color="red">*</font></label>
							<select name="pekerjaan" id="pekerjaan" class="form-control" required>
							  <option value="">Select Pekerjaan</option>
							<?php
							  $get_pro = mysql_query("SELECT * FROM master_pekerjaan WHERE ISNULL(delete_by) order by id");
							  while ($do = mysql_fetch_array($get_pro)) {
							?>
							  <option value="<?php echo $do['id'] ?>"><?php echo $do['pekerjaan'] ?></option>
							<?php
							  }
							?>
							</select>
						  </div>
						</div>
						<div class="col-md-6">
						  <div class="form-group">
							<label>Alamat (Sesuai Identitas) <font color="red">*</font></label>
							<input type="text" name="alamat" id="alamat" class="form-control" placeholder="Alamat" required>
						  </div>
						</div><div class="col-md-3">
						  <div class="form-group">
							<label>Propinsi (Sesuai Identitas) <font color="red">*</font></label>
							<select name="province" id="propinsi" class="form-control prov" onchange="get_kota()" required>
							  <option value="">Select Propinsi</option>
							<?php
							  $get_pro = mysql_query("SELECT * FROM provinsi order by provinsiId");
							  while ($do = mysql_fetch_array($get_pro)) {
							?>
							  <option value="<?php echo $do['provinsiId'] ?>"><?php echo $do['provinsiNama'] ?></option>
							<?php
							  }
							?>
							</select>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Kota (Sesuai Identitas) <font color="red">*</font></label>
							<select name="city" id="kota" class="form-control kota" required>
							  <option value="">Select Kota</option>
							</select>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Kecamatan (Sesuai Identitas) <font color="red">*</font></label>
							<select name="district" id="kecamatan" class="form-control keca" required>
							  <option value="">Select Kecamatan</option>
							</select>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Kelurahan (Sesuai Identitas) <font color="red">*</font></label>
							<select name="kelurahan" id="kelurahan" class="form-control desa" required>
							  <option value="">Select Kelurahan</option>
							</select>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Kode Pos (Sesuai Identitas) <font color="red">*</font></label>
							<input type="text" name="kode_pos" id="kode_pos" class="form-control kode_pos" maxlength="5" placeholder="Kode Pos" required>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Status Tempat Tinggal </label>
							<select name="status_tempat_tinggal" id="status_tempat_tinggal" class="form-control">
							  <option value="">Select Status Tempat Tinggal</option>
							<?php
							  $get_pro = mysql_query("SELECT * FROM master_status_tempat_tinggal WHERE ISNULL(delete_by) order by id");
							  while ($do = mysql_fetch_array($get_pro)) {
							?>
							  <option value="<?php echo $do['id'] ?>"><?php echo $do['status_tempat_tinggal'] ?></option>
							<?php
							  }
							?>
							</select>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Menempati Sejak </label>
							<input type="text" name="menempati_sejak" id="menempati_sejak" class="form-control datepicker" placeholder="Menempati Sejak" >
						  </div>
						</div>
					  </div>
					  <div class="row">
						<div class="col-md-3">
						  <div class="form-group">
							<label>Alamat Sesuai Domisili</label>
							<select name="alamat_sesuai" id="alamat_sesuai" class="form-control">
							  <option value="">Select Alamat Sesuai Domisili</option>
							  <option value="Y">Ya</option>
							  <option value="N">Tidak</option>
							</select>
						  </div>
						</div>
						<div class="col-md-6">
						  <div class="form-group">
							<label>Alamat Domisili <font color="red">*</font></label>
							<input type="text" name="alamat_domisili" id="alamat_domisili" class="form-control" placeholder="Alamat" required>
						  </div>
						</div><div class="col-md-3">
						  <div class="form-group">
							<label>Propinsi Domisili <font color="red">*</font></label>
							<select name="propinsi2" id="propinsi2" class="form-control prov2" onchange="get_kota2()" required>
							  <option value="">Select Propinsi</option>
							<?php
							  $get_pro = mysql_query("SELECT * FROM provinsi order by provinsiId");
							  while ($do = mysql_fetch_array($get_pro)) {
							?>
							  <option value="<?php echo $do['provinsiId'] ?>"><?php echo $do['provinsiNama'] ?></option>
							<?php
							  }
							?>
							</select>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Kota Domisili <font color="red">*</font></label>
							<select name="kota2" id="kota2" class="form-control kota2" required>
							  <option value="">Select Kota</option>
							</select>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Kecamatan Domisili <font color="red">*</font></label>
							<select name="kecamatan2" id="kecamatan2" class="form-control keca2" required>
							  <option value="">Select Kecamatan</option>
							</select>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Kelurahan Domisili <font color="red">*</font></label>
							<select name="kelurahan2" id="kelurahan2" class="form-control desa2" required>
							  <option value="">Select Kelurahan</option>
							</select>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Kode Pos Domisili <font color="red">*</font></label>
							<input type="text" name="kode_pos2" id="kode_pos2" class="form-control kode_pos2" maxlength="5" placeholder="Kode Pos" required>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Alamat Surat Menyurat <font color="red">*</font></label>
							<select name="alamat_surat_menyurat" id="alamat_surat_menyurat" class="form-control" required>
							  <option value="">Select Alamat Surat Menyurat</option>
							<?php
							  $get_pro = mysql_query("SELECT * FROM master_alamat_surat_menyurat WHERE ISNULL(delete_by) order by id");
							  while ($do = mysql_fetch_array($get_pro)) {
							?>
							  <option value="<?php echo $do['id'] ?>"><?php echo $do['alamat_surat_menyurat'] ?></option>
							<?php
							  }
							?>
							</select>
						  </div>
						</div>
					  </div>
					  <div class="row">
						<div class="col-md-12">
						  <button type="submit" class="btn btn-default" onclick=""><i class="fa fa-save"></i> SIMPAN</button>
						  <button type="reset" class="btn btn-default"><i class="fa fa-refresh"></i> CLEAR</button>
						</div>
					  </div>
					</form>
					</div>
				  </div>
				  
				  <div role="tabpanel" class="tab-pane" id="tab-coorporate">
					<form method="POST" onSubmit="return save_cif_coorporate()" >
					  <div class="row">
						<div class="col-md-12">
						<i>NASABAH KORPORASI</i>
						<div style="border:1px solid black;margin-bottom:10px;"></div>
						</div>
					  </div>
					  <div class="row">
						<div class="col-md-3">
						  <div class="form-group">
							<label>Nomor Korporasi <font color="red">*</font></label>
							<input type="text" name="no_cor" id="no_cor" class="form-control" placeholder="Nomor Korporasi" value="<?php echo $cor_number;?>" readonly required>
						  </div>
						</div>
						<div class="col-md-6">
						  <div class="form-group">
							<label>Nama Korporasi/Perusahaan <font color="red">*</font></label>
							<input type="text" name="nama_perusahaan" id="nama_perusahaan" class="form-control" placeholder="Nama Korporasi/Perusahaan" required>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Tanggal Pendirian <font color="red">*</font></label>
							<input type="text" name="tanggal_pendirian" id="tanggal_pendirian" class="form-control datepicker" placeholder="Tanggal Pendirian" required>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>No. Telpon <font color="red">*</font></label>
							<div class="input-group"><span class="input-group-addon">021</span><input class="form-control" id="telpon_perusahaan" name="telpon_perusahaan" type="text" required/></div>
						  </div>
						</div>
						<div class="col-md-6">
						  <div class="form-group">
							<label>Alamat <font color="red">*</font></label>
							<input type="text" name="alamat_perusahaan" id="alamat_perusahaan" class="form-control" placeholder="Alamat" required>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Propinsi <font color="red">*</font></label>
							<select name="province" id="propinsi_perusahaan" class="form-control prov3" onchange="get_kota3()" required>
							  <option value="">Select Propinsi</option>
							<?php
							  $get_pro = mysql_query("SELECT * FROM master_provinsi order by provinsi_id");
							  while ($do = mysql_fetch_array($get_pro)) {
							?>
							  <option value="<?php echo $do['provinsi_id'] ?>"><?php echo $do['provinsi_nama'] ?></option>
							<?php
							  }
							?>
							</select>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Kota <font color="red">*</font></label>
							<select name="city" id="kota_perusahaan" class="form-control kota3" required>
							  <option value="">Select Kota</option>
							</select>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Kecamatan <font color="red">*</font></label>
							<select name="district" id="kecamatan_perusahaan" class="form-control keca3" required>
							  <option value="">Select Kecamatan</option>
							</select>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Kode Pos <font color="red">*</font></label>
							<input type="text" name="kode_pos_perusahaan" id="kode_pos_perusahaan" class="form-control" maxlength="5" placeholder="Kode Pos" required>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Email</label>
							<input type="text" name="email_perusahaan" id="email_perusahaan" class="form-control" placeholder="Email">
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group no_npwp_perusahaan">
							<label>No. NPWP <font color="red">*</font></label>
							<input type="text" name="no_npwp_perusahaan" id="no_npwp_perusahaan" class="form-control" placeholder="No. NPWP" required>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>No. Rekening Bank <font color="red">*</font></label>
							<input type="text" name="no_rekening_perusahaan" id="no_rekening_perusahaan" class="form-control" placeholder="Rekening Bank" required>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>SIUP <font color="red">*</font></label>
							<input type="text" name="siup_perusahaan" id="siup_perusahaan" class="form-control" placeholder="SIUP" required>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Bidang Usaha <font color="red">*</font></label>
							<input type="text" name="bidang_usaha_perusahaan" id="bidang_usaha_perusahaan" class="form-control" placeholder="Bidang Usaha" required>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>TDP <font color="red">*</font></label>
							<input type="text" name="tdp_perusahaan" id="tdp_perusahaan" class="form-control" placeholder="TDP" required>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Nama Izin Usaha <font color="red">*</font></label>
							<input type="text" name="nama_izin_usaha_perusahaan" id="nama_izin_usaha_perusahaan" class="form-control" placeholder="Nama Izin Usaha" required>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>No. Izin Usaha <font color="red">*</font></label>
							<input type="text" name="no_izin_usaha_perusahaan" id="no_izin_usaha_perusahaan" class="form-control" placeholder="No. Izin Usaha" required>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Bentuk Perusahaan <font color="red">*</font></label>
							<select name="bentuk_perusahaan" id="bentuk_perusahaan" class="form-control" required>
							  <option value="">Select Bentuk Perusahaan</option>
							<?php
							  $get_pro = mysql_query("SELECT * FROM master_bentuk_perusahaan WHERE ISNULL(delete_by) order by id");
							  while ($do = mysql_fetch_array($get_pro)) {
							?>
							  <option value="<?php echo $do['id'] ?>"><?php echo $do['bentuk_perusahaan'] ?></option>
							<?php
							  }
							?>
							</select>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Bentuk Perusahaan Lainnya </label>
							<input type="text" name="bentuk_perusahaan_lainnya" id="bentuk_perusahaan_lainnya" class="form-control" placeholder="Bentuk Perusahaan Lainnya">
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Tujuan Transaksi </label>
							<input type="text" name="tujuan_transaksi_perusahaan" id="tujuan_transaksi_perusahaan" class="form-control" placeholder="Tujuan Transaksi">
						  </div>
						</div>
					  </div>
					  <div class="row">
						<div class="col-md-12">
						  <button type="submit" class="btn btn-default" onclick=""><i class="fa fa-save"></i> SIMPAN</button>
						  <button type="reset" class="btn btn-default"><i class="fa fa-refresh"></i> CLEAR</button>
						</div>
					  </div>
					</form>
				  </div>
				  
				  </div>
				</div>
              </div>
            </div>
          </div>

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->