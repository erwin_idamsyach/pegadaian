<?php
$ord = str_replace('-', '/', $ord);
echo $ord;
$get_data = mysql_query("SELECT * FROM tb_front_desk, tb_member_individu, master_kokab, master_kecam
	WHERE
	tb_member_individu.id_member = tb_front_desk.id_member AND
	master_kecam.kecam_id = tb_member_individu.district AND
	master_kokab.kota_id = tb_member_individu.city AND
	tb_front_desk.id_order  = '$ord'");
$data = mysql_query("SELECT * FROM tb_front_desk, tb_member_individu, master_kokab, master_kecam
	WHERE
	tb_member_individu.id_member = tb_front_desk.id_member AND
	master_kecam.kecam_id = tb_member_individu.district AND
	master_kokab.kota_id = tb_member_individu.city AND
	tb_front_desk.id_order  = '$ord'");

$get = mysql_fetch_array($get_data);

$cer_pri = mysql_query("SELECT * FROM tb_service WHERE kode='CE'");
$pri_cer = mysql_fetch_array($cer_pri);

$mem_pri = mysql_query("SELECT * FROM tb_service WHERE kode='GC'");
$pri_mem = mysql_fetch_array($mem_pri);

$dim_pri = mysql_query("SELECT * FROM tb_service WHERE kode='DG'");
$pri_dim = mysql_fetch_array($dim_pri);

$date = date_create($get['input_date']);
$date = date_format($date, 'd-M-Y');

$sub = 0;
$pdf = new FPDF('P','mm','A4');
$pdf->SetAutoPageBreak(false, 0);
$pdf->AddPage();

$pdf->SetFont('arial','B',16);
$pdf->SetXY(0,18);
$pdf->Cell(0,0,'ORDER FORM',0,0,'C');

$pdf->Image('./asset/images/Logo.jpg', 150, 6, 50, 20);

$pdf->SetFont('arial','B',10);
$pdf->SetXY(147, 29);
$pdf->Cell(0, 0, 'Pegadaian Gemological Laboratory');

$pdf->SetFont('arial','',10);
$pdf->SetXY(147, 33);
$pdf->Cell(0, 0, 'Jalan Kramat Raya No. 162, Jakarta');
$pdf->SetXY(147, 37);
$pdf->Cell(0, 0, 'Indonesia');

$pdf->SetXY(7, 45);
$pdf->Cell(0, 0, 'Order No.');
$pdf->SetXY(25, 45);
$pdf->Cell(0, 0, ':');
$pdf->SetXY(28, 45);
$pdf->Cell(0, 0, $get['id_order']);

$pdf->SetXY(7, 49);
$pdf->Cell(0, 0, 'Date.');
$pdf->SetXY(25, 49);
$pdf->Cell(0, 0, ':');
$pdf->SetXY(28, 49);
$pdf->Cell(0, 0, $date);

$pdf->Rect(100, 42, 100, 25,'D');

if($get['kode'] == "A"){
$pdf->SetXY(102, 45);
$pdf->Cell(0, 0, 'Customer :');
$pdf->SetXY(102, 51);
$pdf->Cell(0, 0, $get['first_name']." ".$get['middle_name']." ".$get['last_name']);
$pdf->SetXY(102, 56);
$pdf->Cell(0, 0, $get['address']);
$pdf->SetXY(101.8, 60);
$pdf->Cell(0, 0, trim($get['nama_kecam']).", ".$get['kokab_nama']);
}else{
$pdf->SetXY(102, 45);
$pdf->Cell(0, 0, 'Customer :');
$pdf->SetXY(102, 51);
$pdf->Cell(0, 0, $get['corp_name']);
$pdf->SetXY(102, 56);
$pdf->Cell(0, 0, $get['address']);
$pdf->SetXY(101.8, 60);
$pdf->Cell(0, 0, trim($get['nama_kecam']).", ".$get['kokab_nama']);
}

$pdf->SetFont('arial','',12);
$pdf->SetXY(15, 83);
$pdf->Cell(20, 10, "No", 1, 0, 'C');
$pdf->SetXY(35, 83);
$pdf->Cell(70, 10, "Description", 1, 0, 'C');
$pdf->SetXY(105, 83);
$pdf->Cell(15, 10, "Qty", 1, 0, 'C');
$pdf->SetXY(120, 83);
$pdf->Cell(30, 10, "Unit Price", 1, 0, 'C');
$pdf->SetXY(150, 83);
$pdf->Cell(45, 10, "Amount", 1, 0, 'C');
$pdf->Ln();


$pdf->Rect(15, 93, 20, 110, 'D');
$pdf->Rect(35, 93, 70, 110, 'D');
$pdf->Rect(105, 93, 15, 110, 'D');
$pdf->Rect(120, 93, 30, 110, 'D');
$pdf->Rect(150, 93, 45, 110, 'D');

$pdf->SetFont('arial','',10);

$cer = 0;
$mem = 0;
$dim = 0;
$no = 1;
$ong = 1;
while($g = mysql_fetch_array($data)) {
	$pdf->SetX(15);
	$pdf->Cell(20, 7, $no++, 0, 0, 'C');
	$pdf->SetX(35);
	$pdf->Cell(70, 7, 'Speciment '.$ong++, 0, 0, 'L');
	$pdf->Ln();
	if($g['certificate'] != ''){
		$pdf->SetX(40);
		$pdf->Cell(40, 7, 'Certificate');
		$pdf->SetX(105);
		$pdf->Cell(15, 7, '1', 0, 0, 'C');
		$pdf->SetX(120);
		$pdf->Cell(30, 7, number_format($pri_cer['harga'],0,',','.'), 0, 0, 'R');
		$pdf->SetX(150);
		$pdf->Cell(45, 7, number_format($pri_cer['harga'],0,',','.'), 0, 0, 'R');
		$pdf->Ln();
		$sub = $sub+$pri_cer['harga'];
	}else{
		
	}

	if($g['gem_card'] != ''){
		$pdf->SetX(40);
		$pdf->Cell(40, 7, 'Gem Card');
		$pdf->SetX(105);
		$pdf->Cell(15, 7, '1', 0, 0, 'C');
		$pdf->SetX(120);
		$pdf->Cell(30, 7, number_format($pri_mem['harga'],0,',','.'), 0, 0, 'R');
		$pdf->SetX(150);
		$pdf->Cell(45, 7, number_format($pri_mem['harga'],0,',','.'), 0, 0, 'R');
		$pdf->Ln();
		$sub = $sub+$pri_mem['harga'];
	}else{
		
	}

	if($g['dia_grading'] != ''){
		$pdf->SetX(40);
		$pdf->Cell(40, 7, 'Diamond Grading');
		$pdf->SetX(105);
		$pdf->Cell(15, 7, '1', 0, 0, 'C');
		$pdf->SetX(120);
		$pdf->Cell(30, 7, number_format($pri_dim['harga'],0,',','.'), 0, 0, 'R');
		$pdf->SetX(150);
		$pdf->Cell(45, 7, number_format($pri_dim['harga'],0,',','.'), 0, 0, 'R');
		$pdf->Ln();
		$sub = $sub+$pri_dim['harga'];
	}else{
		
	}
	$pdf->Ln();
}
$ppn = $sub*10/100;
$total = $sub+$ppn;
$pdf->Rect(150, 203, 45, 15, 'D');
$pdf->SetXY(120, 208);
$pdf->Cell(0, 0, 'Sub Total');
$pdf->SetXY(150, 208);
$pdf->Cell(45, 0, number_format($sub,0,',','.'), 0, 0, 'R');

$pdf->SetXY(120, 215);
$pdf->Cell(0, 0, 'PPn');
$pdf->SetXY(150, 215);
$pdf->Cell(45, 0, number_format($ppn,0,',','.'), 0, 0, 'R');

$pdf->SetXY(120, 222);
$pdf->Cell(0, 0, 'Total');
$pdf->SetXY(150, 222);
$pdf->Cell(45, 0, number_format($total,0,',','.'), 0, 0, 'R');
$pdf->Rect(150, 218, 45, 10, 'D');
$pdf->Ln();

/*$pdf->Rect(15, 235, 180, 10, 'D');
include "terbilang.php";
$terbilang = new Terbilang($total); 

$pdf->SetXY(15, 235);
$pdf->Cell(0, 10, $terbilang, 0, 0, 'C');*/
$pdf->Output();
?>