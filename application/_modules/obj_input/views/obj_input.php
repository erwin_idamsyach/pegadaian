
  <script type="text/javascript">
    $(document).ready(function(){
			autoCompleteData("nama");
		
			$('#form-obj').trigger("reset");
			$("#form-obj").on('submit',function(e){
			e.preventDefault();
			var formatData = new FormData($(this)[0]);
				swal({
				 title: "",
				 text: "Apakah Anda akan menyimpan data?",
				 type: "warning",
				 showCancelButton: true,
				 confirmButtonColor: "#DD6B55",
				 confirmButtonText: "Ya",
				 cancelButtonText: "Tidak",
				 closeOnConfirm: false }, function(){
					$.ajax({
						type : 'POST',
						url : '<?php echo base_url()?>obj_input/add_obj',
						data : formatData,
						async: false,
						cache: false,
						contentType: false,
						processData: false,
						success:function(html){
							var data = eval ("(" + html + ")");
							if(data.success){
								swal({
								 title: "Berhasil Menyimpan data!",
								 text: "Klik tombol di bawah.",
								 type: "success",
								 showCancelButton: false,
								 confirmButtonColor: "#257DB6",
								 confirmButtonText: "Ok!",
								 closeOnConfirm: false
								},
								function(){
									swal({
									 title: "",
									 text: "Tambah data dengan nomor FPJS yang sama?",
									 type: "warning",
									 showCancelButton: true,
									 confirmButtonColor: "#DD6B55",
									 confirmButtonText: "Tidak",
									 cancelButtonText: "Ya",
									 closeOnConfirm: false }, 
									 function(value){
										if(value){
											location.href = base_url+"obj_input";
										}else{
											$("#obj").val('224');
											chos();
											$(".dropify-clear").trigger('click');
											$("#weight").val("");
											$("#length").val("");
											$("#width").val("");
											$("#height").val("");
											$("#bentuk").val("");
											$("#comment").val("");
											$("#order_sama").val(data.no_order);
											$("#obj").val("");
											$("input[type=checkbox]").each(function() 
											{ 
												this.checked = false; 
											}); 
											$("input[type=radio]").each(function() 
											{ 
												this.checked = false; 
											}); 
											$("input[type=file]").val("");
										}										
									});
								});
							}else{
								swal({
								 title: data.msg,
								 text: "Klik tombol di bawah.",
								 type: "warning",
								 showCancelButton: false,
								 confirmButtonColor: "#257DB6",
								 confirmButtonText: "Ok!",
								 closeOnConfirm: false
								});
							}
						}
					});
				});
			});
      });
  </script>
  
<script type="text/javascript">
        jQuery(function($){
		   $.mask.definitions['d'] = '[0-9.]';
			
           $("#weight").mask("9?ddd.dd",{placeholder:"____.00"});
           $("#length").mask("9?ddd.dd",{placeholder:"____.00"});
           $("#width").mask("9?ddd.dd",{placeholder:"____.00"});
           $("#height").mask("9?ddd.dd",{placeholder:"____.00"});

        });
		
    function add_obj() {
      var no_trans = $(".no_trans").val();
      var id_mem  = $('#id_mem').val();
      var weight  = $('#weight').val();
      var length  = $('#length').val();
      var width   = $('#width').val();
      var height  = $('#height').val();
      var color   = $("input[name=spe_col]:checked").val();
      var grading = $(".class3:checked").val();
      var certifi = $(".class2:checked").val();
      var gem_car = $(".class1:checked").val();
      var gambar  = $('#gambarout').val();
        $.ajax({
          type : "POST",
          url  : base_url+"obj_input/add_obj",
          data : {
            'no_trans' : no_trans,
            'id_mem'   : id_mem,
            'weight'   : weight,
            'length'   : length,
            'width'    : width,
            'height'   : height,
            'color'    : color,
            'grading'  : grading,
            'gem_car'  : gem_car,
            'certifi'  : certifi,
            'gambar'   : gambar
          },
          success:function(html){
            window.location.reload();
            $("#id_mem").val(id_mem);
          }
        });
      return false;
    }
        function setFinish(){
          $('html, body').animate({
            scrollTop: $("#cart").offset().top
          }, 900);
        }
        function cekCIF(){
          var kode = $(".cif").val();
          $.ajax({
            type : "POST",
            url : base_url+"obj_input/cekCif/"+kode,
            data: "kode="+kode,
            dataType : "json",
            success:function(response){
              if(response['status'] == "SUCCESS"){
                $(".nama").val(response['nama']);
                $(".alamat").val(response['alamat']);
                $(".telp").val(response['phone']);
                $(".form-cif").removeClass("has-error");
                $(".form-cif").addClass("has-success");
              }else{
                $(".form-cif").removeClass("has-success");
                $(".form-cif").addClass("has-error");
              }
            }
          });
        }
		
	/*function cekCIFName(){
        var kode = $("#nama").val();
		kode = kode.replace(" ", "-");
		kode = kode.replace(" ", "-");
          $.ajax({
            type : "POST",
            url : base_url+"obj_input/cekCifName/"+kode,
            data: "kode="+kode,
            dataType : "json",
            success:function(response){
              if(response['status'] == "SUCCESS"){
                $("#id_mem").val(response['id_member']);
                $(".alamat").val(response['alamat']);
                $(".telp").val(response['phone']);
                $(".form-cif").removeClass("has-error");
                $(".form-cif").addClass("has-success");
              }else{
                $(".form-cif").removeClass("has-success");
                $(".form-cif").addClass("has-error");
              }
            }
          });
    }*/
      </script>
      <?php
      $ix = $no_fpjs;
      $get_order  = $this->db->query("SELECT * FROM step WHERE id_order='$ix'");
      $numing = $get_order->num_rows();
      ?>
      
      <!-- Modal -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" style="width:400px;">
          <div class="modal-content">
            <div class="modal-header" style="background-color:#f8f8f8">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h6 class="modal-title" id="myModalLabel"><b>VIEW OBJECT</b></h6>
            </div>
            <div class="modal-body">
              <div class="lol">
                
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>&nbsp;</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-user"></i> Home</a></li>
            <li>Reg. BPS</li>
            <li class="active">Add BPS</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
        <div class="index">
          <div class="box">
            <div class="box-header">
              <b>ADD <?php echo $ix ?></b>
              <div style="border:1px solid black;margin-bottom:-10px;"></div>
                <div class="box-tools pull-right">
                  <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
            <i>DATA NASABAH</i>
              <div style="border:1px solid black;margin-bottom:10px;"></div>
          <form method="post" name="form-obj" id="form-obj" enctype="multipart/form-data">
            <div class="row">
              <div class="col-md-3 col-sm-6">
                <div class="form-group has-feedback form-cif">
                  <label>No. CIF</label>
                  <div class="input-group">
                    <span class="input-group-addon">CIF</span>
                    <input type="hidden" name="no_trans" value="<?php echo $no_trans; ?>">
                    <input type="hidden" name="no_order" value="<?php echo $ix; ?>">
                    <input type="text" name="cif" class="form-control cif" id="id_mem" onkeyup="cekCIF()" placeholder="No. CIF" required>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6">
                <div class="form-group">
                  <label>Nama</label>
                  <input type="text" name="nama" id="nama" class="form-control nama" placeholder="Nama" onkeyup="cekCIFName()" onclick="cekCIFName()">
                </div>
              </div>
              <div class="col-md-3 col-sm-6">
                <div class="form-group">
                  <label>Alamat</label>
                  <input type="text" name="alamat" class="form-control alamat" placeholder="Alamat">
                </div>
              </div>
              <div class="col-md-3 col-sm-6">
                <div class="form-group">
                  <label>No Telp/HP</label>
                  <input type="text" name="telp" class="form-control telp" placeholder="No. Telp/HP">
                </div>
              </div>
            </div>
            <i>DATA OBYEK</i>
              <div style="border:1px solid black;margin-bottom:10px;"></div>
                <div class="row">
                  <div class="col-md-8">
                    <div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Weight <font color="red">*</font></label>
                          <div class="input-group"><input class="form-control" id="weight" name="weight" type="text" required /><span class="input-group-addon">ct(s)</span></div>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Length / Diameter Max </label>
                          <div class="input-group"><input class="form-control" id="length" name="length" type="text"/><span class="input-group-addon">mm</span></div>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Width / Diameter Min </label>
                          <div class="input-group"><input class="form-control" id="width" name="width" type="text"/><span class="input-group-addon">mm</span></div>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Height / Depth </label>
                          <div class="input-group"><input class="form-control" id="height" name="height" type="text"/><span class="input-group-addon">mm</span></div>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Bentuk </label>
							<select name="bentuk" id="bentuk" class="form-control">
							  <option value="">Select Bentuk</option>
							<?php
							  $get_pro = mysql_query("SELECT * FROM shape order by shape");
							  while ($do = mysql_fetch_array($get_pro)) {
							?>
							  <option value="<?php echo $do['code'] ?>"><?php echo $do['shape'] ?></option>
							<?php
							  }
							?>
							</select>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Color <font color="red">*</font></label>
                          <select name="colr" id="obj" class="form-control" onchange="chos()" required>
                            <?php
                            $col = mysql_query("SELECT * FROM color_stone GROUP BY jenis_warna");
                            while ($ge = mysql_fetch_array($col)) {
                            ?>
                            <?php
                            if($ge['jenis_warna'] == "Grey"){
                              ?>
                            <option value="<?php echo $ge['code'] ?>" selected style="text-transform: capitalize;"><?php echo $ge['jenis_warna'] ?></option>  
                            <?php
                            }else{
                            ?>
                            <option value="<?php echo $ge['code'] ?>" style="text-transform: capitalize;"><?php echo $ge['jenis_warna'] ?></option>
                            <?php
                              }
                            }
                            ?>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>Specific Color </label>
                          <div style="min-height: 100px;">
                            <div class="btn-group" id="col-area" data-toggle="buttons">
                              <?php
                              $sql = mysql_query("SELECT * FROM color_stone WHERE jenis_warna='Grey'");
                              while ($do =mysql_fetch_array($sql)) {
                              ?>
                                <div class="btn btn-default" style="white-space:normal; background-color: <?php echo $do['rgb_code'] ?>; width: 110px; height: 110px;">
                                <input type="radio" class="coll" name="spe_col" id="specol" autocomplete="off" value="<?php echo $do['code'] ?>">
                                <span class="fa fa-check fa-1x"></span>
                                <center>
                                  <font style="mix-blend-mode: difference"><?php echo $do['code'] ?></font><br>
                                  <font style="mix-blend-mode: difference"><?php echo $do['color'] ?></font>
                                </center>
								</div>
                              <?php
                              }
                              ?>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-12">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                            <label>Image (Maximal size 300 Kb)</label>
                            <input type="file" class="drop form-control" name="gambar" id="gambar">
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-inline">
                          <label>Requested Service <font color="red">*</font></label>
                          <div class="form-group has-success">
                          <?php
                            $qe = $this->db->query("SELECT * FROM tb_service where kode!='TE' AND status='Y'");
                            $no = 1;
                            foreach ($qe->result_array() as $y) {
								if($y['nama']=="BRIEF REPORT NATURAL"){
									$y['nama'] = "BRIEF REPORT";
								}
                          ?>
                            <label class="checkbox">
                              <input type="checkbox" class="class<?php echo $no ?>" id="class<?php echo $no ?>" name="<?php echo $y['kode']?>" value="<?php echo $y['nama'] ?>" /> <?php echo $y['nama'] ?>
                            </label>&nbsp;
                          <?php
                            $no++;
                            }
                          ?>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Comment(s)</label>
                      <textarea name="comment" id="comment" class="form-control"></textarea>
                    </div>
                  </div>
                  <input class="hidden" type="text" name="gambarout" id="gambarout">
                  <iframe class="hidden" name="iframeUploadImg" id="iframeUploadImg"></iframe>
                  <div class="col-md-12" style="margin-top:20px;">
                    <button type="submit" class="btn btn-default"><i class="fa fa-save"></i> SIMPAN</button>
                    <button type="reset" class="btn btn-default"><i class="fa fa-refresh"></i> CLEAR</button>
					<input class="hidden" type="text" name="order_sama" id="order_sama" value="">
                  </div>
                </div>
              </form>
                    </div>
                  <?php
                    if($numing >= 1){
                      ?>
                    <div class="pull-right">
                      <button type="submit" class="btn btn-primary" onclick="setFinish()"><i class="fa fa-check"></i> FINISH</button>
                    </div>
                      <?php
                    }
                    ?>
                </div>
            </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->