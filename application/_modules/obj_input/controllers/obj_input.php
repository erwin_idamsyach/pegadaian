<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class obj_input extends CI_Controller{	
    function obj_input(){
        parent::__construct();
		
		if(!isLogin()){
			goLogin();
		}
    }
    
    function index($text=NULL){
        $get_transaksi = $this->db->query("SELECT no_transaksi FROM tb_front_desk WHERE store='".sessionValue('kode_store')."' ORDER BY no_transaksi DESC limit 0,1");
        foreach ($get_transaksi->result() as $trans) {
            $no_trans = $trans->no_transaksi;
        }
        $month = date('m');
        switch ($month) {
            case '01':
                # code...
                $m = "I";
                break;
            case '02':
                # code...
                $m = "II";
                break;
            case '03':
                # code...
                $m = "III";
                break;
            case '04':
                # code...
                $m = "IV";
                break;
            case '05':
                # code...
                $m = "V";
                break;
            case '06':
                # code...
                $m = "VI";
                break;
            case '07':
                # code...
                $m = "VII";
                break;
            case '08':
                # code...
                $m = "VIII";
                break;
            case '09':
                # code...
                $m = "IX";
                break;
            case '10':
                # code...
                $m = "X";
                break;
            case '11':
                # code...
                $m = "XI";
                break;
            default:
                # code...
                $m = "XII";
                break;
        }
        $year  = date('Y');
        $no_trans = $no_trans+1;
        if($no_trans < 10){
            $order = "BPS/"."000".$no_trans."/".$m."/".$year."/".sessionValue('kode_unit');
        }else if($no_trans < 100){
            $order = "BPS/"."00".$no_trans."/".$m."/".$year."/".sessionValue('kode_unit');
        }else if($no_trans < 1000){
            $order = "BPS/"."0".$no_trans."/".$m."/".$year."/".sessionValue('kode_unit');
        }else{
            $order = "BPS/".$no_trans."/".$m."/".$year."/".sessionValue('kode_unit');
        }
		
		$data['no_fpjs'] = $order;
		$data['no_trans'] = $no_trans;
		
        $data["filelist"] = "obj_input/obj_input";
        $data["title"] = "Add Formulir Permintaan Jasa Sertifikasi (FPJS)";
        $data["title_menu"] = "Front";
        $data["menu"] = "input_object";
		
        getHTMLWeb($data);
    }
    public function unset_data(){
        $this->session->unset_userdata('id_cif');
        $this->session->unset_userdata('order');
        $this->session->unset_userdata('no_trans');
        redirect(site_url('obj_input'));
    }
    public function color()
    {
        $main = $this->input->post('main');
        $this->load->view('get_color', array('main'=>$main));
    }

    public function edit_obj($main)
    {
        $this->load->view('edit', array('main'=>$main));
    }

    public function delete_order($id)
    {
        $user = sessionValue('username');
        date_default_timezone_set('Asia/Jakarta');
        $now = date('Y-m-d');
        $data = array(
            'delete_by'     => $user,
            'delete_date'   => $now
        );
        $this->db->where('id_order', $id);
        $this->db->update('tb_front_desk', $data);

        $this->db->where('id_order', $id);
        $this->db->update('step', $data);
    }

    public function order_print($ord)
    {
        $this->load->library('fpdf');

        $dat = array(
            'order'  => 'beda',
        );

        $id = $this->session->userdata('order');
        $this->db->where('id_order', $ord);
        $this->db->update('step', $dat);
        $this->session->unset_userdata('order');

        $this->load->view('print-order', array('ord'=>$ord));
    }

    public function invoice_print($inv)
    {
        $this->load->library('fpdf');

        $dat = array(
            'pay'  => 'sudah',
        );

        $id = $this->session->userdata('order');
        $this->db->where('id_order', $inv);
        $this->db->update('step', $dat);
        $this->session->unset_userdata('order');

        $this->load->view('print-2', array('inv'=>$inv));
    }

    public function check_print($ord)
    {
        $que = $this->db->query("SELECT * FROM tb_front_desk where id_order='$ord'");
        foreach ($que->result_array() as $mee) {
        }
        if($que->num_rows() == 0){
            echo "OJO PRINT";
        }else{
            echo "GO PRINT";
        }
    }

    public function check_invo($inv)
    {
        $que = $this->db->query("SELECT * FROM tb_front_desk where id_order='$inv'");
        foreach ($que->result_array() as $mee) {
        }
        if($que->num_rows() == 0){
            echo "OJO PRINT";
        }else{
            echo "GO PRINT";
        }
    }

    function delete_obj($id)
    {
        $user = sessionValue('username');
        date_default_timezone_set('Asia/Jakarta');
        $now  = date('Y-m-d');
        $data = array(
            'delete_by'     => $user,
            'delete_date'   => $now
        );
        $this->db->where('id_object', $id);
        $this->db->update('tb_front_desk', $data);

        $this->db->where('id_object', $id);
        $this->db->update('step', $data);

        redirect('obj_input');
    }

    function edi_obj($id)
    {
        $id_mem  = $this->input->post('id_mem');
        $weight  = $this->input->post('weight');
        $length  = $this->input->post('length');
        $width   = $this->input->post('width');
        $height  = $this->input->post('height');
        $color   = $this->input->post('color');
        $gambar  = $this->input->post('gambar');
        $user    = sessionValue('username');
        date_default_timezone_set('Asia/Jakarta');
        $now = date('Y-m-d H:i:s');

        if($gambar == "" && $color == ""){
            $data = array(
                'id_member'     => $id_mem,
                'obj_weight'    => $weight,
                'obj_length'    => $length,
                'obj_width'     => $width,
                'obj_height'    => $height,
                'update_by'     => $user,
                'update_date'   => $now
            );
        }else if($gambar == ""){
            $data = array(
                'id_member'     => $id_mem,
                'obj_weight'    => $weight,
                'obj_length'    => $length,
                'obj_width'     => $width,
                'obj_height'    => $height,
                'obj_color'     => $color,
                'update_by'     => $user,
                'update_date'   => $now
            );
        }else if($color == ""){
            $data = array(
                'id_member'     => $id_mem,
                'obj_weight'    => $weight,
                'obj_length'    => $length,
                'obj_width'     => $width,
                'obj_height'    => $height,
                'obj_image'     => $gambar,
                'update_by'     => $user,
                'update_date'   => $now
            );
        }else if($color != "" && $gambar != ""){
            $data = array(
                'id_member'     => $id_mem,
                'obj_weight'    => $weight,
                'obj_length'    => $length,
                'obj_width'     => $width,
                'obj_height'    => $height,
                'obj_image'     => $gambar,
                'obj_color'     => $color,
                'update_by'     => $user,
                'update_date'   => $now
            );
        }
        $this->db->where('id_object', $id);
        $this->db->update('tb_front_desk', $data);
    }

    public function add_obj()
    {
        $this->load->library('Barcode39');
        $que = $this->db->query("SELECT * FROM tb_front_desk ORDER BY id_object DESC LIMIT 0, 1");
        foreach ($que->result_array() as $mee) {
        }
        if($que->num_rows() == 0){
        $goo = "O00000";
        }else{
        $goo = $mee['id_object'];
        }

        $goo = substr($goo, 1);
        $goo = $goo+1;
        if($goo < 10){
          $goo = "O0000".$goo;
        }else if($goo < 100){
          $goo = "O000".$goo;
        }else if($goo < 1000){
          $goo = "O00".$goo;
        }else if($goo < 10000){
          $goo = "O0".$goo;
        }else{
          $goo = "O".$goo;
        }
		
		// Order
		$order_sama = $this->input->post('order_sama');
		if($order_sama==""){
			$get_transaksi = $this->db->query("SELECT no_transaksi FROM tb_front_desk WHERE store='".sessionValue('kode_store')."' ORDER BY no_transaksi DESC limit 0,1");
			foreach ($get_transaksi->result() as $trans) {
				$no_trans = $trans->no_transaksi;
			}
			$month = date('m');
			switch ($month) {
				case '01':
					# code...
					$m = "I";
					break;
				case '02':
					# code...
					$m = "II";
					break;
				case '03':
					# code...
					$m = "III";
					break;
				case '04':
					# code...
					$m = "IV";
					break;
				case '05':
					# code...
					$m = "V";
					break;
				case '06':
					# code...
					$m = "VI";
					break;
				case '07':
					# code...
					$m = "VII";
					break;
				case '08':
					# code...
					$m = "VIII";
					break;
				case '09':
					# code...
					$m = "IX";
					break;
				case '10':
					# code...
					$m = "X";
					break;
				case '11':
					# code...
					$m = "XI";
					break;
				default:
					# code...
					$m = "XII";
					break;
			}
			$year  = date('Y');
			$no_trans = $no_trans+1;
			if($no_trans < 10){
				$order = "BPS/"."000".$no_trans."/".$m."/".$year."/".sessionValue('kode_unit');
			}else if($no_trans < 100){
				$order = "BPS/"."00".$no_trans."/".$m."/".$year."/".sessionValue('kode_unit');
			}else if($no_trans < 1000){
				$order = "BPS/"."0".$no_trans."/".$m."/".$year."/".sessionValue('kode_unit');
			}else{
				$order = "BPS/".$no_trans."/".$m."/".$year."/".sessionValue('kode_unit');
			}
		}else{
			$order = $order_sama;
		}
		
		$success = false;
		$msg = "";
		
        if($_FILES['gambar']['name'])
        {
            //if no errors...
            if(!$_FILES['gambar']['error'])
            {
                $valid_file = true;
                //now is the time to modify the future file name and validate the file
                $new_file_name = $goo.".jpg"; //rename file
                if($_FILES['gambar']['size'] > (307200)) //can't be larger than 1 MB
                {
                    $valid_file = false;
                    //$message = 'Oops!  Your file\'s size is to large.';
					$msg = "Ukuran gambar terlalu besar.";
                }
                    
                //if the file has passed the test
                if($valid_file){
                    //move it to where we want it to be
                    $moved = move_uploaded_file($_FILES['gambar']['tmp_name'], './asset/images/'.$new_file_name);
                    if($moved){
						$success = true;
                    }else{
                        $msg = "Image can't upload";
                    }
                    //$message = 'Congratulations!  Your file was accepted.';
                }
                    $message = $_FILES['gambar']['name'];
                    //echo "<script type='text/javascript'>parent.document.getElementById('gambarout').value='".$_FILES['gambar']['name']."';</script>";
            }else{
                //set that to be the returned message
                $message = 'Error: Ooops!  Your upload triggered the following error:  '.$_FILES['gambar']['error'];
            }
        }else{
			//$msg = "Gambar harus diisi.";
			$success = true;
		}

		if($success==true && $this->input->post('DG')=="" && $this->input->post('BR')=="" && $this->input->post('FR')==""){
			$success = false;
			$msg = "Requested Service harus diisi.";
		}
		
		if($success){
			$no_trans  = $this->input->post('no_trans');
			$id_mem  = $this->input->post('cif');
			$weight  = $this->input->post('weight');
			$length  = $this->input->post('length');
			$width   = $this->input->post('width');
			$height  = $this->input->post('height');
			$color   = $this->input->post('spe_col');
			$grading = $this->input->post('DG');
			$comment = $this->input->post('comment');
			$user    = sessionValue('id');
			$kode    = sessionValue('kode_store');
			$id_orde = $order; //$this->input->post('no_order');
			if($grading == ""){
				$grax = "";
			}else{
				$grax = $this->input->post('DG');
			}
			$gem_car = $this->input->post('BR');
			if($gem_car == ""){
				$gemx = "";
			}else{
				$gemx = $this->input->post('BR');
			}
			$certifi = $this->input->post('FR');
			if($certifi == ""){
				$cerx = "";
			}else{
				$cerx = $this->input->post('FR');
			}
			
			$now = date('Y-m-d H:i:s');

			if($grading == ""){
				$gra = 0;
			}else{
				$ql = $this->db->query("SELECT * FROM tb_service where kode='DG'");
				foreach ($ql->result_array() as $la) {
				}   
				$gra = $la['harga'];
			}

			if($certifi == ""){
				$cer = 0;
			}else{
				$ql = $this->db->query("SELECT * FROM tb_service where kode='FR'");
				foreach ($ql->result_array() as $la) {
				}
				$cer = $la['harga'];
			}

			if($gem_car == ""){
				$gem = 0;
			}else{
				$ql = $this->db->query("SELECT * FROM tb_service where kode='BR'");
				foreach ($ql->result_array() as $la) {
				}
				$gem = $la['harga'];
			}   
			
			$id_mem = "CIF".$id_mem;
			$total = $gra + $cer + $gem;
			$data = array(
				'no_transaksi'  => $no_trans,
				'id_member'     => $id_mem,
				'id_order'      => $id_orde,    
				'store'         => $kode,
				'id_object'     => $goo,
				'obj_weight'    => $weight,
				'obj_length'    => $length,
				'obj_width'     => $width,
				'obj_height'    => $height,
				'obj_shape'    	=> $this->input->post('bentuk'),
				'obj_color'     => $color,
				'obj_image'     => $new_file_name,
				'certificate'   => $cerx,
				'gem_card'      => $gemx,
				'dia_grading'   => $grax,
				'price'         => $total,
				'input_date'    => $now,
				'create_by'     => $user,
				'create_date'   => $now,
				'comment'       => $comment
			);

			$dat = array(
				'id_member'     => $id_mem,
				'store'         => $kode,
				'id_order'      => $id_orde,
				'id_object'     => $goo,
				'certificate'   => $cerx,
				'gem_card'      => $gemx,
				'dia_grading'   => $grax,
				'check_dg'      => "",
				'step'          => "FRONT_DESK",
				'print'         => "Belum",
				'print_gem'     => "Belum",
				'print_cert'    => "Belum",
				'print_grading' => "Belum",
				'pay'           => "Belum",
				'create_by'     => $user,
				'create_date'   => $now
			);

			$this->db->insert('step', $dat);
			if($this->db->insert('tb_front_desk', $data)){
				$new_ord = substr($id_orde, -20, 9);
				$new_ord = str_replace("/", "-", $new_ord);
				$bc = new Barcode39($id_mem." ".$new_ord);
				$bc->barcode_bar_thick = 2; 
				$bc->draw("./asset/barcode-fpjs/".$id_mem.$new_ord.".gif");
			}
		
		}
        
		echo json_encode(array("success"=>$success, "msg"=>$msg, "no_order"=>$order));
    }

    public function generate($temp)
    {
        $this->set_barcode($temp);
    }

    private function set_barcode($code)
    {
        //load library
        $this->load->library('zend');
        //load in folder Zend
        $this->zend->load('Zend/Barcode');
        //generate barcode
        Zend_Barcode::render('code128', 'image', array('text'=>$code), array());
        $test = Zend_Barcode::draw('code128', 'image', array('text' => $code), array());
        var_dump($test);
        imagejpeg($test, './asset/images/'.$code.'.jpg', 200);

    }

    public function save_img()
    {
        if($_FILES['gambar']['name'])
        {
            //if no errors...
            if(!$_FILES['gambar']['error'])
            {
                $valid_file = true;
                //now is the time to modify the future file name and validate the file
                $new_file_name = basename($_FILES['gambar']['name']); //rename file
                if($_FILES['gambar']['size'] > (307200)) //can't be larger than 1 MB
                {
                    $valid_file = false;
                    $message = 'Oops!  Your file\'s size is to large.';
                }
                    
                //if the file has passed the test
                if($valid_file){
                    //move it to where we want it to be
                    move_uploaded_file($_FILES['gambar']['tmp_name'], base_url().'asset/images/'.$new_file_name);
                    //$message = 'Congratulations!  Your file was accepted.';
                }
                    $message = $_FILES['gambar']['name'];
                    echo "<script type='text/javascript'>parent.document.getElementById('gambarout').value='".$_FILES['gambar']['name']."';</script>";
            }else{
                //set that to be the returned message
                $message = 'Error: Ooops!  Your upload triggered the following error:  '.$_FILES['gambar']['error'];
            }
        }
        //echo $message;
    }
	
    public function cekCif(){
        $kode = $this->uri->segment(3);
        $kode = "CIF".$kode;
        $get_user = $this->db->query("SELECT * FROM tb_member WHERE id_member='$kode' AND ISNULL(delete_by)");
        if($get_user->num_rows() >= 1){
            foreach ($get_user->result() as $ge) {
                $data = array(
                    'status' => 'SUCCESS',
                    'nama' => $ge->first_name.' '.$ge->middle_name.' '.$ge->last_name,
                    'alamat' => $ge->address,
                    'phone' => $ge->phone
                    );
            }
            echo json_encode($data);
        }else{
            $data = array(
                'status' => 'FAILED'
                );
            echo json_encode($data);
        }
    }
	
	// Hadi	
    public function cekCifName(){
        $nama = $this->uri->segment(3);
        $nama = str_replace('-', ' ', $nama);
		$nama_depan = $nama;
		
		$where ="";
		if($nama_depan==NULL || $nama_depan==""){
			$where = " AND first_name = '' ";
		}else{
			$where = " AND first_name LIKE '%".$nama_depan."%' ";
		}
		
        $get_user = $this->db->query("SELECT * FROM tb_member WHERE ISNULL(delete_by) ".$where);
        if($get_user->num_rows() >= 1){
            foreach ($get_user->result() as $ge) {
                $data = array(
                    'status' => 'SUCCESS',
                    'id_member' => str_replace('CIF', '', $ge->id_member),
                    'nama' => $ge->first_name,
                    'alamat' => $ge->address,
                    'phone' => $ge->phone,
                    'sql' => "SELECT * FROM tb_member WHERE ISNULL(delete_by) ".$where
                    );
            }
            echo json_encode($data);
        }else{
            $data = array(
                'status' => 'FAILED'
                );
            echo json_encode($data);
        }
    }
}
