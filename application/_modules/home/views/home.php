	<!--Load the AJAX API-->
    <script type="text/javascript" src="<?php echo base_url();?>asset/jscript/googlechart.js"></script>
	
	<?php
	$acces = sessionValue('access_level');
	if ($acces == "Front_Desk" || $acces == "Admin") {
        $sql_member_individu = $this->db->query("SELECT id_member FROM tb_member where kode='A' AND ISNULL(delete_by)");
        $sql_member_coorporate = $this->db->query("SELECT id_member FROM tb_member where kode='B' AND ISNULL(delete_by)");  

        $sql_fpjs_individu = $this->db->query("SELECT id_member FROM tb_front_desk where ISNULL(delete_by) GROUP BY id_order");
        $sql_fpjs_coorporate = $this->db->query("SELECT id_member FROM tb_front_desk where id_member='B' AND ISNULL(delete_by) GROUP BY id_order");

		$sql_fpjt_individu = $this->db->query("SELECT id_member FROM tb_fpjt where ISNULL(delete_by) GROUP BY id_order");
        $sql_fpjt_coorporate = $this->db->query("SELECT id_member FROM tb_fpjt where id_member='B' AND (ISNULL(delete_by) OR delete_by='') GROUP BY id_order"); 
	
	?>
    <script type="text/javascript">
	  var total_member_individu = "<?php echo $sql_member_individu->num_rows();?>";
	  var total_member_cooporate = "<?php echo $sql_member_coorporate->num_rows();?>";
	  
	  var total_fpjs_individu = "<?php echo $sql_fpjs_individu->num_rows();?>";
	  var total_fpjs_cooporate = "<?php echo $sql_fpjs_coorporate->num_rows();?>";
	  
	  var total_fpjt_individu = "<?php echo $sql_fpjt_individu->num_rows();?>";
	  var total_fpjt_cooporate = "<?php echo $sql_fpjt_coorporate->num_rows();?>";
	  
      // Load the Visualization API and the corechart package.
      google.charts.load('current', {'packages':['corechart']});

      // Set a callback to run when the Google Visualization API is loaded.
      google.charts.setOnLoadCallback(drawChart);

      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {

	    // Create the data table.
        var data = google.visualization.arrayToDataTable([
			['Element', 'Data CIF', { role: 'style' }, { role: 'annotation' } ],
			['Individual', parseInt(total_member_individu), 'stroke-color: #703593; stroke-width: 4; fill-color: #C5A5CF', total_member_individu],
			['Korporasi', parseInt(total_member_cooporate), 'stroke-color: #871B47; stroke-opacity: 0.6; stroke-width: 4; fill-color: #C5A5CF; fill-opacity: 0.2', total_member_cooporate]
		  ]);

        // Set chart options
        var options = {'title':' ',
                       'width':390,
                       'height':270};

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.ColumnChart(document.getElementById('sales-chart'));
        chart.draw(data, options);
		
        // Create the data table.
        var data = google.visualization.arrayToDataTable([
			['Element', 'Data FPJS', { role: 'style' }, { role: 'annotation' } ],
			['Individual', parseInt(total_fpjs_individu), 'stroke-color: #703593; stroke-width: 4; fill-color: #C5A5CF', total_fpjs_individu],
			['Korporasi', parseInt(total_fpjs_cooporate), 'stroke-color: #871B47; stroke-opacity: 0.6; stroke-width: 4; fill-color: #C5A5CF; fill-opacity: 0.2', total_fpjs_cooporate]
		  ]);

        // Set chart options
        var options = {'title':' ',
                       'width':390,
                       'height':270};

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.ColumnChart(document.getElementById('member'));
        chart.draw(data, options);
		
        // Create the data table.
        var data = google.visualization.arrayToDataTable([
			['Element', 'Data FPJT', { role: 'style' }, { role: 'annotation' } ],
			['Individual', parseInt(total_fpjt_individu), 'stroke-color: #703593; stroke-width: 4; fill-color: #C5A5CF', total_fpjt_individu],
			['Korporasi', parseInt(total_fpjt_cooporate), 'stroke-color: #871B47; stroke-opacity: 0.6; stroke-width: 4; fill-color: #C5A5CF; fill-opacity: 0.2', total_fpjt_cooporate]
		  ]);

        // Set chart options
        var options = {'title':' ',
                       'width':390,
                       'height':270};

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.ColumnChart(document.getElementById('fpjt'));
        chart.draw(data, options);
      }
    </script>
	<?php }else if ($acces == "Lab_Desk" || $acces == "Gemologist") { 
		$sql_permintaan1 = $this->db->query("SELECT id_member FROM step where step='FRONT_DESK' AND request='FPJS' AND certificate<>'' AND status='NOT APPROVED' AND ISNULL(delete_by)");
        $sql_permintaan2 = $this->db->query("SELECT id_member FROM step where step='FRONT_DESK' AND request='FPJS' AND dia_grading<>'' AND status='NOT APPROVED' AND ISNULL(delete_by)");  
        $sql_permintaan3 = $this->db->query("SELECT id_member FROM step where step='FRONT_DESK' AND request='FPJT' AND status='NOT APPROVED' AND ISNULL(delete_by)");  

        $sql_permintaan_selesai1 = $this->db->query("SELECT id_member FROM step where step='LAB_DESK' AND request='FPJS' AND certificate<>'' AND status='NOT APPROVED' AND ISNULL(delete_by)");
        $sql_permintaan_selesai2 = $this->db->query("SELECT id_member FROM step where step='LAB_DESK' AND request='FPJS' AND dia_grading<>'' AND status='NOT APPROVED' AND ISNULL(delete_by)");  
        $sql_permintaan_selesai3 = $this->db->query("SELECT id_member FROM step where step='LAB_DESK' AND request='FPJT' AND status='NOT APPROVED' AND ISNULL(delete_by)");  

		$total_permintaan_cetak1 = $this->db->query("SELECT id_member FROM step where step='LAB_DESK' AND request='FPJS' AND certificate<>'' AND status='APPROVED' AND ISNULL(delete_by)");
        $total_permintaan_cetak2 = $this->db->query("SELECT id_member FROM step where step='LAB_DESK' AND request='FPJS' AND dia_grading<>'' AND status='APPROVED' AND ISNULL(delete_by)");  
        $total_permintaan_cetak3 = $this->db->query("SELECT id_member FROM step where step='LAB_DESK' AND request='FPJT' AND status='APPROVED' AND ISNULL(delete_by)");  
	?>
	
    <script type="text/javascript">
	  var total_permintaan1 = "<?php echo $sql_permintaan1->num_rows();?>";
	  var total_permintaan2 = "<?php echo $sql_permintaan2->num_rows();?>";
	  var total_permintaan3 = "<?php echo $sql_permintaan3->num_rows();?>";
	  
	  var total_permintaan_selesai1 = "<?php echo $sql_permintaan_selesai1->num_rows();?>";
	  var total_permintaan_selesai2 = "<?php echo $sql_permintaan_selesai2->num_rows();?>";
	  var total_permintaan_selesai3 = "<?php echo $sql_permintaan_selesai3->num_rows();?>";
	  
	  var total_permintaan_cetak1 = "<?php echo $total_permintaan_cetak1->num_rows();?>";
	  var total_permintaan_cetak2 = "<?php echo $total_permintaan_cetak2->num_rows();?>";
	  var total_permintaan_cetak3 = "<?php echo $total_permintaan_cetak3->num_rows();?>";
	  
      // Load the Visualization API and the corechart package.
      google.charts.load('current', {'packages':['corechart']});

      // Set a callback to run when the Google Visualization API is loaded.
      google.charts.setOnLoadCallback(drawChart);

      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {

	    // Create the data table.
        var data = google.visualization.arrayToDataTable([
			['Element', 'Data CIF', { role: 'style' }, { role: 'annotation' } ],
			['Sertifikat', parseInt(total_permintaan1), 'stroke-color: #703593; stroke-width: 4; fill-color: #C5A5CF', total_permintaan1],
			['Diamond Grading', parseInt(total_permintaan2), 'stroke-color: #871B47; stroke-opacity: 0.6; stroke-width: 4; fill-color: #C5A5CF; fill-opacity: 0.2', total_permintaan2],
			['Taksiran', parseInt(total_permintaan3), 'stroke-color: #703593; stroke-width: 4; fill-color: #C5A5CF', total_permintaan3]
		  ]);

        // Set chart options
        var options = {'title':' ',
                       'width':390,
                       'height':270};

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.ColumnChart(document.getElementById('sales-chart'));
        chart.draw(data, options);
		
        // Create the data table.
        var data = google.visualization.arrayToDataTable([
			['Element', 'Data FPJS', { role: 'style' }, { role: 'annotation' } ],
			['Sertifikat', parseInt(total_permintaan_selesai1), 'stroke-color: #703593; stroke-width: 4; fill-color: #C5A5CF', total_permintaan_selesai1],
			['Diamond Grading', parseInt(total_permintaan_selesai2), 'stroke-color: #871B47; stroke-opacity: 0.6; stroke-width: 4; fill-color: #C5A5CF; fill-opacity: 0.2', total_permintaan_selesai2],
			['Taksiran', parseInt(total_permintaan_selesai3), 'stroke-color: #703593; stroke-width: 4; fill-color: #C5A5CF', total_permintaan_selesai3]
		  ]);

        // Set chart options
        var options = {'title':' ',
                       'width':390,
                       'height':270};

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.ColumnChart(document.getElementById('member'));
        chart.draw(data, options);
		
        // Create the data table.
        var data = google.visualization.arrayToDataTable([
			['Element', 'Data FPJT', { role: 'style' }, { role: 'annotation' } ],
			['Sertifikat', parseInt(total_permintaan_cetak1), 'stroke-color: #703593; stroke-width: 4; fill-color: #C5A5CF', total_permintaan_cetak1],
			['Diamond Grading', parseInt(total_permintaan_cetak2), 'stroke-color: #871B47; stroke-opacity: 0.6; stroke-width: 4; fill-color: #C5A5CF; fill-opacity: 0.2', total_permintaan_cetak2],
			['Taksiran', parseInt(total_permintaan_cetak3), 'stroke-color: #703593; stroke-width: 4; fill-color: #C5A5CF', total_permintaan_cetak3]
		  ]);

        // Set chart options
        var options = {'title':' ',
                       'width':390,
                       'height':270};

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.ColumnChart(document.getElementById('fpjt'));
        chart.draw(data, options);
      }
    </script>
	<?php } ?>

	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
        </section>

        <!-- Main content -->
        <section class="content">
		</section>

      </div><!-- /.content-wrapper -->