<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class home extends CI_Controller{
    function home(){
        parent::__construct();
		
		if(!isLogin()){
			goLogin();
		}
    }
    
    function index($text=NULL){
        $data["filelist"] = "home/home";
        $data["title"] = "HOME";
        $data["title_menu"] = "Front";
        $data["menu"] = "home";
		
        getHTMLWeb($data);
    }
}
