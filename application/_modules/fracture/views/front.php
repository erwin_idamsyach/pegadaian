<!-- front.php, Create By : Erwin Idamsyach Putra -->
<!-- Hadi -->
<script type="text/javascript">
    jQuery(function($){
        $('#example1').DataTable();
    });
</script>
	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>&nbsp;</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
            <li><?php echo $title;?></li>
            <li class="active">Fracture</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
        	<?php
        	if(!isset($id_fracture)){
        		?>
			<div class="box box-default">
				<div class="box-body">
					<b>ADD FRACTURE</b>
					<div style="border: 1px solid black; margin-bottom: 10px"></div>
					<form action="<?php echo site_url('fracture/add_fracture') ?>" method="post">
					<div class="row">
						<div class="col-md-2 col-sm-6">
							<div class="form-group">
								<label>Fracture</label>
								<input type="text" name="fracture" class="form-control" placeholder="fracture..">
							</div>
						</div>
						<div class="col-md-7 col-sm-6">
							<div class="form-group">
								<label>Description</label>
								<input type="text" name="descr" class="form-control" placeholder="description..">
							</div>
						</div>
						<div class="col-md-3">
							<div class="pull-left">
								<button type="submit" class="btn btn-primary" style="margin-top: 25px;">Submit</button>
							</div>
						</div>
					</div>
					</form>
				</div>
			</div>
			<?php
        	}else{
        		$get_cut_edit = $this->db->query("SELECT * FROM fracture WHERE id='$id_fracture'");
        		foreach ($get_cut_edit->result() as $ed) {
        			?>
        	<div class="box box-default">
				<div class="box-body">
					<b>EDIT FRACTURE</b>
					<div style="border: 1px solid black; margin-bottom: 10px"></div>
					<form action="<?php echo site_url('fracture/update_fracture') ?>" method="post">
					<div class="row">
						<div class="col-md-2 col-sm-6">
							<div class="form-group">
								<label>Fracture</label>
								<input type="hidden" name="id" value="<?php echo $ed->id ?>">
								<input type="text" name="fracture" class="form-control" placeholder="fracture.." value="<?php echo $ed->fracture ?>">
							</div>
						</div>
						<div class="col-md-7 col-sm-6">
							<div class="form-group">
								<label>Description</label>
								<input type="text" name="descr" class="form-control" placeholder="description.." value="<?php echo $ed->penjelasan ?>">
							</div>
						</div>
						<div class="col-md-3">
							<div class="pull-left">
								<button type="submit" class="btn btn-primary" style="margin-top: 25px;">Submit</button>
							</div>
						</div>
					</div>
					</form>
				</div>
			</div>
        		<?php
        		}
        	}
        	?>
			<div class="box box-default">
				<div class="box-body">
					<b>DATA FRACTURE</b>
					<div style="border: 1px solid black; margin-bottom: 10px"></div>
					<table class="table table-bordered table-striped table-hover" id="example1" width="100%">
						<thead>
							<tr>
								<th class="text-center" width="10%">No</th>
								<th class="text-center" width="20%">Fracture</th>
								<th class="text-center">Description</th>
								<th class="text-center" width="10%">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$no = 1;
							$get_store = $this->db->query("SELECT * FROM fracture WHERE delete_by =''");
							foreach ($get_store->result() as $store) {
								?>
							<tr>
								<td class="text-center"><?php echo $no++; ?></td>
								<td><?php echo $store->fracture ?></td>
								<td><?php echo $store->penjelasan ?></td>
								<td>
									<div class="pull-right">
										<a href="<?php echo site_url('fracture/edit_fracture').'/'.$store->id ?>" class="btn btn-warning" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;&nbsp;

										<a href="<?php echo site_url('fracture/delete_fracture').'/'.$store->id ?>" class="btn btn-danger" data-toggle="tooltip" data-placement="bottom" onclick="return confirm('Are you sure to delete this data?');" title="Delete"><i class="fa fa-trash"></i></a>
									</div>
								</td>
							</tr>
								<?php
							}
							?>
						</tbody>
					</table>
				</div>
			</div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->