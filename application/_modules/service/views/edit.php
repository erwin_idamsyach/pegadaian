<?php
	$qu = $this->db->get_where('tb_service', array('id'=>$id));
	foreach ($qu->result_array() as $y) {
	}
?>
<div class="row">
	<div class="col-md-4">
		<div class="form-group">
			<label>Code</label>
			<input type="text" name="code" value="<?php echo $y['kode'] ?>" class="code form-control">
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			<label>Name</label>
			<input type="text" name="name" value="<?php echo $y['nama'] ?>" class="name form-control">
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			<label>Price</label>
			<input type="text" name="price" value="<?php echo number_format($y['harga'],0,',','.') ?>" onKeyUp="this.value=ThausandSeperator(this.value,2);" class="price form-control">
		</div>
	</div>
	<div class="col-md-12">
		<button class="btn btn-default" onclick="save_service(<?php echo $id ?>)"><i class="fa fa-save"></i> SAVE</button>
	</div>
</div>