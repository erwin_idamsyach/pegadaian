<?php
class add_fpjt extends CI_Controller{
	function __construct(){
		parent::__construct();

		if(!isLogin()){
			goLogin();
		}
	}
	
	public function index(){
	
        $get_transaksi = $this->db->query("SELECT no_transaksi FROM tb_fpjt ORDER BY no_transaksi DESC limit 0,1");
        foreach ($get_transaksi->result() as $trans) {
            $no_trans = $trans->no_transaksi;
        }
        if($no_trans == ''){
            $no_trans = 0;
        }else{}
        $month = date('m');
        $year  = date('Y');
        switch ($month) {
        	case '01':
        		# code...
        		$m = "I";
        		break;
        	case '02':
        		# code...
        		$m = "II";
        		break;
        	case '03':
        		# code...
        		$m = "III";
        		break;
        	case '04':
        		# code...
        		$m = "IV";
        		break;
        	case '05':
        		# code...
        		$m = "V";
        		break;
        	case '06':
        		# code...
        		$m = "VI";
        		break;
        	case '07':
        		# code...
        		$m = "VII";
        		break;
        	case '08':
        		# code...
        		$m = "VIII";
        		break;
        	case '09':
        		# code...
        		$m = "IX";
        		break;
			case '10':
        		# code...
        		$m = "X";
        		break;
        	case '11':
        		# code...
        		$m = "XI";
        		break;
        	default:
        		# code...
        		$m = "XII";
        		break;
        }
        if($no_trans == ''){
        	$no_trans = 1;
        }else{
        	$no_trans = $no_trans+1;
        }
        if($no_trans < 10){
            $order = "BPT/"."000".$no_trans."/".$m."/".$year;
        }else if($no_trans < 100){
            $order = "BPT/"."00".$no_trans."/".$m."/".$year;
        }else if($no_trans < 1000){
            $order = "BPT/"."0".$no_trans."/".$m."/".$year;
        }else{
            $order = "BPT/".$no_trans."/".$m."/".$year;
        }

        // Auto Complete
		$queryCIF = $this->db->query("SELECT CONCAT_WS(' ',CONCAT_WS(' ',tb_member.first_name, tb_member.middle_name), tb_member.last_name) AS nama FROM tb_member WHERE ISNULL(delete_by)");
		$auto_name = "";
        foreach ($queryCIF->result() as $tmpCIF) {
            $data_name = $tmpCIF->nama;
			$auto_name .= $data_name.",";
        }
		
		if($auto_name!=""){
			$auto_name = substr($auto_name, 0, -1);
		} 
    
		$data['filelist'] = 'add_fpjt/front';
		$data['title'] = 'Add Form Permintaan Jasa Taksiran (FPJT)';
		$data['menu'] = 'add-fpjt';
		$data['title_menu'] = 'Front';

		$data['no_fpjt'] = $order;
		$data['no_trans'] = $no_trans;
		$data['auto_name'] = $auto_name;
		
		getHTMLWeb($data);
	}
	
	function autoComplete(){
		
		// Auto Complete
		$queryCIF = $this->db->query("SELECT tb_member.first_name AS name 
									FROM tb_member 
									WHERE ISNULL(delete_by)");
		/*$auto_name = "";
        foreach ($queryCIF->result() as $tmpCIF) {
            $data_name = $tmpCIF->name;
			$auto_name .= $data_name.",";
        }
		
		if($auto_name!=""){
			$auto_name = substr($auto_name, 0, -1);
		}*/ 
		
		echo json_encode($queryCIF->result());
	}
	
	public function add(){
        $this->load->library('Barcode39');
		$jumlah_x = $this->input->post('jumlah_x');
		$jumlah_y = $this->input->post('jumlah_y');
		
		if($jumlah_x>$jumlah_y){
			$jumlah = $jumlah_x;
		}else{
			$jumlah = $jumlah_y;
		}
		
		$success = false;
		$msg = "";
		$jenis_perhiasan = "";
		$jumlah_perhiasan = "";
		$berat_perhiasan = "";
		for($i=1;$i<=$jumlah;$i++){
			$id_mem = "CIF".$this->input->post('cif');
			if($i<=$jumlah_x){
				$data = array(
						'id_order' => $this->input->post('no_fpjt'),
						'no_transaksi' => $this->input->post('no_trans'),
						'id_member' => $id_mem,
						'jenis_perhiasan' => $this->input->post('jenis_perhiasan'.$i),
						'jumlah' => $this->input->post('jumlah'.$i),				
						'berat_perhiasan' => $this->input->post('weight'.$i),
						'keterangan' => $this->input->post('comment'.$i),
						'input_date' => date("Y-m-d H:i:s"),
						'create_date' => date("Y-m-d H:i:s"),
						'create_by' => sessionValue('id')
				);
				
				$jenis_perhiasan = $this->input->post('jenis_perhiasan'.$i);
				$jumlah_perhiasan = $this->input->post('jumlah'.$i);
				$berat_perhiasan = $this->input->post('weight'.$i);
			}else{
				$data = array(
						'id_order' => $this->input->post('no_fpjt'),
						'no_transaksi' => $this->input->post('no_trans'),
						'id_member' => $id_mem,
						'jenis_perhiasan' => $jenis_perhiasan,
						'jumlah' => $jumlah_perhiasan,				
						'berat_perhiasan' => $berat_perhiasan,
						'jenis_permata' => $this->input->post('jenis_permata'.$i),
						'jumlah_permata' => $this->input->post('jumlah_permata'.$i),	
						'warna_permata' => $this->input->post('color_permata'.$i),
						'input_date' => date("Y-m-d H:i:s"),
						'create_date' => date("Y-m-d H:i:s"),
						'create_by' => sessionValue('id')
					);
			}
			
			$success_insert = false;
			$id = "";
			if($this->db->insert('tb_fpjt', $data)){
				$success_insert = true;
				
				$new_ord = substr($this->input->post('no_fpjt'), -20, 9);
				$new_ord = str_replace("/", "-", $new_ord);
				$bc = new Barcode39($id_mem." ".$new_ord);
				$bc->barcode_bar_thick = 2; 
				$bc->draw("asset/barcode-fpjt/".$id_mem.$new_ord.".gif");
				
				$id = $this->db->insert_id();
				if($i<=$jumlah_y){
					$data2 = array(
							'jenis_permata' => $this->input->post('jenis_permata'.$i),
							'jumlah_permata' => $this->input->post('jumlah_permata'.$i),	
							'warna_permata' => $this->input->post('color_permata'.$i)
						);
					$this->db->where('id', $id);			
					$this->db->update('tb_fpjt', $data2);
				}
				
				$data_step = array(
								'id_object'     => $id,
								'id_member'     => $id_mem,
								'store'         => sessionValue('kode_store'),
								'id_order'      => $this->input->post('no_fpjt'),
								'request'      => "FPJT",
								'step'          => "FRONT_DESK",
								'print'         => "Belum",
								'print_gem'     => "Belum",
								'print_cert'    => "Belum",
								'print_grading' => "Belum",
								'pay'           => "Belum",
								'create_date' => date("Y-m-d H:i:s"),
								'create_by' => sessionValue('id')
							);
				$this->db->insert('step', $data_step);
				$success = true;
			}else{
				$msg = "Data not inserted.";
			}
		}
			
			/*if($_FILES['image']['name'])
			{
				//if no errors...
				if(!$_FILES['image']['error'])
				{
					$valid_file = true;
					//now is the time to modify the future file name and validate the file
					//$new_file_name = $_FILES['image']['name']; //rename file
					

					if($_FILES['image']['size'] > (307200)) //can't be larger than 300 KB
					{
						$valid_file = false;
						$msg = "Ukuran gambar terlalu besar.";
					}
						
					//if the file has passed the test
					if($valid_file){
						//move it to where we want it to be
						if($success_insert){	
							$new_file_name = $id.".jpg";
							
							move_uploaded_file($_FILES['image']['tmp_name'], './asset/image-fpjt/'.$new_file_name);
							//$message = 'Congratulations!  Your file was accepted.';
							
							$dataUpdate = array(
								'foto' => $new_file_name
							);
							$this->db->where('id', $id);
							if($this->db->update('tb_fpjt', $dataUpdate)){
								$success = true;
							}else{
								 $msg = "Data tidak tersimpan";
							}
						}
					}
					
				}else{
					//set that to be the returned message
					$msg = "Gambar harus diisi.";
					$success = false;
				}
			
		}else{
			//$msg = "Gambar harus diisi.";
			$success = true;
		}*/
        
		echo json_encode(array("success"=>$success, "msg"=>$msg, "jumlah_x"=>$jumlah_x, "jumlah_y"=>$jumlah_y));
        //redirect(site_url('add_fpjt'));
	}
	
	function change_color(){
        $warna = $this->input->post('warna');
		
		$sql = $this->db->query("SELECT code, color, rgb_code FROM color_stone b where code='".$warna."'");
		
		$rgb_color = "";
		$color_spek = "";
		foreach($sql->result_array() as $tmp){
			$exp = explode(';', $tmp['color']);
			if(count($exp)>0){
				$color_spek = $exp[0];
			}else{
				$color_spek = $tmp['color'];
			}
			
			$rgb_color =  $tmp['rgb_code'];
		}		
		
		echo json_encode(array("success"=>true, "rgb_color"=>$rgb_color, "color_spek"=>$color_spek));
    }
	
    public function unset_data(){
        $this->session->unset_userdata('id_mem');
        $this->session->unset_userdata('order_fpjt');
        redirect(site_url('add_fpjt'));
    }
}
?>