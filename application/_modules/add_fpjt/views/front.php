	<script type="text/javascript">
		$(document).ready(function(){		
			autoCompleteData("nama");
		
			//$("#jumlah_permata1").attr("disabled","disabled");
			//$("#jenis_permata1").attr("disabled","disabled");
			//$("#obj").attr("disabled","disabled");
			
			var wrapper1         = $("#item_perhiasan"); //Fields wrapper
			var add_button1      = $("#add_item"); //Add button ID
			var x = 1;
		   
			$(add_button1).click(function(e){ //on add input button click
				e.preventDefault();
				
				x++; //text box increment
				var item = '<div id="row_perhiasan'+x+'"><div class="col-md-12">'+
                    '<div class="row">'+
                    '  <div class="col-md-3">'+
                    '    <div class="form-group">'+
                    '      <label>Jumlah Perhiasan <font color="red">*</font></label>'+
                    '      <input class="form-control" type="text" id="jumlah'+x+'" name="jumlah'+x+'" value="1" onKeyUp="this.value=ThausandSeperator(this.value,2);" required />'+
                    '    </div>'+
                    '  </div>'+
                    '  <div class="col-md-3">'+
                    '    <div class="form-group">'+
                    '      <label>Weight <font color="red">*</font></label>'+
                    '      <div class="input-group"><input class="form-control" id="weight'+x+'" name="weight'+x+'" type="text" required /><span class="input-group-addon">gram</span></div>'+
                    '    </div>'+
                    '  </div>'+
                    '  <div class="col-md-3">'+
                    '    <div class="form-group">'+
                    '      <label>Jenis Perhiasan <font color="red">*</font></label>'+
                    '      <select name="jenis_perhiasan'+x+'" class="form-control" id="jenis_perhiasan'+x+'" required>'+
                    '        <option value="">--Jenis Perhiasan--</option>'+
                            '<?php $get_perhiasan = $this->db->query("SELECT * FROM master_jenis_permata"); foreach ($get_perhiasan->result() as $per) { ?>'+
                            '<option value="<?php echo $per->id ?>">'+'<?php echo $per->jenis ?>'+'</option>'+
                              '<?php } ?>'+
                    '      </select>'+
                    '    </div>'+
                    '  </div>'+
					'  <div class="col-md-6">'+
					'	<div class="form-group">'+
					'	  <label>Keterangan Tambahan</label>'+
					'	  <textarea name="comment'+x+'" id="comment'+x+'" class="form-control"></textarea>'+
					'	</div>'+
					'  </div>'+
					'</div>'+
                  '</div></div>';
				
				$(wrapper1).append(item); //add input box7
				$("#jumlah_x").val(x);
				
				$.mask.definitions['d'] = '[0-9.]';
				$("#weight"+x).mask("9?ddd.dd",{placeholder:"____.00"});
				
			});
		   
			$(wrapper1).on("click","#remove_item", function(e){ //user click on remove text
				e.preventDefault();
				$('#row_perhiasan'+x).remove(); 
				if(x>1){
					x--;
					$("#jumlah_x").val(x);
				}
			});
			
			
			
			var wrapper2         = $("#item_permata"); //Fields wrapper
			var add_button2      = $("#add_item_permata"); //Add button ID
			var y = 1;
		   
			$(add_button2).click(function(e){ //on add input button click
				e.preventDefault();
				
				y++; //text box increment
				var item2 = '<div id="row_permata'+y+'"><div class="col-md-12">'+
					'<div class="row">'+
                    '  <div class="col-md-3">'+
                    '    <div class="form-group">'+
                    '      <label>Jenis Permata </label>'+
                    '      <select name="jenis_permata'+y+'" class="form-control" id="jenis_permata'+y+'" >'+
                    '        <option value="">--Jenis Permata--</option>'+
                            '<?php $get_perhiasan = $this->db->query("SELECT * FROM master_permata"); foreach ($get_perhiasan->result() as $per) { ?>'+
                            '<option value="<?php echo $per->id ?>">'+'<?php echo $per->permata ?>'+'</option>'+
                              '<?php } ?>'+
                    '      </select>'+
                    '    </div>'+
                    '  </div>'+
                    '  <div class="col-md-3">'+
                    '    <div class="form-group">'+
                    '      <label>Jumlah Permata </label>'+
                    '      <input type="text" name="jumlah_permata'+y+'" class="form-control" id="jumlah_permata'+y+'" onKeyUp="this.value=ThausandSeperator(this.value,2);" >'+
                    '    </div>'+
                    '  </div>'+
					'  <div class="col-md-3">'+
					'	<input type="hidden" name="color_permata'+y+'" id="color_permata'+y+'" value="">'+
					'	<div class="form-group" style="border:2px solid black;padding:10px;border-radius:10px;">'+
					'	  <div id="div_color'+y+'" style="height:45px;padding-top:5px;border-radius:10px;">'+
					'	  <center>'+
					'		<div style="mix-blend-mode: difference;" id="div_color_text'+y+'">'+
					'		  &nbsp;'+							  
					'		</div>'+							
					'	  </center>'+
					'	  </div>'+
					'	</div>'+
					'  </div>'+
					'  <div class="col-md-3">'+
					'	<div class="form-group">'+
                    '      <br />'+
                    '      <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modalFPJT" onclick="changeColorId('+y+')">Pilih Warna</button>'+
                    '    </div>'+
					'  </div>'+
                    '</div>'+
				  '</div></div>';
				
				$(wrapper2).append(item2);
				
				$("#jumlah_y").val(y); 
			});
		   
			$(wrapper2).on("click","#remove_item_permata", function(e){ //user click on remove text
				e.preventDefault();
				$('#row_permata'+y).remove(); 
				if(y>1){
					y--;
					
					$("#jumlah_y").val(y);
				}
			});
			
			$('#form-fpjt').trigger("reset");
			$("#form-fpjt").on('submit',function(e){
			e.preventDefault();
			var formatData = new FormData($(this)[0]);
				swal({
				 title: "",
				 text: "Apakah Anda akan menyimpan data?",
				 type: "warning",
				 showCancelButton: true,
				 confirmButtonColor: "#DD6B55",
				 confirmButtonText: "Ya",
				 cancelButtonText: "Tidak",
				 closeOnConfirm: false }, function(){
					
					$.ajax({
						type : 'POST',
						url : '<?php echo base_url()?>add_fpjt/add',
						data : formatData,
						async: false,
						cache: false,
						contentType: false,
						processData: false,
						success:function(html){
							//window.location.reload();
							var data = eval ("(" + html + ")");
							if(data.success){
								swal({
								 title: "Berhasil Menyimpan data!",
								 text: "Klik tombol di bawah.",
								 type: "success",
								 showCancelButton: false,
								 confirmButtonColor: "#257DB6",
								 confirmButtonText: "Ok!",
								 closeOnConfirm: false
								},
								function(){
									location.href = base_url+"add_fpjt";
									/*swal({
									 title: "",
									 text: "Tambah data dengan nomor FPJT yang sama?",
									 type: "warning",
									 showCancelButton: true,
									 confirmButtonColor: "#DD6B55",
									 confirmButtonText: "Tidak",
									 cancelButtonText: "Ya",
									 closeOnConfirm: false }, function(value){
										if(value){
											location.href = base_url+"add_fpjt";
										}else{
											$("#jumlah").val("1");
											$("#weight").val("");
											$("#jenis_perhiasan").val("");
											$("#dengan_permata").val("");
											$("#jumlah_permata").val("");
											$("#comment").val("");
											//$("#obj").val("");
											$("input:radio").removeAttr("checked");
											//setDisable(1);
										}
									});*/
								});
							}else{
								swal({
								 title: data.msg,
								 text: "Klik tombol di bawah.",
								 type: "warning",
								 showCancelButton: false,
								 confirmButtonColor: "#257DB6",
								 confirmButtonText: "Ok!",
								 closeOnConfirm: false
								});
							}
						}
					});
				});
			});
			
		});
		jQuery(function($){
			$.mask.definitions['d'] = '[0-9.]';
			
			$("#weight1").mask("9?ddd.dd",{placeholder:"____.00"});
        });
    
        function setFinish(){
          $('html, body').animate({
            scrollTop: $("#cart").offset().top
          }, 900);
        }
		
        function cekCIF(){
          var kode = $(".cif").val();
          $.ajax({
            type : "POST",
            url : base_url+"obj_input/cekCif/"+kode,
            data: "kode="+kode,
            dataType : "json",
            success:function(response){ 
              if(response['status'] == "SUCCESS"){
                $(".nama").val(response['nama']);
                $(".alamat").val(response['alamat']);
                $(".telp").val(response['phone']);
                $(".form-cif").removeClass("has-error");
                $(".form-cif").addClass("has-success");
              }else{
                $(".form-cif").removeClass("has-success");
                $(".form-cif").addClass("has-error");
              }
            }
          });
        }
		
		// Hadi
		function changeColorId(id_element){
			$("#select_warna").val(id_element);
		}
		
		function setColor(){
		  var color = $(".coll:checked").val();
		  var id_element = $("#select_warna").val();
		  
		  $.ajax({
			type : "POST",
			url : base_url+"add_fpjt/change_color",
			data : {
			  "warna" : color
			},
			success:function(response){ 
				var data = eval ("(" + response + ")");
				if(data.success){
					$("#modalFPJT").modal('hide');
					$("#div_color"+id_element).css("background-color","#"+data.rgb_color);
					$("#div_color_text"+id_element).html(data.color_spek);
					$("#color_permata"+id_element).val(color);
					
				}
				
			}
		  });
		}
		// End Hadi
		
        function setDisable(i){
          if($("#dengan_permata"+i).val()=="Y") {
             $("#jumlah_permata"+i).removeAttr("disabled");
             $("#jenis_permata"+i).removeAttr("disabled");
             //$("#obj").removeAttr("disabled");
             $(".coll").removeAttr("disabled");
          } else {
             $("#jumlah_permata"+i).attr("disabled","disabled");
             $("#jenis_permata"+i).attr("disabled","disabled");
             //$("#obj").attr("disabled","disabled");
             $(".coll").attr("disabled","true");
          }
        }
    </script>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>&nbsp;</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-user"></i> Home</a></li>
            <li>Reg. BPT</li>
            <li class="active">Add BPT</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
        <div class="index">
          <div class="box">
            <div class="box-header">
              <b>ADD : <?php echo $no_fpjt; ?></b>
			  
              <div style="border:1px solid black;margin-bottom:-10px;"></div>
                <div class="box-tools pull-right">
                  <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
            <i>DATA NASABAH</i>
              <div style="border:1px solid black;margin-bottom:10px;"></div>
          <form method="post" name="form-fpjt" id="form-fpjt"  enctype="multipart/form-data">
			<input type="hidden" name="no_fpjt" id="no_fpjt" value="<?php echo $no_fpjt; ?>">
			<input type="hidden" name="no_trans" id="no_trans" value="<?php echo $no_trans; ?>">
            <div class="row">
              <div class="col-md-3 col-sm-6">
                <div class="form-group has-feedback form-cif">
                  <label>No. CIF <font color="red">*</font></label>
                  <div class="input-group">
                    <span class="input-group-addon">CIF</span>
                    <input type="text" name="cif" class="form-control cif" id="id_mem" onkeyup="cekCIF()" placeholder="No. CIF" required>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6">
                <div class="form-group">
                  <label>Nama</label>
                  <input type="text" name="nama" id="nama" class="form-control nama" placeholder="Nama" onkeyup="cekCIFName()" onclick="cekCIFName()">
                </div>
              </div>
              <div class="col-md-3 col-sm-6">
                <div class="form-group">
                  <label>Alamat</label>
                  <input type="text" name="alamat" class="form-control alamat" placeholder="Alamat">
                </div>
              </div>
              <div class="col-md-3 col-sm-6">
                <div class="form-group">
                  <label>No Telp/HP</label>
                  <input type="text" name="telp" class="form-control telp" placeholder="No. Telp/HP">
                </div>
              </div>
            </div>
            <i>DATA OBYEK</i>
              <div style="border:1px solid black;margin-bottom:10px;"></div>
                <div class="row" id="item_perhiasan">
                  <div class="col-md-12">
                    <div class="row">
					  <div class="col-md-3">
                        <div class="form-group">
                            <button type="button" class="btn btn-info" id="add_item">Add More</button>
							<button type="button" class="btn btn-info" id="remove_item">Remove</button>
                        </div>
                      </div>
					</div>
                    <div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Jumlah Perhiasan <font color="red">*</font></label>
                          <input class="form-control" type="text" id="jumlah1" name="jumlah1" value="1" onKeyUp="this.value=ThausandSeperator(this.value,2);" required />
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Weight <font color="red">*</font></label>
                          <div class="input-group"><input class="form-control" id="weight1" name="weight1" type="text" required /><span class="input-group-addon">gram</span></div>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Jenis Perhiasan <font color="red">*</font></label>
                          <select name="jenis_perhiasan1" class="form-control" id="jenis_perhiasan1" required>
                            <option value="">--Jenis Perhiasan--</option>
                            <?php
                            $get_perhiasan = $this->db->query("SELECT * FROM master_jenis_permata");
                            foreach ($get_perhiasan->result() as $per) {
                              ?>
                            <option value="<?php echo $per->id ?>"><?php echo $per->jenis ?></option>
                              <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div>
					  <div class="col-md-6">
						<div class="form-group">
						  <label>Keterangan Tambahan</label>
						  <textarea name="comment1" id="comment1" class="form-control"></textarea>
						</div>
					  </div>
					</div>
                  </div>				  
                </div>
            <i>DATA PERMATA</i>
              <div style="border:1px solid black;margin-bottom:10px;"></div>
				<div class="row" id="item_permata">
				  <div class="col-md-12">
                    <div class="row">
					  <div class="col-md-3">
                        <div class="form-group">
                            <button type="button" class="btn btn-info" id="add_item_permata">Add More</button>
							<button type="button" class="btn btn-info" id="remove_item_permata">Remove</button>
                        </div>
                      </div>
					</div>
					<div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Jenis Permata </label>
                          <select name="jenis_permata1" class="form-control" id="jenis_permata1" >
                            <option value="">--Jenis Permata--</option>
                            <?php
                            $get_perhiasan = $this->db->query("SELECT * FROM master_permata");
                            foreach ($get_perhiasan->result() as $per) {
                              ?>
                            <option value="<?php echo $per->id; ?>"><?php echo $per->permata;?></option>
                              <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Jumlah Permata </label>
                          <input type="text" name="jumlah_permata1" class="form-control" id="jumlah_permata1" onKeyUp="this.value=ThausandSeperator(this.value,2);" >
                        </div>
                      </div>
					  <div class="col-md-3">
						<input type="hidden" name="color_permata1" id="color_permata1" value="">
						<div class="form-group" style="border:2px solid black;padding:10px;border-radius:10px;">
						  <div id="div_color1" style="height:45px;padding-top:5px;border-radius:10px;">
						  <center>
							<div style="mix-blend-mode: difference;" id="div_color_text1">
							  &nbsp;							  
							</div>							
						  </center>
						  </div>
						</div>
					  </div>
					  <div class="col-md-3">
						<div class="form-group">
                          <br />
                          <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modalFPJT" onclick="changeColorId(1)">Pilih Warna</button>
                        </div>
					  </div>
					  <!--<div class="col-md-6">
						<div class="form-group">
						  <label>Keterangan Tambahan</label>
						  <textarea name="comment1" id="comment1" class="form-control"></textarea>
						</div>
					  </div>-->
                    </div>
				  </div>
				</div>
                <div class="row">
				  <div class="col-md-12" style="margin-top:20px;">
					<input type="hidden" id="select_warna" value="1">
					<input type="hidden" name="jumlah_x" id="jumlah_x" value="1">
					<input type="hidden" name="jumlah_y" id="jumlah_y" value="1">
                    <button type="submit" class="btn btn-default"><i class="fa fa-save"></i> SIMPAN</button>
                    <button type="reset" class="btn btn-default"><i class="fa fa-refresh"></i> CLEAR</button>
                  </div>
				</div>
              </form>
                    </div>
                </div>
            </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
		<div class="modal fade" id="modalFPJT">
            <div class="modal-dialog modal-md">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <div class="modal-title" id="myModalLabel">Edit Color</div>
                </div>
                <div class="modal-body">
                  <div class="row">
                      <div class="col-md-6">
                        <div class="form-group has-success">
                          <label>Color</label>
                          <select name="colr" id="obj" class="form-control" onchange="chos()" required>
                            <option>Select color..</option>
                              <?php
                                $col = mysql_query("SELECT * FROM color_stone GROUP BY jenis_warna");
                                while ($ge = mysql_fetch_array($col)) {
                              ?>
                              <?php
                                if($ge['jenis_warna'] == "Select color.."){
                              ?>
                            <option value="<?php echo $ge['code'] ?>" selected style="text-transform: capitalize;"><?php echo $ge['jenis_warna'] ?></option>  
                              <?php
                                }else{
                              ?>
                            <option value="<?php echo $ge['code'] ?>" style="text-transform: capitalize;"><?php echo $ge['jenis_warna'] ?></option>
                              <?php
                                  }
                              }
                              ?>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group has-success">
                          <label>Specific Color</label>
                          <div style="min-height: 120px;">
                            <div class="btn-group" id="col-area" data-toggle="buttons">
                              -
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <button class="btn btn-primary" onclick="setColor()">SAVE</button>
                        </div>
                      </div>
                   </div>
                  </div>
                </div>
            </div>
        </div>