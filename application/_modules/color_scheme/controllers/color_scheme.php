<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
* Controller Parameter Color
* Create By : Erwin Idamsyach Putra
* 23 April 2016
*/
class color_scheme extends CI_Controller{
	function __construct(){
		parent::__construct();

		if(!isLogin()){
			goLogin();
		}
	}
	
	public function index(){
		$data['filelist'] = 'color_scheme/front';
		$data['title'] = 'Master Lab Desk';
		$data['title_menu'] = 'Admin';
		$data['menu'] = 'color_scheme';

		getHTMLWeb($data);
	}

	public function add_color(){
		$name = $this->input->post('color_name');
		$rgb_code = $this->input->post('color');
		$group = $this->input->post('group');

		$rgb_code = substr($rgb_code, 1);

		$get_code = $this->db->query("SELECT * FROM color_stone ORDER BY code DESC LIMIT 0, 1");
		foreach ($get_code->result() as $fre) {
			$code = $fre->code;
			$code = $code+1;
		}

		$data = array(
			'code' => $code,
			'color' => $name,
			'rgb_code' => $rgb_code,
			'jenis_warna' => $group,
			'create_by' => sessionValue('nama'),
			'create_date' => date('Y-m-d')
			);
		$this->db->insert('color_stone', $data);
		redirect(site_url('color_scheme'));
	}

	public function edit_color(){
		$id_color = $this->uri->segment(3);
		$data['filelist'] = 'color_scheme/front';
		$data['title'] = 'Master Lab Desk';
		$data['title_menu'] = 'Admin';
		$data['menu'] = 'color_scheme';
		$data['id_color'] = $id_color;

		getHTMLWeb($data);
	}

	public function update_color(){
		$id = $this->input->post('id');
		$name = $this->input->post('color_name');
		$rgb_code = $this->input->post('color');
		$group = $this->input->post('group');

		$rgb_code = substr($rgb_code, 1);

		$data = array(
			'color' => $name,
			'rgb_code' => $rgb_code,
			'jenis_warna' => $group,
			'update_by' => sessionValue('nama'),
			'update_date' => date('Y-m-d')
			);
		$this->db->where('id', $id);
		$this->db->update('color_stone', $data);
		redirect(site_url('color_scheme'));
	}

	public function delete_color(){
		$id = $this->uri->segment(3);
		$data = array(
			'delete_by' => sessionValue('nama'),
			'delete_date' => date('Y-m-d')
			);
		$this->db->where('id', $id);
		$this->db->update('color_stone', $data);
		redirect(site_url('color_scheme'));
	}
}
?>