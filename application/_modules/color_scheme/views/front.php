<!-- front.php, Create By : Erwin Idamsyach Putra -->
<!-- Hadi -->
<script type="text/javascript">
    jQuery(function($){
        $('#example1').DataTable();
    });
</script>
	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>&nbsp;</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
            <li><?php echo $title;?></li>
            <li class="active">Color</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
        	<?php
        	if(!isset($id_color)){
        		?>
			<div class="box box-default">
				<div class="box-body">
					<b>ADD COLOR</b>
					<div style="border: 1px solid black; margin-bottom: 10px"></div>
					<form action="<?php echo site_url('color_scheme/add_color') ?>" method="post">
					<div class="row">
						<div class="col-md-3 col-sm-6">
							<div class="form-group">
								<label>Color Name</label>
								<input type="text" name="color_name" class="form-control" placeholder="store name..">
							</div>
						</div>
						<div class="col-md-5 col-sm-6">
							<div class="form-group">
								<label>Color</label>
								<input type="color" name="color" class="form-control" placeholder="address..">
							</div>
						</div>
						<div class="col-md-4 col-sm-6">
							<div class="form-group">
								<label>Group Color</label>
								<select name="group" class="form-control" required="">
									<option value="" selected disabled>--Select Group--</option>
									<?php
									$get_group = $this->db->query("SELECT * FROM color_stone GROUP BY jenis_warna");
									foreach ($get_group->result() as $get) {
										?>
									<option value="<?php echo $get->jenis_warna ?>"><?php echo ucwords($get->jenis_warna) ?></option>
										<?php
									}
									?>
								</select>
							</div>
						</div>
						<div class="col-md-12">
							<div class="pull-right">
								<button type="submit" class="btn btn-primary">Submit</button>
							</div>
						</div>
					</div>
					</form>
				</div>
			</div>
			<?php
        	}else{
        		$get_color_edit = $this->db->query("SELECT * FROM color_stone WHERE id='$id_color'");
        		foreach ($get_color_edit->result() as $ed) {
        			?>
        	<div class="box box-default">
				<div class="box-body">
					<b>EDIT COLOR</b>
					<div style="border: 1px solid black; margin-bottom: 10px"></div>
					<form action="<?php echo site_url('color_scheme/update_color') ?>" method="post">
					<div class="row">
						<div class="col-md-3 col-sm-6">
							<div class="form-group">
								<label>Color Name</label>
								<input type="hidden" name="id" value="<?php echo $ed->id ?>">
								<input type="text" name="color_name" class="form-control" placeholder="store name.." value="<?php echo $ed->color ?>">
							</div>
						</div>
						<div class="col-md-5 col-sm-6">
							<div class="form-group">
								<label>Color</label>
								<input type="color" name="color" class="form-control" placeholder="address.." value="<?php echo "#".$ed->rgb_code ?>">
							</div>
						</div>
						<div class="col-md-4 col-sm-6">
							<div class="form-group">
								<label>Group Color</label>
								<select name="group" class="form-control" required="">
									<option value="" selected disabled>--Select Group--</option>
									<?php
									$get_group = $this->db->query("SELECT * FROM color_stone GROUP BY jenis_warna");
									foreach ($get_group->result() as $get) {
										if($ed->jenis_warna == $get->jenis_warna){
											?>
										<option value="<?php echo $get->jenis_warna ?>" selected><?php echo $get->jenis_warna ?></option>
											<?php
										}else{
											?>
										<option value="<?php echo $get->jenis_warna ?>"><?php echo $get->jenis_warna ?></option>
											<?php
										}
									}
									?>
								</select>
							</div>
						</div>
						<div class="col-md-12">
							<div class="pull-right">
								<button type="submit" class="btn btn-primary">Submit</button>
							</div>
						</div>
					</div>
					</form>
				</div>
			</div>
        		<?php
        		}
        	}
        	?>
			<div class="box box-default">
				<div class="box-body">
					<b>DATA COLOR</b>
					<div style="border: 1px solid black; margin-bottom: 10px"></div>
					<table class="table table-bordered table-striped table-hover" id="example1">
						<thead>
							<tr>
								<th class="text-center">No</th>
								<th class="text-center">Color</th>
								<th class="text-center">Name</th>
								<th class="text-center">Group	</th>
								<th class="text-center">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$no = 1;
							$get_store = $this->db->query("SELECT * FROM color_stone WHERE delete_by =''");
							foreach ($get_store->result() as $store) {
								?>
							<tr>
								<td class="text-center"><?php echo $no++; ?></td>
								<td>
									<div style="widows: 10px; height: 10px; background-color: #<?php echo $store->rgb_code ?>;"></div>
								</td>
								<td><?php echo $store->color ?></td>
								<td><?php echo ucwords($store->jenis_warna) ?></td>
								<td>
									<div class="pull-right">
										<a href="<?php echo site_url('color_scheme/edit_color').'/'.$store->id ?>" class="btn btn-warning" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;&nbsp;

										<a href="<?php echo site_url('color_scheme/delete_color').'/'.$store->id ?>" class="btn btn-danger" data-toggle="tooltip" data-placement="bottom" onclick="return confirm('Are you sure to delete this data?');" title="Delete"><i class="fa fa-trash"></i></a>
									</div>
								</td>
							</tr>
								<?php
							}
							?>
						</tbody>
					</table>
				</div>
			</div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->