<?php if (! defined('BASEPATH')){ exit('No direct script allowed'); }
/*
* Controller List FPJS
* Create by Erwin Idamsyach @ 2 May 2016
*/
class pembayaran extends CI_Controller{
	function __construct(){
		parent::__construct();

		if(!isLogin()){
			goLogin();
		}
        $this->load->library('fpdf');
		$this->load->library("terbilang");
	}
	
	public function index(){
		$data['filelist'] = 'pembayaran/fpjs';
		$data['title'] = 'List Pembayaran (BPS)';
		$data['title_menu'] = 'Front';
		$data['menu'] = 'pembayaran';

		getHTMLWeb($data);
	}
	
	function formFPJS($id){
		$data["filelist"] = "pembayaran/form-fpjs";
        $data["title"] = "Form Pembayaran (BPS)";
        $data["title_menu"] = "Front";
        $data["menu"] = "pembayaran";
        $data["id"] = $id;
		
		$id = str_replace('-', '/', $id);
		
		$getData = $this->db->query("SELECT tb_front_desk.*, step.certificate, step.gem_card, step.dia_grading,
									tb_lab_desk.obj_natural,
									tb_member.first_name, tb_member.middle_name, tb_member.last_name, tb_member.address, tb_member.postal_code, tb_member.phone,
									kecamatan.kecamatanNama kec1, kabupaten.kabupatenNama kota1, provinsi.provinsiNama prov1
									FROM tb_front_desk 
									LEFT JOIN step ON step.id_object = tb_front_desk.id_object
									LEFT JOIN tb_lab_desk ON tb_lab_desk.id_object = tb_front_desk.id_object
									LEFT JOIN tb_member ON tb_member.id_member = tb_front_desk.id_member
									LEFT JOIN kecamatan ON kecamatan.kecamatanId = tb_member.district
									LEFT JOIN kabupaten ON kabupaten.kabupatenId = tb_member.city
									LEFT JOIN provinsi ON provinsi.provinsiId = tb_member.province
									WHERE (step.print_gem = 'Sudah' OR step.print_cert = 'Sudah' OR step.print_grading = 'Sudah')
									AND step.store='".sessionValue('kode_store')."'
									AND tb_front_desk.id_order='".$id."'
									LIMIT 0,1");
		$tmp = $getData->result();
		$data['tmp'] = $tmp[0];	
		
		$getDataCertificate = $this->db->query("SELECT * FROM tb_service WHERE kode='FR'");
		$tmpCertificate = $getDataCertificate->result();
		$data['tmpCertificate'] = $tmpCertificate[0];	
		
		if($tmp[0]->obj_natural=="NATURAL"){
			$getDataBrief = $this->db->query("SELECT * FROM tb_service WHERE kode='BR'");
		}else{
			$getDataBrief = $this->db->query("SELECT * FROM tb_service WHERE kode='BRS'");
		}
		
		$tmpBrief = $getDataBrief->result();
		$data['tmpBrief'] = $tmpBrief[0];
		
		$getDataDG = $this->db->query("SELECT * FROM tb_service WHERE kode='DG'");
		$tmpDG = $getDataDG->result();
		$data['tmpDG'] = $tmpDG[0];
		
		$getDataCash = $this->db->query("SELECT * FROM tb_cashier WHERE id_order='".$id."' ORDER BY create_date DESC LIMIT 0,1");
		$data['getDataCash'] = $getDataCash;
        getHTMLWeb($data);
    }
	
	function submitFormFPJS(){
		$total_bayar = $this->input->post('total_bayar');
		$bayar = convertToNumber($this->input->post('bayar'));
		
		$success = false;
		$msg = "";
		if($bayar<$total_bayar){
			$msg = "Jumlah pembayaran kurang dari harga pembayaran.";
		}else if($bayar>$total_bayar){
			$msg = "Jumlah pembayaran melebihi harga pembayaran.";
		}else{
			if($this->input->post('jumlah_bayar')>0){
				$data = array(
				'jumlah' => $bayar,
				'jumlah_sebelumnya' => $this->input->post('jumlah_bayar'),
				'update_date' => date("Y-m-d H:i:s"),
				'update_by' => sessionValue('id')
				);
				$this->db->where('id', $this->input->post('id_bayar'));
				if($this->db->update('tb_cashier', $data)){
					$success = true;
				}else{
					$msg = "Gagal disimpan.";
				}
			}else{
				$data = array(
				'id_order' => $this->input->post('id_order'),
				'jumlah' => $bayar,
				'create_date' => date("Y-m-d H:i:s"),
				'create_by' => sessionValue('id')
				);
				if($this->db->insert('tb_cashier', $data)){
					$success = true;
				}else{
					$msg = "Gagal disimpan.";
				}
			}
			
			if($success){ // SELESAI
				$data = array(
				//'status_barang' => 'TERTINGGAL',
				'pay' => 'Y',
				'update_date' => date("Y-m-d H:i:s"),
				'update_by' => sessionValue('id')
				);
				$this->db->where('id_order', $this->input->post('id_order'));
				if($this->db->update('step', $data)){
					$success = true;
				}else{
					$msg = "Gagal disimpan.";
					$success = false;
				}
			}
		}
		
		echo json_encode(array("success"=>$success, "msg"=>$msg));
	}
	
	public function print_pembayaran_fpjs(){
		$ord = $this->uri->segment(3);
		$ord = str_replace('-', '/', $ord);
		$data['ord'] = $ord;
		$this->load->view('print-pembayaran-fpjs', $data);
	}
	
	public function view_data(){
		$key = $this->input->post('key');
		$data['key'] = $key;
		$this->load->view('view', $data);
	}
	
	public function fpjt(){
		$data['filelist'] = 'pembayaran/fpjt';
		$data['title'] = 'List Pembayaran (BPT)';
		$data['title_menu'] = 'Front';
		$data['menu'] = 'pembayaran_fpjt';

		getHTMLWeb($data);
	}
	
	function formFPJT($id){
		$data["filelist"] = "pembayaran/form-fpjt";
        $data["title"] = "Form Pembayaran (BPT)";
        $data["title_menu"] = "Front";
        $data["menu"] = "pembayaran_fpjt";
        $data["id"] = $id;
		
		$id = str_replace('-', '/', $id);
		
		$getData = $this->db->query("SELECT tb_fpjt.*,
									tb_member.first_name, tb_member.middle_name, tb_member.last_name, tb_member.address, tb_member.postal_code, tb_member.phone,
									kecamatan.kecamatanNama kec1, kabupaten.kabupatenNama kota1, provinsi.provinsiNama prov1
									FROM tb_fpjt 
									LEFT JOIN step ON step.id_object = tb_fpjt.id
									LEFT JOIN tb_member ON tb_member.id_member = tb_fpjt.id_member
									LEFT JOIN kecamatan ON kecamatan.kecamatanId = tb_member.district
									LEFT JOIN kabupaten ON kabupaten.kabupatenId = tb_member.city
									LEFT JOIN provinsi ON provinsi.provinsiId = tb_member.province
									WHERE step.print = 'Sudah'
									AND step.store='".sessionValue('kode_store')."'
									AND tb_fpjt.id_order='".$id."'
									LIMIT 0,1");
		$tmp = $getData->result();
		$data['tmp'] = $tmp[0];	
		
		$harga = 100000;
		$harga_permata = 50000;
		
		$data['harga'] = $harga;
		$data['harga_permata'] = $harga_permata;
		
		$getDataCash = $this->db->query("SELECT * FROM tb_cashier WHERE id_order='".$id."' ORDER BY create_date DESC LIMIT 0,1");
		$data['getDataCash'] = $getDataCash;
        getHTMLWeb($data);
    }
	
	public function print_pembayaran_fpjt(){
		$ord = $this->uri->segment(3);
		$ord = str_replace('-', '/', $ord);
		$data['ord'] = $ord;
		$this->load->view('print-pembayaran-fpjt', $data);
	}
}
?>