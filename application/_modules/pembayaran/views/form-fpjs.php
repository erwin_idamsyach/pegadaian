<script type="text/javascript">
	$(document).ready(function(){
			$('#form-pembayaran-fpjs').trigger("reset");
			$("#form-pembayaran-fpjs").on('submit',function(e){
			e.preventDefault();
			var formatData = new FormData($(this)[0]);
				swal({
				 title: "",
				 text: "Apakah Anda akan menyimpan data?",
				 type: "warning",
				 showCancelButton: true,
				 confirmButtonColor: "#DD6B55",
				 confirmButtonText: "Ya",
				 cancelButtonText: "Tidak",
				 closeOnConfirm: false }, function(){
					$.ajax({
						type : 'POST',
						url : '<?php echo base_url()?>pembayaran/submitFormFPJS',
						data : formatData,
						async: false,
						cache: false,
						contentType: false,
						processData: false,
						success:function(html){
							var data = eval ("(" + html + ")");
							if(data.success){
								swal({
								 title: "Berhasil Menyimpan data!",
								 text: "Klik tombol di bawah.",
								 type: "success",
								 showCancelButton: false,
								 confirmButtonColor: "#257DB6",
								 confirmButtonText: "Ok!",
								 closeOnConfirm: false
								},
								function(){
									location.href = base_url+"pembayaran";
								});
							}else{
								swal({
								 title: data.msg,
								 text: "Klik tombol di bawah.",
								 type: "warning",
								 showCancelButton: false,
								 confirmButtonColor: "#257DB6",
								 confirmButtonText: "Ok!",
								 closeOnConfirm: false
								});
							}
						}
					});
				});
			});
	});
		
		function backToList(){
			location.href = base_url+"pembayaran";
		}
		
      </script>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>&nbsp;</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-user"></i> Home</a></li>
            <li>Pembayaran</li>
            <li class="active">Form Pembayaran FPJS</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
        <div class="index">
          <div class="box">
            <div class="box-header">
              <b>No. FPJS : <?php echo $tmp->id_order; ?></b>
              <div style="border:1px solid black;margin-bottom:-10px;"></div>
                <div class="box-tools pull-right">
                  <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
            <i>DATA NASABAH</i>
              <div style="border:1px solid black;margin-bottom:10px;"></div>
          <form method="post" id="form-pembayaran-fpjs" enctype="multipart/form-data">
			<table width="100%">
				<tr>
					<td width="10%"><b>No. CIF</b></td>
					<td width="10">:</td>
					<td><?php echo $tmp->id_member;?></td>
				</tr>
				<tr>
					<td><b>Nama</b></td>
					<td>:</td>
					<td><?php echo $tmp->first_name." ".$tmp->middle_name." ".$tmp->last_name;?></td>
				</tr>
				<tr>
					<td><b>Alamat</b></td>
					<td>:</td>
					<td><?php echo $tmp->address." ".$tmp->postal_code.", ".$tmp->kec1." - ".str_replace('Kota ','', $tmp->kota1);?></td>
				</tr>
				<tr>
					<td><b>&nbsp;</b></td>
					<td>&nbsp;</td>
					<td><?php echo $tmp->prov1;?></td>
				</tr>
				<tr>
					<td><b>No Telp / HP</b></td>
					<td>:</td>
					<td><?php echo ($tmp->phone)?$tmp->phone:"-";?></td>
				</tr>
			</table>
			<br>
			<i>DATA OBYEK</i>
            <div style="border:1px solid black;margin-bottom:10px;"></div>
			<table width="100%">
				<thead>
        			<tr>
        				<th class="text-center" width="5%">No</th>
						<th class="text-center" >Description</th>
						<th class="text-center" width="12%">Qty</th>
						<th class="text-center" width="15%">Unit Price</th>
						<th class="text-center" width="15%">Amount</th>
        			</tr>
        		</thead>
				<tbody>
				<?php
				$dataDetail = $this->db->query("SELECT tb_front_desk.*, step.certificate, step.gem_card, step.dia_grading, 
							color_stone.color, tb_lab_desk.obj_natural FROM tb_front_desk 
							LEFT JOIN color_stone ON color_stone.code = tb_front_desk.obj_color
							LEFT JOIN step ON step.id_object = tb_front_desk.id_object
							LEFT JOIN tb_lab_desk ON tb_lab_desk.id_object = tb_front_desk.id_object
							WHERE (step.print_gem = 'Sudah' OR step.print_cert = 'Sudah' OR step.print_grading = 'Sudah')
							AND tb_front_desk.id_order='".$tmp->id_order."' 
							AND step.store='".sessionValue('kode_store')."'
							AND ISNULL(tb_front_desk.delete_by)");
							
				$no = 1;
				$total = 0;
				foreach ($dataDetail->result() as $tmpDetail) {			
					$exp = explode(';', $tmpDetail->color);
					if(count($exp)>0){
						$color_spek = $exp[0];
					}else{
						$color_spek = $tmpDetail->color;
					}
				?>
					<tr>
						<td class="text-center" valign="top"><?php echo $no++; ?></td>
						<td class="text-center">
							<table width="100%">
								<tr>
									<td width="10%">ID Object</td>
									<td width="10">:</td>
									<td><?php echo $tmpDetail->id_object; ?></td>
								</tr>
								<?php
								if($tmpDetail->certificate!=""){
									$total = $total + $tmpCertificate->harga;
								?>
								<tr>
									<td colspan="3">&nbsp;&nbsp; <?php echo $tmpCertificate->nama; ?></td>
								</tr>
								<tr>
								<?php } 
								if($tmpDetail->gem_card!=""){
									//$total = $total + $tmpBrief->harga;
								?>
								<tr>
									<td colspan="3">&nbsp;&nbsp; <?php echo $tmpBrief->nama; ?></td>
								</tr>
								<?php } 
								if($tmpDetail->dia_grading!=""){
								?>
								<tr>
									<td colspan="3">&nbsp;&nbsp; Diamond Grading</td>
								</tr>
								<?php } ?>
								<tr>
									<td colspan="3">&nbsp;&nbsp; <?php echo $color_spek; ?></td>
								</tr>
								<tr>
									<td>&nbsp;&nbsp; Weight</td>
									<td>:</td>
									<td><?php echo $tmpDetail->obj_weight; ?> ct(s)</td>
								</tr>
								<tr>
									<td>&nbsp;&nbsp; Width</td>
									<td>:</td>
									<td><?php echo $tmpDetail->obj_width; ?> mm</td>
								</tr>
								<tr>
									<td>&nbsp;&nbsp; Height</td>
									<td>:</td>
									<td><?php echo $tmpDetail->obj_height; ?> mm</td>
								</tr>
								<tr>
									<td>&nbsp;&nbsp; Length</td>
									<td>:</td>
									<td><?php echo $tmpDetail->obj_length; ?> mm</td>
								</tr>
							</table>
						</td>
						<td class="text-center" valign="top">
							<table width="100%">
								<tr>
									<td>&nbsp;</td>
								</tr>
								<?php
								if($tmpDetail->certificate!=""){
								?>
								<tr>
									<td class="text-center">1</td>
								</tr>
								<?php } 
								if($tmpDetail->gem_card!=""){
									if($tmpDetail->obj_natural!="NATURAL"){
										$mem_pri = mysql_query("SELECT * FROM tb_service WHERE kode='BRS'");
										$pri_mem = mysql_fetch_array($mem_pri);
									}else{
										$mem_pri = mysql_query("SELECT * FROM tb_service WHERE kode='BR'");
										$pri_mem = mysql_fetch_array($mem_pri);
									}
									$tmpBrief->harga = $pri_mem['harga'];
									$total = $total + $pri_mem['harga'];
								?>
								<tr>
									<td class="text-center">1</td>
								</tr>
								<?php } 
								if($tmpDetail->dia_grading!=""){
								?>
								<tr>
									<td class="text-center">1</td>
								</tr>
								<?php } ?>
							</table>
						</td>
						<td class="text-center" valign="top">
							<table width="100%">
								<tr>
									<td>&nbsp;</td>
								</tr>
								<?php
								if($tmpDetail->certificate!=""){
								?>
								<tr>
									<td class="text-right"><?php echo number_format($tmpCertificate->harga,0,',','.'); ?></td>
								</tr>
								<?php } 
								if($tmpDetail->gem_card!=""){
								?>
								<tr>
									<td class="text-right"><?php echo number_format($tmpBrief->harga,0,',','.'); ?></td>
								</tr>
								<?php } 
								if($tmpDetail->dia_grading!=""){
									$getHarga = $this->db->query("SELECT harga
															FROM master_harga_grading
															WHERE berat2 >= ".$tmpDetail->obj_weight." ORDER BY ID ASC LIMIT 0,1");
									$harga_grading = 0;
									foreach ($getHarga->result() as $tmpHarga) {
										$harga_grading = $tmpHarga->harga;
									}
								?>
								<tr>
									<td class="text-right"><?php echo number_format($harga_grading,0,',','.'); ?></td>
								</tr>
								<?php } ?>
							</table>
						</td>
						<td class="text-center" valign="top">
							<table width="100%">
								<tr>
									<td>&nbsp;</td>
								</tr>
								<?php
								if($tmpDetail->certificate!=""){
								?>
								<tr>
									<td class="text-right"><?php echo number_format($tmpCertificate->harga,0,',','.'); ?></td>
								</tr>
								<?php } 
								if($tmpDetail->gem_card!=""){
								?>
								<tr>
									<td class="text-right"><?php echo number_format($tmpBrief->harga,0,',','.'); ?></td>
								</tr>
								<?php } 
								if($tmpDetail->dia_grading!=""){
									$getHarga = $this->db->query("SELECT harga
															FROM master_harga_grading
															WHERE berat2 >= ".$tmpDetail->obj_weight." ORDER BY ID ASC LIMIT 0,1");
									$harga_grading = 0;
									foreach ($getHarga->result() as $tmpHarga) {
										$harga_grading = $tmpHarga->harga;
									}
									$total = $total + $harga_grading;
								?>
								<tr>
									<td class="text-right"><?php echo number_format($harga_grading,0,',','.'); ?></td>
								</tr>
								<?php } ?>
							</table>
						</td>
					</tr>
				<?php 
				} 
				$ppn = $total*10/100;
				$total_harga = $total; //+$ppn;
				?>
				</tbody>
				<tfoot style="border-top:2px solid black;margin-bottom:10px;">
        			<tr>
        				<th colspan="3">&nbsp;</th>
        				<th class="text-right">Sub Total</th>
						<th class="text-right"><?php echo number_format($total,0,',','.'); ?></th>
        			</tr>
        			<!--<tr>
        				<th colspan="3">&nbsp;</th>
        				<th class="text-right">PPN</th>
						<th class="text-right"><?php //echo number_format($ppn,0,',','.'); ?></th>
        			</tr>-->
        			<tr>
        				<th colspan="3">&nbsp;</th>
        				<th class="text-right">Total</th>
						<th class="text-right"><?php echo number_format($total_harga,0,',','.'); ?></th>
        			</tr>
        			<tr>
        				<th colspan="3">&nbsp;</th>
        				<th class="text-right">&nbsp;</th>
						<th class="text-right">&nbsp;</th>
        			</tr>
        			<tr>
        				<th colspan="3">&nbsp;</th>
        				<th class="text-right">Jumlah Bayar</th>
						<th class="text-right" style="padding-left:15px;">
						<input type="hidden" name="id_order" id="id_order" value="<?php echo $tmp->id_order; ?>">
						<input type="hidden" name="total_bayar" id="total_bayar" value="<?php echo $total_harga; ?>">
						<?php
						$id_bayar = "";
						$jumlah_bayar = 0;
						foreach($getDataCash->result() as $tmpCash){
							$id_bayar = $tmpCash->id;
							$jumlah_bayar = $tmpCash->jumlah;
						}
						if($jumlah_bayar==0){
							$disabled_button = "";
						}else{
							$disabled_button = "disabled";
						}
						?>
						<input type="hidden" name="id_bayar" id="id_bayar" value="<?php echo $id_bayar; ?>">
						<input type="hidden" name="jumlah_bayar" id="jumlah_bayar" value="<?php echo $jumlah_bayar; ?>">
						<input type="text" name="bayar" id="bayar" class="form-control text-right" placeholder="Jumlah Bayar" value="<?php echo number_format($jumlah_bayar,0,',','.'); ?>" onKeyUp="this.value=ThausandSeperator(this.value,2);" required>
						</th>
        			</tr>
        			<tr>
        				<th colspan="3">&nbsp;</th>
						<th colspan="2" class="text-right" style="padding-left:10px; vertical-align: middle;">
						<div class="col-md-12 text-right" style="margin-top:20px; padding-right: 0px; float: right;">
							<button type="button" class="btn btn-default" onclick="backToList()"><i class="fa fa-refresh"></i> BACK</button>
							<?php if($jumlah_bayar==0){ ?>
							<button type="submit" class="btn btn-default <?php echo $disabled_button;?>"><i class="fa fa-money"></i> BAYAR</button>
							<? }else{
							?>
							<button type="button" class="btn btn-default <?php echo $disabled_button;?>"><i class="fa fa-money"></i> BAYAR</button>
							<?
							}?>
						</div>
						</th>
        			</tr>
				</tfoot>
			</table>
			</form>
              </div>
            </div>
          </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->