<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
* Controller Master Luster
* Create By : Erwin Idamsyach Putra
* 23 April 2016
*/
class luster extends CI_Controller{
	function __construct(){
		parent::__construct();

		if(!isLogin()){
			goLogin();
		}
	}
	
	public function index(){
		$data['filelist'] = 'luster/front';
		$data['title'] = 'Master Luster';
		$data['title_menu'] = 'Admin';
		$data['menu'] = 'luster';

		getHTMLWeb($data);
	}

	public function add_luster(){
		$luster = $this->input->post('luster');
		$descr = $this->input->post('descr');

		$data = array(
			'luster' => $luster,
			'penjelasan' => $descr,
			'create_by' => sessionValue('nama'),
			'create_date' => date('Y-m-d')	
			);
		$this->db->insert('luster', $data);
		redirect(site_url('luster'));
	}

	public function edit_luster(){
		$id  = $this->uri->segment(3);

		$data['filelist'] = 'luster/front';
		$data['title'] = 'Master Luster';
		$data['title_menu'] = 'Admin';
		$data['menu'] = 'luster';
		$data['id_luster'] = $id;

		getHTMLWeb($data);
	}

	public function update_luster(){
		$id   = $this->input->post('id');
		$luster = $this->input->post('luster');
		$descr = $this->input->post('descr');

		$data = array(
			'luster' => $luster,
			'penjelasan' => $descr,
			'update_by' => sessionValue('nama'),
			'update_date' => date('Y-m-d')
			);

		$this->db->where('id', $id);
		$this->db->update('luster', $data);
		redirect(site_url('luster'));
	}

	public function delete_luster(){
		$id = $this->uri->segment(3);

		$data = array(
			'delete_by' => sessionValue('nama'),
			'delete_date' => date('Y-m-d')
			);
		$this->db->where('id', $id);
		$this->db->update('luster', $data);
		redirect(site_url('luster'));
	}
}
?>