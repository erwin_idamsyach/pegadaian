<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
* Controller Parameter Color Group
* Create By : Erwin Idamsyach Putra
* 23 April 2016
*/
class color_group extends CI_Controller{
	public function index(){
		$data['filelist'] = 'color_group/front';
		$data['title'] = 'Color Group';
		$data['title_menu'] = 'Admin';
		$data['menu'] = 'color_group';

		getHTMLWeb($data);
	}
}
?>