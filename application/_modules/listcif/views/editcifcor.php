	   <script type="text/javascript">
        jQuery(function($){
           $("#kode_pos").mask("99999",{placeholder:"_____"});
		   
		   $('.datepicker').datepicker();
			
        });
		
		function backToList(){
			location.href = base_url+"listcif";
		}
		
		function edit_cif_coorporate() {
		  var no_cor = $('#no_cor').val();
		  var nama_perusahaan = $('#nama_perusahaan').val();
		  var tanggal_pendirian = $('#tanggal_pendirian').val();
		  var telpon_perusahaan = $('#telpon_perusahaan').val();
		  var alamat_perusahaan = $('#alamat_perusahaan').val();
		  var propinsi_perusahaan = $('#propinsi_perusahaan').val();
		  var kota_perusahaan = $('#kota_perusahaan').val();
		  var kecamatan_perusahaan = $('#kecamatan_perusahaan').val();
		  var kode_pos_perusahaan = $('#kode_pos_perusahaan').val();
		  var email_perusahaan = $('#email_perusahaan').val();
		  var no_npwp_perusahaan = $('#no_npwp_perusahaan').val();
		  var no_rekening_perusahaan = $('#no_rekening_perusahaan').val();
		  var siup_perusahaan = $('#siup_perusahaan').val();
		  var bidang_usaha_perusahaan = $('#bidang_usaha_perusahaan').val();
		  var tdp_perusahaan = $('#tdp_perusahaan').val();
		  var nama_izin_usaha_perusahaan = $('#nama_izin_usaha_perusahaan').val();
		  var no_izin_usaha_perusahaan = $('#no_izin_usaha_perusahaan').val();
		  var bentuk_perusahaan = $('#bentuk_perusahaan').val();
		  var bentuk_perusahaan_lainnya = $('#bentuk_perusahaan_lainnya').val();
		  var tujuan_transaksi_perusahaan = $('#tujuan_transaksi_perusahaan').val();
		  
		  if(no_cor == "" || nama_perusahaan == "" || tanggal_pendirian == "" || telpon_perusahaan == "" || alamat_perusahaan == "" || no_npwp_perusahaan == "" || tdp_perusahaan == ""){
			return false;
		  }else{
			swal({
				 title: "",
				 text: "Apakah Anda akan menyimpan data?",
				 type: "warning",
				 showCancelButton: true,
				 confirmButtonColor: "#DD6B55",
				 confirmButtonText: "Ya",
				 cancelButtonText: "Tidak",
				 closeOnConfirm: false }, function(){
					$.ajax({
					  type : "POST",
					  url  : base_url+"listcif/edit_cif_coorporate",
					  data : {
						'no_cor' : no_cor,
						'nama_perusahaan' : nama_perusahaan,
						'tanggal_pendirian' : tanggal_pendirian,
						'telpon_perusahaan' : telpon_perusahaan,
						'alamat_perusahaan' : alamat_perusahaan,
						'propinsi_perusahaan'    : propinsi_perusahaan,
						'kota_perusahaan'     : kota_perusahaan,
						'kecamatan_perusahaan'     : kecamatan_perusahaan,
						'kode_pos_perusahaan'     : kode_pos_perusahaan,
						'email_perusahaan'     : email_perusahaan,
						'no_npwp_perusahaan'     : no_npwp_perusahaan,
						'no_rekening_perusahaan'     : no_rekening_perusahaan,
						'siup_perusahaan'     : siup_perusahaan,
						'bidang_usaha_perusahaan'     : bidang_usaha_perusahaan,
						'tdp_perusahaan'     : tdp_perusahaan,
						'nama_izin_usaha_perusahaan'     : nama_izin_usaha_perusahaan,
						'no_izin_usaha_perusahaan'     : no_izin_usaha_perusahaan,
						'bentuk_perusahaan'     : bentuk_perusahaan,
						'bentuk_perusahaan_lainnya'     : bentuk_perusahaan_lainnya,
						'tujuan_transaksi_perusahaan'     : tujuan_transaksi_perusahaan
					  },
					  success:function(html){ 
						var data = eval ("(" + html + ")");
						if(data.success){
							swal({
							  title: "Berhasil Menyimpan data!",
							  text: "Klik tombol di bawah.",
							  type: "success",
							  showCancelButton: false,
							  confirmButtonColor: "#257DB6",
							  confirmButtonText: "Ok!",
							  closeOnConfirm: false
							},
							function(){
							  location.href = base_url+"listcif";
							});
						}else{
							swal.close();
							popOverMsg('no_npwp_perusahaan', data.msg);
						}
					  }
					});
				});	
			return false;
		  }
		}
      </script>
	  
    <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>&nbsp;</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-user"></i> Reg. CIF</a></li>
            <li class="active">Edit CIF</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="index">
            <div class="box">
              <div class="box-header">
                <b>EDIT CIF KORPORASI</b>
                <div style="border:1px solid black;margin-bottom:0px;"></div>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div>
              </div>
              <div class="box-body">
					<form method="POST" onSubmit="return edit_cif_coorporate()" >
					  <div class="row">
						<div class="col-md-12">
						<i>NASABAH KORPORASI</i>
						<div style="border:1px solid black;margin-bottom:10px;"></div>
						</div>
					  </div>
					  <div class="row">
						<div class="col-md-3">
						  <div class="form-group">
							<label>Nomor Korporasi <font color="red">*</font></label>
							<input type="text" name="no_cor" id="no_cor" class="form-control" placeholder="Nomor Korporasi" value="<?php echo $id;?>" readonly required>
						  </div>
						</div>
						<div class="col-md-6">
						  <div class="form-group">
							<label>Nama Korporasi/Perusahaan <font color="red">*</font></label>
							<input type="text" name="nama_perusahaan" id="nama_perusahaan" class="form-control" placeholder="Nama Korporasi/Perusahaan" value="<?php echo $tmp['corp_name'];?>" required>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Tanggal Pendirian <font color="red">*</font></label>
							<input type="text" name="tanggal_pendirian" id="tanggal_pendirian" class="form-control datepicker" placeholder="Tanggal Pendirian" value="<?php echo date('m/d/Y', strtotime($tmp['tanggal_lahir']));?>" required>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>No. Telpon <font color="red">*</font></label>
							<div class="input-group"><span class="input-group-addon">021</span><input class="form-control" id="telpon_perusahaan" name="telpon_perusahaan" type="text" value="<?php echo $tmp['phone'];?>" required/></div>
						  </div>
						</div>
						<div class="col-md-6">
						  <div class="form-group">
							<label>Alamat <font color="red">*</font></label>
							<input type="text" name="alamat_perusahaan" id="alamat_perusahaan" class="form-control" placeholder="Alamat" value="<?php echo $tmp['address'];?>" required>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Propinsi <font color="red">*</font></label>
							<select name="province" id="propinsi_perusahaan" class="form-control prov3" onchange="get_kota3()" required>
							  <option value="">Select Propinsi</option>
							<?php
							  $get_pro = mysql_query("SELECT * FROM master_provinsi order by provinsi_id");
							  while ($do = mysql_fetch_array($get_pro)) {
								  if($do['provinsi_id']==$tmp['province']){
									  $select_propinsi = "selected";
								  }else{
									  $select_propinsi = "";
								  }
							?>
							  <option value="<?php echo $do['provinsi_id'] ?>" <?php echo $select_propinsi;?>><?php echo $do['provinsi_nama'] ?></option>
							<?php
							  }
							?>
							</select>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Kota <font color="red">*</font></label>
							<select name="city" id="kota_perusahaan" class="form-control kota3" required>
							  <option value="<?php echo $tmp['city'];?>"><?php echo $kota;?></option>
							</select>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Kecamatan <font color="red">*</font></label>
							<select name="district" id="kecamatan_perusahaan" class="form-control keca3" required>
							  <option value="<?php echo $tmp['district'];?>"><?php echo $kecamatan;?></option>
							</select>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Kode Pos <font color="red">*</font></label>
							<input type="text" name="kode_pos_perusahaan" id="kode_pos_perusahaan" class="form-control" maxlength="5" placeholder="Kode Pos" value="<?php echo $tmp['postal_code'];?>" required>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Email</label>
							<input type="text" name="email_perusahaan" id="email_perusahaan" class="form-control" placeholder="Email" value="<?php echo $tmp['email'];?>">
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group no_npwp_perusahaan">
							<label>No. NPWP <font color="red">*</font></label>
							<input type="text" name="no_npwp_perusahaan" id="no_npwp_perusahaan" class="form-control" placeholder="No. NPWP" value="<?php echo $tmp['no_npwp'];?>" required>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>No. Rekening Bank <font color="red">*</font></label>
							<input type="text" name="no_rekening_perusahaan" id="no_rekening_perusahaan" class="form-control" placeholder="Rekening Bank" value="<?php echo $tmp['corp_norek'];?>" required>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>SIUP <font color="red">*</font></label>
							<input type="text" name="siup_perusahaan" id="siup_perusahaan" class="form-control" placeholder="SIUP" value="<?php echo $tmp['corp_siup'];?>" required>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Bidang Usaha <font color="red">*</font></label>
							<input type="text" name="bidang_usaha_perusahaan" id="bidang_usaha_perusahaan" class="form-control" placeholder="Bidang Usaha" value="<?php echo $tmp['corp_bidang_usaha'];?>" required>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>TDP <font color="red">*</font></label>
							<input type="text" name="tdp_perusahaan" id="tdp_perusahaan" class="form-control" placeholder="TDP" value="<?php echo $tmp['corp_tdp'];?>" required>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Nama Izin Usaha <font color="red">*</font></label>
							<input type="text" name="nama_izin_usaha_perusahaan" id="nama_izin_usaha_perusahaan" class="form-control" placeholder="Nama Izin Usaha" value="<?php echo $tmp['corp_nama_izin_usaha'];?>" required>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>No. Izin Usaha <font color="red">*</font></label>
							<input type="text" name="no_izin_usaha_perusahaan" id="no_izin_usaha_perusahaan" class="form-control" placeholder="No. Izin Usaha" value="<?php echo $tmp['corp_no_izin_usaha'];?>" required>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Bentuk Perusahaan <font color="red">*</font></label>
							<select name="bentuk_perusahaan" id="bentuk_perusahaan" class="form-control" required>
							  <option value="">Select Bentuk Perusahaan</option>
							<?php
							  $get_pro = mysql_query("SELECT * FROM master_bentuk_perusahaan WHERE ISNULL(delete_by) order by id");
							  while ($do = mysql_fetch_array($get_pro)) {
								  if($do['id']==$tmp['corp_form']){
									  $select_bentuk_perusahaan = "selected";
								  }else{
									  $select_bentuk_perusahaan = "";
								  }
							?>
							  <option value="<?php echo $do['id'] ?>" <?php echo $select_bentuk_perusahaan;?>><?php echo $do['bentuk_perusahaan'] ?></option>
							<?php
							  }
							?>
							</select>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Bentuk Perusahaan Lainnya </label>
							<input type="text" name="bentuk_perusahaan_lainnya" id="bentuk_perusahaan_lainnya" class="form-control" placeholder="Bentuk Perusahaan Lainnya" value="<?php echo $tmp['corp_form_other'];?>">
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Tujuan Transaksi </label>
							<input type="text" name="tujuan_transaksi_perusahaan" id="tujuan_transaksi_perusahaan" class="form-control" placeholder="Tujuan Transaksi" value="<?php echo $tmp['corp_tujuan_transaksi'];?>">
						  </div>
						</div>
					  </div>
					  <div class="row">
						<div class="col-md-12">
						  <button type="submit" class="btn btn-default" onclick=""><i class="fa fa-save"></i> SIMPAN</button>
						  <button type="button" class="btn btn-default" onclick="backToList()"><i class="fa fa-refresh"></i> BACK</button>
						</div>
					  </div>
					</form>
              </div>
            </div>
          </div>

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->