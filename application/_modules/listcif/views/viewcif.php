  
    <!-- Content Wrapper. Contains page content -->
      

        <!-- Main content -->
        <section class="content">
          <div class="index">
            <div class="box">
              <div class="box-header">
                <b>VIEW CIF INDIVIDUAL</b>
                <div style="border:1px solid black;margin-bottom:0px;"></div>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div>
              </div>
              <div class="box-body">
				<div class="nav-tabs-custom">
                  
					<div>
					  <div class="row">
						<div class="col-md-12">
						<i>CUSTOMER INFORMATION FILE(CIF)</i>
						<div style="border:1px solid black;margin-bottom:10px;"></div>
						</div>
					  </div>
					  <div class="row">		
						<div class="col-md-3">
						  <div class="form-group">
							<label>Nomor CIF <font color="red">*</font></label>
							<br /><?php echo $id;?>
						  </div>
						</div>
						<div class="col-md-6">
						  <div class="form-group">
							<label>Cabang <font color="red">*</font></label>
							<br /><?php echo $tmp['store'];?>
						  </div>
						</div>
					  </div>
					  <div class="row">
						<div class="col-md-12">
						<i>DATA PRIBADI</i>
						<div style="border:1px solid black;margin-bottom:10px;"></div>
						</div>
					  </div>
					  <div class="row">
						<div class="col-md-9">
						  <div class="form-group">
							<label>Nama Lengkap <font color="red">*</font></label>
							<br /><?php echo $tmp['first_name'];?>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Nama Ibu Kandung <font color="red">*</font></label>
							<br /><?php echo $tmp['nama_ibu'];?>
						  </div>
						</div>
						<div class="col-md-6">
						  <div class="form-group">
							<label>Tempat Lahir <font color="red">*</font></label>
							<br /><?php echo $tmp['tempat_lahir'];?>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Tanggal Lahir <font color="red">*</font></label>
							<br /><?php echo date('m/d/Y', strtotime($tmp['tanggal_lahir']));?>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Agama <font color="red">*</font></label>
							<br /><?php echo $tmp['agama'];?>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Identitas <font color="red">*</font></label>
							<br /><?php echo $tmp['identitas'];?>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>No. KTP/SIM/PASSPOR <font color="red">*</font></label></label>
							<br /><?php echo $tmp['no_identitas'];?>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Masa Berlaku <font color="red">*</font></label>
							<br /><?php echo date('m/d/Y', strtotime($tmp['masa_berlaku']));?>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Jenis Kelamin <font color="red">*</font></label>
							<br /><?php echo $tmp['jenis_kelamin'];?>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Pendidikan Terakhir <font color="red">*</font></label>
							<br /><?php echo $tmp['pendidikan'];?>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Status Perkawinan <font color="red">*</font></label>
							<br /><?php echo $tmp['perkawinan'];?>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Nama Pasangan</label>
							<br /><?php echo (($tmp['nama_pasangan']!="")?$tmp['nama_pasangan']:'-');?>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>No. Telpon Rumah</label>
							<br /><?php echo (($tmp['telpom_rumah']!="")?'021 '.$tmp['telpom_rumah']:'-');?>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>No. Handphone <font color="red">*</font></label>
							<br /><?php echo (($tmp['phone']!="")?'62'.$tmp['phone']:'-');?>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Email</label>
							<br /><?php echo (($tmp['email']!="")?$tmp['email']:'-');?>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>No. NPWP</label>
							<br /><?php echo (($tmp['no_npwp']!="")?$tmp['no_npwp']:'-');?>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Kewarganegaraan <font color="red">*</font></label>
							<br /><?php echo $tmp['kewarganegaraan'];?>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Kewarganegaraan Lainnya</label>
							<br /><?php echo (($tmp['kewarganegaraan_lainnya']!="")?$tmp['kewarganegaraan_lainnya']:'-');?>
						  </div>
						</div>
					  </div>
					  <div class="row">
						<div class="col-md-12">
						<i>DATA KEUANGAN</i>
						<div style="border:1px solid black;margin-bottom:10px;"></div>
						</div>
					  </div>
					  <div class="row">
						<div class="col-md-3">
						  <div class="form-group">
							<label>Sumber Dana <font color="red">*</font></label>
							<br /><?php echo $tmp['sumber_dana'];?>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Rata-Rata Penghasilan</label>
							<br /><?php echo (($tmp['penghasilan']!="")?$tmp['penghasilan']:'-');?>
						  </div>
						</div>
					  </div>
					  <div class="row">
						<div class="col-md-12">
						<i>DATA PEKERJAAN</i>
						<div style="border:1px solid black;margin-bottom:10px;"></div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Pekerjaan <font color="red">*</font></label>
							<br /><?php echo $tmp['pekerjaan'];?>
						  </div>
						</div>
						<div class="col-md-6">
						  <div class="form-group">
							<label>Alamat (Sesuai Identitas) <font color="red">*</font></label>
							<br /><?php echo $tmp['address'];?>
						  </div>
						</div><div class="col-md-3">
						  <div class="form-group">
							<label>Propinsi (Sesuai Identitas) <font color="red">*</font></label>
							<br /><?php echo $tmp['prov1'];?>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Kota (Sesuai Identitas) <font color="red">*</font></label>
							<br /><?php echo $tmp['kota1'];?>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Kecamatan (Sesuai Identitas) <font color="red">*</font></label>
							<br /><?php echo $tmp['kec1'];?>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Kode Pos (Sesuai Identitas) <font color="red">*</font></label>
							<br /><?php echo $tmp['postal_code'];?>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Status Tempat Tinggal </label>
							<br /><?php echo (($tmp['status_tempat_tinggal']!="")?$tmp['status_tempat_tinggal']:'-');?>
							</select>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Menempati Sejak </label>
							<br /><?php echo (($tmp['menempati_sejak']!="")?date('m/d/Y', strtotime($tmp['nama_pasangan'])):'-');?>
						  </div>
						</div>
						<div class="col-md-6">
						  <div class="form-group">
							<label>Alamat Domisili <font color="red">*</font></label>
							<br /><?php echo $tmp['address2'];?>
						  </div>
						</div><div class="col-md-3">
						  <div class="form-group">
							<label>Propinsi Domisili <font color="red">*</font></label>
							<br /><?php echo $tmp['prov2'];?>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Kota Domisili <font color="red">*</font></label>
							<br /><?php echo $tmp['kota2'];?>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Kecamatan Domisili <font color="red">*</font></label>
							<br /><?php echo $tmp['kec2'];?>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Kode Pos Domisili <font color="red">*</font></label>
							<br /><?php echo $tmp['postal_code2'];?>
						  </div>
						</div>
						<div class="col-md-3">
						  <div class="form-group">
							<label>Alamat Surat Menyurat <font color="red">*</font></label>
							<br /><?php echo $tmp['alamat_surat_menyurat'];?>
						  </div>
						</div>
					  </div>
					</div>
				</div>
              </div>
            </div>
          </div>

        </section><!-- /.content -->
      <!-- /.content-wrapper -->