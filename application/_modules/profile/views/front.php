	<script type="text/javascript">
		$(document).ready(function(){
			$('#form-profile').trigger("reset");
			$("#form-profile").on('submit',function(e){
			e.preventDefault();
			var formatData = new FormData($(this)[0]);
				swal({
				 title: "",
				 text: "Apakah Anda akan menyimpan data?",
				 type: "warning",
				 showCancelButton: true,
				 confirmButtonColor: "#DD6B55",
				 confirmButtonText: "Ya",
				 cancelButtonText: "Tidak",
				 closeOnConfirm: false }, function(){
					$.ajax({
						type : 'POST',
						url : base_url+'profile/edit',
						data : formatData,
						async: false,
						cache: false,
						contentType: false,
						processData: false,
						success:function(html){
							//window.location.reload();
							var data = eval ("(" + html + ")");
							if(data.success){
								swal({
								 title: "Berhasil Menyimpan data!",
								 text: "Klik tombol di bawah.",
								 type: "success",
								 showCancelButton: false,
								 confirmButtonColor: "#257DB6",
								 confirmButtonText: "Ok!",
								 closeOnConfirm: false
								},
								function(){
									location.href = base_url+"profile";
								});
							}else{
								swal({
								 title: data.msg,
								 text: "Klik tombol di bawah.",
								 type: "warning",
								 showCancelButton: false,
								 confirmButtonColor: "#257DB6",
								 confirmButtonText: "Ok!",
								 closeOnConfirm: false
								});
							}
						}
					});
				});
			});
		});
	</script>
	
	<?php
    $get_profile = $this->db->query("SELECT * FROM tb_login WHERE username='".sessionValue('username')."'");
    ?>
    <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>&nbsp;</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Profile</li>
          </ol>
        </section>
        <!-- Main content -->
        <?php
        foreach ($get_profile->result() as $get) {
          ?>
        <section class="content">
          <div class="box box-default">
            <div class="box-body">
              <b>PROFILE</b>
              <div style="border:1px solid black;margin-bottom:0px;"></div><br/>
              <form id="form-profile" method="post" enctype="multipart/form-data">
              <div class="row">
                <div class="col-md-3 col-sm-3">
                  <div class="form-group">
                    <label>Photo</label>
                    <input type="hidden" name="id" value="<?php echo $get->id ?>">
                    <input type="file" name="image" class="form-control drop" data-height="168px" data-default-file="<?php echo base_url() ?>asset/foto-petugas/<?php echo $get->foto ?>">
                  </div>
                </div>
                <div class="col-md-9 col-sm-9">
                  <div class="row">
                    <div class="col-md-4 col-sm-6">
                      <div class="form-group form-user">
                        <label>Username</label>
                        <input type="text" name="profle_user" id="profle_user" class="form-control" placeholder="Username" disabled="" value="<?php echo $get->username ?>">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                      <div class="form-group form-user">
                        <label>New Password</label>
                        <input type="password" name="new_password" id="new_password" class="form-control" placeholder="New Password" >
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                      <div class="form-group form-user">
                        <label>Old Password</label>
                        <input type="password" name="old_password" id="old_password" class="form-control" placeholder="Old Password" >
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                      <div class="form-group">
                        <label>Name</label>
                        <input type="text" name="name" class="form-control" placeholder="officer name.." required value="<?php echo $get->nama ?>">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                      <div class="form-group">
                        <label>Gender</label>
                        <select name="gender" class="form-control" disabled="">
                        <?php
                        if($get->gender == "Laki-Laki"){
                          ?>
                          <option value="" disabled selected>--Select gender--</option>
                          <option value="Laki-Laki" selected="">Male</option>
                          <option value="Perempuan">Female</option>
                          <?php
                        }else if($get->gender == "Perempuan"){
                          ?>
                          <option value="" disabled selected>--Select gender--</option>
                          <option value="Laki-Laki">Male</option>
                          <option value="Perempuan" selected="">Female</option>
                          <?php
                        }else{
                          ?>
                          <option value="" disabled selected>--Select gender--</option>
                          <option value="Laki-Laki">Male</option>
                          <option value="Perempuan">Female</option>
                          <?php
                        }
                        ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                      <div class="form-group">
                        <label>Email</label>
                        <input type="email" name="email" class="form-control" placeholder="Email" value="<?php echo $get->email ?>">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                      <div class="form-group">
                        <label>Phone Number</label>
                        <input type="text" name="phone" class="form-control" placeholder="Phone Number" value="<?php echo $get->phone ?>">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                      <div class="form-group">
                        <label>Work Area</label>
                        <select name="area" class="form-control" required="" disabled="">
                          <option value="" selected disabled>--Select area--</option>
                          <?php
                          $get_area = $this->db->query("SELECT * FROM tb_store");
                          foreach ($get_area->result() as $area) {
                            if($get->store == $area->kode){
                              ?>
                              <option value="<?php echo $area->kode ?>" selected><?php echo $area->store ?></option>
                              <?php
                            }else{
                              ?>
                              <option value="<?php echo $area->kode ?>"><?php echo $area->store ?></option>
                              <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="pull-right">
                        <button type="submit" class="btn btn-primary">Submit</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              </form>
            </div>
          </div>
        </section><!-- /.content -->
        <?php
        }
        ?>
      </div><!-- /.content-wrapper -->