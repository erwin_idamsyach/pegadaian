	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>&nbsp;</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
            <li>Report Order</li>
            <li class="active">Report Order Today</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="box box-default">
            <div class="box-body">
              <b>DATA ORDER TODAY</b>
              <div style="border: 1px solid black; margin-bottom: 10px;"></div>
              <table class="table table-bordered table-striped table-hover" id="example1">
                <thead>
                  <tr>
                    <th class="text-center">No</th>
                    <th class="text-center">Store</th>
                    <th class="text-center">ID Order</th>
                    <th class="text-center">Total Speciment</th>
                    <th class="text-center">Price</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $no = 1;
                  $date_se = date('Y-m-d');
                  $get_today = $this->db->query("SELECT * FROM tb_front_desk, tb_store WHERE tb_front_desk.store = tb_store.kode AND tb_front_desk.create_date='$date_se' AND tb_front_desk.delete_by='' GROUP BY tb_front_desk.id_order");
                  foreach ($get_today->result() as $get) {
                    ?>
                    <tr>
                      <td class="text-center"><?php echo $no++; ?></td>
                      <td class="text-lefy"><?php echo $get->store ?></td>
                      <td class="text-center"><?php echo $get->id_order ?></td>
                      <td class="text-center">
                        <?php
                        $get_sum = $this->db->query("SELECT COUNT(*) as total_speciment FROM tb_front_desk WHERE id_order='".$get->id_order."' AND delete_by=''");
                        foreach ($get_sum->result() as $sum) {
                          echo $sum->total_speciment;
                        }
                        ?>
                      </td>
                      <td class="text-right"><?php echo number_format($get->price,0,',','.') ?></td>
                    </tr>
                    <?php
                  }
                  ?>
                </tbody>
                <tfoot>
                  <tr>
                    <td colspan="5">
                      <div class="pull-right">
                        <a href="<?php echo site_url('report_order_day/print_order_today').'/'.$date_se ?>" target="_blank" class="btn btn-primary">PRINT REPORT</a>
                      </div>
                    </td>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->