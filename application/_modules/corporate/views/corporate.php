      <script type="text/javascript">
        jQuery(function($){
           $("#postal").mask("99999",{placeholder:"_____"});
           $("#phone").mask("99999999999",{placeholder:""});
        });
      </script>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>&nbsp;</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-user"></i> Reg. Costumer</a></li>
            <li class="active">Corporate</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="index">
            <div class="box">
              <div class="box-header">
                <b>ADD MEMBER-CORPORATE</b>
                <div style="border:1px solid black;margin-bottom:0px;"></div>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
              </div>
              <div class="box-body">
                <div class="col-md-12">
                  <div class="row">
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Company Name <font color="red">*</font></label>
                        <input type="text" name="fir_name" id="fir_name" class="form-control" placeholder="Company name">
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Category <font color="red">*</font></label>
                        <select name="pic" id="pic" class="form-control">
                          <option value="">---select---</option>
                          <option value="PT">Perseroan Terbatas</option>
                          <option value="CV">CV</option>
                          <option value="Firma">Firma</option>
                          <option value="PP">Perusahaan Perseorangan</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Email</label>
                        <input type="text" name="email" id="email" class="form-control" placeholder="Email">
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Phone <font color="red">*</font></label>
                        <div class="input-group"><span class="input-group-addon">62</span><input class="form-control" id="phone" name="phone" type="text"/></div>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Province <font color="red">*</font></label>
                        <select name="province" class="form-control prov" onchange="get_kota()">
                          <option>Select Province</option>
                        <?php
                          $get_pro = mysql_query("SELECT * FROM master_provinsi");
                          while ($do = mysql_fetch_array($get_pro)) {
                        ?>
                          <option value="<?php echo $do['provinsi_id'] ?>"><?php echo $do['provinsi_nama'] ?></option>
                        <?php
                          }
                        ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>City <font color="red">*</font></label>
                        <select name="city" class="form-control kota">
                          <option>Select City</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Districts <font color="red">*</font></label>
                        <select name="district" class="form-control keca">
                          <option>Select District</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Postal Code</label>
                        <input type="text" name="postal" id="postal" class="form-control" placeholder="your postal code..">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Address <font color="red">*</font></label>
                        <input type="text" name="address" id="address" class="form-control" placeholder="Address">
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Person in Charge <font color="red">*</font></label>
                        <input type="text" name="person" id="person" class="form-control" placeholder="">
                      </div>
                    </div>
                    <div class="col-md-12">
                      <button type="submit" class="btn btn-default" onclick="member_corp()"><i class="fa fa-save"></i> SAVE</button>
                      <button type="reset" class="btn btn-default"><i class="fa fa-refresh"></i> CLEAR</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="box collapsed-box">
            <div class="box-header">
              <b>CUSTOMER LIST CORPORATE</b>
              <div style="border:1px solid black;margin-bottom:0px;"></div>
                <div class="box-tools pull-right">
                  <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                  <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
              <div class="table-responsive">
                <table class="table table-bordered table-striped" id="example2">
                  <thead>
                    <tr>
                      <th class="text-center">No</th>
                      <th class="text-center">Member</th>
                      <th class="text-center">Name</th>
                      <th class="text-center">Province</th>
                      <th class="text-center">City</th>
                      <th class="text-center">District</th>
                      <th class="text-center">Email</th>
                      <th class="text-center">Phone</th>
                      <th class="text-center">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                      $no = 1;
                      $q = $this->db->query("SELECT * FROM tb_member_individu a, master_provinsi b, master_kokab c, master_kecam d
                                        WHERE a.province=b.provinsi_id AND a.city=c.kota_id AND a.district=d.kecam_id and a.kode='B'");
                      foreach ($q->result_array() as $i) {
                    ?>
                      <tr>
                        <td style="text-align:center"><?php echo $no++?></td>
                        <td><?php echo $i['id_member']?></td>
                        <td><?php echo $i['corp_name']?></td>
                        <td><?php echo $i['provinsi_nama']?></td>
                        <td><?php echo $i['kokab_nama']?></td>
                        <td><?php echo $i['nama_kecam']?></td>
                        <td><?php echo $i['email']?></td>
                        <td class="text-center">+62 <?php echo $i['phone']?></td>
                        <td class="text-right" style="width:150px;">
                          <a onclick="editcorpo('<?php echo $i['id_member'] ?>')" class="btn btn-warning"><i class="fa fa-edit"></i></a>
                          <a class="btn btn-danger" href="./reg_customer/delete/<?php echo $i['id_member'] ?>" onclick="return confirm('Apakah anda yakin akan menghapus data ini?')"><i class="fa fa-close"></i></a>
                        </td>
                      </tr>
                    <?php
                      }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->