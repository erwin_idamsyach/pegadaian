<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class corporate extends CI_Controller{
    function corporate(){
        parent::__construct();
		
		if(!isLogin()){
			goLogin();
		}
    }
    
    function index($text=NULL){
        $data["filelist"] = "corporate/corporate";
        $data["title"] = "Front";
        $data["title_menu"] = "Front";
        $data["menu"] = "corporate";
		
        getHTMLWeb($data);
    }

    public function edit_corpo($id)
    {
        $this->load->view('edit', array('id'=>$id));
    }

    public function edit_mem_corp($id)
    {
        $fir_name   = $this->input->post('fir_name');
        $person     = $this->input->post('person');
        $pic        = $this->input->post('pic');
        $phone      = $this->input->post('phone');
        $prov       = $this->input->post('prov');
        $kota       = $this->input->post('kota');
        $keca       = $this->input->post('keca');
        $postal     = $this->input->post('postal');
        $address    = $this->input->post('address');
        $email      = $this->input->post('email');
        date_default_timezone_set('Asia/Jakarta');
        $tgl = date('Y-m-d');

        $data = array(
            'corp_name'     => $fir_name,
            'corp_form'     => $pic,
            'corp_person'   => $person,
            'phone'         => $phone,
            'province'      => $prov,
            'city'          => $kota,
            'district'      => $keca,
            'postal_code'   => $postal,
            'address'       => $address,
            'email'         => $email,
        );
        $this->db->where('id_member', $id);
        $this->db->update('tb_member_individu', $data);

    }

    public function mem_corp()
    {
        $fir_name   = $this->input->post('fir_name');
        $person     = $this->input->post('person');
        $pic        = $this->input->post('pic');
        $phone      = $this->input->post('phone');
        $prov       = $this->input->post('prov');
        $kota       = $this->input->post('kota');
        $keca       = $this->input->post('keca');
        $postal     = $this->input->post('postal');
        $address    = $this->input->post('address');
        $email      = $this->input->post('email');
        date_default_timezone_set('Asia/Jakarta');
        $tgl = date('Y-m-d');

        $gid = $this->db->query("SELECT id_member FROM tb_member_individu where kode='B' ORDER BY id_member desc LIMIT 0,1");
        foreach ($gid->result_array() as $get) {
        }

        $id = $get['id_member'];
        $idd = substr($id, 1);
        $idd = $idd+1;
        $cek = strlen($idd);
        if ($cek < 2) {
            $idd = "B0000".$idd;
        }else{
            $idd = "B000".$idd;
        }

        $data = array(
            'id_member'     => $idd,
            'kode'          => 'B',
            'corp_name'     => $fir_name,
            'corp_form'     => $pic,
            'corp_person'   => $person,
            'phone'         => $phone,
            'province'      => $prov,
            'city'          => $kota,
            'district'      => $keca,
            'postal_code'   => $postal,
            'address'       => $address,
            'email'         => $email,
            'member_from'   => $tgl
        );
        $this->db->insert('tb_member_individu', $data);

    }
}
