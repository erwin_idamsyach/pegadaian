<?php
$key = str_replace('-', '/', $key);
$get_user = $this->db->query("SELECT * FROM tb_front_desk, tb_member WHERE tb_front_desk.id_member = tb_member.id_member AND tb_front_desk.id_order='$key' AND store='".sessionValue('kode_store')."' GROUP BY tb_front_desk.id_order");
?>
<div class="pull-left">
<table>
	<tr>
		<td style="padding-right: 15px;">No. FPJS</td>
		<td style="padding-right: 15px;padding-left: 15px;">:</td>
		<td style="padding-left: 15px;"><?php echo $key?></td>
	</tr>
	<?php
	foreach ($get_user->result() as $userData) {
		$new_ord = substr($userData->id_order, -20, 9);
		$new_ord = str_replace("/", "-", $new_ord);
		$barcode = $userData->id_member.$new_ord;
		?>
	<tr>
		<td style="padding-right: 15px;">Nama Pemohon</td>
		<td style="padding-right: 15px;padding-left: 15px;">:</td>
		<td style="padding-left: 15px;"><?php echo $userData->first_name." ".$userData->middle_name." ".$userData->last_name?></td>
	</tr>	
	<tr>
		<td style="padding-right: 15px;">Alamat</td>
		<td style="padding-right: 15px;padding-left: 15px;">:</td>
		<td style="padding-left: 15px;"><?php echo $userData->address?></td>
	</tr>
		<?php
	}
	?>
</table>
</div>
<div class="pull-right">
	<img height="50" src="<?php echo base_url() ?>asset/barcode-fpjs/<?php echo $barcode ?>.gif">
</div>
<br>
<table class="table table-bordered table-striped table-hover">
	<tr>
		<th class="text-center" width="7%">No</th>
		<th class="text-center" width="12%">ID Object</th>
		<th class="text-center" width="12%">Image</th>
		<th class="text-center" width="10%">Weight (mm)</th>
		<th class="text-center" width="10%">Length (mm)</th>
		<th class="text-center" width="10%">Width (mm)</th>
		<th class="text-center" width="10%">Height (mm)</th>
		<th class="text-center">Requested Service</th>
		<th class="text-center" width="25%">Action</th>
	</tr>
	<?php
	$no = 1;
	$get_data = $this->db->query("SELECT * FROM tb_front_desk WHERE
			tb_front_desk.id_order = '$key'  AND store='".sessionValue('kode_store')."' AND ISNULL(delete_by)");
	foreach ($get_data->result() as $get) {
		?>
	<tr>
		<td style="vertical-align: middle;"><?php echo $no++; ?></td>
		<td style="vertical-align: middle;"><?php echo $get->id_object ?></td>
		<td>
			<img src="<?php echo base_url() ?>asset/images/<?php echo $get->obj_image ?>" height="50">
		</td>
		<td style="vertical-align: middle;"><?php echo $get->obj_weight ?></td>
		<td style="vertical-align: middle;"><?php echo $get->obj_length ?></td>
		<td style="vertical-align: middle;"><?php echo $get->obj_width ?></td>
		<td style="vertical-align: middle;"><?php echo $get->obj_height ?></td>
		<td style="vertical-align: middle;"><?php
		if($get->certificate != ''){
			echo "FR ";
		}else{}

		if($get->gem_card != ''){
			echo "BR";
		}else{}

		if($get->dia_grading != ''){
			echo "DG";
		}else{}
		?></td>
		<td class="text-right" style="vertical-align: middle;">
			<a href="#" class="btn btn-info disabled" data-toggle="tooltip" data-placement="bottom" title="View"><i class="fa fa-eye"></i></a>

        	<a href="<?php echo base_url() ?>list_fpjs/editobject/<?php echo $get->id_object ?>" class="btn btn-warning" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="fa fa-edit"></i></a>

        	<a href="#" class="btn btn-danger" onclick="sweets('<?php echo $get->id_object ?>')" data-toggle="tooltip" data-placement="bottom" title="Delete"><i class="fa fa-trash"></i></a>

        	<a href="<?php echo site_url('list_fpjs/print_bps').'/'.$get->id_object ?>" class="btn btn-primary disabled" target="_blank" data-toggle="tooltip" data-placement="bottom" title="Print BPS"><i class="fa fa-print"></i></a>
		</td>
	</tr>
		<?php
	}
	?>
	<tr>
		<td colspan="8">
			<div style="margin-top: 10px;">
				<b>Note : CE = CERTIFICATE, GC = GEM CARD, DG = DIAMOND GRADING</b>
			</div>
		</td>
		<td class="pull-right">
			<a href="<?php $ord= str_replace('/', '-', $get->id_order);echo site_url('list_fpjs/print_bps').'/'.$ord ?>" class="btn btn-primary" target="_blank" data-toggle="tooltip" data-placement="bottom" title="Print BPS">Print BPS</a>
		</td>
	</tr>
</table>