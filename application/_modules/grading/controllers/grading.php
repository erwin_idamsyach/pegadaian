<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
* Controller Print Diamond Grading
* Create By : Erwin Idamsyach Putra
* 23 April 2016
*/
class grading extends CI_Controller{
	function __construct(){
		parent::__construct();

		if(!isLogin()){
			goLogin();
		}
	}
	
	public function index(){
		$data['filelist'] = 'grading/front';
		$data['title'] = 'Print Diamond Grading';
		$data['title_menu'] = 'Print';
		$data['menu'] = 'print-grading';

		getHTMLWeb($data);
	}

	public function print_grading(){
		$this->load->library('fpdf_rotation');
		
		$id_obj = $this->uri->segment(3);
		
		$getUrl = $this->db->query("SELECT url FROM tb_url WHERE ISNULL(delete_by)");
		$tmpUrl = $getUrl->result();
		
		$getNumNoCert = $this->db->query("SELECT COUNT(no_grading) no_grading, no_grading noCert FROM step WHERE id_object='".$id_obj."'");
		$tmpNumNoCert = $getNumNoCert->result_array();
		
		if($tmpNumNoCert[0]['no_grading']==0){		
			$getNoCert = $this->db->query("SELECT no_grading FROM step ORDER BY no_grading DESC LIMIT 0, 1");
			foreach ($getNoCert->result() as $no_cert) {
				$noCert = $no_cert->no_grading;
			}
			if(!isset($noCert)){
				$noCert = 0;
			}else{}
			$noCert = substr($noCert, 3);
			$noCert = $noCert+1;
			$getServices = $this->db->query("SELECT kode_produk FROM tb_service WHERE kode='DG' LIMIT 0, 1");
			$kode_produk = "";
			foreach ($getServices->result() as $tmpService) {
				$kode_produk = $tmpService->kode_produk;
			}
			$kode_produk = sessionValue('kode_unit')."-".date('y')."-".$kode_produk."-";
			$noCert = generateNoReport($kode_produk, $noCert);
			$this->db->where("id_object", $id_obj);
			$this->db->update("step", array("no_grading" => $noCert));
			
			$this->db->where('id_object', $id_obj);
			$this->db->update('step', array("print_grading"=>'Sudah', 'status_grading'=>'NOT APPROVED', 'status'=>'NOT APPROVED', 'update_by'  => sessionValue('id'), 'update_date' => date('Y-m-d H:i:s')));
			$noCert2 = $noCert;
			$noCert = $tmpUrl[0]->url."&no_sertifikat=".$noCert;
			setQarcode("barcode-fpjs", $noCert);
		}else{
			$noCert2 = $tmpNumNoCert[0]['noCert'];
			$noCert = $tmpUrl[0]->url."&no_sertifikat=".$tmpNumNoCert[0]['noCert'];
		}

		$pdf = new FPDF_Rotation();
		
		$get_data = $this->db->query("SELECT tb_lab_grading.*, tb_login.nama approve_name FROM tb_lab_grading
				LEFT JOIN step ON step.id_object = tb_lab_grading.id_object
				LEFT JOIN diamond_clarity ON diamond_clarity.clarity_grade = tb_lab_grading.clarity
				LEFT JOIN diamond_color ON diamond_color.color_code = tb_lab_grading.color_grading
				LEFT JOIN tb_login ON tb_login.id = step.approve_by
				WHERE 
				step.dia_grading != '' AND
				tb_lab_grading.id_object = '$id_obj'");
				
		$pos_y = array(45, 54, 63, 72, 81, 90, 99, 108, 117, 126, 135, 144);
		$pdf->SetAutoPageBreak(false, 0);
		$pdf->AddPage('L','A4');
		
		$pdf->AddFont('ronnia','','ronnia.php');
		$pdf->AddFont('ronnia','B','ronniab.php');
		$pdf->AddFont('ronnia','BI','ronniabi.php');
		$pdf->AddFont('ronnia','I','ronniai.php');
				
		foreach ($get_data->result() as $get) {
		$date = date_create($get->create_date);
		$day  = date_format($date, 'd');
		$month  = date_format($date, 'm');
		$year  = date_format($date, 'Y');
		
		$pdf->Image('./asset/images/Logo.png', 240, 5, 40, 26);
		/*$pdf->Image('asset/logo-pegadaian/logo-png-1.png', 225, 13, 50, 10);*/
		
		// Hadi
		$pdf->Image('asset/barcode-fpjs/'.$noCert.'.png', 15, 155, 25, 25);
		// End Hadi

		$pdf->SetDrawColor(130, 186, 83);
		$pdf->SetLineWidth(1);
		$pdf->Line(10, 33, 37, 33);
		
		$pdf->SetFont('ronnia','',12);
		$pdf->SetXY(1, 1);
		$pdf->Cell(0, 0, '-');
		//$pdf->SetFont('ronnia','',8);
		//$pdf->SetXY((290/2)+2, 1);
		//$pdf->Cell(0, 0, '|');
		//$pdf->SetXY((290/2)+2, 207);
		//$pdf->Cell(0, 0, '|');
		//$pdf->SetFont('ronnia','',16);
		$pdf->SetXY(292, 1);
		$pdf->Cell(0, 0, '-');
		//$pdf->SetXY(3, 205);
		//$pdf->Cell(0, 0, '+');
		//$pdf->SetXY(290, 205);
		//$pdf->Cell(0, 0, '+');

		$pdf->SetFont('ronnia','',16);
		$pdf->SetTextColor(16,95,62);

		$pdf->SetXY(37, 32);
		$pdf->Cell(0, 0, 'Diamond Grading Report');

		$pdf->Line(100, 33, 148, 33);
		$pdf->SetDrawColor(16,95,62);
		$pdf->Line(148, 33, 285, 33);

		$pdf->SetFont('ronnia','',13);
		$pdf->SetTextColor(0, 0, 0);

		$pdf->SetFont('ronnia','',13);
		$pdf->SetTextColor(16,95,62);
		$pdf->SetXY(10, $pos_y[0]);
		$pdf->Cell(0, 0, 'No. Laporan');
		$pdf->SetFont('ronnia','',9);
		$pdf->SetTextColor(72, 201, 84);
		$pdf->SetXY(10, $pos_y[0]+4);
		$pdf->Cell(0, 0, 'Report No');
		$pdf->SetXY(50, $pos_y[0]);
		$pdf->SetFont('ronnia','',13);
		$pdf->SetTextColor(0, 0, 0);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(55, $pos_y[0]);
		$pdf->Cell(0, 0, $noCert2);

		$pdf->SetFont('ronnia','',13);
		$pdf->SetTextColor(16,95,62);
		$pdf->SetXY(10, $pos_y[1]);
		$pdf->Cell(0, 0, 'Tanggal');
		$pdf->SetFont('ronnia','',9);
		$pdf->SetTextColor(72, 201, 84);
		$pdf->SetXY(10, $pos_y[1]+4);
		$pdf->Cell(0, 0, 'Date');
		$pdf->SetXY(50, $pos_y[1]);
		$pdf->SetFont('ronnia','',13);
		$pdf->SetTextColor(0, 0, 0);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(55, $pos_y[1]);
		$pdf->Cell(0, 0, $day.bulan($month)." ".$year);

		$pdf->SetFont('ronnia','',13);
		$pdf->SetTextColor(16,95,62);
		$pdf->SetXY(10, $pos_y[2]);
		$pdf->Cell(0, 0, 'Bentuk');
		$pdf->SetFont('ronnia','',9);
		$pdf->SetTextColor(72, 201, 84);
		$pdf->SetXY(10, $pos_y[2]+4);
		$pdf->Cell(0, 0, 'Shape');
		$pdf->SetXY(50, $pos_y[2]);
		$pdf->SetFont('ronnia','',13);
		$pdf->SetTextColor(0, 0, 0);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(55, $pos_y[2]);
		$pdf->Cell(0, 0, $get->shape);

		$pdf->SetFont('ronnia','',13);
		$pdf->SetTextColor(16,95,62);
		$pdf->SetXY(10, $pos_y[3]);
		$pdf->Cell(0, 0, 'Model Gosokan');
		$pdf->SetFont('ronnia','',9);
		$pdf->SetTextColor(72, 201, 84);
		$pdf->SetXY(10, $pos_y[3]+4);
		$pdf->Cell(0, 0, 'Cutting Style');
		$pdf->SetXY(50, $pos_y[3]);
		$pdf->SetFont('ronnia','',13);
		$pdf->SetTextColor(0, 0, 0);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(55, $pos_y[3]);
		$pdf->Cell(0, 0, $get->cut);

		$pdf->SetFont('ronnia','',13);
		$pdf->SetTextColor(16,95,62);
		$pdf->SetXY(10, $pos_y[4]);
		$pdf->Cell(0, 0, 'Ukuran');
		$pdf->SetFont('ronnia','',9);
		$pdf->SetTextColor(72, 201, 84);
		$pdf->SetXY(10, $pos_y[4]+4);
		$pdf->Cell(0, 0, 'Measurements');
		$pdf->SetXY(50, $pos_y[4]);
		$pdf->SetFont('ronnia','',13);
		$pdf->SetTextColor(0, 0, 0);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(55, $pos_y[4]);
		if($get->shape=="Round"){
			$pdf->Cell(0, 0, $get->obj_length." - ".$get->obj_width." x ".$get->obj_height." mm");
		}else{
			$pdf->Cell(0, 0, $get->obj_length." x ".$get->obj_width." x ".$get->obj_height." mm");
		}

		$pdf->SetFont('ronnia','',13);
		$pdf->SetTextColor(16,95,62);
		$pdf->SetXY(10, $pos_y[5]);
		$pdf->Cell(0, 0, 'Berat');
		$pdf->SetFont('ronnia','',9);
		$pdf->SetTextColor(72, 201, 84);
		$pdf->SetXY(10, $pos_y[5]+4);
		$pdf->Cell(0, 0, 'Carat Weight');
		$pdf->SetXY(50, $pos_y[5]);
		$pdf->SetFont('ronnia','',13);
		$pdf->SetTextColor(0, 0, 0);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(55, $pos_y[5]);
		$pdf->Cell(0, 0, $get->obj_weight." cts");

		$pdf->SetFont('ronnia','',13);
		$pdf->SetTextColor(16,95,62);
		$pdf->SetXY(10, $pos_y[6]);
		$pdf->Cell(0, 0, 'Warna');
		$pdf->SetFont('ronnia','',9);
		$pdf->SetTextColor(72, 201, 84);
		$pdf->SetXY(10, $pos_y[6]+4);
		$pdf->Cell(0, 0, 'Color');
		$pdf->SetXY(50, $pos_y[6]);
		$pdf->SetFont('ronnia','',13);
		$pdf->SetTextColor(0, 0, 0);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(55, $pos_y[6]);
		$pdf->Cell(0, 0, $get->color_grading); // ." (".$get->color_name.")"

		$pdf->SetFont('ronnia','',13);
		$pdf->SetTextColor(16,95,62);
		$pdf->SetXY(10, $pos_y[7]);
		$pdf->Cell(0, 0, 'Kejernihan');
		$pdf->SetFont('ronnia','',9);
		$pdf->SetTextColor(72, 201, 84);
		$pdf->SetXY(10, $pos_y[7]+4);
		$pdf->Cell(0, 0, 'Clarity');
		$pdf->SetXY(50, $pos_y[7]);
		$pdf->SetFont('ronnia','',13);
		$pdf->SetTextColor(0, 0, 0);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(55, $pos_y[7]);
		$pdf->Cell(0, 0, $get->clarity); // ." (".$get->inform.")"

		$pdf->SetFont('ronnia','',13);
		$pdf->SetTextColor(16,95,62);
		$pdf->SetXY(10, $pos_y[8]);
		$pdf->Cell(0, 0, 'Gosokan');
		$pdf->SetFont('ronnia','',9);
		$pdf->SetTextColor(72, 201, 84);
		$pdf->SetXY(10, $pos_y[8]+4);
		$pdf->Cell(0, 0, 'Cut');
		$pdf->SetXY(50, $pos_y[8]);
		$pdf->SetFont('ronnia','',13);
		$pdf->SetTextColor(0, 0, 0);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(55, $pos_y[8]);
		$pdf->Cell(0, 0, $get->cut_grade);

		$pdf->SetFont('ronnia','',13);
		$pdf->SetTextColor(16,95,62);
		$pdf->SetXY(10, $pos_y[9]);
		$pdf->Cell(0, 0, 'Girdle');
		$pdf->SetFont('ronnia','',9);
		$pdf->SetTextColor(72, 201, 84);
		$pdf->SetXY(10, $pos_y[9]+4);
		$pdf->Cell(0, 0, 'Girdle');
		$pdf->SetXY(50, $pos_y[9]);
		$pdf->SetFont('ronnia','',13);
		$pdf->SetTextColor(0, 0, 0);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(55, $pos_y[9]);
		$pdf->Cell(0, 0, $get->girdle);

		$pdf->SetFont('ronnia','',13);
		$pdf->SetTextColor(16,95,62);
		$pdf->SetXY(10, $pos_y[10]);
		$pdf->Cell(0, 0, 'Kulet');
		$pdf->SetFont('ronnia','',9);
		$pdf->SetTextColor(72, 201, 84);
		$pdf->SetXY(10, $pos_y[10]+4);
		$pdf->Cell(0, 0, 'Culet');
		$pdf->SetXY(50, $pos_y[10]);
		$pdf->SetFont('ronnia','',13);
		$pdf->SetTextColor(0, 0, 0);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(55, $pos_y[10]);
		$pdf->Cell(0, 0, $get->culet);

		$pdf->SetFont('ronnia','',13);
		$pdf->SetTextColor(16,95,62);
		$pdf->SetXY(10, $pos_y[11]);
		$pdf->Cell(0, 0, 'Fluoresen');
		$pdf->SetFont('ronnia','',9);
		$pdf->SetTextColor(72, 201, 84);
		$pdf->SetXY(10, $pos_y[11]+4);
		$pdf->Cell(0, 0, 'Fluorescence');
		$pdf->SetXY(50, $pos_y[11]);
		$pdf->SetFont('ronnia','',13);
		$pdf->SetTextColor(0, 0, 0);
		$pdf->Cell(0, 0, ':');
		$pdf->SetXY(55, $pos_y[11]);
		$pdf->Cell(0, 0, $get->fluorescence);
		if($get->obj_image!=""){
		$pdf->Image('asset/images/'.$get->obj_image, 187, 45, 25, 30);
		}
		$pdf->SetFont('ronnia','U',9);
		$pdf->SetXY(177, 75);
		$pdf->Cell(0, 0, 'Foto bukan ukuran sebenarnya');
		$pdf->SetFont('ronnia','',9);
		$pdf->SetXY(177, 78);
		$pdf->Cell(0, 0, 'Photo not the actual size');

		$pdf->SetFont('ronnia','',13);
		$pdf->SetTextColor(16,95,62);
		$pdf->SetXY(160, 85);
		$pdf->Cell(0, 0, 'Plotting');
		$pdf->SetFont('ronnia','',9);
		$pdf->SetTextColor(72, 201, 84);
		$pdf->SetXY(160, 89);
		$pdf->Cell(0, 0, 'Plotting');

		//$pdf->Image('asset/diamond-cut-sketch/'.$get->shape.'.jpg', 167, 90, 65, 30);

		$pdf->SetFont('ronnia','',13);
		$pdf->SetTextColor(16,95,62);
		$pdf->SetXY(160, 123);
		$pdf->Cell(0, 0, 'Simbol');
		$pdf->SetFont('ronnia','',9);
		$pdf->SetTextColor(72, 201, 84);
		$pdf->SetXY(160, 127);
		$pdf->Cell(0, 0, 'Key to symbol(s)');
		
		// Hadi
		$exp_key = explode(',', $get->key);
		if(count($exp_key)>0){
			$pos_x = 160;
			$y = 132;
			$a=0;
			for($i=0; $i<count($exp_key); $i++){
				if($i>=6){
					continue;
				}
				
				$symbol = $exp_key[$i];
				if($exp_key[$i]=="Included Crystal"){
					$symbol = "Crystal";
				}
				
				if($a==3){
					$pos_x = $pos_x + 27;
					$y = 132;
					$a=0;
				}
				$a++;
				
				$pdf->SetTextColor(0, 0, 0);
				$pdf->Image('asset/images-plot/'.strtolower($symbol).'.png', $pos_x, $y-4, 8, 8);
				$pdf->SetXY($pos_x+8, $y);
				$pdf->Cell(0, 0, $exp_key[$i]);
				//$pos_x = $pos_x+7;
				$y = $y+5;
			}
		}
		// End Hadi

		$pdf->SetFont('ronnia','',13);
		$pdf->SetTextColor(72, 201, 84);
		$pdf->SetXY(160, 147);
		$pdf->Cell(0, 0, ''); // Proporsi
		$pdf->SetFont('ronnia','',9);
		//$pdf->SetTextColor(240, 183, 62);
		$pdf->SetXY(160, 150);
		$pdf->Cell(0, 0, ''); // Proportions
		//$pdf->Image('asset/diamond-cut-sketch/diamond_proportions.jpg', 175, 155, 55, 30);
		
		$pdf->SetFont('ronnia','',4.9);
		$pdf->SetTextColor(0, 0, 0);
		$pdf->RotatedText(238,64,strtoupper('Colorless'),270);
		$pdf->RotatedText(238,80,strtoupper('Near Colorless'),270);
		$pdf->RotatedText(238,102,strtoupper('Faint'),270);
		$pdf->RotatedText(238,120,strtoupper('Very Light'),270);
		$pdf->RotatedText(238,154,strtoupper('Light'),270);
		
		$pdf->RotatedText(263,77,strtoupper('Very-very'),270);
		$pdf->RotatedText(261,77.4,strtoupper('Slightly'),270);
		$pdf->RotatedText(259,77.2,strtoupper('Included'),270);
		$pdf->RotatedText(263,90,strtoupper('Very'),270);
		$pdf->RotatedText(261,88.8,strtoupper('Slightly'),270);
		$pdf->RotatedText(259,88.6,strtoupper('Included'),270);
		$pdf->RotatedText(263,98.1,strtoupper('Slightly'),270);
		$pdf->RotatedText(261,97.9,strtoupper('Included'),270);
		$pdf->RotatedText(263,110,strtoupper('Included'),270);
		
		$pdf->SetFont('ronnia','',9);
		$pdf->SetDrawColor(208, 208, 208);
		$pdf->SetLineWidth(0);
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetXY(240, 47);
		$pdf->Cell(13, 5, "COLOR", 0, 0);
		$pdf->SetXY(240, 52);
		$pdf->Cell(13, 5, "GRADING", 0, 0);
		$pdf->SetXY(240, 57);
		$pdf->Cell(13, 5, "SCALE", 0, 0);

		
		
		$pdf->SetDrawColor(0, 0, 0);
		$pdf->SetXY(240, 62);
		$pdf->Cell(13, 5, "D", "LTB", 1, "C");
		$pdf->SetXY(240, 67);
		$pdf->Cell(13, 5, "E", "LB", 1, "C");
		$pdf->SetXY(240, 72);
		$pdf->Cell(13, 5.3, "F", "LB", 1, "C");

		$pdf->SetLineWidth(0);
		$pdf->Line(240, 77, 253, 77);
		$pdf->SetLineWidth(0);

		$pdf->SetXY(240, 77);
		$pdf->Cell(13, 5, "G", "LB", 1, "C");
		$pdf->SetXY(240, 82);
		$pdf->Cell(13, 5, "H", "LB", 1, "C");
		$pdf->SetXY(240, 87);
		$pdf->Cell(13, 5, "I", "LB", 1, "C");
		$pdf->SetXY(240, 92);
		$pdf->Cell(13, 5.3, "J", "LB", 1, "C");

		$pdf->SetLineWidth(0);
		$pdf->Line(240, 97, 253, 97);
		$pdf->SetLineWidth(0);

		$pdf->SetXY(240, 97);
		$pdf->Cell(13, 5, "K", "LB", 1, "C");
		$pdf->SetXY(240, 102);
		$pdf->Cell(13, 5, "L", "LB", 1, "C");
		$pdf->SetXY(240, 107);
		$pdf->Cell(13, 5.3, "M", "LB", 1, "C");

		$pdf->SetLineWidth(0);
		$pdf->Line(240, 112, 253, 112);
		$pdf->SetLineWidth(0);

		$pdf->SetXY(240, 112);
		$pdf->Cell(13, 5, "N", "LB", 1, "C");
		$pdf->SetXY(240, 117);
		$pdf->Cell(13, 5, "O", "LB", 1, "C");
		$pdf->SetXY(240, 122);
		$pdf->Cell(13, 5, "P","LB", 1, "C");
		$pdf->SetXY(240, 127);
		$pdf->Cell(13, 5, "Q", "LB", 1, "C");
		$pdf->SetXY(240, 132);
		$pdf->Cell(13, 5.3, "R", "LB", 1, "C");

		$pdf->SetLineWidth(0);
		$pdf->Line(240, 137, 253, 137);
		$pdf->SetLineWidth(0);

		$pdf->SetXY(240, 137);
		$pdf->Cell(13, 5, "S", "LB", 1, "C");
		$pdf->SetXY(240, 142);
		$pdf->Cell(13, 5, "T", "LB", 1, "C");
		$pdf->SetXY(240, 147);
		$pdf->Cell(13, 5, "U", "LB", 1, "C");
		$pdf->SetXY(240, 152);
		$pdf->Cell(13, 5, "V", "LB", 1, "C");
		$pdf->SetXY(240, 157);
		$pdf->Cell(13, 5, "W", "LB", 1, "C");
		$pdf->SetXY(240, 162);
		$pdf->Cell(13, 5, "X", "LB", 1, "C");
		$pdf->SetXY(240, 167);
		$pdf->Cell(13, 5, "Y", "LB", 1, "C");
		$pdf->SetXY(240, 172);
		$pdf->Cell(13, 5, "Z", "LB", 1, "C");

		$pdf->SetFont('ronnia','',9);
		$pdf->SetDrawColor(208, 208, 208);
		$pdf->SetLineWidth(0);
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetXY(265, 47);
		$pdf->Cell(20, 5, "CLARITY", 0, 0);
		$pdf->SetXY(265, 52);
		$pdf->Cell(20, 5, "GRADING", 0, 0);
		$pdf->SetXY(265, 57);
		$pdf->Cell(20, 5, "SCALE", 0, 0);

		$pdf->SetDrawColor(0, 0, 0);
		$pdf->SetXY(265, 62);
		$pdf->Cell(20, 5, "FLAWLESS", "LBT", "", "C");
		$pdf->SetXY(265, 67);
		$pdf->Cell(20, 5, "INTERNALLY", "L", "", "C");

		//$pdf->SetDrawColor(208, 208, 208);
		//$pdf->Line(265, 72, 285, 72);
		//$pdf->SetDrawColor(0, 0, 0);

		$pdf->SetXY(265, 72);
		$pdf->Cell(20, 5, "FLAWLESS", "LB", "", "C");
		$pdf->SetXY(265, 77);
		$pdf->Cell(20, 5, "VVS1", "L", 1, "C");

		$pdf->SetDrawColor(208, 208, 208);
		$pdf->Line(265, 82, 285, 82);
		$pdf->SetDrawColor(0, 0, 0);

		$pdf->SetXY(265, 82);
		$pdf->Cell(20, 5, "VVS2", "LB", 1, "C");
		$pdf->SetXY(265, 87);
		$pdf->Cell(20, 5, "VS1", "L", 1, "C");

		$pdf->SetDrawColor(208, 208, 208);
		$pdf->Line(265, 92, 285, 92);
		$pdf->SetDrawColor(0, 0, 0);

		$pdf->SetXY(265, 92);
		$pdf->Cell(20, 5, "VS2", "LB", 1, "C");
		$pdf->SetXY(265, 97);
		$pdf->Cell(20, 5, "SI1", "L", 1, "C");

		$pdf->SetDrawColor(208, 208, 208);
		$pdf->Line(265, 102, 285, 102);
		$pdf->SetDrawColor(0, 0, 0);

		$pdf->SetXY(265, 102);
		$pdf->Cell(20, 5, "SI2", "LB", 1, "C");
		$pdf->SetXY(265, 107);
		$pdf->Cell(20, 5, "I1", "L", 1, "C");

		$pdf->SetDrawColor(208, 208, 208);
		$pdf->Line(265, 112, 285, 112);
		$pdf->SetDrawColor(0, 0, 0);

		$pdf->SetXY(265, 112);
		$pdf->Cell(20, 5, "I2", "L", 1, "C");

		$pdf->SetDrawColor(208, 208, 208);
		$pdf->Line(265, 117, 285, 117);
		$pdf->SetDrawColor(0, 0, 0);

		$pdf->SetXY(265, 117);
		$pdf->Cell(20, 5, "I3", "LB", 1, "C");

		$pdf->SetDrawColor(208, 208, 208);
		$pdf->SetLineWidth(0);
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetXY(265, 127);
		$pdf->Cell(20, 5, "", 0, 0);
		$pdf->SetXY(265, 127.5);
		$pdf->Cell(20, 5, "CUT", 0, 0);
		$pdf->SetXY(265, 132.5);
		$pdf->Cell(20, 5, "SCALE", 0, 0);

		$pdf->SetDrawColor(0, 0, 0);
		
		$y_cut = 137.5;
		$height_cut = 8;
		
		$pdf->SetXY(265, $y_cut);
		$pdf->Cell(20, $height_cut, "EXCELLENT", "LTB", 0, "C");
		$y_cut += 8;
		$pdf->SetXY(265, $y_cut);
		$pdf->Cell(20, $height_cut, "VERY GOOD", "LB", 0, "C");

		//$pdf->SetDrawColor(208, 208, 208);
		//$pdf->Line(265, 152, 285, 152);
		//$pdf->SetDrawColor(0, 0, 0);

		//$pdf->SetXY(265, 162);
		//$pdf->Cell(20, 5, "GOOD", "LB", 0, "C");
		$y_cut += 8;
		$pdf->SetXY(265, $y_cut);
		$pdf->Cell(20, $height_cut, "GOOD", "LB", 0, "C");
		$y_cut += 8;
		$pdf->SetXY(265, $y_cut);
		$pdf->Cell(20, $height_cut, "FAIR", "LB", 0, "C");
		$y_cut += 8;
		$pdf->SetXY(265, $y_cut);
		$pdf->Cell(20, $height_cut, "POOR", "LB", 0, "C");
		
		$pdf->SetFont("ronnia","", 12);
		$pdf->SetXY(65, 173);
		$pdf->Cell(65, 0, ucwords($get->approve_name), 0, 0, "C"); // $get->gemolog
		$pdf->Line(70, 175, 120, 175);
		$pdf->SetXY(65, 178);
		$pdf->Cell(65, 0, "Accredited Gemologist", 0, 0, "C");

		$pdf->SetFont("ronnia","", 8);
		//$pdf->SetTextColor(37, 156, 10);
		$pdf->SetTextColor(16,95,62);
		$pdf->SetDrawColor(16,95,62);
		$pdf->SetFillColor(255, 255, 255);

		$pdf->SetXY(10, 192);
		$pdf->Cell(130, 0, "Semua informasi yang diberikan merupakan pendapat dari Laboratorium Pegadaian Gemologi pada saat", 0, 0, "C", false);
		$pdf->SetXY(11, 195);
		$pdf->Cell(130, 0, "pemeriksaan. Tidak berlaku apabila selanjutnya terdapat modifikasi terhadap barang yang telah diidentifikasi.", 0, 0, "C", false);
		$pdf->Line(7, 196.7, 144, 196);

		//$pdf->SetTextColor(240, 183, 62);
		$pdf->SetTextColor(72, 201, 84);
		$pdf->SetFillColor(255, 255, 255);

		$pdf->SetXY(10, 198.5);
		$pdf->Cell(130, 0, "All information given represents the opinion of Pegadaian Gemological Laboratory at the time of the testing.", 0, 0, "C", false);
		$pdf->SetXY(11, 201.5);
		$pdf->Cell(130, 0, "It does not take into account of any subsequent modification of the item tested", 0, 0, "C", false);

		//$pdf->Image("asset/logo-pegadaian/Logo.png", 160, 188, 33, 15);
		$store = "";
		$lokasi = sessionValue('alamat');
		$hunting = " ";
		//if(sessionValue('flag_store')=="Y"){
		//	$store = "- ".ucfirst(sessionValue('nama_store'));
		//}
		if(sessionValue('lokasi')=="PUSAT"){
			$hunting = " (Hunting) ".ucfirst(sessionValue('store'));
			$lokasi = "Kantor ".ucfirst(strtolower(sessionValue('lokasi')))." PT Pegadaian (Persero) ".sessionValue('alamat');
		}else{
			$store = "- ".ucfirst(sessionValue('nama_store'));
		}
		
		$pdf->SetFont("ronnia","B",8);
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetXY(170, 192);
		$pdf->Cell(0, 0, "Laboratorium Gemologi Pegadaian ".$store);
		$pdf->SetXY(170, 195);
		$pdf->Cell(0, 0, $lokasi); // Jl. Kramat Raya 162 Jakarta Pusat 10430 Indonesia
		$pdf->SetFont("ronnia","B",7);
		$pdf->SetXY(170, 198);
		$pdf->Cell(0, 0, 'T +62 '.sessionValue('telepon').$hunting.' Fax +62 '.sessionValue('fax').'  '.$tmpUrl[0]->url); // T +62 21 315 5550 ext. 171 Fax +62 21 3983 8014
		}
		
		$pdf->AddPage('L','A4');
		
		$width = array(115,5,110,35);
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetFont('ronnia','',10);
		
		$pdf->SetTopMargin(15);
		$pdf->SetLeftMargin(15);
		$pdf->Cell($width[0], 5, 'SYARAT DAN KETENTUAN',0,0,'C');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[0], 5, 'TERMS AND CONDITION',0,0,'C');
		$pdf->Ln(7);
		
		$pdf->Cell($width[1], 5, '1.',0,0,'L');
		$pdf->Cell($width[2], 5, 'PT PEGADAIAN (Persero) selanjutnya disebut Laboratorium',0,0,'FJ');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '1.',0,0,'L');
		$pdf->Cell($width[2], 5, 'PT PEGADAIAN (Persero) hereinafter reffered to as Pegadaian',0,0,'FJ');
		$pdf->Ln();		
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2]-17, 5, 'Gemologi Pegadaian di dalam Laporan Tingkatan Berlian',0,0,'L');
		$pdf->SetFont('ronnia','I',10);
		$pdf->Cell(17, 5, '(Diamond ',0,0,'FJ');
		$pdf->SetFont('ronnia','',10);
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'Gemological Laboratory in this Diamond Grading Report.',0,0,'FJ');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->SetFont('ronnia','I',10);
		$pdf->Cell(16, 5, 'Grading) ',0,0,'L');
		$pdf->SetFont('ronnia','',10);
		$pdf->Cell($width[2]-16, 5, 'ini.',0,0,'L');
		
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '2.',0,0,'L');
		$pdf->Cell($width[2], 5, 'This Diamond Grading Report  is not an appraising or a transaction',0,0,'FJ');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '2.',0,0,'L');
		$pdf->Cell($width[2]-69, 5, 'Laporan Tingkatan Berlian',0,0,'L');
		$pdf->SetFont('ronnia','I',10);
		$hasil = $width[2]-69;
		$pdf->Cell(31, 5, ' (Diamond Grading) ',0,0,'L');
		$pdf->SetFont('ronnia','',10);
		$pdf->Cell($width[2]-$hasil-31, 5, 'ini bukan merupakan',0,0,'FJ');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'recommendation.',0,0,'L');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'taksiran harga ataupun rekomendasi transaksi.',0,0,'L');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '3.',0,0,'L');
		$pdf->Cell($width[2], 5, 'This Diamond Grading Report is made by customer request at the',0,0,'FJ');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '3.',0,0,'L');
		$pdf->Cell($width[2]-69, 5, 'Laporan Tingkatan Berlian',0,0,'L');
		$pdf->SetFont('ronnia','I',10);
		$hasil = $width[2]-69;
		$pdf->Cell(31, 5, ' (Diamond Grading) ',0,0,'L');
		$pdf->SetFont('ronnia','',10);
		$pdf->Cell($width[2]-$hasil-31, 5, 'ini dibuat atas,',0,0,'FJ');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'time of the testing, represents specification as detailed in this Report',0,0,'FJ');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'permintaan nasabah berisi spesifikasi sebagaimana terinci di dalam',0,0,'FJ');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'based upon the application and identification techniques using',0,0,'FJ');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'laporan ini setelah melalui pemeriksaan dan pengujian standar ilmu',0,0,'FJ');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'standard gemological equipment and other necessary equipment.',0,0,'L');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'gemologi dengan menggunakan peralatan standar gemologi dan',0,0,'FJ');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '4.',0,0,'L');
		$pdf->Cell($width[2], 5, 'Customer agrees and accepts the result of inspections and tests',0,0,'FJ');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'peralatan lain yang diperlukan.',0,0,'L');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'performed by Pegadaian Gemological Laboratory, either of the',0,0,'FJ');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '4.',0,0,'L');
		$pdf->Cell($width[2], 5, 'Nasabah setuju dan menerima hasil pemeriksaan dan pengujian',0,0,'FJ');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'methods, testing standards, terms and criteria used in this report.',0,0,'L');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'yang dilakukan oleh Laboratorium Gemologi Pegadaian, baik',0,0,'FJ');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '5.',0,0,'L');
		$pdf->Cell($width[2], 5, 'This Diamond Grading Report may not be reproduced in whole or in',0,0,'FJ');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'terhadap metode, standar pengujian, istilah dan kriteria yang',0,0,'FJ');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'part without written authorization from Pegadaian Gemological',0,0,'FJ');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2]-17, 5, 'digunakan dalam pembuatan Laporan Tingkatan Berlian',0,0,'L');
		$pdf->SetFont('ronnia','I',10);
		$pdf->Cell(17, 5, '(Diamond ',0,0,'FJ');
		$pdf->SetFont('ronnia','',10);
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'Laboratory.',0,0,'L');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->SetFont('ronnia','I',10);
		$pdf->Cell(17, 5, 'Grading) ',0,0,'L');
		$pdf->SetFont('ronnia','',10);
		$pdf->Cell($width[2]-17, 5, 'ini.',0,0,'L');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '6.',0,0,'L');
		$pdf->Cell($width[2], 5, 'Pegadaian Gemological Laboratory are not responsible for any',0,0,'FJ');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '5.',0,0,'L');
		$pdf->Cell($width[2], 5, 'Laporan Tingkatan Berlian ini tidak dapat digandakan baik sebagian',0,0,'FJ');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'possibility of misidentification as set forth in this Diamond Grading',0,0,'FJ');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'ataupun seluruhnya tanpa izin tertulis dari Laboratorium Gemologi',0,0,'FJ');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'Report, or misuse of this Diamond Grading Report which resulted in',0,0,'FJ');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'Pegadaian.',0,0,'L');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'the loss of customers or other parties.',0,0,'L');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '6.',0,0,'L');
		$pdf->Cell($width[2], 5, 'Laboratorium Gemologi Pegadaian tidak bertanggung jawab atas',0,0,'FJ');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '7.',0,0,'L');
		$pdf->Cell($width[2], 5, 'Pegadaian Gemological Laboratory are not responsible for any',0,0,'FJ');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'kemungkinan adanya segala kesalahan identifikasi sebagaimana',0,0,'FJ');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'damage and changes that may occur during the process of',0,0,'FJ');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2]-46, 5, 'tertera dalam Laporan Tingkatan Berlian',0,0,'L');
		$pdf->SetFont('ronnia','I',10);
		$hasil = $width[2]-46;
		$pdf->Cell(31, 5, ' (Diamond Grading) ',0,0,'L');
		$pdf->SetFont('ronnia','',10);
		$pdf->Cell($width[2]-$hasil-31, 5, 'ataupun',0,0,'L');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'examination and testing, customer agrees to release Pegadaian',0,0,'FJ');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2]-41, 5, 'penyalahgunaan Laporan Tingkatan Berlian',0,0,'L');
		$pdf->SetFont('ronnia','I',10);
		$hasil = $width[2]-41;
		$pdf->Cell(31, 5, ' (Diamond Grading) ',0,0,'L');
		$pdf->SetFont('ronnia','',10);
		$pdf->Cell($width[2]-$hasil-31, 5, 'yang',0,0,'L');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'Gemological Laboratory for any lawsuits.',0,0,'L');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'berakibat kerugian pada nasabah ataupun pihak lainnya.',0,0,'L');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '8.',0,0,'L');
		$pdf->Cell($width[2], 5, 'All expenses related to the making of this report by Pegadaian',0,0,'FJ');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '7.',0,0,'L');
		$pdf->Cell($width[2], 5, 'Laboratorium Gemologi Pegadaian tidak bertanggung jawab atas',0,0,'FJ');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'Gemological Laboratory is the responsibility of the customer.',0,0,'L');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'segala kerusakan dan perubahan yang mungkin terjadi pada saat',0,0,'FJ');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '9.',0,0,'L');
		$pdf->Cell($width[2], 5, 'This Diamond Grading Report can not be and are not for sale.',0,0,'L');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'proses pemeriksaan dan pengujian, nasabah setuju untuk',0,0,'FJ');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, '',0,0,'L');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'melepaskan Laboratorium Gemologi Pegadaian atas segala',0,0,'FJ');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, '',0,0,'L');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'tuntutan hukum.',0,0,'L');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, '',0,0,'L');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '8.',0,0,'L');
		$pdf->Cell($width[2], 5, 'Segala biaya yang timbul atas pembuatan Laporan Tingkatan',0,0,'FJ');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, '',0,0,'L');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2]-93, 5, 'Berlian',0,0,'L');		
		$pdf->SetFont('ronnia','I',10);
		$hasil = $width[2]-93;
		$pdf->Cell(31, 5, ' (Diamond Grading) ',0,0,'L');
		$pdf->SetFont('ronnia','',10);
		$pdf->Cell($width[2]-$hasil-31, 5, 'oleh Laboratorium Gemologi Pegadaian',0,0,'L');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, '',0,0,'L');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'merupakan tanggung jawab nasabah.',0,0,'L');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, '',0,0,'L');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '9.',0,0,'L');
		$pdf->Cell($width[2]-69, 5, 'Laporan Tingkatan Berlian',0,0,'L');
		$pdf->SetFont('ronnia','I',10);
		$hasil = $width[2]-69;
		$pdf->Cell(31, 5, ' (Diamond Grading) ',0,0,'L');
		$pdf->SetFont('ronnia','',10);
		$pdf->Cell($width[2]-$hasil-31, 5, 'ini tidak dapat dan',0,0,'FJ');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, '',0,0,'L');
		$pdf->Ln();
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, 'tidak untuk diperjualbelikan.',0,0,'L');
		$pdf->Cell($width[3], 5, '',0,0,'L');
		$pdf->Cell($width[1], 5, '',0,0,'L');
		$pdf->Cell($width[2], 5, '',0,0,'L');
		$pdf->Ln(7);
		
		$pdf->SetFont('ronnia','',10);
		$pdf->Cell($width[0]+$width[0]+$width[3], 5, 'LABOTARIUM GEMOLOGI PEGADAIAN '.strtoupper($store),0,0,'C');
		$pdf->Ln();
		$pdf->Cell($width[0]+$width[0]+$width[3], 5, $lokasi,0,0,'C');
		$pdf->Ln();
		$pdf->Cell($width[0]+$width[0]+$width[3], 5, 'Telp. +62 '.sessionValue('telepon').$hunting.'Fax. +62 '.sessionValue('fax').'   '.$tmpUrl[0]->url,0,0,'C');
		$pdf->Ln();
		
		/*$pdf->SetFont('ronnia','',16);
		$pdf->SetXY(3, 3);
		$pdf->Cell(0, 0, '+');
		//$pdf->SetXY(290/2, 3);
		//$pdf->Cell(0, 0, '|');
		$pdf->SetXY(290, 3);
		$pdf->Cell(0, 0, '+');
		$pdf->SetXY(3, 205);
		$pdf->Cell(0, 0, '+');
		$pdf->SetXY(290, 205);
		$pdf->Cell(0, 0, '+');*/
		
		$pdf->Output();
	}
}
?>