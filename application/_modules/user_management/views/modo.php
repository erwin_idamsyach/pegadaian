<?php
$get_user = $this->db->query("SELECT
                                  tb_login.id, tb_login.foto,
                                  tb_login.email,tb_login.nama,
                                  tb_login.phone, tb_login.access_level,
                                  tb_store.store
                                  FROM 
                                  tb_login, tb_store
                                  WHERE
                                  tb_login.store = tb_store.kode AND
                                  tb_login.id='$id'");
foreach ($get_user->result() as $get) {
	?>
<div class="row">
	<div class="col-md-5 col-sm-6">
		<?php
		if($get->foto == ''){
			?>
			<img src="<?php echo base_url() ?>asset/foto-petugas/noimage.png" class="img-responsive" style="height: 180px">
			<?php
		}else{
			?>
			<img src="<?php echo base_url() ?>asset/foto-petugas/<?php echo $get->foto ?>" class="img-responsive">
			<?php
		}
		?>
	</div>
	<div class="col-md-7 col-sm-6">
		<table class="table">
			<tr>
				<td>Name</td>
				<td>:</td>
				<td><?php echo $get->nama ?></td>
			</tr>
			<tr>
				<td>Phone</td>
				<td>:</td>
				<td><?php echo $get->phone ?></td>
			</tr>
			<tr>
				<td>Email</td>
				<td>:</td>
				<td><?php echo $get->email ?></td>
			</tr>
			<tr>
				<td>Access Level</td>
				<td>:</td>
				<td><?php echo $get->access_level ?></td>
			</tr>
			<tr>
				<td>Store</td>
				<td>:</td>
				<td><?php echo $get->store ?></td>
			</tr>

		</table>
	</div>
</div>
	<?php
}
?>