<?php


class approve extends CI_Controller{
	function __construct(){
		parent::__construct();

		if(!isLogin()){
			goLogin();
		}
	}
	
	function index(){
		redirect(base_url());
	}
	function sertifikasi(){
		$data['filelist'] = 'approve/sertifikasi';
		$data['title'] = 'Approval';
		$data['title_menu'] = 'Lab';
		$data['menu'] = 'app_sertifikasi';

		getHTMLWeb($data);
	}
	
	function view_approve_sertifikasi($id)
    {
        $this->load->view('view_sertifikat', array('id'=>$id));
    }

	function approve_sertifikasi(){
		$id = $this->input->post('id_step');
		$data = array(
			'status' => 'APPROVED',
            'update_by' => sessionValue('id'),
            'update_date'=> date('Y-m-d H:i:s'),
			'approve_by' => sessionValue('id'),
            'approve_date'=> date('Y-m-d H:i:s')
			);
		
		$success = false;
		$msg = "";
		$this->db->where('id', $id);
		if($this->db->update('step', $data)){
			$success = true;
		}else{
			$msg = "Gagal approve data";
		}
		
		echo json_encode(array("success"=>$success, "msg"=>$msg));
		
		//redirect(site_url('approve/sertifikasi'));
	}

	function grading(){
		$data['filelist'] = 'approve/grading';
		$data['title'] = 'Approval';
		$data['title_menu'] = 'Lab';
		$data['menu'] = 'app_grading';

		getHTMLWeb($data);
	}
	
	function view_approve_grading($id)
    {
        $this->load->view('view_grading', array('id'=>$id));
    }

	function approve_grading(){
		$id = $this->input->post('id_step');
		$data = array(
			'status_grading' => 'APPROVED',
            'update_by' => sessionValue('id'),
            'update_date'=> date('Y-m-d H:i:s'),
			'approve_by' => sessionValue('id'),
            'approve_date'=> date('Y-m-d H:i:s')
			);
		
		$success = false;
		$msg = "";
		$this->db->where('id', $id);
		if($this->db->update('step', $data)){
			$success = true;
		}else{
			$msg = "Gagal approve data";
		}
		
		echo json_encode(array("success"=>$success, "msg"=>$msg));
			
		//redirect(site_url('approve/grading'));
	}

	function taksiran(){
		$data['filelist'] = 'approve/taksiran_front';
		$data['title'] = 'Approval';
		$data['title_menu'] = 'Lab';
		$data['menu'] = 'app_taksiran';

		getHTMLWeb($data);
	}

	function taksiran_detail($id_order=NULL){
		$id_order = str_replace('-','/',$id_order);
        $data["id_order"] = $id_order;
		
		$data['filelist'] = 'approve/taksiran';
		$data['title'] = 'Approval';
		$data['title_menu'] = 'Lab';
		$data['menu'] = 'app_taksiran';

		getHTMLWeb($data);
	}
	
	function view_approve_taksiran($id, $id_order)
    {
        $this->load->view('view_taksiran', array('id'=>$id, 'id_order'=>$id_order));
    }

	function approve_taksiran(){
		$id = $this->input->post('id_step');
		$data = array(
			'status' => 'APPROVED',
            'update_by' => sessionValue('id'),
            'update_date'=> date('Y-m-d H:i:s'),
			'approve_by' => sessionValue('id'),
            'approve_date'=> date('Y-m-d H:i:s')
			);
		
		$success = false;
		$msg = "";
		$this->db->where('id', $id);
		if($this->db->update('step', $data)){
			$success = true;
		}else{
			$msg = "Gagal approve data";
		}
		
		echo json_encode(array("success"=>$success, "msg"=>$msg));
		
		//redirect(site_url('approve/taksiran'));
	}
}
?>