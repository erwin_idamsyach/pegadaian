      
      <div class="index">
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>&nbsp;</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-user"></i> Home</a></li>
            <li>Daftar Persetujuan</li>
            <li class="active">Taksiran</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="box">
            <div class="box-header">
              <h5><b>DAFTAR PERSETUJUAN TAKSIRAN</b></h5>
              <hr style="border:1px solid black;margin-bottom:-10px;margin-top:-5px;">
              <br>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><span class="fa fa-minus"></span></button>
                <button class="btn btn-box-tool" data-widget="remove"><span class="fa fa-times"></span></button>
              </div>
            </div><!-- /.box-header -->
            <div class="box-body" style="margin-top:-15px;">
              <?php
                date_default_timezone_set('Asia/Jakarta');
                $tgl = date('Y-m-d');
                $no = 1;
                $per_page = 5;
                $kode = sessionValue('kode_store');
                $u = $this->db->query("SELECT tb_taksiran.id, master_jenis_permata.jenis, tb_taksiran.berat_bersih, tb_taksiran.berat_kotor, tb_taksiran.id_order, tb_taksiran.jumlah_permata, tb_fpjt.create_date, tb_member.first_name nama 
									FROM tb_taksiran
									LEFT JOIN tb_fpjt ON tb_fpjt.id = tb_taksiran.id
									LEFT JOIN master_jenis_permata ON master_jenis_permata.id = tb_taksiran.jenis_perhiasan
									LEFT JOIN step ON step.id_object = tb_fpjt.id
									LEFT JOIN tb_member ON tb_member.id_member = tb_taksiran.id_member
									WHERE tb_taksiran.jenis_perhiasan = master_jenis_permata.id AND tb_fpjt.status='SUDAH' 
									AND step.status='NOT APPROVED' AND ISNULL(step.delete_by) 
									AND step.status_barang='TIDAK TERTINGGAL'
									GROUP BY tb_taksiran.id_order");
              ?>
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th class="text-center" width="10%">NO</th>
                      <th class="text-center" width="17%">DATE</th>
                      <th class="text-center" width="20%">NO ORDER</th>
                      <th class="text-center">NASABAH</th>
                      <th class="text-center" width="10%">ACTION</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php
                    foreach ($u->result_array() as $i) {
                    $tglx = date('H:i A', strtotime($i['create_date']));
					$id_order = str_replace('/','-',$i['id_order']);
                  ?>
                    <tr>
                      <td style="text-align:center"><?php echo $no++?></td>
                      <td class="text-center"><?php echo $tglx ?></td>
                      <td><?php echo $i['id_order'] ?></td>
                      <td><?php echo $i['nama'] ?></td>
                      <td style="text-align:right;">
                        <a href="<?php echo site_url('approve/taksiran_detail/'.$id_order); ?>" class="btn btn-info" data-toggle="tooltip" data-placement="bottom" title="View Object"><i class="fa fa-eye"></i></a>
                      </td>
                    </tr>
                    <?php
                      }
                    ?>
                  </tbody>
                </table>
              </div>
            </div><!-- /.box-body -->
          </section><!-- /.content -->
        </div><!-- /.content-wrapper -->
      </div>
      <script>
        $("#example1").DataTable();
      </script>