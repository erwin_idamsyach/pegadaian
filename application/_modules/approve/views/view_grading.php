      <script type="text/javascript">
        jQuery(function($){
		    $('#form-approve-grading').trigger("reset");
			$("#form-approve-grading").on('submit',function(e){
			e.preventDefault();
			var formatData = new FormData($(this)[0]);
				swal({
				 title: "",
				 text: "Apakah Anda akan meng-approve data?",
				 type: "warning",
				 showCancelButton: true,
				 confirmButtonColor: "#DD6B55",
				 confirmButtonText: "Ya",
				 cancelButtonText: "Tidak",
				 closeOnConfirm: false }, function(){
					$.ajax({
						type : 'POST',
						url : '<?php echo base_url()?>approve/approve_grading',
						data : formatData,
						async: false,
						cache: false,
						contentType: false,
						processData: false,
						success:function(html){
							//window.location.reload();
							var data = eval ("(" + html + ")");
							if(data.success){
								swal({
								 title: "Berhasil approve data!",
								 text: "Klik tombol di bawah.",
								 type: "success",
								 showCancelButton: false,
								 confirmButtonColor: "#257DB6",
								 confirmButtonText: "Ok!",
								 closeOnConfirm: false
								},
								function(){
									location.href = base_url+"approve/grading";
								});
							}else{
								swal({
								 title: data.msg,
								 text: "Klik tombol di bawah.",
								 type: "warning",
								 showCancelButton: false,
								 confirmButtonColor: "#257DB6",
								 confirmButtonText: "Ok!",
								 closeOnConfirm: false
								});
							}
						}
					});
				});
			});
        });
      </script>
<?php
$ox = mysql_query("SELECT tb_lab_grading.*, step.id id_step
					FROM tb_lab_grading 
					LEFT JOIN step ON step.id_object = tb_lab_grading.id_object
					where tb_lab_grading.id_object='$id'");
$yi = mysql_fetch_array($ox);
$o = mysql_query("SELECT * FROM tb_front_desk where id_object='$id'");
$m = mysql_fetch_array($o);
?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <div class="modal fade" id="myModalxv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
          <div class="modal-dialog">
            <div class="loginmodal-container">
              <center><label style="font-size:20px;">Specific Gravity</label></center><br>
                <div class="col-md-12">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Berat Kering</label>
                        <input type="text" class="form-control" id="bk" name="bk" value="<?php echo $m['obj_weight']; ?>" placeholder="Berat Kering ...">
                      </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Berat Basah</label>
                          <input type="text" class="form-control" id="bb" name="bb" value="" placeholder="Berat Basah ...">
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <input type="submit" name="login" class="login loginmodal-submit" value="Hitung" onclick="hitung()">
                        </div>
                      </div>
                      <div id="hasil_hitung">
                        
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        <!-- Main content -->
  <section class="content">

    <div class="box box-warning">
      <div class="box-body">
        <div class="col-md-12">
          <div class="row">
            <div class="form-group">
              <div class="col-md-12 text-right">            
                <h4><b>F-02 - Diamond Grading</b></h4>
              </div>
            </div>
          </div>
        </div>
      
	  <form id="form-approve-grading" method="post">
	  <input type="hidden" name="id_step" id="id_step" value="<?php echo $yi['id_step'] ?>">
        <div class="col-md-12">
          <div class="row">
            <div class="form-group">
              <div class="col-md-12">
                  <h5><b>DATA GEMOLOG</b></h5>
                  <hr style="border:1px solid black;margin-top:-5px;">
              </div>
            </div>
          </div>

            <div>
              <div class="col-md-12">
                <div class="row">
                  <div class="col-md-3 col-sm-6">
                    <div class="form-group has-success">
                      <label>Gemologist Name <font color="red">*</font></label>
                      <br /><?php echo $yi['gemolog'] ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <div class="col-md-12">
          <div class="row">
            <div class="form-group">
              <div class="col-md-12">
                <div class="pull-right">
                  <a onclick="front()" id="minfro"><span class="fa fa-minus fa-2x"></span></a> &nbsp;
                  <a onclick="frontx()" id="plufro"><span class="fa fa-plus fa-2x"></span></a>
                </div>
                  <h5><b>FRONT DESK</b></h5>
                  <hr style="border:1px solid black;margin-top:-5px;">
              </div>
            </div>
          </div>

          <div id="frontc">
            <div class="col-md-12">
            <?php
            $y = mysql_query("SELECT * FROM tb_front_desk where id_object='$id'");
            $x = mysql_fetch_array($y);
            ?>
              <div class="row">
                <div class="col-md-6" style="border:2px solid #f39c12;padding:10px;border-radius:10px;">
                  <div class="row">

                    <div class="col-lg-5 col-md-5 col-xs-12">
                      <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess">Weight</label>
						<br /><?php echo ($yi['obj_weight'])?$yi['obj_weight']:"-"; ?>
                      </div>
                    </div>

                    <div class="col-lg-5 col-md-5 col-xs-12">
                      <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess">Length</label>
						<br /><?php echo ($yi['obj_length'])?$yi['obj_length']:"-"; ?>
                      </div>
                    </div>

                    <div class="col-lg-5 col-md-5 col-xs-12">
                      <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess"> Width</label>
						<br /><?php echo ($yi['obj_width'])?$yi['obj_width']:"-"; ?>
                      </div>
                    </div>

                    <div class="col-lg-5 col-md-5 col-xs-12">
                      <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess"> Height</label>
						<br /><?php echo ($yi['obj_height'])?$yi['obj_height']:"-"; ?>
                      </div>
                    </div>
                  </div>
                </div>
                  <div class="col-md-3">
                    <div class="form-group" style="border: 1px solid black;border-radius:10px; height: 173px">
                      <?php
                        $x = mysql_query("SELECT * FROM tb_front_desk where id_object='$id'");
                        $xx = mysql_fetch_array($x);
                      ?>
                        <center><img src="<?php echo base_url() ?>asset/images/<?php echo $xx['obj_image']?>" style="width:53%;padding:-10px;"></center>
                        <br><br>
                      <br>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <?php
                      $q = mysql_query("SELECT * FROM tb_front_desk a, color_stone b where a.obj_color=b.id and a.id_object='$id'");
                      $xf = mysql_fetch_array($q);
                    ?>
                    <div class="form-group" style="border:2px solid black;padding:10px;border-radius:10px;">
                      <div style="background-color:#<?php echo $xf['rgb_code']?>;height:148px;padding-top:50px;border-radius:10px;">
                      <center>
                        <div style="mix-blend-mode: hard-light;">
                          <?php $color = explode(";", $xf['color']); echo $color[0] ?>
                        </div>
                      </center>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
    <!-- input states -->
      <div class="col-md-12">
        <div class="row">
          <div class="form-group">
            <div class="col-md-12">
                <h5><b>MEASUREMENT & WEIGHT</b></h5>
                <hr style="border:1px solid black;margin-top:-5px;">
            </div>
          </div>
        </div>

          <div id="physy">
            <div class="col-md-12" style="width:100%;">
              <div class="row">
                <div class="col-md-12">
                  <div class="row">
                    <div class="col-md-2 col-sm-4 col-xs-6">
                      <div class="form-group has-success">
                        <label>Diameter Minimum</label>
						<br /><?php echo ($yi['diameter_min'])?$yi['diameter_min']:"-"; ?>
                      </div>
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6">
                      <div class="form-group has-success">
                        <label>Diameter Maximum</label>
						<br /><?php echo ($yi['diameter_max'])?$yi['diameter_max']:"-"; ?>
                      </div>
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6">
                      <div class="form-group has-success">
                        <label>Diameter Average</label>
						<br /><?php echo ($yi['diameter_avg'])?$yi['diameter_avg']:"-"; ?>
                      </div>
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6">
                      <div class="form-group has-success">
                        <label>Girdle Thickness Min.</label>
						<br /><?php echo ($yi['girdle_min'])?$yi['girdle_min']:"-"; ?>
                      </div>
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6">
                      <div class="form-group has-success">
                        <label>Girdle Thickness Max.</label>
						<br /><?php echo ($yi['girdle_max'])?$yi['girdle_max']:"-"; ?>
                      </div>
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6">
                      <div class="form-group has-success">
                        <label>Girdle Thickness Avg.</label>
						<br /><?php echo ($yi['girdle_avg'])?$yi['girdle_avg']:"-"; ?>
                      </div>
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6">
                      <div class="form-group has-success">
                        <label>Depth</label>
						<br /><?php echo ($yi['depth'])?$yi['depth']:"-"; ?>
                      </div>
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6">
                      <div class="form-group has-success">
                        <label>Depth Percentage</label>
						<br /><?php echo ($yi['depth_percentage'])?$yi['depth_percentage']:"-"; ?>
                      </div>
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6">
                      <div class="form-group has-success">
                        <label>Actual Weight</label>
						<br /><?php echo ($yi['actual_weight'])?$yi['actual_weight']:"-"; ?>
                      </div>
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6">
                      <div class="form-group has-success">
                        <label>Specific Gravity</label>
						<br /><?php echo ($yi['specific_gravity'])?$yi['specific_gravity']:"-"; ?>
                      </div>
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6">
                      <div class="form-group has-success">
                        <label>Table Percentage</label>
						<br /><?php echo ($yi['table_percentage'])?$yi['table_percentage']:"-"; ?>
                      </div>
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6">
                      <div class="form-group has-success">
                        <label>Crown Angles</label>
						<br /><?php echo ($yi['crown_angles'])?$yi['crown_angles']:"-"; ?>
                      </div>
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6">
                      <div class="form-group has-success">
                        <label>Pavillion Depth</label>
						<br /><?php echo ($yi['pavillion_depth'])?$yi['pavillion_depth']:"-"; ?>
                      </div>
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6">
                      <div class="form-group has-success">
                        <label>Culet Size</label>
						<br /><?php echo ($yi['culet_size'])?$yi['culet_size']:"-"; ?>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      <div class="col-md-12">
        <div class="row">
          <div class="form-group">
            <div class="col-md-12">
                <h5><b>GRADING</b></h5>
                <hr style="border:1px solid black;margin-top:-5px;">
            </div>
          </div>
        </div>

          <div id="physy">
            <div class="col-md-12" style="width:100%;">
              <div class="row">
                <div class="col-md-12">
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group has-success">
                        <label>Cut <font color='red'>*</font></label>
						<br /><?php echo ($yi['cut'])?$yi['cut']:"-"; ?>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group has-success">
                        <label>Shape <font color='red'>*</font></label>
						<br /><?php echo ($yi['shape'])?$yi['shape']:"-"; ?>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group has-success">
                        <label>Color Grading <font color='red'>*</font></label>
						<br /><?php echo ($yi['color_grading'])?$yi['color_grading']:"-"; ?>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group has-success">
                        <label>Clarity Grade <font color='red'>*</font></label>
						<br /><?php echo ($yi['clarity'])?$yi['clarity']:"-"; ?>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group has-success">
                        <label>Cut Grade <font color='red'>*</font></label>
						<br /><?php echo ($yi['cut_grade'])?$yi['cut_grade']:"-"; ?>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group has-success has-feedback">
                      <label class="control-label" for="inputSuccess">Girdle <font color='red'>*</font></label>
					  <br /><?php echo ($yi['girdle'])?$yi['girdle']:"-"; ?>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group has-success has-feedback">
                      <label class="control-label" for="inputSuccess">Culet <font color='red'>*</font></label>
					  <br /><?php echo ($yi['culet'])?$yi['culet']:"-"; ?>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group has-success has-feedback">
                      <label class="control-label" for="inputSuccess">Fluorescence <font color='red'>*</font></label>
					  <br /><?php echo ($yi['fluorescence'])?$yi['fluorescence']:"-"; ?>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group has-success has-feedback">
                      <label class="control-label" for="inputSuccess">Proportions</label>
					  <br /><?php echo ($yi['proportions'])?$yi['proportions']:"-"; ?>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group has-success has-feedback">
                      <label class="control-label" for="inputSuccess">Polish Grade</label>
					  <br /><?php echo ($yi['polish_grade'])?$yi['polish_grade']:"-"; ?>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group has-success has-feedback">
                      <label class="control-label" for="inputSuccess">Symmetry Grade</label>
					  <br /><?php echo ($yi['symmetry_grade'])?$yi['symmetry_grade']:"-"; ?>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group has-success has-feedback">
                      <label class="control-label" for="inputSuccess">Keys To Symbols <font color='red'>*</font></label>
					  <br /><?php echo ($yi['key'])?$yi['key']:"-"; ?>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

                <div class="form-group">
                  <div class="col-md-12">
                      <button type="submit" name="submit" class="btn btn-success pull-right" style="margin-bottom:20px;margin-right:10px;"><i class="fa fa-reply"></i> Approve </button>
                      <a href="" class="btn btn-danger pull-right" style="margin-bottom:20px;margin-right:10px;"><i class="fa fa-chevron-left"></i> Back</a>
                  </div>
                </div>
              <!-- END -->
              </form>
          <?php
          $xf = $id;
          ?>
          <div class="modal fade" id="modalEdit">
            <div class="modal-dialog modal-sm">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <div class="modal-title" id="myModalLabel">Edit Image</div>
                </div>
                <div class="modal-body">
                  <div class="row">
                    <div class="col-md-12 col-sm-12">
                      <div class="form-group">
                        <form action="data-ajax/save_img.php" id="frmuploadImgx" method="post" target="iframeUploadImgx" enctype="multipart/form-data">
                          <input type="file" class="drop form-control" name="gambar" id="drop" onchange="submitImagex()" data-default-file="./asset/images/<?php echo $yi['obj_image'] ?>">
                        </form>
                      </div>
                    </div>
                      <input class="hidden" type="text" name="gambaroutx" id="gambaroutx" value="<?php echo $yi['obj_image'] ?>">
                      <iframe class="hidden" name="iframeUploadImgx" id="iframeUploadImgx"></iframe> 
                      <div class="col-md-12 text-right">
                        <button class="btn btn-primary" onclick="edit_gambar('<?php echo $xf?>')">SAVE</button>
                      </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="modal fade" id="modalFro">
            <div class="modal-dialog modal-sm">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <div class="modal-title" id="myModalLabel">Edit Data Front</div>
                </div>
                <div class="modal-body">
                <div class="row">
                <?php
                $k = mysql_query("SELECT * FROM tb_front_desk where id_object='$id'");
                $x = mysql_fetch_array($k);
                ?>
                  <div class="col-lg-6 col-md-6 col-xs-12">
                      <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess">Weight</label>
                        <input type="number" step="0.01" name="weight" id="cts" class="form-control" value="<?php echo $x['obj_weight']?>" placeholder="Enter..." required>
                      </div>
                    </div>

                    <div class="col-lg-6 col-md-6 col-xs-12">
                      <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess">Length</label>
                        <input type="number" step="0.01" class="form-control" id="lmm" name="length" value="<?php echo $x['obj_length']?>" placeholder="Enter..." required>
                      </div>
                    </div>

                    <div class="col-lg-6 col-md-6 col-xs-12">
                      <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess"> Width</label>
                        <input type="number" step="0.01" class="form-control" id="wmm" name="width" value="<?php echo $x['obj_width']?>" placeholder="Enter..." required>
                      </div>
                    </div>

                    <div class="col-lg-6 col-md-6 col-xs-12">
                      <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess"> Height</label>
                        <input type="number" step="0.01" class="form-control" id="hmm" name="height" value="<?php echo $x['obj_height']?>" placeholder="Enter..." required>
                      </div>
                    </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <button class="btn btn-primary" type="submit" onclick="save_dimen('<?php echo $id?>')">SAVE</button>
                    </div>
                  </div>
                </div>
                </div>
              </div>
            </div>
          </div>
          <div class="modal fade" id="modalCol">
            <div class="modal-dialog modal-md">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <div class="modal-title" id="myModalLabel">Edit Color</div>
                </div>
                <div class="modal-body">
                  <div class="row">
                      <div class="col-md-6">
                        <div class="form-group has-success">
                          <label>Color</label>
                          <select name="colr" id="obj" class="form-control" onchange="chos()" required>
                            <option>Select color..</option>
                              <?php
                                $col = mysql_query("SELECT * FROM color_stone GROUP BY jenis_warna");
                                while ($ge = mysql_fetch_array($col)) {
                              ?>
                              <?php
                                if($ge['jenis_warna'] == "Select color.."){
                              ?>
                            <option value="<?php echo $ge['jenis_warna'] ?>" selected style="text-transform: capitalize;"><?php echo $ge['jenis_warna'] ?></option>  
                              <?php
                                }else{
                              ?>
                            <option value="<?php echo $ge['jenis_warna'] ?>" style="text-transform: capitalize;"><?php echo $ge['jenis_warna'] ?></option>
                              <?php
                                  }
                              }
                              ?>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group has-success">
                          <label>Specific Color</label>
                          <div style="min-height: 120px;">
                            <div class="btn-group" id="col-area" data-toggle="buttons">
                              -
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <button class="btn btn-primary" onclick="save_color('<?php echo $xf ?>')">SAVE</button>
                        </div>
                      </div>
                   </div>
                  </div>
                </div>
              </div>
            </div>
    
    <script>
  $(function(){
    $('#frontc').hide();
    $('#minfro').hide();
  });
  function frontx() {
    $('#minfro').show();
    $('#plufro').hide();
    $('#frontc').show(700);
  }
  function front() {
    $('#minfro').hide();
    $('#plufro').show();
    $('#frontc').hide(700);
  }
  function count_dim(){
    var min = $("#diameter-min").val();
    var max = $("#diameter-max").val();
    var avg = (parseFloat(min)+parseFloat(max))/2;
    $("#diameter-avg").val(avg);
  }
  function count_girdle(){
    var min = $("#gird_min").val();
    var max = $("#gird_max").val();
    var avg = (parseFloat(min)+parseFloat(max))/2;
    $("#gird_avg").val(avg);
  }
    function submit() {
      $('#coba').html('Gambar Sudah Ditetapkan');
    }
    $("#drop").dropify();

    $(document).on('keypress',function(e){
      if(e.keyCode==13){
          e.preventDefault();
      }
    });
    function submitImagex(){
      $( "#frmuploadImgx" ).submit();
    }

    </script>