      
      <div class="index">
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>&nbsp;</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-user"></i> Home</a></li>
            <li>Daftar Persetujuan</li>
            <li class="active">Taksiran</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="box">
            <div class="box-header">
              <h5><b>DAFTAR PERSETUJUAN TAKSIRAN</b></h5>
              <hr style="border:1px solid black;margin-bottom:-10px;margin-top:-5px;">
              <br>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><span class="fa fa-minus"></span></button>
                <button class="btn btn-box-tool" data-widget="remove"><span class="fa fa-times"></span></button>
              </div>
            </div><!-- /.box-header -->
            <div class="box-body" style="margin-top:-15px;">
              <?php
                date_default_timezone_set('Asia/Jakarta');
                $tgl = date('Y-m-d');
                $no = 1;
                $per_page = 5;
                $kode = sessionValue('kode_store');
                $u = $this->db->query("SELECT tb_taksiran.id_order, tb_taksiran.id, master_jenis_permata.jenis, tb_taksiran.berat_bersih, tb_taksiran.berat_kotor, tb_taksiran.jumlah_permata, tb_fpjt.create_date 
									FROM tb_taksiran
									LEFT JOIN tb_fpjt ON tb_fpjt.id = tb_taksiran.id
									LEFT JOIN master_jenis_permata ON master_jenis_permata.id = tb_taksiran.jenis_perhiasan
									LEFT JOIN step ON step.id_object = tb_fpjt.id
									WHERE tb_taksiran.jenis_perhiasan = master_jenis_permata.id AND tb_fpjt.status='SUDAH' 
									AND step.status='NOT APPROVED' AND ISNULL(step.delete_by)
									AND step.status_barang='TIDAK TERTINGGAL'
									AND tb_taksiran.id_order='".$id_order."'");
              ?>
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th style="text-align:center">NO</th>
                      <th style="text-align:center">Jenis Perhiasan</th>
                      <th style="text-align:center">Berat Kotor</th>
                      <th style="text-align:center">Berat Bersih</th>
                      <th style="text-align:center">Jumlah Permata</th>
                      <th style="text-align:center">TIME IN</th>
                      <th class="text-center">ACTION</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php
                    foreach ($u->result_array() as $i) {
                    $tglx = date('H:i A', strtotime($i['create_date']));
					$id_order = str_replace('/','-',$i['id_order']);
                  ?>
                    <tr>
                      <td style="text-align:center"><?php echo $no++?></td>
                      <td><?php echo $i['jenis'] ?></td>
                      <td style="text-align:center"><?php echo $i['berat_kotor']?></td>
                      <td style="text-align:center"><?php echo $i['berat_bersih']?></td>
                      <td style="text-align:center"><?php echo $i['jumlah_permata']?></td>
                      <td style="text-align:center"><?php echo $tglx?></td>
                      <td style="text-align:right;width:200px;">
                        <a onclick="view('<?php echo $i['id'] ?>')" class="btn btn-info disabled" data-toggle="tooltip" data-placement="bottom" title="View Object"><i class="fa fa-eye"></i></a>
						<a onclick="startTaksiran('<?php echo $i['id'] ?>', '<?php echo $id_order ?>')" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Edit Examintion"><i class="fa fa-edit"></i></a>
                        <a href="#" class="btn btn-success" onclick="goViewApproveTaksiran('<?php echo $i['id']; ?>', '<?php echo $id_order ?>')" data-toggle="tooltip" data-placement="bottom" title="Approve"><i class="fa fa-check"></i></a>
                      </td>
                    </tr>
                    <?php
                      }
                    ?>
                  </tbody>
                </table>
              </div>
            </div><!-- /.box-body -->
          </section><!-- /.content -->
        </div><!-- /.content-wrapper -->
      </div>
      <script>
        $("#example1").DataTable();
      </script>