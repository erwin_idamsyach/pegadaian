      <!-- Modal -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" style="width:400px;">
          <div class="modal-content">
            <div class="modal-header" style="background-color:#f8f8f8">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h6 class="modal-title" id="myModalLabel"><b>VIEW OBJECT</b></h6>
            </div>
            <div class="modal-body">
              <div class="lol">
                
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="index">
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>&nbsp;</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-user"></i> Home</a></li>
            <li>Daftar Persetujuan</li>
            <li class="active">Grading</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="box">
            <div class="box-header">
              <h5><b>DAFTAR PERSETUJUAN GRADING</b></h5>
              <hr style="border:1px solid black;margin-bottom:-10px;margin-top:-5px;">
              <br>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><span class="fa fa-minus"></span></button>
                <button class="btn btn-box-tool" data-widget="remove"><span class="fa fa-times"></span></button>
              </div>
            </div><!-- /.box-header -->
            <div class="box-body" style="margin-top:-15px;">
              <?php
                date_default_timezone_set('Asia/Jakarta');
                $tgl = date('Y-m-d');
                $no = 1;
                $per_page = 5;
                $kode = sessionValue('kode_store');
                $u = $this->db->query("SELECT a.*, b.id, c.rgb_code FROM tb_lab_grading a
										LEFT JOIN step b ON b.id_object = a.id_object
										LEFT JOIN tb_front_desk d ON d.id_object = a.id_object
										LEFT JOIN color_stone c ON c.code = a.obj_color	
										where d.store='$kode' 
										AND b.status_grading='NOT APPROVED' AND b.dia_grading != '' AND b.check_dg != '' 
										AND ISNULL(b.delete_by) AND b.status_barang='TIDAK TERTINGGAL'");
              ?>
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th style="text-align:center">NO</th>
                      <th style="text-align:center">ID</th>
                      <th style="text-align:center">COLOR</th>
                      <th style="text-align:center">CTS</th>
                      <th style="text-align:center">L (mm)</th>
                      <th style="text-align:center">W (mm)</th>
                      <th style="text-align:center">H (mm)</th>
                      <th style="text-align:center">TIME IN</th>
                      <th class="text-center">ACTION</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php
                    foreach ($u->result_array() as $i) {
                    $tglx = date('H:i A', strtotime($i['input_date']));
                  ?>
                    <tr>
                      <td style="text-align:center"><?php echo $no++?></td>
                      <td><?php echo $i['id_object']?></td>
                      <td style="text-align:center"><a class="btn btn-default btn-lg" style="background-color:#<?php echo $i['rgb_code']?>;"></a></td>
                      <td style="text-align:center"><?php echo $i['obj_weight']?></td>
                      <td style="text-align:center"><?php echo $i['obj_length']?></td>
                      <td style="text-align:center"><?php echo $i['obj_width']?></td>
                      <td style="text-align:center"><?php echo $i['obj_height']?></td>
                      <td style="text-align:center"><?php echo $tglx?></td>
                      <td style="text-align:right;width:200px;">
                        <a onclick="view('<?php echo $i['id_object'] ?>')" class="btn btn-info" data-toggle="tooltip" data-placement="bottom" title="View Object"><i class="fa fa-eye"></i></a>
                        <a onclick="godadit('<?php echo $i['id_object'] ?>')" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="Edit Examintion (Diamond Grading)"><i class="fa fa-edit"></i></a>
                        <a href="#" onclick="goViewApproveGrading('<?php echo $i['id_object'] ?>')" class="btn btn-success" data-toggle="tootlip" data-placement="bottom" title="Approve">
                          <i class="fa fa-check"></i>
                        </a>
                      </td>
                    </tr>
                    <?php
                      }
                    ?>
                  </tbody>
                </table>
              </div>
            </div><!-- /.box-body -->
          </section><!-- /.content -->
        </div><!-- /.content-wrapper -->
      </div>
      <script>
        $("#example1").DataTable();
      </script>