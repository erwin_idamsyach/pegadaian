      <script type="text/javascript">
        jQuery(function($){
		    $('#form-approve-taksiran').trigger("reset");
			$("#form-approve-taksiran").on('submit',function(e){
			e.preventDefault();
			var formatData = new FormData($(this)[0]);
				swal({
				 title: "",
				 text: "Apakah Anda akan meng-approve data?",
				 type: "warning",
				 showCancelButton: true,
				 confirmButtonColor: "#DD6B55",
				 confirmButtonText: "Ya",
				 cancelButtonText: "Tidak",
				 closeOnConfirm: false }, function(){
					$.ajax({
						type : 'POST',
						url : '<?php echo base_url()?>approve/approve_taksiran',
						data : formatData,
						async: false,
						cache: false,
						contentType: false,
						processData: false,
						success:function(html){
							//window.location.reload();
							var data = eval ("(" + html + ")");
							if(data.success){
								swal({
								 title: "Berhasil approve data!",
								 text: "Klik tombol di bawah.",
								 type: "success",
								 showCancelButton: false,
								 confirmButtonColor: "#257DB6",
								 confirmButtonText: "Ok!",
								 closeOnConfirm: false
								},
								function(){
									location.href = base_url+"approve/taksiran_detail/<?php echo $id_order; ?>";
								});
							}else{
								swal({
								 title: data.msg,
								 text: "Klik tombol di bawah.",
								 type: "warning",
								 showCancelButton: false,
								 confirmButtonColor: "#257DB6",
								 confirmButtonText: "Ok!",
								 closeOnConfirm: false
								});
							}
						}
					});
				});
			});
        });
      </script>
<?php
$ox = mysql_query("SELECT tb_taksiran.*, step.id id_step, master_jenis_permata.jenis, master_permata.permata,
					master_logam.jenis_logam
					FROM tb_taksiran 
					LEFT JOIN step ON step.id_object = tb_taksiran.id
					LEFT JOIN master_jenis_permata ON master_jenis_permata.id = tb_taksiran.jenis_perhiasan
					LEFT JOIN master_permata ON master_permata.id = tb_taksiran.jenis_permata
					LEFT JOIN master_logam ON master_logam.id = tb_taksiran.jenis_logam
					where tb_taksiran.id='$id'");
$yi = mysql_fetch_array($ox);
?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Main content -->
  <section class="content">

    <div class="box box-warning">
      <div class="box-body">
        <div class="col-md-12">
          <div class="row">
            <div class="form-group">
              <div class="col-md-12 text-right">            
                <h4><b>F-03 - Taksiran </b></h4>
              </div>
            </div>
          </div>
        </div>
		
      <form id="form-approve-taksiran" method="post">
	  <input type="hidden" name="id_step" id="id_step" value="<?php echo $yi['id_step'] ?>">
        <div class="col-md-12">
          <div class="row">
            <div class="form-group">
              <div class="col-md-12">
                  <h5><b>DATA GEMOLOG</b></h5>
                  <hr style="border:1px solid black;margin-top:-5px;">
              </div>
            </div>
          </div>

            <div>
              <div class="col-md-12">
                <div class="row">
                  <div class="col-md-3 col-sm-6">
                    <div class="form-group has-success">
                      <label>Gemologist Name <font color="red">*</font></label>
                      <br /><?php echo $yi['gemolog'] ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-12">
          <div class="row">
            <div class="form-group">
			  <div class="col-md-12">
                  <h5><b>PENAKSIRAN - PERHIASAN</b></h5>
                  <hr style="border:1px solid black;margin-top:-5px;">
              </div>
			  <div class="col-md-12">
				<div class="row">
				  <div class="col-md-2 col-sm-6">
                    <div class="form-group has-success">
                      <label>Jenis Perhiasan <font color="red">*</font></label>
                      <br /><?php echo ($yi['jenis'])?$yi['jenis']:"-"; ?>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-4">
                    <div class="form-group has-success">
                      <label>Jumlah Perhiasan <font color="red">*</font></label>
					  <br /><?php echo ($yi['jumlah_perhiasan'])?$yi['jumlah_perhiasan']:"-"; ?>
                    </div>
                  </div>
				  <div class="col-md-6">
					<div class="form-group has-success">
					  <label>Keterangan Tambahan</label>
					  <br /><?php echo ($yi['keterangan'])?$yi['keterangan']:"-"; ?>
					</div>
				  </div>
				</div>
			  </div>
            </div>
          </div>
            <div class="row">
			  <div class="col-md-12">
                  <h5><b>PENAKSIRAN - PERMATA</b></h5>
                  <hr style="border:1px solid black;margin-top:-5px;">
              </div>
              <div class="col-md-12">
                <div class="row">
                  <div class="col-md-2 col-sm-6">
                    <div class="form-group has-success">
                      <label>Jenis Logam</label>
					  <br /><?php echo ($yi['jenis_logam'])?$yi['jenis_logam']:"-"; ?>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-4">
                    <div class="form-group has-success">
                      <label>Berat Kotor</label>
					  <br /><?php echo ($yi['berat_kotor'])?$yi['berat_kotor']:"-"; ?> gram
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-4">
                    <div class="form-group has-success">
                      <label>Berat Bersih</label>
					  <br /><?php echo ($yi['berat_bersih'])?$yi['berat_bersih']:"-"; ?> gram
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-4">
                    <div class="form-group has-success">
                      <label>Karatase Emas</label>
					  <br /><?php echo ($yi['karatase_emas'])?$yi['karatase_emas']." Karat":"-"; ?>
                    </div>
                  </div>
				</div>
				<div class="row"> 
                  <div class="col-md-2 col-sm-4">
                    <div class="form-group has-success">
                      <label>Jenis Permata</label>
					  <br /><?php echo ($yi['permata'])?$yi['permata']:"-"; ?>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-4">
                    <div class="form-group has-success">
                      <label>Jumlah Permata</label>
					  <br /><?php echo ($yi['jumlah_permata'])?$yi['jumlah_permata']:"-"; ?>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-4">
                    <div class="form-group has-success">
                      <label>Berat Total Berlian</label>
					  <br /><?php echo ($yi['berat_total_permata'])?$yi['berat_total_permata']." carat":"-"; ?>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-4">
                    <div class="form-group has-success">
                      <label>4C Berlian</label>
					  <br /><?php echo ($yi['keterangan_berlian'])?$yi['keterangan_berlian']:"-"; ?>
                    </div>
                  </div>
				  <div class="col-md-12">
					<div class="form-group">
						<div class="col-md-12">
							<button type="submit" name="submit" class="btn btn-success pull-right" style="margin-bottom:20px;margin-right:10px;"><i class="fa fa-reply"></i> Approve </button>
                      <a href="" class="btn btn-danger pull-right" style="margin-bottom:20px;margin-right:10px;"><i class="fa fa-chevron-left"></i> Back</a>
						</div>
					</div>
				  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
              <!-- END -->
              </form>
          <?php
          $xf = $id;
          ?>
<script>
  function showSweets(){
    swal({
     title: "",
     text: "Apakah Anda akan menyimpan data?",
     type: "warning",
     showCancelButton: true,
     confirmButtonColor: "#DD6B55",
     confirmButtonText: "Ya",
     cancelButtonText: "Tidak",
     closeOnConfirm: false }, function(){
      document.getElementById("form-taksiran").submit();
    });
  }
</script>