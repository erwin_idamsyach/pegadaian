	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>&nbsp;</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Daftar Cetak Ulang</li>
            <li class="active"><?php echo $bread; ?></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
        <?php
        if($bread == "Sertifikasi"){
        ?>
        	<div class="box box-default">
        		<b>PRINT BRIEF REPORT</b>
	              <div style="border: 1px solid black; margin-bottom: 10px;"></div>
	              <table class="table table-bordered table-striped table-hover" id="example1">
	                <thead>
	                  <tr>
	                    <th class="text-center">No</th>
	                    <th class="text-center">ID Object</th>
	                    <th class="text-center">Identification Result</th>
	                    <th class="text-center">Create Date</th>
	                    <th class="text-center">Action</th>
	                  </tr>
	                </thead>
	                <tbody>
	                	<?php
	                	$no = 1;
	                	$sql = $this->db->query($query_gem);
	                	foreach ($sql->result() as $get) {
	                		?>
	                	<tr>
                        <td class="text-center"><?php echo $no++ ?></td>
                        <td><?php echo $get->id_object ?></td>
                        <td><?php echo $get->obj_natural." ".$get->variety." ".$get->nama_batu ?></td>
                        <td><?php $dd = date_create($get->create_date); echo date_format($dd, 'D, d-m-Y') ?></td>
                        <td>
                          <div class="pull-right">
                            <a href="<?php echo site_url('ready/print_gem_card').'/'.$get->id_object ?>" class="btn btn-info" target='_blank' data-toggle="tooltip" data-placement="bottom" title="Print Gem Card"><i class="fa fa-print"></i></a>&nbsp;&nbsp;
                          </div>
                        </td>
                      </tr>
	                		<?php
	                	}
	                	?>
	                </tbody>
	               </table>
        	</div>
          <div class="box box-default">
            <b>PRINT IDENTIFICATION REPORT</b>
                <div style="border: 1px solid black; margin-bottom: 10px;"></div>
                <table class="table table-bordered table-striped table-hover" id="example1">
                  <thead>
                    <tr>
                      <th class="text-center">No</th>
                      <th class="text-center">ID Object</th>
                      <th class="text-center">Identification Result</th>
                      <th class="text-center">Create Date</th>
                      <th class="text-center">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $no = 1;
                    $sql = $this->db->query($query_cert);
                    foreach ($sql->result() as $get) {
                      ?>
                    <tr>
                        <td class="text-center"><?php echo $no++ ?></td>
                        <td><?php echo $get->id_object ?></td>
                        <td><?php echo $get->obj_natural." ".$get->variety." ".$get->nama_batu ?></td>
                        <td><?php $dd = date_create($get->create_date); echo date_format($dd, 'D, d-m-Y') ?></td>
                        <td>
                          <div class="pull-right">
                            <a href="<?php echo site_url('certificate/print_certificate').'/'.$get->id_object ?>" class="btn btn-info" target='_blank' data-toggle="tooltip" data-placement="bottom" title="Print Gem Card"><i class="fa fa-print"></i></a>&nbsp;&nbsp;
                          </div>
                        </td>
                      </tr>
                      <?php
                    }
                    ?>
                  </tbody>
                 </table>
          </div>
          <?php
        }else if($bread == "Grading"){
          ?>
          <div class="box box-default">
            <b>PRINT DIAMOND GRADING</b>
                <div style="border: 1px solid black; margin-bottom: 10px;"></div>
                <table class="table table-bordered table-striped table-hover" id="example1">
                  <thead>
                    <tr>
                      <th class="text-center">No</th>
                      <th class="text-center">ID Object</th>
                      <th class="text-center">Identification Result</th>
                      <th class="text-center">Create Date</th>
                      <th class="text-center">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $no = 1;
                    $sql = $this->db->query($query);
                    foreach ($sql->result() as $get) {
                      ?>
                    <tr>
                        <td class="text-center"><?php echo $no++ ?></td>
                        <td><?php echo $get->id_object ?></td>
                        <td><?php echo $get->shape.", ".$get->cut.", ".$get->color_grading.", ".$get->clarity ?></td>
                        <td><?php $dd = date_create($get->create_date); echo date_format($dd, 'D, d-m-Y') ?></td>
                        <td>
                          <div class="pull-right">
                            <a href="<?php echo site_url('grading/print_grading').'/'.$get->id_object ?>" class="btn btn-info" target='_blank' data-toggle="tooltip" data-placement="bottom" title="Print Grading"><i class="fa fa-print"></i></a>&nbsp;&nbsp;
                          </div>
                        </td>
                      </tr>
                      <?php
                    }
                    ?>
                  </tbody>
                 </table>
          </div>
          <?php
        }else{
          ?>
          <div class="box box-default">
            <b>PRINT HASIL TAKSIRAN</b>
            <div style="border: 1px solid black; margin-bottom: 10px;"></div>
          <table id="example1" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th style="text-align:center">NO</th>
                      <th style="text-align:center">ID Order</th>
                      <th style="text-align:center">ID Nasabah</th>
                      <th style="text-align:center">Nama Nasabah</th>
                      <th style="text-align:center">TIME IN</th>
                      <th class="text-center">ACTION</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php
                    $no = 1;
                    $u = $this->db->query($query);
                    foreach ($u->result_array() as $i) {
                    $tglx = date('H:i A', strtotime($i['create_date']));
					 $id_order = str_replace('/', '-', $i['id_order']);
                  ?>
                    <tr>
                      <td style="text-align:center"><?php echo $no++?></td>
                      <td style="text-align:center"><?php echo $i['id_order'] ?></td>
                      <td style="text-align:center"><?php echo $i['id_member']?></td>
                      <td style="text-align:center"><?php echo $i['nama']?></td>
                      <td style="text-align:center"><?php echo $tglx?></td>
                      <td style="text-align:right;width:200px;">
                        <a href="<?php echo site_url('print_taksiran/print_sht')."/".$i['id']."/".$id_order ?>" target="_blank" class="btn btn-info" data-toggle="tooltip" data-placement="bottom" title="Print"><i class="fa fa-print"></i></a>
                      </td>
                    </tr>
                    <?php
                      }
                    ?>
                  </tbody>
                </table>
              </div>
          <?php
        }
          ?>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->