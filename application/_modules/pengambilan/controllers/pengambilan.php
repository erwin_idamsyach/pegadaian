<?php if (! defined('BASEPATH')){ exit('No direct script allowed'); }
/*
* Controller List FPJS
* Create by Erwin Idamsyach @ 2 May 2016
*/
class pengambilan extends CI_Controller{
	function pengambilan(){
        parent::__construct();
		
		if(!isLogin()){
			goLogin();
		}
        $this->load->library('fpdf');
    }
	
	public function index(){
		$data['filelist'] = 'pengambilan/fpjs';
		$data['title'] = 'List Pengambilan Barang (BPS)';
		$data['title_menu'] = 'Front';
		$data['menu'] = 'pengambilan';

		getHTMLWeb($data);
	}
	
	function formFPJS($id){
		$data["filelist"] = "pengambilan/form-fpjs";
        $data["title"] = "Form Pengambilan (BPS)";
        $data["title_menu"] = "Front";
        $data["menu"] = "pengambilan";
        $data["id"] = $id;
		
		$id = str_replace('-', '/', $id);
		
		$getData = $this->db->query("SELECT tb_front_desk.*, step.certificate, step.gem_card, step.dia_grading, step.pay,
									tb_lab_desk.obj_natural,
									tb_member.first_name, tb_member.middle_name, tb_member.last_name, tb_member.address, tb_member.postal_code, tb_member.phone,
									master_kecam.nama_kecam kec1, master_kokab.kokab_nama kota1, master_provinsi.provinsi_nama prov1
									FROM step
									LEFT JOIN tb_front_desk ON step.id_object = tb_front_desk.id_object
									LEFT JOIN tb_lab_desk ON tb_lab_desk.id_object = tb_front_desk.id_object
									LEFT JOIN tb_member ON tb_member.id_member = tb_front_desk.id_member
									LEFT JOIN master_kecam ON master_kecam.kecam_id = tb_member.district
									LEFT JOIN master_kokab ON master_kokab.kota_id = tb_member.city
									LEFT JOIN master_provinsi ON master_provinsi.provinsi_id = tb_member.province
									WHERE (step.status_barang='TERTINGGAL' OR step.step='FRONT_DESK' OR (step.pay='Y' AND step.status_barang!='SELESAI'))
									AND step.store='".sessionValue('kode_store')."'
									AND tb_front_desk.id_order='".$id."'
									LIMIT 0,1");
		$tmp = $getData->result();
		$data['tmp'] = $tmp[0];	
		
		$getDataCertificate = $this->db->query("SELECT * FROM tb_service WHERE kode='FR'");
		$tmpCertificate = $getDataCertificate->result();
		$data['tmpCertificate'] = $tmpCertificate[0];	
		
		if($tmp[0]->obj_natural=="NATURAL"){
			$getDataBrief = $this->db->query("SELECT * FROM tb_service WHERE kode='BR'");
		}else{
			$getDataBrief = $this->db->query("SELECT * FROM tb_service WHERE kode='BRS'");
		}
		
		$tmpBrief = $getDataBrief->result();
		$data['tmpBrief'] = $tmpBrief[0];
		
		$getDataDG = $this->db->query("SELECT * FROM tb_service WHERE kode='DG'");
		$tmpDG = $getDataDG->result();
		$data['tmpDG'] = $tmpDG[0];
		
		$getDataCash = $this->db->query("SELECT * FROM tb_cashier WHERE id_order='".$id."' ORDER BY create_date DESC LIMIT 0,1");
		$data['getDataCash'] = $getDataCash;
        getHTMLWeb($data);
    }
	
	function pengambilan_barang(){
		//$ord = $this->uri->segment(3);
		//$id_order = str_replace('-', '/', $ord);
		$id_order = $this->input->post('order_no');
		$tmp_id_order = $this->input->post('tmp_order_no');
		
		$success = false;
		$msg = "";
		if($id_order==$tmp_id_order){
			$data = array(
				'status_barang' => 'SELESAI',
				'update_by' => sessionValue('id'),
				'update_date'=> date('Y-m-d H:i:s')
			);
			
			$this->db->where('id_order', $id_order);
			$this->db->where('status_barang', 'TERTINGGAL');
			if($this->db->update('step', $data)){
				$success = true;
			}else{
				$msg = "Gagal memproses data";
			}
		}else{
			$msg = "No. Order Tidak Sesuai";
		}
		
		
		echo json_encode(array("success"=>$success, "msg"=>$msg));
	}
	
	function fpjt(){
		$data['filelist'] = 'pengambilan/fpjt';
		$data['title'] = 'List pengambilan (BPT)';
		$data['title_menu'] = 'Front';
		$data['menu'] = 'pengambilan_fpjt';

		getHTMLWeb($data);
	}
	
	function formFPJT($id){
		$data["filelist"] = "pengambilan/form-fpjt";
        $data["title"] = "Form Pengambilan (BPT)";
        $data["title_menu"] = "Front";
        $data["menu"] = "pengambilan_fpjt";
        $data["id"] = $id;
		
		$id = str_replace('-', '/', $id);
		
		$getData = $this->db->query("SELECT tb_fpjt.*, step.pay,
									tb_member.first_name, tb_member.middle_name, tb_member.last_name, tb_member.address, tb_member.postal_code, tb_member.phone,
									master_kecam.nama_kecam kec1, master_kokab.kokab_nama kota1, master_provinsi.provinsi_nama prov1
									FROM tb_fpjt 
									LEFT JOIN step ON step.id_object = tb_fpjt.id
									LEFT JOIN tb_member ON tb_member.id_member = tb_fpjt.id_member
									LEFT JOIN master_kecam ON master_kecam.kecam_id = tb_member.district
									LEFT JOIN master_kokab ON master_kokab.kota_id = tb_member.city
									LEFT JOIN master_provinsi ON master_provinsi.provinsi_id = tb_member.province
									WHERE (step.status_barang='TERTINGGAL' OR step.step='FRONT_DESK' OR (step.pay='Y' AND step.status_barang!='SELESAI'))
									AND step.store='".sessionValue('kode_store')."'
									AND tb_fpjt.id_order='".$id."'
									LIMIT 0,1");
		$tmp = $getData->result();
		$data['tmp'] = $tmp[0];	
		
		$harga = 100000;
		$harga_permata = 50000;
		
		$data['harga'] = $harga;
		$data['harga_permata'] = $harga_permata;
		
		$getDataCash = $this->db->query("SELECT * FROM tb_cashier WHERE id_order='".$id."' ORDER BY create_date DESC LIMIT 0,1");
		$data['getDataCash'] = $getDataCash;
        getHTMLWeb($data);
    }
}
?>