<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome/welcome_message');
	}
	
	function getDataWelcome(){
		$page = isset($_POST['page']) ? intval($_POST['page']) : 1;
		$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
		$sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'ID';  
		$order = isset($_POST['order']) ? strval($_POST['order']) : 'DESC';
		$offset = ($page-1)*$rows;
		$result = array();
		$keyword = (isset($_POST["keyword"])!="")?mysql_real_escape_string($_POST["keyword"]):"";

		if($keyword!=""){
			$WHERE_FILTER = "AND (LOWER(NAME) LIKE LOWER('%".$keyword."%') OR
							LOWER(LOCATION) LIKE LOWER('%".$keyword."%'))";
			$count = $this->system_parameter_model->getData("WHERE FLAG<>'DELETED'".$WHERE_FILTER);
			$query = $this->system_parameter_model->getData("WHERE FLAG<>'DELETED'".$WHERE_FILTER." ORDER BY ".$sort." ".$order." LIMIT ".$offset.",".$rows);
		}else{
			$count = $this->system_parameter_model->getData('WHERE FLAG<>"DELETED"');
			$query = $this->system_parameter_model->getData('WHERE FLAG<>"DELETED" ORDER BY '.$sort.' '.$order.' LIMIT '.$offset.','.$rows);
		}  
		$array = $query->result();
		
		$result["total"] = $count->num_rows();
		
		$items = array();
		foreach($array as $maintenancedata){
			array_push($items, $maintenancedata);
		}
		$result["rows"] = $items;
	
		echo json_encode($result);
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */