<!-- front.php, Create By : Hadi Setiawan -->
<script type="text/javascript">
    jQuery(function($){
        $('#example1').DataTable();
    });
	
	function deleteData(id){
		swal({
            title: "",
            text: "Apakah Anda akan menghapus data?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: false },
			function(isConfirm) {
				if (isConfirm) {
					location.href = base_url+"<?php echo $controls;?>/deleteData/"+id;
				} else {
					
				}
			}
        );		
		return false;
	}
</script>

	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>&nbsp;</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
            <li><?php echo $title;?></li>
            <li class="active"><?php echo ucfirst($menu);?></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
        	<?php
        	if(!isset($id)){
        		?>
			<div class="box box-default">
				<div class="box-body">
					<b>ADD <?php echo strtoupper($menu);?></b>
					<div style="border: 1px solid black; margin-bottom: 10px"></div>
					<form action="<?php echo site_url($controls.'/addData') ?>" method="post">
					<div class="row">
						<div class="col-md-3 col-sm-6">
							<div class="form-group">
								<label><?php echo ucfirst($menu);?></label>
								<input type="text" name="nama" class="form-control" placeholder="<?php echo ucfirst($menu);?>">
							</div>
						</div>
						<div class="col-md-9">
							<div class="pull-left">
								<button type="submit" class="btn btn-primary" style="margin-top: 25px;">Submit</button>
							</div>
						</div>
					</div>
					</form>
				</div>
			</div>
			<?php
        	}else{
        		$query = $this->db->query("SELECT * FROM ".$table." WHERE id='".$id."'");
        		foreach ($query->result() as $tmp) {
        			?>
        	<div class="box box-default">
				<div class="box-body">
					<b>EDIT <?php echo strtoupper($menu);?></b>
					<div style="border: 1px solid black; margin-bottom: 10px"></div>
					<form action="<?php echo site_url($controls.'/updateData') ?>" method="post">
					<div class="row">
						<div class="col-md-3 col-sm-6">
							<div class="form-group">
								<label><?php echo ucfirst($menu);?></label>
								<input type="hidden" name="id" value="<?php echo $id; ?>">
								<input type="text" name="nama" class="form-control" placeholder="<?php echo ucfirst($menu);?>" value="<?php echo $tmp->key; ?>">
							</div>
						</div>
						<div class="col-md-9">
							<div class="pull-left">
								<button type="submit" class="btn btn-primary" style="margin-top: 25px;">Submit</button>
							</div>
						</div>
					</div>
					</form>
				</div>
			</div>
        		<?php
        		}
        	}
        	?>
			<div class="box box-default">
				<div class="box-body">
					<b>DATA <?php echo strtoupper($menu);?></b>
					<div style="border: 1px solid black; margin-bottom: 10px"></div>
					<table class="table table-bordered table-striped table-hover" id="example1" width="100%">
						<thead>
							<tr>
								<th class="text-center" width="10%">No</th>
								<th class="text-center"><?php echo ucfirst($menu);?></th>
								<th class="text-center" width="10%">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$no = 1;
							$query = $this->db->query("SELECT * FROM ".$table." WHERE ISNULL(delete_by) ORDER BY id");
							foreach ($query->result() as $tmp) {
								?>
							<tr>
								<td class="text-center"><?php echo $no++; ?></td>
								<td><?php echo $tmp->key; ?></td>
								<td width="190px">
									<div class="pull-right">
										<a href="<?php echo site_url($controls.'/editData').'/'.$tmp->id ?>" class="btn btn-warning" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;&nbsp;

										<a href="#" class="btn btn-danger" data-toggle="tooltip" data-placement="bottom" onclick="deleteData('<?php echo $tmp->id;?>');" title="Delete"><i class="fa fa-trash"></i></a>
									</div>
								</td>
							</tr>
								<?php
							}
							?>
						</tbody>
					</table>
				</div>
			</div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->