<?php
class cetak extends CI_Controller{
	function __construct(){
		parent::__construct();

		if(!isLogin()){
			goLogin();
		}
	}
	
	public function sertifikasi(){
		$kode  = sessionValue('kode_store');

		$data['query_gem'] = "SELECT a.* FROM tb_lab_desk a
							LEFT JOIN step b ON b.id_object = a.id_object
							WHERE 
							b.gem_card != '' AND
							b.step='LAB_DESK' AND
							b.status='APPROVED' AND
							b.print_gem='Belum' AND
							b.status_barang='TIDAK TERTINGGAL' AND
							b.store = '$kode'";

		$data['query_cert'] = "SELECT a.* FROM tb_lab_desk a
							LEFT JOIN step b ON b.id_object = a.id_object
							WHERE
							b.certificate <>'' AND
							b.step='LAB_DESK' AND
							b.status='APPROVED' AND
							b.print_cert='Belum' AND
							b.status_barang='TIDAK TERTINGGAL' AND
							b.store = '$kode'";

		$data['menu'] = "print_sertifikasi";
		$data['filelist'] = "cetak/front";
		$data['title'] = "Lab";
		$data['title_menu'] = "Lab";
		$data['bread'] = "Sertifikasi";
		getHTMLWeb($data);
	}
	public function grading(){
		$kode  = sessionValue('kode_store');
		$data['query'] = "SELECT tb_lab_grading.* FROM tb_lab_grading
							LEFT JOIN step ON step.id_object = tb_lab_grading.id_object
							WHERE 
							step.dia_grading != '' 
							AND step.print_grading='Belum' 
							AND step.check_dg != '' 
							AND step.store='$kode' AND step.status_grading='APPROVED'
							AND step.status_barang='TIDAK TERTINGGAL' ";
		$data['menu'] = "print_grading";
		$data['filelist'] = "cetak/front";
		$data['title'] = "Lab";
		$data['title_menu'] = "Lab";
		$data['bread'] = "Grading";
		getHTMLWeb($data);
	}
	public function taksiran(){
		$data['query'] = "SELECT tb_taksiran.id, tb_taksiran.id_order, master_jenis_permata.jenis, tb_taksiran.berat_bersih, tb_taksiran.berat_kotor, tb_taksiran.jumlah_permata, tb_fpjt.create_date,
						tb_taksiran.id_member, tb_member.first_name nama
						FROM tb_taksiran
						LEFT JOIN tb_fpjt ON tb_fpjt.id = tb_taksiran.id
						LEFT JOIN master_jenis_permata ON master_jenis_permata.id = tb_taksiran.jenis_perhiasan
						LEFT JOIN step ON step.id_object = tb_fpjt.id
						LEFT JOIN tb_member ON tb_member.id_member = tb_taksiran.id_member
						WHERE 
						tb_fpjt.status='SUDAH' 
						AND step.status='APPROVED' 
						AND step.print = 'Belum'
						AND step.status_barang='TIDAK TERTINGGAL'
						GROUP BY tb_taksiran.id_order ";
		$data['menu'] = "print_taksiran";
		$data['filelist'] = "cetak/front";
		$data['title'] = "Lab";
		$data['title_menu'] = "Lab";
		$data['bread'] = "Taksiran";
		getHTMLWeb($data);
	}
}
?>