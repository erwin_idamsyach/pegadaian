    
    var inputQuantity = [];
    $(function() {
      $(".quantity").each(function(i) {
        inputQuantity[i]=this.defaultValue;
        $(this).data("idx",i); // save this field's index to access later
      });
      $(".quantity").on("keyup", function (e) {
        var $field = $(this),
        val=this.value,
          $thisIndex=parseInt($field.data("idx"),10); // retrieve the index
    //        window.console && console.log($field.is(":invalid"));
              //  $field.is(":invalid") is for Safari, it must be the last to not error in IE8
          if (this.validity && this.validity.badInput || isNaN(val) || $field.is(":invalid") ) {
            this.value = inputQuantity[$thisIndex];
            return;
          } 
          if (val.length > Number($field.attr("maxlength"))) {
            val=val.slice(0, 11);
            $field.val(val);
          }
        inputQuantity[$thisIndex]=val;
      });      
    });

    function get_kota(){
      var id_prov = $(".prov").val(); 
      $.ajax({
        type : "POST",
        url : base_url+"tambahcif/kota/"+id_prov,
        data : "id_prov="+id_prov,
        success:function(html){ 
          $(".kota").html(html);

          $(".kota").on("change", function(){
            var id_kota = $(".kota").val();
            $.ajax({
              type : "POST",
              url : base_url+"tambahcif/kecam/"+id_kota,
              data : "id_kota="+id_kota,
              success:function(html){
                $(".keca").html(html);
					$(".keca").on("change", function(){
						var id_kec = $(".keca").val();
						$.ajax({
						  type : "POST",
						  url : base_url+"tambahcif/kelur/"+id_kec,
						  data : "id_kec="+id_kec,
						  success:function(html){
							$(".desa").html(html);
							
							$(".desa").on("change", function(){
								var id_desa = $(".desa").val();
								$.ajax({
								  type : "POST",
								  url : base_url+"tambahcif/kode_pos/"+id_desa,
								  data : "id_desa="+id_desa,
								  success:function(html){
									$(".kode_pos").val(html);
								  }
								});
							});
						  }
						});
					});
              }
            });
          });
        }
      });
    }
	
	function get_kota2(){
      var id_prov = $(".prov2").val(); 
      $.ajax({
        type : "POST",
        url : base_url+"tambahcif/kota/"+id_prov,
        data : "id_prov="+id_prov,
        success:function(html){ 
          $(".kota2").html(html);

          $(".kota2").on("change", function(){
            var id_kota = $(".kota2").val();
            $.ajax({
              type : "POST",
              url : base_url+"tambahcif/kecam/"+id_kota,
              data : "id_kota="+id_kota,
              success:function(html){
                $(".keca2").html(html);
					$(".keca2").on("change", function(){
						var id_kec = $(".keca2").val();
						$.ajax({
						  type : "POST",
						  url : base_url+"tambahcif/kelur/"+id_kec,
						  data : "id_kec="+id_kec,
						  success:function(html){
							$(".desa2").html(html);
							
							$(".desa2").on("change", function(){
								var id_desa = $(".desa2").val();
								$.ajax({
								  type : "POST",
								  url : base_url+"tambahcif/kode_pos/"+id_desa,
								  data : "id_desa="+id_desa,
								  success:function(html){
									$(".kode_pos2").val(html);
								  }
								});
							});
						  }
						});
					});
              }
            });
          });
        }
      });
    }
	
	function get_kota3(){
      var id_prov = $(".prov3").val();
      $.ajax({
        type : "POST",
        url : base_url+"tambahcif/kota/"+id_prov,
        data : "id_prov="+id_prov,
        success:function(html){
          $(".kota3").html(html);

          $(".kota3").on("change", function(){
            var id_kota = $(".kota3").val();
            $.ajax({
              type : "POST",
              url : base_url+"tambahcif/kecam/"+id_kota,
              data : "id_kota="+id_kota,
              success:function(html){
                $(".keca3").html(html);
              }
            });
          });
        }
      });
    }

    function delete_indi(id){
      $.ajax({
        type : "POST",
        url  : base_url+"reg_customer/delete/"+id,
        data : "id="+id,
        success:function(html){
          window.location.reload();
        }
      });
    }

    function dewa(batu){
      $.ajax({
        type : "POST",
        url  : base_url+"obj_today/ambilbatu",
        data : "batu="+batu,
        success:function(html){
          $('.spec').html(html);
        }
      });
    }

    function final_advice(){
      var ha = $('#hardness').val();
      var ri = $('#ri_start').val();
      var sg = $('#sg_end').val();
      $.ajax({
        type : "POST",
        url  : base_url+"obj_today/finalx",
        data : {
          'hard'  : ha,
          'ri'    : ri,
          'sg'    : sg
        },
        success:function(html){
          $('.cobafinal').html(html);
        }
      });
    }

    function delete_order(id){
      $.ajax({
        type : "POST",
        url  : base_url+"obj_input/delete_order/"+id,
        data : "id="+id,
        success:function(html){
          window.location.reload();
        }
      });
    }

    function view(id){
      $.ajax({
        type : "POST",
        url  : base_url+"obj_today/view_obj/"+id,
        data : "id="+id,
        success:function(html){
          $('#myModal').modal('show');
          $('.lol').html(html);
        }
      });
    }

    function gocedit(id){
      $.ajax({
        type : "POST",
        url  : base_url+"object/edit_obj/"+id,
        data : "id="+id,
        success:function(html){ 
          $('.index').html(html);
        }
      });
    }
    function goceditexpo(id){
      $.ajax({
        type : "POST",
        url  : base_url+"object/edit_obj_expo/"+id,
        data : "id="+id,
        success:function(html){ 
          $('.index').html(html);
        }
      });
    }
	
	// Hadi
	function goViewApproveCertificate(id){
      $.ajax({
        type : "POST",
        url  : base_url+"approve/view_approve_sertifikasi/"+id,
        data : "id="+id,
        success:function(html){ 
          $('.index').html(html);
        }
      });
    }
	
	function goViewApproveGrading(id){
      $.ajax({
        type : "POST",
        url  : base_url+"approve/view_approve_grading/"+id,
        data : "id="+id,
        success:function(html){ 
          $('.index').html(html);
        }
      });
    }
	
	function goViewApproveTaksiran(id, id_order){
      $.ajax({
        type : "POST",
        url  : base_url+"approve/view_approve_taksiran/"+id+"/"+id_order,
        data : "id="+id,
        success:function(html){ 
          $('.index').html(html);
        }
      });
    }
	// End Hadi

    function editcorpo(id){
      $.ajax({
        type : "POST",
        url  : base_url+"corporate/edit_corpo/"+id,
        data : "id="+id,
        success:function(html){
          $('.index').html(html);
        }
      });
    }

    function editindi(id){
      $.ajax({
        type : "POST",
        url  : base_url+"liscif/edit_indiv/"+id,
        data : "id="+id,
        success:function(html){
          $('.index').html(html);
        }
      });
    }


    function goeditobj(id){
      $.ajax({
        type : "POST",
        url  : base_url+"obj_input/edit_obj/"+id,
        data : "id="+id,
        success:function(html){
          $('.index').html(html);
          $("body, html").scrollTop('.index');
        }
      });
    }

    function godadit(id){
      $.ajax({
        type : "POST",
        url  : base_url+"object/edit_dg/"+id,
        data : "id="+id,
        success:function(html){
          $('.index').html(html);
        }
      });
    }

    function edi_servi(id){
      $.ajax({
        type : "POST",
        url  : base_url+"service/edi_servi/"+id,
        data : "id="+id,
        success:function(html){
          $('#myModal').modal('show');
          $('.lol').html(html);
        }
      });
    }

    function save_service(id){
      var code  = $('.code').val();
      var name  = $('.name').val();
      var price = $('.price').val();
      $.ajax({
        type : "POST",
        url  : base_url+"service/edit_service/"+id,
        data : {
          'code'  : code,
          'name'  : name,
          'price' : price
        },
        success:function(html){
          window.location.reload();
        }
      });
    }


    function chos(){
      var du = $("#obj").val();
      $.ajax({
        type : "POST",
        url : base_url+"obj_input/color/"+du,
        data : "main="+du,
        success:function(ht){
          $("#col-area").html(ht);
        }
      });
    }

    function save_cif_individu() {
      var no_cif = $('#no_cif').val();
	  var cabang = $('#cabang').val();
      var nama_depan = $('#nama_depan').val();
      //var nama_tengah = $('#nama_tengah').val();
      //var nama_belakang = $('#nama_belakang').val();
      var nama_ibu = $('#nama_ibu').val();
      //var gelar = $('#gelar').val();
      var tempat_lahir = $('#tempat_lahir').val();
      var tanggal_lahir = $('#tanggal_lahir').val();
      var agama = $('#agama').val();
      var identitas = $('#identitas').val();
      var no_identitas = $('#no_identitas').val();
      var masa_berlaku = $('#masa_berlaku').val();
      var jenis_kelamin = $('#jenis_kelamin').val();
      var pendidikan = $('#pendidikan').val();
      var perkawinan = $('#perkawinan').val();
      var nama_pasangan = $('#nama_pasangan').val();
      var telpon = $('#telpon').val();
      var handphone = $('#handphone').val();
      var email = $('#email').val();
      var no_npwp = $('#no_npwp').val();
      var kewarganegaraan = $('#kewarganegaraan').val();
      var kewarganegaraan_lainnya = $('#kewarganegaraan_lainnya').val();
      var sumber_dana = $('#sumber_dana').val();
      var penghasilan = $('#penghasilan').val();
      var pekerjaan = $('#pekerjaan').val();
      var alamat = $('#alamat').val();
      var propinsi = $('#propinsi').val();
      var kota = $('#kota').val();
      var kecamatan = $('#kecamatan').val();
      var kelurahan = $('#kelurahan').val();
      var kode_pos = $('#kode_pos').val();
      var status_tempat_tinggal = $('#status_tempat_tinggal').val();
      var menempati_sejak = $('#menempati_sejak').val();
      var alamat_domisili = $('#alamat_domisili').val();
      var propinsi2 = $('#propinsi2').val();
      var kota2 = $('#kota2').val();
      var kecamatan2 = $('#kecamatan2').val();
      var kelurahan2 = $('#kelurahan2').val();
      var kode_pos2 = $('#kode_pos2').val();
      var alamat_surat_menyurat = $('#alamat_surat_menyurat').val();
      //var produk = $('#produk').val();
	  
      if(no_cif == "" || cabang == "" || nama_depan == "" || nama_ibu == "" || tempat_lahir == "" || agama == "" || identitas == "" || no_identitas == ""){
		return false;
      }else{
		swal({
             title: "",
             text: "Apakah Anda akan menyimpan data?",
             type: "warning",
             showCancelButton: true,
             confirmButtonColor: "#DD6B55",
             confirmButtonText: "Ya",
             cancelButtonText: "Tidak",
             closeOnConfirm: false }, function(){
                $.ajax({
				  type : "POST",
				  url  : base_url+"tambahcif/save_cif_individu",
				  data : {
					'no_cif' : no_cif,
					'cabang' : cabang,
					'nama_depan' : nama_depan,
					//'nama_tengah' : nama_tengah,
					//'nama_belakang' : nama_belakang,
					'nama_ibu'    : nama_ibu,
					//'gelar'     : gelar,
					'tempat_lahir'     : tempat_lahir,
					'tanggal_lahir'     : tanggal_lahir,
					'agama'     : agama,
					'identitas'     : identitas,
					'no_identitas'     : no_identitas,
					'masa_berlaku'     : masa_berlaku,
					'jenis_kelamin'     : jenis_kelamin,
					'pendidikan'     : pendidikan,
					'perkawinan'     : perkawinan,
					'nama_pasangan'     : nama_pasangan,
					'telpon'     : telpon,
					'handphone'     : handphone,
					'email'     : email,
					'no_npwp'     : no_npwp,
					'kewarganegaraan'     : kewarganegaraan,
					'kewarganegaraan_lainnya'     : kewarganegaraan_lainnya,
					'sumber_dana'     : sumber_dana,
					'penghasilan'     : penghasilan,
					'pekerjaan'     : pekerjaan,
					'alamat'     : alamat,
					'propinsi'     : propinsi,
					'kota'     : kota,
					'kecamatan'     : kecamatan,
					'kelurahan'     : kelurahan,
					'kode_pos'     : kode_pos,
					'status_tempat_tinggal'     : status_tempat_tinggal,
					'menempati_sejak'     : menempati_sejak,
					'alamat_domisili'     : alamat_domisili,
					'propinsi2'     : propinsi2,
					'kota2'     : kota2,
					'kecamatan2'     : kecamatan2,
					'kelurahan2'     : kelurahan2,
					'kode_pos2'     : kode_pos2,
					'alamat_surat_menyurat'     : alamat_surat_menyurat
					//'produk'     : produk
				  },
				  success:function(html){
					var data = eval ("(" + html + ")");
					if(data.success){
						swal({
						  title: "Berhasil Menyimpan data!",
						  text: "Klik tombol di bawah.",
						  type: "success",
						  showCancelButton: false,
						  confirmButtonColor: "#257DB6",
						  confirmButtonText: "Ok!",
						  closeOnConfirm: false
						},
						function(){
						  location.href = base_url+"tambahcif";
						});
					}else{
						swal.close();
						popOverMsg('no_identitas', data.msg);
					}
					
				  }
				});
            });	
        return false;
      }
    }
	
	function save_cif_coorporate() {
      var no_cor = $('#no_cor').val();
	  var nama_perusahaan = $('#nama_perusahaan').val();
      var tanggal_pendirian = $('#tanggal_pendirian').val();
      var telpon_perusahaan = $('#telpon_perusahaan').val();
      var alamat_perusahaan = $('#alamat_perusahaan').val();
      var propinsi_perusahaan = $('#propinsi_perusahaan').val();
      var kota_perusahaan = $('#kota_perusahaan').val();
      var kecamatan_perusahaan = $('#kecamatan_perusahaan').val();
      var kode_pos_perusahaan = $('#kode_pos_perusahaan').val();
      var email_perusahaan = $('#email_perusahaan').val();
      var no_npwp_perusahaan = $('#no_npwp_perusahaan').val();
      var no_rekening_perusahaan = $('#no_rekening_perusahaan').val();
      var siup_perusahaan = $('#siup_perusahaan').val();
      var bidang_usaha_perusahaan = $('#bidang_usaha_perusahaan').val();
      var tdp_perusahaan = $('#tdp_perusahaan').val();
      var nama_izin_usaha_perusahaan = $('#nama_izin_usaha_perusahaan').val();
      var no_izin_usaha_perusahaan = $('#no_izin_usaha_perusahaan').val();
      var bentuk_perusahaan = $('#bentuk_perusahaan').val();
      var bentuk_perusahaan_lainnya = $('#bentuk_perusahaan_lainnya').val();
      var tujuan_transaksi_perusahaan = $('#tujuan_transaksi_perusahaan').val();
	  
      if(no_cor == "" || nama_perusahaan == "" || tanggal_pendirian == "" || telpon_perusahaan == "" || alamat_perusahaan == "" || no_npwp_perusahaan == "" || tdp_perusahaan == ""){
		return false;
      }else{
		swal({
             title: "",
             text: "Apakah Anda akan menyimpan data?",
             type: "warning",
             showCancelButton: true,
             confirmButtonColor: "#DD6B55",
             confirmButtonText: "Ya",
             cancelButtonText: "Tidak",
             closeOnConfirm: false }, function(){
                $.ajax({
				  type : "POST",
				  url  : base_url+"tambahcif/save_cif_coorporate",
				  data : {
					'no_cor' : no_cor,
					'nama_perusahaan' : nama_perusahaan,
					'tanggal_pendirian' : tanggal_pendirian,
					'telpon_perusahaan' : telpon_perusahaan,
					'alamat_perusahaan' : alamat_perusahaan,
					'propinsi_perusahaan'    : propinsi_perusahaan,
					'kota_perusahaan'     : kota_perusahaan,
					'kecamatan_perusahaan'     : kecamatan_perusahaan,
					'kode_pos_perusahaan'     : kode_pos_perusahaan,
					'email_perusahaan'     : email_perusahaan,
					'no_npwp_perusahaan'     : no_npwp_perusahaan,
					'no_rekening_perusahaan'     : no_rekening_perusahaan,
					'siup_perusahaan'     : siup_perusahaan,
					'bidang_usaha_perusahaan'     : bidang_usaha_perusahaan,
					'tdp_perusahaan'     : tdp_perusahaan,
					'nama_izin_usaha_perusahaan'     : nama_izin_usaha_perusahaan,
					'no_izin_usaha_perusahaan'     : no_izin_usaha_perusahaan,
					'bentuk_perusahaan'     : bentuk_perusahaan,
					'bentuk_perusahaan_lainnya'     : bentuk_perusahaan_lainnya,
					'tujuan_transaksi_perusahaan'     : tujuan_transaksi_perusahaan
				  },
				  success:function(html){ 
					var data = eval ("(" + html + ")");
					if(data.success){
						swal({
						  title: "Berhasil Menyimpan data!",
						  text: "Klik tombol di bawah.",
						  type: "success",
						  showCancelButton: false,
						  confirmButtonColor: "#257DB6",
						  confirmButtonText: "Ok!",
						  closeOnConfirm: false
						},
						function(){
						  location.href = base_url+"tambahcif";
						});
					}else{
						swal.close();
						popOverMsg('no_npwp_perusahaan', data.msg);
					}
				  }
				});
            });	
        return false;
      }
    }

    function edit_member_indi(id) {
      var fir_name = $('#fir_name').val();
      var mid_name = $('#mid_name').val();
      var las_name = $('#las_name').val();
      var phone    = $('#phone').val();
      var prov     = $('.prov').val();
      var kota     = $('.kota').val();
      var keca     = $('.keca').val();
      var postal   = $('#postal').val();
      var address  = $('#address').val();
      var email    = $('#email').val();
      $.ajax({
        type : "POST",
        url  : base_url+"reg_customer/edit_mem_indi/"+id,
        data : {
          'fir_name' : fir_name,
          'mid_name' : mid_name,
          'las_name' : las_name,
          'phone'    : phone,
          'prov'     : prov,
          'kota'     : kota,
          'keca'     : keca,
          'postal'   : postal,
          'address'  : address,
          'email'    : email
        },
        success:function(html){
          window.location.reload();
        }
      });
    }

    function member_corp() {
      var fir_name = $('#fir_name').val();
      var pic      = $('#pic').val();
      var person   = $('#person').val();
      var phone    = $('#phone').val();
      var prov     = $('.prov').val();
      var kota     = $('.kota').val();
      var keca     = $('.keca').val();
      var postal   = $('#postal').val();
      var address  = $('#address').val();
      var email    = $('#email').val();
      if(fir_name == "" || pic == "" || person == "" || phone == "" || kota == "" || prov == "" || keca == "" || address == ""){

      }else{
        $.ajax({
          type : "POST",
          url  : base_url+"corporate/mem_corp",
          data : {
            'fir_name' : fir_name,
            'person'   : person,
            'pic'      : pic,
            'phone'    : phone,
            'prov'     : prov,
            'kota'     : kota,
            'keca'     : keca,
            'postal'   : postal,
            'address'  : address,
            'email'    : email
          },
          success:function(html){
            window.location.reload();
          }
        });
      }
    }

    function edit_member_corp(id) {
      var fir_name = $('#fir_name').val();
      var pic      = $('#pic').val();
      var person   = $('#person').val();
      var phone    = $('#phone').val();
      var prov     = $('.prov').val();
      var kota     = $('.kota').val();
      var keca     = $('.keca').val();
      var postal   = $('#postal').val();
      var address  = $('#address').val();
      var email    = $('#email').val();
        $.ajax({
          type : "POST",
          url  : base_url+"corporate/edit_mem_corp/"+id,
          data : {
            'fir_name' : fir_name,
            'person'   : person,
            'pic'      : pic,
            'phone'    : phone,
            'prov'     : prov,
            'kota'     : kota,
            'keca'     : keca,
            'postal'   : postal,
            'address'  : address,
            'email'    : email
          },
          success:function(html){
            window.location.reload();
        }
      });
    }

    function next_member(){
      $.ajax({
        type : "POST",
        url  : base_url+"obj_input/reset_member",
        success:function(html){
          window.location.reload();
        }
      }); 
    }

    function edit_obj(id){
      var id_mem  = $('#id_mem').val();
      var weight  = $('#weight').val();
      var length  = $('#length').val();
      var width   = $('#width').val();
      var height  = $('#height').val();
      var color   = $("input[name=spe_col]:checked").val();
      var gambar  = $('#gambarout').val();
        $.ajax({
          type : "POST",
          url  : base_url+"obj_input/edi_obj/"+id,
          data : {
            'id_mem'   : id_mem,
            'weight'   : weight,
            'length'   : length,
            'width'    : width,
            'height'   : height,
            'color'    : color,
            'gambar'   : gambar
          },
          success:function(html){
            window.location.reload();
          }
      });
    }


    function add_obj() {
      var id_mem  = $('#id_mem').val();
      var weight  = $('#weight').val();
      var length  = $('#length').val();
      var width   = $('#width').val();
      var height  = $('#height').val();
      var color   = $("input[name=spe_col]:checked").val();
      var grading = $(".class3:checked").val();
      var certifi = $(".class2:checked").val();
      var gem_car = $(".class1:checked").val();
      var gambar  = $('#gambarout').val();
      if(id_mem == "" || weight == "" || length == "" || width == "" || height == "" || color == "" || gambar == ""){

      }else{
        $.ajax({
          type : "POST",
          url  : base_url+"obj_input/add_obj",
          data : {
            'id_mem'   : id_mem,
            'weight'   : weight,
            'length'   : length,
            'width'    : width,
            'height'   : height,
            'color'    : color,
            'grading'  : grading,
            'gem_car'  : gem_car,
            'certifi'  : certifi,
            'gambar'   : gambar
          },
          success:function(html){
            window.location.reload();
          }
        });
      }
    }

    function printorder(id){
      var newWindow = window.open();
      $.ajax({
        type : "POST",
        url  : base_url+"obj_input/check_print/"+id,
        data : "id="+id,
        success:function(html){
          if(html == "OJO PRINT"){
            newWindow.close();
            swal('','DATA EMPTY','error');
          }else if(html == "GO PRINT"){
            newWindow.location = base_url+'obj_input/order_print/'+id;
            window.location=(base_url+'/obj_input/unset_data');
          }
        }
      });
    }

    function check_member(){
      var member = $('#id_mem').val();
      $.ajax({
        type : "POST",
        url  : base_url+"obj_input/check_member",
        data : "member="+member,
        success:function(html){
          if(html == "ono"){
            $(".form-member").removeClass('has-error').addClass('has-success');
          }else if(html == "ora ono"){
            $(".form-member").removeClass('has-success').addClass('has-error');
          }
        }
      });
    }

    function printinvo(id){
      var newWindow = window.open();
      $.ajax({
        type : "POST",
        url  : base_url+"obj_input/check_invo/"+id,
        data : "id="+id,
        success:function(html){
          if(html == "OJO PRINT"){
            $('.alertx').modal('show');
          }else if(html == "GO PRINT"){
            newWindow.location = base_url+'obj_input/invoice_print/'+id;
            window.location.reload();
          }
        }
      });
    }

    $('#class3').change(function () {
      if ($(this).attr("checked")) {
        $('#class2').attr('disabled', true);
        $('#class1').attr('disabled', true);
      } else {
        $('#class1').attr('disabled', false);
        $('#class2').attr('disabled', false);
      }
    });

    function cek_user(){
      var user = $("#user").val();
      $.ajax({
        method : 'post',
        url : base_url+'user_management/cek_user/'+user,
        data : 'user='+user,
        dataType : 'json',
        success:function(response) {
          if(response['status'] == "available"){
            $(".form-user").removeClass('has-error').addClass('has-success');
			$("#avaliable_user").val("true");
          }else{
            $(".form-user").removeClass('has-success').addClass('has-error');
			$("#avaliable_user").val("false");
		  }
        }
      });
    }

    function cek_same(){
      var pass1 = $("#pass-1").val();
      var pass2 = $("#pass-2").val();
      if(pass1 != pass2){
        $("#password-1").removeClass().addClass('form-group has-error');
        $("#password-2").removeClass().addClass('form-group has-error');
      }else{
        $("#password-1").removeClass().addClass('form-group has-success');
        $("#password-2").removeClass().addClass('form-group has-success');
      }
    }
    function view_data(key){
      $.ajax({
        type : "post",
        url : "user_management/ajax_data/"+key,
        data: "data="+key,
        success:function(response){
          $("#modal-user").modal('show');
          $(".area-data-jos").html(response);
        }
      });
    }
    function cek_lab(){
      var cek = $(".access").val();
      if(cek == "Lab_Desk"){
        $(".disb").attr('disabled',false);
      }else{
        $(".disb").attr('disabled',true);
      }
    }
	
	// Funtion 
	function popOverMsg(id, msg){
		$('#'+id).focus();
		$('#'+id).popover({
			content: msg,
			placement: 'bottom',
			trigger: 'focus'
		});
		$("."+id).removeClass("has-success");
		$("."+id).addClass("has-error");
	}
	
	function autoCompleteData(id){
		var options = {
			url: base_url+'add_fpjt/autoComplete',
			getValue: "name",
			list: {
				maxNumberOfElements: 5,
				match: {
					enabled: true
				},
				onClickEvent: function() {
					cekCIFName();
				}	
			}
		};
		
		$("#"+id).easyAutocomplete(options);
	}
	
	function cekCIFName(){
          var kode = $("#nama").val();
		  kode = kode.replace(" ", "-");
		   kode = kode.replace(" ", "-");
          $.ajax({
            type : "POST",
            url : base_url+"obj_input/cekCifName/"+kode,
            data: "kode="+kode,
            dataType : "json",
            success:function(response){
              if(response['status'] == "SUCCESS"){
                $("#id_mem").val(response['id_member']);
                $(".alamat").val(response['alamat']);
                $(".telp").val(response['phone']);
                $(".form-cif").removeClass("has-error");
                $(".form-cif").addClass("has-success");
              }else{
                $(".form-cif").removeClass("has-success");
                $(".form-cif").addClass("has-error");
              }
            }
          });
    }
	function startTaksiran(key, id_order){
		$.ajax({
        type : "POST",
        url  : base_url+"object/startTaksiran",
        data : "id="+key+"&id_order="+id_order,
        success:function(html){ 
          $('.index').html(html);
        }
      });
	}
  function cariData(target, type){
    var weight = $("#inp-weight").val();
    var date   = $("#inp-date").val();

    $.ajax({
      type : "GET",
      url : base_url+"searching/data",
      data : {
        "weight" : weight,
        "date"   : date,
        "type"   : type
      },
      success:function(resp){
        $(target).html(resp);
      }
    });
  }
  function refreshTable(target, type){
    $.ajax({
      type : "GET",
      url : base_url+"searching/data",
      data : {
        "weight" : "",
        "date"   : "",
        "type"   : type
      },
      success:function(resp){
        $(target).html(resp);
      }
    });
  }